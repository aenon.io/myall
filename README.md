# MyAll

## Public Database

Local MariaDB installation, you can access with the following details:

- **Username:** `myall`
- **Password:** `MN64V$hsat4r`
- **Database:** `myall`

## Test User Credentials

For testing purposes, you can log into the application using the following test
users:

- **Email:** `test@myall.co.za`
- **Password:** `12341234`

Alternatively:

- **Email:** `test1@myall.co.za`
- **Password:** `12341234`

## Code Structure Overview

The MyAll project is built using the **Laravel** framework with **Livewire** components.

### Hosting Information

The application is hosted on **Amazon Lightsail** and can be accessed using the
following credentials:

- **Server IP (Static):** `18.130.118.67`
- **User:** `ubuntu`
- **SSH Key:** `LightsailDefaultKey-eu-west-2.pem` (The key can be shared upon request)

### Local Development Setup

For local development, you can set up a Homestead environment. Detailed setup
instructions can be found on the official [Laravel Homestead
documentation](https://laravel.com/docs/homestead).

### Git Repository

The project repository is hosted on GitLab.
