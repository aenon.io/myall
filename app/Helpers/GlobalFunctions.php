<?php function getInitials($name)
{
    $words = explode(' ', $name);
    if (count($words) === 1) {
        return isset($words[0][0]) ? strtoupper($words[0][0]) : '';
    } else {
        $firstInitial = isset($words[0][0]) ? $words[0][0] : '';
        $lastInitial = isset($words[count($words) - 1][0]) ? $words[count($words) - 1][0] : '';
        return strtoupper($firstInitial . $lastInitial);
    }
}
