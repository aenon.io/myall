<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use Session;

class AuthController extends Controller
{
    public function logOut(){
        Session::flush();
        Auth::logout();
        return redirect('/');
    }
}
