<?php

namespace App\Http\Livewire\Account;

use Livewire\Component;

class FamilyTree extends Component
{
    public $relations;

    public function mount(){
        $this->relations = ['Father', 'Mother', 'Wife', 'Hasband', 'Sister', 'Brother', 'Daughter', 'Son'];
    }

    public function render(){
        return view('livewire.account.family-tree');
    }
}
