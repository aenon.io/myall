<?php

namespace App\Http\Livewire\Account\FamilyTree;

use Livewire\Component;
use Livewire\WithFileUploads;
use Auth;
use App\Models\Family;
use App\Lib\FamilyTreeGen;
use App\Models\PopUp;

class FamilyTree extends Component
{
    use WithFileUploads;

    public $relations;
    public $show_form;
    public $name, $surname, $relation, $dob, $id_number, /**$gender,**/ $living, $show_on_tree, $dod, $color, $photo, $cur_rel, $other_parent;

    protected $listeners = [
        'showForm' => 'processShowForm', 
        'delete-node' => 'deleteNode',
        'show-edit-node' => 'showEditNode'
    ];

    public $tree;

    public $cur_id, $cur_person;
    public $show_other_parent = false, $spouse_title, $spouse_list;
    public $p_id;
    
    public $show_init_pop_up = false;

    public $cur_node_id;
    public $cur_name, $cur_surname, $cur_dob, $cur_id_number, $cur_email, $cur_color, $cur_photo;
    //public $cur_node_id;

    public function processShowForm($id = null){
        if($id){
            $node = Family::find($id);
            if($node){
                $living = true;
                if($node->dod){
                    $living = false;
                }
                $this->name = $node->name; 
                $this->surname = $node->surname;
                $this->relation = $node->relation;
                $this->dob = $node->dob;
                $this->id_number = $node->id_number;
                $this->living = $living;
                $this->show_on_tree = $node->show_on_tree;
                $this->dod = $node->dod;
                $this->color = $node->color;
                //$this->photo, 
                //$this->cur_rel, 
                //$this->other_parent
                
                $this->cur_node_id = $id;
            }
        }
        $this->show_form = true;
    }
    
    public function saveEditNode(){
        if($this->cur_node_id){
            $node = Family::find($this->cur_node_id);
            if($node){
                
                $node->name = $this->name;
                $node->surname = $this->surname;
                $node->dob = $this->dob;
                $node->id_number = $this->id_number;
                // $node->email = $this->email;
                $node->color = $this->color;
                $node->show_on_tree = $this->show_on_tree;
                
                //$name, 
                //$surname, 
                //$relation, $dob, $id_number, /**$gender,**/ $living, $show_on_tree, $dod, $color, $photo, $cur_rel, $other_parent;
                
                //dd($this->photo);
                
                if($this->photo){
                    $path = $this->photo->storePublicly('photos', 'public');
                    $node->img = $path;
                    //dd($path);
                }
                $node->save();
                //dd($node);
                $this->dispatchBrowserEvent('hide-modals');
                $this->dispatchBrowserEvent('data-updated', ['message' => 'Member added to family tree successfully.']);
                $this->cur_node_id = null;
            }
        }
    }
    public function showEditNode($id){
        $node = Family::find($id);
        if($node){
            $this->cur_node_id = $id;
            
            $this->cur_name = $node->name;
            $this->cur_surname = $node->surname;
            $this->cur_dob = $node->dob;
            $this->cur_id_number = $node->id_number;
            $this->cur_email = $node->email;
            $this->cur_color = $node->color;

            $this->dispatchBrowserEvent('show-edit-node-modal');
        }
    }

    public function deleteNode($id){
        $node = Family::find($id);
        $node->delete();
        $this->checkFamily();
        $this->dispatchBrowserEvent('data-updated', ['message' => 'Member added to family tree successfully.']);
    }

    public function mount(){
        $this->relations = [
            'Great Grand Father',
            'Great Grand Mother',

            'Grand Father', 
            'Grand Mother',

            'Father', 
            'Uncle',
            'Step Father',
            'Father In Law',

            'Mother',
            'Aunt',
            'Step Mother',
            'Mother In Law',
            
            'Brother', 
            'Step Brother',
            'Brother In Law',
            
            'Sister',
            'Step Sister',
            'Sister In Law',

            'Cousin',
            'Friend', 

            'Spouse',
            'Partner', 
            'Wife',
            'Husband',

            'Son', 
            'Daughter',
            'Son In Law',
            'Daughter In Law',
            "Son's Partner",
            "Daughter's Partner",
            'Niece', 
            'Nephew',

            'Grand Son', 
            'Grand Daughter',
            /*
            'Great Grand Son',
            'Great Grand Daughter',
            */
        ];
        sort($this->relations);
        $this->living = 1;
        $this->show_form = false;
        $this->show_on_tree = true;

        $this->checkFamily();
        //TESTING REMOVE ON LIVE
        //$this->processShowForm(1);
        
        $pop_up = PopUp::where('user_id', Auth::user()->id)->first();
        if($pop_up){
            if($pop_up->tree){
                $this->show_init_pop_up = false;
            }
            else{
                $this->show_init_pop_up = true;
            }
        }
        else{
            $this->show_init_pop_up = true;
            $pop_up = new PopUp();
            $pop_up->user_id = Auth::user()->id;
            $pop_up->save();
        }

    }
    public $is_father, $is_mother, $is_spouse, $is_child;
    public $children_list = null;
    public $ch_label;
    public $show_children_list = false, $show_parent_list = false;
    public $link_id;
    public $f_id, $m_id;
    public $show_child_spouse = false;
    public $children_spouses;
    
    public function SaveFamilyTreeMember(){
        $this->validate([
            'name' => 'required', 
            'surname' => 'required', 
            'relation' => 'required', 
            'dob' => 'required', 
            // 'id_number' => 'required', 
        ]);
        
        if($this->cur_node_id){
            $this->saveEditNode();
        }
        else{
            $id_number = null;
            $email = null;
            if(filter_var($this->id_number, FILTER_VALIDATE_EMAIL)) {
                $email = $this->id_number;
            }
            else{
                $id_number = $this->id_number;
            }

            // $person = Family::query()
            // ->when($id_number, function($q) use($id_number){
            //     return $q->where('id_number', $id_number);
            // })
            // ->when($email, function($q) use($email){
            //     return $q->where('email', $email);
            // })
            // ->first();
            
            // if(!$person){
                $person = new Family();
            // }
    
            $is_email = false;
            if(filter_var($this->id_number, FILTER_VALIDATE_EMAIL)) {
                $is_email = true;
            }
            
            $person->name = $this->name;
            $person->surname = $this->surname;
            $person->dob = $this->dob;
            $person->dod = $this->dod;
            if(!$is_email){
                $person->id_number = $this->id_number;
            }
            if($is_email){ 
                $person->email = $this->id_number;
            }
            $person->relation = $this->relation;
            $person->show_on_tree = $this->show_on_tree;
            $person->color = $this->color;
    
            $path = null;
            if($this->photo){
                $path = $this->photo->storePublicly('photos', 'public');
            }
            $person->img = $path;
            $person->save();
    
            $sibling_types = ['Brother', 'Step Brother', 'Sister', 'Step Sister', 'Cousin'];
            $spouse_arr = ['Spouse', 'Partner', 'Wife','Husband'];
            $parents_inlaw = ['Father In Law', 'Mother In Law'];
            $siblings_in_law = ['Brother In Law', 'Sister In Law'];
            $child_in_laws = ["Son In Law", "Daughter In Law", "Son's Partner", "Daughter's Partner"];
    
            $node = $this->cur_person;
            if($this->link_id){
                $link = Family::find($this->link_id);
                if($this->relation == "Uncle" || $this->relation == "Aunt"){
                    if($link->mid || $link->fid){
                        $person->mid = $link->mid;
                        $person->fid = $link->fid;
                    }
                    else{
                        $person->pids = $link->id;    
                    }
                }
                elseif(in_array($this->relation, $siblings_in_law)){
                    if($link->mid || $link->fid){
                        $person->mid = $link->mid;
                        $person->fid = $link->fid;
                    }
                    else{
                        $person->pid = $link->id;
                    }
                }
                elseif(in_array($this->relation, $parents_inlaw)){
                    if($this->relation == "Father In Law"){
                        $link->fid = $person->id;
                        if($link->mid){
                            $person->pids = $link->mid;
                        }
                    }
                    if($this->relation == "Mother In Law"){
                        $link->mid = $person->id;
                        if($link->fid){
                            $person->pids = $link->fid;
                        }
                    }
                }
                else{
                    if($this->is_father){
                        $cur_f = Family::find($link->fid);
                        if($cur_f){
                            if($cur_f->relation == "Great Grand Father"){
                                $person->fid = $cur_f->id;
                            }
                        }
                        $link->fid = $person->id;
    
                        if($link->mid){
                            $person->pids = $link->mid;
                        }
                    }
                    if($this->is_mother){
                        $cur_m = Family::find($link->mid);
                        if($cur_m){
                            if($cur_m->relation == "Great Grand Mother"){
                                $person->mid = $cur_m->id;
                            }
                        }
                        $link->mid = $person->id;
    
                        if($link->fid){
                            $person->pids = $link->fid;
                        }
                    }
                }
                
                $link->save();
                $person->save();
            }
            elseif(in_array($this->relation, $siblings_in_law)){
                if($this->p_id){
                    $partner = Family::find($this->p_id);
                    $person->pids = $this->p_id;
                    $person->save();
                }
            }
            elseif($this->relation == 'Grand Son' || $this->relation == 'Grand Daughter'){
                $ch = Family::find($this->cp_id);
                if($ch){
                    if($ch->relation == "Son"){
                        $person->fid = $ch->id;
                        $person->mid = $ch->pids;
                    }
                    else{
                        $person->mid = $ch->id;
                        $person->fid = $ch->pids;
                    }
                    $person->save();
                }
    
            }
            elseif(in_array($this->relation, $child_in_laws)){
                $child = Family::find($this->p_id);
                $child->pids = $person->id;
                $child->save();
                $person->pids = $this->p_id;
                $person->save();
            }
            elseif($this->relation == "Son" || $this->relation == "Daughter" || $this->relation == 'Niece' || $this->relation == 'Nephew'){
                $person->mid = $this->m_id;
                $person->fid = $this->f_id;
                $person->save();
            }
            elseif($this->relation == "Friend"){
                $person->pids = $node->id;
                $person->save();
            }
            elseif($this->relation == "Step Father" || $this->relation == "Step Mother"){
                if($this->relation == "Step Father"){
                    $link = Family::find($node->mid);
                }
                if($this->relation == "Step Mother"){
                    $link = Family::find($node->fid);
                }
                $person->pids = $link->id;
                $person->save();
            }
            elseif(in_array($this->relation, $sibling_types)){
                $person->mid = $this->m_id;
                $person->fid = $this->f_id;
                $person->save();
            }
            elseif(in_array($this->relation, $spouse_arr)){
                $person->pids = $node->id;
                $node->pids = $person->id;
                $node->save();
                $person->save();
            }
            else{
                if($this->is_father){
                    $cur_m = Family::find($node->fid);
                    if($cur_m){
                        if($cur_m->relation == "Great Grand Father" || $cur_m->relation == "Grand Father"){
                            $person->fid = $cur_m->id;
                        }
                    }
                    $node->fid = $person->id;
                    if($node->mid){
                        $person->pids = $node->mid;
                    }
                }
                if($this->is_mother){
                    
                    $cur_m = Family::find($node->mid);
                    if($cur_m){
                        if($cur_m->relation == "Great Grand Mother" || $cur_m->relation == "Grand Mother"){
                            $person->fid = $cur_m->id;
                        }
                    }
                    $node->mid = $person->id;
                    if($node->fid){
                        $person->pids = $node->fid;
                    }
                }
                $person->save();
                $node->save();
            }
            $this->clearFields(); 
            $this->checkFamily();
            $this->dispatchBrowserEvent('data-updated', ['message' => 'Member added to family tree successfully.']);
        }
    }

    public function updatedRelation(){
        if($this->relation == "Great Grand Father" || $this->relation == "Great Grand Mother"){
            if($this->relation == "Great Grand Father"){
                $ch_arr = $this->getGrandParents($this->cur_person, 'Father');
                $this->is_father = true;
                $this->is_mother = false;
                $this->is_spouse = false;
                $this->is_child = false;
            }
            else{
                $ch_arr = $this->getGrandParents($this->cur_person, 'Mother');
                $this->is_father = false;
                $this->is_mother = true;
                $this->is_spouse = false;
                $this->is_child = false;
            }
            if(count($ch_arr) > 0){
                $this->children_list = Family::whereIn('id', $ch_arr)->get();
                $this->show_children_list = true;
            }
        }
        if($this->relation == "Grand Father" || $this->relation == "Grand Mother"){
            $type = null;
            if($this->relation == "Grand Father"){
                $this->is_father = true;
                $this->is_mother = false;
                $this->is_spouse = false;
                $this->is_child = false;
                $type = "Father";
            }
            else{
                $this->is_father = false;
                $this->is_mother = true;
                $this->is_spouse = false;
                $this->is_child = false;
                $type = "Mother";
            }
            $parents_arr = $this->getParents($this->cur_person, $type);
            if(count($parents_arr) > 0){
                $parents_arr = array_unique($parents_arr);
                $this->children_list = Family::whereIn('id', $parents_arr)->get();
                $this->show_children_list = true;

            }
        }

        $fs = ['Father', 'Uncle', 'Step Father', 'Father In Law'];
        if(in_array($this->relation, $fs)){
            $this->is_father = true;
            $this->is_mother = false;
            $this->is_spouse = false;
            $this->is_child = false;
        }
        $ms = ['Mother', 'Aunt', 'Step Mother', 'Mother In Law'];
        if(in_array($this->relation, $ms)){
            $this->is_father = false;
            $this->is_mother = true;
            $this->is_spouse = false;
            $this->is_child = false;
        }
        if($this->relation == "Uncle" || $this->relation == "Aunt"){
            if($this->relation == "Uncle"){
                $type = "Brother";
            }
            if($this->relation == "Aunt"){
                $type = "Sister";
            }
            $parents_arr = $this->getParents($this->cur_person, $type);
            if(count($parents_arr) > 0){
                $parents_arr = array_unique($parents_arr);
                $this->children_list = Family::whereIn('id', $parents_arr)->get();
                $this->show_children_list = true;

            }
        }
        $arr = ['Brother', 'Step Brother', 'Sister', 'Step Sister'];
        if(in_array($this->relation, $arr)){
            $type = "Child";
            $parents_arr = $this->getParents($this->cur_person, $type);
            if(count($parents_arr) > 0){
                $parents_arr = array_unique($parents_arr);
                $this->children_list = Family::whereIn('id', $parents_arr)->get();
                $this->show_parent_list = true;
            }
        }
        $parents_inlaw = ['Father In Law', 'Mother In Law'];
        if(in_array($this->relation, $parents_inlaw)){
            if($this->relation == "Father In Law"){
                $this->is_father = true;
                $this->is_mother = false;
                $this->is_spouse = false;
                $this->is_child = false;
                $this->ch_label = "Father To:";
            }
            if($this->relation == "Mother In Law"){
                $this->is_father = false;
                $this->is_mother = true;
                $this->is_spouse = false;
                $this->is_child = false;
                $this->ch_label = "Mother To:";
            }
            $sps = $this->getSpouses($this->cur_person);
            $this->children_list = Family::whereIn('id', $sps)->get();
            $this->show_children_list = true;
        }
        $siblings_in_law = ['Brother In Law', 'Sister In Law'];
        if(in_array($this->relation, $siblings_in_law)){
            if($this->relation == "Brother In Law"){
                $this->ch_label = "Brother To:";
            }
            if($this->relation == "Sister In Law"){
                $this->ch_label = "Sister To:";
            }
            $sps = $this->getSpouses($this->cur_person);
            $siblings = $this->getSiblings($this->cur_person);
            $this->children_spouses = Family::whereIn('id', $siblings)->get();

            $this->children_list = Family::whereIn('id', $sps)->get();
            $this->show_children_list = true;
            $this->show_child_spouse = true;
        }

        if($this->relation == "Cousin"){
            $prent_siblings = $this->getParentSiblings($this->cur_person);  
            $this->children_list = $prent_siblings;
            $this->show_parent_list = true;  
        }

        if($this->relation == "Son" || $this->relation == "Daughter"){
            $sps = Family::where('pids', $this->cur_person->id)->whereIn('relation', ['Spouse','Partner', 'Wife','Husband',])->get();
            $arr = [];
            foreach($sps AS $sp){
                $arr[] = $sp->id;
            }
            $arr[] = $this->cur_person->id;
            $this->children_list = Family::whereIn('id', $arr)->get();
            $this->show_parent_list = true;
        }
        if($this->relation == 'Niece' || $this->relation == 'Nephew'){
            $arr = [];
            $sps = Family::where('pids', $this->cur_person->id)->whereIn('relation', ['Spouse','Partner', 'Wife','Husband',])->get();
            foreach($sps AS $sp){
                $sp_gen = $this->getSiblings($sp);
                $arr = array_merge($sp_gen, $arr);
            }
            $my_gen = $this->getSiblings($this->cur_person);
            $arr = array_merge($my_gen, $arr);
            
            $this->children_list = Family::whereIn('id', $arr)->get();
            $this->show_parent_list = true;
        }
        $child_in_laws = ["Son In Law", "Daughter In Law", "Son's Partner", "Daughter's Partner"];
        if(in_array($this->relation, $child_in_laws)){
            $this->children_spouses = Family::where('fid', $this->cur_person->id)->orWhere('mid', $this->cur_person->id)->get();
            $this->show_child_spouse = true;
        }

        if($this->relation == 'Grand Son' || $this->relation == 'Grand Daughter'){
            $this->grand_children_parents = Family::where('fid', $this->cur_person->id)->orWhere('mid', $this->cur_person->id)->get();
            $this->show_grand_children_parents = true;
        }
    }
    public $show_grand_children_parents = false;
    public $grand_children_parents;
    public $cp_id;

    public function getSiblings($person){
        $f = $person->fid;
        $m = $person->mid;
        $siblings = Family::where('fid', $f)->orwhere('mid', $m)->get();
        $arr = [];
        foreach($siblings AS $sb){
            $arr[] = $sb->id;
        }
        return $arr;
    }

    public function getParentSiblings($person){
        $ff = null;
        $fm = null;
        $mf = null;
        $mm = null;
        if($person->fid){
            $father = Family::find($person->fid);
            $ff = $father->fid;
            $fm = $father->mid;
        }
        if($person->mid){
            $mother = Family::find($person->mid);
            $mf = $mother->fid;
            $mm = $mother->mid;
        }
        $siblings = Family::whereIn('fid', [$ff, $mf])->orWhereIn('mid', [$fm, $mm])->get();
        return $siblings;
    }

    public function getParents($person, $type){
        $arr = [];
        $f = Family::find($person->fid);
        if($f){
            $arr[] = $f->id;
            $sps = $this->getSpouses($f);
            $arr = array_merge($arr,$sps);
        }
        $m = Family::find($person->mid);
        if($m){
            $arr[] = $m->id;
            $sps = $sps = $this->getSpouses($m);
            $arr = array_merge($arr,$sps);
        }
        $this->ch_label = "Is ".$type." to:";
        return $arr;
    }

    public function getSpouses($person){
        $arr = [];
        $spouses = Family::where('pids', $person->id)->get();
        foreach($spouses AS $sp){
            $arr[] = $sp->id;
        }
        return $arr;
    }

    public function getGrandParents($person, $type){
        $arr = [];
        $f = Family::find($person->fid);
        $m = Family::find($person->mid);
        if($f){
            if($f->fid){
                $arr[] = $f->fid;
            }
            if($f->mid){
                $arr[] = $f->mid;
            }
            $this->ch_label = "Is ".$type." To:";
        }
        if($m){
            if($m->fid){
                $arr[] = $m->fid;
            }
            if($m->mid){
                $arr[] = $m->mid;
            }
            $this->ch_label = "Is ".$type." To:";
        }
        if(count($arr) == 0){
            if($f){
                $arr[] = $f->id;
            }
            if($m){
                $arr[] = $m->id;
            }
            $this->ch_label = "Is Grand ".$type." To:";
        }
        return $arr;
    }

    public function checkFamily(){
        $me = Family::where('user_id', Auth::user()->id)->first();
        //if(!$me){
            //if(Auth::user()->id_number){
            //    $me = Family::where('id_number', Auth::user()->id_number)->first();
            //}
            //if(!$me){
            //    $me = Family::where('email', Auth::user()->email)->first();
            //}
        //}
        if(!$me){
            $me = Family::create([
                'user_id' => Auth::user()->id,
                'name' => Auth::user()->name,
                'surname' => Auth::user()->surname,
                'dob' => Auth::user()->date_of_birth,
                'id_number' => Auth::user()->id_number,
                'email' => Auth::user()->email,
                'relation' => "Me",
                'show_on_tree' => 1,
                'img' => Auth::user()->profile_pic,
            ]);
        }

        $this->cur_id = $me->id;
        $this->cur_person = $me;

        $tree_gen = new FamilyTreeGen();
        $this->tree = json_encode($tree_gen->get_tree($me->id));
        //dd($this->tree);
    }

    public function hideForm(){
        $this->clearFields();
        $this->show_form = false;
    }

    public function updatedCurRel(){
        if($this->cur_rel == "fid"){
            $this->spouse_title = "Mother is";
        }
        if($this->cur_rel == "mid"){
            $this->spouse_title = "Father is";
        }
        $this->spouse_list = Family::where('pids', $this->cur_id)->get();
        if($this->spouse_list->count() > 0){
            $this->show_other_parent = true;
        }
    }

    /*
    public function SaveFamilyTreeMember(){
        $this->validate([
            'name' => 'required', 
            'surname' => 'required', 
            'relation' => 'required', 
            'dob' => 'required', 
            'id_number' => 'required', 
        ]);

        $person = Family::where('user_id', Auth::user()->id)->where('id_number', $this->id_number)->orWhere('email', $this->id_number)->first();
        if(!$person){
            $person = new Family();
        }

        $is_email = false;
        if(filter_var($this->id_number, FILTER_VALIDATE_EMAIL)) {
            $is_email = true;
        }
        
        $person->user_id = Auth::user()->id;
        $person->name = $this->name;
        $person->surname = $this->surname;
        $person->dob = $this->dob;
        $person->dod = $this->dod;
        if(!$is_email){
            $person->id_number = $this->id_number;
        }
        if($is_email){ 
            $person->email = $this->id_number;
        }
        $person->relation = $this->relation;
        $person->show_on_tree = $this->show_on_tree;
        $person->color = $this->color;

        $path = null;
        if($this->photo){
            $path = $this->photo->storePublicly('photos', 'public');
        }
        $person->img = $path;
        $person->save();

        $node = Family::find($this->cur_id);
        if($this->relation == "Father"){
            $node->fid = $person->id;
            if($node->mid){
                $person->pids = $node->mid;
            }
        }
        if($this->relation == "Mother"){
            $node->mid = $person->id;
            if($node->fid){
                $person->pids = $node->fid;
            }
        }
        if($this->relation == "Spouse"){
            if(!$node->pids){
                $node->pids = $person->id;
            }
            $person->pids = $node->id;
        }
        if($this->relation == "Child"){
            if($this->cur_rel == "fid"){
                $person->fid = $node->id;
                $person->mid = $this->other_parent;
            }
            if($this->cur_rel == "mid"){
                $person->mid = $node->id;
                $person->fid = $this->other_parent;
            }
        }
        $node->save();
        $person->save();

        $this->clearFields(); 
        $this->checkFamily();
        $this->dispatchBrowserEvent('data-updated', ['message' => 'Member added to family tree successfully.']); 
    }
    */

    public function clearFields(){
        $this->show_form = false;
        $this->name = null; 
        $this->surname = null;
        $this->relation = null;
        $this->dob = null;
        $this->id_number = null;
        /**$this->gender = null;**/
        $this->living = null;
        $this->show_on_tree = null;
        $this->dod = null;
        $this->color = null;
        $this->photo = null;
        $this->cur_rel = null;

        //$this->cur_id = null; 
        //$this->cur_person = null;
        $this->show_other_parent = false;
        $this->spouse_title = null; 
        $this->spouse_list = null;
    }
    
     public function initModalSeen(){
        $pop_up = PopUp::where('user_id', Auth::user()->id)->first();
        if($pop_up){
            $pop_up->tree = 1;
            $pop_up->save();
        }
        $this->dispatchBrowserEvent('close-modals');
    }

    public function render(){
        return view('livewire.account.family-tree.family-tree');
    }
}
