<?php

namespace App\Http\Livewire\Account\MyNetwork;

use Livewire\Component;

use Auth;

class MyNetwork extends Component
{
    public $contact_id;

    public function mount(){
        $this->contact_id = Auth::user()->id;
    }

    public function render(){
        return view('livewire.account.my-network.my-network');
    }
}
