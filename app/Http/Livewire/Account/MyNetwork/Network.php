<?php

namespace App\Http\Livewire\Account\MyNetwork;

use Illuminate\Support\Facades\Session;
use Livewire\Component;

use App\Models\Share;

class Network extends Component
{
    protected $listeners = ['loadTabContent' => 'handleLoadTabContent'];
    public $user_id;

    public function handleLoadTabContent($tab)
    {
        $this->activeTab = $tab;
        session()->put('sharesActiveTab', $tab);
        return redirect()->to('share-profile/form');
    }

    public function mount($id)
    {
        $this->user_id = $id;
    }

    public function viewShare($id)
    {
        $this->emit('viewShareActivated', $id);
    }

    public function setColor($id, $color)
    {
        $sh = Share::find($id);
        $sh->color = $color;
        $sh->save();
    }

    public function removeColor($id)
    {
        $sh = Share::find($id);
        $sh->color = null;
        $sh->save();
    }

    public function reOrderItems($orderedItems)
    {
        foreach ($orderedItems as $order) {
            $num = $order['order'];
            $id = $order['value'];
            $sh = Share::find($id);
            if ($sh) {
                $sh->order_num = $num;
                $sh->save();
            }
        }
    }

    public function render()
    {
        $family = Share::where('user_id', $this->user_id)
            ->whereNotNull('family')
            ->where('family', '<>', '')
            ->orderBy('order_num', 'ASC')
            ->get();
        $friends = Share::where('user_id', $this->user_id)
            ->whereNotNull('friends')
            ->where('friends', '<>', '')
            ->orderBy('order_num', 'ASC')
            ->get();
        $professional = Share::where('user_id', $this->user_id)
            ->whereNotNull('professional')
            ->where('professional', '<>', '')
            ->orderBy('order_num', 'ASC')
            ->get();

        return view('livewire.account.my-network.network', [
            'family' => $family,
            'friends' => $friends,
            'professional' => $professional,
        ]);
    }
}
