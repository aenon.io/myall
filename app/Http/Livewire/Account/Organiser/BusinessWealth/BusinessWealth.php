<?php

namespace App\Http\Livewire\Account\Organiser\BusinessWealth;

use Auth;
use Livewire\Component;
use Livewire\WithFileUploads;
use App\Models\BusinessWealth AS BusinessWealthModel;
use App\Models\BusinessWealthTrustee;
use App\Models\BusinessWealthMember;
use App\Models\BusinessWealthDebtor;
use App\Models\Possession;
use App\Models\BusinessWealthCreditor;

class BusinessWealth extends Component
{
    use WithFileUploads;
    
    public $business_types = [], $masters_offices = [], $trust_types = [], $possessions_arr = [];
    public $business_type, $business_name, $registration_number, $vat_registered, $vat_number;
    public $masters_office, $trust_type, $specific_trust_type, $possessions;
    public $buy_sell_agreement, $value_min, $value_max, $equipment, $outstanding_debtors, $goodwill;
    public $net_value, $credit, $debt;
    public $auditor_name, $auditor_surname, $auditor_tel, $auditor_email, $auditor_address;
    public $trustees = [], $members = [], $debtors = [], $poses = [];
    public $company_url;

    public $founding_statement, $amended_founding_statement, $members_resolution; 
    public $certificate_of_registration, $sars_issued_document, $letter_from_auditors, $directors_resolution;
    public $trust_deed, $letter_of_authority, $resolution_by_trustee;
    
    public $founding_statement_file, $amended_founding_statement_file, $members_resolution_file;
    public $certificate_of_registration_file, $sars_issued_document_file, $letter_from_auditors_file;
    public $directors_resolution_file, $trust_deed_file, $letter_of_authority_file, $resolution_by_trustee_file;

    public $cur_view, $cur_id;
    
    public $percentage_count;

    public $creditors = [];
    
    public $view;

    public function mount(){
        $this->cur_view = "list";

        $this->initData();
        $this->getData();
        $this->addDebtor();
        $this->addCreditor();
        $this->addMember();
        $this->addTrustee();
        $this->addPossession();

        //$this->business_type = "Private Company";
        //$this->cur_view = "form";
    }
    
    public function hydrate(){
        $this->dispatchBrowserEvent('reload-js');
    }
    
    public function updatedMembers(){
        $percentage_count = 0;
        foreach($this->members AS $mem){
            $percentage_count += (int)$mem['stake_percentage'];
        }
        if($percentage_count > 100){
            $this->dispatchBrowserEvent('percentage-overload', ['message' => 'Cannot exceed 100% - make sure percentage stake amounts to 100%.']);
        }
    }

    public function showForm($view = null, $id = null){
        $this->cur_view = "form";
        if($id){
            $this->cur_id = $id;
            $this->getData();
        }else{
            $this->cur_id = null;
            $this->getData();
        }
        
        if(isset($view)){
            $this->view = $view;
        }
    }

    public function saveBuysinessWealth(){
        $this->validate([
            'business_type' => 'required',
            'business_name' => 'required',
            'founding_statement' => 'nullable|file|mimes:pdf,jpeg,png|max:2048',
            'amended_founding_statement' => 'nullable|file|mimes:pdf,jpeg,png|max:2048',
            'members_resolution' => 'nullable|file|mimes:pdf,jpeg,png|max:2048',
            'certificate_of_registration' => 'nullable|file|mimes:pdf,jpeg,pmg|max:2048',
            'sars_issued_document' => 'nullable|file|mimes:pdf,jpeg,png|max:2048',
            'letter_from_auditors' => 'nullable|file|mimes:pdf,jpeg,png|max:2048',
            'directors_resolution' => 'nullable|file|mimes:pdf,jpeg,png|max:2048',
            'trust_deed' => 'nullable|file|mimes:pdf,jpeg,png|max:2048',
            'letter_of_authority' => 'nullable|file|mimes:pdf,jpeg,png|max:2048',
            'resolution_by_trustee' => 'nullable|file|mimes:pdf,jpeg,png|max:2048',
        ]);
        
        if($this->business_type != 'Trust' && $this->business_type != "Sole Ownership"){
            $percentage_count = 0;
            foreach($this->members AS $mem){
                $percentage_count += (int)$mem['stake_percentage'];
            }
            /*
            if($percentage_count > 100){
                $this->dispatchBrowserEvent('percentage-overload', ['message' => 'Cannot exceed 100% - make sure percentage stake amounts to 100%.']);
                return;
            }
            elseif($percentage_count < 100){
                $this->dispatchBrowserEvent('percentage-overload', ['message' => 'Cannot be lower than 100% - make sure percentage stake amounts to 100%.']);
                return;
            }
            */
        }
        
        if($this->cur_id){
            $bus_welth = BusinessWealthModel::where('user_id', Auth::user()->id)->where('id', $this->cur_id)->first();    
        }
        else{
            $bus_welth = new BusinessWealthModel();
        }
        $bus_welth->user_id = Auth::user()->id;
        $bus_welth->business_type = $this->business_type;
        $bus_welth->business_name = $this->business_name; 
        $bus_welth->registration_number = $this->registration_number; 
        $bus_welth->company_url = $this->company_url;
        $bus_welth->vat_registered = $this->vat_registered; 
        $bus_welth->vat_number = $this->vat_number;
        $bus_welth->masters_office = $this->masters_office; 
        $bus_welth->trust_type = $this->trust_type; 
        $bus_welth->specific_trust_type = $this->specific_trust_type; 
        // $bus_welth->possessions = $this->possessions;
        $bus_welth->buy_sell_agreement = $this->buy_sell_agreement; 
        $bus_welth->value_min = str_replace(' ', '', str_replace(',', '',$this->value_min)); 
        $bus_welth->value_max = str_replace(' ', '', str_replace(',', '',$this->value_max)); 
        $bus_welth->equipment = $this->equipment; 
        $bus_welth->outstanding_debtors = $this->outstanding_debtors; 
        $bus_welth->goodwill = $this->goodwill;
        $bus_welth->net_value = str_replace(' ', '', str_replace(',', '',$this->net_value)); 
        $bus_welth->credit = str_replace(' ', '', str_replace(',', '',$this->credit)); 
        $bus_welth->debt = str_replace(' ', '', str_replace(',', '',$this->debt));
        $bus_welth->auditor_name = $this->auditor_name; 
        $bus_welth->auditor_surname = $this->auditor_surname; 
        $bus_welth->auditor_tel = $this->auditor_tel; 
        $bus_welth->auditor_email = $this->auditor_email; 
        $bus_welth->auditor_address = $this->auditor_address;

        $fs_path = null;
        if($this->founding_statement){
            $fs_path = $this->founding_statement->storePublicly('document', 'public');
        }else{
            $fs_path = $bus_welth->founding_statement;
        }

        $afs_path = null;
        if($this->amended_founding_statement){
            $afs_path = $this->amended_founding_statement->storePublicly('document', 'public');
        }else{
             $afs_path = $bus_welth->amended_founding_statement;
        }

        $mr_path = null;
        if($this->members_resolution){
            $mr_path = $this->members_resolution->storePublicly('document', 'public');
        }else{
            $mr_path = $bus_welth->members_resolution;
        }

        $cor_path = null;
        if($this->certificate_of_registration){
            $cor_path = $this->certificate_of_registration->storePublicly('document', 'public');
        }else{
            $cor_path = $bus_welth->certificate_of_registration;
        }

        $sid_path = null;
        if($this->sars_issued_document){
            $sid_path = $this->sars_issued_document->storePublicly('document', 'public');
        }else{
            $sid_path = $bus_welth->sars_issued_document;
        }

        $lfa_path = null;
        if($this->letter_from_auditors){
            $lfa_path = $this->letter_from_auditors->storePublicly('document', 'public');
        }else{
            $lfa_path = $bus_welth->letter_from_auditors;
        }

        $dr_path = null;
        if($this->directors_resolution){
            $dr_path = $this->directors_resolution->storePublicly('document', 'public');
        }else{
            $dr_path = $bus_welth->directors_resolution;
        }

        $td_path = null;
        if($this->trust_deed){
            $td_path = $this->trust_deed->storePublicly('document', 'public');
        }else{
            $td_path = $bus_welth->trust_deed;
        }

        $loa_path = null;
        if($this->letter_of_authority){
            $loa_path = $this->letter_of_authority->storePublicly('document', 'public');
        }else{
            $loa_path = $bus_welth->letter_of_authority;
        }

        $rbt_path = null;
        if($this->resolution_by_trustee){
            $rbt_path = $this->resolution_by_trustee->storePublicly('document', 'public');
        }else{
            $rbt_path = $bus_welth->resolution_by_trustee;
        }

        $bus_welth->founding_statement = $fs_path;
        $bus_welth->amended_founding_statement = $afs_path;
        $bus_welth->members_resolution = $mr_path;
        $bus_welth->certificate_of_registration = $cor_path;
        $bus_welth->sars_issued_document = $sid_path;
        $bus_welth->letter_from_auditors = $lfa_path;
        $bus_welth->directors_resolution = $dr_path;
        $bus_welth->trust_deed = $td_path;
        $bus_welth->letter_of_authority = $loa_path;
        $bus_welth->resolution_by_trustee = $rbt_path;
        $bus_welth->save();
    
        if($this->trustees){
            foreach($this->trustees AS $trustee){
                if($trustee['trustee_first_name'] && $trustee['trustee_surname'] && $trustee['trustee_contact']){
                    if(isset($trustee['id'])){
                        $trst = BusinessWealthTrustee::find($trustee['id']);
                    }
                    else{
                        $trst = new BusinessWealthTrustee();
                    }
                    $trst->user_id = Auth::user()->id;
                    $trst->business_wealth_id = $bus_welth->id;
                    $trst->trustee_first_name = $trustee['trustee_first_name'];
                    $trst->trustee_surname = $trustee['trustee_surname'];
                    $trst->trustee_contact = $trustee['trustee_contact'];
                    $trst->save();
                }
            }
        }
        
        //if($this->business_type == 'Trust'){
             if($this->poses){
                foreach($this->poses AS $pos){
                    if(isset($pos['id'])){
                        $p = Possession::find($pos['id']);
                    }
                    else{
                        $p = new Possession();
                    }
                    $p->user_id = Auth::user()->id;
                    $p->business_wealth_id = $bus_welth->id;
                    $p->possessions = $pos['possessions'];
                    if(isset($pos['specify_possession'])){
                        $p->specify_possession = $pos['specify_possession'];
                    }
                    $p->value = $pos['value'];
                    $p->save();
                }
            }  
        //}

        if($this->members){
            foreach($this->members AS $member){

            if($member['member_name'] && $member['member_surname']){
                if(isset($member['id'])){
                    $mb = BusinessWealthMember::find($member['id']);
                }
                else{
                    $mb = new BusinessWealthMember();
                }
                
                $mb->user_id = Auth::user()->id;
                $mb->business_wealth_id = $bus_welth->id;
                $mb->member_name = $member['member_name'];
                $mb->member_surname = $member['member_surname'];
                $mb->stake_percentage = $member['stake_percentage'];
                $mb->save();
            }
        }
        }
        
        if($this->debtors){
            foreach($this->debtors AS $debtor){
                if($debtor['debt_to']){
                    if(isset($debtor['id'])){
                        $dt = BusinessWealthDebtor::find($debtor['id']);
                    }
                    else{
                        $dt = new BusinessWealthDebtor();
                    }

                    $dt->user_id = Auth::user()->id;
                    $dt->business_wealth_id = $bus_welth->id;
                    $dt->debt_to = $debtor['debt_to'];
                    $dt->debt_value = $debtor['debt_value'];
                    $dt->debt_insured = $debtor['debt_insured'];
                    $dt->save();
                }
            }
        }

        foreach($this->creditors AS $creditor){
            if($creditor['credit_to']){
                if(isset($creditor['id'])){
                    $credit = BusinessWealthCreditor::find($creditor['id']);
                }
                else{
                    $credit = new BusinessWealthCreditor();
                }

                $credit->user_id = Auth::user()->id;
                $credit->business_wealth_id = $bus_welth->id;
                $credit->credit_to = $creditor['credit_to'];
                $credit->credit_value = $creditor['credit_value'];
                $credit->credit_insured = $creditor['credit_insured'];
                $credit->save();
            }
        }
        
        session()->flash('message', 'Successfully updated.');
        $this->clearFields();
    }

    public function clearFields(){
        $this->cur_view = "list";
        $this->cur_id = null;
        $this->business_type = null; 
        $this->business_name = null;
        $this->registration_number = null;
        $this->company_url = null;
        $this->vat_registered = null;
        $this->vat_number = null;
        $this->masters_office = null;
        $this->trust_type = null;
        $this->specific_trust_type = null;
        $this->buy_sell_agreement = null; 
        $this->value_min = null;
        $this->value_max = null;
        $this->equipment = null;
        $this->outstanding_debtors = null;
        $this->goodwill = null;
        $this->net_value = null;
        $this->credit = null;
        $this->debt = null;
        $this->auditor_name = null;
        $this->auditor_surname = null;
        $this->auditor_tel = null;
        $this->auditor_email = null;
        $this->auditor_address = null;
        $this->founding_statement = null;
        $this->amended_founding_statement = null;
        $this->members_resolution = null;
        $this->certificate_of_registration = null;
        $this->sars_issued_document = null;
        $this->letter_from_auditors = null;
        $this->directors_resolution = null;
        $this->trust_deed = null;
        $this->letter_of_authority = null;
        $this->resolution_by_trustee = null;
        $this->view = null;
        
        $this->poses = [];
        $this->debtors = [];
        $this->creditors = [];
        $this->trustees = [];
        $this->members = [];
    }

    public function getData(){
        if($this->cur_id){
            $cmp = BusinessWealthModel::find($this->cur_id);
            if($cmp){
                $this->business_type = $cmp->business_type; 
                $this->business_name = $cmp->business_name;
                $this->registration_number = $cmp->registration_number;
                $this->company_url = $cmp->company_url;
                $this->vat_registered = $cmp->vat_registered;
                $this->vat_number = $cmp->vat_number;
                $this->masters_office = $cmp->masters_office;
                $this->trust_type = $cmp->trust_type;
                $this->specific_trust_type = $cmp->specific_trust_type;
                // $this->possessions = $cmp->possessions;
                $this->buy_sell_agreement = $cmp->buy_sell_agreement; 
                $this->value_min = $cmp->value_min;
                $this->value_max = $cmp->value_max;
                $this->equipment = $cmp->equipment;
                $this->outstanding_debtors = $cmp->outstanding_debtors;
                $this->goodwill = $cmp->goodwill;
                $this->net_value = $cmp->net_value;
                $this->credit = $cmp->credit;
                $this->debt = $cmp->debt;
                $this->auditor_name = $cmp->auditor_name;
                $this->auditor_surname = $cmp->auditor_surname;
                $this->auditor_tel = $cmp->auditor_tel;
                $this->auditor_email = $cmp->auditor_email;
                $this->auditor_address = $cmp->auditor_address;
                
                $this->founding_statement_file = $cmp->founding_statement;
                $this->amended_founding_statement_file = $cmp->amended_founding_statement;
                $this->members_resolution_file = $cmp->members_resolution;
                $this->certificate_of_registration_file = $cmp->certificate_of_registration;
                $this->sars_issued_document_file = $cmp->sars_issued_document;
                $this->letter_from_auditors_file = $cmp->letter_from_auditors;
                $this->directors_resolution_file = $cmp->directors_resolution;
                $this->trust_deed_file = $cmp->trust_deed;
                $this->letter_of_authority_file = $cmp->letter_of_authority;
                $this->resolution_by_trustee_file = $cmp->resolution_by_trustee;

                if($cmp->debtors->count() > 0){
                    $this->debtors = [];
                    foreach($cmp->debtors AS $dt){
                        $arr = [
                            'id' => $dt->id,
                            'debt_to' => $dt->debt_to,
                            'debt_value' => $dt->debt_value,
                            'debt_insured' => $dt->debt_insured,
                        ];
                        $this->debtors[] = $arr;
                    }
                }
                if($cmp->creditors->count() > 0){
                    $this->creditors = [];
                    foreach($cmp->creditors AS $credit){
                        $arr = [
                            "id" => $credit->id,
                            "credit_to" => $credit->credit_to,
                            "credit_value" => $credit->credit_value,
                            "credit_insured" => $credit->credit_insured
                        ];
                        $this->creditors[] = $arr;
                    }
                }
                
                if($cmp->trustees->count() > 0){
                    $this->trustees = [];
                    foreach($cmp->trustees AS $trst){
                        $arr = [
                            'id' => $trst->id,
                            'trustee_first_name' => $trst->trustee_first_name,
                            'trustee_surname' => $trst->trustee_surname,
                            'trustee_contact' => $trst->trustee_contact
                        ];
                        $this->trustees[] = $arr;
                    }
                }
                
                if($cmp->shareholders->count() > 0){
                    $this->members = [];
                    foreach($cmp->shareholders AS $shhldr){
                        $arr = [
                            'id' => $shhldr->id,
                            'member_name' => $shhldr->member_name,
                            'member_surname' => $shhldr->member_surname,
                            'stake_percentage' => $shhldr->stake_percentage
                        ];
                        $this->members[] = $arr;
                    }
                }
                
                if($cmp->my_possessions->count() > 0){
                    $this->poses = [];
                    foreach($cmp->my_possessions as $ps){
                        $arr = [
                            'id' => $ps->id,
                            'possessions' => $ps->possessions, 
                            'specify_possession' => $ps->specify_possession,
                            'value' => $ps->value,
                        ];
                        $this->poses[] = $arr;
                    }
                }
            }
        }
    }

    public function addCreditor(){
        $arr = [
            "credit_to" => "",
            "credit_value" => "",
            "credit_insured" => ""
        ];
        $this->creditors[] = $arr;
    }

    public function addDebtor(){
        $arr = [
            'debt_to' => '',
            'debt_value' => '',
            'debt_insured' => '',
        ];
        $this->debtors[] = $arr;
    }

    public function addMember(){
        $arr = [
            'member_name' => '',
            'member_surname' => '',
            'stake_percentage' => ''
        ];
        $this->members[] = $arr;
    }

    public function addTrustee(){
        $arr = [
            'trustee_first_name' => '',
            'trustee_surname' => '',
            'trustee_contact' => ''
        ];
        $this->trustees[] = $arr;
    }
    
    public function addPossession(){
        $arr = [
            'possessions' => '',
            'specify_possession' => '',
            'value' => '',
        ];
        $this->poses[] = $arr;
    }

    public function initData(){
        $this->business_types = [
            'Sole Ownership',
            'Closed Corporation',
            'Private Company',
            'Trust',
            'Partnership'
        ];
        $this->masters_offices = [
            'Bloemfontein',   
            'Cape Town',   
            'Grahamstown (Makhanda)',
            'Kimberley' , 
            'Mmabatho/Mafikeng',
            'Nelspruit', 
            'Pietermaritzburg', 
            'Pretoria', 
            'Mthatha (Umtata)', 
            'Bisho', 
            'Thohoyandou',  
            'Johannesburg',
            'Polokwane', 
            'Durban',
            'Port Elizabeth (Gqeberha)',
        ];
        $this->trust_types = [
            'Business',
            'Family',
            'Overseas',
            'Other',
        ];
        $this->possessions_arr = [
            'House',
            'Farm',
            'Car',
            'Investment',
            'Bank Account',
            'Equipment',
            'Stock',
            'Other'
        ];
    }
    
    public function removeBusiness($id){
        BusinessWealthTrustee::where('business_wealth_id', $id)->delete();
        BusinessWealthMember::where('business_wealth_id', $id)->delete();
        BusinessWealthDebtor::where('business_wealth_id', $id)->delete();
        BusinessWealthModel::where('id', $id)->delete();
        $this->clearFields();
    }
    
    public function removeDebtor($id){
        BusinessWealthDebtor::where('id', $id)->delete();
        $this->getData();
    }
    
    public function removePossession($id){
        Possession::where('id', $id)->delete();
        $this->getData();
    }
    
    public function removeMember($id){
        BusinessWealthMember::where('id', $id)->delete();
        $this->getData();
    }
    
    public function removeTrustee($id){
        BusinessWealthTrustee::where('id', $id)->delete();
        $this->getData();
    }

    public function removeDocument($id, $type){
        $doc = BusinessWealthModel::find($id);

        if($doc->$type){
            $doc->$type = null;
            $doc->save();
        }
        
        $this->getData();
    }
    
    public function closeWindow(){
         $this->clearFields();
    }

    public function render(){
        $cmps = null;
        if($this->cur_view == "list"){
            $cmps = BusinessWealthModel::where('user_id', Auth::user()->id)->paginate(12);
        }
        return view('livewire.account.organiser.business-wealth.business-wealth', [
            'cmps' => $cmps
        ]);
    }
}
