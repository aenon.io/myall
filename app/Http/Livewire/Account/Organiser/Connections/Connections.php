<?php

namespace App\Http\Livewire\Account\Organiser\Connections;

use Livewire\Component;
use Auth;
use App\Models\Contact;

class Connections extends Component
{
    //public $reltionship, $name, $surname, $email, $contact_number, $company, $company_info;
    public $contacts = [];
    public $relations;

    public function mount(){
        $this->loadInitValues();
        
        $this->relations = [
            'Great Grand Father',
            'Great Grand Mother',

            'Grand Father', 
            'Grand Mother',

            'Father', 
            'Uncle',
            'Step Father',
            'Father In Law',

            'Mother',
            'Aunt',
            'Step Mother',
            'Mother In Law',
            
            'Brother', 
            'Step Brother',
            'Brother In Law',
            
            'Sister',
            'Step Sister',
            'Sister In Law',

            'Cousin',
            

            'Spouse',
            'Partner', 
            'Wife',
            'Husband',

            'Son', 
            'Daughter',
            'Son In Law',
            'Daughter In Law',
            "Son's Partner",
            "Daughter's Partner",
            'Niece', 
            'Nephew',
            'Step Son',
            'Step Daughter',

            'Grand Son', 
            'Grand Daughter',
            
            'Great Grand Son',
            'Great Grand Daughter',
            
            'Other'
        ];
        
        sort($this->relations);
    }

    public function loadInitValues(){
        $this->contacts = [];
        $contact_types = ['family', 'personal', 'business','doctor','emergency', 'accountant', 'banker', 'lawyer', 'other'];
        $contacts = Contact::where('user_id', Auth::user()->id)->whereIn('contact_type', $contact_types)->get();
        if($contacts->count()> 0){
            foreach($contacts AS $cnt){
                $arr = [
                    'id' => $cnt->id,
                    'contact_type' => $cnt->contact_type,
                    'relation' => $cnt->relation,
                    'specify_relation' => $cnt->specify_relation,
                    'name' => $cnt->name,
                    'surname' => $cnt->surname,
                    'email' => $cnt->email,
                    'contact_number' => $cnt->contact_number,
                    'company_info' => $cnt->company_info,
                    'aa_company_name' => $cnt->aa_company_name,
                    'aa_contact_details' => $cnt->aa_contact_details,
                    'aa_contact_name' => $cnt->aa_contact_name,
                    'aa_contact_surname' => $cnt->aa_contact_surname,
                    'aa_contact_email' => $cnt->aa_contact_email,
                    'aa_contact_number' => $cnt->aa_contact_number
                ];
                $this->contacts[] = $arr;
            }
        }
    }

    public function addContact($type){
        $arr = [
            'contact_type' => $type,
            'relation' => '',
            'specify_relation' => '',
            'name' => '',
            'surname' => '',
            'email' => '',
            'contact_number' => '',
            'company_info' => '',
            'aa_company_name' => '',
            'aa_contact_details' => '',
            'aa_contact_name' => '',
            'aa_contact_surname' => '',
            'aa_contact_email' => '',
            'aa_contact_number' => ''
        ];
        $this->contacts[] = $arr;
    }
    
    public function remove($id){
        $cnt = Contact::find($id);
        $cnt->delete();
        $this->loadInitValues();
    }
    
    public function SaveContacts(){
        foreach($this->contacts AS $cnt){
            if($cnt['name'] && $cnt['surname']){
                if(isset($cnt['id'])){
                    $cnt_item = Contact::find($cnt['id']);
                }
                else{
                    $cnt_item = new Contact();
                }
                $cnt_item->user_id = Auth::user()->id;
                $cnt_item->contact_type = $cnt['contact_type'];
                $cnt_item->relation = $cnt['relation'];
                $cnt_item->specify_relation = $cnt['specify_relation'];
                $cnt_item->name = $cnt['name'];
                $cnt_item->surname = $cnt['surname'];
                $cnt_item->email = $cnt['email'];
                $cnt_item->contact_number = $cnt['contact_number'];
                $cnt_item->is_connection = 1;
                $cnt_item->company_info = $cnt['company_info'];
                $cnt_item->contact_number = $cnt['contact_number'];
                $cnt_item->company_info = $cnt['company_info'];
                $cnt_item->aa_company_name = $cnt['aa_company_name'];
                $cnt_item->aa_contact_details = $cnt['aa_contact_details'];
                $cnt_item->aa_contact_name = $cnt['aa_contact_name'];
                $cnt_item->aa_contact_surname = $cnt['aa_contact_surname'];
                $cnt_item->aa_contact_email = $cnt['aa_contact_email'];
                $cnt_item->aa_contact_number = $cnt['aa_contact_number'];
                $cnt_item->save();
            }
        }
        $this->dispatchBrowserEvent('data-updated', ['message' => 'Updated successfully.']); 
        $this->loadInitValues();
    }

    public function render(){
        return view('livewire.account.organiser.connections.connections');
    }
}
