<?php

namespace App\Http\Livewire\Account\Organiser\Expenses;

use Livewire\Component;
use Auth;
use App\Models\Expense;
use Livewire\WithFileUploads;
class Expenses extends Component
{
    use WithFileUploads;
    public $types = [];
    public $terms = [];
    public $type, $description, $amount, $rate, $term, $lender, $registered_lender, $family_lender;
    public $cur_id;
    public $isOpen = false;
    public $placeholder;
    public $payment_date, $show_type = true;
    public $title;
    public $document, $document_file;
    public $view;

    public function mount(){
        $this->types = [
            'Expenses' => '',
            'Loans' => '',
            'Other' => '',
            
        ];
        
        $this->terms = [
            'daily', 
            'weekly', 
            'monthly', 
            'quarterly', 
            'yearly', 
            'once-off',
            ];
            
        $this->type = "";
        $this->placeholder = "";
    }
    
    public function hydrate(){
        $this->dispatchBrowserEvent('reload-js');
    }

    public function updatedType(){
        if(isset($this->types[$this->type])){
            $this->placeholder = $this->types[$this->type];
        }
    }

    public function showModal($type = null, $view = null, $id = null){
        $this->show_type = true;
        if($type == "expense"){
            $this->title = "Expense";
            $this->types = [
                "Communication" => "(e.g. Cellphone, Telkom, WiFi services)",
                "Donations" => "(e.g. Church Tithes, Trade Union, Political Party, Civil Organisations)",
                "Education" => "(e.g. Schoolfees, University fees, Course Fees)",
                "Entertainment & Subscriptions" => "(e.g. DSTV, Netflix, Holiday Costs, Sport, Extracurricular Activities Such as Sport, Dance, Art, Music)",
                "Household" => "(e.g. Meat, Fruit & Vegetables, Domestic Services, Cleaning Services, Cleaning Products",
                "Personal" => "(e.g. Hairdresser, Cosmetics, Nails) ",
                "Professional Services" => "(e.g. Accountant, Lawyer, Doctor)",
                "Rent, Levies & Licenses" => "(e.g. House Rent, Homeowner Association, Body Corporate, Motor License, Rates, Water, Electricity)",
                "Other" => ""
            ];
        }elseif($type == "debt"){
            $this->type = 'debt';
            $this->title = "Debt";
            $this->placeholder = '(e.g. Absa Credit Card, FNB Home Bond, Ndlovu Accounting Fees, etc)';
            $this->show_type = false;
        }elseif($type == "loan"){
            $this->type = 'loan';
            $this->title = "Loan";
            $this->placeholder = '(e.g. Personal, Study, Holiday, Emergency, House Renovations, etc.)';
            $this->show_type = false;
        }
        else{
            $this->types = [
                'Expenses' => '',
                'Loans' => '',
                'Other' => '',
            ];
        }
        
        if(isset($view)){
            $this->view = $view;
        }
        
        if($id){
            $expense = Expense::find($id);
            if ($expense) {
                $this->cur_id = $id;
                $this->type = $expense->type;
                $this->description = $expense->description;
                $this->amount = $expense->amount;
                $this->rate = $expense->rate;
                $this->term = $expense->term;
                $this->lender = $expense->lender;
                $this->registered_lender = $expense->registered_lender;
                $this->family_lender = $expense->family_lender;
                $this->payment_date = $expense->payment_date;
                $this->document = '';
                $this->document_file = $expense->document;

            }    
        }
        
        $this->dispatchBrowserEvent('show-insurance-form-modal');
    }

    public function saveExpense(){
        $this->validate([
            'type' => 'required', 
            'description' => 'required', 
            'amount' => 'required',
            'rate' => 'nullable',
            'term' => 'nullable',
            'lender' => 'required_if:type,loan',
            'registered_lender' => 'required_if:type,loan',
            'document' => 'nullable|file|mimes:pdf,jpeg,png|max:2048',
        ]);

        if($this->cur_id){
            $exp = Expense::find($this->cur_id);
        }
        else{
            $exp = new Expense();
        }
        
        $path = null;
        if($this->document){
            $path = $this->document->storePublicly('document', 'public');
        }
        
        $exp->user_id = Auth::user()->id;
        $exp->type = $this->type;
        $exp->description = $this->description;
        $exp->amount = str_replace(' ', '', str_replace(',', '',$this->amount));
        $exp->rate = $this->rate;
        $exp->term = $this->term;
        if($this->type == 'loan'){
            $exp->lender = $this->lender;
            $exp->registered_lender = $this->registered_lender;
            $exp->family_lender = $this->family_lender;
        }
        $exp->payment_date = $this->payment_date;
        if($path){
            $exp->document = $path;
        }
        $exp->save();
        $this->clearFields();
        $this->dispatchBrowserEvent('close-form-modal');
        session()->flash('message', 'Successfully saved.');
        
    }

    public function clearFields(){
        $this->cur_id = null;
        $this->type = null; 
        $this->description = null; 
        $this->amount = null; 
        $this->rate = null;
        $this->term = null;
        $this->lender = null;
        $this->registered_lender = null;
        $this->family_lender = null;
        $this->payment_date = null;
        $this->document = null;
        $this->placeholder = null;
        $this->view = null;
    }
    
    public function removeExpense($id){
        Expense::where('id',$id)->delete();    
        session()->flash('message', 'Expense deleted.');
    }
    
    public function closeModal(){
        $this->clearFields();
        $this->dispatchBrowserEvent('close-form-modal');
    }
    
    public function removeFile($id){
        $ins = Expense::find($id);
        $ins->document = null;
        $ins->save();
        $this->clearFields();
        return redirect()->to('organiser/debt-expenses');
    }

    public function render(){
        $exps = Expense::where('user_id', Auth::user()->id)->get()->groupBy('type');
        //dd($exps);
        return view('livewire.account.organiser.expenses.expenses', [
            'exps' => $exps
        ]);
    }
}
