<?php

namespace App\Http\Livewire\Account\Organiser\Funerals;

use Livewire\Component;

use Auth;
use App\Models\Funeral;
use App\Models\Dependents;
use App\Models\FuneralCover;

class Funerals extends Component
{
    public $policy_details, $memorials_service, $cremation_details;
    public $parlor_details, $requests, $obituary, $percentage;
    public $cur_id, $view;
    public $dependents = [], $add_dependents = [];
    // public $name, $surname, $id_number;

    public $cur_dependants = [];
    
    public function mount(){
        $this->initValues();
    }
    
    public function hydrate(){
        $this->dispatchBrowserEvent('reload-js');
    }
    
    public function initValues(){
        $fn = Funeral::where('user_id', Auth::user()->id)->first();
        if($fn){
            $this->policy_details = $fn->policy_details;
            $this->memorials_service = $fn->memorials_service;
            $this->cremation_details = $fn->cremation_details;
            $this->parlor_details = $fn->parlor_details;
            $this->requests = $fn->requests;
            $this->obituary = $fn->obituary;
        }
        foreach(Auth::user()->spouses()->where('marital_status', 'Now Married')->get() AS $spouse){
            $arr = [
                "type" => "spouse",
                "key" => 'sp_'.$spouse->id,
                'name' => $spouse->spouse_name,
                'surname' => $spouse->spouse_surname,
                'id_number' => $spouse->spouse_id_number,
                'percentage' => ''
            ];
            $this->cur_dependants[] = $arr;
        }
        foreach(Auth::user()->children AS $child){
            $arr = [
                "type" => "child",
                "key" => 'ch_'.$child->id,
                'name' => $child->child_name,
                'surname' => $child->child_surname,
                'id_number' => $child->child_id_number,
                'percentage' => ''
            ];
            $this->cur_dependants[] = $arr;
        }
        $arr = [
            "type" => "other",
            'key' => 'other',
            'name' => "",
            'surname' => "",
            'id_number' => "",
            'percentage' => ""
        ];
        $this->cur_dependants[] = $arr;
    }

    public function updatedDependents($val, $key){
        $arr = explode(".", $key);
        $key = $arr[0];
        foreach($this->cur_dependants AS $cur_dpt){
            if($cur_dpt['key'] == $val){
                $this->dependents[$key] = $cur_dpt;
            }
        }
    }

    public function addDependent(){
        $arr = [
            'type' => '',
            'key' => '',
            'name' => '',
            'surname' => '',
            'id_number' => '',
            'percentage' => ''
        ];
        $this->dependents[] = $arr;
    }

    public function showModal($view = null, $id = null){
        if($id){
           $policy = FuneralCover::where('user_id', Auth::user()->id)->where('id', $id)->first(); 
           if($policy){
                $this->cur_id = $id;
                $this->view = $view;
                $this->policy_number = $policy->policy_number;
                $this->policy_holder = $policy->policy_holder;
                $this->company = $policy->company;
                $this->coverage_amount  = $policy->coverage_amount;
                
                $dps = $policy->dependants;
                if($dps->count() > 0){
                    $this->dependents = [];
                    foreach($dps AS $dp){
                        $dp_arr = [
                            'type' => 'other',
                            'id' => $dp->id,
                            'name' => $dp->name,
                            'surname'=> $dp->surname,
                            'id_number' => $dp->id_number,
                            'percentage' => $dp->percentage
                        ];
    
                        $this->dependents[] = $dp_arr;
                    }
                }
           }
        }
        else{
                $this->view = $view;
                $this->policy_number = '';
                $this->policy_holder = '';
                $this->company = '';
                $this->coverage_amount  = ''; 
        }
        
        $this->dispatchBrowserEvent('show-policy-form-modal');
    }
    
    public function closeModal(){
        $this->clearFields();
        $this->dispatchBrowserEvent('close-policy-form-modal');
    }
    
    public function clearFields(){
        $this->cur_id = null;
        $this->view = null;
        $this->policy_number = null;
        $this->policy_holder = null;
        $this->company = null;
        $this->coverage_amount  = null;
        $this->view = null;
        $dp_arr = [
            'id' => '',
            'name' => '',
            'surname'=> '',
            'id_number' => '',
            'percentage' => ''
        ];
        
        $this->dependents = [];
    }
    
    public function saveFuneral(){
        
        if($this->cur_id){
            $fn = FuneralCover::where('id', $this->cur_id)->first();    
        }else{
            $fn = new FuneralCover();
        }
        
        $fn->user_id = Auth::user()->id;
        $fn->policy_number = $this->policy_number;
        $fn->policy_holder = Auth::user()->name;
        $fn->company = $this->company;
        $fn->coverage_amount = $this->coverage_amount;
        $fn->save();
        
        foreach($this->dependents as $dependent){
            if(isset($dependent['id'])){
                $dep = Dependents::find($dependent['id']);
            }else{
                $dep = new Dependents();
            }
            
            $dep->user_id = Auth::user()->id;
            $dep->funeral_cover_id = $fn->id;
            $dep->name = $dependent['name'];
            $dep->surname = $dependent['surname'];
            $dep->id_number = $dependent['id_number'];
            $dep->percentage = $dependent['percentage'];
            $dep->save();
        }

        $this->clearFields();
        $this->dispatchBrowserEvent('close-policy-form-modal');
        session()->flash('message', 'Saved successfully.');
        
    }
    
    public function savefuneralDetails(){
        $fn = Funeral::where('user_id', Auth::user()->id)->first();
        if(!$fn){
            $fn = new Funeral();
        }
        
        $fn->user_id = Auth::user()->id;
        $fn->policy_details = $this->policy_details;
        $fn->memorials_service = $this->memorials_service;
        $fn->cremation_details = $this->cremation_details;
        $fn->parlor_details = $this->parlor_details;
        $fn->requests = $this->requests;
        $fn->obituary = $this->obituary;
        $fn->save();
        
        session()->flash('message', 'Saved successfully.');
    }

    public function deleteFuneralPolicy($id){
        $cover = FuneralCover::find($id);
        if($cover){
            $cover->delete();
        }
        
        $this->clearFields();
        $this->dispatchBrowserEvent('close-policy-form-modal');
        session()->flash('message', 'Deleted successfully.');
    }

    public function removeDependent($id){
        Dependents::where('id', $id)->delete();
        $this->clearFields();
        $this->dispatchBrowserEvent('close-policy-form-modal');
        session()->flash('message', 'Deleted successfully.');
    }
    
    public function render(){
        $funeral_policies = FuneralCover::where('user_id', Auth::user()->id)->get();
        return view('livewire.account.organiser.funerals.funerals' , [
            'funeral_policies' => $funeral_policies
        ]);
    }
}
