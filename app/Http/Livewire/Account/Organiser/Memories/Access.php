<?php

namespace App\Http\Livewire\Account\Organiser\Memories;

use Livewire\Component;

use Auth;
use App\Models\MemoriesAccess;

class Access extends Component
{
    public $contacts = [];

    public function mount(){
        $this->loadData();
    }

    public function loadData(){
        $this->contacts = [];
        $acc = MemoriesAccess::where('user_id', Auth::user()->id)->get();
        if($acc->count() > 0){
            foreach($acc AS $ac){
                $arr = [
                    'id' => $ac->id,
                    'name' => $ac->name,
                    'surname' => $ac->surname,
                    'email' => $ac->email,
                    'phone_number' => $ac->phone_number,
                    'id_number' => $ac->id_number,
                    'message' => $ac->message
                ];
                $this->contacts[] = $arr;
            }
        }
        else{
            $this->addContact();
        }
    }

    public function addContact(){
        $arr = [
            'name' => '',
            'surname' => '',
            'email' => '',
            'phone_number' => '',
            'id_number' => '',
            'message' => ''
        ];
        $this->contacts[] = $arr;
    }

    public function removeContact($id){
        $acc = MemoriesAccess::find($id);
        if($acc){
            $acc->delete();
        }
        $this->loadData();
    }

    public function saveContacts(){
        foreach($this->contacts AS $cnt){
            if($cnt['name'] && $cnt['surname']){
                if(isset($cnt['id'])){
                    $acc = MemoriesAccess::find($cnt['id']);
                }
                else{
                    $acc = new MemoriesAccess();
                }
                $acc->user_id = Auth::user()->id;
                $acc->name = $cnt['name'];
                $acc->surname = $cnt['surname'];
                $acc->email = $cnt['email'];
                $acc->phone_number = $cnt['phone_number'];
                $acc->id_number = $cnt['id_number'];
                $acc->message = $cnt['message'];
                $acc->save();
            }
        }
        session()->flash('message', 'Saved successfully.');
        $this->loadData();
    }

    public function render(){
        return view('livewire.account.organiser.memories.access');
    }
}
