<?php

namespace App\Http\Livewire\Account\Organiser\Memories;

use Livewire\Component;
use Livewire\WithFileUploads;

use Auth;
use App\Models\Memory;
use App\Models\PopUp;
use App\Models\Questionniaire;

class Memories extends Component
{
    use WithFileUploads;
    
    public $memories = [];
    public $media_types = [];
    public $media_type, $memory_type, $media ,$notes, $recipient, $link;
    public $memory_types = [];
    public $questionnaires;
    
    public $favorite_song, $favorite_movie, $favorite_place, $favorite_food, $special_memory, $biggest_lesson;
    public $regret, $religion_beliefs, $knew_earlier, $thank_someone ,$proud_of, $remember_me_by;
    public $happy_thing, $sad_thing, $best_quality, $bucket_list, $advice, $funny_memory, $q_id;


    public $show_init_pop_up = false;
    
    public function mount(){
        $this->media_types = [
            'Video Clip', 
            'Song', 
            'Image', 
            'Document', 
            'Link',
            'Questionnaire'
        ];
        
        $this->getData();

        $pop_up = PopUp::where('user_id', Auth::user()->id)->first();
        if($pop_up){
            if($pop_up->memories){
                $this->show_init_pop_up = false;
            }
            else{
                $this->show_init_pop_up = true;
            }
        }
        else{
            $this->show_init_pop_up = true;
            $pop_up = new PopUp();
            $pop_up->user_id = Auth::user()->id;
            $pop_up->save();
        }
    }

    public function initModalSeen(){
        $pop_up = PopUp::where('user_id', Auth::user()->id)->first();
        if($pop_up){
            $pop_up->memories = 1;
            $pop_up->save();
        }
        $this->dispatchBrowserEvent('close-modals');
    }
    
    public function getData(){
        $memories = Memory::where('user_id', Auth::user()->id)->get();
        if($memories->count() > 0){
            $this->memories = [];
            foreach($memories AS $mem){
                $arr = [
                    'id' => $mem->id,
                    'media_type' => $mem->media_type,
                    'memory_type' => $mem->memory_type,
                    'notes' => $mem->notes,
                    'recipient' => $mem->recipient,
                    'link_url' => $mem->link,
                    'media' => '',
                    'file_url' => $mem->media,
                    
                    'name' => $mem['name'],
                    'surname' => $mem['surname'],
                    'email' => $mem['email'],
                    'id_number' => $mem['id_number'],
                    'phone_number' => $mem['phone_number'],
                    'questionnaires' => [],
                ];
                
                $quest = $mem->questionnaire;
                if ($quest->count() > 0) {
                    foreach ($quest as $q) {
                        $quest_arr = [
                            'id' => $q->id,
                            'favorite_song' => $q->favorite_song,
                            'favorite_movie' => $q->favorite_movie,
                            'favorite_place' => $q->favorite_place,
                            'favorite_food' => $q->favorite_food,
                            'special_memory' => $q->special_memory,
                            'biggest_lesson' => $q->biggest_lesson,
                            'regret' => $q->regret,
                            'religion_beliefs' => $q->religion_beliefs,
                            'knew_earlier' => $q->knew_earlier,
                            'thank_someone' => $q->thank_someone,
                            'happy_thing' => $q->happy_thing,
                            'sad_thing' => $q->sad_thing,
                            'best_quality' => $q->best_quality,
                            'bucket_list' => $q->bucket_list,
                            'advice' => $q->advice,
                            'proud_of' => $q->proud_of,
                            'remember_me_by' => $q->remember_me_by,
                            'funny_memory' => $q->funny_memory,
                            'best_part' => $q->best_part,
                        ];
                        $arr['questionnaires'][] = $quest_arr;
                    }
                }
                else{
                    $arr['questionnaires'] = ['questionnaire' => ''];
                }
                
                $this->memories[] = $arr;
            }
        }
        else{
            $this->addMemory();
        }
    }
    
    public function addMemory(){
        $arr = [
            'media_type' => '',
            'memory_type' => '',
            'notes' => '',
            'recipient' => '',
            'media' => '',
            'link_url' => '',
            'file_url' => '',
            
            'name' => '',
            'surname' => '',
            'email' => '',
            'id_number' => '',
            'phone_number' => '',
            'questionnaires' => ['questionnaire' => ''],
        ];
        $this->memories[] = $arr;
        
    }
    
    public function saveMemory(){
        foreach($this->memories as $k=>$mem){

            if($mem['media_type'] != 'Link'){
                if ($mem['media_type'] === 'Video' || $mem['media_type'] === 'Song') {

                    $rules = [
                        'memories.'.$k.'.media' => 'nullable|file|mimes:mp4,mp3,wav,avi|max:5012',
                    ];
        
                    $validatedData = $this->validate($rules, [], [
                        'memories.'.$k.'.media' => 'video/song',
                    ]);

                }elseif($mem['media_type'] === 'Image'){

                    $rules = [
                        'memories.'.$k.'.media' => 'nullable|file|mimes:jpeg,png|max:1048',
                    ];
        
                    $validatedData = $this->validate($rules, [], [
                        'memories.'.$k.'.media' => 'image',
                    ]);

                }elseif($mem['media_type'] === 'Document'){

                    $rules = [
                        'memories.'.$k.'.media' => 'nullable|file|mimes:pdf|max:2048',
                    ];
        
                    $validatedData = $this->validate($rules, [], [
                        'memories.'.$k.'.media' => 'document',
                    ]);
                }
            }
            
            if(isset($mem['id'])){
                $_mem = Memory::find($mem['id']);
            }
            else{
                $_mem = new Memory();
            }
            
            if($mem['media']){
                $path = $mem['media']->storePublicly('memory', 'public');
                $_mem->media = $path;
            }
    
            $_mem->user_id = Auth::user()->id;
            $_mem->media_type = $mem['media_type'];
            $_mem->memory_type = $mem['memory_type'];
            $_mem->notes = $mem['notes'];
            $_mem->recipient = $mem['recipient'];
            
            $_mem->name = $mem['name'];
            $_mem->surname = $mem['surname'];
            $_mem->email = $mem['email'];
            $_mem->id_number = $mem['id_number'];
            $_mem->phone_number = $mem['phone_number'];
    
            $path = null;
            
            if($mem['media_type'] === 'Link'){
                $_mem->link = $mem['link_url'];
            }
            
            $_mem->save();
            
            if(isset($mem['questionnaires']) && is_array($mem['questionnaires'])){
                foreach($mem['questionnaires'] AS $q){
                    if(is_array($q)){
                        if(isset($q['id'])){
                            $quest = Questionniaire::find($q['id']);
                        }else{
                            $quest = new Questionniaire();
                            $quest->user_id = Auth::user()->id;
                            $quest->memory_id = $_mem->id;
                        }
                        
                        if(isset($q['favorite_song'])){
                            $quest->favorite_song = $q['favorite_song'];    
                        }
                        
                        if(isset($q['favorite_movie'])){
                            $quest->favorite_movie = $q['favorite_movie'];    
                        }
                        
                        if(isset($q['favorite_place'])){
                            $quest->favorite_place = $q['favorite_place'];    
                        }
                        
                        if(isset($q['favorite_food'])){
                            $quest->favorite_food = $q['favorite_food'];    
                        }
                        
                        if(isset($q['special_memory'])){
                            $quest->special_memory = $q['special_memory'];    
                        }
                        
                        if(isset($q['biggest_lesson'])){
                            $quest->biggest_lesson = $q['biggest_lesson'];
                        }
                        
                        if(isset($q['regret'])){
                            $quest->regret = $q['regret'];    
                        }
                        
                        if(isset($q['religion_beliefs'])){
                            $quest->religion_beliefs = $q['religion_beliefs'];    
                        }
                        
                        if(isset($q['knew_earlier'])){
                            $quest->knew_earlier = $q['knew_earlier'];    
                        }
                        
                        if(isset($q['thank_someone'])){
                            $quest->thank_someone = $q['thank_someone'];    
                        }
                        
                        if(isset($q['happy_thing'])){
                            $quest->happy_thing = $q['happy_thing'];    
                        }
                        
                        if(isset($q['sad_thing'])){
                            $quest->sad_thing = $q['sad_thing'];    
                        }
                        
                        if(isset($q['best_quality'])){
                            $quest->best_quality = $q['best_quality'];    
                        }
                        
                        if(isset($q['bucket_list'])){
                            $quest->bucket_list = $q['bucket_list'];    
                        }
                        
                        if(isset($q['advice'])){
                            $quest->advice = $q['advice'];    
                        }
                        
                        if(isset($q['proud_of'])){
                            $quest->proud_of = $q['proud_of'];    
                        }
                        
                        if(isset($q['remember_me_by'])){
                            $quest->remember_me_by = $q['remember_me_by'];    
                        }
                        
                        if(isset($q['funny_memory'])){
                            $quest->funny_memory = $q['funny_memory'];    
                        }
                        
                        if(isset($q['best_part'])){
                            $quest->best_part = $q['best_part'];    
                        }
                        
                        $quest->save();
                    }
                }
                    
            }
            
            //dd($_mem);
        }
    
        session()->flash('message', 'Successfully saved.');
        $this->getData();
    }
    
    public function removeMemory($id){
        Memory::where('id',$id)->delete(); 
        Questionniaire::where('memory_id', $id)->delete();
        session()->flash('message', 'Deleted');
        return redirect()->to('/organiser/memories');
    }
    
    public function removeDocument($id){
        $mem = Memory::find($id);
        $mem->media = null;
        $mem->save();
        $this->getData();
    }

    public function render()
    {
        return view('livewire.account.organiser.memories.memories');
    }
}
