<?php

namespace App\Http\Livewire\Account\Organiser\Memories;

use Livewire\Component;
use Auth;
use App\Models\Questionniaire;

class Questionnaire extends Component
{
    public $favorite_song, $favorite_movie, $favorite_place, $favorite_food, $special_memory, $biggest_lesson, $regret, $miss_most, $life_best_part, $life_worst_part, $religion_beliefs, $funeral_wish, $dint_know, $hopes_for_someone, $happy_thing, $sad_thing, $best_quality, $bucket_list, $remember_me_by, $funny_memory;
    
    public function mount(){
        $qs = Questionniaire::where('user_id', Auth::user()->id)->first();
        if($qs){
            $this->favorite_song = $qs->favorite_song;
            $this->favorite_movie = $qs->favorite_movie;
            $this->favorite_place = $qs->favorite_place;
            $this->favorite_food = $qs->favorite_food;
            $this->special_memory = $qs->special_memory;
            $this->biggest_lesson = $qs->biggest_lesson;
            $this->regret = $qs->regret;
            $this->miss_most = $qs->miss_most;
            $this->life_best_part = $qs->life_best_part;
            $this->life_worst_part = $qs->life_worst_part; 
            $this->religion_beliefs = $qs->religion_beliefs;
            $this->funeral_wish = $qs->funeral_wish;
            $this->dint_know = $qs->dint_know;
            $this->hopes_for_someone = $qs->hopes_for_someone;
            $this->happy_thing = $qs->happy_thing;
            $this->sad_thing = $qs->sad_thing;
            $this->best_quality = $qs->best_quality;
            $this->bucket_list = $qs->bucket_list;
            $this->remember_me_by = $qs->remember_me_by;
            $this->funny_memory = $qs->funny_memory;
        }

    }

    public function saveMemoryQuestionniare(){
        $qs = Questionniaire::where('user_id', Auth::user()->id)->first();
        if(!$qs){
            $qs = new Questionniaire();
        }
        $qs->user_id = Auth::user()->id;
        $qs->favorite_song = $this->favorite_song;
        $qs->favorite_movie = $this->favorite_movie;
        $qs->favorite_place = $this->favorite_place;
        $qs->favorite_food = $this->favorite_food;
        $qs->special_memory = $this->special_memory;
        $qs->biggest_lesson = $this->biggest_lesson;
        $qs->regret = $this->regret;
        $qs->miss_most = $this->miss_most;
        $qs->life_best_part = $this->life_best_part;
        $qs->life_worst_part = $this->life_worst_part; 
        $qs->religion_beliefs = $this->religion_beliefs;
        $qs->funeral_wish = $this->funeral_wish;
        $qs->dint_know = $this->dint_know;
        $qs->hopes_for_someone = $this->hopes_for_someone;
        $qs->happy_thing = $this->happy_thing;
        $qs->sad_thing = $this->sad_thing;
        $qs->best_quality = $this->best_quality;
        $qs->bucket_list = $this->bucket_list;
        $qs->remember_me_by = $this->remember_me_by;
        $qs->funny_memory = $this->funny_memory;
        $qs->save();

        session()->flash('message', 'Saved successfully.');

    }

    public function render(){
        return view('livewire.account.organiser.memories.questionnaire');
    }
}
