<?php

namespace App\Http\Livewire\Account\Organiser\OnlineWill;

use Livewire\Component;
use Livewire\WithFileUploads;

use Auth;
use PDF;
use App\Models\WillUpload;
use App\Models\WillOption;
use App\Models\ChildGuardian;
use App\Models\Review;
use App\Models\PopUp;
use Log;

class OnlineWill extends Component
{
    use WithFileUploads;

    public $will_view;
    public $stage, $review_page;

    public $will_upload, $opts, $terms_and_conditions;

    protected $listeners = ['deleteOnlinewill' => 'removeWillData'];

    public $show_init_pop_up = false;
    public $read_and_accept_terms;

    public function mount($stage = null, $review_page)
    {
        if ($stage) {
            $this->stage = $stage;
            $this->will_view = $stage;
        } else {
            $this->will_view = 'start';
        }

        if ($review_page) {
            $this->review_page = $review_page;
        }

        $this->opts = WillOption::where('user_id', Auth::user()->id)->first();

        // Disable the initial pop-up
        $this->show_init_pop_up = false;

        // Optional: Handle the PopUp record if you need to perform other actions
        $pop_up = PopUp::where('user_id', Auth::user()->id)->first();
        if (!$pop_up) {
            $pop_up = new PopUp();
            $pop_up->user_id = Auth::user()->id;
            $pop_up->save();
        }

        // $pop_up = PopUp::where('user_id', Auth::user()->id)->first();
        // if ($pop_up) {
        //     if ($pop_up->will) {
        //         $this->show_init_pop_up = false;
        //     } else {
        //         $this->show_init_pop_up = true;
        //     }
        // } else {
        //     $this->show_init_pop_up = true;
        //     $pop_up = new PopUp();
        //     $pop_up->user_id = Auth::user()->id;
        //     $pop_up->save();
        // }
    }

    public function confirmwillUpload()
    {
        if ($this->read_and_accept_terms == 'accept') {
            return redirect('organiser/safekeeping');
        } else {
            $this->addError('error', 'Please acknowledge your understanding by checking the "I understand" box.');
        }
    }

    public function initModalSeen()
    {
        $pop_up = PopUp::where('user_id', Auth::user()->id)->first();
        if ($pop_up) {
            $pop_up->will = 1;
            $pop_up->save();
        }
        $this->dispatchBrowserEvent('close-modals');
    }

    public function showUploadWillModal()
    {
        $this->dispatchBrowserEvent('show-upload-will-modal');
    }

    public function saveUploadWill()
    {
        $this->validate([
            'will_upload' => 'required',
            'terms_and_conditions' => 'required',
        ]);

        if ($this->will_upload) {
            $path = $this->will_upload->storePublicly('wills', 'public');
            $will = WillUpload::where('user_id', Auth::user()->id)->first();
            if (!$will) {
                $will = new WillUpload();
            }
            $will->user_id = Auth::user()->id;
            $will->will_url = $path;
            $will->save();
            $this->dispatchBrowserEvent('close-modals');
            $this->dispatchBrowserEvent('data-updated', ['message' => 'Updated successfully.']);
        }
    }

    public function downloadwill()
    {
        $data = [
            'user' => Auth::user(),
        ];
        $pdfContent = PDF::loadView('pdf.will', $data)->output();
        return response()->streamDownload(fn() => print $pdfContent, 'will.pdf');
    }

    public function removeWillData()
    {
        $usr = Auth::user();
        foreach ($usr->guardians as $gd) {
            $ch_gd = ChildGuardian::where('guardian_id', $gd->id)->get();
            foreach ($ch_gd as $ch_g) {
                $ch_g->delete();
            }
            $gd->delete();
        }
        if ($usr->willConfirmation) {
            $usr->willConfirmation->delete();
        }
        $rv = Review::where('user_id', Auth::user()->id)->first();
        if ($rv) {
            $rv->delete();
        }
        if ($usr->interview) {
            $int = $usr->interview;

            // Log::info($int);

            // $int->have_property_outside_sa = null;
            // $int->read_and_write_capability = null;

            // $int->leave_possesions_to_icapitated_persons = null;
            // $int->exclude_persons_from_will = null;

            // Delete the record, instead of updating it
            $int->delete();
            // $int->save();
        }
        foreach ($usr->ind_beneficiaries as $ind) {
            $ind->delete();
        }
        foreach ($usr->org_beneficiaries as $org) {
            $org->delete();
        }
        foreach ($usr->trust_beneficiaries as $tr_ben) {
            $tr_ben->delete();
        }
        foreach ($usr->will_spouses as $sp) {
            $sp->delete();
        }
        foreach ($usr->executors as $ex) {
            $ex->delete();
        }
        foreach ($usr->inheritance as $in) {
            $in->delete();
        }
        foreach ($usr->testamentary_trust as $tr) {
            $tr->delete();
        }
        $fw = $usr->funeral_wishes;
        if ($fw) {
            $fw->delete();
        }
        foreach ($usr->witnesses as $wt) {
            $wt->delete();
        }
        $wmo = $usr->will_menu_options;
        if ($wmo) {
            $wmo->delete();
        }
        $trusts = $usr->trusts;
        foreach ($trusts as $trst) {
            $trst->delete();
        }

        return redirect('organiser/online-will');
    }

    public function render()
    {
        $uploaded = WillUpload::where('user_id', Auth::user()->id)->first();
        return view('livewire.account.organiser.online-will.online-will', [
            'uploaded' => $uploaded,
        ]);
    }
}
