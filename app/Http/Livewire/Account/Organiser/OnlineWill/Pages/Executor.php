<?php

namespace App\Http\Livewire\Account\Organiser\OnlineWill\Pages;

use Livewire\Component;
use Auth;
use App\Models\Executor AS ExecModel;
use App\Lib\Sharedfunctions;

class Executor extends Component
{
    
    public $first_name, $middle_name, $surname, $id_number, $relation, $specify_relation;
    public $sub_first_name, $sub_middle_name, $sub_surname, $sub_id_number, $sub_relation, $sub_specify_relation;
    public $id_type, $sub_id_type;
    
    public $relations;

    public function mount(){
        $this->id_type = "rsa_id";
        $this->sub_id_type = "rsa_id";
        $this->relation = "";
        $this->sub_relation = "";

        $fnc = new Sharedfunctions();
        
        $this->relations = [
            'Great Grand Father',
            'Great Grand Mother',

            'Grand Father', 
            'Grand Mother',

            'Father', 
            'Uncle',
            'Step Father',
            'Father In Law',

            'Mother',
            'Aunt',
            'Step Mother',
            'Mother In Law',
            
            'Brother', 
            'Step Brother',
            'Brother In Law',
            
            'Sister',
            'Step Sister',
            'Sister In Law',

            'Cousin',
            'Friend', 

            'Spouse',
            'Partner', 
            'Wife',
            'Husband',

            'Son', 
            'Daughter',
            'Son In Law',
            'Daughter In Law',
            "Son's Partner",
            "Daughter's Partner",
            'Niece', 
            'Nephew',
            'Step Son',
            'Step Daughter',

            'Grand Son', 
            'Grand Daughter',
            
            'Great Grand Son',
            'Great Grand Daughter',
            
            'Other'
        ];
        
        sort($this->relations);

        $main_exec = ExecModel::where('user_id', Auth::user()->id)->where('exec_type', 'main_exec')->first();
        if($main_exec){
            $this->first_name = $main_exec->first_name;
            $this->middle_name = $main_exec->middle_name;
            $this->surname = $main_exec->surname;
            $this->id_number = $main_exec->id_number;
            $this->relation = $main_exec->relation;
            $this->specify_relation = $main_exec->specify_relation;

            if($fnc->isSaIdNumber($main_exec->id_number)){
                $this->id_type = "rsa_id";
            }
            else{
                $this->id_type = "passport";
            }
        }
        $sub_exec = ExecModel::where('user_id', Auth::user()->id)->where('exec_type', 'sub_exec')->first();
        if($sub_exec){
            $this->sub_first_name = $sub_exec->first_name;
            $this->sub_middle_name = $sub_exec->middle_name;
            $this->sub_surname = $sub_exec->surname;
            $this->sub_id_number = $sub_exec->id_number;
            $this->sub_relation = $sub_exec->relation;
            $this->sub_specify_relation = $sub_exec->specify_relation;

            if($fnc->isSaIdNumber($sub_exec->id_number)){
                $this->sub_id_type = "rsa_id";
            }
            else{
                $this->sub_id_type = "passport";
            }
        }
    }

    public function clearSectionData(){
        $main_exec = ExecModel::where('user_id', Auth::user()->id)->where('exec_type', 'main_exec')->first();
        if($main_exec){
            $main_exec->delete();
        }
        $sub_exec = ExecModel::where('user_id', Auth::user()->id)->where('exec_type', 'sub_exec')->first();
        if($sub_exec){
            $sub_exec->delete();
        }
        return redirect('organiser/online-will/executor');
    }

    public function getNextPage(){
        $fnc = new Sharedfunctions();
        $next_page = $fnc->getNextWillPage('executor');
        if($next_page){
            return redirect('organiser/online-will/'.$next_page);
        }
    }

    public function saveExecutor($action = null){
        $rules = [
            'first_name' => 'required',
            'surname' => 'required',
            'relation' => 'required',
            'id_number' => 'required',
            'specify_relation' => 'required_if:relation,Other'
        ];
        if($this->id_type == "rsa_id"){
            $rules['id_number'] = 'said';
        }

        $this->validate($rules);
        $main_exec = ExecModel::where('user_id', Auth::user()->id)->where('exec_type', 'main_exec')->first();
        if(!$main_exec){
            $main_exec = new ExecModel();
        }
        $main_exec->user_id = Auth::user()->id;
        $main_exec->exec_type = "main_exec";
        $main_exec->first_name = $this->first_name;
        $main_exec->middle_name = $this->middle_name;
        $main_exec->surname = $this->surname;
        $main_exec->id_number = $this->id_number;
        $main_exec->relation = $this->relation;
        $main_exec->specify_relation = $this->specify_relation;
        $main_exec->save();

        
        if($this->sub_first_name){
            $sub_rules = [
                'sub_first_name' => 'required',
                'sub_surname' => 'required',
                'sub_id_number' => 'required',
                'sub_relation' => 'required',
                'sub_specify_relation' => 'required_if:sub_relation,Other'
            ];
            if($this->sub_id_type == "rsa_id"){
                $sub_rules['sub_id_number'] = 'said';
            }
            $this->validate($sub_rules);

            $sub_exec = ExecModel::where('user_id', Auth::user()->id)->where('exec_type', 'sub_exec')->first();
            if(!$sub_exec){
                $sub_exec = new ExecModel();
            }

            $sub_exec->user_id = Auth::user()->id;
            $sub_exec->exec_type = "sub_exec";
            $sub_exec->first_name = $this->sub_first_name;
            $sub_exec->middle_name = $this->sub_middle_name;
            $sub_exec->surname = $this->sub_surname;
            $sub_exec->id_number = $this->sub_id_number;
            $sub_exec->relation = $this->sub_relation;
            $sub_exec->specify_relation = $this->sub_specify_relation;
            $sub_exec->save();
        }
        
        if($action == "go_to_next"){
            $this->getNextPage();
        }
        else{
            $this->dispatchBrowserEvent('data-updated', ['message' => 'Updated successfully.']); 
        }
    }

    public function render(){
        return view('livewire.account.organiser.online-will.pages.executor');
    }
}
