<?php

namespace App\Http\Livewire\Account\Organiser\OnlineWill\Pages;

use Livewire\Component;

use Auth;
use App\Models\Funeralwish;
use App\Lib\Sharedfunctions;

class FuneralWishes extends Component
{
    public $organ_donor, $cremated, $special_wishes, $has_funeral_wishes, $berried;

    public function mount(){
        $this->getData();
    }

    public function getNextPage(){
        $fnc = new Sharedfunctions();
        $next_page = $fnc->getNextWillPage('funeral-wishes');
        if($next_page){
            return redirect('organiser/online-will/'.$next_page);
        }
    }

    public function getData(){
        $fn = Funeralwish::where('user_id', Auth::user()->id)->first();
        if($fn){
            $this->organ_donor = $fn->organ_donor;
            $this->cremated = $fn->cremated;
            $this->special_wishes = $fn->special_wishes;
            $this->berried = $fn->berried;
        }
    }

    public function saveFuneralWishes($action = null){
        $this->validate([
            'cremated' => 'required',
            'organ_donor' => 'required', 
        ]);
        
        $donor = "no";
        $cremated = "no";
        if($this->organ_donor == "yes"){
            $donor = "yes";    
        }
        if($this->cremated == "yes"){
            $cremated = "yes";
        }

        $fn = Funeralwish::where('user_id', Auth::user()->id)->first();
        if(!$fn){
            $fn = new Funeralwish();
        }
        $fn->user_id = Auth::user()->id;
        $fn->organ_donor = $donor;
        $fn->cremated = $cremated;
        $fn->special_wishes = $this->special_wishes;
        $fn->berried = $this->berried;
        $fn->save();
        
        if($action == "go_to_next"){
            $this->getNextPage();
        }
        else{
            $this->dispatchBrowserEvent('data-updated', ['message' => 'Updated successfully.']); 
        }
    }

    public function render(){
        return view('livewire.account.organiser.online-will.pages.funeral-wishes');
    }
}
