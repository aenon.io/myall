<?php

namespace App\Http\Livewire\Account\Organiser\OnlineWill\Pages;

use Livewire\Component;

use Auth;
use App\Models\Child;
use App\Models\Guardian;
use App\Models\ChildGuardian;
use App\Lib\Sharedfunctions;

class Guardians extends Component
{
    public $children = [];
    public $child, $first_name, $middle_name, $surname, $id_number;
    public $cur_gd_id;
    public $show_modal = false;

    public function mount(){
        // $this->children = Child::where('user_id', Auth::user()->id)->get();
        $this->loadChildren();
        if(Auth::user()->guardians->count() == 0){
            $this->show_modal = true;
        }
    }

    public function redoSection(){
        $chlds = Child::where('user_id', Auth::user()->id)->get();
        foreach($chlds AS $ch){
            $gd = $ch->guardian;
            if($gd){
                $gd->delete();
            }
        }
        $this->loadChildren();
    }
    
    public function removeGuadian($key){
        $arr = $this->children[$key];
        $child = Child::find($arr['id']);
        $gd = $child->guardian;
        if($gd){
            $gd->delete();
            $this->loadChildren();
        }
        else{
            $this->children[$key]['guardian_name'] = "";
            $this->children[$key]['guardian_middle_name'] = "";
            $this->children[$key]['guardian_surname'] = "";
            $this->children[$key]['guardian_id_number'] = "";
            $this->children[$key]['guardian_passport_number'] = "";
            $this->children[$key]['id_type'] = "";

        }
    }
    
    public function loadChildren(){
        $this->children = [];
        $chlds = Child::where('user_id', Auth::user()->id)->get();
        foreach($chlds AS $ch){
            $add = true;
            $dob = $ch->child_date_of_birth;
            if($dob){
                $yob = date('Y', strtotime($dob));
                $cy = date('Y');
                $age = $cy - $yob;
                if($age >= 18){
                    $add = false;
                }
            }
            if($add){
                $arr = [];
                $arr['id'] = $ch->id;
                // $arr['child_name'] = $ch->child_name.' '.$ch->middle_name.' '.$ch->child_surname;
                $arr['child_name'] = $ch->child_name;
                $arr['child_middle_name'] = $ch->middle_name;
                $arr['child_surname'] = $ch->child_surname;
                $gd = $ch->guardian;
                if($gd){
                    if($gd->guardian){
                        $arr['guardian_name'] = $gd->guardian->first_name;
                        $arr['guardian_middle_name'] = $gd->guardian->middle_name;
                        $arr['guardian_surname'] = $gd->guardian->surname;
                        $arr['guardian_id_number'] = $gd->guardian->id_number;
                        $arr['guardian_passport_number'] = $gd->guardian->passport_number;
                        if($gd->guardian->id_number){
                            $arr['id_type'] = 'rsa_id';
                        }
                        elseif($arr['guardian_passport_number']){
                            $arr['id_type'] = 'passport';
                        }
                        else{
                            $arr['id_type'] = "";
                        }
                    }
                    else{
                        $arr['guardian_name'] = null;
                        $arr['guardian_middle_name'] = null;
                        $arr['guardian_surname'] = null;
                        $arr['guardian_id_number'] = null;
                        $arr['guardian_passport_number'] = null;
                        $arr['id_type'] = '';
                    }
                }
                else{
                    $arr['guardian_name'] = null;
                    $arr['guardian_middle_name'] = null;
                    $arr['guardian_surname'] = null;
                    $arr['guardian_id_number'] = null;
                    $arr['guardian_passport_number'] = null;
                    $arr['id_type'] = '';                
                }
                $this->children[] = $arr;
            }
        }
    }

    public function getNextPage(){
        $fnc = new Sharedfunctions();
        $next_page = $fnc->getNextWillPage('guardian');
        if($next_page){
            return redirect('organiser/online-will/'.$next_page);
        }
    }
    
    public function saveGuardians($action = null){
        $this->dispatchBrowserEvent('go-to-top');
        $has_error = false;
        $has_some_errors = false;
        $fnc = new Sharedfunctions();
        foreach($this->children AS $child){
            if($child['id_type'] == "rsa_id"){
                $valid = $fnc->isSaIdNumber($child['guardian_id_number']);
                if(!$valid){
                    $has_error = true;
                    $this->addError('error', "Please enter valid SA ID number.");
                }
            }
        }
        if(!$has_error){
        foreach($this->children AS $child){
            if($child['guardian_name'] && $child['guardian_surname'] && ($child['guardian_id_number'] || $child['guardian_passport_number'])){
                $gd = Guardian::where('id_number', $child['guardian_id_number'])->first();
                if(!$gd){
                    $gd = new Guardian();
                }
                if(!$child['guardian_id_number'] && $child['guardian_passport_number']){
                    if($valid = $fnc->isSaIdNumber($child['guardian_passport_number'])){
                        $child['guardian_id_number'] = $child['guardian_passport_number'];
                        $child['guardian_passport_number'] = "";
                    }
                }

                $gd->user_id = Auth::user()->id;
                $gd->first_name = $child['guardian_name']; 
                $gd->middle_name = $child['guardian_middle_name']; 
                $gd->surname = $child['guardian_surname'];
                if($fnc->isSaIdNumber($child['guardian_id_number'])){
                    $gd->id_number = $child['guardian_id_number'];
                }
                $gd->passport_number = $child['guardian_passport_number'];
                $gd->save();
                
                $ch_gd = ChildGuardian::where('child_id', $child['id'])->first();
                if($ch_gd){
                    $ch_gd->delete();
                }
                $ch_gd = ChildGuardian::create([
                    'child_id' => $child['id'],
                    'guardian_id' => $gd->id
                ]);
            }
            elseif($child['guardian_name'] || $child['guardian_surname'] || $child['guardian_id_number'] || $child['guardian_passport_number']){
                $has_some_errors = true;
            }
        }
        if($has_some_errors){
            $this->addError('error', "Some  sections did not save because you did not enter all required data.");
        }
        elseif($action == "go_to_next"){
            $this->getNextPage();
        }
        else{
            $this->dispatchBrowserEvent('data-updated', ['message' => 'Updated successfully.']); 
        }
        }
    }

    public function render(){
        return view('livewire.account.organiser.online-will.pages.guardians');
    }
}
