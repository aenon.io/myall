<?php

namespace App\Http\Livewire\Account\Organiser\OnlineWill\Pages;

use Livewire\Component;
use Auth;
use App\Models\Interview as InterviewModel;
use App\Models\WillOption;

class Interview extends Component
{
    public $questions = [],
        $notes = [];
    public $answers = [],
        $cur_question;
    public $reply;

    public $stage_images = [],
        $stage_vids = [];

    public $confirmation;
    public $confirmation0;

    public function mount()
    {
        $this->setQuestions();
        $this->reply = '';

        $int = InterviewModel::where('user_id', Auth::user()->id)->first();
        if (!$int) {
            $this->cur_question = 0;
        } else {
            if ($int->have_property_outside_sa) {
                $this->cur_question = 1;
            }
            if ($int->read_and_write_capability) {
                $this->cur_question = 2;
            }
            if ($int->leave_possesions_to_icapitated_persons) {
                $this->cur_question = 3;
            }
        }
    }

    public function showInfo()
    {
        if ($this->cur_question == 2) {
            $path = public_path() . '/infographics/interview/mentally_disabled';

            $images = glob($path . '/*.jpg');

            $this->stage_images = $images;
            $this->dispatchBrowserEvent('show-interview-images-modal');
        }
        if ($this->cur_question == 3) {
            $path = public_path() . '/infographics/interview/disown';

            $images = glob($path . '/*.jpg');

            $this->stage_images = $images;
            $this->dispatchBrowserEvent('show-interview-images-modal');
        }
        $this->loadVids();
    }

    public function loadVids()
    {
        if ($this->cur_question == 2) {
            $this->stage_vids = ['What is a Will', 'Understanding the rules for inheriting specific possessions', 'When does a testamentary trust come into operation and the procedure to appoint trustees', 'What are the disadvantages of dying without a Will'];
        }
        if ($this->cur_question == 3) {
            $this->stage_vids = ['What is a Will', 'Do I need to change my Will after my divorce', 'What happens when you disinherit your spouse or dependent child in your Will', 'What are the disadvantages of dying without a Will'];
        }
    }

    public function showVid()
    {
        $this->loadVids();
        if (count($this->stage_vids) > 0) {
            $this->dispatchBrowserEvent('show-interview-vids-modal');
        } else {
            $this->dispatchBrowserEvent('show-vid-coming-soon');
        }
    }

    public function showSingleVid($name)
    {
        $name = asset('/vids/' . $name . '.mp4');
        $this->dispatchBrowserEvent('show-interview-single-vid-modal', ['name' => $name]);
    }

    public function showSingleImage($image_name)
    {
        $path = url('/infographics/' . $image_name);
        $this->dispatchBrowserEvent('show-interview-single-image-modal', ['image_path' => $path]);
    }

    public function updatedReply()
    {
        $this->saveOption($this->reply);
    }

    public function showNextquestion()
    {
        if ($this->cur_question == 3) {
            if (!$this->confirmation) {
                $this->addError('error', 'Confirm that you understand');
                $this->showNote('Yes');
                return;
            }
        }

        if ($this->cur_question != 0 && $this->cur_question != 1) {
            $this->cur_question++;
        }
        if ($this->cur_question == 4) {
            return redirect('organiser/online-will/stage-2');
        }
    }

    public function saveOption($answer)
    {
        $int = InterviewModel::where('user_id', Auth::user()->id)->first();
        if (!$int) {
            $int = new InterviewModel();
        }
        $int->user_id = Auth::user()->id;
        if ($this->cur_question == 0) {
            $int->have_property_outside_sa = $answer;
        }
        if ($this->cur_question == 1) {
            $int->read_and_write_capability = $answer;
        }
        if ($this->cur_question == 2) {
            $int->leave_possesions_to_icapitated_persons = $answer;
        }
        if ($this->cur_question == 3) {
            $int->exclude_persons_from_will = $answer;
        }
        $int->save();
        if ($this->cur_question == 3) {
            return redirect('organiser/online-will/stage-2');
        }
        $this->showNote($answer);
    }

    public function showNote($answer)
    {
        $show_note = false;
        if ($this->cur_question == 0) {
            if ($answer == 'No' || $answer == 'Yes, and I have possessions in South Africa') {
                $this->cur_question++;
            } else {
                $show_note = true;
            }
        } elseif ($this->cur_question == 1) {
            if ($answer == 'No') {
                $show_note = true;
            } else {
                $this->cur_question++;
            }
            /* } elseif ($this->cur_question == 2 || $this->cur_question == 3) { */
        } elseif ($this->cur_question == 2) {
            if ($answer == 'Yes') {
                /* $show_note = true; */
                $this->cur_question++;
            } else {
                $this->cur_question++;
            }
            /* $this->confirmation = false; */
        }
        if ($this->cur_question == 4) {
            return redirect('organiser/online-will/stage-2');
        } elseif ($show_note) {
            $this->dispatchBrowserEvent('show-note-modal');
        }
    }

    public function setQuestions()
    {
        $this->questions = [
            'Do you own any assets outside of South Africa that you wish to distribute in your will? This includes not just real estate, but also financial accounts, investments, personal belongings, and other valuables.' => ['No', 'Yes, and I have possessions in South Africa', 'Yes, and I have no possessions in South Africa'],
            // 'Can you read and write your full name on your will by means of a signature?' => ['Yes', 'No'],
            'Can you read and write your full name on your will by means of a signature?' => [],
            'Do you wish to leave any of your possessions to a person that is currently mentally disabled or incapacitated?' => ['Yes', 'No'],
            // 'Do you want to disinherit your spouse / child?' => ['Yes', 'No'],
            'Do you want to disinherit your spouse / child?' => [],
        ];

        $this->notes = [
            'This online Will application only applies to South African inheritance property to be administered in accordance with South African law.
            If you have foreign property, the law of that foreign country may apply, depending on where the property is situated, registered, held, or even managed. Additionally, your nationality, last place of residence, or your main place of residence may further determine which country’s law governs the inheritance rules.
            Any applicable foreign law may also govern who the beneficiary and executor may be, as well as the formalities of your Will.
            This includes whether you can have one Will for all the countries or whether you need a separate Will for each country.
            If you already have an offshore Will, a later Will may revoke your existing offshore Will.
            Please contact us for assistance.',

            'If you cannot sign your own name, you may ask someone to sign the Will on your behalf or sign the Will by making a mark, cross, or a thumbprint.
            When the Will is signed by someone on your behalf or by making a mark, a Commissioner of Oaths must certify your identity.
            The Commissioner of Oaths must also sign every page of the Will.
            Please confirm that you have understood this by selecting the checkbox below to continue.',

            "If you are considering leaving any of your possessions to a person
            that is currently mentally disabled or incapacitated <em>being
            unable to do things or make decisions due to physical or mental
            issues. It could be temporary or permanent, caused by illness,
            injury, or other conditions. In a legal context, it might lead to
            appointing someone to help with decision-making when a person can't
            do it themselves.</em>",

            'If you are considering disinheriting (<em>not leaving any of your possessions</em>) to your spouse and child, acknowledge that you understand the following',
        ];
    }

    public function render()
    {
        return view('livewire.account.organiser.online-will.pages.interview');
    }
}
