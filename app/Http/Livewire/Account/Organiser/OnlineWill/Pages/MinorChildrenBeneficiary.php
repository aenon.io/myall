<?php

namespace App\Http\Livewire\Account\Organiser\OnlineWill\Pages;

use Livewire\Component;
use Auth;
use App\Models\Child;
use App\Lib\Sharedfunctions;

class MinorChildrenBeneficiary extends Component
{
    public $children = [];
    
    public $id_type;
    
    public function mount(){
        $this->loadData();
    }

    public function loadData(){
        $this->children = [];
        $children = Child::where('user_id', Auth::user()->id)->get();
        if($children->count() > 0){
            foreach($children AS $child){
                $add = true;
                $dob = $child->child_date_of_birth;
                if($dob){
                    $yob = date('Y', strtotime($dob));
                    $cy = date('Y');
                    $age = $cy - $yob;
                    if($age >= 18){
                        $add = false;
                    }
                }
                if($add){
                    $id_type = null;
                    
                    if($child->child_date_of_birth){
                        $id_type = "dob";
                    }
                    elseif($child->child_id_number){
                        $id_type = "rsa_id";
                    }
                    
                    $arr = [
                        'id' => $child->id,
                        'child_name' => $child->child_name,
                        'middle_name' => $child->middle_name,
                        'child_surname' => $child->child_surname,
                        'child_id_number' => $child->child_id_number,
                        'child_date_of_birth' => $child->child_date_of_birth,
                        'id_type' => $id_type,
                    ];
                    $this->children[] = $arr;
                }
            }
        }
        else{
            $this->addChild();
        }
    }

    public function addChild(){
        $arr = [
            'child_name' => '',
            'middle_name' => '',
            'child_surname' => '',
            'child_id_number' => '',
            'id_type' => '',
            'child_date_of_birth' => ''
            
        ];
        $this->children[] = $arr;
    }

    public function getNextPage(){
        $fnc = new Sharedfunctions();
        $next_page = $fnc->getNextWillPage('minor-children-beneficiary');
        if($next_page){
            return redirect('organiser/online-will/'.$next_page);
        }
    }

    public function saveChildren($action = null){
        $has_error = false;
        foreach($this->children AS $child){
            if($child['child_name'] && $child['child_surname'] && ($child['child_id_number'] || $child['child_date_of_birth'])){}
            else{
                $this->addError('chils', $child['child_name'].' was not saved. Please enter all required data.');
                $has_error = true;
            }
        }
        if(!$has_error){
            foreach($this->children AS $child){
                if(isset($child['id'])){
                    $cl = Child::find($child['id']);
                }
                else{
                    $cl = new Child();
                }

                $cl->user_id = Auth::user()->id;
                if($child['child_id_number']){
                    $cl->child_id_number = $child['child_id_number'];
                }
                if($child['child_date_of_birth']){
                    $cl->child_date_of_birth = $child['child_date_of_birth'];
                }
                $cl->child_name = $child['child_name'];
                $cl->middle_name = $child['middle_name'];
                $cl->child_surname = $child['child_surname'];
                $cl->save();
            }
            
            if($action == "go_to_next"){
                $this->getNextPage();
            }
            else{
                $this->loadData();
                $this->dispatchBrowserEvent('data-updated', ['message' => 'Updated successfully.']); 
            }
        }
    } 

    public function render(){
        return view('livewire.account.organiser.online-will.pages.minor-children-beneficiary');
    }
}
