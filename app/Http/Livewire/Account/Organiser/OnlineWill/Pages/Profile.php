<?php

namespace App\Http\Livewire\Account\Organiser\OnlineWill\Pages;

use Livewire\Component;
use Auth;
use App\Models\User;

class Profile extends Component
{
    public $middle_name, $first_name, $surname, $id_number, $passport_number, $date_of_birth, $address, $city, $postal_code, $province, $instructions;
    public $provinces;
    
    public function mount(){
        $this->setValues();
    }

    public function setValues(){
        $usr = Auth::user();
        $this->middle_name = $usr->middle_name;
        $this->first_name = $usr->name;
        $this->surname = $usr->surname;
        $this->id_number = $usr->id_number;
        $this->passport_number = $usr->passport_number;
        $this->date_of_birth = $usr->date_of_birth;
        $this->address = $usr->street_address;
        $this->city = $usr->city;
        $this->postal_code = $usr->postal_code;
        if($usr->province){
            $this->province = $usr->province;
        }
        else{
            $this->province = "";
        }
        $this->instructions = $usr->instructions;
        
        $this->provinces = [
            'Eastern Cape',
            'Free State',
            'Gauteng',
            'KwaZulu Natal',
            'Limpopo',
            'Mpumalanga',
            'Nothern Cape',
            'North West',
            'Western Cape'
        ];
    }

    public function saveProfile($action = null){
        $this->validate([
            'first_name' => 'required',
            'surname' => 'required',
            'id_number' => 'nullable|required_without:passport_number|said', 
            'passport_number' => 'required_without:id_number',
            'date_of_birth' => 'required',
            'address' => 'required',
            'city' => 'required',
            // 'postal_code' => 'required',
            'province' => 'required',
        ]);
        $usr = Auth::user();
        $usr->middle_name = $this->middle_name;
        $usr->name = $this->first_name;
        $usr->surname = $this->surname;
        $usr->id_number = $this->id_number;
        $usr->date_of_birth = $this->date_of_birth;
        $usr->passport_number = $this->passport_number;
        $usr->street_address = $this->address;
        $usr->city = $this->city;
        $usr->postal_code = $this->postal_code;
        $usr->province = $this->province;
        $usr->instructions = $this->instructions;
        $usr->save();
        
        if($action == "go_to_next"){
            return redirect('organiser/online-will/executor');
        }
        $this->dispatchBrowserEvent('data-updated', ['message' => 'Updated successfully.']); 
    }

    public function render(){
        return view('livewire.account.organiser.online-will.pages.profile');
    }
}
