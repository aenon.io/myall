<?php

namespace App\Http\Livewire\Account\Organiser\OnlineWill\Pages;

use Livewire\Component;
use App\Lib\Sharedfunctions;

use Auth;
use App\Models\Inheritance;
use App\Models\IndividualBeneficiary;
use App\Models\OrganisationBeneficiary;

class RemainderInheritances extends Component
{
    public $division_type, $beneficiary_type, $disable_beneficiary_type = false;
    public $individuals = [], $organisations = [];
    public $show_modal = false;
    public $tot_percentage;
    public $provinces, $org_types;

    public $show_inheritance_type = true;
    public $relations;

    public function mount(){
        $this->loadData();

        $this->provinces = [
            'Mpumalanga',
            'Limpopo',
            'Gauteng',
            'North West',
            'Western Cape',
            'Eastern Cape',
            'Kwazulu Natal',
            'Free State',
            'Northen Cape'
        ];
        sort($this->provinces);

        $this->org_types = [
            'charity',
            'church',
            'close corporation',
            'company',
            'education institute',
            'public benefit organisation',
            'religious organisation',
            'trade union',
            'trust',
            'other'
        ];

        $this->relations = [
            'Great Grand Father',
            'Great Grand Mother',

            'Grand Father', 
            'Grand Mother',

            'Father', 
            'Uncle',
            'Step Father',
            'Father In Law',

            'Mother',
            'Aunt',
            'Step Mother',
            'Mother In Law',
            
            'Brother', 
            'Step Brother',
            'Brother In Law',
            
            'Sister',
            'Step Sister',
            'Sister In Law',

            'Cousin',
            'Friend', 

            'Spouse',
            'Partner', 
            'Wife',
            'Husband',

            'Son', 
            'Daughter',
            'Son In Law',
            'Daughter In Law',
            "Son's Partner",
            "Daughter's Partner",
            'Niece', 
            'Nephew',
            'Step Son',
            'Step Daughter',

            'Grand Son', 
            'Grand Daughter',
            /*
            'Great Grand Son',
            'Great Grand Daughter',
            */
            'Other',
        ];
        sort($this->relations);
    }

    public function removeIndividual($id){
        $ind = IndividualBeneficiary::find($id);
        $ind->delete();
        $this->loadData();
    }

    public function removeOrganisation($id){
        $org = OrganisationBeneficiary::find($id);
        $org->delete();
        $this->loadData();
    }

    public function updatedOrganisations($val,$key){
        $this->tot_percentage = 0;
        if($this->division_type == "percentage_shares"){
            if(count($this->individuals) > 0){
                foreach($this->individuals AS $ind){
                    if($ind['percentage']){
                        $per = $ind['percentage'];
                        $this->tot_percentage += (int)$per;
                    }
                }
            }
            foreach($this->organisations AS $org){
                //dd($this->organisations);
                if(isset($org['percentage'])){
                    $per = $org['percentage'];
                    $this->tot_percentage += (int)$per;
                }
            }
        }
        if($this->tot_percentage > 100){
            $key = explode(".", $key);
            $this->organisations[$key[0]][$key[1]] = 0;
            $this->dispatchBrowserEvent('percentage-overload', ['message' => 'Cannot exceed 100% - decrease the inheritance percentage in another section in order to increase the inheritance percentage in this section.']);
        }
    }
    public function updatedIndividuals($val,$key){
        $this->tot_percentage = 0;
        if($this->division_type == "percentage_shares"){
            foreach($this->individuals AS $ind){
                if(isset($ind['percentage'])){
                    $per = $ind['percentage'];
                    $this->tot_percentage += (int)$per;
                }
            }
            foreach($this->organisations AS $org){
                if(isset($org['percentage'])){
                    $per = $org['percentage'];
                    $this->tot_percentage += (int)$per;
                }
            }
        }
        if($this->tot_percentage > 100){
            $key = explode(".", $key);
            $this->individuals[$key[0]][$key[1]] = 0;
            $this->dispatchBrowserEvent('percentage-overload', ['message' => 'Cannot exceed 100% - decrease the inheritance percentage in another section in order to increase the inheritance percentage in this section.']);
        }
    }

    public function getNextPage(){
        $fnc = new Sharedfunctions();
        $next_page = $fnc->getNextWillPage('remainder-inheritance');
        if($next_page){
            return redirect('organiser/online-will/'.$next_page);
        }
    }

    public function redosection(){
        $in = Inheritance::where('user_id', Auth::user()->id)->where('type', 'remainder-inheritance')->first();
        if($in){
            foreach($in->individual_beneficiary AS $ind){
                $ind->delete();
            }
            foreach($in->organisation_beneficiery AS $org){
                $org->delete();
            }
            $in->delete();
            $this->loadData();
            $this->show_inheritance_type = true;
        }
        $this->loadData();
        $this->show_inheritance_type = true;
        return redirect('organiser/online-will/remainder-inheritance');
    }

    public function loadData(){
        $this->individuals = [];
        $this->organisations = [];
        
        $in = Inheritance::where('user_id', Auth::user()->id)->where('type', 'remainder-inheritance')->first();
        if($in){
            $this->division_type = $in->division_type;
            if($this->division_type == "one_beneficiery"){
                $this->disable_beneficiary_type = true;
            }
            
            foreach($in->individual_beneficiary AS $ind){
                $this->show_inheritance_type = false;
                $id_type = null;
                if(strlen($ind->id_number) == 13){
                    $id_type = "rsa_id";
                }
                else{
                    $id_type = "dob";
                }
                $arr = [
                    'id' => $ind->id,
                    'first_name' => $ind->first_name,
                    'middle_name' => $ind->middle_name,
                    'surname' => $ind->surname,
                    'id_number' => $ind->id_number,
                    'percentage' => $ind->percentage,
                    'id_type' => $id_type,
                    'relation' => $ind->relation,
                    'specify_relation' => $ind->specify_relation,
                ];
                $this->individuals[] = $arr;
            }
            foreach($in->organisation_beneficiery AS $org){
                $this->show_inheritance_type = false;
                $arr = [
                    'id' => $org->id,
                    'organisation_name' => $org->organisation_name,
                    'registration_number' => $org->registration_number,
                    'address' => $org->address,
                    'city' => $org->city,
                    'postal_code' => $org->postal_code,
                    'province' => $org->province,
                    'organisation_type' => $org->organisation_type,
                    'percentage' => $org->percentage,
                    'organisation_other_type' => $org->organisation_other_type,
                ];
                $this->organisations[] = $arr;
            }
        }
        else{
            $this->show_modal = true;
        }
    }

    public function updatedDivisionType(){
        $this->individuals = [];
        $this->organisations = [];
        if($this->division_type == "percentage_shares"){
            $this->dispatchBrowserEvent('show-percentage-shares-modal');
        }
    }

    public function updatedBeneficiaryType(){
        $this->individuals = [];
        $this->organisations = [];
        if($this->beneficiary_type == "individual"){
            $this->addIndividual();
        }
        elseif($this->beneficiary_type == "organisation"){
            $this->addOrganisation();
        }
    }

    public function addIndividual(){
        $arr = [
            'first_name' => '',
            'middle_name' => '',
            'surname' => '',
            'id_number' => '',
            'percentage' => '',
            'id_type' => 'rsa_id',
            'relation' => '',
            'specify_relation' => '',
        ];
        $this->individuals[] = $arr;
    }

    public function addOrganisation(){
        $arr = [
            'organisation_name' => '',
            'registration_number' => '',
            'address' => '',
            'city' => '',
            'postal_code' => '',
            'province' => '',
            'organisation_type' => '',
            'percentage' => '',
            'organisation_other_type' => '',
        ];
        $this->organisations[] = $arr;
    }

    public function saveInheritance($action = null){
        $this->validate([
            'division_type' => 'required'
        ]);

        $has_error = false;

        if(count($this->individuals) == 0 && count($this->organisations) == 0){
            $has_error = true;
            $this->addError("data", "Please enter all required data");
        }

        foreach($this->individuals AS $ind){
            if($ind['first_name'] && $ind['surname'] && $ind['id_number'] && $ind['relation']){}
            else{
                $has_error = true;
                $this->addError("data", "Please enter all required data");
            }

            if($ind['relation'] == "Other"){
                if($ind['specify_relation'] == ""){
                    $has_error = true;
                    $this->addError("data", "Please enter all required data");
                }
            }
        }
        foreach($this->organisations AS $org){
            if($org['organisation_name'] && $org['province'] && $org['organisation_type']){}
            else{
                $has_error = true;
                $this->addError("data", "Please enter all required data");
            }
            if($org['organisation_type'] == "other"){
                if($org['organisation_other_type'] == ""){
                    $has_error = true;
                    $this->addError("data", "Please enter all required data");
                }
            }
        }

        if($this->division_type == "equal_part" || $this->division_type == "percentage_shares"){
            $count = 0;
            $count += count($this->individuals);
            $count += count($this->organisations);
            if($count < 2){
                $has_error = true;
                $this->dispatchBrowserEvent('percentage-overload', ['message' => 'Please enter two or more beneficiaries.']);
            }
        }

        if(!$has_error){
            if($this->checkPercentage()){
                $in = Inheritance::where('user_id', Auth::user()->id)->where('type', 'remainder-inheritance')->first();
                if(!$in){
                    $in = new Inheritance();
                }

                $in->user_id = Auth::user()->id;
                $in->type = "remainder-inheritance";
                $in->division_type = $this->division_type;
                $in->save();

                foreach($this->individuals AS $ind){
                    if($ind['first_name'] && $ind['surname'] && $ind['id_number']){
                        if(isset($ind['id'])){
                            $ind_ben = IndividualBeneficiary::find($ind['id']);
                        }
                        else{
                            $ind_ben = new IndividualBeneficiary();
                        }
                        $ind_ben->user_id = Auth::user()->id;
                        $ind_ben->inheritance_id = $in->id;
                        $ind_ben->first_name = $ind['first_name'];
                        $ind_ben->middle_name = $ind['middle_name'];
                        $ind_ben->surname = $ind['surname'];
                        $ind_ben->id_number = $ind['id_number'];
                        $ind_ben->percentage = $ind['percentage'];
                        $ind_ben->relation = $ind['relation'];
                        $ind_ben->specify_relation = $ind['specify_relation'];
                        $ind_ben->save();
                    }
                }
                foreach($this->organisations AS $org){
                    if($org['organisation_name']){
                        if(isset($org['id'])){
                            $org_ben = OrganisationBeneficiary::find($org['id']);
                        }
                        else{
                            $org_ben = new OrganisationBeneficiary();
                        }
                        $org_ben->user_id = Auth::user()->id;
                        $org_ben->inheritance_id = $in->id;
                        $org_ben->organisation_name = $org['organisation_name'];
                        $org_ben->registration_number = $org['registration_number'];
                        $org_ben->address = $org['address'];
                        $org_ben->city = $org['city'];
                        $org_ben->postal_code = $org['postal_code'];
                        $org_ben->province = $org['province'];
                        $org_ben->organisation_type = $org['organisation_type'];
                        $org_ben->organisation_other_type = $org['organisation_other_type'];
                        $org_ben->percentage = $org['percentage'];
                        $org_ben->save();
                    }
                }
                if($action == "go_to_next"){
                    $this->getNextPage();
                }
                else{
                    $this->loadData();
                    $this->dispatchBrowserEvent('data-updated', ['message' => 'Updated successfully.']); 
                }
            }
        }
    }

    public function checkPercentage(){
        $this->tot_percentage = 0;
        if($this->division_type == "percentage_shares"){
            foreach($this->individuals AS $ind){
                if(isset($ind['percentage'])){
                    if($ind['percentage'] == 0 || $ind['percentage'] == ""){
                        $this->dispatchBrowserEvent('percentage-overload', ['message' => 'Please make sure all percentages have a value greater than 0.']);
                        return false;
                    }
                    $per = $ind['percentage'];
                    $this->tot_percentage += (int)$per;
                }
            }
            foreach($this->organisations AS $org){
                if(isset($org['percentage'])){
                    if($per = $org['percentage'] == 0 || $per = $org['percentage'] == ""){
                        $this->dispatchBrowserEvent('percentage-overload', ['message' => 'Please make sure all percentages have a value greater than 0.']);
                        return false;
                    }
                    $per = $org['percentage'];
                    $this->tot_percentage += (int)$per;
                }
            }
            if($this->tot_percentage > 100){
                $this->dispatchBrowserEvent('percentage-overload', ['message' => 'Cannot exceed 100% - decrease the inheritance percentage in another section in order to increase the inheritance percentage in this section.']);
                return false;
            }
            if($this->tot_percentage < 100){
                $this->dispatchBrowserEvent('percentage-overload', ['message' => 'Must be 100% - Increase other sections to reach a maximum of 100%.']);
                return false;
            }
        }
        return true;
    }

    public function render(){
        return view('livewire.account.organiser.online-will.pages.remainder-inheritances');
    }
}
