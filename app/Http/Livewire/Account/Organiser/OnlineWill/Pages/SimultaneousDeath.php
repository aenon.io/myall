<?php

namespace App\Http\Livewire\Account\Organiser\OnlineWill\Pages;

use Livewire\Component;

use Auth;
use App\Models\Inheritance;
use App\Lib\Sharedfunctions;
use App\Models\IndividualBeneficiary;
use App\Models\OrganisationBeneficiary;
use Illuminate\Support\Facades\Log;

class SimultaneousDeath extends Component
{
    protected $listeners = ['saveInheritance', 'saveSpecificInheritance'];

    public $view_page;
    public $inheritances;
    public $specific_inheritances = [];
    public $relations, $provinces, $org_types;
    public $division_type, $beneficiary_type;
    public $benefiaciaries = [];
    public $show_options = true, $show_redo_btn = false;

    public function mount(){
        $this->view_page = "start";
        $this->setInitData();
        $this->loadData();
    }

    public function updatedDivisionType(){
        if($this->division_type == "percentage_shares"){
            $this->dispatchBrowserEvent('show-percentage-shares-modal');
        }
    }

    public function clearInheritance(){
        // dd("HERE");
        $inhs = Inheritance::where('user_id', Auth::user()->id)->where('type', 'simultaneous-death')->get();
        foreach($inhs AS $inh){
            foreach($inh->individual_beneficiary AS $ind){
                $ind->delete();
            }
            foreach($inh->organisation_beneficiery AS $org){
                $org->delete();
            }
            $inh->delete();
        }
        $this->view_page = "start";
        $this->specific_inheritances = [];
        $this->division_type = null;
        $this->beneficiary_type = null;
        $this->benefiaciaries = [];
        $this->show_options = true;
        $this->show_redo_btn = false;

        $this->setInitData();
        $this->loadData();
        return redirect('organiser/online-will/simultaneous-death');
    }

    public function removeBeneficiary($type, $id){
        if($type == "individual"){
            $ind = IndividualBeneficiary::find($id);
            $ind->delete();
        }
        if($type == "organisation"){
            $org = OrganisationBeneficiary::find($id);
            $org->delete();
        }
        $tmp_view_page = $this->view_page;
        $this->loadData();
        $this->view_page = $tmp_view_page;

    }

    public function getNextPage(){
        $fnc = new Sharedfunctions();
        $next_page = $fnc->getNextWillPage('simultaneous-death');
        if($next_page){
            return redirect('organiser/online-will/'.$next_page);
        }
    }

    public function saveInheritance($next = null){
        $has_error = false;
        if($this->division_type == "equal_part" || $this->division_type == "percentage_shares"){
            if(count($this->benefiaciaries) < 2){
                $has_error = true;
                $this->dispatchBrowserEvent('error_on_page', ['message' => "Please enter at least two beneficiaries."]);
            }
        }
        elseif(count($this->benefiaciaries) == 0){
            $has_error = true;
            $this->dispatchBrowserEvent('error_on_page', ['message' => "Please enter at least one beneficiary."]);
        }
        $tot_percentage = 0;
        foreach($this->benefiaciaries AS $ben){
            if($ben['type'] == "individual"){
                if($ben['name'] == "" || $ben['surname'] == "" || $ben['id_type'] == "" || $ben['relation'] == ""){
                    $this->dispatchBrowserEvent('error_on_page', ['message' => "Please enter all required data"]);
                    $has_error = true;
                }
                if($ben['id_type'] == "rsa_id"){
                    if($ben['id_number'] == ""){
                        $this->dispatchBrowserEvent('error_on_page', ['message' => "Please enter all required data."]);
                        $has_error = true;
                    }
                }
                if($ben['id_type'] == "dob"){
                    if($ben['date_of_birth'] == ""){
                        $this->dispatchBrowserEvent('error_on_page', ['message' => "Please enter all required data."]);
                        $has_error = true;
                    }
                }
                if($ben['relation'] == "Other"){
                    if($ben['specify_relation'] == ""){
                        $has_error = true;
                        $this->dispatchBrowserEvent('error_on_page', ['message' => "Please enter all required data."]);
                    }
                }
            }
            if($ben['type'] == "organisation"){
                if($ben['organisation_name'] == "" || $ben['org_type'] == "" || $ben['province'] == ""){
                    $this->dispatchBrowserEvent('error_on_page', ['message' => "Please enter all required data."]);
                    $has_error = true;
                }
                if($ben['org_type'] == "other"){
                    if($ben['specify_organisation_type'] == ""){
                        $this->dispatchBrowserEvent('error_on_page', ['message' => "Please enter all required data."]);
                        $has_error = true;
                    }
                }
                if($ben['org_type'] == "other"){
                    if($ben['specify_organisation_type'] == ""){
                        $has_error = true;
                        $this->dispatchBrowserEvent('error_on_page', ['message' => "Please enter all required data."]);
                    }
                }
            }
            if($this->division_type == "percentage_shares"){
                if($ben['percentage'] == "" || $ben['percentage'] == 0){
                    $this->dispatchBrowserEvent('error_on_page', ['message' => "Percentage must be greater than 0."]);
                    $has_error = true;
                }
                else{
                    $tot_percentage += $ben['percentage'];
                }
            }
        }
        if($this->division_type == "percentage_shares"){
            if($tot_percentage != 100){
                $this->dispatchBrowserEvent('error_on_page', ['message' => "Percentages must equal to 100%."]);
                $has_error = true;
            }
        }
        if(!$has_error){
            $inh = null;
            $type = null;
            if($this->view_page == "remainder_inheritance"){
                $inh = Inheritance::where('user_id', Auth::user()->id)->where('type', 'simultaneous-death')->where('sub_type', 'remainder-inheritance')->first();
                $type = 'remainder-inheritance';
            }
            elseif($this->view_page == "inheritance"){
                $inh = Inheritance::where('user_id', Auth::user()->id)->where('type', 'simultaneous-death')->where('sub_type', 'inheritance')->first();
                $type = 'inheritance';
            }
            if(!$inh){
                $inh = new Inheritance();
            }
            $inh->user_id = Auth::user()->id;
            $inh->type = "simultaneous-death";
            $inh->sub_type = $type;
            $inh->division_type = $this->division_type;
            $inh->save();

            foreach($this->benefiaciaries AS $ben){
                if($ben['type'] == "individual"){
                    if(isset($ben['id'])){
                        $ind_ben = IndividualBeneficiary::find($ben['id']);
                    }
                    else{
                        $ind_ben = new IndividualBeneficiary();
                    }

                    $id_number = null;
                    if($ben['id_type'] == "rsa_id"){
                        $id_number = $ben['id_number'];
                    }
                    if($ben['id_type'] == "dob"){
                        $id_number = $ben['date_of_birth'];
                    }

                    $ind_ben->user_id = Auth::user()->id;
                    $ind_ben->inheritance_id = $inh->id;
                    $ind_ben->first_name = $ben['name'];
                    $ind_ben->middle_name = $ben['middle_name'];
                    $ind_ben->surname = $ben['surname'];
                    $ind_ben->id_number = $id_number;
                    $ind_ben->relation = $ben['relation'];
                    $ind_ben->specify_relation = $ben['specify_relation'];
                    $ind_ben->percentage = $ben['percentage'];
                    $ind_ben->save();
                }
                if($ben['type'] == "organisation"){
                    $org_ben = null;
                    if(isset($ben['id'])){
                        $org_ben = OrganisationBeneficiary::find($ben['id']);
                    }
                    else{
                        $org_ben = new OrganisationBeneficiary();
                    }

                    $org_ben->user_id = Auth::user()->id;
                    $org_ben->inheritance_id = $inh->id;
                    $org_ben->organisation_name = $ben['organisation_name'];
                    $org_ben->registration_number = $ben['reg_number'];
                    $org_ben->address = $ben['address'];
                    $org_ben->city = $ben['city'];
                    $org_ben->postal_code = $ben['postal_code'];
                    $org_ben->province = $ben['province'];
                    $org_ben->organisation_type = $ben['org_type'];
                    $org_ben->organisation_other_type = $ben['specify_organisation_type'];
                    $org_ben->percentage = $ben['percentage'];
                    $org_ben->save();
                }
            }
            if($next){
                $this->getNextPage();
            }
            else{
                $this->loadData();
                if($type == "remainder-inheritance"){
                    $this->view_page = "remainder_inheritance";
                }
                $this->dispatchBrowserEvent('data-updated', ['message' => 'Updated successfully.']);
            }
        }
    }

    public function updatedBeneficiaryType(){
        $this->benefiaciaries = [];
        if($this->beneficiary_type == "individual"){
            $this->addIndividual();
        }
        if($this->beneficiary_type == "organisation"){
            $this->addOrganisation();
        }
    }

    public function addIndividual(){
        $arr = [
            'type' => 'individual',
            'name' => "",
            'middle_name' => "",
            'surname' => "",
            'id_type' => "",
            'date_of_birth' => "",
            'id_number' => "",
            'relation' => "",
            'specify_relation' => '',
            'percentage' => ""
        ];
        $this->benefiaciaries[] = $arr;
    }

    public function addOrganisation(){
        $arr = [
            'type' => "organisation",
            'organisation_name' => "",
            'reg_number' => "",
            'address' => "",
            'city' => "",
            'postal_code' => "",
            'province' => "",
            'org_type' => "",
            'specify_organisation_type' => "",
            'percentage' => ""
        ];
        $this->benefiaciaries[] = $arr;
    }

    public function saveSpecificInheritance($next = null){
        $has_error = false;
        foreach($this->specific_inheritances AS $sp){
            if($sp['beneficiary_type'] == "individual"){
                if($sp['name'] == "" || $sp['surname'] == "" || $sp['id_type'] == "" || $sp['description'] == "" || $sp['relation'] == ""){
                    $this->dispatchBrowserEvent('error_on_page', ['message' => "Please enter all required data."]);
                    $has_error = true;
                }
                if($sp['id_type'] == "rsa_id"){
                    if($sp['id_number'] == ""){
                        $this->dispatchBrowserEvent('error_on_page', ['message' => "Please enter all required data."]);
                        $has_error = true;
                    }
                }
                if($sp['id_type'] == "dob"){
                    if($sp['date_of_birth'] == ""){
                        $this->dispatchBrowserEvent('error_on_page', ['message' => "Please enter all required data."]);
                        $has_error = true;
                    }
                }
                if($sp['relation'] == "Other"){
                    if($sp['specify_relation'] == ""){
                        $has_error = true;
                        $this->dispatchBrowserEvent('error_on_page', ['message' => "Please enter all required data."]);
                    }
                }
            }
            if($sp['beneficiary_type'] == "organisation"){
                if($sp['organisation_name'] == "" || $sp['org_type'] == "" || $sp['province'] == ""){
                    $this->dispatchBrowserEvent('error_on_page', ['message' => "Please enter all required data."]);
                    $has_error = true;
                }
                if($sp['org_type'] == "other"){
                    if($sp['specify_organisation_type'] == ""){
                        $this->dispatchBrowserEvent('error_on_page', ['message' => "Please enter all required data."]);
                        $has_error = true;
                    }
                }
                if($sp['org_type'] == "other"){
                    if($sp['specify_organisation_type'] == ""){
                        $has_error = true;
                        $this->dispatchBrowserEvent('error_on_page', ['message' => "Please enter all required data."]);
                    }
                }
            }
        }
        if(!$has_error){
            $inh = Inheritance::where('user_id', Auth::user()->id)->where('type', 'simultaneous-death')->where('sub_type', 'specific-inheritance')->first();
            if(!$inh){
                $inh = new Inheritance();
            }

            $inh->user_id = Auth::user()->id;
            $inh->type = "simultaneous-death";
            $inh->sub_type = "specific-inheritance";
            $inh->save();

            foreach($this->specific_inheritances AS $sp){
                if($sp['beneficiary_type'] == "individual"){
                    $ind = null;
                    if(isset($sp['id'])){
                        $ind = IndividualBeneficiary::find($sp['id']);
                    }
                    if(!$ind){
                        $ind = new IndividualBeneficiary();
                    }
                    $id_number = null;
                    if($sp['id_type'] == "rsa_id"){
                        $id_number = $sp['id_number'];
                    }
                    if($sp['id_type'] == "dob"){
                        $id_number = $sp['date_of_birth'];
                    }

                    $ind->user_id = Auth::user()->id;
                    $ind->inheritance_id = $inh->id;
                    $ind->first_name = $sp['name'];
                    $ind->middle_name = $sp['middle_name'];
                    $ind->surname = $sp['surname'];
                    $ind->id_number = $id_number;
                    $ind->relation = $sp['relation'];
                    $ind->specify_relation = $sp['specify_relation'];
                    $ind->description = $sp['description'];
                    $ind->save();
                }
                if($sp['beneficiary_type'] == "organisation"){
                    $org = null;
                    if(isset($sp['id'])){
                        $org = OrganisationBeneficiary::find($sp['id']);
                    }
                    if(!$org){
                        $org = new OrganisationBeneficiary();
                    }
                    $org->user_id = Auth::user()->id;
                    $org->inheritance_id = $inh->id;
                    $org->organisation_name = $sp['organisation_name'];
                    $org->registration_number = $sp['reg_number'];
                    $org->address = $sp['address'];
                    $org->city = $sp['city'];
                    $org->postal_code = $sp['postal_code'];
                    $org->province = $sp['province'];
                    $org->organisation_type = $sp['org_type'];
                    $org->organisation_other_type = $sp['specify_organisation_type'];
                    $org->description = $sp['description'];
                    $org->save();
                }
            }
            $this->loadData();
            if($next){
                $this->view_page = "remainder_inheritance";
            }
            else{
                $this->dispatchBrowserEvent('data-updated', ['message' => 'Updated successfully.']);
            }
        }
    }

    public function showInheritance($type){
        /* Log::info('Emitting setPageView event with type: ' . $type); */
        $this->emitTo('account.organiser.partials.steps', 'setPageView' ,$type);
        $this->view_page = $type;
        if($type == "specific_inheritance"){
            $this->addSpecificInheritance();
        }
    }

    public function loadData(){
        $inhs = Inheritance::where('user_id', Auth::user()->id)->where('type', 'simultaneous-death')->where('sub_type', 'specific-inheritance')->first();
        if($inhs){
            $this->show_redo_btn = true;
            $this->benefiaciaries = [];
            $this->specific_inheritances = [];

            $this->view_page = "specific_inheritance";
            foreach($inhs->individual_beneficiary AS $ind){
                $id_type = null;
                $dob = null;
                $id_number = null;
                if(strlen($ind->id_number) == 13){
                    $id_type = "rsa_id";
                    $id_number = $ind->id_number;
                }
                else{
                    $id_type = "dob";
                    $dob = $ind->id_number;
                }
                $arr = [
                    "id" => $ind['id'],
                    "inheritance_type" => "specific_inheritance",
                    "beneficiary_type" => "individual",

                    "name" => $ind['first_name'],
                    "middle_name" => $ind['middleware'],
                    "surname" => $ind['surname'],
                    "id_type" => $id_type,
                    "date_of_birth" => $dob,
                    "id_number" => $id_number,
                    "relation" => $ind['relation'],
                    "specify_relation" => $ind['specify_relation'],
                    "description" => $ind['description']
                ];
                $this->specific_inheritances[] = $arr;
            }
            foreach($inhs->organisation_beneficiery AS $org){
                $arr = [
                    "id" => $org['id'],
                    "inheritance_type" => "specific_inheritance",
                    "beneficiary_type" => "organisation",

                    "organisation_name" => $org['organisation_name'],
                    "reg_number" => $org['registration_number'],
                    "address" => $org['address'],
                    "city" => $org['city'],
                    "postal_code" => $org['postal_code'],
                    "province" => $org['province'],
                    "org_type" => $org['organisation_type'],
                    "specify_organisation_type" => $org['organisation_other_type'],
                    "description" => $org['description']
                ];
                $this->specific_inheritances[] = $arr;
            }

            $rem_inhs = Inheritance::where('user_id', Auth::user()->id)->where('type', 'simultaneous-death')->where('sub_type', 'remainder-inheritance')->first();
            if($rem_inhs){
                $this->show_options = false;
                $this->division_type = $rem_inhs->division_type;
                foreach($rem_inhs->individual_beneficiary AS $ind){
                    $id_type = null;
                    $dob = null;
                    $id_number = null;
                    if(strlen($ind->id_number) == 13){
                        $id_type = "rsa_id";
                        $id_number = $ind->id_number;
                    }
                    else{
                        $id_type = "dob";
                        $dob = $ind->id_number;
                    }
                    $arr = [
                        "id" => $ind['id'],
                        'type' => 'individual',
                        'name' => $ind['first_name'],
                        'middle_name' => $ind['middle_name'],
                        'surname' => $ind['surname'],
                        'id_type' => $id_type,
                        'date_of_birth' => $dob,
                        'id_number' => $id_number,
                        'relation' => $ind['relation'],
                        'specify_relation' => $ind['specify_relation'],
                        'percentage' => $ind['percentage']
                    ];
                    $this->benefiaciaries[] = $arr;
                }
                foreach($rem_inhs->organisation_beneficiery AS $org){
                    $arr = [
                        "id" => $org['id'],
                        'type' => "organisation",
                        'organisation_name' => $org['organisation_name'],
                        'reg_number' => $org['registration_number'],
                        'address' => $org['address'],
                        'city' => $org['city'],
                        'postal_code' => $org['postal_code'],
                        'province' => $org['province'],
                        'org_type' => $org['organisation_type'],
                        'specify_organisation_type' => $org['organisation_other_type'],
                        'percentage' => $org['percentage']
                    ];
                    $this->benefiaciaries[] = $arr;
                }
            }
        }
        else{
            $inhs = Inheritance::where('user_id', Auth::user()->id)->where('type', 'simultaneous-death')->where('sub_type', 'inheritance')->first();
            if($inhs){
                $this->view_page = "inheritance";
                $this->division_type = $inhs->division_type;

                $this->benefiaciaries = [];
                $this->show_options = false;
                $this->show_redo_btn = true;

                foreach($inhs->individual_beneficiary AS $ind){
                    $id_type = null;
                    $dob = null;
                    $id_number = null;
                    if(strlen($ind->id_number) == 13){
                        $id_type = "rsa_id";
                        $id_number = $ind->id_number;
                    }
                    else{
                        $id_type = "dob";
                        $dob = $ind->id_number;
                    }
                    $arr = [
                        "id" => $ind['id'],
                        'type' => 'individual',
                        'name' => $ind['first_name'],
                        'middle_name' => $ind['middle_name'],
                        'surname' => $ind['surname'],
                        'id_type' => $id_type,
                        'date_of_birth' => $dob,
                        'id_number' => $id_number,
                        'relation' => $ind['relation'],
                        'specify_relation' => $ind['specify_relation'],
                        'percentage' => $ind['percentage']
                    ];
                    $this->benefiaciaries[] = $arr;
                }
                foreach($inhs->organisation_beneficiery AS $org){
                    $arr = [
                        "id" => $org['id'],
                        'type' => "organisation",
                        'organisation_name' => $org['organisation_name'],
                        'reg_number' => $org['registration_number'],
                        'address' => $org['address'],
                        'city' => $org['city'],
                        'postal_code' => $org['postal_code'],
                        'province' => $org['province'],
                        'org_type' => $org['organisation_type'],
                        'specify_organisation_type' => $org['organisation_other_type'],
                        'percentage' => $org['percentage']
                    ];
                    $this->benefiaciaries[] = $arr;
                }

            }
        }
    }

    public function addSpecificInheritance(){
        $arr = [
            "inheritance_type" => "specific_inheritance",
            "beneficiary_type" => "",

            "name" => "",
            "middle_name" => "",
            "surname" => "",
            "id_type" => "",
            "date_of_birth" => "",
            "id_number" => "",
            "relation" => "",
            "specify_relation" => "",

            "organisation_name" => "",
            "reg_number" => "",
            "address" => "",
            "city" => "",
            "postal_code" => "",
            "province" => "",
            "org_type" => "",
            "specify_organisation_type" => "",

            "description" => ""
        ];
        $this->specific_inheritances[] = $arr;
    }


    public function setInitData(){
        $this->inheritances = Inheritance::where('user_id', Auth::user()->id)->where('type', '<>', 'simultaneous-death')->orderBy('type', 'ASC')->get();
        $this->org_types = [
            'charity',
            'church',
            'close corporation',
            'company',
            'education institute',
            'public benefit organisation',
            'religious organisation',
            'trade union',
            'trust',
            'other'
        ];
        $this->provinces = [
            'Mpumalanga',
            'Limpopo',
            'Gauteng',
            'North West',
            'Western Cape',
            'Eastern Cape',
            'Kwazulu Natal',
            'Free State',
            'Northen Cape'
        ];
        sort($this->provinces);
        $this->relations = [
            'Great Grand Father',
            'Great Grand Mother',

            'Grand Father',
            'Grand Mother',

            'Father',
            'Uncle',
            'Step Father',
            'Father In Law',

            'Mother',
            'Aunt',
            'Step Mother',
            'Mother In Law',

            'Brother',
            'Step Brother',
            'Brother In Law',

            'Sister',
            'Step Sister',
            'Sister In Law',

            'Cousin',
            'Friend',

            'Spouse',
            'Partner',
            'Wife',
            'Husband',

            'Son',
            'Daughter',
            'Son In Law',
            'Daughter In Law',
            "Son's Partner",
            "Daughter's Partner",
            'Niece',
            'Nephew',
            'Step Son',
            'Step Daughter',

            'Grand Son',
            'Grand Daughter',
            /*
            'Great Grand Son',
            'Great Grand Daughter',
            */
            'Other',
        ];
        sort($this->relations);
    }

    public function render(){
        return view('livewire.account.organiser.online-will.pages.simultaneous-death');
    }
}
