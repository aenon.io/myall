<?php

namespace App\Http\Livewire\Account\Organiser\OnlineWill\Pages\SimultaneousDeath;

use Livewire\Component;

use Auth;
use App\Models\Inheritance AS InheritanceModel;
use App\Models\IndividualBeneficiary;
use App\Models\OrganisationBeneficiary;
use App\Lib\Sharedfunctions;

class Inheritance extends Component
{
    public $division_type, $beneficiary_type;
    public $individuals = [], $organisations = [];
    public $tot_percentage;

    public function mount(){
        $this->loadData();
    }

    public function updatedOrganisations($val,$key){
        $this->tot_percentage = 0;
        if($this->division_type == "percentage_shares"){
            foreach($this->individuals AS $ind){
                if(isset($ind['percentage'])){
                    $per = $ind['percentage'];
                    $this->tot_percentage += (int)$per;
                }
            }
            foreach($this->organisations AS $org){
                if(isset($org['percentage'])){
                    $per = $org['percentage'];
                    $this->tot_percentage += (int)$per;
                }
            }
        }
        if($this->tot_percentage > 100){
            $key = explode(".", $key);
            $this->individuals[$key[0]][$key[1]] = 0;
            $this->dispatchBrowserEvent('percentage-overload', ['message' => 'Cannot exceed 100% - decrease the inheritance percentage in another section in order to increase the inheritance percentage in this section.']);
        }
    }
    public function updatedIndividuals($val,$key){
        $this->tot_percentage = 0;
        if($this->division_type == "percentage_shares"){
            foreach($this->individuals AS $ind){
                if(isset($ind['percentage'])){
                    $per = $ind['percentage'];
                    $this->tot_percentage += (int)$per;
                }
            }
            foreach($this->organisations AS $org){
                if(isset($org['percentage'])){
                    $per = $org['percentage'];
                    $this->tot_percentage += (int)$per;
                }
            }
        }
        if($this->tot_percentage > 100){
            $key = explode(".", $key);
            $this->individuals[$key[0]][$key[1]] = 0;
            $this->dispatchBrowserEvent('percentage-overload', ['message' => 'Cannot exceed 100% - decrease the inheritance percentage in another section in order to increase the inheritance percentage in this section.']);
        }
    }

    public function loadData(){
        $in = InheritanceModel::where('user_id', Auth::user()->id)->where('type', 'simultaneous-death')->where('sub_type', 'inheritance')->first();
        if($in){
            $this->individuals = [];
            $this->organisations = [];
            $this->division_type = $in->division_type;
            foreach($in->individual_beneficiary AS $ind){
                $arr = [
                    'id' => $ind->id,
                    'first_name' => $ind->first_name,
                    'middle_name' => $ind->middle_name,
                    'surname' => $ind->surname,
                    'id_number' => $ind->id_number,
                    'percentage' => $ind->percentage,
                    'id_type' => '',
                ];
                $this->individuals[] = $arr;
            }
            foreach($in->organisation_beneficiery AS $org){
                $arr = [
                    'id' => $org->id,
                    'organisation_name' => $org->organisation_name,
                    'registration_number' => $org->registration_number,
                    'address' => $org->address,
                    'city' => $org->city,
                    'postal_code' => $org->postal_code,
                    'province' => $org->province,
                    'organisation_type' => $org->organisation_type,
                    'percentage' => $org->percentage
                ];
                $this->organisations[] = $arr;
            }
        }
    }

    public function updatedDivisionType(){
        $this->individuals = [];
        $this->organisations = [];
    }

    public function updatedBeneficiaryType(){
        $this->individuals = [];
        $this->organisations = [];
        if($this->beneficiary_type == "individual"){
            $this->addIndividual();
        }
        elseif($this->beneficiary_type == "organisation"){
            $this->addOrganisation();
        }
    }

    public function addIndividual(){
        $arr = [
            'first_name' => '',
            'middle_name' => '',
            'surname' => '',
            'id_number' => '',
            'percentage' => 0,
            'id_type' => "dob"
        ];
        $this->individuals[] = $arr;
    }

    public function addOrganisation(){
        $arr = [
            'organisation_name' => '',
            'registration_number' => '',
            'address' => '',
            'city' => '',
            'postal_code' => '',
            'province' => '',
            'organisation_type' => '',
            'percentage' => 0
        ];
        $this->organisations[] = $arr;
    }

    public function saveInheritance($action = null){
        $this->validate([
            'division_type' => 'required'
        ]);

        $in = InheritanceModel::where('user_id', Auth::user()->id)->where('type', 'simultaneous-death')->where('sub_type', 'inheritance')->first();
        if(!$in){
            $in = new InheritanceModel();
        }

        $in->user_id = Auth::user()->id;
        $in->type = "simultaneous-death";
        $in->sub_type = 'inheritance';
        $in->division_type = $this->division_type;
        $in->save();

        foreach($this->individuals AS $ind){
            if($ind['first_name'] && $ind['surname'] && $ind['id_number']){
                if(isset($ind['id'])){
                    $ind_ben = IndividualBeneficiary::find($ind['id']);
                }
                else{
                    $ind_ben = new IndividualBeneficiary();
                }
                $ind_ben->user_id = Auth::user()->id;
                $ind_ben->inheritance_id = $in->id;
                $ind_ben->first_name = $ind['first_name'];
                $ind_ben->middle_name = $ind['middle_name'];
                $ind_ben->surname = $ind['surname'];
                $ind_ben->id_number = $ind['id_number'];
                $ind_ben->percentage = $ind['percentage'];
                $ind_ben->save();
            }
        }
        foreach($this->organisations AS $org){
            if($org['organisation_name']){
                if(isset($org['id'])){
                    $org_ben = OrganisationBeneficiary::find($org['id']);
                }
                else{
                    $org_ben = new OrganisationBeneficiary();
                }
                $org_ben->user_id = Auth::user()->id;
                $org_ben->inheritance_id = $in->id;
                $org_ben->organisation_name = $org['organisation_name'];
                $org_ben->registration_number = $org['registration_number'];
                $org_ben->address = $org['address'];
                $org_ben->city = $org['city'];
                $org_ben->postal_code = $org['postal_code'];
                $org_ben->province = $org['province'];
                $org_ben->organisation_type = $org['organisation_type'];
                $org_ben->percentage = $org['percentage'];
                $org_ben->save();
            }
        }
        $this->loadData();
        if($action == "go_to_next"){
            $this->getNextPage();
        }
        else{
            $this->dispatchBrowserEvent('data-updated', ['message' => 'Updated successfully.']);
        }
    }
    
    public function getNextPage(){
        $fnc = new Sharedfunctions();
        $next_page = $fnc->getNextWillPage('simultaneous-death');
        if($next_page){
            return redirect('organiser/online-will/'.$next_page);
        }
    }

    public function render(){
        return view('livewire.account.organiser.online-will.pages.simultaneous-death1.inheritance');
    }
}
