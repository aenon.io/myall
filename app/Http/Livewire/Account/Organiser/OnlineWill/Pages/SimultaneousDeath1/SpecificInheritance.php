<?php

namespace App\Http\Livewire\Account\Organiser\OnlineWill\Pages\SimultaneousDeath;

use Livewire\Component;

use Auth;
use App\Models\Inheritance;
use App\Models\IndividualBeneficiary;
use App\Models\OrganisationBeneficiary;
use App\Lib\Sharedfunctions;

class SpecificInheritance extends Component
{
    
    public $beneficiary_type;
    public $first_name, $middle_name, $surname, $id_number, $relation;
    public $organisation_name, $registration_number, $address, $city, $postal_code, $province; 
    public $description, $value;

    public function mount(){
        $in = Inheritance::where('user_id', Auth::user()->id)->where('type', 'simultaneous-death')->where('sub_type', 'specific-inheritance')->first();
        if($in){
            $ind = $in->individual_beneficiary->first();
            if($ind){
                $this->beneficiary_type = "individual";
                $this->first_name = $ind->first_name; 
                $this->middle_name = $ind->middle_name;
                $this->surname = $ind->surname;
                $this->id_number = $ind->id_number;
                $this->relation = $ind->relation;
                $this->value = $ind->value;
                $this->description = $ind->description;
            }
            $org = $in->organisation_beneficiery->first();
            if($org){
                $this->beneficiary_type = "organisation";
                $this->organisation_name = $org->organisation_name; 
                $this->registration_number = $org->registration_number;
                $this->address = $org->address;
                $this->city = $org->city;
                $this->postal_code = $org->city;
                $this->province = $org->province;
                $this->value = $org->value;
                $this->description = $org->description;
            }
        }
    }

    public function saveInheritance($action = null){
        if($this->beneficiary_type == "individual"){
            $this->validate([
                'first_name' => 'required',  
                'surname' => 'required', 
                'id_number' => 'required', 
                'relation' => 'required'
            ]);
        }
        if($this->beneficiary_type == "organisation"){
            $this->validate([
                'organisation_name' => 'required', 
                'registration_number' => 'required',
                'address' => 'required',
                'city' => 'required',
                'postal_code' => 'required',
                'province' => 'required',
            ]);
        }

        $in = Inheritance::where('user_id', Auth::user()->id)->where('type', 'simultaneous-death')->where('sub_type', 'specific-inheritance')->first();
        if(!$in){
            $in = new Inheritance();
        }
        $in->user_id = Auth::user()->id;
        $in->type = 'simultaneous-death';
        $in->sub_type = 'specific-inheritance';
        $in->save();

        $org = $in->organisation_beneficiery->first();
        if($org){
            $org->delete();
        }
        $ind = $in->individual_beneficiary->first();
        if($ind){
            $ind->delete();
        }

        if($this->beneficiary_type == "individual"){
            $ind = IndividualBeneficiary::where('user_id', Auth::user()->id)->where('inheritance_id', $in->id)->first();
            if(!$ind){
                $ind = new IndividualBeneficiary();
            }
            $ind->user_id = Auth::user()->id;
            $ind->inheritance_id = $in->id;
            $ind->first_name = $this->first_name;
            $ind->middle_name = $this->middle_name;
            $ind->surname = $this->surname;
            $ind->id_number = $this->id_number;
            $ind->relation = $this->relation;
            $ind->description = $this->description;
            $ind->value = $this->value;
            $ind->save();
        }
        if($this->beneficiary_type == "organisation"){
            $org = OrganisationBeneficiary::where('user_id', Auth::user()->id)->where('inheritance_id', $in->id)->first();
            if(!$org){
                $org = new OrganisationBeneficiary();
            }
            $org->user_id = Auth::user()->id;
            $org->inheritance_id = $in->id;
            $org->organisation_name = $this->organisation_name;
            $org->registration_number = $this->registration_number;
            $org->address = $this->address;
            $org->city = $this->city;
            $org->postal_code = $this->postal_code;
            $org->province = $this->province;
            $org->description = $this->description;
            $org->value = $this->value;
            $org->save();
        }
        if($action == "go_to_next"){
            $this->emit('show-remainder-inheritance');
            //$this->getNextPage();
        }
        else{
            $this->dispatchBrowserEvent('data-updated', ['message' => 'Updated successfully.']);
        }
    }
    
    public function getNextPage(){
        $fnc = new Sharedfunctions();
        $next_page = $fnc->getNextWillPage('simultaneous-death');
        if($next_page){
            return redirect('organiser/online-will/'.$next_page);
        }
    }

    public function render(){
        return view('livewire.account.organiser.online-will.pages.simultaneous-death1.specific-inheritance');
    }
}
