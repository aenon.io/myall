<?php

namespace App\Http\Livewire\Account\Organiser\OnlineWill\Pages;

use Livewire\Component;

use Auth;
use App\Models\Inheritance;
use App\Lib\Sharedfunctions;
use App\Models\IndividualBeneficiary;
use App\Models\OrganisationBeneficiary;

class SimultaneousDeath2 extends Component
{
    public $sim_option;

    public $sim_inheritances = [], $provinces = [], $relations = [];
    public $show_inheritance_add_btns, $show_percentage;
    public $show_division_type = true;
    public $org_types;
    public $show_remainder_inheritance;
    
    protected $listeners = ['show-remainder-inheritance' => 'showRemainderInheritance'];

    public function removeIndividual($id){
        $ben = IndividualBeneficiary::find($id);
        if($ben){
            $inh = Inheritance::where('id', $ben->inheritance_id)->first();
            if($inh){
                $inh->delete();
            }
            $ben->delete();
        }
        $this->getData();
    }

    public function removeOrganisation($id){
        $ben = OrganisationBeneficiary::find($id);
        if($ben){
            $inh = Inheritance::where('id', $ben->inheritance_id)->first();
            if($inh){
                $inh->delete();
            }
            $ben->delete();
        }
        $this->getData();
    }
    
    public function mount(){
        $this->show_remainder_inheritance = false;
        $this->getData();

        $this->org_types = [
            'charity',
            'church',
            'close corporation',
            'company',
            'education institute',
            'public benefit organisation',
            'religious organisation',
            'trade union',
            'trust',
            'other'
        ];

        $this->provinces = [
            'Mpumalanga',
            'Limpopo',
            'Gauteng',
            'North West',
            'Western Cape',
            'Eastern Cape',
            'Kwazulu Natal',
            'Free State',
            'Northen Cape'
        ];
        sort($this->provinces);

        $this->relations = [
            'Great Grand Father',
            'Great Grand Mother',

            'Grand Father', 
            'Grand Mother',

            'Father', 
            'Uncle',
            'Step Father',
            'Father In Law',

            'Mother',
            'Aunt',
            'Step Mother',
            'Mother In Law',
            
            'Brother', 
            'Step Brother',
            'Brother In Law',
            
            'Sister',
            'Step Sister',
            'Sister In Law',

            'Cousin',
            'Friend', 

            'Spouse',
            'Partner', 
            'Wife',
            'Husband',

            'Son', 
            'Daughter',
            'Son In Law',
            'Daughter In Law',
            "Son's Partner",
            "Daughter's Partner",
            'Niece', 
            'Nephew',

            'Grand Son', 
            'Grand Daughter',
            /*
            'Great Grand Son',
            'Great Grand Daughter',
            */
        ];
        sort($this->relations);

        $this->show_inheritance_add_btns = false;
        $this->show_percentage = false;
    }

    public function getNextPage(){
        $fnc = new Sharedfunctions();
        $next_page = $fnc->getNextWillPage('simultaneous-death');
        if($next_page){
            return redirect('organiser/online-will/'.$next_page);
        }
    }

    public function deleteInheritance(){
        $ins = Inheritance::where('user_id', Auth::user()->id)->where('type', 'simultaneous-death')->get();
        foreach($ins AS $in){
            foreach($in->individual_beneficiary AS $ben){
                $ben->delete();
            }
            foreach($in->organisation_beneficiery AS $ben){
                $ben->delete();
            }
            $in->delete();
        }
        $this->show_division_type = true;
        $this->getData();
    }

    public function getData(){
        $this->sim_inheritances = [];
        $in = Inheritance::where('user_id', Auth::user()->id)->where('type', 'simultaneous-death')->get();
        //dd($in);
        if($in->count() == 0){
            $this->addInheritance();
        }
        else{
            foreach($in AS $inh){
                $type = null;
                if($inh['sub_type'] == "specific_inheritance"){
                    if($inh->individual_beneficiary->count() > 0){
                        $type = 'individual';
                    }
                    if($inh->organisation_beneficiery->count() > 0){
                        $type = 'organisation';
                    }
                }
                if($inh['sub_type'] == "inheritance" && $inh['division_type'] == "one_beneficiery"){
                    if($inh->individual_beneficiary->count() > 0){
                        $type = 'individual';
                    }
                    if($inh->organisation_beneficiery->count() > 0){
                        $type = 'organisation';
                    }
                }

                $arr = [
                    'id' => $inh->id,
                    'inheritance_type' => $inh->sub_type,
                    'division_type' => $inh->division_type,
                    'beneficiary_type' => $type,
                    'beneficiaries' => []
                ];
                foreach($inh->individual_beneficiary AS $ben){
                    $this->show_division_type = false;
                    $id_type = null;
                    $id_number = null;
                    $date_of_birth = null;
                    if(strlen($ben['id_number']) == 13){
                        $id_type = "rsa_id";
                        $id_number = $ben->id_number;
                    }
                    else{
                        $id_type = "dob";
                        $date_of_birth = $ben->id_number;
                    }
                    $ben_arr = [
                        'id' => $ben->id,
                        'type' => 'individual',
                        'first_name' => $ben->first_name,
                        'middle_name' => $ben->middle_name,
                        'surname' => $ben->surname,
                        'id_type' => $id_type,
                        'id_number' => $id_number,
                        'date_of_birth' => $date_of_birth,
                        'percentage' => $ben->percentage,
                        'description' => $ben->description,
                        'relation' => $ben->relation,
                    ];
                    $arr['beneficiaries'][] = $ben_arr;
                }
                foreach($inh->organisation_beneficiery AS $ben){
                    $this->show_division_type = false;
                    $ben_arr = [
                        'id' => $ben->id,
                        'type' => 'organisation',
                        'organisation_name' => $ben->organisation_name,
                        'registration_number' => $ben->registration_number,
                        'address' => $ben->address,
                        'city' => $ben->city,
                        'postal_code' => $ben->postal_code,
                        'province' => $ben->province,
                        'organisation_type' => $ben->organisation_type,
                        'organisation_other_type' => $ben->organisation_other_type,
                        'percentage' => $ben->percentage,
                        'description' => $ben->description,
                    ];
                    $arr['beneficiaries'][] = $ben_arr;
                }
                $this->sim_inheritances[] = $arr;
            }
        }
    }

    public function addInheritance(){
        $arr = [
            'inheritance_type' => '',
            'division_type' => '',
            'beneficiary_type' => '',
            'beneficiaries' => [],
        ];
        $this->sim_inheritances[] = $arr;
    }

    public function updatedSimInheritances($val,$key){
        $keys = explode(".",$key);
        $key = $keys[0];

        if($keys[1] == "beneficiary_type"){
            if($this->sim_inheritances[$key]['inheritance_type'] == "inheritance"){
                if($this->sim_inheritances[$key]['division_type'] == "one_beneficiery"){
                    if($this->sim_inheritances[$key]['beneficiary_type'] == "individual"){
                        if(count($this->sim_inheritances[$key]['beneficiaries']) > 0){
                            if($this->sim_inheritances[$key]['beneficiaries'][0]['type'] != "individual"){
                                unset($this->sim_inheritances[$key]['beneficiaries']);
                                $this->addIndividual($key);
                            }
                        }
                        else{
                            $this->addIndividual($key);
                        }
                    }
                    if($this->sim_inheritances[$key]['beneficiary_type'] == "organisation"){
                        if(count($this->sim_inheritances[$key]['beneficiaries']) > 0){
                            if($this->sim_inheritances[$key]['beneficiaries'][0]['type'] != "organisation"){
                                unset($this->sim_inheritances[$key]['beneficiaries']);
                                $this->addOrganisation($key);
                            }
                        }
                        else{
                            $this->addOrganisation($key);
                        }
                    }
                }
            }
            elseif($this->sim_inheritances[$key]['inheritance_type'] == "specific_inheritance"){
                if($this->sim_inheritances[$key]['beneficiary_type'] == "individual"){
                    if(count($this->sim_inheritances[$key]['beneficiaries']) > 0){
                        if($this->sim_inheritances[$key]['beneficiaries'][0]['type'] != "individual"){
                            unset($this->sim_inheritances[$key]['beneficiaries']);
                            $this->addIndividual($key);
                        }
                    }
                    else{
                        $this->addIndividual($key);
                    }
                }
                if($this->sim_inheritances[$key]['beneficiary_type'] == "organisation"){
                    if(count($this->sim_inheritances[$key]['beneficiaries']) > 0){
                        if($this->sim_inheritances[$key]['beneficiaries'][0]['type'] != "organisation"){
                            unset($this->sim_inheritances[$key]['beneficiaries']);
                            $this->addOrganisation($key);
                        }
                    }
                    else{
                        $this->addOrganisation($key);
                    }
                }
            }
        }

        if($keys[1] == "division_type"){
            if($this->sim_inheritances[$key]['inheritance_type'] == "inheritance"){
                if($this->sim_inheritances[$key]['division_type'] != "one_beneficiery"){
                    $this->show_inheritance_add_btns = true;
                    if($this->sim_inheritances[$key]['division_type'] == "percentage_shares"){
                        $this->show_percentage = true;
                    }
                    else{
                        $this->show_percentage = false;
                    }
                }
                else{
                    $this->show_inheritance_add_btns = false;
                    $this->show_percentage = false;
                }
            }
        }
    }

    public function addIndividual($key){
        $arr = [
            'type' => 'individual',
            'first_name' => '',
            'middle_name' => '',
            'surname' => '',
            'id_type' => '',
            'id_number' => '',
            'date_of_birth' => '',
            'percentage' => '',
            'description' => '',
            'relation' => '',
        ];
        $this->sim_inheritances[$key]['beneficiaries'][] = $arr;
    }

    public function addOrganisation($key){
        $arr = [
            'type' => 'organisation',
            'organisation_name' => '',
            'registration_number' => '',
            'address' => '',
            'city' => '',
            'postal_code' => '',
            'province' => '',
            'organisation_type' => '',
            'organisation_other_type' => '',
            'percentage' => '',
            'description' => '',
        ];
        $this->sim_inheritances[$key]['beneficiaries'][] = $arr;
    }

    public function saveInheritance($action = null){
        //Validate
        $has_error = false;
        foreach($this->sim_inheritances AS $inh){
            if($inh['inheritance_type'] == "inheritance"){
                if($inh['division_type'] == "percentage_shares"){
                    $tot_per = 0;
                    foreach($inh['beneficiaries'] AS $ben){
                        $tot_per += $ben['percentage'];
                    }
                    if($tot_per > 100){
                        $this->dispatchBrowserEvent('error_on_page', ['message' => 'Cannot exceed 100% - decrease the inheritance percentage in another section in order to increase the inheritance percentage in this section.']);
                        $has_error = true;
                    }
                    if($tot_per < 100){
                        $this->dispatchBrowserEvent('error_on_page', ['message' => 'Must be 100% - Increase other sections to reach a maximum of 100%.']);
                        $has_error = true;
                    }
                }

                foreach($inh['beneficiaries'] AS $ben){
                    if($ben['type'] == "organisation"){
                        if(!$ben['organisation_name'] || !$ben['registration_number']){
                            $this->dispatchBrowserEvent('error_on_page', ['message' => 'Please enter all organisation beneficiary details']);
                            $has_error = true;
                        }
                    }
                    if($ben['type'] == "individual"){
                        if(!$ben['first_name'] || !$ben['surname']){
                            $this->dispatchBrowserEvent('error_on_page', ['message' => 'Please enter all individual beneficiary details']);
                            $has_error = true;
                        }
                        if($ben['id_number'] == "" && $ben['date_of_birth'] == ''){
                            $this->dispatchBrowserEvent('error_on_page', ['message' => 'Please enter all individual ID Numbers / date of birth beneficiary details']);
                            $has_error = true;
                        }
                    }
                    if($inh['division_type'] == "percentage_shares"){
                        if($ben['percentage'] == "" || $ben['percentage'] == 0){
                            $this->dispatchBrowserEvent('error_on_page', ['message' => 'Percentages must be a number greater than 0']);
                            $has_error = true;
                        }
                    }
                }

                if($inh['division_type'] == "percentage_shares" || $inh['division_type'] == "equal_part"){
                    $count = count($inh['beneficiaries']);
                    if($count < 2){
                        $has_error = true;
                        $this->dispatchBrowserEvent('error_on_page', ['message' => 'Please enter two or more beneficiaries for '.strtolower(str_replace("_", " ", $inh['division_type'])).' inheritance']);
                    }
                }
            }
        }
        //Save
        if(!$has_error){
            $inheritance_types = [];
            foreach($this->sim_inheritances AS $inh){
                $in = null;
                if(isset($inh['id'])){
                    $in = Inheritance::find($inh['id']);
                }
                if(!$in){
                    $in = new Inheritance();
                }
                $in->user_id = Auth::user()->id;
                $in->type = "simultaneous-death";
                $in->sub_type = $inh['inheritance_type'];
                $in->division_type = $inh['division_type'];
                $in->save();

                $inheritance_types[] = $inh['inheritance_type'];

                foreach($inh['beneficiaries'] AS $ben){
                    if($ben['type'] == "individual"){
                        if(isset($ben['id'])){
                            $ind_ben = IndividualBeneficiary::find($ben['id']);
                        }
                        else{
                            $ind_ben = new IndividualBeneficiary();
                        }
                        $id_number = null;
                        if($ben['id_number']){
                            $id_number = $ben['id_number'];
                        }
                        else{
                            $id_number = $ben['date_of_birth'];
                        }
                        $ind_ben->user_id = Auth::user()->id;
                        $ind_ben->inheritance_id = $in->id;
                        $ind_ben->first_name = $ben['first_name'];
                        $ind_ben->middle_name = $ben['middle_name'];
                        $ind_ben->surname = $ben['surname'];
                        $ind_ben->id_number = $id_number;
                        $ind_ben->percentage = $ben['percentage'];
                        $ind_ben->relation = $ben['relation'];
                        $ind_ben->description = $ben['description'];
                        $ind_ben->save();    
                    }
                    if($ben['type'] == "organisation"){
                        if(isset($ben['id'])){
                            $org_ben = OrganisationBeneficiary::find($ben['id']);
                        }
                        else{
                            $org_ben = new OrganisationBeneficiary();
                        }
                        $org_ben->user_id = Auth::user()->id;
                        $org_ben->inheritance_id = $in->id;
                        $org_ben->organisation_name = $ben['organisation_name'];
                        $org_ben->registration_number = $ben['registration_number'];
                        $org_ben->address = $ben['address'];
                        $org_ben->city = $ben['city'];
                        $org_ben->postal_code = $ben['postal_code'];
                        $org_ben->province = $ben['province'];
                        $org_ben->organisation_type = $ben['organisation_type'];
                        $org_ben->organisation_other_type = $ben['organisation_other_type'];
                        $org_ben->percentage = $ben['percentage'];
                        $org_ben->description = $ben['description'];
                        $org_ben->save();
                    }
                }
            }
            $this->sim_inheritances = [];
            $this->getData();
            
            if(!in_array('inheritance', $inheritance_types)){
                $this->show_remainder_inheritance = true;
            }
            elseif($action == "go_to_next"){
                $this->getNextPage();
            }
            else{
                $this->dispatchBrowserEvent('data-updated', ['message' => 'Updated successfully.']);
            }
        }
    }

    public function render(){
        $inheritances = Inheritance::where('user_id', Auth::user()->id)->where('type', '<>', 'simultaneous-death')->orderBy('type', 'ASC')->get();
        return view('livewire.account.organiser.online-will.pages.simultaneous-death', [
            'inheritances' => $inheritances
        ]);
    }

}
