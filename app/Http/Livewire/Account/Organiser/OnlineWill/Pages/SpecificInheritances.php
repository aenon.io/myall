<?php

namespace App\Http\Livewire\Account\Organiser\OnlineWill\Pages;

use Livewire\Component;
use App\Lib\Sharedfunctions;

use Auth;
use App\Models\Inheritance;
use App\Models\IndividualBeneficiary;
use App\Models\OrganisationBeneficiary;

class SpecificInheritances extends Component
{
    public $beneficiary_type;
    public $first_name, $middle_name, $surname, $id_number, $relation;
    public $organisation_name, $registration_number, $address, $city, $postal_code, $province; 
    public $description, $value;
    public $show_modal = false;
    public $relations = [], $provinces = [], $org_types = [];

    public $benefiaciaries = [];
    
    public $sim_option;

    public function mount(){
        $this->loadData();
    }

    public function loadData(){
        $ins = Inheritance::where('user_id', Auth::user()->id)->where('type', 'specific-inheritance')->get();
        
        $this->org_types = [
            'charity',
            'church',
            'close corporation',
            'company',
            'education institute',
            'public benefit organisation',
            'religious organisation',
            'trade union',
            'trust',
            'other'
        ];
        
        $this->provinces = [
            'Mpumalanga',
            'Limpopo',
            'Gauteng',
            'North West',
            'Western Cape',
            'Eastern Cape',
            'Kwazulu Natal',
            'Free State',
            'Northen Cape'
        ];
        sort($this->provinces);
        
        $this->relations = [
            'Great Grand Father',
            'Great Grand Mother',

            'Grand Father', 
            'Grand Mother',

            'Father', 
            'Uncle',
            'Step Father',
            'Father In Law',

            'Mother',
            'Aunt',
            'Step Mother',
            'Mother In Law',
            
            'Brother', 
            'Step Brother',
            'Brother In Law',
            
            'Sister',
            'Step Sister',
            'Sister In Law',

            'Cousin',
            'Friend', 

            'Spouse',
            'Partner', 
            'Wife',
            'Husband',

            'Son', 
            'Daughter',
            'Son In Law',
            'Daughter In Law',
            "Son's Partner",
            "Daughter's Partner",
            'Niece', 
            'Nephew',
            'Step Son',
            'Step Daughter',

            'Grand Son', 
            'Grand Daughter',
            /*
            'Great Grand Son',
            'Great Grand Daughter',
            */

            'Other',
        ];
        sort($this->relations);
        
        if($ins->count() > 0){
            $this->benefiaciaries = [];
            foreach($ins AS $in){
                $inds = $in->individual_beneficiary;
                foreach($inds AS $ind){
                    if($ind){
                        $id_type = null;
                        if(strtotime($ind->id_number)){
                            $id_type = "dob";
                        }
                        else{
                            $id_type = "rsa_id";
                        }
                        $arr = [
                            'id' => $ind->id,
                            'beneficiary_type' => 'individual',

                            'first_name' => $ind->first_name,
                            'middle_name' => $ind->middle_name,
                            'surname' => $ind->surname,
                            'id_number' => $ind->id_number,
                            'relation' => $ind->relation,
                            'specify_relation' => $ind->specify_relation,
                            'description' => $ind->description,

                            'organisation_name' => '',
                            'registration_number' => '',
                            'address' => '',
                            'city' => '',
                            'postal_code' => '',
                            'province' => '',
                            'organisation_type' => '',
                            'organisation_other_type' => '',
                            
                            'id_type' => $id_type,
                        ];
                        $this->benefiaciaries[] = $arr;
                    }
                }
                $orgs = $in->organisation_beneficiery;
                foreach($orgs AS $org){
                    if($org){
                        $arr = [
                            'id' => $org->id,
                            'beneficiary_type' => 'organisation',

                            'organisation_name' => $org->organisation_name,
                            'registration_number' => $org->registration_number,
                            'address' => $org->address,
                            'city' => $org->city,
                            'postal_code' => $org->postal_code,
                            'province' => $org->province,
                            'description' => $org->description,
                            'organisation_type' => $org->organisation_type,
                            'organisation_other_type' => $org->organisation_other_type,

                            'first_name' => '',
                            'middle_name' => '',
                            'surname' => '',
                            'id_number' => '',
                            'relation' => '',
                        ];
                        $this->benefiaciaries[] = $arr;
                    }
                }
            }
        }
        else{
            $this->benefiaciaries = [];
            $this->show_modal = true;
            $this->addBeneficiary();
        }
    }

    public function redoSection(){
        $ins = Inheritance::where('user_id', Auth::user()->id)->where('type', 'specific-inheritance')->get();
        if($ins){
            foreach($ins AS $in){
                $inds = $in->individual_beneficiary;  
                foreach($inds AS $ind){
                    $ind->delete();
                } 
                $orgs = $in->organisation_beneficiery;
                foreach($orgs AS $org){
                    $org->delete();
                }
                $this->benefiaciaries = [];
                $in->delete();
            }
        }
        $this->loadData();
    }

    public function addBeneficiary(){
        $arr = [
            'beneficiary_type' => '',

            'first_name' => '',
            'middle_name' => '',
            'surname' => '',
            'id_number' => '',
            'relation' => '',
            'specify_relation' => '',

            'organisation_name' => '',
            'registration_number' => '',
            'address' => '',
            'city' => '',
            'postal_code' => '',
            'province' => '',
            'description' => '',

            'organisation_type' => '',
            'organisation_other_type' => '',
            
            'id_type' => 'rsa_id',
        ];

        $this->benefiaciaries[] = $arr;
    }

    public function getNextPage(){
        $fnc = new Sharedfunctions();
        $next_page = $fnc->getNextWillPage('specific-inheritance');
        if($next_page){
            return redirect('organiser/online-will/'.$next_page);
        }
    }

    public function removeBeneficiary($type, $id){
        if($type == "individual"){
            $ind = IndividualBeneficiary::find($id);
            if($ind){
                $ind->delete();
            }
        }
        if($type == 'organisation'){
            $org = OrganisationBeneficiary::find($id);
            if($org){
                $org->delete();
            }
        }
        $this->loadData();
    }

    public function saveInheritance($action = null){
        if(count($this->benefiaciaries) > 0){
            if($this->benefiaciaries[0]['beneficiary_type']){
                $in = Inheritance::where('user_id', Auth::user()->id)->where('type', 'specific-inheritance')->first();
                if(!$in){
                    $in = new Inheritance();
                }
                $in->user_id = Auth::user()->id;
                $in->type = 'specific-inheritance';
                $in->save();
                
                $has_error = false;
                foreach($this->benefiaciaries AS $ben){
                    if($ben['beneficiary_type'] == "individual"){
                        if($ben['first_name'] == "" || $ben['surname'] == "" || $ben['id_number'] == "" || $ben['relation'] == "" || $ben['description'] == ""){
                            $has_error = true;
                        }
                        if($ben['relation'] == "Other"){
                            if($ben['specify_relation'] == ""){
                                $has_error = true;
                            }
                        }
                    }
                    elseif($ben['beneficiary_type'] == "organisation"){
                        if($ben['organisation_name'] == "" || $ben['description'] == "" || $ben['organisation_type'] == "" || $ben['province'] == ""){
                            $has_error = true;    
                        }
                        if($ben['organisation_type'] == "other"){
                            if($ben['organisation_other_type'] == ""){
                                $has_error = true;
                            }
                        }
                    }
                }
                
                if($has_error){
                    $this->dispatchBrowserEvent('has-error', ['message' => 'Please enter all required data.']); 
                }
                else{
                    foreach($this->benefiaciaries AS $ben){
                        if($ben['beneficiary_type'] == "individual"){
                            if(isset($ben['id'])){
                                $ind = IndividualBeneficiary::find($ben['id']);
                            }
                            else{
                                $ind = new IndividualBeneficiary();   
                            }
            
                            $is_date = strtotime($ben['id_number']);
                            $fnc = new Sharedfunctions();
                            $is_sa_id = $fnc->isSaIdNumber($ben['id_number']);
            
                            // if(!$is_date || !$is_sa_id){
                            //     $this->addError('id', "Please enter a valid SA ID Number or Date in the format: YYYY-MM-DD");
                            // }
            
                            $ind->user_id = Auth::user()->id;
                            $ind->inheritance_id = $in->id;
                            $ind->first_name = $ben['first_name'];
                            $ind->middle_name = $ben['middle_name'];
                            $ind->surname = $ben['surname'];
                            $ind->id_number = $ben['id_number'];
                            $ind->relation = $ben['relation'];
                            $ind->specify_relation = $ben['specify_relation'];
                            $ind->description = $ben['description'];
                            $ind->value = $this->value;
                            $ind->save();
                        }
                        elseif($ben['beneficiary_type'] == "organisation"){
                            if(isset($ben['id'])){
                                $org = OrganisationBeneficiary::find($ben['id']);
                            }
                            else{
                                $org = new OrganisationBeneficiary();
                            }
                            $org->user_id = Auth::user()->id;
                            $org->inheritance_id = $in->id;
                            $org->organisation_name = $ben['organisation_name'];
                            $org->registration_number = $ben['registration_number'];
                            $org->address = $ben['address'];
                            $org->city = $ben['city'];
                            $org->postal_code = $ben['postal_code'];
                            $org->province = $ben['province'];
                            $org->description = $ben['description'];
                            $org->organisation_type = $ben['organisation_type'];
                            $org->organisation_other_type = $ben['organisation_other_type'];
                            $org->save();
                        }
            
                    }
                    if($action == "go_to_next"){
                        $this->getNextPage();
                    }
                    else{
                        $this->loadData();
                        $this->dispatchBrowserEvent('data-updated', ['message' => 'Updated successfully.']); 
                    }
                }
            }
        }
    }

    public function render(){
        return view('livewire.account.organiser.online-will.pages.specific-inheritances');
    }
}
