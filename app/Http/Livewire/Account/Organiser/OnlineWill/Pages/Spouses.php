<?php

namespace App\Http\Livewire\Account\Organiser\OnlineWill\Pages;
use Auth;
use Livewire\Component;
use App\Models\willSpouse;
use App\Models\Spouse;
use App\Lib\Sharedfunctions;

class Spouses extends Component
{
    public $spouses = [];

    public function mount(){
        $this->loadData();
    }
    
    public function removeSpouse($key){
        unset($this->spouses[$key]);
    }

    public function loadData(){
        $this->spouses = [];
        $spouses = Spouse::where('user_id', Auth::user()->id)->where('marital_status', 'Now Married')->get();
        if($spouses->count() > 0){
            foreach($spouses AS $sp){
                $fnc = new Sharedfunctions();
                $type = "rsa_id";
                if(!$fnc->isSaIdNumber($sp->spouse_id_number)){
                    $type = "passport";
                }
                $arr = [
                    'id' => $sp->id,
                    'first_name' => $sp->spouse_name,
                    'middle_name' => $sp->spouse_middle_name,
                    'surname' => $sp->spouse_surname,
                    'id_number' => $sp->spouse_id_number,
                    'comunity_marriage' => $sp->comunity_marriage,
                    'id_type' => $type,
                    'accrual' => $sp->accrual,
                    'marital_status' => $sp->marital_status,
                ];
                $this->spouses[] = $arr;
            }    
        }
        else{
            $this->addSpouse();
        }
    }

    public function addSpouse(){
        $arr = [
            'first_name' => '',
            'middle_name' => '',
            'surname' => '',
            'id_number' => '',
            'comunity_marriage' => '',
            'id_type' => '',
            'accrual' => '',
            'marital_status' => '',
        ];
        $this->spouses[] = $arr;
        $this->dispatchBrowserEvent('show-spouse-modal');
    }

    public function getNextPage(){
        $fnc = new Sharedfunctions();
        $next_page = $fnc->getNextWillPage('spouse');
        if($next_page){
            return redirect('organiser/online-will/'.$next_page);
        }
    }

    public function saveSpouse($action = null){
        $errors = false;
        foreach($this->spouses AS $k=>$spouse){
            $locale = 'en_US';
            $nf = new \NumberFormatter($locale, \NumberFormatter::ORDINAL);
            $spose_num = $nf->format(($k + 1));
            $spouse_name = $spouse['first_name'] ?: $spose_num.' spouse';

            if($spouse['first_name'] && $spouse['surname']){}
            else{
                $this->addError('not-saved', $spouse_name.' was not saved. Please enter all required data.');
                $errors = true;    
            }
        }
        
        if(!$errors){
            foreach($this->spouses AS $k=>$spouse){
                if($spouse['id_type'] == "rsa_id" && $spouse['id_number']){
                    $fnc = new Sharedfunctions();
                    if(!$fnc->isSaIdNumber($spouse['id_number'])){
                        $errors = true;
                        $this->addError('not-saved', $spouse_name.' was not saved. Please enter a valid RSA ID Number.');
                        continue;
                    }
                }

                if(isset($spouse['id'])){
                    $sp = Spouse::find($spouse['id']);
                }
                else{
                    $sp = new Spouse();
                }

                $comm = null;
                // if($spouse['comunity_marriage'] == "yes"){
                    $comm = $spouse['comunity_marriage'];
                // }

                $sp->user_id = Auth::user()->id;
                $sp->spouse_name = $spouse['first_name'];
                $sp->spouse_middle_name = $spouse['middle_name'];
                $sp->spouse_surname = $spouse['surname'];
                $sp->spouse_id_number = $spouse['id_number'];
                $sp->comunity_marriage = $comm;
                $sp->accrual = $spouse['accrual'];
                $sp->marital_status = "Now Married";
                $sp->save();
            }

            $this->loadData();

            if($action == "go_to_next"){
                $this->getNextPage();
            }
            else{
                $this->dispatchBrowserEvent('data-updated', ['message' => 'Updated successfully.']); 
            }
        }
    }

    public function render()
    {
        return view('livewire.account.organiser.online-will.pages.spouses');
    }
}
