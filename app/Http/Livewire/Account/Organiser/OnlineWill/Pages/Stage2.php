<?php

namespace App\Http\Livewire\Account\Organiser\OnlineWill\Pages;

use Livewire\Component;

use Auth;
use App\Models\WillOption;
use App\Models\Interview;

class Stage2 extends Component
{
    public $question, $question_description;
    public $question_number;
    public $info = [];
    public $answers = [];
    public $init_modal = false;

    public $stage_images = [],
        $stage_vids = [];

    public function showInfographics()
    {
        if ($this->question_number == 1) {
            $path = public_path() . '/infographics/interview/maried_and_minor';

            $images = glob($path . '/*.jpg');

            $this->stage_images = $images;
            $this->dispatchBrowserEvent('show-interview-images-modal');
        }
        if ($this->question_number == 2) {
            $path = public_path() . '/infographics/interview/minor';

            $images = glob($path . '/*.jpg');

            $this->stage_images = $images;
            $this->dispatchBrowserEvent('show-interview-images-modal');
        }
        if ($this->question_number == 3) {
            $path = public_path() . '/infographics/interview/under_18';

            $images = glob($path . '/*.jpg');

            $this->stage_images = $images;
            $this->dispatchBrowserEvent('show-interview-images-modal');
        }
        if ($this->question_number == 4) {
            $path = public_path() . '/infographics/interview/leave_possesions';

            $images = glob($path . '/*.jpg');

            $this->stage_images = $images;
            $this->dispatchBrowserEvent('show-interview-images-modal');
        }
        if ($this->question_number == 5) {
            $path = public_path() . '/infographics/interview/simulteaneous_death';

            $images = glob($path . '/*.jpg');

            $this->stage_images = $images;
            $this->dispatchBrowserEvent('show-interview-images-modal');
        }
    }

    public function loadVids()
    {
        $this->stage_vids = [];
        if ($this->question_number == 1) {
            $this->stage_vids = ['What is a Will'];
        }
        if ($this->question_number == 2) {
            $this->stage_vids = ['What is the role of the appointed guardian', 'What is the difference between a guardian of a minor and the Guardian Fund', 'When does the nomination of a guardian take effect', 'Will the guardian administer the inheritance of my minor child', 'How to apply for funds from the Guardian Fund'];
        }
        if ($this->question_number == 3) {
            $this->stage_vids = ['What is the role of the appointed guardian', 'What is the difference between a guardian of a minor and the Guardian Fund', 'When does the nomination of a guardian take effect', 'Will the guardian administer the inheritance of my minor child', 'How to apply for funds from the Guardian Fund'];
        }
        if ($this->question_number == 4) {
            $this->stage_vids = ['What is a Will', 'Who may not inherit as a beneficiary in a valid Will', 'No original Will or only a copy is found', 'South African legal requirements for a valid Will', 'Do I have to name each possession in my Will', 'Understanding the rules for inheriting specific possessions', 'Who pays the estate debts. Understanding the process', 'Understanding the different types of inheritances  heirs and legatees'];
        }
    }

    public function showVid()
    {
        $this->loadVids();
        if (count($this->stage_vids) > 0) {
            $this->dispatchBrowserEvent('show-interview-vids-modal');
        } else {
            $this->dispatchBrowserEvent('show-vid-coming-soon');
        }
    }

    public function showSingleVid($name)
    {
        $name = asset('/vids/' . $name . '.mp4');
        $this->dispatchBrowserEvent('show-interview-single-vid-modal', ['name' => $name]);
    }

    public function showSingleImage($image_name)
    {
        $path = url('/infographics/' . $image_name);
        $this->dispatchBrowserEvent('show-interview-single-image-modal', ['image_path' => $path]);
    }

    public function mount()
    {
        $this->question_number = 1;
        $this->loadQuestions();
    }

    public function loadQuestions()
    {
        if ($this->question_number == 1) {
            $this->question = 'Are you married?';
            $this->info = [
                'title' => 'Marital Status',
                'description' => 'If you are legally married, choose "yes," and if you are not legally married, choose "no".',
            ];
            $this->showInfo();
        }
        /*
        if($this->question_number == 12){
            $this->question = "Do you have more than one wife?";
            $this->question_description = "In SA law, customary marriages are allowed. A customary marriage is a union that is formed in terms of indigenous African customary law. This is law that allows a Black South African man to have more than one wife.";
            $this->info = [];
        }
        */

        if ($this->question_number == 2) {
            $this->question = 'Would you like to nominate a guardian for your child under the age of 18?';

            // $this->info = [
            //     "title" => "Children",
            //     "description" => "Choose No if you have no children. Choose No if your children are above 18 years"
            // ];
            $this->showInfo();
        }
        if ($this->question_number == 3) {
            $this->question = 'Do you have any beneficiaries who are currently under 18 years old?';
            $this->info = [];
        }
        if ($this->question_number == 4) {
            $this->question = 'Do you want to leave a specific possession or sum of money to someone?';
            $this->info = [
                'title' => 'Specific Inheritance',
                'description' => "Select YES if you wish to allocate a particular property or possession to a specific beneficiary. For instance, a diamond wedding ring, R2000 cash, or a Toyota Hilux with registration number CRE123GP.<br />
                Select NO if you do not intend to assign a specific property or possession to a particular beneficiary. By choosing NO you will be directed to the section where you can nominate one or more beneficiaries who will inherit your entire estate.",
            ];
            $this->showInfo();
        }
        if ($this->question_number == 5) {
            $this->question = 'In event that you and your beneficiary die simultaneously (at the same time), do you want to nominate a substitute beneficiary?';
            $this->info = [];
        }
        if ($this->question_number == 6) {
            $this->question = "
                <div class='text-start'>
                Do you want to:
                <ul>
                    <li>be an organ donor</li>
                    <li>be cremated</li>
                    <li>express specific funeral wishes</li>
                </ul>
                </div>";
        }
    }

    public function saveOption($option)
    {
        $this->answers[$this->question_number] = $option;
        if ($this->question_number == 1) {
            if ($option == 'yes') {
                $this->question_number = 2;
            } else {
                $this->question_number = 2;
            }
        } elseif ($this->question_number == 12) {
            $this->question_number = 2;
        } elseif ($this->question_number == 2) {
            $this->question_number = 3;
        } elseif ($this->question_number == 3) {
            $this->question_number = 4;
        } elseif ($this->question_number == 4) {
            $this->question_number = 5;
        } elseif ($this->question_number == 5) {
            $this->question_number = 6;
        }
        if ($this->question_number == 6) {
            $this->answers[$this->question_number] = $option;
            $this->saveOptions();
        }

        $this->info = [];
        $this->question_description = '';
        $this->question = '';

        $this->loadQuestions();
    }

    public function saveOptions()
    {
        $opt = WillOption::find(Auth::user()->id);
        if (!$opt) {
            $opt = new WillOption();
        }
        $opt->user_id = Auth::user()->id;
        $opt->profile = 1;
        $opt->executor = 1;

        $int = Interview::where('user_id', Auth::user()->id)->first();
        if ($int->leave_possesions_to_icapitated_persons == 'Yes') {
            $opt->testamentary_trust = 1;
        }

        if ($this->answers[1] == 'yes') {
            $opt->spouse = 1;
        }

        if ($this->answers[2] == 'yes') {
            $opt->minor_children_beneficiary = 1;
            $opt->guardian = 1;
            //$opt->testamentary_trust = 1;
        }

        if ($this->answers[3] == 'yes') {
            $opt->testamentary_trust = 1;
        }

        if ($this->answers[4] == 'yes') {
            $opt->specific_inheritance = 1;
            $opt->remainder_inheritance = 1;
        } else {
            $opt->inheritance = 1;
        }

        if ($this->answers[5] == 'yes') {
            $opt->simultaneous_death = 1;
        }

        $opt->funeral_wishes = 1;
        $opt->witnesses = 1;

        $opt->save();
        return redirect('organiser/online-will/profile');
    }

    public function showInfo()
    {
        if (count($this->info) > 0) {
            /* $this->dispatchBrowserEvent('show-info-modal'); */
            /* $this->init_modal = true; */
        }
    }

    public function render()
    {
        return view('livewire.account.organiser.online-will.pages.stage2');
    }
}
