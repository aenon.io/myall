<?php

namespace App\Http\Livewire\Account\Organiser\OnlineWill\Pages;

use Livewire\Component;
use Auth;
use App\Models\Trust;
use App\Models\Trustee;
use App\Lib\Sharedfunctions;
use App\Models\TrustBeneficiary;
use App\Models\WillOption;

class TestamentaryTrust extends Component
{
    
    public $minor_trustees = [], $incapacity_trustees = [], $beneficiaries = [];
    public $show_form;
    
    public $beneficiary_type, $same_trustees;
    public $minor_testamentary_trust_understand, $incapacity_testamentary_trust_understand;
    public $cur_minor_trust_id, $cur_incapacity_trust_id;
    public $has_data = false;
    
    public function mount(){
        $this->show_form = false;
        $this->cur_minor_trust_id = null;
        $this->cur_incapacity_trust_id = null;
        $this->loadData();
    }
    
    function updatedSameTrustees(){
        $this->incapacity_trustees = $this->minor_trustees;
    }

    public function clearData(){
        $trusts = Trust::where('user_id', Auth::user()->id)->get();
        foreach($trusts AS $trust){
            foreach($trust->trustees AS $trustees){
                $trustees->delete();
            }
            foreach($trust->beneficiaries AS $ben){
                $ben->delete();
            }
            $trust->delete();
        }
        $this->show_form = false;
        $this->has_data = false;
        $this->cur_minor_trust_id = null;
        $this->cur_incapacity_trust_id = null;
        $this->loadData();
    }

    public function removeTrustee($id){
        $tr = Trustee::find($id);
        if($tr){
            $tr->delete();
        }
        $this->loadData();
    }
    
    public function showTrustFrom($type){
        $this->beneficiary_type = $type;
        $this->show_form = true;
    }

    public function getNextPage(){
        $fnc = new Sharedfunctions();
        $next_page = $fnc->getNextWillPage('testamentary-trust');
        if($next_page){
            return redirect('organiser/online-will/'.$next_page);
        }
    }
    
    public function removeMenu(){
        $opt = WillOption::where('user_id', Auth::user()->id)->first();
        $opt->testamentary_trust = 0;
        $opt->save();
        $this->getNextPage();   
    }

    public function loadData(){
        $this->minor_trustees = [];
        $this->incapacity_trustees = []; 
        $this->beneficiaries = [];
        
        $trusts = Trust::where('user_id', Auth::user()->id)->get();
        
        if($trusts->count() > 0){
            $this->has_data = true;
            if($trusts->count() > 1){
                $this->beneficiary_type = "minor_incapacity";
            }
            else{
                $trust = $trusts->first();
                if($trust->trust_type == "minor-children"){
                    $this->beneficiary_type = "minor";
                    $this->minor_testamentary_trust_understand = true;    
                }
                else{
                    $this->beneficiary_type = $trust->trust_type;
                }
            }
            
            foreach($trusts AS $trust){
                if($trust->trust_type == "minor-children"){
                    $this->cur_minor_trust_id = $trust->id;
                    $this->minor_testamentary_trust_understand = true; 
                }
                elseif($trust->trust_type == "incapacity"){
                    $this->cur_incapacity_trust_id = $trust->id;
                    $this->incapacity_testamentary_trust_understand = true;
                }
                
                foreach($trust->trustees AS $trustees){
                    $id_type = null;
                    if(strlen($trustees->id_number) == 13){
                        $id_type = "rsa_id";
                    }
                    else{
                        $id_type = "passport";
                    }

                    $arr = [
                        'id' => $trustees->id,
                        'first_name' => $trustees->first_name,
                        'middle_name' => $trustees->middle_name,
                        'surname' => $trustees->surname,
                        'id_number' => $trustees->id_number,
                        'id_type' => $id_type,
                    ];
                    if($trust->trust_type == "minor-children"){
                        $this->minor_trustees[] = $arr;
                    }
                    if($trust->trust_type == "incapacity"){
                        $this->incapacity_trustees[] = $arr;
                    }
                }
                foreach($trust->beneficiaries AS $ben){
                    $arr = [
                        'id' => $ben->id,
                        'beneficiary_name' => $ben->name, 
                        'beneficiary_middle_name' => $ben->middle_name, 
                        'beneficiary_surname' => $ben->surname
                    ];
                    $this->beneficiaries[] = $arr;
                }
            }
            
            $this->show_form = true;
        }
        else{
            $this->addBeneficiary();
            for($i = 0; $i < 3; $i++){
                $this->addMinorTrustee();
            }
            for($i = 0; $i < 3; $i++){
                $this->addIncapacityTrustees();
            }
        }
    }
    
    public function addBeneficiary(){
        $arr = [
            'beneficiary_name' => '', 
            'beneficiary_middle_name' => '', 
            'beneficiary_surname' => ''    
        ];
        $this->beneficiaries[] = $arr;
    }
    
    public function addIncapacityTrustees(){
        $arr = [
            'first_name' => '',
            'middle_name' => '',
            'surname' => '',
            'id_number' => '',
            'id_type' => '',
        ];
        $this->incapacity_trustees[] = $arr;
    }
    
    public function addMinorTrustee(){
        $arr = [
            'first_name' => '',
            'middle_name' => '',
            'surname' => '',
            'id_number' => '',
            'id_type' => '',
        ];
        $this->minor_trustees[] = $arr;
    }
    
    public function validateTrustees($type){
        $arr = [
            "status" => "success",
            "message" => ""
        ];
        
        $trustee_arr = null;
        if($type == "minor"){
            $trustee_arr = $this->minor_trustees;
        }
        elseif($type == "incapacity"){
            $trustee_arr = $this->incapacity_trustees;
        }
        
        if(count($trustee_arr) > 0){
            foreach($trustee_arr AS $k=>$trustee){
                $locale = 'en_US';
                $nf = new \NumberFormatter($locale, \NumberFormatter::ORDINAL);
                $trustee_num = $nf->format(($k + 1));
                $trustee_name = $trustee_num;
                
                if($k < 2){
                    if($trustee['first_name'] == "" || $trustee['surname'] == "" || $trustee['id_number'] == ""){
                        $arr["status"] = "error";
                        $arr["message"] = 'Please enter all required data for '.$trustee_name.' trustee.';
                    }
                }
                else{
                    if($trustee['first_name'] || $trustee['surname'] || $trustee['id_number']){
                        if($trustee['first_name'] == "" || $trustee['surname'] == "" || $trustee['id_number'] == ""){
                            $arr["status"] = "error";
                            $arr["message"] = 'Please enter all required data or none for the '.$trustee_name.' trustee.';
                        }
                    }
                }
                if($trustee['id_type'] == "rsa_id"){
                    $func = new Sharedfunctions();
                    if(!$func->isSaIdNumber($trustee['id_number'])){
                        $arr['status'] = "error";
                        $arr["message"] = 'Please enter a valid SA ID number for '.$trustee_name.' trustee.';
                    }
                }
                
            }
        }
        else{
            $arr = [
                "status" => "error",
                "message" => "You did not enter any trustees."
            ];
        }
        return $arr;
    }
    
    public function validateBeneficiaries(){
        $arr = [
            "status" => "success",
            "message" => ""
        ];
        
        if(count($this->beneficiaries) > 0){
            foreach($this->beneficiaries AS $k=>$ben){
                $locale = 'en_US';
                $nf = new \NumberFormatter($locale, \NumberFormatter::ORDINAL);
                $ben_num = $nf->format(($k + 1));
                $ben_name = $ben_num;
                
                if($ben["beneficiary_name"] == "" || $ben["beneficiary_surname"] == ""){
                    $arr = [
                        "status" => "error",
                        "message" => $ben_name." beneficiary was not saved, please enter all required information."
                    ];
                }
            }
        }
        else{
            $arr = [
                "status" => "error",
                "message" => "You did not enter any beneficiaries."
            ];
        }
        return $arr;
    }
    
    public function saveTrustee($type, $trust_id){
        $trustee_arr = null;
        if($type == "minor"){
            $trustee_arr = $this->minor_trustees;
        }
        elseif($type == "incapacity"){
            $trustee_arr = $this->incapacity_trustees;
        }
        
        if(count($trustee_arr) > 0){
            foreach($trustee_arr AS $trustee){
                if($trustee['first_name'] != "" && $trustee['surname'] != "" && $trustee['id_number'] != ""){
                    
                    if(isset($trustee['id'])){
                        $tr = Trustee::find($trustee['id']);
                    }
                    else{
                        $tr = new Trustee();
                    }
                    $tr->user_id = Auth::user()->id;
                    $tr->trust_id = $trust_id;
                    $tr->first_name = $trustee['first_name'];
                    $tr->middle_name = $trustee['middle_name'];
                    $tr->surname = $trustee['surname'];
                    $tr->id_number = $trustee['id_number'];
                    $tr->save();
                }
            }
        }
    }
    
    public function saveBeneficiary($trust_id){
        foreach($this->beneficiaries AS $ben){
            if(isset($ben['id'])){
                $b = TrustBeneficiary::find($ben['id']);
            }
            else{
                $b = new TrustBeneficiary();
            }
            $b->trust_id = $trust_id;
            $b->user_id = Auth::user()->id;
            $b->name = $ben['beneficiary_name'];
            $b->middle_name = $ben["beneficiary_middle_name"];
            $b->surname = $ben["beneficiary_surname"];
            $b->save();
        }
    }

    public function saveTrustees($action = null){
        $has_error = false;
        
        if($this->beneficiary_type == "minor"){
            $res = $this->validateTrustees('minor');
            if($res['status'] == "error"){
                $has_error = true;
                $this->addError('not-saved', $res['message']);
            }
            if(!$this->minor_testamentary_trust_understand){
                $has_error = true;
                $this->addError('not-saved', "Please confirm that you understand.");
            }
        }
        if($this->beneficiary_type == "incapacity"){
            $res = $this->validateTrustees('incapacity');
            if($res['status'] == "error"){
                $has_error = true;
                $this->addError('not-saved', $res['message']);
            }  
            $res = $this->validateBeneficiaries();
            if($res['status'] == "error"){
                $has_error = true;
                $this->addError('not-saved', $res['message']);
            }
            if(!$this->incapacity_testamentary_trust_understand){
                $has_error = true;
                $this->addError('not-saved', "Please confirm that you understand.");
            }
        }
        if($this->beneficiary_type == "minor_incapacity"){
            $res = $this->validateTrustees('minor');
            if($res['status'] == "error"){
                $has_error = true;
                $this->addError('not-saved', $res['message']);
            }
            $res = $this->validateTrustees('incapacity');
            if($res['status'] == "error"){
                $has_error = true;
                $this->addError('not-saved', $res['message']);
            }
            $res = $this->validateBeneficiaries();
            if($res['status'] == "error"){
                $has_error = true;
                $this->addError('not-saved', $res['message']);
            }
            if(!$this->minor_testamentary_trust_understand){
                $has_error = true;
                $this->addError('not-saved', "Please confirm that you understand.");
            }
            if(!$this->incapacity_testamentary_trust_understand){
                $has_error = true;
                $this->addError('not-saved', "Please confirm that you understand.");
            }
        }
        
        if(!$has_error){
            if($this->beneficiary_type == "minor"){
                if($this->cur_minor_trust_id){
                    $trust = Trust::find($this->cur_minor_trust_id);
                }
                else{
                    $trust = new Trust();
                }
                
                $trust->user_id = Auth::user()->id;
                $trust->trust_type = "minor-children";
                $trust->save();
                
                $this->saveTrustee("minor", $trust->id);
            }
            if($this->beneficiary_type == "incapacity"){
                if($this->cur_incapacity_trust_id){
                    $trust = Trust::find($this->cur_incapacity_trust_id);
                }
                else{
                    $trust = new Trust();
                }
                
                $trust->user_id = Auth::user()->id;
                $trust->trust_type = "incapacity";
                $trust->save();
                
                $this->saveTrustee("incapacity", $trust->id);
                $this->saveBeneficiary($trust->id);
            }
            if($this->beneficiary_type == "minor_incapacity"){
                if($this->cur_minor_trust_id){
                    $trust = Trust::find($this->cur_minor_trust_id);
                }
                else{
                    $trust = new Trust();
                }
                
                $trust->user_id = Auth::user()->id;
                $trust->trust_type = "minor-children";
                $trust->save();
                
                $this->saveTrustee("minor", $trust->id);
                
                if($this->cur_incapacity_trust_id){
                    $trust_2 = Trust::find($this->cur_incapacity_trust_id);
                }
                else{
                    $trust_2 = new Trust();
                }
                
                $trust_2->user_id = Auth::user()->id;
                $trust_2->trust_type = "incapacity";
                $trust_2->save();
                
                $this->saveTrustee("incapacity", $trust_2->id);
                $this->saveBeneficiary($trust_2->id);
            }
            if($action == "go_to_next"){
                $this->getNextPage();
            }
            else{
                $this->loadData();
                $this->dispatchBrowserEvent('data-updated', ['message' => 'Updated successfully.']); 
            }
        }
    }
    

    public function render(){
        return view('livewire.account.organiser.online-will.pages.testamentary-trust');
    }
}
