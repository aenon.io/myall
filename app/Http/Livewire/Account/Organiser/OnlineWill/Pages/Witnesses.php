<?php

namespace App\Http\Livewire\Account\Organiser\OnlineWill\Pages;

use Livewire\Component;

use Auth;
use App\Models\Witness;
use PDF;
use App\Lib\Sharedfunctions;

class Witnesses extends Component
{
    public $witnesses = [];
    public $show_modal = false;
    public $provinces;

    public function mount()
    {
        $this->loadData();
    }

    public function loadData()
    {
        $this->provinces = ['Mpumalanga', 'Limpopo', 'Gauteng', 'North West', 'Western Cape', 'Eastern Cape', 'Kwazulu Natal', 'Free State', 'Northen Cape'];
        sort($this->provinces);

        $this->witnesses = [];
        $wts = Witness::where('user_id', Auth::user()->id)->get();
        if ($wts->count() > 0) {
            foreach ($wts as $k => $wt) {
                $fnc = new Sharedfunctions();
                $type = 'rsa_id';
                if (!$fnc->isSaIdNumber($wt->id_number)) {
                    $type = 'passport';
                }
                if ($k < 2) {
                    $arr = [
                        'id' => $wt->id,
                        'first_name' => $wt->first_name,
                        'middle_name' => $wt->middle_name,
                        'surname' => $wt->surname,
                        'id_number' => $wt->id_number,
                        'phone_number' => $wt->phone_number,
                        'address' => $wt->address,
                        'city' => $wt->city,
                        'postal_code' => $wt->postal_code,
                        'province' => $wt->province,
                        'id_type' => $type,
                    ];
                    $this->witnesses[] = $arr;
                }
            }
            if ($wts->count() == 1) {
                $this->addWiness();
            }
        } else {
            for ($i = 0; $i < 2; $i++) {
                $this->addWiness();
            }
            $this->show_modal = true;
        }
    }

    public function addWiness()
    {
        $arr = [
            'first_name' => '',
            'middle_name' => '',
            'surname' => '',
            'id_number' => '',
            'phone_number' => '',
            'address' => '',
            'city' => '',
            'postal_code' => '',
            'province' => '',
            'id_type' => '',
        ];
        $this->witnesses[] = $arr;
    }

    public function saveWitnesses($go_to_page = null)
    {
        $has_error = false;
        $witness_id = null;
        foreach ($this->witnesses as $k => $witness) {
            $locale = 'en_US';
            $nf = new \NumberFormatter($locale, \NumberFormatter::ORDINAL);
            $witness_num = $nf->format($k + 1);
            $witness_name = $witness['first_name'] ?: $witness_num . ' witness';

            if ($witness['first_name'] && $witness['surname'] && $witness['id_number'] && $witness['address'] && $witness['city'] && $witness['province']) {
                if (!$witness_id) {
                    $witness_id = $witness['id_number'];
                }

                if ($witness['id_type'] == 'rsa_id') {
                    $fnc = new Sharedfunctions();
                    if (!$fnc->isSaIdNumber($witness['id_number'])) {
                        $has_error = true;
                        $this->addError('not-saved', $witness_name . ' was not saved. Please enter a valid RSA ID Number.');
                        continue;
                    }

                    $year = substr($witness['id_number'], 0, 2);
                    $dt = \DateTime::createFromFormat('y', $year);
                    $year = $dt->format('Y');
                    if ($year > date('Y')) {
                        $year = $year - 100;
                    }
                    $diff = date('Y') - $year;
                    if ($diff < 16) {
                        $has_error = true;
                        $this->addError('age', 'Witness cannot be younger than 14 years old.');
                        continue;
                    }
                }
                if ($witness['id_type'] == 'rsa_id') {
                    if ($k > 0) {
                        if ($witness['id_number'] == $witness_id) {
                            $has_error = true;
                            $this->addError('not-saved', 'Witness ID numbers cannot be the same.');
                            continue;
                        }
                    }
                }
            } else {
                $this->addError('witness', $witness_name . ' was not saved. Please enter all required data.');
                $has_error = true;
            }
        }
        if (!$has_error) {
            foreach ($this->witnesses as $k => $witness) {
                if (isset($witness['id'])) {
                    $wt = Witness::find($witness['id']);
                } else {
                    $wt = new Witness();
                }
                $wt->user_id = Auth::user()->id;
                $wt->first_name = $witness['first_name'];
                $wt->middle_name = $witness['middle_name'];
                $wt->surname = $witness['surname'];
                $wt->id_number = $witness['id_number'];
                $wt->phone_number = $witness['phone_number'];
                $wt->address = $witness['address'];
                $wt->city = $witness['city'];
                $wt->postal_code = $witness['postal_code'];
                $wt->province = $witness['province'];
                $wt->save();
            }
        }
        if (!$has_error) {
            if ($go_to_page) {
                return redirect('organiser/online-will/review/executor');
            } else {
                $this->loadData();
                $this->dispatchBrowserEvent('data-updated', ['message' => 'Updated successfully.']);
            }
        }
    }

    public function deleteWitness($id)
    {
        $wt = Witness::find($id);
        $wt->delete();
        $this->loadData();
    }

    public function render()
    {
        return view('livewire.account.organiser.online-will.pages.witnesses');
    }
}
