<?php

namespace App\Http\Livewire\Account\Organiser\OnlineWill;

use Livewire\Component;

use App\Models\Review as ReviewModel;
use App\Models\Confirmation;
use Auth;
use App\Lib\willPdfGen;

class Review extends Component
{
    public $review_page;

    public $exec_name_check, $exec_id_check;
    public $inh_name_check, $inh_id_check, $inh_org_check, $inh_allocation_check;
    public $wit_name_check, $wit_id_check, $wit_contact_check, $wit_age_check, $wit_write_check, $wit_in_will_check;
    public $gud_name_check, $gud_id_check;
    public $trs_name_check, $trs_id_check;
    public $ack_check_one, $ack_check_two, $ack_check_three, $ack_check_four, $ack_check_five, $ack_check_six, $ack_check_seven, $ack_check_eight, $ack_check_nine, $ack_check_ten, $ack_check_eleven, $ack_check_twelve, $ack_check_thirteen, $ack_check_fourteen;
    public $check_all_pages_printed, $check_will_read;
    public $check_prev_revoke, $check_valid_date, $check_sig_conf, $check_witness_presen, $check_witness_irrel;
    public $check_terms_read, $check_two_coppies, $check_sign_with_witness, $check_witness_sign_together, $check_sign_place_date;
    public $check_revoke_cur_will, $check_destroy_types, $check_will_storage, $check_notify_family;
    public $sp_name_check, $sp_id_check;

    public function mount($review_page)
    {
        if ($review_page) {
            $this->review_page = $review_page;
        }
    }

    public function checkStage7A()
    {
        $this->validate(
            [
                'check_revoke_cur_will' => 'required',
                'check_destroy_types' => 'required',
                'check_will_storage' => 'required',
                'check_notify_family' => 'required',
            ],
            [
                'check_revoke_cur_will.required' => 'Confirm that you understand that you may revoke your signed Will by explicitly mentioning such revocation in a later dated original signed Will or by destroying all ORIGINAL copies of such a Will.',
                'check_destroy_types.required' => 'Confirm that you understand destroying your Will means tearing it up, burning it, or cutting it into pieces. ',
                'check_will_storage.required' => 'Confirm you put your original signed Will in a safe place.',
                'check_notify_family.required' => 'Confirm that you told your spouse, family members or beneficiaries where they can find your Will.',
            ],
        );

        $cnf = Confirmation::where('user_id', Auth::user()->id)->first();
        if (!$cnf) {
            $cnf = new Confirmation();
        }

        $cnf->user_id = Auth::user()->id;
        $cnf->step_7 = 1;
        $cnf->save();

        return redirect('organiser/online-will/review/complete');
    }

    public function checkStage6B()
    {
        $this->validate(
            [
                'check_terms_read' => 'required',
                'check_two_coppies' => 'required',
                'check_sign_with_witness' => 'required',
                'check_witness_sign_together' => 'required',
                'check_sign_place_date' => 'required',
            ],
            [
                'check_terms_read.required' => 'Confirm that you read the full Terms and Conditions as well as the Legal Disclaimer of the Online Will Service before signing the Will.',
                'check_two_coppies.required' => 'Confirm that you made at least two copies of the Will to be signed.',
                'check_sign_with_witness.required' => 'Confirm that you ensured that you sign your Will in the presence of the identified two witnesses.',
                'check_witness_sign_together.required' => 'Confirm that you ensured that the same two witnesses sign your Will in your presence.',
                'check_sign_place_date.required' => 'Confirm that you wrote down the full date {day, month and year} and the name of the town or city where you signed your Will: e.g 12 January 2021 at Cape Town.',
            ],
        );

        $cnf = Confirmation::where('user_id', Auth::user()->id)->first();
        if (!$cnf) {
            $cnf = new Confirmation();
        }

        $cnf->user_id = Auth::user()->id;
        $cnf->step_6 = 1;
        $cnf->save();

        return redirect('organiser/online-will/review/stage-7-a');
    }

    public function checkStage6A()
    {
        $this->validate(
            [
                'check_prev_revoke' => 'required',
                'check_valid_date' => 'required',
                'check_sig_conf' => 'required',
                'check_witness_presen' => 'required',
                'check_witness_irrel' => 'required',
            ],
            [
                'check_prev_revoke.required' => 'Confirm that you understand that your most recent valid signed Will revokes all your previous Wills.',
                'check_valid_date.required' => 'Confirm that the date of your valid signed Will indicates which valid Will is your most recent Will.',
                'check_sig_conf.required' => 'Confirm that your signature confirms that the contents of your Will reflect your wishes and intentions.',
                'check_witness_presen.required' => 'Confirm that the two witnesses only serve to confirm that you have signed the Will in their presence.',
                'check_witness_irrel.required' => 'Confirm that you understand that the content of your Will is irrelevant to the witnesses.',
            ],
        );

        return redirect('organiser/online-will/review/stage-6-b');
    }

    public function checkStage5()
    {
        $this->validate(
            [
                'check_all_pages_printed' => 'required',
                'check_will_read' => 'required',
            ],
            [
                'check_all_pages_printed.required' => 'Confirm that you check that each page is printed out and following the numerical sequence, e.g. page 1, 2, 3, 4.',
                'check_will_read.required' => 'Confirm that you read through the Will and ensure that all the details are correct and that your Will reflects your wishes and instructions.',
            ],
        );

        $cnf = Confirmation::where('user_id', Auth::user()->id)->first();
        if (!$cnf) {
            $cnf = new Confirmation();
        }

        $cnf->user_id = Auth::user()->id;
        $cnf->step_5 = 1;
        $cnf->save();

        return redirect('organiser/online-will/review/stage-6-a');
    }

    public function downloadPdf()
    {
        /* $this->dispatchBrowserEvent('notify', ['title' => "Please note",  'message' => "Once you have downloaded your Will, you will need to proceed through stages 5 - 7 to complete your MyAll Will."]); */
        $pdf_gen = new willPdfGen(Auth::user()->id);
        $pdfContent = $pdf_gen->getPdf();
        $filename = 'Last Will and Testament.pdf';
        return response()->streamDownload(function () use ($pdfContent) {
            echo $pdfContent;
        }, $filename);
    }

    public function checkAknowledgement()
    {
        $this->dispatchBrowserEvent('go-to-top');
        $this->validate(
            [
                'ack_check_one' => 'required',
                'ack_check_two' => 'required',
                'ack_check_three' => 'required',
                // 'ack_check_four' => 'required',
                // 'ack_check_five' => 'required',
                // 'ack_check_six' => 'required',
                'ack_check_seven' => 'required',
                'ack_check_eight' => 'required',
                'ack_check_nine' => 'required',
                'ack_check_ten' => 'required',
                // 'ack_check_eleven' => 'required',
                'ack_check_twelve' => 'required',
                'ack_check_thirteen' => 'required',
                // 'ack_check_fourteen' => 'required'
            ],
            [
                'ack_check_one.required' => 'Confirm you are 16 years and older.',
                'ack_check_two.required' => 'Confirm you understand that your original signed Will regulates your deceased estate from the date of your passing.',
                'ack_check_three.required' => 'Confirm you understand that under South African Law you have the freedom to leave your possessions to whomever you please and that you must name these beneficiaries in your Will.',
                // 'ack_check_four.required' => "Confirm you understand that the people or organisations I nominate as beneficiaries will inherit my South African possessions.",
                // 'ack_check_five.required' => "Confirm you understand, that if I disinherit or to a large degree disinherit my spouse, minor biological, and/or legally adopted children, such a person may submit a maintenance claim against my deceased estate.",
                // 'ack_check_six.required' => "Confirm you have the mental capacity to draft and sign my Will in so far as that I understand the nature and effect of my will at the time of signing of my Will.",
                'ack_check_seven.required' => 'Confirm that you have voluntarily and on your own accord completed this Online Will application.',
                'ack_check_eight.required' => 'Confirm that you understand that at any time after the signing of your Will, you may change the details of your Will with another signed and later-dated will.',
                'ack_check_nine.required' => 'Confirm you understand that your Will must be written and signed on paper and must be original (not a photocopy or scanned copy).',
                'ack_check_ten.required' => 'Confirm you understand that your Will is only valid as an original signed paper document and NOT as a video, email, computer file or any other electronic document or format.',
                // 'ack_check_eleven.required' => "Confirm you undertake to follow the procedures set out in Stages 3 to 7 to ensure that my Will meets the conditions required by the Master of the Court to accept my Will as legally enforceable.",
                'ack_check_twelve.required' => 'Confirm you understand that the required witness to the signing of this Will CANNOT be a beneficiary, executor, guardian, trust nor their spouses or anyone named in your Will.',
                'ack_check_thirteen.required' => "Confirm you understand that SA law does not allow a Will to be signed with a digital signature. This means a Will still has to be physically signed in 'wet ink' (where a signature is manually signed, using the liquid ink of a pen).",
                // 'ack_check_fourteen.required' => "Confirm you can read my drafted Will."
            ],
        );

        $cnf = Confirmation::where('user_id', Auth::user()->id)->first();
        if (!$cnf) {
            $cnf = new Confirmation();
        }

        $cnf->user_id = Auth::user()->id;
        $cnf->step_4 = 1;
        $cnf->save();

        return redirect('organiser/online-will/review/download-will');
    }

    public function checkTrustees()
    {
        $this->dispatchBrowserEvent('go-to-top');
        $this->validate(
            [
                'trs_name_check' => 'required',
                'trs_id_check' => 'required',
            ],
            [
                'trs_name_check.required' => 'Confirm correct spelling of full names and surnames.',
                'trs_id_check.required' => 'Confirm correct ID Numbers of each trustee.',
            ],
        );

        $rev = ReviewModel::where('user_id', Auth::user()->id)->first();
        if (!$rev) {
            $rev = new ReviewModel();
        }

        $rev->user_id = Auth::user()->id;
        $rev->trustees = 1;
        $rev->save();

        return redirect('organiser/online-will/review/start-stage-four');
    }

    public function checkGuardians()
    {
        $this->dispatchBrowserEvent('go-to-top');
        $this->validate(
            [
                'gud_name_check' => 'required',
                'gud_id_check' => 'required',
            ],
            [
                'gud_name_check.required' => 'Confirm correct spelling of full names and surnames.',
                'gud_id_check.required' => 'Confirm correct ID Numbers or date of birth of minor and guardian.',
            ],
        );

        $rev = ReviewModel::where('user_id', Auth::user()->id)->first();
        if (!$rev) {
            $rev = new ReviewModel();
        }

        $rev->user_id = Auth::user()->id;
        $rev->guardians = 1;
        $rev->save();

        if (Auth::user()->trusts->count() > 0) {
            return redirect('organiser/online-will/review/trustees');
        } else {
            return redirect('organiser/online-will/review/start-stage-four');
        }
    }

    public function checkWitnesses()
    {
        $this->dispatchBrowserEvent('go-to-top');
        $this->validate(
            [
                'wit_name_check' => 'required',
                'wit_id_check' => 'required',
                'wit_contact_check' => 'required',
                'wit_age_check' => 'required',
                'wit_write_check' => 'required',
                'wit_in_will_check' => 'required',
            ],
            [
                'wit_name_check.required' => 'Confirm correct spelling of full names and surnames.',
                'wit_id_check.required' => 'Confirm correct ID Numbers.',
                'wit_contact_check.required' => 'Confirm correct contact details.',
                'wit_age_check.required' => 'Confirm each witness is 14 years or older.',
                'wit_write_check.required' => 'Confirm each witness can sign the will in his / her own handwriting.',
                'wit_in_will_check.required' => 'Confirm that neither each witness nor their spouse is a beneficiary, guardian, trustee, or executor in your Will.',
            ],
        );

        $rev = ReviewModel::where('user_id', Auth::user()->id)->first();
        if (!$rev) {
            $rev = new ReviewModel();
        }

        $rev->user_id = Auth::user()->id;
        $rev->witnesses = 1;
        $rev->save();
        if (Auth::user()->guardians->count() > 0) {
            return redirect('organiser/online-will/review/guardians');
        } elseif (Auth::user()->trusts->count() > 0) {
            return redirect('organiser/online-will/review/trustees');
        } else {
            return redirect('organiser/online-will/review/start-stage-four');
        }
    }

    public function checkBeneficiaries()
    {
        $this->dispatchBrowserEvent('go-to-top');
        $this->validate(
            [
                'inh_name_check' => 'required',
                'inh_id_check' => 'required',
                'inh_org_check' => 'required',
                'inh_allocation_check' => 'required',
            ],
            [
                'inh_name_check.required' => 'Confirm correct spelling of full names and surnames.',
                'inh_id_check.required' => 'Confirm correct ID Numbers / Dates of birth.',
                'inh_org_check.required' => 'Confirm full and proper identification of organisation beneficiaries.',
                'inh_allocation_check.required' => "Confirm if each beneficiary's inheritance is correctly allocated to such beneficiary.",
            ],
        );

        $rev = ReviewModel::where('user_id', Auth::user()->id)->first();
        if (!$rev) {
            $rev = new ReviewModel();
        }

        $rev->user_id = Auth::user()->id;
        $rev->beneficiaries = 1;
        $rev->save();

        return redirect('organiser/online-will/review/witnesses');
    }

    public function checkSpouse()
    {
        $this->dispatchBrowserEvent('go-to-top');
        $this->validate(
            [
                'sp_name_check' => 'required',
                'sp_id_check' => 'required',
            ],
            [
                'sp_name_check.required' => 'Confirm the correct spelling of full name and surname of each spouse.',
                'sp_id_check.required' => 'Please confirm that spouse IDs are correct.',
            ],
        );
        return redirect('organiser/online-will/review/beneficiaries');
    }

    public function checkExecutors()
    {
        $this->dispatchBrowserEvent('go-to-top');
        $this->validate(
            [
                'exec_name_check' => 'required',
                'exec_id_check' => 'required',
            ],
            [
                'exec_name_check.required' => 'Confirm correct spelling of full names and surnames.',
                'exec_id_check' => 'Confirm correct identity numbers.',
            ],
        );
        $rev = ReviewModel::where('user_id', Auth::user()->id)->first();
        if (!$rev) {
            $rev = new ReviewModel();
        }

        $rev->user_id = Auth::user()->id;
        $rev->executors = 1;
        $rev->save();
        if (Auth::user()->will_menu_options->spouse) {
            return redirect('organiser/online-will/review/spouse');
        } else {
            return redirect('organiser/online-will/review/beneficiaries');
        }
    }

    public function render()
    {
        return view('livewire.account.organiser.online-will.review');
    }
}
