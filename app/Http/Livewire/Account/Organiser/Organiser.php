<?php

namespace App\Http\Livewire\Account\Organiser;

use Livewire\Component;
use Illuminate\Support\Facades\Auth;
use App\Models\Interview;
use App\Models\WillOption;
use App\Models\PopUp;

class Organiser extends Component
{
    public $menu_size = '3';
    public $view, $stage, $review_page;
    public $show_init_pop_up = false;

    protected $listeners = ['expand-org-menu' => 'expandOrgMenu'];

    /**
     * Mount the component.
     *
     * @param string|null $view
     * @param string|null $stage
     * @param string|null $review_page
     * @return void
     */
    public function mount($view = null, $stage = null, $review_page = null): void
    {
        $this->view = $view ?? $this->view;
        $this->stage = $stage ?? $this->stage;
        $this->review_page = $review_page ?? $this->review_page;

        $this->setPopUpStatus();
        $this->getStage();
    }

    /**
     * Set the initial pop-up status.
     *
     * @return void
     */
    private function setPopUpStatus(): void
    {
        $user_id = Auth::id();
        $pop_up = PopUp::firstOrCreate(['user_id' => $user_id], ['organiser' => 0]);

        $this->show_init_pop_up = !$pop_up->organiser;
    }

    /**
     * Expand the organizer menu.
     *
     * @return void
     */
    public function expandOrgMenu(): void
    {
        $this->menu_size = '3';
    }

    /**
     * Get the current stage and redirect if necessary.
     *
     * @return \Illuminate\Routing\Redirector|\Illuminate\Http\RedirectResponse|null
     */
    private function getStage()
    {
        if ($this->view === 'online-will') {
            $valid_stages = ['profile', 'executor', 'spouse', 'minor-children-beneficiary', 'guardian', 'testamentary-trust', 'inheritance', 'specific-inheritance', 'remainder-inheritance', 'simultaneous-death', 'funeral-wishes', 'witnesses', 'review'];

            if (!in_array($this->stage, $valid_stages)) {
                $opts = WillOption::where('user_id', Auth::id())->first();

                if ($opts) {
                    if ($this->stage !== 'start') {
                        return redirect('organiser/online-will/start');
                    }
                } else {
                    return $this->checkInterviewStage();
                }
            }
        }

        return null;
    }

    /**
     * Check the interview stage and redirect if necessary.
     *
     * @return \Illuminate\Routing\Redirector|\Illuminate\Http\RedirectResponse|null
     */
    private function checkInterviewStage()
    {
        $int = Interview::where('user_id', Auth::id())->first();
        if ($int && $int->leave_possesions_to_icapitated_persons && $int->exclude_persons_from_will) {
            if ($this->stage !== 'stage-2') {
                return redirect('organiser/online-will/stage-2');
            }
        }

        return null;
    }

    /**
     * Render the component.
     *
     * @return \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory
     */
    public function render()
    {
        $comingSoon = $this->view === 'online-will';
        return view('livewire.account.organiser.organiser', [
        'comingSoon' => $comingSoon,
    ]);
    }

    /**
     * Mark the initial modal as seen.
     *
     * @return void
     */
    public function initModalSeen(): void
    {
        $pop_up = PopUp::where('user_id', Auth::id())->first();
        if ($pop_up) {
            $pop_up->organiser = 1;
            $pop_up->save();
        }
        $this->dispatchBrowserEvent('close-modals');
    }
}
