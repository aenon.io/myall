<?php

namespace App\Http\Livewire\Account\Organiser\Partials;

use Livewire\Component;

use App\Models\Interview;
use App\Models\WillOption;
use Auth;

class QuestionsFooter extends Component
{
    public function redoSesction()
    {
        $int = Interview::where('user_id', Auth::user()->id)->first();
        if ($int) {
            $int->delete();
            /* $int->have_property_outside_sa = 'No'; */
            /* $int->read_and_write_capability = 'Yes'; */
            /* $int->leave_possesions_to_icapitated_persons = null; */
            /* $int->exclude_persons_from_will = null; */
            /* $int->save(); */
        }
        $opt = WillOption::where('user_id', Auth::user()->id)->first();
        if ($opt) {
            $opt->delete();
        }
        return redirect('organiser/online-will/interview');
    }

    public function render()
    {
        return view('livewire.account.organiser.partials.questions-footer');
    }
}
