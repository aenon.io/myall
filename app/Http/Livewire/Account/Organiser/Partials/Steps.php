<?php

namespace App\Http\Livewire\Account\Organiser\Partials;

use Request;

use Livewire\Component;
use App\Lib\Sharedfunctions;
use Illuminate\Support\Facades\Log;

class Steps extends Component
{
    public $title, $stage, $stage_val;
    public $stage_images = [],
        $stage_vids = [];
    public $enable_save;
    public $enable_save_simultaneous;
    public $inheritance_type;

    /* protected $listeners = ['setPageView' => 'setPageView']; */
    protected $listeners = ['setPageView'];

    public function mount($enable_save = false, $enable_save_simultaneous = false, $inheritance_type = '')
    {
        $funcs = new Sharedfunctions();
        $progress = $funcs->getProgress();

        $segment = Request::segment(3);

        $stage_data = $this->getPageTitle($segment);
        if (isset($stage_data['title'])) {
            $this->title = $stage_data['title'];
        }

        if (isset($stage_data['stage_path'])) {
            $path = public_path() . '/infographics/' . $stage_data['stage_path'];
            $images = glob($path . '/*.jpg');
            $this->stage_images = $images;
        } else {
            $this->stage_images = [];
        }

        $info_images = 'mytheme/images/myimages';

        $this->stage = (int) $progress['stage'];
        $this->stage_val = $progress['stage_val'];

        $this->enable_save = $enable_save;
        $this->enable_save_simultaneous = $enable_save_simultaneous;
        $this->inheritance_type = $inheritance_type;
    }

    public function setPageView($type)
    {
        /* Log::info('Receiving setPageView listner with type: ' . $type); */
        $this->inheritance_type = $type;
    }

    public function showVid()
    {
        if (count($this->stage_vids) > 0) {
            $this->dispatchBrowserEvent('show-vids-modal');
        }
    }

    public function showSingleVid($name)
    {
        $name = asset('/vids/' . $name . '.mp4');
        $this->dispatchBrowserEvent('show-single-vid-modal', ['name' => $name]);
    }

    public function showInfo()
    {
        if (count($this->stage_images) > 0) {
            $this->dispatchBrowserEvent('show-images-modal');
        }
    }

    public function showSingleImage($image_name)
    {
        $path = url('/infographics/' . urldecode($image_name));
        $this->dispatchBrowserEvent('show-single-image-modal', ['image_path' => $path]);
    }

    public function getPageTitle($segment)
    {
        if ($segment == 'interview') {
            $arr = [
                'title' => 'Stage 1: interview',
                'stage_path' => 'stage1',
            ];
            return $arr;
        } elseif ($segment == 'stage-2') {
            $arr = [
                'title' => 'Stage 2: Profile Generator',
                'stage_path' => 'stage2/1',
            ];
            return $arr;
        } elseif ($segment == 'profile') {
            $arr = [
                'title' => 'Your Details',
                'stage_path' => 'stage2/profile',
            ];

            $this->stage_vids = ['What is a Will', 'Who may not inherit as a beneficiary in a valid Will', 'No original Will or only a copy is found', 'South African legal requirements for a valid Will', 'What happens if changes are made to the original signed Will', 'What you can include in your Will', 'Who pays the estate debts. Understanding the process', 'What are the disadvantages of dying without a Will'];
            return $arr;
        } elseif ($segment == 'executor') {
            $arr = [
                'title' => 'Executor (Compulsory)',
                'stage_path' => 'stage2/executor',
            ];
            $this->stage_vids = ['What is a Will', 'Who may not inherit as a beneficiary in a valid Will'];
            return $arr;
        } elseif ($segment == 'spouse') {
            $arr = [
                'title' => 'Spouse',
                'stage_path' => 'stage2/spouse',
            ];
            $this->stage_vids = ['What happens when you disinherit your spouse or dependent child in your Will', 'Do I need to change my Will after my divorce'];
            return $arr;
        } elseif ($segment == 'minor-children-beneficiary') {
            $arr = [
                'title' => 'Minor Children',
                'stage_path' => 'stage2/minor_children',
            ];
            $this->stage_vids = ['When are parents not considered guardians of their child', 'What is the role of the appointed guardian', 'What is the difference between a guardian of a minor and the Guardian Fund', 'When does the nomination of a guardian take effect', 'Will the guardian administer the inheritance of my minor child', 'How to apply for funds from the Guardian Fund', 'What you can include in your Will'];
            return $arr;
        } elseif ($segment == 'guardian') {
            $arr = [
                'title' => 'Guardians',
                'stage_path' => 'stage2/guardian',
            ];
            $this->stage_vids = ['When are parents not considered guardians of their child', 'What is the role of the appointed guardian', 'What is the difference between a guardian of a minor and the Guardian Fund', 'When does the nomination of a guardian take effect', 'Will the guardian administer the inheritance of my minor child', 'How to apply for funds from the Guardian Fund', 'What you can include in your Will'];
            return $arr;
        } elseif ($segment == 'testamentary-trust') {
            $arr = [
                'title' => 'Testamentary Trust',
                'stage_path' => 'stage2/testamentary_trust',
            ];
            $this->stage_vids = ['Understanding the rules for inheriting specific possessions', 'When does a testamentary trust come into operation and the procedure to appoint trustees', 'What you can include in your Will'];
            return $arr;
        } elseif ($segment == 'inheritance') {
            $arr = [
                'title' => 'Inheritance',
                'stage_path' => 'stage2/inheritance',
            ];
            $this->stage_vids = ['Do I have to name each possession in my Will', 'Understanding the rules for inheriting specific possessions', 'What you can include in your Will', 'Who pays the estate debts. Understanding the process', 'Understanding the different types of inheritances  heirs and legatees'];
            return $arr;
        } elseif ($segment == 'specific-inheritance') {
            $arr = [
                'title' => 'Specific Inheritance',
                'stage_path' => 'stage2/specific_inheritance',
            ];
            $this->stage_vids = ['Do I have to name each possession in my Will', 'Understanding the rules for inheriting specific possessions', 'What happens when you disinherit your spouse or dependent child in your Will', 'Who pays the estate debts. Understanding the process', 'What you can include in your Will', 'Understanding the different types of inheritances  heirs and legatees'];
            return $arr;
        } elseif ($segment == 'remainder-inheritance') {
            $arr = [
                'title' => 'Remainder Inheritance',
                'stage_path' => 'stage2/remainder_inheritance',
            ];
            $this->stage_vids = ['Do I have to name each possession in my Will', 'Understanding the rules for inheriting specific possessions', 'Understanding the different types of inheritances  heirs and legatees', 'What happens when you disinherit your spouse or dependent child in your Will', 'Who pays the estate debts. Understanding the process'];
            return $arr;
        } elseif ($segment == 'simultaneous-death') {
            $arr = [
                'title' => 'Simultaneous Death',
                'stage_path' => 'stage2/simultaneous_death',
            ];
            $this->stage_vids = ['Do I have to name each possession in my Will', 'Understanding the rules for inheriting specific possessions', 'Understanding the different types of inheritances  heirs and legatees', 'What happens when you disinherit your spouse or dependent child in your Will', 'Who pays the estate debts. Understanding the process'];
            return $arr;
        } elseif ($segment == 'funeral-wishes') {
            $arr = [
                'title' => 'Funeral Wishes',
                'stage_path' => 'stage2/funeral_wishes',
            ];
            return $arr;
        } elseif ($segment == 'witnesses') {
            $arr = [
                'title' => 'Witnesses',
                'stage_path' => 'stage2/witness',
            ];
            $this->stage_vids = ['Who may not inherit as a beneficiary in a valid Will', 'South African legal requirements for a valid Will'];
            return $arr;
        } elseif ($segment == 'review') {
            $seg2 = Request::segment(4);

            if ($seg2 == 'start-stage-four' || $seg2 == 'acknowledgement' || $seg2 == 'download-will') {
                $arr = [
                    'title' => 'Download & Sign',
                    'stage_path' => 'stage4',
                ];
                $this->stage_vids = ['Who may not inherit as a beneficiary in a valid Will', 'South African legal requirements for a valid Will', 'What are the disadvantages of dying without a Will'];
                return $arr;
            }

            if ($seg2 == 'sign-will') {
                $arr = [
                    'title' => 'Download & Sign',
                    /* 'title' => 'Stage 5: Sign Will', */
                    'stage_path' => 'stage4',
                ];
                $this->stage_vids = ['Who may not inherit as a beneficiary in a valid Will', 'No original Will or only a copy is found', 'South African legal requirements for a valid Will', 'What are the disadvantages of dying without a Will'];
                return $arr;
            }

            if ($seg2 == 'stage-6-a' || $seg2 == 'stage-6-b') {
                $arr = [
                    'title' => 'Download & Sign',
                    'stage_path' => 'stage6',
                ];
                $this->stage_vids = ['Who may not inherit as a beneficiary in a valid Will', 'No original Will or only a copy is found', 'South African legal requirements for a valid Will', 'What happens if changes are made to the original signed Will', 'What are the disadvantages of dying without a Will'];
                return $arr;
            }

            if ($seg2 == 'stage-7-a') {
                $arr = [
                    'title' => 'Download & Sign',
                    'stage_path' => 'stage7',
                ];
                $this->stage_vids = ['No original Will or only a copy is found', 'South African legal requirements for a valid Will', 'What happens if changes are made to the original signed Will', 'Do I need to change my Will after my divorce', 'Who pays the estate debts. Understanding the process', 'What are the disadvantages of dying without a Will'];
                return $arr;
            }

            if ($seg2 == 'complete') {
                $arr = [
                    'title' => 'Complete',
                    'stage_path' => null,
                ];
                return $arr;
            }

            if ($seg2 == 'spouse') {
                $arr = [
                    'title' => 'Spouse',
                    'stage_path' => null,
                ];
                return $arr;
            }

            if ($seg2 == 'trustees') {
                $arr = [
                    'title' => 'Stage 3: Review',
                    'stage_path' => 'stage3/testamentary_trust',
                ];
                $this->stage_vids = ['Understanding the rules for inheriting specific possessions', 'When does a testamentary trust come into operation and the procedure to appoint trustees'];
                return $arr;
            }
            if ($seg2 == 'guardians') {
                $arr = [
                    'title' => 'Stage 3: Review',
                    'stage_path' => 'stage3/guardians',
                ];
                $this->stage_vids = ['What is the role of the appointed guardian', 'When are parents not considered guardians of their child', 'When does the nomination of a guardian take effect', 'Will the guardian administer the inheritance of my minor child'];
                return $arr;
            }
            if ($seg2 == 'witnesses') {
                $arr = [
                    'title' => 'Stage 3: Review',
                    'stage_path' => 'stage3/witeness',
                ];
                $this->stage_vids = ['Who may not inherit as a beneficiary in a valid Will', 'South African legal requirements for a valid Will'];
                return $arr;
            }
            if ($seg2 == 'beneficiaries') {
                $arr = [
                    'title' => 'Stage 3: Review',
                    'stage_path' => 'stage3/inheritance',
                ];
                $this->stage_vids = ['Do I have to name each possession in my Will', 'Understanding the rules for inheriting specific possessions', 'What you can include in your Will', 'Who pays the estate debts. Understanding the process', 'Understanding the different types of inheritances  heirs and legatees'];
                return $arr;
            }
            if ($seg2 == 'executor') {
                $arr = [
                    'title' => 'Executor',
                    'stage_path' => 'stage3/executor',
                ];
                $this->stage_vids = ['What is a Will', 'Who may not inherit as a beneficiary in a valid Will', 'South African legal requirements for a valid Will'];
                return $arr;
            }

            $arr = [
                'title' => 'Stage 3: Review',
                'stage_path' => 'stage3/start',
            ];
            $this->stage_vids = ['Who may not inherit as a beneficiary in a valid Will', 'South African legal requirements for a valid Will', 'What happens if changes are made to the original signed Will', 'What you can include in your Will', 'What are the disadvantages of dying without a Will'];
            return $arr;
        }
    }

    public function render()
    {
        return view('livewire.account.organiser.partials.steps');
    }
}
