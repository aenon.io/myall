<?php

namespace App\Http\Livewire\Account\Organiser\Personal\Forms;

use Livewire\Component;
use Livewire\WithFileUploads;

use Illuminate\Support\Facades\Auth as Auth;
use App\Models\Spouse;

class Spouses extends Component
{
    use WithFileUploads;

    public $spouses = [];
    public $marital_statuses,
        $master_offices,
        $filter_marital_status = [];
    public $isNeverMarried = false,
        $multiple_partners = false;

    public function mount()
    {
        if ((Auth::user()->ethnicity == 'Black' || Auth::user()->ethnicity == 'Indian') && Auth::user()->gender == 'Male') {
            $this->multiple_partners = true;
        }
        $this->getData();
        $this->setStaticData();
    }

    public function removeSposeDocument($type, $id)
    {
        $sp = Spouse::find($id);
        if ($sp) {
            $sp->$type = null;
            $sp->save();
        }
        $this->getData();
    }

    public $tip_title, $tip_body;
    public function showTipModal($type)
    {
        if ($type == 'ethnicity') {
            $this->tip_title = 'Ethnicity';
            $this->tip_body = 'Choosing your ethnicity is important when creating your online Will as it helps us determine your marital status and ensures compliance with South African law. South African law recognises specific marital structures based on culture and race.';
        } elseif ($type == 'gender') {
            $this->tip_title = 'Gender';
            $this->tip_body = 'This helps us when creating your Online Will';
        } elseif ($type == 'spouse') {
            $this->tip_title = 'Spouse';
            $this->tip_body = "It's essential to provide all your marital details, including past marriages, divorces, and the death of a spouse. This will help your loved ones when reporting your estate to the Master of the High Court, as they will require this information upon your passing.";
        }
        $this->dispatchBrowserEvent('show-tip-modal');
    }

    public function removeSpouse($id)
    {
        $sp = Spouse::find($id);
        if ($sp) {
            $sp->delete();
        }
        $this->getData();
    }

    public function saveSpouse()
    {
        foreach ($this->spouses as $s => $spouse) {
            $rules = [
                'spouses.' . $s . '.divorce_agreement' => 'nullable|file|mimes:pdf,jpeg,png|max:2048',
                'spouses.' . $s . '.marriage_certificate' => 'nullable|file|mimes:pdf,jpeg,png|max:2048',
                'spouses.' . $s . '.antenuptial_contract' => 'nullable|file|mimes:pdf,jpeg,png|max:2048',
                'spouses.' . $s . '.death_certificate' => 'nullable|file|mimes:pdf,jpeg,png|max:2048',
                'spouses.' . $s . '.will' => 'nullable|file|mimes:pdf,jpeg,png|max:2048',
                'spouses.' . $s . '.court_order_and_agreement' => 'nullable|file|mimes:pdf,jpeg,png|max:2048',
                'spouses.' . $s . '.proof_of_maintenance_order' => 'nullable|file|mimes:pdf,jpeg,png|max:2048',
                'spouses.' . $s . '.specific_agreements_file' => 'nullable|file|mimes:pdf,jpeg,png|max:2048',
                'spouses.' . $s . '.domestic_agreement' => 'nullable|file|mimes:pdf,jpeg,png|max:2048',
            ];

            $validatedData = $this->validate(
                $rules,
                [],
                [
                    'spouses.' . $s . '.divorce_agreement' => 'divorce agreement',
                    'spouses.' . $s . '.marriage_certificate' => 'marriage certificate',
                    'spouses.' . $s . '.antenuptial_contract' => 'antenuptial contract',
                    'spouses.' . $s . '.death_certificate' => 'death certificate',
                    'spouses.' . $s . '.will' => 'will',
                    'spouses.' . $s . '.court_order_and_agreement' => 'court order and agreement',
                    'spouses.' . $s . '.proof_of_maintenance_order' => 'proof of maintenance order',
                    'spouses.' . $s . '.specific_agreements_file' => 'specific agreements',
                    'spouses.' . $s . '.domestic_agreement' => 'agreement',
                ],
            );

            if (array_key_exists('id', $spouse)) {
                $sp = Spouse::find($spouse['id']);
            } else {
                $sp = new Spouse();
            }

            if ($spouse['marital_status']) {
                $sp->user_id = Auth::user()->id;
                $sp->marital_status = $spouse['marital_status'];
                $sp->spouse_name = $spouse['spouse_name'];
                $sp->spouse_surname = $spouse['spouse_surname'];
                $sp->spouse_middle_name = $spouse['spouse_middle_name'];
                $sp->spouse_id_number = $spouse['spouse_id_number'];
                $sp->date_of_death = $spouse['date_of_death'];
                $sp->masters_office = $spouse['masters_office'];
                $sp->estate_number = $spouse['estate_number'];
                $sp->attorney_auditor_trust_contact_details = $spouse['attorney_auditor_trust_contact_details'];
                $sp->estate_wrapping_up_company = $spouse['estate_wrapping_up_company'];
                $sp->dweling_property = $spouse['dweling_property'];
                $sp->usufruct = $spouse['usufruct'];
                $sp->date_of_divorce = $spouse['date_of_divorce'];
                $sp->maintenance_order = $spouse['maintenance_order'];
                $sp->specific_agreements_description = $spouse['specific_agreements_description'];
                $sp->comunity_marriage = $spouse['comunity_marriage'];
                $sp->accrual = $spouse['accrual'];
                $sp->executor = $spouse['executor'];

                if ($spouse['divorce_agreement']) {
                    $divorce_agreement_path = $spouse['divorce_agreement']->storePublicly('documents', 'public');
                    $sp->divorce_agreement = $divorce_agreement_path;
                }

                if ($spouse['marriage_certificate']) {
                    $marriage_certificate_path = $spouse['marriage_certificate']->storePublicly('documents', 'public');
                    $sp->marriage_certificate = $marriage_certificate_path;
                }

                if ($spouse['antenuptial_contract']) {
                    $antenuptial_contract_path = $spouse['antenuptial_contract']->storePublicly('documents', 'public');
                    $sp->antenuptial_contract = $antenuptial_contract_path;
                }

                if ($spouse['death_certificate']) {
                    $death_certificate_path = $spouse['death_certificate']->storePublicly('documents', 'public');
                    $sp->death_certificate = $death_certificate_path;
                }
                /*
                if($spouse['will']){
                    $will_path = $spouse['will']->storePublicly('documents', 'public');
                    $sp->will = $will_path;
                }
                */

                if ($spouse['court_order_and_agreement']) {
                    $court_order_and_agreement_path = $spouse['court_order_and_agreement']->storePublicly('documents', 'public');
                    $sp->court_order_and_agreement = $court_order_and_agreement_path;
                }

                if ($spouse['proof_of_maintenance_order']) {
                    $proof_of_maintenance_order_path = $spouse['proof_of_maintenance_order']->storePublicly('documents', 'public');
                    $sp->proof_of_maintenance_order = $proof_of_maintenance_order_path;
                }
                /*
                if($spouse['specific_agreements_file']){
                    $specific_agreements_file_path = $spouse['specific_agreements_file']->storePublicly('documents', 'public');
                    $sp->specific_agreements_file = $specific_agreements_file_path;
                }
                */

                if ($spouse['domestic_agreement']) {
                    $domestic_agreement_path = $spouse['domestic_agreement']->storePublicly('documents', 'public');
                    $sp->domestic_agreement = $domestic_agreement_path;
                }
                $sp->save();
            }
        }
        $this->dispatchBrowserEvent('spouse-updated', ['message' => 'Successfully saved.']);
        // return redirect('organiser/personal');
    }

    public function getData()
    {
        $this->spouses = [];
        $spouses = Spouse::where('user_id', Auth::user()->id)->get();
        if ($spouses->count() == 0) {
            $this->addSpouse();
        } else {
            foreach ($spouses as $sp) {
                if ($sp->marital_status == 'Never Married') {
                    $this->isNeverMarried = true;
                }
                $arr = [
                    'id' => $sp->id,
                    'marital_status' => $sp->marital_status,
                    'spouse_name' => $sp->spouse_name,
                    'spouse_middle_name' => $sp->spouse_middle_name,
                    'spouse_surname' => $sp->spouse_surname,
                    'spouse_id_number' => $sp->spouse_id_number,
                    'date_of_death' => $sp->date_of_death,
                    'masters_office' => $sp->masters_office,
                    'estate_number' => $sp->estate_number,
                    'attorney_auditor_trust_contact_details' => $sp->attorney_auditor_trust_contact_details,
                    'estate_wrapping_up_company' => $sp->estate_wrapping_up_company,
                    'dweling_property' => $sp->dweling_property,
                    'usufruct' => $sp->usufruct,
                    'date_of_divorce' => $sp->date_of_divorce,
                    'specific_agreements_description' => $sp->specific_agreements_description,
                    'comunity_marriage' => $sp->comunity_marriage,
                    'accrual' => $sp->accrual,
                    'executor' => $sp->executor,
                    'maintenance_order' => $sp->maintenance_order,

                    'death_certificate' => '',
                    'divorce_agreement' => '',
                    'court_order_and_agreement' => '',
                    'proof_of_maintenance_order' => '',
                    'domestic_agreement' => '',
                    'marriage_certificate' => '',
                    'antenuptial_contract' => '',

                    'marriage_certificate_file' => $sp->marriage_certificate,
                    'antenuptial_contract_file' => $sp->antenuptial_contract,
                    'domestic_agreement_file' => $sp->domestic_agreement,
                    'court_order_and_agreement_file' => $sp->court_order_and_agreement,
                    'divorce_agreement_file' => $sp->divorce_agreement,
                    'proof_of_maintenance_order_file' => $sp->proof_of_maintenance_order,
                    'death_certificate_file' => $sp->death_certificate,
                ];
                $this->spouses[] = $arr;
            }
        }
    }

    public function addSpouse()
    {
        $arr = [
            'marital_status' => '',
            'spouse_name' => '',
            'spouse_middle_name' => '',
            'spouse_surname' => '',
            'spouse_id_number' => '',
            'marriage_certificate' => '',
            'antenuptial_contract' => '',
            'date_of_death' => '',
            'masters_office' => '',
            'estate_number' => '',
            'attorney_auditor_trust_contact_details' => '',
            'estate_wrapping_up_company' => '',
            'dweling_property' => '',
            'usufruct' => '',
            'death_certificate' => '',
            'will' => '',
            'date_of_divorce' => '',
            'court_order_and_agreement' => '',
            'maintenance_order' => '',
            'proof_of_maintenance_order' => '',
            'specific_agreements_description' => '',
            'specific_agreements_file' => '',
            'comunity_marriage' => '',
            'accrual' => '',
            'executor' => '',
            'divorce_agreement' => '',
            'domestic_agreement' => '',

            'marriage_certificate_file' => '',
            'antenuptial_contract_file' => '',
            'domestic_agreement_file' => '',
            'court_order_and_agreement_file' => '',
            'divorce_agreement_file' => '',
            'proof_of_maintenance_order_file' => '',
            'death_certificate_file' => '',
        ];
        $this->spouses[] = $arr;
        if ((Auth::user()->ethnicity == 'Black' || Auth::user()->ethnicity == 'Indian') && Auth::user()->gender == 'Male') {
            $this->dispatchBrowserEvent('show-spouse-info-modal');
        }
    }

    public function setStaticData()
    {
        $this->masters_offices = ['Bloemfontein', 'Cape Town', 'Grahamstown (Makhanda)', 'Kimberley', 'Mmabatho/Mafikeng', 'Nelspruit', 'Pietermaritzburg', 'Pretoria', 'Mthatha (Umtata)', 'Bisho', 'Thohoyandou', 'Johannesburg', 'Polokwane', 'Durban', 'Port Elizabeth (Gqeberha)'];
        sort($this->masters_offices);

        $this->marital_statuses = [
            'Never Married' => 'Someone who has never been married.',
            'Now Married' => 'Now Married relates to your current marital status as defined by the Marriage Act or Civil Union Act and registered with Home Affairs.',
            'Domestic Partnership' => 'Domestic Partnership, also called a Permanent Life Partnership, is when partners are committed to living together and supporting each other.',
            'Separated' => 'Separated refers to a situation where you are married but not living together, and the marriage has not yet been dissolved by a divorce court order..',
            'Engaged' => 'Engaged refers to the state where you are presently formally committed to getting married or entering an intended marriage.',
            'Widowed' => 'Widowed refers to the dissolution of your marriage or previous marriage due to the passing of your spouse.',
            'Divorced' => 'Divorced refers to the dissolution of your marriage or previous marriage through a divorce court order.',
        ];
        $spouses = Spouse::where('user_id', Auth::user()->id)->get();
        if ($spouses->count() > 0) {
            if (!$this->multiple_partners) {
                //Check Married
                if (
                    Spouse::where('user_id', Auth::user()->id)
                        ->where('marital_status', 'Now Married')
                        ->first()
                ) {
                    $this->filter_marital_status = ['Never Married', 'Now Married', 'Engaged', 'Domestic Partnership'];
                } elseif (
                    Spouse::where('user_id', Auth::user()->id)
                        ->where('marital_status', '<>', 'Now Married')
                        ->first()
                ) {
                    $this->filter_marital_status = ['Never Married', 'Engaged', 'Domestic Partnership'];
                } else {
                    $this->filter_marital_status = ['Never Married', 'Engaged', 'Domestic Partnership'];
                }
            } else {
                $this->filter_marital_status = ['Never Married'];
            }
        }
    }

    public function render()
    {
        return view('livewire.account.organiser.personal.forms.spouses');
    }
}
