<?php

namespace App\Http\Livewire\Account\Organiser\Personal\Forms;

use Illuminate\Support\Facades\Auth as Auth;
use Illuminate\Support\Facades\Mail as Mail;
use Livewire\Component;
use App\Models\TrustedContact;

class TrustedContacts extends Component
{
    public $name, $surname, $nickname, $id_number, $dob, $address, $other_info, $additional_info, $terms_and_conditions;

    public function mount()
    {
        $this->getData();
    }

    public function getData()
    {
        $cnt = TrustedContact::where('user_id', Auth::user()->id)->first();
        if ($cnt) {
            $this->name = $cnt->name;
            $this->surname = $cnt->surname;
            $this->nickname = $cnt->nickname;
            $this->id_number = $cnt->id_number;
            $this->dob = $cnt->dob;
            $this->address = $cnt->address;
            $this->other_info = $cnt->other_info;
            $this->additional_info = $cnt->additional_info;
            $this->terms_and_conditions = $cnt->terms_and_conditions;
        }
    }

    public function saveContact($notify = null)
    {
        $this->validate([
            'name' => 'required',
            'surname' => 'required',
            'id_number' => 'required',
            'dob' => 'required',
            'address' => 'required',
            'terms_and_conditions' => 'required',
        ]);

        $cnt = TrustedContact::where('user_id', Auth::user()->id)->first();
        if (!$cnt) {
            $cnt = new TrustedContact();
        }

        $cnt->user_id = Auth::user()->id;
        $cnt->name = $this->name;
        $cnt->surname = $this->surname;
        $cnt->nickname = $this->nickname;
        $cnt->id_number = $this->id_number;
        $cnt->dob = $this->dob;
        $cnt->address = $this->address;
        $cnt->other_info = $this->other_info;
        $cnt->additional_info = $this->additional_info;
        $cnt->terms_and_conditions = $this->terms_and_conditions;
        $cnt->save();
        $this->dispatchBrowserEvent('trusted-rep-updated', ['message' => 'Successfully saved.']);

        if ($notify) {
            $mail_content =
                '
            <div class="">' .
                Auth::user()->name .
                ' ' .
                Auth::user()->middle_name .
                ' ' .
                Auth::user()->surname .
                ' has added their trusted contact</div>
            <table>
                <tr>
                    <td><b>Account Name</b><br />' .
                Auth::user()->name .
                ' ' .
                Auth::user()->middle_name .
                ' ' .
                Auth::user()->surname .
                '</td>
                </tr>
                <tr>
                    <td><b>Account ID Number</b><br />' .
                Auth::user()->id_number .
                '</td>
                </tr>
                <tr>
                    <td><b>Contact Name</b><br />' .
                $this->name .
                ' ' .
                $this->surname .
                '</td>
                </tr>
                <tr>
                    <td><b>Contact Nickname</b><br />' .
                $this->nickname .
                '</td>
                </tr>
                <tr>
                    <td><b>Contact ID Number</b><br />' .
                $this->id_number .
                '</td>
                </tr>
                <tr>
                    <td><b>Contact Date Of Birth</b><br />' .
                $this->dob .
                '</td>
                </tr>
                <tr>
                    <td><b>Contact Address</b><br />' .
                $this->address .
                '</td>
                </tr>
                <tr>
                    <td><b>Other Information</b><br />' .
                $this->other_info .
                '</td>
                </tr>
                <tr>
                    <td><b>Additional Information</b><br />' .
                $this->additional_info .
                '</td>
                </tr>
            </table>
            <br />
            ';

            $data = [
                'name' => 'MyAll Admin',
                'content' => $mail_content,
            ];
            $to = 'admin@myall.co.za';

            Mail::send('mail.notify_plain', $data, function ($message) use ($to) {
                $message->to($to, '')->subject('MyAll Trusted Contact Added');
                $message->from('hello@myall.co.za', 'MyAll');
            });
        }
    }

    public function render()
    {
        return view('livewire.account.organiser.personal.forms.trusted-contacts');
    }
}
