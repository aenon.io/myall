<?php

namespace App\Http\Livewire\Account\Organiser\Personal;

use Livewire\Component;
use Livewire\WithFileUploads;

use Auth;
use App\Models\User;
use App\Models\Spouse;
use App\Models\PlaceOfWork;
// use App\Models\Tax;
use App\Models\Child;
// use App\Models\ItemToFind;
use App\Models\MedicalAid;
use App\Models\Dependents;
use App\Models\FuneralCover;
use App\Models\Education;
use App\Models\MedicalAidCondition;

class Personal extends Component
{
    use WithFileUploads;

    public $marital_statuses, $master_offices, $qualifications;

    //Me fields
    public $name, $surname, $id_number, $passport_number, $gender, $ethnicity, $street_address, $city, $province, $birth_certificate, $id_document, $passport_document, $drivers_license_document, $power_of_attorney_document, $separate_power_of_attorney_document;
    //Spouse Fields
    public $marital_status, $spouse_name, $spouse_surname, $spouse_id_number, $marriage_certificate, $antenuptial_contract, $date_of_death, $masters_office, $estate_number, $attorney_auditor_trust_contact_details, $estate_wrapping_up_compan, $usufruct;
    public $dweling_property, $death_certificate, $will, $date_of_divorce, $court_order_and_agreement, $maintenance_order, $proof_of_maintenance_order, $specific_agreements_description, $specific_agreements_file, $comunity_marriage, $accrual, $executor;
    //Place Of Work Fields
    public $employer_name,$employer_address,$salary_number;
    //Tax Fields
    // public $tax_number,$tax_assessment,$tax_certificate;
    //Children fields
    public $children = [], $child_name, $middle_name, $child_surname, $child_id_number, $child_date_of_birth, $child_birth_certificate,$child_adoption_certificate;
    //Items To Fields
    // public $items_to_find = [], $item_name, $location;
    //Medical Aid Fields
    public $reference_number, $policy_type,$company,$medical_aid_certificate, $m_name, $m_surname, $m_id_number, $conditions;
    public $medical_aids = [];
    //Funeral Cover
    public $f_company, $policy_number, $policy_holder, $coverage_amount;
    
    public $marriage_certificate_file, $antenuptial_contract_file, $death_certificate_file, $will_file, $court_order_and_agreement_file, $proof_of_maintenance_order_file, $specific_agreements_file_file, $domestic_agreement, $domestic_agreement_file;
    
    public $tax_assesment_file, $tax_file, $medical_aid_certificate_file;
    
    //Items To Fields
    public $dependents = [], $add_dependents = [];

    public $spouses = [];

    public $educations = [];

    public $qualification, $year, $comment, $certificate;

    public $tip_body, $tip_title;

    public $dependent_select = [];
    public $masters_offices = [];
    public $select_id;
    public $isMarried, $isDomestic, $isSeparated, $isEngaged, $isNeverMarried = false, $isWidowed, $isDivorced;
    public $provinces = [];

    public function mount(){
        
        $this->masters_offices = [
            'Bloemfontein',   
            'Cape Town',   
            'Grahamstown (Makhanda)',
            'Kimberley' , 
            'Mmabatho/Mafikeng',
            'Nelspruit', 
            'Pietermaritzburg', 
            'Pretoria', 
            'Mthatha (Umtata)', 
            'Bisho', 
            'Thohoyandou',  
            'Johannesburg',
            'Polokwane', 
            'Durban',
            'Port Elizabeth (Gqeberha)',
        ];
        
        $this->qualifications = [
            'Matric',
            'Diploma',
            'Degree',
            'Other',
            'None',
        ];

        $this->provinces = [
            'Gauteng', 'Mpumalanga', 'Limpopo', 'Eastern Cape', 'Western Cape', 'North West', 'Free State', 'KwaZulu Natal', 'Northen Cape'
        ];
        sort($this->provinces);

        
        $this->marital_status = "Never Married";
        $this->addChild();
        $this->addSpouse();
        $this->spouseOptions();
        $this->addEducation();
        $this->setInitValues();
    }
    
    public function spouseOptions(){
        $this->marital_statuses = [];
        $hasSpouses = Auth::user()->spouses;
        if($hasSpouses->count() == 0 || (Auth::user()->ethnicity == "Black" && Auth::user()->gender == "Male") || (Auth::user()->ethnicity == "Indian" && Auth::user()->gender == "Male")){
            $this->marital_statuses = [
                ['Someone who has never been married.' => 'Never Married'],
                ['Now Married relates to your current marital status as defined by the Marriage Act or Civil Union Act and registered with Home Affairs.' => 'Now Married'],
                ['Domestic Partnership, also called a Permanent Life Partnership, is when partners are committed to living together and supporting each other.' => 'Domestic Partnership'],
                ['Separated refers to a situation where you are married but not living together, and the marriage has not yet been dissolved by a divorce court order..' => 'Separated'],
                ['Engaged refers to the state where you are presently formally committed to getting married or entering an intended marriage.' => 'Engaged'],
                ['Widowed refers to the dissolution of your marriage or previous marriage due to the passing of your spouse.' => 'Widowed'],
                ['Divorced refers to the dissolution of your marriage or previous marriage through a divorce court order.' => 'Divorced'],
            ];
        
        }elseif($hasSpouses && Auth::user()->ethnicity !== "Black" && Auth::user()->ethnicity !== "Indian") {
            foreach($hasSpouses as $sp){    
                if($sp->marital_status == 'Never Married'){
                    $this->isNeverMarried = true;
                }
                if($sp->marital_status == 'Now Married'){
                    $this->isMarried = true;
                }
                if($sp->marital_status == 'Domestic Partnership'){
                    $this->isDomestic = true;
                }
                if($sp->marital_status == 'Separated'){
                    $this->isSeparated = true;
                }
                if($sp->marital_status == 'Engaged'){
                    $this->isEngaged = true;
                }
                if($sp->marital_status == 'Widowed'){
                    $this->isWidowed = true;
                }
                if($sp->marital_status == 'Divorced'){
                    $this->isDivorced = true;
                }
            }
            
            if($this->isMarried || $this->isDomestic || $this->isSeparated || $this->isEngaged){
                // $this->marital_statuses = [];
                $this->marital_statuses = [
                    ['Late/Previous spouse passed away ' => 'Widowed'],
                    ['Divorce means a marriage dissolved through a divorce court order.' => 'Divorced'],    
                ];
            }
            if($this->isWidowed || $this->isDivorced){
                $this->marital_statuses = [
                    ['Marriage, as defined by the Marriage Act or Civil Union Act and registered at Home Affairs.' => 'Now Married'],
                    ['When partners live together and commit to supporting each other mutually.' => 'Domestic Partnership'],
                    ['Married but not living together, with the marriage not yet dissolved by means of a divorce court order.' => 'Separated'],
                    ['Being formally committed to getting married or entering into an intended marriage.' => 'Engaged'],
                    ['Late/Previous spouse passed away ' => 'Widowed'],
                    ['Divorce means a marriage dissolved through a divorce court order.' => 'Divorced'],
                ];
            }
        }
    }

    public function showTipModal($type){
        if($type == "ethnicity"){
            $this->tip_title = "Ethnicity";
            $this->tip_body = "Choosing your ethnicity is important when creating your online Will as it helps us determine your marital status and ensures compliance with South African law. South African law recognises specific marital structures based on culture and race.";
        }
        elseif($type == "gender"){
            $this->tip_title = "Gender";
            $this->tip_body = "This helps us when creating your Online Will";
        }
        elseif($type == "spouse"){
            $this->tip_title = "Spouse";
            $this->tip_body = "It's essential to provide all your marital details, including past marriages, divorces, and the death of a spouse. This will help your loved ones when reporting your estate to the Master of the High Court, as they will require this information upon your passing.";
        }
        $this->dispatchBrowserEvent('show-tip-modal');
    }
    
    public function setInitValues(){
        //Set Me Values
        $usr = Auth::user();
        $this->name = $usr->name; 
        $this->surname = $usr->surname; 
        $this->id_number = $usr->id_number; 
        $this->passport_number = $usr->passport_number;
        $this->street_address = $usr->street_address; 
        $this->city = $usr->city;
        $this->province = $usr->province;
        $this->gender = $usr->gender;
        $this->ethnicity = $usr->ethnicity;

        //Set Spouse Values
        $sps = Spouse::where('user_id', Auth::user()->id)->get();
        if(count($sps) > 0){
            $this->spouses = [];
            foreach($sps as $sp){
                $arr = [
                    'id' => $sp->id,
                    'marital_status' => $sp->marital_status,
                    'spouse_name' => $sp->spouse_name,
                    'spouse_surname' => $sp->spouse_surname,
                    'spouse_middle_name' => $sp->spouse_middle_name,
                    'spouse_id_number' => $sp->spouse_id_number, 
                    'date_of_death' => $sp->date_of_death, 
                    'masters_office' => $sp->masters_office, 
                    'estate_number' => $sp->estate_number, 
                    'attorney_auditor_trust_contact_details' => $sp->attorney_auditor_trust_contact_details, 
                    'estate_wrapping_up_company' => $sp->estate_wrapping_up_company, 
                    'dweling_property' => $sp->dweling_property,  
                    'usufruct' => $sp->usufruct,
                    'date_of_divorce' => $sp->date_of_divorce, 
                    'maintenance_order' => $sp->maintenance_order,  
                    'specific_agreements_description' => $sp->specific_agreements_description, 
                    'comunity_marriage' => $sp->comunity_marriage, 
                    'accrual' => $sp->accrual,
                    'executor' => $sp->executor,
                    'marriage_certificate' => '',
                    'antenuptial_contract' => '',
                    'death_certificate' => '',
                    'will' => '',
                    'court_order_and_agreement' => '',
                    'divorce_agreement' => '',
                    'proof_of_maintenance_order' => '',
                    'specific_agreements_file' => '',
                    'domestic_agreement' => '',

                    'marriage_certificate_file' => $sp->marriage_certificate,
                    'antenuptial_contract_file' => $sp->antenuptial_contract,
                    'death_certificate_file' => $sp->death_certificate,
                    'will_file' => $sp->will,
                    'court_order_and_agreement_file' => $sp->court_order_and_agreement,
                    'proof_of_maintenance_order_file' => $sp->proof_of_maintenance_order,
                    'specific_agreements_file_file' => $sp->specific_agreements_file,
                    'domestic_agreement_file' => $sp->domestic_agreement,
                    'divorce_agreement_file' => $sp->divorce_agreement,
                ];

                $this->spouses[] = $arr;
            }
        }

        $edu = Education::where('user_id', Auth::user()->id)->get();
        if(count($edu) > 0){
            $this->educations = [];

            foreach($edu as $e){
                $arr = [
                    'id' => $e->id,
                    'user_id' => $e->user,
                    'qualification' => $e->qualification,
                    'year' => $e->year,
                    'comment' => $e->comment,
                    'certificate' => '',
                    'certificate_file' => $e->certificate
                ];
                $this->educations[] = $arr;

            }
        }
        
        //Set Place Of Work Values
        $work = PlaceOfWork::where('user_id', Auth::user()->id)->first();
        if($work){
            $this->employer_name = $work->employer_name;
            $this->employer_address = $work->employer_address;
            $this->salary_number = $work->salary_number;
        }
        
        //Set children Values
        $children = Child::where('user_id', Auth::user()->id)->get();
        if($children->count() > 0){
            $this->children = [];
            foreach($children AS $child){
                $arr = [
                    'id' => $child->id,
                    'child_name' => $child->child_name, 
                    'middle_name' => $child->middle_name,
                    'child_surname' => $child->child_surname, 
                    'child_id_number' => $child->child_id_number,
                    'child_date_of_birth' => $child->child_date_of_birth, 
                    'child_birth_certificate' => '',
                    'child_adoption_certificate' => '',
                    
                    'child_birth_certificate_file' => $child->child_birth_certificate,
                    'child_adoption_certificate_file' => $child->child_adoption_certificate
                ];
                $this->children[] = $arr;
            }
        }
        
        //Set Medical Aid Values
        $aids = MedicalAid::where('user_id', Auth::user()->id)->get();
        $this->medical_aids = [];
        if($aids->count() > 0){
            foreach($aids AS $aid){
                $arr = [
                    'id' => $aid->id,
                    'reference_number' => $aid->reference_number, 
                    'policy_type' => $aid->policy_type,
                    'company' => $aid->company,
                    'medical_aid_certificate' => '',
                    'medical_aid_certificate_file' => $aid->medical_aid_certificate, 
                    'name' => $aid->name, 
                    'surname' => $aid->surname, 
                    'id_number' => $aid->id_number, 
                ];

                $cnds = $aid->sub_conditions;
                if($cnds->count() > 0){
                    foreach($cnds AS $cnd){
                        $cnd_arr = [
                            'id' => $cnd->id,
                            'condition' => $cnd->condition,
                        ];
                        $arr['conditions'][] = $cnd_arr;
                    }
                }
                else{
                    $arr['conditions'] = ['condition' => ''];
                }

                $dps = $aid->dependants;
                foreach($dps AS $dp){
                    $dp_arr = [
                        'id' => $dp->id,
                        'name' => $dp->name,
                        'surname'=> $dp->surname,
                        'id_number' => $dp->id_number,
                        'condition' => $dp->condition
                    ];

                    $arr['dependents'][] = $dp_arr;
                }
                $this->medical_aids[] = $arr;
            }
        }
        else{
            $this->addMedicalaid();
        }
        
        $f_cover = FuneralCover::where('user_id', Auth::user()->id)->first();
        if($f_cover){
            $this->policy_number = $f_cover->policy_number;
            $this->policy_holder = $f_cover->policy_holder;
            $this->f_company = $f_cover->company;
            $this->coverage_amount = $f_cover->coverage_amount;
        }
        
        //Set Dependents Values
        $dependents = Dependents::where('user_id', Auth::user()->id)->get();
        if($dependents->count() > 0){
            $this->add_dependents = [];
            foreach($dependents AS $dependent){
                $arr = [
                    'id' => $dependent->id,
                    'd_name' => $dependent->name,
                    'd_surname' => $dependent->surname,
                    'd_id_number' => $dependent->id_number
                ];
                $this->add_dependents[] = $arr;
            }
        }
    }

    public function selectOptions(){
        $children_sel = Child::where('user_id', Auth::user()->id)->get();
        $spouse_sel = Spouse::where('user_id', Auth::user()->id)->where('marital_status', 'Now Married')->get();
        $dependents_sel = Dependents::where('user_id', Auth::user()->id)->get();

        $this->dependent_select = [];
        if($children_sel->count() > 0){
            foreach($children_sel AS $option){
                $optionAdded = false;
                foreach($dependents_sel as $sel){
                    if ($sel->id_number == $option->child_id_number) {
                        //$optionAdded = true;
                        break;
                    }
                }
                $id_no = $option->child_id_number;
                if(!$id_no){
                    $id_no = $option->child_date_of_birth;
                }
                if(!$optionAdded){
                    $arr = [
                        'option_id' => $id_no,
                        'option_name' => $option->child_name,
                        'option_surname' => $option->child_surname
                    ];
                    $this->dependent_select[] = $arr;
                }

            }
        }

        if($spouse_sel->count() > 0){
            foreach($spouse_sel AS $option){
                $optionAdded = false;
                foreach($dependents_sel as $sel){
                    if ($sel->id_number == $option->spouse_id_number) {
                        //$optionAdded = true;
                        break;
                    }
                }

                if(!$optionAdded){
                    $arr = [
                        'option_id' => $option->spouse_id_number,
                        'option_name' => $option->spouse_name,
                        'option_surname' => $option->spouse_surname
                    ];
                    $this->dependent_select[] = $arr;
                }
            }
        }
    }

    public function deleteCondition($id){
        $cnd = MedicalAidCondition::find($id);
        if($cnd){
            $cnd->delete();
        }
        $this->setInitValues();
    }

    public function addMedicalaid(){
        $arr = [
            'reference_number' => '', 
            'policy_type' => '',
            'company' => '',
            'medical_aid_certificate' => '', 
            'name' => '', 
            'surname' => '', 
            'id_number' => '', 
            'conditions' => ['condition' => ''],
            'dependents' => []
        ];
        $this->medical_aids[] = $arr;
    }

    public function addCondition($key){
        $arr = [
            'condition' => ''
        ];
        $this->medical_aids[$key]['conditions'][] = $arr;
    }

    public function addEducation(){
        $arr = [
            'qualification' => '',
            'year' => '',
            'comment' => '',
            'certificate' => '',
            'certificate_file' => ''
        ];

        $this->educations[] = $arr;
    }

    public function addSpouse(){
        $arr = [
            'marital_status' => '',
            'spouse_name' => '',
            'spouse_surname' => '', 
            'spouse_middle_name' => '',
            'spouse_id_number' => '', 
            'date_of_death' => '', 
            'masters_office' => '', 
            'estate_number' => '', 
            'attorney_auditor_trust_contact_details' => '', 
            'estate_wrapping_up_company' => '', 
            'dweling_property' => '',  
            'usufruct' => '',
            'date_of_divorce' => '', 
            'maintenance_order' => '',  
            'specific_agreements_description' => '', 
            'comunity_marriage' => '', 
            'accrual' => '',
            'executor' => '',
            'marriage_certificate' => '',
            'antenuptial_contract' => '',
            'death_certificate' => '',
            'will' => '',
            'court_order_and_agreement' => '',
            'divorce_agreement' => '',
            'proof_of_maintenance_order' => '',
            'specific_agreements_file' => '',
            'domestic_agreement' => '',
        ];

        $this->spouseOptions();
        $this->spouses[] = $arr;
        if((Auth::user()->gender == "Male" && Auth::user()->ethnicity == "Black") || (Auth::user()->gender == "Male" && Auth::user()->ethnicity == "Indian")){
            $this->dispatchBrowserEvent('show-spouse-info-modal');    
        }
    }
    
    public function addDependent($key){
        $this->selectOptions();
        $arr = [
            'name' => '',
            'surname' => '',
            'id_number' => '',
            'select_id' => null,
        ];
        $this->medical_aids[$key]['dependents'][] = $arr;
    }
    
    public function addChild(){
        $arr = [
            'child_name' => '',
            'middle_name' => '',
            'child_surname' => '',
            'child_id_number' => '',
            'child_date_of_birth' => '',
            'child_birth_certificate' => '',
            'child_adoption_certificate' => ''
        ];
        $this->children[] = $arr;
    }

    public function saveMe(){
        $this->validate([
            'name' => 'required',
            'surname' => 'required',
            'id_number' => 'nullable|required_without:passport_number|said', 
            'passport_number' => 'required_without:id_number',
            'birth_certificate' => 'nullable|file|mimes:pdf,jpeg,png|max:2048',
            'id_document' => 'nullable|file|mimes:pdf,jpeg,png|max:2048',
            'passport_document' => 'nullable|file|mimes:pdf,jpeg,png|max:2048',
            'drivers_license_document' => 'nullable|file|mimes:pdf,jpeg,png|max:2048',
            'power_of_attorney_document' => 'nullable|file|mimes:pdf,jpeg,png|max:2048',
            'separate_power_of_attorney_document' => 'nullable|file|mimes:pdf,jpeg,png|max:2048',
        ]);
        $usr = Auth::user();

        $usr->name = $this->name; 
        $usr->surname = $this->surname; 
        $usr->id_number = $this->id_number; 
        $usr->passport_number = $this->passport_number;
        $usr->street_address = $this->street_address;
        $usr->city = $this->city;
        $usr->province = $this->province;
        $usr->gender = $this->gender;
        $usr->ethnicity = $this->ethnicity;

        if($this->birth_certificate){
            $birth_path = $this->birth_certificate->storePublicly('documents', 'public');
            $usr->birth_certificate = $birth_path;
        }
        if($this->id_document){
            $id_doc_path = $this->id_document->storePublicly('documents', 'public');
            $usr->id_document = $id_doc_path;
        } 
        if($this->passport_document){
            $passport_path = $this->passport_document->storePublicly('documents', 'public');
            $usr->passport_document = $passport_path;
        } 
        if($this->drivers_license_document){
            $drivers_path = $this->drivers_license_document->storePublicly('documents', 'public');
            $usr->drivers_license_document = $drivers_path;
        } 
        if($this->power_of_attorney_document){
            $att_path = $this->power_of_attorney_document->storePublicly('documents', 'public');
            $usr->power_of_attorney_document = $att_path;
        } 
        if($this->separate_power_of_attorney_document){
            $att2_path = $this->separate_power_of_attorney_document->storePublicly('documents', 'public');
            $usr->separate_power_of_attorney_document = $att2_path;
        }
        $usr->save();
        //session()->flash('message', 'Updated successfully.');
        $this->dispatchBrowserEvent('data-updated', ['message' => 'Updated successfully.']);
    }

    public function saveSpouse(){
        foreach($this->spouses as $s=>$spouse){

            $rules = [
                'spouses.'.$s.'.divorce_agreement' => 'nullable|file|mimes:pdf,jpeg,png|max:2048',
                'spouses.'.$s.'.marriage_certificate' => 'nullable|file|mimes:pdf,jpeg,png|max:2048',
                'spouses.'.$s.'.antenuptial_contract' => 'nullable|file|mimes:pdf,jpeg,png|max:2048',
                'spouses.'.$s.'.death_certificate' => 'nullable|file|mimes:pdf,jpeg,png|max:2048',
                'spouses.'.$s.'.will' => 'nullable|file|mimes:pdf,jpeg,png|max:2048',
                'spouses.'.$s.'.court_order_and_agreement' => 'nullable|file|mimes:pdf,jpeg,png|max:2048',
                'spouses.'.$s.'.proof_of_maintenance_order' => 'nullable|file|mimes:pdf,jpeg,png|max:2048',
                'spouses.'.$s.'.specific_agreements_file' => 'nullable|file|mimes:pdf,jpeg,png|max:2048',
                'spouses.'.$s.'.domestic_agreement' => 'nullable|file|mimes:pdf,jpeg,png|max:2048',
            ];

            $validatedData = $this->validate($rules, [], [
                'spouses.'.$s.'.divorce_agreement' => 'divorce agreement',
                'spouses.'.$s.'.marriage_certificate' => 'marriage certificate',
                'spouses.'.$s.'.antenuptial_contract' => 'antenuptial contract',
                'spouses.'.$s.'.death_certificate' => 'death certificate',
                'spouses.'.$s.'.will' => 'will',
                'spouses.'.$s.'.court_order_and_agreement' => 'court order and agreement',
                'spouses.'.$s.'.proof_of_maintenance_order' => 'proof of maintenance order',
                'spouses.'.$s.'.specific_agreements_file' => 'specific agreements',
                'spouses.'.$s.'.domestic_agreement' => 'agreement',
            ]);

            if(array_key_exists('id',$spouse)){
                $sp = Spouse::find($spouse['id']);
            }
            else{
                $sp = new Spouse();
            }
            
            if($spouse['marital_status']){
                $sp->user_id = Auth::user()->id;
                $sp->marital_status = $spouse['marital_status'];
                $sp->spouse_name = $spouse['spouse_name']; 
                $sp->spouse_surname = $spouse['spouse_surname']; 
                $sp->spouse_middle_name = $spouse['spouse_middle_name'];
                $sp->spouse_id_number = $spouse['spouse_id_number'];  
                $sp->date_of_death = $spouse['date_of_death']; 
                $sp->masters_office = $spouse['masters_office']; 
                $sp->estate_number = $spouse['estate_number']; 
                $sp->attorney_auditor_trust_contact_details = $spouse['attorney_auditor_trust_contact_details']; 
                $sp->estate_wrapping_up_company = $spouse['estate_wrapping_up_company']; 
                $sp->dweling_property = $spouse['dweling_property']; 
                $sp->usufruct = $spouse['usufruct'];
                $sp->date_of_divorce = $spouse['date_of_divorce']; 
                $sp->maintenance_order = $spouse['maintenance_order']; 
                $sp->specific_agreements_description = $spouse['specific_agreements_description']; 
                $sp->comunity_marriage = $spouse['comunity_marriage']; 
                $sp->accrual = $spouse['accrual'];
                $sp->executor = $spouse['executor'];
            
                if($spouse['divorce_agreement']){
                    $divorce_agreement_path = $spouse['divorce_agreement']->storePublicly('documents', 'public');
                    $sp->divorce_agreement = $divorce_agreement_path;
                }

                if($spouse['marriage_certificate']){
                    $marriage_certificate_path = $spouse['marriage_certificate']->storePublicly('documents', 'public');
                    $sp->marriage_certificate = $marriage_certificate_path;
                } 

                if($spouse['antenuptial_contract']){
                    $antenuptial_contract_path = $spouse['antenuptial_contract']->storePublicly('documents', 'public');
                    $sp->antenuptial_contract = $antenuptial_contract_path;
                }

                if($spouse['death_certificate']){
                    $death_certificate_path = $spouse['death_certificate']->storePublicly('documents', 'public');
                    $sp->death_certificate = $death_certificate_path;
                } 

                if($spouse['will']){
                    $will_path = $spouse['will']->storePublicly('documents', 'public');
                    $sp->will = $will_path;
                }

                if($spouse['court_order_and_agreement']){
                    $court_order_and_agreement_path = $spouse['court_order_and_agreement']->storePublicly('documents', 'public');
                    $sp->court_order_and_agreement = $court_order_and_agreement_path;
                } 

                if($spouse['proof_of_maintenance_order']){
                    $proof_of_maintenance_order_path = $spouse['proof_of_maintenance_order']->storePublicly('documents', 'public');
                    $sp->proof_of_maintenance_order = $proof_of_maintenance_order_path;
                } 

                if($spouse['specific_agreements_file']){
                    $specific_agreements_file_path = $spouse['specific_agreements_file']->storePublicly('documents', 'public');
                    $sp->specific_agreements_file = $specific_agreements_file_path;
                }

                if($spouse['domestic_agreement']){
                    $domestic_agreement_path = $spouse['domestic_agreement']->storePublicly('documents', 'public');
                    $sp->domestic_agreement = $domestic_agreement_path;
                }
                $sp->save();
            }
        }
        // $this->spouseOptions();
        // $this->setInitValues();
        // $this->dispatchBrowserEvent('data-updated', ['message' => 'Updated successfully.']);
        // return redirect("organiser/personal");
    }

    public function savePlaceOfWork(){
        $this->validate([
            'employer_name' => 'required',
            'employer_address' => 'required',
            'salary_number' => 'required'
        ]);
        $work = PlaceOfWork::where('user_id', Auth::user()->id)->first();
        if(!$work){
            $work = new PlaceOfWork();
        }
        $work->user_id = Auth::user()->id;
        $work->employer_name = $this->employer_name;
        $work->employer_address = $this->employer_address;
        $work->salary_number = $this->salary_number;
        $work->save();
        $this->dispatchBrowserEvent('data-updated', ['message' => 'Updated successfully.']); 
    }

    public function saveEducation(){
        foreach($this->educations as $e=>$edu){
            $rules = [
                'educations.'.$e.'.certificate' => 'nullable|file|mimes:pdf,jpeg,png|max:2048',
            ];

            $validatedData = $this->validate($rules, [], [
                'educations.'.$e.'.certificate' => 'certificate',
            ]);

            if(array_key_exists('id',$edu)){
                $ed = Education::find($edu['id']);
            }
            else{
                $ed = new Education();
            }

            $ed->user_id = Auth::user()->id;
            $ed->qualification = $edu['qualification'];
            $ed->year = $edu['year'];
            $ed->comment = $edu['comment'];
            if($edu['certificate']){
                $certificate_path = $edu['certificate']->storePublicly('documents', 'public');
                $ed->certificate = $certificate_path;
            }
            $ed->save();
        }

        $this->setInitValues();
        $this->dispatchBrowserEvent('data-updated', ['message' => 'Updated successfully.']);
        // return redirect("organiser/personal");
    }

    public function saveChildren(){
        $has_error = false;
        foreach($this->children AS $k=>$child){

            $rules = [
                'children.'.$k.'.child_birth_certificate' => 'nullable|file|mimes:pdf,jpeg,png|max:2048',
                'children.'.$k.'.child_adoption_certificate' => 'nullable|file|mimes:pdf,jpeg,png|max:2048',
            ];

            $validatedData = $this->validate($rules, [], [
                'children.'.$k.'.child_birth_certificate' => 'birth certificate',
                'children.'.$k.'.child_adoption_certificate' => 'adoption certificate',
            ]);

            if(array_key_exists('id', $child)){
                $cld = Child::find($child['id']);
            }
            else{
                $cld = new Child();
            }

            if($child['child_name'] && $child['child_surname'] && $child['child_date_of_birth']){
                $cld->user_id = Auth::user()->id;
                $cld->child_name = $child['child_name'];
                $cld->middle_name = $child['middle_name'];
                $cld->child_surname = $child['child_surname'];
                $cld->child_id_number = $child['child_id_number'];
                $cld->child_date_of_birth = $child['child_date_of_birth'];

                if($child['child_birth_certificate']){
                    $path = $child['child_birth_certificate']->storePublicly('documents', 'public');
                    $cld->child_birth_certificate = $path;
                }
                if($child['child_adoption_certificate']){
                    $path = $child['child_adoption_certificate']->storePublicly('documents', 'public');
                    $cld->child_adoption_certificate = $path;
                }
                $cld->save();
            }
            else{
                $this->addError("error", "One of the children was not saved, you did not enter all required data");
                $has_error = true;
            }
        }
        $this->setInitValues();
        if(!$has_error){
            $this->dispatchBrowserEvent('data-updated', ['message' => 'Updated successfully.']);
            // return redirect("organiser/personal");
        }
    }

    public function saveMedicalAid(){
        foreach($this->medical_aids AS $k=>$medical_aid){

            $rules = [
                'medical_aids.'.$k.'.medical_aid_certificate' => 'nullable|file|mimes:pdf,jpeg,jpg,png|max:2048',
            ];

            $validatedData = $this->validate($rules, [], [
                'medical_aids.'.$k.'.medical_aid_certificate' => 'medical aid certificate',
            ]);

            if($medical_aid['company'] && $medical_aid['reference_number']){
                if(isset($medical_aid['id'])){
                    $aid = MedicalAid::find($medical_aid['id']);
                }
                else{
                    $aid = new MedicalAid();
                }

                $aid->user_id = Auth::user()->id;
                $aid->company = $medical_aid['company'];
                $aid->reference_number = $medical_aid['reference_number'];
                $aid->name = $medical_aid['name'];
                $aid->surname = $medical_aid['surname'];
                $aid->id_number = $medical_aid['id_number'];
                $aid->policy_type = $medical_aid['policy_type'];
                // $aid->conditions = $medical_aid['conditions'];
                if($medical_aid['medical_aid_certificate']){
                    $path = $medical_aid['medical_aid_certificate']->storePublicly('documents', 'public');
                    $aid->medical_aid_certificate = $path;
                }
                $aid->save();
                
                //Save Conditions
                if(isset($medical_aid['conditions']) && is_array($medical_aid['conditions'])){
                    foreach($medical_aid['conditions'] AS $cnd){
                        if(is_array($cnd) && isset($cnd['condition'])){
                            if(isset($cnd['id'])){
                                $cond = MedicalAidCondition::find($cnd['id']);
                            }
                            else{
                                $cond = new MedicalAidCondition();
                            }
                            $cond->user_id = Auth::user()->id;
                            $cond->medical_aid_id = $aid->id;
                            $cond->condition = $cnd['condition'];
                            $cond->save();
                        }
                    }
                    
                }

                //dd($medical_aid['dependents']);
                if(isset($medical_aid['dependents'])){
                    foreach($medical_aid['dependents'] AS $dp){
                        if(isset($dp['id'])){
                            $dpt = Dependents::find($dp['id']);
                        }
                        else{
                            $dpt = new Dependents();
                        }
                        if(!isset($dp['id_number'])){
                            //dd($dp);
                        }

                        if(isset($dp['select_id'])){
                            if($dp['select_id'] == 'other'){
                                if($dp['name'] != "" && $dp['surname'] != "" && $dp['id_number'] != ""){
                                    $dpt->medical_aid_id = $aid->id;
                                    $dpt->user_id = Auth::user()->id;
                                    $dpt->name = $dp['name'];
                                    $dpt->surname = $dp['surname'];
                                    $dpt->id_number = $dp['id_number'];
                                    if(isset($dp['condition'])){
                                        $dpt->condition = $dp['condition'];
                                    }
                                    $dpt->save();
                                }
                            }else{
                                $spouse = Spouse::where('spouse_id_number', $dp['select_id'])->where('user_id', Auth::user()->id)->first();
                                $child = Child::where('child_id_number', $dp['select_id'])->orWhere('child_date_of_birth', $dp['select_id'])->where('user_id', Auth::user()->id)->first();
    
                                if($spouse){
                                    $dpt->medical_aid_id = $aid->id;
                                    $dpt->user_id = Auth::user()->id;
                                    $dpt->name = $spouse->spouse_name;
                                    $dpt->surname = $spouse->spouse_surname;
                                    $dpt->id_number = $spouse->spouse_id_number;
                                    if(isset($dp['condition'])){
                                        $dpt->condition = $dp['condition'];
                                    }
                                    $dpt->save();
                                }elseif($child){
                                    $dpt->medical_aid_id = $aid->id;
                                    $dpt->user_id = Auth::user()->id;
                                    $dpt->name = $child->child_name;
                                    $dpt->surname = $child->child_surname;
                                    $dpt->id_number = $child->child_id_number;
                                    if(isset($dp['condition'])){
                                        $dpt->condition = $dp['condition'];
                                    }
                                    $dpt->save();
                            } 
                                }
                        }else{
                            if($dp['name'] != "" && $dp['surname'] != "" && $dp['id_number'] != ""){
                                $dpt->medical_aid_id = $aid->id;
                                $dpt->user_id = Auth::user()->id;
                                $dpt->name = $dp['name'];
                                $dpt->surname = $dp['surname'];
                                $dpt->id_number = $dp['id_number'];
                                if(isset($dp['condition'])){
                                    $dpt->condition = $dp['condition'];
                                }
                                $dpt->save();
                            }
                        }
                    }
                }
            }
        }
        $this->setInitValues();
        $this->dispatchBrowserEvent('data-updated', ['message' => 'Updated successfully.']);
        // return redirect("organiser/personal");
    }
    
    public function saveFuneralCover(){
        $this->validate([
            'policy_number' => 'required',
            'policy_holder' => 'required',
            'company' => 'required',
            'coverage_amount' => 'required'
        ]);
        $f_cover= FuneralCover::where('user_id', Auth::user()->id)->first();
        if(!$f_cover){
            $f_cover = new FuneralCover();
        }
        $f_cover->user_id = Auth::user()->id;
        $f_cover->policy_number = $this->policy_number;
        $f_cover->policy_holder = $this->policy_holder;
        $f_cover->company = $this->f_company;
        $f_cover->coverage_amount = $this->coverage_amount;
        $f_cover->save();
        $this->setInitValues();
        $this->dispatchBrowserEvent('data-updated', ['message' => 'Updated successfully.']);
    }
    
    public function removeChild($id){
        $ch = Child::find($id);
        $ch->delete();
        $this->setInitValues();
        return redirect()->to('/organiser/personal');
    }
    
    public function removeDocument($type){
        $usr = Auth::user();
        if($usr->$type){
            $usr->$type = null;
            $usr->save();
        }

        $this->setInitValues();
    }
    
    public function removeSposeDocument($type, $id){
        
        $sp = Spouse::find($id);
        if($sp){
            $sp->$type = null;
            $sp->save();
        }
        $this->setInitValues();
    }
    
    public function removeTaxDocument($type){
        $usr = Auth::user();
        if($usr->tax){
            $tx = $usr->tax;
            $tx->$type = null;
            $tx->save();
        }
        $this->setInitValues();
    }
    
    public function removeChildDocument($type, $id){
        $ch = Child::find($id);
        if($ch){
            $ch->$type = null;
            $ch->save();
        }
        $this->setInitValues();
    }
    
    public function removeMedicalAidDocument($type){
        $usr = Auth::user();
        if($usr->medicalAids->count() > 0){
            $ma = $usr->medicalAids->first();
            $ma->$type = null;
            $ma->save();
        }
        $this->setInitValues();
    }
    
    public function removeFuneraCover($id){
        FuneralCover::where('id', $id)->delete();
        $this->setInitValues();
        return redirect()->to('/organiser/personal');
    }
    
    public function removeDependent($id){
        Dependents::where('id', $id)->delete();
        $this->setInitValues();
        return redirect()->to('/organiser/personal');
    }

    public function removeSpouse($id){
        Spouse::where('id', $id)->delete();
        $this->setInitValues();
        return redirect()->to('/organiser/personal');
    }

    public function removeCertificate($id){
        $ed = Education::find($id);
        $ed->certificate = null;
        $ed->save();
        $this->setInitValues();
    }

    public function removeEducation($id){
        $ed = Education::where('id', $id)->delete();
        $this->setInitValues();
        return redirect()->to('/organiser/personal');
    }

    public function render(){
        return view('livewire.account.organiser.personal.personal');
    }
}