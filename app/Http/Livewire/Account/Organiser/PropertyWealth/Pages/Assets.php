<?php

namespace App\Http\Livewire\Account\Organiser\PropertyWealth\Pages;

use Livewire\Component;

use Auth;
use App\Models\Asset;

class Assets extends Component
{
    public $modal_title;
    public $asset_types = [], $cur_id;
    public $asset_type, $value, $outstanding ,$description, $view;
    
    public function mount(){
        $this->asset_types = [
            'House',
            'Furniture',
            'Household Items',
            'Caravan',
            'Car',
            'Camera',
            'Computer',
            'Jewellery',
            'Art',
            'Farm',
            'Townhouse',
            'Other'
        ];
        sort($this->asset_types);
    }
    
    public function hydrate(){
        $this->dispatchBrowserEvent('reload-js');
    }

    public function showModal($view = null, $id = null){
        if($id){
            $this->cur_id = $id;
            $ast = Asset::find($id);
            $this->asset_type = $ast->asset_type;
            $this->value = $ast->value;
            $this->outstanding = $ast->outstanding;
            $this->description = $ast->description;
        }
        
        if($id && $view == 'show'){
            $this->modal_title = "Asset";
        }elseif($id){
            $this->modal_title = "Edit Asset";
        }else{
            $this->modal_title = "Create Asset";
        }
        
        if(isset($view)){
            $this->view = $view;
        }
        
        $this->dispatchBrowserEvent('show-form-modal');
    }

    public function deleteAsset($id){
        $ast = Asset::find($id);
        $ast->delete();
    }
    
    public function closeModal(){
        $this->clearFields();
        $this->dispatchBrowserEvent('close-form-modal');
    }

    public function saveAsset(){
        $this->validate([
            'asset_type' => 'required',
            'value' => 'required',
            'outstanding' => 'nullable',
            'description' => 'nullable'
        ]);

        if($this->cur_id){
            $ast = Asset::find($this->cur_id);
        }
        else{
            $ast = new Asset();
        }

        $ast->user_id = Auth::user()->id;
        $ast->asset_type = $this->asset_type;
        $ast->value = str_replace('R', '', str_replace(',', '', str_replace(' ', '', $this->value)));
        $ast->outstanding = str_replace('R', '',str_replace(',', '', str_replace(' ', '', $this->outstanding)));
        $ast->description = $this->description;
        $ast->save();
        $this->clearFields();
        $this->dispatchBrowserEvent('close-form-modal');

    }
    
    public function clearFields(){
        $this->cur_id = null;
        $this->asset_type = null;
        $this->value = null;
        $this->outstanding = null;
        $this->description = null;
        $this->view = null;
    }

    public function render(){
        $assets = Asset::where('user_id', Auth::user()->id)->get();
        return view('livewire.account.organiser.property-wealth.pages.assets', [
            'assets' => $assets
        ]);
    }
}
