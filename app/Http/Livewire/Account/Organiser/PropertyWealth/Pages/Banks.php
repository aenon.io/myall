<?php

namespace App\Http\Livewire\Account\Organiser\PropertyWealth\Pages;

use Livewire\Component;
use Livewire\WithFileUploads;

use Auth;
use App\Models\Bank;

class Banks extends Component
{
    use WithFileUploads;

    public $modal_title, $account_types;
    public $account_type, $bank, $account_number, $value, $file, $message;
    public $cur_id, $file_path, $view;
    
    public function mount(){
        $this->account_types = [
            'Savings Account',
            'Money Market Account',
            'Credit Card',
            'Loan Account',
            'Cheque Account',
            'Other'
        ];
        sort($this->account_types);
    }
    
    public function hydrate(){
        $this->dispatchBrowserEvent('reload-js');
    }

    public function deleteBank($id){
        $bnk = Bank::find($id);
        $bnk->delete();
    }

    public function showModal($view = null, $id = null){
        if($id){
            $this->cur_id = $id;
            $bnk = Bank::find($id);
            if($bnk){
                $this->account_type = $bnk->account_type; 
                $this->bank = $bnk->bank;
                $this->account_number = $bnk->account_number;
                $this->value = $bnk->value;
                $this->message = $bnk->message;
                $this->file = '';
                $this->file_path = $bnk->file;
            }
        }
        
        if($id && $view == 'show'){
            $this->modal_title = "Bank Account";
        }elseif($id){
            $this->modal_title = "Edit Bank Account";
        }else{
            $this->modal_title = "Add Bank Account";
        }
        
        if(isset($view)){
            $this->view = $view;
        }
        
        $this->dispatchBrowserEvent('show-form-modal');
    }
    
    public function closeModal(){
        $this->clearFields();
        $this->dispatchBrowserEvent('close-form-modal');
    }
    
    public function clearFields(){
        $this->cur_id = null;
        $this->account_type = null; 
        $this->bank = null;
        $this->account_number = null;
        $this->value = null;
        $this->message = null;
        $this->file = null;
        $this->view = null;
    }

    public function saveBank(){
        $this->validate([
            'account_type' => 'required', 
            'bank' => 'required', 
            'account_number' => 'required',
            'value' => 'required',
            'file' => 'nullable|file|mimes:pdf,jpeg,png|max:2048',
        ]);
        if($this->cur_id){
            $bnk = Bank::find($this->cur_id);
        }
        else{
            $bnk = new Bank();
        }

        $path = null;
        if($this->file){
            $path = $this->file->storePublicly('document', 'public');
        }

        $bnk->user_id = Auth::user()->id;
        $bnk->account_type = $this->account_type;
        $bnk->bank = $this->bank;
        $bnk->account_number = $this->account_number;
        $bnk->value = str_replace('R', '',str_replace(',', '', str_replace(' ', '', $this->value)));
        $bnk->message = $this->message;
        if($path){
            $bnk->file = $path;
        }
        $bnk->save();
        $this->clearFields();
        $this->dispatchBrowserEvent('close-form-modal');
    }

    public function render(){
        $bnks = Bank::where('user_id', Auth::user()->id)->get();
        return view('livewire.account.organiser.property-wealth.pages.banks', [
            'bnks' => $bnks
        ]);
    }
    
    public function removeFile($id){
        $bnk = Bank::find($id);
        $bnk->file = null;
        $bnk->save();
         return redirect()->to('organiser/properties-wealth');
    }
}
