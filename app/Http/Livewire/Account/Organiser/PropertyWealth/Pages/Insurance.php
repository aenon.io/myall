<?php

namespace App\Http\Livewire\Account\Organiser\PropertyWealth\Pages;

use Livewire\Component;
use Auth;
use App\Models\Insurance AS InsuranceModel;
use Livewire\WithFileUploads;

class Insurance extends Component
{
    use WithFileUploads;
    public $modal_title, $insurance_types =[];
    public $insurance_type, $insurer, $policy_number, $contact, $financial_advisor, $outstanding_debt, $file, $payment_date, $payment_terms;
    public $cur_id, $file_path;
    public $terms;
    public $view;

    public function mount(){
        $this->modal_title = "Insurance";
        $this->insurance_types = [
            'Pet Insurance',
            'Short Term Insurance',
            'Property Insurance',
            'Vehicle Insurance'
        ];
        $this->terms = [
            "daily",
            "weekly",
            "monthly",
            "quarterly",
            "yearly",
            "once-off"
        ];
    }
    
    public function hydrate(){
        $this->dispatchBrowserEvent('reload-js');
    }

    public function showModal($view = null, $id = null){
        if($id){
            $ins = InsuranceModel::find($id);
            if($ins){
                $this->cur_id = $id;
                $this->insurance_type = $ins->insurance_type;
                $this->insurer = $ins->insurer;
                $this->policy_number = $ins->policy_number;
                $this->contact = $ins->contact;
                $this->financial_advisor = $ins->financial_advisor;
                $this->outstanding_debt = $ins->outstanding_debt;
                $this->payment_date = $ins->payment_date;
                $this->payment_terms = $ins->payment_terms;
                $this->file = '';
                $this->file_path = $ins->file;
            }
        }
        
        if(isset($view)){
            $this->view = $view;
        }
        
        $this->dispatchBrowserEvent('show-form-modal');
    }
    
    public function clearFields(){
        $this->cur_id = null;
        $this->insurance_type = null;
        $this->insurer = null; 
        $this->policy_number = null; 
        $this->contact = null; 
        $this->financial_advisor = null;
        $this->outstanding_debt = null;
        $this->payment_date = null;
        $this->payment_terms = null;
        $this->file = null;
        $this->view = null;
    }

    public function deleteinsurance($id){
        $ins = InsuranceModel::find($id);
        $ins->delete();
    }
    
    public function closeModal(){
        $this->clearFields();
        $this->dispatchBrowserEvent('close-form-modal');
    }

    public function saveInsurance(){
        $this->validate([
            'insurance_type' => 'required', 
            'insurer' => 'required',
            'policy_number' => 'required',
            'outstanding_debt' => 'nullable',
            'file' => 'nullable|file|mimes:pdf,jpeg,png|max:2048',
        ]);
        if($this->cur_id){
            $ins = InsuranceModel::find($this->cur_id);
        }
        else{
            $ins = new InsuranceModel();
        }
        
        $path = null;
        if($this->file){
            $path = $this->file->storePublicly('document', 'public');
        }
        
        $ins->user_id = Auth::user()->id;
        $ins->insurance_type = $this->insurance_type;
        $ins->insurer = $this->insurer;
        $ins->policy_number = $this->policy_number;
        $ins->contact = $this->contact;
        $ins->financial_advisor = $this->financial_advisor;
        $ins->outstanding_debt = $this->outstanding_debt;
        $ins->payment_date = $this->payment_date;
        $ins->payment_terms = $this->payment_terms;
        if($path){
            $ins->file = $path;
        }
        $ins->save();
        $this->clearFields();
        $this->dispatchBrowserEvent('close-form-modal');
    }

    public function render()
    {
        $insurances = InsuranceModel::where('user_id', Auth::user()->id)->get();
        return view('livewire.account.organiser.property-wealth.pages.insurance', [
            'insurances' => $insurances
        ]);
    }
    
    public function removeFile($id){
        $ins = InsuranceModel::find($id);
        $ins->file = null;
        $ins->save();
        return redirect()->to('organiser/debt-expenses');
    }
}
