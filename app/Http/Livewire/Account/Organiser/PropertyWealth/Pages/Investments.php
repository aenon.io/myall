<?php

namespace App\Http\Livewire\Account\Organiser\PropertyWealth\Pages;

use Livewire\Component;
use Livewire\WithFileUploads;
use App\Models\Investment;
use Auth;
use App\Models\InvestmentBeneficiaries;

class Investments extends Component
{
    use WithFileUploads;

    public $modal_title, $investment_types;
    public $investment_type, $value, $file;
    public $cur_id, $file_path;

    public $beneficiaries = [];

    // public $nominated_beneficiary, $percentage;
    
    public $policy_number, $description, $view;

    public function mount(){
        $this->modal_title = "Investment";
        $this->investment_types = [
            'Share Portfolio',
            'Unit Trust',
            'Life Policy',
            'Education Policy',
            'Retirement Annuity',
            'Other'
        ];

        $this->addBeneficiary();
    }
    
    public function hydrate(){
        $this->dispatchBrowserEvent('reload-js');
    }

    public function showModal($view = null, $id = null){
        if($id){
            $inv = Investment::find($id);
            if($inv){
                $this->cur_id = $id;
                $this->investment_type = $inv->investment_type;
                $this->value = $inv->value;
                $this->file = '';
                $this->file_path = $inv->file;
                $this->policy_number = $inv->policy_number; 
                $this->description = $inv->description;

                if($inv->beneficiaries->count() > 0){
                    $this->beneficiaries = [];
                    foreach($inv->beneficiaries AS $ben){
                        $arr = [
                            'id' => $ben->id,
                            'nominated_beneficiary' => $ben->nominated_beneficiary,
                            'percentage' => $ben->percentage,
                        ];
                        $this->beneficiaries[] = $arr;
                    }
                }
            }
        }
        
        if(isset($view)){
            $this->view = $view;
        }
        
        $this->dispatchBrowserEvent('show-form-modal');
    }

    public function addBeneficiary(){
        $arr = [];
        $arr = [
            'nominated_beneficiary' => null,
            'percentage' => null,
        ];
        $this->beneficiaries[] = $arr;
        //dd($this->beneficiaries);
    }

    public function updatedBeneficiaries(){
        //dd($this->beneficiaries);
    }

    public function saveInvestment(){
        $this->validate([
            'investment_type' => 'required', 
            'value' => 'required',
            'file' => 'nullable|file|mimes:pdf,jpeg,png|max:2048' 
        ]);
        if($this->cur_id){
            $inv = Investment::find($this->cur_id);
        }
        else{
            $inv = new Investment();
        }

        $path = null;
        if($this->file){
            $path = $this->file->storePublicly('document', 'public');
        }

        $inv->user_id = Auth::user()->id;
        $inv->investment_type = $this->investment_type; 
        $inv->value = str_replace(',', '', str_replace(' ', '', $this->value)); 
        if($path){
            $inv->file = $path;
        }
        
        $inv->policy_number = $this->policy_number; 
        $inv->description = $this->description;
        
        $inv->save();
        
        // dd($this->beneficiaries);
        foreach($this->beneficiaries AS $benenficiary){
            if(isset($benenficiary['id'])){
                $ben = InvestmentBeneficiaries::find($benenficiary['id']);
            }
            else{
                $ben = new InvestmentBeneficiaries();
            }

            $ben->user_id = Auth::user()->id;
            $ben->investment_id = $inv->id;
            $ben->nominated_beneficiary = $benenficiary['nominated_beneficiary'];
            $ben->percentage = $benenficiary['percentage'];
            $ben->save();
        }

        $this->clearFields();
        $this->dispatchBrowserEvent('close-form-modal');
    }
    
    public function deleteInvestment($id){
        $inv = Investment::find($id);
        $inv->delete();
    }

    public function deleteBeneficiary($id){
        InvestmentBeneficiaries::where('id', $id)->delete();
        $this->closeModal();
    }

    public function removeFile($id){
        $inv = Investment::find($id);
        $inv->file = null;
        $inv->save();
        return redirect()->to('organiser/properties-wealth');
    }
    
    public function clearFields(){
        $this->cur_id = null;
        $this->investment_type = null;
        $this->value = null;
        $this->file = null;
        $this->policy_number = null; 
        $this->description = null;
        $this->view = null;
        $arr = [
            'id' => '',
            'nominated_beneficiary' => '',
            'percentage' => '',
        ];
        $this->beneficiaries = [];
    }
    
    public function closeModal(){
        $this->clearFields();
        $this->dispatchBrowserEvent('close-form-modal');
    }

    public function render(){
        $invs = Investment::where('user_id', Auth::user()->id)->get();
        return view('livewire.account.organiser.property-wealth.pages.investments', [
            'invs' => $invs
        ]);
    }
    
}