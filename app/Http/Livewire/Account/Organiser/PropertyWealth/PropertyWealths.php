<?php

namespace App\Http\Livewire\Account\Organiser\PropertyWealth;

use Livewire\Component;

class PropertyWealths extends Component
{
    public function render()
    {
        return view('livewire.account.organiser.property-wealth.property-wealths');
    }
}
