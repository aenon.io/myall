<?php

namespace App\Http\Livewire\Account\Organiser\SafeKeeping;

use Livewire\Component;
use Livewire\WithFileUploads;

use Auth;
use App\Models\Tax;
use App\Models\PopUp;
use App\Models\SafeKeep;
use App\Models\ItemToFind;

class SafeKeeping extends Component
{
    use WithFileUploads;
    public $items = [];
    
    //Tax Fields
    public $tax_number,$tax_assessment,$tax_certificate, $tax_assesment_file, $tax_file, $tax_assesment_file_path, $tax_file_path;
    
    //Items To Fields
    public $items_to_find = [], $item_name, $location, $description, $file, $file_path;

    public $show_init_pop_up = false;
    
    public function mount(){
        $this->getData();
        $this->addItemsTofind();
        $this->setInitValues();

        $pop_up = PopUp::where('user_id', Auth::user()->id)->first();
        if($pop_up){
            if($pop_up->safe){
                $this->show_init_pop_up = false;
            }
            else{
                $this->show_init_pop_up = true;
            }
        }
        else{
            $this->show_init_pop_up = true;
            $pop_up = new PopUp();
            $pop_up->user_id = Auth::user()->id;
            $pop_up->save();
        }
    }
    
    public function setInitValues(){
        //Set Tax Values
        $tax = Tax::where('user_id', Auth::user()->id)->first();
        if($tax){
            $this->tax_number = $tax->tax_number;
            $this->tax_assesment_file = '';
            $this->tax_file = '';
            $this->tax_assesment_file_path = $tax->tax_assessment;  
            $this->tax_file_path = $tax->tax_certificate;
        }
        
        //Set Items To Find Values
        $items = ItemToFind::where('user_id', Auth::user()->id)->get();
        if($items->count() > 0){
            $this->items_to_find = [];
            foreach($items AS $item){
                $arr = [
                    'id' => $item->id,
                    'item_name' => $item->item_name,
                    'description' => $item->description,
                    'file' => '',
                    'file_path' => $item->file,
                ];
                $this->items_to_find[] = $arr;
            }
        }
        
    }
    
    public function addItemsTofind(){
        $arr = [
            'item_name' => '',
            'description' => '',
            'file' => '',
        ];
        $this->items_to_find[] = $arr;
    }
    
    public function getData(){
        $items = SafeKeep::where('user_id', Auth::user()->id)->get();
        if($items->count() > 0){
            $this->items = [];
            
            foreach($items AS $item){
                $arr = [
                    'id' => $item->id,
                    'title' => $item->title,
                    'description' => $item->description,
                    'location' => $item->location,
                    'file' => '',
                    'file_path' => $item->file,
                ];
                $this->items[] = $arr;
            }
        }
        else{
            $this->addMore();
        }
    }
    
    public function addMore(){
        $arr = [
            "name",
            "title",
            "description",
            "location",
            "file"
        ];
        $this->items[] = $arr;
    }
    
    public function saveSafeKeeping(){
        $has_error = false;
        foreach($this->items AS $k=>$item){
            if($item['title'] == "" || $item['description'] == ""){
                $has_error = true;
            }
        }
        
        if(!$has_error){
        foreach($this->items AS $k=>$item){

            $rules = [
                'items.'.$k.'.file' => 'nullable|file|mimes:pdf,jpeg,png|max:2048',
            ];

            $validatedData = $this->validate($rules, [], [
                'items.'.$k.'.file' => 'file',
            ]);
            
            if($item['title']){
                if(isset($item['id'])){
                    $itm = SafeKeep::find($item['id']);
                }
                else{
                    $itm = new SafeKeep();
                }
                
                $path = null;
                if(isset($item['file'])){
                    if($item['file']) {
                        $path = $item['file']->storePublicly('safekeep', 'public');
                    }else{
                        $path = $item['file'];
                    }
                }
                $des = "";
                if(isset($item['description'])){
                    $des = $item['description'];
                }
                $itm->user_id = Auth::user()->id;
                $itm->title = $item['title'];
                $itm->description = $des;
                $itm->location = $item['location'];
                $itm->file = $path;
                $itm->save();
            }
        }
        session()->flash('message', 'Successfully saved.');
        $this->getData();
        }
        else{
            $this->addError("error", "Please enter all required data");
        }
    }
    
    public function removeSafeKeeping($id){
        SafeKeep::where('id',$id)->delete();
        $this->getData();
        session()->flash('message', 'Deleted');
    }
    
    public function removeFile($id){
        $itm = SafeKeep::find($id);
        $itm->file = null;
        $itm->save();
        $this->getData();
        session()->flash('message', 'File Deleted');
    }
    
    public function saveTax(){
        $this->validate([
            'tax_number' => 'required',
            'tax_assessment' => 'nullable|file|mimes:pdf,jpeg,png|max:2048',
            'tax_certificate' => 'nullable|file|mimes:pdf,jpeg,png|max:2048',
        ]);

        $tax = Tax::where('user_id', Auth::user()->id)->first();
        if(!$tax){
            $tax = new Tax();
        }

        $tax->user_id = Auth::user()->id;
        $tax->tax_number = $this->tax_number;
        if($this->tax_assessment){
            $tax_assessment_path = $this->tax_assessment->storePublicly('documents', 'public');
            $tax->tax_assessment = $tax_assessment_path;
        }
        if($this->tax_certificate){
            $tax_certificate_path = $this->tax_certificate->storePublicly('documents', 'public');
            $tax->tax_certificate = $tax_certificate_path;
        }
        $tax->save();
        $this->setInitValues();
        $this->dispatchBrowserEvent('data-updated', ['message' => 'Updated successfully.']); 
    }
    
    public function removeTaxDocument($type){
        $usr = Auth::user();
        if($usr->tax){
            $tx = $usr->tax;
            $tx->$type = null;
            $tx->save();
        }
        $this->setInitValues();
    }
    
    // public function saveItems(){
    //     foreach($this->items_to_find AS $item){
    //         if(isset($item['id'])){
    //             $itm = ItemToFind::find($item['id']);
    //         }
    //         else{
    //             $itm = new ItemToFind();
    //         }
    //         if($item['item_name'] && $item['location']){
    //             $itm->user_id = Auth::user()->id;
    //             $itm->item_name = $item['item_name'];
    //             $itm->location = $item['location'];
    //             $itm->save();
    //         }
    //     }
    //     $this->dispatchBrowserEvent('data-updated', ['message' => 'Updated successfully.']);
    // }
    
    // public function removeFind($id){
    //     $it = ItemToFind::find($id);
    //     $it->delete();
    //     $this->setInitValues();
    // }

    public function initModalSeen(){
        $pop_up = PopUp::where('user_id', Auth::user()->id)->first();
        if($pop_up){
            $pop_up->safe = 1;
            $pop_up->save();
        }
        $this->dispatchBrowserEvent('close-modals');
    }
    
    public function render(){
        return view('livewire.account.organiser.safe-keeping.safe-keeping');
    }
}
