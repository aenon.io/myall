<?php

namespace App\Http\Livewire\Account\Organiser\SocialMedias;

use Livewire\Component;
use Auth;
use App\Models\SocialMedia;
use App\Models\PopUp;

class SocialMedias extends Component
{
    public $networks;
    public $network, $specific_network, $user_name, $link, $legacy_contact, $instructions;
    public $cur_id;

    public $show_init_pop_up = false;
    
    public function mount(){
        $this->networks = [
            'Facebook',
            'Twitter',
            'LinkedIn',
            'Instagram',
            'Other'
        ];
        
        $pop_up = PopUp::where('user_id', Auth::user()->id)->first();
        if($pop_up){
            if($pop_up->social){
                $this->show_init_pop_up = false;
            }
            else{
                $this->show_init_pop_up = true;
            }
        }
        else{
            $this->show_init_pop_up = true;
            $pop_up = new PopUp();
            $pop_up->user_id = Auth::user()->id;
            $pop_up->save();
        }
    }
    
    public function initModalSeen(){
        $pop_up = PopUp::where('user_id', Auth::user()->id)->first();
        if($pop_up){
            $pop_up->social = 1;
            $pop_up->save();
        }
        $this->dispatchBrowserEvent('close-modals');
    }

    public function showModal(){
        $this->dispatchBrowserEvent('show-form-modal');
    }
    
    public function showEdit($id){
        $ntwrk = SocialMedia::find($id);
        $this->cur_id = $id;
        
        $this->network = $ntwrk->network; 
        $this->specific_network = $ntwrk->specific_network;
        $this->user_name = $ntwrk->user_name;
        $this->link = $ntwrk->link;
        $this->legacy_contact = $ntwrk->legacy_contact;
        $this->instructions = $ntwrk->instructions;
        $this->dispatchBrowserEvent('show-form-modal');
    }

    public function saveSocialMedia(){
        $this->validate([
            'network' => 'required',  
            'user_name' => 'required', 
            'link' => 'nullable',
        ]);

        if($this->cur_id){
            $ntwrk = SocialMedia::find($this->cur_id);
        }
        else{
            $ntwrk = new SocialMedia();
        }

        $ntwrk->user_id = Auth::user()->id;
        $ntwrk->network = $this->network; 
        $ntwrk->specific_network = $this->specific_network;
        $ntwrk->user_name = $this->user_name;
        $ntwrk->link = $this->link;
        $ntwrk->legacy_contact = $this->legacy_contact;
        $ntwrk->instructions = $this->instructions;
        $ntwrk->save();

        $this->dispatchBrowserEvent('close-form-modal');
        session()->flash('message', 'Successfully saved.');
        $this->clearFields();
    }

    public function clearFields(){
        $this->cur_id = null;
        $this->network = null; 
        $this->specific_network = null; 
        $this->user_name = null; 
        $this->link = null; 
        $this->legacy_contact = null; 
        $this->instructions = null;
    }
    
    public function remove($id){
        $md = SocialMedia::find($id);
        $md->delete();
    }

    public function render(){
        $medias = SocialMedia::where('user_id', Auth::user()->id)->get()->groupBy('network');
        return view('livewire.account.organiser.social-medias.social-medias', [
            'medias' => $medias
        ]);
    }
}
