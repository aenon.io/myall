<?php

namespace App\Http\Livewire\Account\Partials;

use Livewire\Component;

class MySearchBar extends Component
{
    public function render()
    {
        return view('livewire.account.partials.my-search-bar');
    }
}
