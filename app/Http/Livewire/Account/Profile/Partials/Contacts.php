<?php

namespace App\Http\Livewire\Account\Profile\Partials;

use Livewire\Component;
use App\Models\Share as ShareModel;
use Auth;

class Contacts extends Component
{
    protected $listeners = ['loadTabContentProfile' => 'handleLoadTabContent', 'emitNewShare' => 'handleNewShare', 'loadViewShare' => 'handleLoadViewShare'];

    public $fm_acc, $fm_acc_id;
    public $cmp_acc, $cmp_acc_id;

    public $activeTab;

    public function showActionButtons($id, $type)
    {
        if ($type == 'fam') {
            $this->fm_acc = true;
            $this->fm_acc_id = $id;

            $this->cmp_acc = false;
            $this->cmp_acc_id = null;
        }
        if ($type == 'cmp') {
            $this->cmp_acc = true;
            $this->cmp_acc_id = $id;

            $this->fm_acc = false;
            $this->fm_acc_id = null;
        }
    }

    public function handleLoadTabContent($tab)
    {
        $this->activeTab = $tab;
        session()->put('profileActiveTab', $tab);
    }

    public function handleNewShare()
    {
        session()->put('sharesActiveTab', 'tabShared');
        return redirect('share-profile/form/');
    }

    public function loadViewShare($id, $tab)
    {
        session()->put('sharesActiveTab', $tab);
        return redirect('share-profile/edit/' . $id);
    }

    public function mount()
    {
        $this->activeTab = session()->get('profileActiveTab', 'tabShared');
    }

    public function render()
    {
        $shared_with = ShareModel::where('user_id', Auth::user()->id)->get();
        $shared_with_me = ShareModel::where('email', Auth::user()->email)
            ->orWhere('contact_id', Auth::user()->id)
            ->get();
        return view('livewire.account.profile.partials.contacts', [
            'shared_with' => $shared_with,
            'shared_with_me' => $shared_with_me,
            'activeTab' => $this->activeTab,
        ]);
    }
}
