<?php

namespace App\Http\Livewire\Account\Profile\Partials;

use App\Models\OrganiserStatus;
use Auth;

use App\Lib\Sharedfunctions;
use Livewire\Component;

class Organiser extends Component
{
    public $cur_progress, $cur_progress_text;
    public $organiser_progress, $organiser_percentage;

    protected $organiser_sections = [
        'personal' => 'Personal',
        'emergency_contacts' => 'Emergency Contacts',
        'business_wealth' => 'Business Wealth',
        'personal_wealth' => 'Personal Wealth',
        'debt_and_expenses' => 'Debt & Expenses',
        'social_media' => 'Social Media',
        'safekeeping' => 'Safekeeping',
        'memories' => 'Memories',
        'funeral' => 'Funeral',
    ];

    public function setInitData()
    {
        $funcs = new Sharedfunctions();

        // $organiser = $funcs->getOrganiserStatus();
        // $this->organiser_progress = $organiser;

        $progress = $funcs->getProgress();
        $this->cur_progress = $progress['stage'];
        $pr_arr = [
            0 => 'Lets Get Started!',
            20 => 'Well Done!',
            40 => 'Lets keep going!',
            60 => 'Over halfway done, wow!',
            80 => 'Almost there!',
            100 => 'Done!',
        ];
        foreach ($pr_arr as $k => $v) {
            if ($k == $this->cur_progress || $k < $this->cur_progress) {
                $this->cur_progress_text = $v;
            }
        }
    }

    public function setOrganiserStatus()
    {
        $statuses = OrganiserStatus::where('user_id', Auth::user()->id)->get();

        $sections = $statuses->pluck('organiser_section')->toArray();
        $uniqueSections = array_unique($sections);
        $uniqueCount = count($uniqueSections);
        $totalSections = count($this->organiser_sections);
        $percentage = ($uniqueCount / $totalSections) * 100;

        $this->organiser_percentage = round($percentage);
    }

    public function mount()
    {
        $this->setInitData();
        $this->setOrganiserStatus();
    }

    public function render()
    {
        return view('livewire.account.profile.partials.organiser');
    }
}
