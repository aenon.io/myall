<?php

namespace App\Http\Livewire\Account\Profile\Partials;

use Livewire\Component;
use App\Models\UserEvent;
use Auth;
use Carbon\Carbon;

class Timeline extends Component
{
    public function render()
    {
        $events = UserEvent::where('user_id', Auth::user()->id)->get();
        $groupedEvents = $events->groupBy(function ($event) {
            $eventDate = Carbon::parse($event->timestamp);
            if ($eventDate->isToday()) {
                return 'Today';
            }
            if ($eventDate->isYesterday()) {
                return 'Yesterday';
            }
            return $eventDate->diffForHumans();
        });
        return view('livewire.account.profile.partials.timeline', [
            'groupedEvents' => $groupedEvents,
        ]);
    }
}
