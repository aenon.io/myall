<?php

namespace App\Http\Livewire\Account\Profile;

use Livewire\Component;
use Livewire\WithFileUploads;

use Auth;

use App\Models\User;
use App\Models\Contact;

use App\Lib\Sharedfunctions;
use App\Lib\willPdfGen;

class Profile extends Component
{
    use WithFileUploads;

    public $activeTab = 'tabShared';
    public $view;
    public $file_selected = false;
    public $img_preview, $profile_pic, $name, $middle_name, $surname, $email, $contact_number, $id_number, $passport_number, $date_of_birth, $occupation, $bio;
    // public $cur_progress, $cur_progress_text;
    public $contact_type, $cur_contact_id;
    public $contact_name, $contact_surname, $contact_email, $relation, $organisation_or_company;

    public $fm_acc, $fm_acc_id;
    public $cmp_acc, $cmp_acc_id;

    public $showModal = true;

    public $relations;

    public function mount($view = null)
    {
        if ($view) {
            $this->view = $view;
        } else {
            $this->view = 'home';
            // $this->setInitData();
        }

        if ($this->view == 'edit') {
            $this->loadUserinfo();
        }

        $this->relations = [
            'Great Grand Father',
            'Great Grand Mother',

            'Grand Father',
            'Grand Mother',

            'Father',
            'Uncle',
            'Step Father',
            'Father In Law',

            'Mother',
            'Aunt',
            'Step Mother',
            'Mother In Law',

            'Brother',
            'Step Brother',
            'Brother In Law',

            'Sister',
            'Step Sister',
            'Sister In Law',

            'Cousin',
            'Friend',

            'Spouse',
            'Partner',
            'Wife',
            'Husband',

            'Son',
            'Daughter',
            'Son In Law',
            'Daughter In Law',
            "Son's Partner",
            "Daughter's Partner",
            'Niece',
            'Nephew',
            'Step Son',
            'Step Daughter',

            'Grand Son',
            'Grand Daughter',
            /*
            'Great Grand Son',
            'Great Grand Daughter',
            */
        ];
    }

    public function closeWelcomeModal($vid = null)
    {
        $usr = Auth::user();
        $usr->welcome_modal = 1;
        $usr->save();
        if ($vid) {
            $this->dispatchBrowserEvent('show-emploer-vid-modal');
        }
    }

    public function closeModal()
    {
        $this->showModal = false;
        $this->dispatchBrowserEvent('show-modal');
    }

    public function downloadPdf()
    {
        $pdf_gen = new willPdfGen(Auth::user()->id);
        $pdfContent = $pdf_gen->getPdf();
        $filename = 'Will.pdf';
        return response()->streamDownload(function () use ($pdfContent) {
            echo $pdfContent;
        }, $filename);
    }

    public function showActionButtons($id, $type)
    {
        if ($type == 'fam') {
            $this->fm_acc = true;
            $this->fm_acc_id = $id;

            $this->cmp_acc = false;
            $this->cmp_acc_id = null;
        }
        if ($type == 'cmp') {
            $this->cmp_acc = true;
            $this->cmp_acc_id = $id;

            $this->fm_acc = false;
            $this->fm_acc_id = null;
        }
    }

    public function deleteContact($id)
    {
        $cnt = Contact::find($id);
        $cnt->delete();
    }

    public function clearFields()
    {
        $this->profile_pic = null;
        $this->name = null;
        $this->surname = null;
        $this->email = null;
        $this->contact_number = null;
        $this->id_number = null;
        $this->date_of_birth = null;
        $this->occupation = null;
        $this->bio = null;
        $this->contact_type = null;
        $this->contact_name = null;
        $this->contact_surname = null;
        $this->contact_email = null;
        $this->relation = null;
        $this->organisation_or_company = null;
        $this->cur_contact_id = null;
    }

    public function saveContact()
    {
        $rules_arr = [
            'contact_name' => 'required',
            'contact_surname' => 'required',
            'contact_email' => 'required',
        ];
        if ($this->contact_type == 'contact') {
            $rules_arr['relation'] = 'required';
        }
        if ($this->contact_type == 'banks_attorneys') {
            $rules_arr['organisation_or_company'] = 'required';
        }
        $this->validate($rules_arr);

        if ($this->cur_contact_id) {
            $cnt = Contact::find($this->cur_contact_id);
        } else {
            $cnt = new Contact();
        }
        $cnt->user_id = Auth::user()->id;
        $cnt->name = $this->contact_name;
        $cnt->surname = $this->contact_surname;
        if ($this->contact_type == 'contact') {
            $cnt->relation = $this->relation;
        }
        if ($this->contact_type == 'banks_attorneys') {
            $cnt->relation = $this->organisation_or_company;
        }
        $cnt->email = $this->contact_email;
        $cnt->contact_number = $this->contact_number;
        $cnt->contact_type = $this->contact_type;
        $cnt->save();
        $this->clearFields();
        $this->dispatchBrowserEvent('close-contact-modal');
    }

    public function updateUser($formType)
    {
        if ($formType === 'profile_pic') {
            $this->validate([
                'profile_pic' => 'nullable|file|mimes:jpeg,png|max:1048', // 1MB Max
            ]);

            $img_url = null;
            if ($this->profile_pic) {
                $img_url = $this->profile_pic->storePublicly('photos', 'public');
            }

            $usr = Auth::user();
            if ($img_url) {
                $usr->profile_pic = $img_url;
            }
            $usr->save();
        } elseif ($formType === 'details') {
            $this->validate([
                'name' => 'required',
                'surname' => 'required',
                'id_number' => 'nullable|required_without:passport_number|said',
                'passport_number' => 'required_without:id_number',
                'date_of_birth' => 'required',
                'profile_pic' => 'nullable|file|mimes:jpeg,png|max:1048',
            ]);

            $usr = Auth::user();
            $usr->name = $this->name;
            $usr->middle_name = $this->middle_name;
            $usr->surname = $this->surname;
            $usr->email = $this->email;
            $usr->contact_number = $this->contact_number;
            $usr->id_number = $this->id_number;
            $usr->passport_number = $this->passport_number;
            $usr->date_of_birth = $this->date_of_birth;
            $usr->occupation = $this->occupation;
            $usr->bio = $this->bio;
            $usr->save();
        }

        return redirect('profile');
    }

    public function updatedProfilePic()
    {
        $this->file_selected = $this->profile_pic != null;
        if ($this->file_selected) {
            $this->img_preview = $this->profile_pic->temporaryUrl();
        }
    }

    public function loadUserinfo()
    {
        $usr = Auth::user();

        if ($usr->profile_pic) {
            $this->img_preview = 'storage/' . $usr->profile_pic;
        } else {
            $this->img_preview = 'img/placeholder.png';
        }

        $this->name = $usr->name;
        $this->middle_name = $usr->middle_name;
        $this->surname = $usr->surname;
        $this->email = $usr->email;
        $this->contact_number = $usr->contact_number;
        $this->id_number = $usr->id_number;
        $this->passport_number = $usr->passport_number;
        $this->date_of_birth = $usr->date_of_birth;
        $this->occupation = $usr->occupation;
        $this->bio = $usr->bio;
    }

    // public function setInitData()
    // {
    //     $funcs = new Sharedfunctions();
    //     $progress = $funcs->getProgress();
    //
    //     $this->cur_progress = $progress['stage'];
    //     $pr_arr = [
    //         0 => 'Lets Get Started!',
    //         20 => 'Well Done!',
    //         40 => 'Lets keep going!',
    //         60 => 'Over halfway done, wow!',
    //         80 => 'Almost there!',
    //         100 => 'Done!',
    //     ];
    //     foreach ($pr_arr as $k => $v) {
    //         if ($k == $this->cur_progress || $k < $this->cur_progress) {
    //             $this->cur_progress_text = $v;
    //         }
    //     }
    // }

    public function showAddContactModal($type)
    {
        $this->contact_type = $type;
        $this->dispatchBrowserEvent('show-contact-modal');
    }

    public function showEditContactModal($id)
    {
        $cnt = Contact::find($id);
        $this->cur_contact_id = $id;
        $this->contact_type = $cnt->contact_type;
        $this->contact_name = $cnt->name;
        $this->contact_surname = $cnt->surname;
        $this->contact_email = $cnt->email;
        $this->contact_number = $cnt->contact_number;
        if ($cnt->contact_type == 'contact') {
            $this->relation = $cnt->relation;
        } else {
            $this->organisation_or_company = $cnt->relation;
        }
        $this->dispatchBrowserEvent('show-contact-modal');
    }

    public function render()
    {
        return view('livewire.account.profile.profile');
    }
}
