<?php

namespace App\Http\Livewire\Account\Search;

use Livewire\Component;
use Request;

use App\Models\InfoGraphic;

class Search extends Component
{
    public function render(){
        $key = Request::input('s');
        $results = null;

        if($key){
            $key = strtolower($key);

            $results = InfoGraphic::query()
            ->where('title', 'LIKE', '%'.$key.'%')
            ->orWhereHas('infoKey', function($qq) use($key){
                return $qq->whereHas('word', function($qqq) use($key){
                    return $qqq->where('word', 'LIKE', '%'.$key.'%');
                });
            })
            ->get();
        }
        return view('livewire.account.search.search', [
            'results' => $results
        ]);
    }
}
