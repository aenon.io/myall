<?php

namespace App\Http\Livewire\Account\Share\Pages;

use Livewire\Component;
use Auth;
use Mail;
use App\Models\Share;
use App\Models\ShareOption;
use App\Models\Contact;

class Form extends Component
{
    public $share_id;
    public $show_specify = false;
    public $families = [],
        $friends_arr = [],
        $professional_arr = [],
        $sections = [],
        $share_sections = [];
    public $name, $email, $contact_number, $id_number, $dob, $family, $friends, $professional, $specify_relation, $emergency_contact, $accept_share_terms;
    public $share_consent;
    public $shared_opts = [];

    public function mount($id = null)
    {
        if ($id) {
            $this->share_id = $id;
        }
        $this->initData();
        $this->getShareInfo();
    }

    public function showShareHistoryModal($id)
    {
        $this->shared_opts = ShareOption::find($id)->toArray();
        $this->dispatchBrowserEvent('show-shared-options-modal');
    }

    public function deleteShare()
    {
        $sh = Share::find($this->share_id);
        foreach ($sh->shareOptions as $opt) {
            $opt->delete();
        }
        $sh->delete();
        return redirect('share-profile');
    }

    public function shareProfile()
    {
        $this->dispatchBrowserEvent('close-share-modal');
        if ($this->share_consent) {
            $this->saveShare('mail');
        } else {
            $this->dispatchBrowserEvent('alert-error', ['message' => 'Please click on "I understand" to continue.']);
        }
    }

    public function getShareInfo()
    {
        if ($this->share_id) {
            $share = Share::find($this->share_id);
            if ($share) {
                $this->name = $share->name;
                $this->email = $share->email;
                $this->contact_number = $share->contact_number;
                $this->id_number = $share->id_number;
                $this->dob = $share->dob;
                $this->family = $share->family;
                $this->friends = $share->friends;
                $this->professional = $share->professional;
                $this->specify_relation = $share->specify_relation;
                if ($share->emergency_contact) {
                    $this->emergency_contact = 'yes';
                }
                $this->accept_share_terms = 'yes';

                if ($share->activeShare()) {
                    $ignore = ['id', 'share_id', 'created_at', 'updated_at'];
                    foreach ($share->activeShare()->toArray() as $key => $val) {
                        if (!in_array($key, $ignore)) {
                            if ($val) {
                                $this->share_sections[] = $key;
                            }
                        }
                    }
                }
            }
        }
    }

    public function saveShare($mail = null)
    {
        $this->validate(
            [
                'name' => 'required',
                'email' => 'required',
                'id_number' => 'required',
                'dob' => 'required',
                'accept_share_terms' => 'required',
            ],
            [
                'accept_share_terms.required' => 'You did not accept terms of sharing your profile.',
            ],
        );

        if ($this->emergency_contact) {
            if ($this->contact_number == '') {
                $this->addError('error', 'Please enter a contact number');
                return;
            }
        }

        if ($this->share_id) {
            $sh = Share::find($this->share_id);
        } elseif (
            Share::where('user_id', Auth::user()->id)
                ->where('email', $this->email)
                ->first()
        ) {
            $sh = Share::where('user_id', Auth::user()->id)
                ->where('email', $this->email)
                ->first();
        } else {
            $sh = new Share();
        }

        $sh->user_id = Auth::user()->id;
        $sh->name = $this->name;
        $sh->email = $this->email;
        $sh->contact_number = $this->contact_number;
        $sh->id_number = $this->id_number;
        $sh->dob = $this->dob;
        $sh->family = $this->family;
        $sh->friends = $this->friends;
        $sh->professional = $this->professional;
        $sh->specify_relation = $this->specify_relation;
        if ($this->emergency_contact) {
            $sh->emergency_contact = 1;
        } else {
            $sh->emergency_contact = 0;
        }
        $sh->save();

        $share_option = new ShareOption();
        $share_option->share_id = $sh->id;
        foreach ($this->sections as $k => $sec) {
            if (is_array($sec)) {
                foreach ($sec as $kk => $ss) {
                    if (is_array($ss)) {
                        foreach ($ss as $sss) {
                            if (in_array($sss, $this->share_sections)) {
                                $share_option->$sss = 1;
                                $share_option->$kk = 1;
                                $share_option->$k = 1;
                            }
                        }
                    } elseif (in_array($ss, $this->share_sections)) {
                        $share_option->$k = 1;
                        $share_option->$ss = 1;
                    }
                }
            } elseif (in_array($sec, $this->share_sections)) {
                $share_option->$sec = 1;
            }
        }
        $share_option->save();

        if ($this->emergency_contact) {
            if ($this->contact_number) {
                $cnt = Contact::where('user_id', Auth::user()->id)
                    ->where('email', $this->email)
                    ->first();
                if (!$cnt) {
                    $cnt = new Contact();
                }

                $cnt->user_id = Auth::user()->id;
                $cnt->name = $this->name;
                $cnt->surname = '';
                if ($this->family) {
                    $cnt->relation = $this->family;
                    $cnt->contact_type = 'family';
                }
                if ($this->friends) {
                    $cnt->relation = $this->friends;
                    $cnt->contact_type = 'personal';
                }
                if ($this->professional) {
                    $cnt->relation = $this->professional;
                    $cnt->contact_type = 'other';
                }
                $cnt->specify_relation = $this->specify_relation;
                $cnt->email = $this->email;
                $cnt->contact_number = $this->contact_number;
                $cnt->save();
            } else {
                $this->addError('error', 'Please enter a contact number');
            }
        }

        if ($mail) {
            $data = [
                'name' => Auth::user()->name . ' ' . Auth::user()->surname,
                'id' => Auth::user()->id,
                'hash' => md5($this->email),
            ];
            $to = $this->email;

            Mail::send('mail.invite', $data, function ($message) use ($to) {
                $message->to($to, '')->subject('MyAll Invite');
                // TODO: This needs to change when in production
                $message->from('hello@myall.co.za', 'MyAll Team');
                // $message->from('hello@myall.co.za', 'MyAll Team');
            });

            session()->flash('message', 'Profile shared successfully.');
        } else {
            session()->flash('message', 'Profile updated successfully.');
        }
    }

    public function updatedFamily()
    {
        if ($this->family != '') {
            $this->friends = '';
            $this->professional = '';
            if ($this->family == 'Other') {
                $this->show_specify = true;
            } else {
                $this->show_specify = false;
            }
        }
    }

    public function updatedFriends()
    {
        if ($this->friends != '') {
            $this->family = '';
            $this->professional = '';
            if ($this->friends == 'Other') {
                $this->show_specify = true;
            } else {
                $this->show_specify = false;
            }
        }
    }

    public function updatedProfessional()
    {
        if ($this->professional != '') {
            $this->family = '';
            $this->friends = '';
            if ($this->professional == 'Other') {
                $this->show_specify = true;
            } else {
                $this->show_specify = false;
            }
        }
    }

    public function initData()
    {
        $this->family = '';
        $this->friends = '';
        $this->professional = '';

        $this->families = ['Great Grand Father', 'Great Grand Mother', 'Grand Father', 'Grand Mother', 'Father', 'Uncle', 'Step Father', 'Father In Law', 'Mother', 'Aunt', 'Step Mother', 'Mother In Law', 'Brother', 'Step Brother', 'Brother In Law', 'Sister', 'Step Sister', 'Sister In Law', 'Cousin', 'Friend', 'Spouse', 'Partner', 'Wife', 'Husband', 'Son', 'Daughter', 'Son In Law', 'Daughter In Law', "Son's Partner", "Daughter's Partner", 'Niece', 'Nephew', 'Step Son', 'Step Daughter', 'Grand Son', 'Grand Daughter', 'Other'];
        sort($this->families);
        $this->friends_arr = ['Romantic Friend', 'Best Friend', 'Work Friend', 'Social Friend', 'Other'];
        sort($this->friends_arr);

        $this->professional_arr = ['Colleague', 'Employer', 'Accountant', 'Bookkeeper', 'Private Banker', 'Lawyer', 'Other'];
        sort($this->professional_arr);

        $this->sections = [
            'profile',
            'organiser' => [
                'personal' => [
                    // 'spouse',
                    'work',
                    'education',
                    // 'tax',
                    'children',
                    'medical_aid',
                    // 'funeral_cover'
                ],
                'connections' => ['family', 'friends', 'personal_connections', 'business', 'doctors', 'emergency', 'accountant_bookkeeper', 'private_banker', 'lawyer'],
                'properties_wealth' => ['assets', 'investments_and_policies', 'bank_accounts'],
                'debt_expenses' => ['expenses', 'loans', 'insurance', 'other_expenses'],
                'safe_keeping' => [
                    'safe_keeping',
                    // 'where_to_find',
                    'income_tax',
                ],
                'business_wealth',
                'social_media',
                'memories',
                'funeral',
            ],
            // 'family_tree',
        ];
    }

    public function render()
    {
        $cur_share = null;
        if ($this->share_id) {
            $cur_share = Share::find($this->share_id);
        }
        return view('livewire.account.share.pages.form', [
            'cur_share' => $cur_share,
        ]);
    }
}
