<?php

namespace App\Http\Livewire\Account\Share\Pages;

use Livewire\Component;

use Auth;
use App\Models\Share;

class SharedWithMe extends Component
{
    public $contact_id, $cur_view, $view;

    public function mount($id)
    {
        $this->contact_id = $id;
        $this->showPage('profile', 'profile');
    }

    public function showPage($menu, $view)
    {
        $this->cur_view = $menu;
        $this->view = $view;
    }

    public function deleteShare()
    {
        $sh = Share::where('email', Auth::user()->email)
            ->where('user_id', $this->contact_id)
            ->first();
        foreach ($sh->shareOptions as $opt) {
            $opt->delete();
        }
        $sh->delete();
        return redirect('share-profile');
    }

    public function render()
    {
        $share = null;
        $cur_share = null;
        $cur_contact = null;

        if ($this->contact_id) {
            $share = Share::where('email', Auth::user()->email)
                ->where('user_id', $this->contact_id)
                ->first();
            if ($share) {
                $cur_share = $share->activeShare();
                $cur_contact = $share->user;
            }
        }
        return view('livewire.account.share.pages.shared-with-me', [
            'share' => $share,
            'cur_share' => $cur_share,
            'cur_contact' => $cur_contact,
        ]);
    }
}
