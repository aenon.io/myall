<?php

namespace App\Http\Livewire\Account\Share\Partials;

use Livewire\Component;

class ReceivedTab extends Component
{
    public $view;
    public $share_id;
    public $contact_id;
    public $shared_with_me;
    public $activeTab;

    public function mount($view, $share_id, $contact_id, $shared_with_me)
    {
        $this->view = $view;
        $this->share_id = $share_id;
        $this->contact_id = $contact_id;
        $this->shared_with_me = $shared_with_me;
        $this->activeTab = session()->get('sharesActiveTab', 'tabShared');
    }

    public function render()
    {
        return view('livewire.account.share.partials.received-tab', [
            'view' => $this->view,
            'share_id' => $this->share_id,
            'contact_id' => $this->contact_id,
            'shared_with_me' => $this->shared_with_me,
        ]);
    }
}
