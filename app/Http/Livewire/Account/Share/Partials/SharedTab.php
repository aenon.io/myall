<?php

namespace App\Http\Livewire\Account\Share\Partials;

use Livewire\Component;

class SharedTab extends Component
{
    public $view;
    public $share_id;
    public $contact_id;
    public $shared_with;

    public function mount($shared_with, $view, $share_id, $contact_id)
    {
        $this->shared_with = $shared_with;
        $this->view = $view;
        $this->share_id = $share_id;
        $this->contact_id = $contact_id;
    }

    public function render()
    {
        return view('livewire.account.share.partials.shared-tab', [
            'shared_with' => $this->shared_with,
            'view' => $this->view,
            'share_id' => $this->share_id,
            'contact_id' => $this->contact_id,
        ]);
    }
}
