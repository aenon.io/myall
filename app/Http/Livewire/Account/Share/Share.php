<?php

namespace App\Http\Livewire\Account\Share;

use Livewire\Component;
use App\Models\Share as ShareModel;
use Auth;
use App\Models\User;

class Share extends Component
{
    protected $listeners = ['loadTabContent' => 'handleLoadTabContent', 'viewShareActivated' => 'handleViewShareActivated'];

    public $view, $share_id, $contact_id;
    public $single_info_image;
    public $activeTab;

    public function mount($view = null, $id = null)
    {
        $this->activeTab = session()->get('sharesActiveTab');

        if (!$this->activeTab) {
            if (Auth::user()->free_account == 1) {
                $this->activeTab = 'tabReceived';
            } else {
                $this->activeTab = 'tabContacts';
            }
        }

        if ($view) {
            $this->view = $view;
            if ($view == 'edit') {
                if ($id) {
                    $this->share_id = $id;
                }
            }
            if ($view == 'view') {
                if ($id) {
                    $this->contact_id = $id;
                }
            }
        } else {
            $this->view = 'view';
        }
        $this->checkShares();
    }

    public function handleLoadTabContent($tab)
    {
        $this->activeTab = $tab;
        session()->put('sharesActiveTab', $tab);
    }

    public function handleViewShareActivated($id)
    {
        $this->handleLoadTabContent('tabShared');
        return redirect('share-profile/edit/' . $id);
    }

    public function checkShares()
    {
        $shared = ShareModel::where('user_id', Auth::user()->id)
            ->orWhere('email', Auth::user()->email)
            ->whereNull('contact_id')
            ->get();
        foreach ($shared as $sh) {
            $usr = User::where('email', $sh->email)->first();
            if ($usr) {
                $sh->contact_id = $usr->id;
                $sh->save();
            }
        }
    }

    public function deleteShare($id)
    {
        $sh = ShareModel::find($id);
        foreach ($sh->shareOptions as $opt) {
            $opt->delete();
        }
        $sh->delete();
    }

    public function showSingleInfoImage($url)
    {
        $this->single_info_image = 'infographics/share/' . $url;
        $this->dispatchBrowserEvent('show-single-info-image');
    }

    public function render()
    {
        $shared_with = ShareModel::where('user_id', Auth::user()->id)->get();
        $shared_with_me = ShareModel::where('email', Auth::user()->email)
            ->orWhere('contact_id', Auth::user()->id)
            ->get();
        return view('livewire.account.share.share', [
            'shared_with' => $shared_with,
            'shared_with_me' => $shared_with_me,
        ]);
    }
}
