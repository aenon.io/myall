<?php

namespace App\Http\Livewire\Account\Share;

use Livewire\Component;

use Auth;
use Mail;
use App\Models\Share AS ShareModel;
use App\Models\User;
use App\Lib\FamilyTreeGen;
use App\Models\Family;

class Share1 extends Component
{
    public $sections = [];
    public $name, $email, $id_number, $share_sections = [];
    public $cur_contact_id;
    public $confirmModal = false;
    public $cur_contact, $cur_share;
    public $showSharePage = false;
    public $questionnaire_questions = [];
    
    public function mount(){
        $this->questionnaire_questions = [
            "favorite_song" => "Favorite Song",
            "favorite_movie" => "Favorite Movie",
            "favorite_place" => "Favorite Place visited and why",
            "favorite_food" => "Favorite food",
            "special_memory" => "Special memory that comes to mind",
            "biggest_lesson" => "Your biggest lesson in life",
            "regret" => "A regret",
            "miss_most" => "You will miss the most",
            "life_best_part" => "Best part of life",
            "life_worst_part" => "Worst part of life",
            "religion_beliefs" => "Religion / Beliefs",
            "funeral_wish" => "Funeral wishes - anything specific",
            "dint_know" => "What you din't know",
            "hopes_for_someone" => "What you hope for someone",
            "happy_thing" => "This made you very happy",
            "sad_thing" => "This made you very sad",
            "best_quality" => "Your best quality",
            "remember_me_by" => "What must people remember about you?",
            "funny_memory" => "A funny memory",
            "bucket_list" => "Bucket list"
        ];
        $this->sections = [
            'profile',
            'organiser' => [
                'personal' => [
                    'spouse',
                    'work',
                    'education',
                    // 'tax',
                    'children',                 
                    'medical_aid',
                    'funeral_cover'
                ],
                'connections' => [
                    'family',
                    'friends',
                    'personal_connections',
                    'business',
                    'doctors',
                    'emergency',
                    'accountant_bookkeeper',
                    'private_banker',
                    'lawyer'
                ],
                'properties_wealth' => [
                    'assets',
                    'investments_and_policies',
                    'bank_accounts'
                ],
                'debt_expenses' => [
                    'expenses',
                    'loans',
                    'insurance',
                    'other_expenses'
                ],
                'safe_keeping' => [
                    'safe_keeping',
                    // 'where_to_find',
                    'income_tax'
                ],
                'business_wealth',
                'social_media',
                'memories',
                'funeral',
            ],
            'family_tree',
        ];
        if(Auth::user()->free_account == 1){
            $this->showSharePage = true;
            $shared_with_me = ShareModel::where('id_number', Auth::user()->id_number)->orWhere('email', Auth::user()->email)->first();
            if($shared_with_me){
                $this->showSharePage($shared_with_me->user->id);
            }
        }
    }

    public $cur_view, $view;
    public $tree;

    public function showPage($menu, $view){
        $this->cur_view = $menu;
        $this->view = $view;
    }

    public function showSharePage($id){
        $this->cur_contact = User::find($id);
        $this->cur_share = ShareModel::where('user_id', $id)->where('email', Auth::user()->email)->first();
        $this->cur_view = "profile";
        $this->view = "profile";
        $this->showSharePage = true;

        $tree_gen = new FamilyTreeGen();
        $fam = Family::where('user_id', $id)->first();
        $this->tree = null;
        if($fam){
            $this->tree = json_encode($tree_gen->get_tree($fam->id));
        }
    }

    public function removeShare($id){
        $cnt = ShareModel::find($id);
        $cnt->delete();
    }

    public function clearShareForm(){
        $this->name = null; 
        $this->email = null; 
        $this->id_number = null; 
        $this->share_sections = [];
        $this->cur_contact_id = null;
        $this->showSharePage = false;
    }

    public function showShare($id){
        $cnt = ShareModel::find($id);
        if($cnt){
            $this->cur_contact_id = $cnt->id;
            $this->name = $cnt->name; 
            $this->email = $cnt->email; 
            $this->id_number = $cnt->id_number;

            $cnt_arr = $cnt->toArray();
            unset($cnt_arr['id']);
            unset($cnt_arr['user_id']);
            unset($cnt_arr['name']);
            unset($cnt_arr['email']);
            unset($cnt_arr['hash']);
            unset($cnt_arr['id_number']);
            unset($cnt_arr['created_at']);
            unset($cnt_arr['updated_at']);

            $this->share_sections = [];

            foreach($cnt_arr AS $k=>$v){
                if($v == 1){
                    $this->share_sections[] = $k;
                }
            }
        }
    }
    
    public function shareProfile(){
        if($this->confirmModal){
            
            $this->validate([
                'name' => 'required', 
                'email' => 'required', 
                'id_number' => 'required'    
            ]);
        
            $sh = ShareModel::where('user_id', Auth::user()->id)->where('hash', md5($this->email))->first();
            if(!$sh){
                $sh = new ShareModel();
            }
        
            $sh->user_id = Auth::user()->id;
            $sh->name = $this->name;
            $sh->email = $this->email;
            $sh->hash = md5($this->email);
            $sh->id_number = $this->id_number;

            $cnt_arr = $sh->toArray();
            unset($cnt_arr['id']);
            unset($cnt_arr['user_id']);
            unset($cnt_arr['name']);
            unset($cnt_arr['email']);
            unset($cnt_arr['hash']);
            unset($cnt_arr['id_number']);
            unset($cnt_arr['created_at']);
            unset($cnt_arr['updated_at']);
            foreach($cnt_arr AS $k=>$v){
                $sh->$k = 0;
            }
            
            foreach($this->sections AS $k=>$sec){
                if(is_array($sec)){
                    foreach($sec AS $kk=>$ss){
                        if(is_array($ss)){
                            foreach($ss AS $sss){
                                if(in_array($sss, $this->share_sections)){
                                    $sh->$sss = 1;
                                    $sh->$kk = 1;
                                    $sh->$k = 1;
                                }
                            }
                        }
                        elseif(in_array($ss, $this->share_sections)){
                            $sh->$k = 1;
                            $sh->$ss = 1;
                        }
                    }
                }
                elseif(in_array($sec, $this->share_sections)){
                    $sh->$sec = 1;
                }
            }
            $sh->save();
            
            $data = [
                'name' => Auth::user()->name.' '.Auth::user()->surname,  
                'id' => Auth::user()->id,
                'hash' => md5($this->email),
            ];
            $to = $this->email;
            
            Mail::send('mail.invite', $data, function ($message) use($to){
                $message->to($to, '')
                ->subject('WrapUp Invite');
                $message->from('hello@wrapup.co.za', 'WrapUp');
            });
            
            $this->dispatchBrowserEvent('close-share-modal');
            session()->flash('message', 'Profile shared successfully.');
        }
    }

    public function showModal(){
        $this->dispatchBrowserEvent('show-share-modal');
    }
    
    public function closeModal(){
        $this->dispatchBrowserEvent('close-share-modal');
    }
    
    public function render()
    {
        $contacts = ShareModel::where('user_id', Auth::user()->id)->get();
        $shared_with_me = ShareModel::where('id_number', Auth::user()->id_number)->orWhere('email', Auth::user()->email)->get();
        return view('livewire.account.share.share', [
            'contacts' => $contacts,
            'shared_with_me' => $shared_with_me
        ]);
    }
}
