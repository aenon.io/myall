<?php

namespace App\Http\Livewire\Auth;

use Livewire\Component;
use App\Models\User;
use Illuminate\Support\Facades\Password;

class ForgotPassword extends Component
{
    public $email;

    public function resetPass()
    {
        $this->validate([
            'email' => 'required|email',
        ]);

        $user = User::where('email', $this->email)->first();

        if (!$user) {
            return $this->addError('email', 'The email address does not exist in our records.');
        }

        $token = Password::getRepository()->create($user);
        $user->sendPasswordResetNotification($token);

        session()->flash('message', 'We have sent a reset password link to your email.');
    }

    public function render()
    {
        return view('livewire.auth.forgot-password')->extends('layouts.landing');
    }
}
