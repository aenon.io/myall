<?php

namespace App\Http\Livewire\Auth;

use Livewire\Component;

use Auth;
use App\Models\User;
use Session;

class Login extends Component
{
    public $email, $password;
    public $show_password = false;
    
    public function mount(){
        if(Auth::user()){
            return redirect("profile");
        }
    }
    
    public function authUser(){
        $this->validate([
            'email' => 'required',
            'password' => 'required'
        ]);

        $credentials = [
            'email' => $this->email,
            'password' => $this->password
        ];
        if(Auth::attempt($credentials)){
            $usr = Auth::user();
            $date = false;
            $has_error = false;
            if($usr->status != 1){
                $this->addError('user', 'Your account is inactive.');
                $has_error = true;
            }
            
            if($usr->free_account == 1){
                if(Session::has('invited')){
                    Session::forget('invited');
                }
                $date = false;
                $has_error = false;
            }
            elseif(!$usr->payment_date){
                $date = true;
            }
            else{
                $paid_at = $usr->payment_date;
                $cur_date = date('Y-m-d');
                $date1 = \DateTime::createFromFormat('Y-m-d', $paid_at);
                $date2 = \DateTime::createFromFormat('Y-m-d', $cur_date);
                $interval = $date1->diff($date2);
                $diff = $interval->format('%m');
                if($diff > 12){
                    $date = true;
                }
            }
            
            if($date){
                Auth::logout();
                return redirect('register/payment/'.$usr->id);
            }
            elseif($has_error){
                Auth::logout();
                return redirect('login');
            }
            
            return redirect()->intended('profile');
        }
        $this->addError('user', 'Invalid email or password.');
    }

    public function showPassword(){
        $this->show_password = !$this->show_password;
    }
    
    public function render(){
        return view('livewire.auth.login')->extends('layouts.landing');
    }
}
