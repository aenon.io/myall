<?php

namespace App\Http\Livewire\Auth;

use Illuminate\Support\Facades\Session as Session;
use Illuminate\Support\Facades\Log as IlluminateLog;
use Illuminate\Support\Facades\Mail as Mail;
use Livewire\Component;

use App\Models\User;
use App\Models\Interview;
use Hash;
use Illuminate\Support\Facades\Auth as Auth;
use Request;
use Log;
use Illuminate\Validation\Rules\Password;

use ZxcvbnPhp\Zxcvbn;

class Register extends Component
{
    public $view, $user_id, $question_stage;
    public $name, $middle_name, $surname, $date_of_birth, $id_number, $passport_number, $gender, $ethnicity, $email, $password, $password_confirmation;
    public $form_data = [],
        $pf_url;

    public $own_property_outside_sa;
    public $q_one_options, $property_options;
    public $q_one_des, $q_two_des, $q_three_des, $q_four_des;
    public $q_one_pass, $q_two_pass, $q_three_pass, $q_four_pass;
    public $q_1_ans, $q2_ans;
    public $ethnicities;

    public $show_password = false;
    public $info_graphics = [];
    public $tip_body, $tip_title;

    protected $listeners = ['setSessionPlan' => 'putPlanSession'];
    public $selectedPlan;

    public function putPlanSession($data)
    {
        if (is_array($data)) {
            $plan = $data[0];
            $upgrade = $data[1] ?? false;
            session()->put('selectedPlan', 'monthly');
        } else {
            $plan = $data;
            $upgrade = false;
        }
        session()->put('selectedPlan', $plan);
        $this->selectedPlan = $plan;
        if ($upgrade) {
            return redirect('register/payment/' . Auth::user()->id);
        }
    }

    public function mount($view = null, $user_id = null)
    {
        $this->password_score = 0;
        if (Auth::user()) {
            if (Auth::user()->payment_date) {
                return redirect('profile');
            } else {
                $this->question_stage = 'form';
            }
        }

        if ($view) {
            $this->view = $view;
        }
        if ($user_id) {
            $this->user_id = $user_id;
        }
        if ($view == 'payment') {
            $this->setPaymentFields();
        }
        if ($view == 'process-payment' || $view == 'payment-complete') {
            $this->activateUser();
        }
        $this->q_one_options = false;
        $this->q_one_des = false;
        $this->q_two_des = false;
        $this->q_three_des = false;
        $this->q_four_des = false;
        if (Auth::guest()) {
            $this->question_stage = 'form';
        }

        if (Session::has('invited')) {
            $this->question_stage = 'form';
        }

        if (!Session::get('selectedPlan')) {
            $this->selectedPlan = 'monthly';
        } else {
            $this->selectedPlan = Session::get('selectedPlan');
        }

        $this->ethnicities = ['Asian', 'Black', 'Coloured', 'Indian', 'White'];
    }

    public $password_score, $password_warning;
    public $pass_bg;
    public function passwordMeter()
    {
        $this->pass_bg = 'bg-poor-pass';
        $zxcvbn = new Zxcvbn();
        $userData = [$this->name, $this->surname, $this->middle_name, $this->date_of_birth, $this->id_number];
        $strength = $zxcvbn->passwordStrength($this->password, $userData);
        $scrore = $strength['score'];
        $this->password_warning = $strength['feedback']['warning'];

        if ($scrore == 4) {
            if (strlen($this->password) < 15) {
                $scrore = 3;
                $this->password_warning = 'Password must be at least 15 characters';
            } elseif (!preg_match('/[A-Z]/', $this->password)) {
                $scrore = 3;
                $this->password_warning = 'Password must contain at least one uppercase letter';
            } elseif (!preg_match('/[a-z]/', $this->password)) {
                $scrore = 3;
                $this->password_warning = 'Password must contain at least one lowercase letter';
            } elseif (!preg_match('/[0-9]/', $this->password)) {
                $scrore = 3;
                $this->password_warning = 'Password must contain at least one number';
            } elseif (!preg_match('/[!@#$%^&*()_+=]/', $this->password)) {
                $scrore = 3;
                $this->password_warning = 'Password must contain at least one special character';
            }
        }

        $this->password_score = ($scrore / 4) * 100;
        if ($scrore == 1) {
            $this->pass_bg = 'bg-poor-pass';
        }
        if ($scrore == 2) {
            $this->pass_bg = 'bg-bad-pass';
        }
        if ($scrore == 3) {
            $this->pass_bg = 'bg-good-pass';
        }
        if ($scrore == 4) {
            $this->pass_bg = 'bg-excellent-pass';
        }
    }

    public function updatedPassword()
    {
        $this->passwordMeter();
    }

    public function showTipModal($type)
    {
        if ($type == 'ethnicity') {
            $this->tip_title = 'Ethnicity';
            // $this->tip_body = "Choosing ethnicity for the online will is necessary to determine marital status and comply with South African law, which recognises specific marriage structures based on culture and race.";
            $this->tip_body = 'Choosing your ethnicity is important when creating your online Will as it helps us determine your marital status and ensures compliance with South African law. South African law recognises specific marital structures based on culture and race.';
        } elseif ($type == 'gender') {
            $this->tip_title = 'Gender';
            //$this->tip_body = "This only helps us when creating your Online Will";
            $this->tip_body = 'This helps us when creating your Online Will';
        }
        $this->dispatchBrowserEvent('show-tip-modal');
    }

    public function showSingleGraphic($img)
    {
        $this->dispatchBrowserEvent('showSingleInfoModal', ['img' => $img]);
    }

    public function showInfoGraphics()
    {
        if ($this->question_stage == 1) {
            $this->info_graphics = [
                'Do you own property outside of South Africa' => 'infographics/stage1/Do you own property outside of South Africa.jpg',
                'Understanding legal competence in making a Will' => 'infographics/stage4/Understanding legal competence in making a Will.jpg',
            ];
        } else {
            $this->info_graphics = [
                'Exploring your signature options' => 'infographics/stage1/Exploring your signature options.jpg',
                'Understanding legal competence in making a Will' => 'infographics/stage4/Understanding legal competence in making a Will.jpg',
            ];
        }
        $this->dispatchBrowserEvent('showInfoModal');
    }

    public function showPassword()
    {
        $this->show_password = !$this->show_password;
    }

    public function setQuestionStage($stage)
    {
        $this->question_stage = $stage;
    }

    public function updateQustionOne($val)
    {
        if ($val == 'no') {
            $this->q_one_pass = 1;
            $this->question_stage = 2;
        } else {
            $this->q_one_options = true;
            $this->q_1_ans = $val;
            // $this->q_one_pass = 1;
        }
    }

    public function updateQustionTwo($val)
    {
        if ($val == 'yes') {
            if (Auth::user()) {
                $int = Interview::where('user_id', Auth::user()->id)->first();
                if (!$int) {
                    $int = new Interview();
                }

                $int->user_id = Auth::user()->id;
                $int->have_property_outside_sa = 'No';
                $int->read_and_write_capability = 'Yes';
                $int->save();

                return redirect('register/payment/' . Auth::user()->id);
            } else {
                $this->question_stage = 'form';
            }
        } else {
            $this->q_two_des = true;
            $this->dispatchBrowserEvent('go-to-bottom');
        }
    }

    public function updatedQOnePass()
    {
        if ($this->q_one_pass) {
            // if($this->q_1_ans == "no"){
            $this->question_stage = 2;
            // }
            // else{
            // $this->question_stage = "fail";
            // }
        }
        // else{
        // $this->question_stage = "fail";
        // }
    }

    public function updatedQTwoPass()
    {
        $this->question_stage = 'form';
    }

    public function updatedPropertyOptions()
    {
        $this->dispatchBrowserEvent('go-to-bottom');
        $val = $this->property_options;
        if ($val == 'go') {
            $this->q_one_des = true;
        } else {
            $this->q_one_des = true;
        }
    }

    public function activateUser()
    {
        $pfData = Request::all();

        foreach ($pfData as $key => $val) {
            $pfData[$key] = stripslashes($val);
        }

        $pfParamString = null;

        foreach ($pfData as $key => $val) {
            if ($key !== 'signature') {
                $pfParamString .= $key . '=' . urlencode($val) . '&';
            } else {
                break;
            }
        }
        $pfParamString = substr($pfParamString, 0, -1);
        $salt = env('PAYFAST_MERCHANT_SALT');

        $paymentAmount = null;
        if (session()->get('selectedPlan') == 'annual') {
            $paymentAmount = env('PAYFAST_SUBSCRIPTION_ANNUAL_AMOUNT');
        } else {
            $paymentAmount = env('PAYFAST_SUBSCRIPTION_MONTHLY_AMOUNT');
        }

        // $check1 = $this->pfValidSignature($pfData, $pfParamString, $salt);
        // $check2 = $this->pfValidIP();
        // $check3 = $this->pfValidPaymentData(env('PAYFAST_SUBSCRIPTION_AMOUNT'), $pfData);
        // $check3 = $this->pfValidPaymentData($paymentAmount, $pfData);
        // $check4 = $this->pfValidServerConfirmation($pfParamString, env('PAYFAST_VERIFY_DATA_URL'));

        //dd($check1, $check3, $check4);

        // if ($check1 && $check3) {
        if (true) {
            $user = User::find($this->user_id);
            $user->status = 1;
            $user->payment_date = date('Y-m-d');
            $user->free_account = 0;
            $user->save();
        } else {
            IlluminateLog::error('Payment Failed - User ID: ' . $this->user_id);
        }
    }

    public function pfValidSignature($pfData, $pfParamString, $pfPassphrase = null)
    {
        if ($pfPassphrase === null) {
            $tempParamString = $pfParamString;
        } else {
            $tempParamString = $pfParamString . '&passphrase=' . urlencode($pfPassphrase);
        }
        $signature = md5($tempParamString);
        return $pfData['signature'] === $signature;
    }

    public function pfValidIP()
    {
        $validHosts = ['www.payfast.co.za', 'sandbox.payfast.co.za', 'w1w.payfast.co.za', 'w2w.payfast.co.za'];

        $validIps = [];

        foreach ($validHosts as $pfHostname) {
            $ips = gethostbynamel($pfHostname);
            if ($ips !== false) {
                $validIps = array_merge($validIps, $ips);
            }
        }

        $validIps = array_unique($validIps);
        $referrerIp = gethostbyname(parse_url($_SERVER['HTTP_REFERER'])['host']);
        if (in_array($referrerIp, $validIps, true)) {
            return true;
        }
        return false;
    }

    public function pfValidPaymentData($cartTotal, $pfData)
    {
        return !(abs((float) $cartTotal - (float) $pfData['amount_gross']) > 0.01);
    }

    public function pfValidServerConfirmation($pfParamString, $pfHost = 'sandbox.payfast.co.za', $pfProxy = null)
    {
        if (in_array('curl', get_loaded_extensions(), true)) {
            $url = 'https://' . $pfHost . '/eng/query/validate';
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_USERAGENT, null);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);

            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $pfParamString);
            if (!empty($pfProxy)) {
                curl_setopt($ch, CURLOPT_PROXY, $pfProxy);
            }

            $response = curl_exec($ch);
            //dd($response);
            curl_close($ch);
            if ($response === 'VALID') {
                return true;
            }
        }
        return false;
    }

    public function setPaymentFields()
    {
        if ($this->user_id) {
            $user = User::find($this->user_id);
            if ($user) {
                $this->pf_url = env('PAYFAST_URL');

                $return_url = url('/register/payment-complete/' . $this->user_id);
                $cancel_url = url('/register/payment-canceled/' . $this->user_id);
                $notify_url = url('/register/process-payment/' . $this->user_id);

                // $payment_amount = env('PAYFAST_SUBSCRIPTION_AMOUNT');
                $selected_plan = session()->get('selectedPlan');
                $payment_amount = $selected_plan == 'annual' ? env('PAYFAST_SUBSCRIPTION_ANNUAL_AMOUNT') : env('PAYFAST_SUBSCRIPTION_MONTHLY_AMOUNT');

                $data = [
                    'merchant_id' => env('PAYFAST_MERCHANT_ID'),
                    'merchant_key' => env('PAYFAST_MERCHANT_KEY'),
                    'return_url' => $return_url,
                    'cancel_url' => $cancel_url,
                    'notify_url' => $notify_url,
                    'fica_idnumber' => $user->id_number,
                    'name_first' => $user->name,
                    'name_last' => $user->surname,
                    'email_address' => $user->email,
                    'm_payment_id' => 'MyAll-' . str_pad($user->id, 4, '0', STR_PAD_LEFT),
                    'amount' => number_format(sprintf('%.2f', $payment_amount), 2, '.', ''),
                    'item_name' => 'MyAll ' . ucfirst($selected_plan) . ' Subscription',
                    'subscription_type' => 1,
                    'frequency' => $selected_plan == 'annual' ? 6 : 3,
                    'cycles' => 0,
                    'subscription_notify_buyer' => true,
                ];
                $notify_email = env('PAYFAST_NOTIFICATION_EMAIL');
                if ($notify_email) {
                    $data['email_confirmation'] = 1;
                    $data['confirmation_address'] = $notify_email;
                }

                // $salt = env('PAYFAST_MERCHANT_SALT');
                // $signature = $this->generateSignature($data, $salt);
                // $data['signature'] = $signature;

                $this->form_data = $data;
            }
        }
    }

    function generateSignature($data, $passPhrase = null)
    {
        $pfOutput = '';
        foreach ($data as $key => $val) {
            if ($val !== '') {
                $pfOutput .= $key . '=' . urlencode(trim($val)) . '&';
            }
        }
        $getString = substr($pfOutput, 0, -1);
        if ($passPhrase !== null) {
            $getString .= '&passphrase=' . urlencode(trim($passPhrase));
        }
        return md5($getString);
    }

    public function saveUser()
    {
        $this->validate(
            [
                'name' => 'required',
                'surname' => 'required',
                'id_number' => 'nullable|required_without:passport_number|said',
                'passport_number' => 'required_without:id_number',
                'gender' => 'required',
                'ethnicity' => 'required',
                'date_of_birth' => 'required|date',
                'email' => 'required|email|unique:users',
                'password' => 'required|confirmed|string|min:15|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%^&*()_+=])/',
            ],
            [
                'password.regex' => 'The password must be at least 15 characters and include at least one uppercase letter, one lowercase, one digit, and one special character.',
            ],
        );

        $usr = User::create([
            'name' => $this->name,
            'surname' => $this->surname,
            'middle_name' => $this->middle_name,
            'date_of_birth' => $this->date_of_birth,
            'gender' => $this->gender,
            'ethnicity' => $this->ethnicity,
            'id_number' => $this->id_number,
            'passport_number' => $this->passport_number,
            'email' => $this->email,
            'password' => Hash::make($this->password),
        ]);
        if (!Session::has('invited')) {
            Interview::create([
                'user_id' => $usr->id,
                'have_property_outside_sa' => 'No',
                'read_and_write_capability' => 'Yes',
            ]);
        }

        $data = [
            'name' => $this->name,
            'surname' => $this->surname,
        ];
        $to = $this->email;

        $files = [public_path('docs/MyAll Privacy Policy.pdf'), public_path('docs/MyAll Terms and Conditions.pdf'), public_path('docs/MyAll PAIA Manual.pdf')];

        Mail::send('mail.welcome', $data, function ($message) use ($to, $files) {
            $message->to($to, '')->subject('Welcome to MyAll');
            $message->from('hello@myall.co.za', 'MyAll');
            foreach ($files as $file) {
                $message->attach($file);
            }
        });

        $content =
            "
        <p>A new user has signed up.</p>
        <table>
            <tr><td>Name: </td><td>" .
            $this->name .
            $this->surname .
            "</td></tr>
            <tr><td>Email: </td><td>" .
            $this->email .
            "</td></tr>
        </table>
        ";

        $notify_data = [
            'name' => 'MyAll Admin',
            'content' => $content,
        ];

        Mail::send('mail.notify', $notify_data, function ($message) use ($to, $files) {
            $message->to('hello@myall.co.za')->subject('New User Registered');
            $message->from('hello@myall.co.za', 'MyAll');
        });

        if (!Session::has('invited') && session()->get('selectedPlan') != 'free') {
            return redirect('register/payment/' . $usr->id);
        } else {
            $usr->free_account = 1;
            $usr->status = 1;
            $usr->save();
            return redirect('profile');
        }
    }

    public function render()
    {
        return view('livewire.auth.register')->extends('layouts.landing');
    }
}
