<?php

namespace App\Http\Livewire\Auth;

use Livewire\Component;

use App\Models\User;
use Illuminate\Auth\Events\PasswordReset;
use Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Str;
use Auth;
use ZxcvbnPhp\Zxcvbn;

class ResetPassword extends Component
{
    public $mail, $token, $password, $password_confirmation;
    public function mount(){
        $this->email = Request::input('email');
        $this->token = Request::input('token');
    }

    public $show_pass = false;
    public function showPass(){
        $this->show_pass = !$this->show_pass;
    }

    public $password_score, $password_warning;
    public $pass_bg;
    public function passwordMeter(){
        $this->pass_bg = "bg-poor-pass";
        $zxcvbn = new Zxcvbn();
        $strength = $zxcvbn->passwordStrength($this->password);
        $scrore = $strength['score'];
        $this->password_warning = $strength['feedback']['warning'];

        if($scrore == 4){
            if(strlen($this->password) < 15){
                $scrore = 3;
                $this->password_warning = "Password must be at least 15 characters";
            }
            elseif(!preg_match('/[A-Z]/', $this->password)){
                $scrore = 3;
                $this->password_warning = "Password must contain at least one uppercase letter";
            }
            elseif(!preg_match('/[a-z]/', $this->password)){
                $scrore = 3;
                $this->password_warning = "Password must contain at least one lowercase letter";
            }
            elseif(!preg_match('/[0-9]/', $this->password)){
                $scrore = 3;
                $this->password_warning = "Password must contain at least one number";
            }
            elseif(!preg_match('/[!@#$%^&*()_+=]/', $this->password)){
                $scrore = 3;
                $this->password_warning = "Password must contain at least one special character";    
            }
        }

        $this->password_score = ($scrore/4) * 100;
        if($scrore == 1){
            $this->pass_bg = "bg-poor-pass";
        }
        if($scrore == 2){
            $this->pass_bg = "bg-bad-pass";
        }
        if($scrore == 3){
            $this->pass_bg = "bg-good-pass";
        }
        if($scrore == 4){
            $this->pass_bg = "bg-excellent-pass";
        }
    }

    public function updatedPassword(){
        $this->passwordMeter();
    }
    
    public function updatePassword(){
        $this->validate([
            'email' => 'required',
            'password' => 'required|confirmed|string|min:15|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%^&*()_+=])[A-Za-z\d@#$!%*?&]+$/',
            'token' => 'required'
        ],[
            'password.regex' => 'The password must include at least one uppercase letter, one lowercase, one digit, and one special character.',
        ]);
        
        $status = Password::reset(
            ['email' => $this->email, 'password' => $this->password, 'password_confirmation' => $this->password_confirmation, 'token' => $this->token],
            function (User $user, string $password) {
                $user->forceFill([
                    'password' => Hash::make($password)
                ])->setRememberToken(Str::random(60));
                $user->save();
                event(new PasswordReset($user));
            }
        );
        if($status === Password::PASSWORD_RESET){
            $credentials = [
                'email' => $this->email,
                'password' => $this->password
            ];
            if(Auth::attempt($credentials)){
                return redirect()->intended('profile');
            }
        }
        else{
            if($status == "passwords.token"){
                $this->addError('status', "Invalid password reset token");
            }
            else{
                $this->addError('status', $status);
            }
        }
    }
    
    public function render(){
        return view('livewire.auth.reset-password')->extends('layouts.landing');
    }
}
