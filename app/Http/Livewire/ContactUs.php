<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Mail;

class ContactUs extends Component
{
    public $name, $surname, $contact_number, $email, $message, $accept_terms;

    public function clearFields()
    {
        $this->name = null;
        $this->surname = null;
        $this->contact_number = null;
        $this->email = null;
        $this->message = null;
    }

    public function submitContactForm()
    {
        $this->validate(
            [
                'name' => 'required',
                'surname' => 'required',
                'contact_number' => 'required',
                'email' => 'required|email',
                'message' => 'required',
                'accept_terms' => 'required',
            ],
            [
                'accept_terms.required' => 'You did not accept terms and conditions',
            ],
        );

        $data = [
            'name' => $this->name . ' ' . $this->surname,
            'contact_number' => $this->contact_number,
            'email' => $this->email,
            'message_content' => $this->message,
        ];

        $to = 'hello@myall.co.za';

        Mail::send('mail.contact', $data, function ($message) use ($to) {
            $message->to($to, '')->subject('MyAll Contact');
            $message->from('hello@myall.co.za', 'MyAll');
        });
        session()->flash('message', 'Submited successfully.');
        $this->clearFields();
    }
    public function render()
    {
        return view('livewire.contact-us');
    }
}
