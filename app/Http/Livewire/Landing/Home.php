<?php

namespace App\Http\Livewire\Landing;

use Livewire\Component;

use Laravel\Fortify\Fortify;
use Auth;
use Hash;

use Mail;

use App\Models\User;

use Illuminate\Http\Request;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Password;

class Home extends Component
{
    public $name, $surname, $date_of_birth, $id_number, $email, $password, $password_confirmation, $accept_terms;

    public function openMailingListModal()
    {
        $this->dispatchBrowserEvent('show-mailing-list-modal');
    }

    public function mount()
    {
    }

    public function loginUser()
    {
        $this->validate([
            'email' => 'required',
            'password' => 'required',
        ]);

        $credentials = [
            'email' => $this->email,
            'password' => $this->password,
        ];
        if (Auth::attempt($credentials)) {
            $usr = Auth::user();
            $date = false;
            $has_error = false;
            if ($usr->status != 1) {
                $this->addError('user', 'Your account is inactive.');
                $has_error = true;
            }

            if (!$usr->payment_date) {
                $date = true;
            } else {
                $paid_at = $usr->payment_date;
                $cur_date = date('Y-m-d');
                $date1 = \DateTime::createFromFormat('Y-m-d', $paid_at);
                $date2 = \DateTime::createFromFormat('Y-m-d', $cur_date);
                $interval = $date1->diff($date2);
                $diff = $interval->format('%m');
                if ($diff > 12) {
                    $date = true;
                }
            }

            if ($date) {
                Auth::logout();
                return redirect('register/payment/' . $usr->id);
            } elseif ($has_error) {
                Auth::logout();
                return redirect('login');
            }

            return redirect()->intended('profile');
        }
        $this->addError('user', 'Invalid email or password.');
    }

    public function registerUser()
    {
        $this->validate([
            'name' => 'required',
            'surname' => 'required',
            'date_of_birth' => 'required',
            'email' => 'required|unique:users',
            'password' => 'required|confirmed',
            'accept_terms' => 'required',
        ]);
        User::create([
            'name' => $this->name,
            'surname' => $this->surname,
            'date_of_birth' => $this->date_of_birth,
            'id_number' => $this->id_number,
            'email' => $this->email,
            'password' => Hash::make($this->password),
        ]);

        $data = [
            'name' => $this->name,
        ];
        $to = $this->email;

        Mail::send('mail.welcome', $data, function ($message) use ($to) {
            $message->to($to, '')->subject('Welcome to MyAll');
            $message->from('hello@myall.co.za', 'MyAll');
        });

        $this->loginUser();
    }

    public function authenticateUser($type)
    {
        if (!Auth::user()) {
            $this->dispatchBrowserEvent('show-modal', ['modalType' => $type]);
        } else {
            return redirect('profile');
        }
    }

    public function resetPass()
    {
        $user = User::where('email', $this->email)->first();
        $token = Password::getRepository()->create($user);
        $user->sendPasswordResetNotification($token);

        session()->flash('message', 'Email has been sent.');
        /*
        $credentials = ['email' => $this->email];
        $response = Password::sendResetLink($credentials, function (Message $message) {
            $message->subject($this->getEmailSubject());
        });
        */
    }

    public function showForgotPassword()
    {
        $this->dispatchBrowserEvent('show-modal', ['modalType' => 'forgotpass']);
    }

    public function render()
    {
        return view('livewire.landing.home')->extends('layouts.landing');
    }
}
