<?php

namespace App\Http\Livewire\Landing\Partials;

use Livewire\Component;
use Jenssegers\Agent\Agent;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Date;
use Illuminate\Http\RedirectResponse;
use Illuminate\Contracts\View\View;
use Illuminate\Contracts\View\Factory;
use Illuminate\Support\Facades\Log;
use App\Models\MailingList as MailingListModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail as Mail;

class Header extends Component
{
    public $email, $password;
    protected $listeners = ['show-login-modal' => 'showLoginModal'];
    public $show_password = false;
    private const STATUS_ACTIVE = 1;

    public $name, $contact_number, $mailing_email, $mailing_confirm;

    public function savemailingList()
    {
        $this->validate(
            [
                'name' => 'required',
                'contact_number' => 'required',
                'mailing_email' => 'required',
                'mailing_confirm' => 'required',
            ],
            [
                'mailing_confirm.required' => 'You did not accept terms and conditions',
            ],
        );

        MailingListModel::create([
            'name' => $this->name,
            'contact_number' => $this->contact_number,
            'email' => $this->mailing_email,
        ]);

        $data = [
            'name' => $this->name,
            'surname' => '',
        ];
        $to = $this->mailing_email;

        $files = [public_path('docs/MyAll Privacy Policy.pdf'), public_path('docs/MyAll Terms and Conditions.pdf'), public_path('docs/MyAll PAIA Manual.pdf')];

        Mail::send('mail.mailing-list', $data, function ($message) use ($to, $files) {
            $message->to($to, '')->subject('MyAll marketing subscription');
            $message->from('hello@myall.co.za', 'MyAll');
            foreach ($files as $file) {
                $message->attach($file);
            }
        });

        $content =
            "
        <p>U user has signed up for the mailing list</p>
        <table>
            <tr><td>Name: </td><td>" .
            $this->name .
            "</td></tr>
            <tr><td>Contact Number: </td><td>" .
            $this->contact_number .
            "</td></tr>
            <tr><td>Email: </td><td>" .
            $this->mailing_email .
            "</td></tr>
        </table>
        ";
        $data2 = [
            'name' => 'MyAll Admin',
            'content' => $content,
        ];
        Mail::send('mail.notify', $data2, function ($message) {
            $message->to('hello@myall.co.za', 'MyAll')->subject('MyAll marketing subscription');
            $message->from('hello@myall.co.za', 'MyAll');
        });
        session()->flash('message', 'Sumitted successfully.');
    }

    /**
     * Toggle the visibility of the password on the form.
     * @return void
     */
    public function togglePassword(): void
    {
        $this->show_password = !$this->show_password;
    }

    /**
     * Show the login modal if the user is not authenticated.
     * If the user is on a mobile device, redirect to the login page.
     * @return mixed
     */
    public function showLoginModal()
    {
        if (Auth::check()) {
            return redirect()->route('profile');
        }

        if ((new Agent())->isMobile()) {
            return redirect()->route('login');
        }

        $this->dispatchBrowserEvent('show-modal');
        return;
    }

    /**
     * Attempt to authenticate the user with the provided credentials.
     * @return mixed
     */
    public function loginUser()
    {
        $this->validate([
            'email' => 'required',
            'password' => 'required',
        ]);

        $credentials = $this->only(['email', 'password']);
        if (Auth::attempt($credentials)) {
            return $this->handleAuthenticatedUser();
        }

        $this->addError('user', 'Invalid email or password.');
        return null;
    }

    /**
     * Handle an authenticated user's status and subscription.
     * @return mixed
     */
    private function handleAuthenticatedUser()
    {
        $user = Auth::user();
        if ($user->status != self::STATUS_ACTIVE) {
            $this->addError('user', 'Your account is inactive.');
            Auth::logout();
            return redirect()->route('login');
        }

        if ($this->userRequiresPayment($user)) {
            Auth::logout();
            // return redirect()->route('register.payment', $user->id);
            // return redirect()->route('register');
            return redirect('register');
        }

        return redirect()->intended('profile');
    }

    /**
     * Check if the user requires a payment.
     * @param \App\User $user The user entity
     * @return bool
     */
    private function userRequiresPayment($user): bool
    {
        if ($user->free_account) {
            return false;
        }

        $paymentDate = Date::createFromFormat('Y-m-d', $user->payment_date ?? '');
        // Log::info('paymentDate: ' . $paymentDate->diffInMonths(now()) > 12);
        return $paymentDate->diffInMonths(now()) > 12;
    }

    /**
     * Render the component.
     * @return View|Factory
     */
    public function render(): View|Factory
    {
        return view('livewire.landing.partials.header');
    }
}
