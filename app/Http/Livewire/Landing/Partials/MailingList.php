<?php

namespace App\Http\Livewire\Landing\Partials;

use Livewire\Component;

use App\Models\MailingList as MailingListModel;
use Illuminate\Support\Facades\Mail as Mail;

class MailingList extends Component
{
    public $name, $contact_number, $email, $mailing_confirm;

    public function savemailingList()
    {
        $this->validate(
            [
                'name' => 'required',
                'contact_number' => 'required',
                'email' => 'required',
                'mailing_confirm' => 'required',
            ],
            [
                'mailing_confirm.required' => 'You did not accept terms and conditions',
            ],
        );

        MailingListModel::create([
            'name' => $this->name,
            'contact_number' => $this->contact_number,
            'email' => $this->email,
        ]);

        $data = [
            'name' => $this->name,
            'surname' => '',
        ];
        $to = $this->email;

        $files = [public_path('docs/MyAll Privacy Policy.pdf'), public_path('docs/MyAll Terms and Conditions.pdf'), public_path('docs/MyAll PAIA Manual.pdf')];

        Mail::send('mail.mailing-list', $data, function ($message) use ($to, $files) {
            $message->to($to, '')->subject('MyAll marketing subscription');
            $message->from('hello@myall.co.za', 'MyAll');
            foreach ($files as $file) {
                $message->attach($file);
            }
        });

        $content =
            "
        <p>U user has signed up for the mailing list</p>
        <table>
            <tr><td>Name: </td><td>" .
            $this->name .
            "</td></tr>
            <tr><td>Contact Number: </td><td>" .
            $this->contact_number .
            "</td></tr>
            <tr><td>Email: </td><td>" .
            $this->email .
            "</td></tr>
        </table>
        ";
        $data2 = [
            'name' => 'MyAll Admin',
            'content' => $content,
        ];
        Mail::send('mail.notify', $data2, function ($message) {
            $message->to('hello@myall.co.za', 'MyAll')->subject('MyAll marketing subscription');
            $message->from('hello@myall.co.za', 'MyAll');
        });
        session()->flash('message', 'Sumitted successfully.');
    }

    public function render()
    {
        return view('livewire.landing.partials.mailing-list');
    }
}
