<?php

namespace App\Http\Livewire\Landing\Partials;

use Livewire\Component;
use Session;

class Pricing extends Component
{
    public $selectedPlan;
    protected $listeners = ['setSessionPlan' => 'putPlanSession'];

    public function mount()
    {
        $this->selectedPlan = session()->get('selectedPlan', 'monthly');
    }

    public function putPlanSession($plan)
    {
        session()->put('selectedPlan', $plan);
        $this->selectedPlan = $plan;
    }

    public function render()
    {
        return view('livewire.landing.partials.pricing');
    }
}
