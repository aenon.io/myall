<?php

namespace App\Http\Livewire\Landing;

use Livewire\Component;
use Session;

class Pricing extends Component
{
    public function showRegister($type){
        if($type == "free"){
            Session::put('invited', true);
        }
        else{
            if(Session::has('invited')){
                Session::forget('invited');
            }
        }
        return redirect('register');
    }

    public function render(){
        return view('livewire.landing.pricing')->extends('layouts.landing');
    }
}
