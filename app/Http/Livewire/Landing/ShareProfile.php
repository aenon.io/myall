<?php

namespace App\Http\Livewire\Landing;

use Livewire\Component;

use Auth;
use Session;

use App\Models\Share;
use App\Models\User;
use App\Lib\FamilyTreeGen;
use App\Models\Family;

class ShareProfile extends Component
{
    public $email_hash, $user_id, $is_logged_in, $id_number;
    
    public function mount($id, $hash){
        $this->user_id = $id;
        $this->email_hash = $hash;
        
        $this->is_logged_in = false;
        $auth = Session::get('auth_token');
        if($auth){
            $this->is_logged_in = true;    
        }
    }
    
    public function verifyUser(){
        $this->validate([
            'id_number' => 'required'
        ]);
        $sh = Share::where('user_id', $this->user_id)->where('hash', $this->email_hash)->first();
        if($sh->id_number == $this->id_number){
            Session::put('auth_token', $sh->hash);
            $this->is_logged_in = true;
        }
        else{
            $this->addError('id_number', "Invalid ID number");
        }
        
    }
    
    public function render(){
        $sh = Share::where('user_id', $this->user_id)->where('hash', $this->email_hash)->first();
        if($sh){
            $user = User::find($sh->user_id);
        }
        else{
            $user = null;
        }
        
        $tree_gen = new FamilyTreeGen();
        $fam = Family::where('user_id', $this->user_id)->first();
        $tree = null;
        if($fam){
            $tree = json_encode($tree_gen->get_tree($fam->id));
        }
        
        return view('livewire.landing.share-profile', [
            'share' => $sh,
            'user' => $user,
            'tree' => $tree,
        ])->extends('layouts.landing');
    }
}
