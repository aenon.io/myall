<?php
namespace App\Lib;

use App\Models\Family;
use App\Models\User;

class FamilyTreeGen{
	public $tree = [], $grand_id;
	
	public function get_tree($id){
		$p = Family::find($id);
		
		$this->getGrandParents($id);
		$this->getPerson($this->grand_id);
		
		if($p->pids){
			$this->getGrandParents($p->pids);
			$this->getPerson($this->grand_id);
		}
		
		//dd($this->tree);
		return $this->tree;
	}

	public function getGrandParents($id, $pid = null){
		$person = Family::find($id);
		if($person){
			if($person->fid){
				$this->getGrandParents($person->fid);
			}
			elseif($person->mid){
				$this->getGrandParents($person->mid);
			}
			/*
			elseif($person->pids){
				dd($person->pids, $id);
				if($pid != $id){
					$this->getGrandParents($person->pids, $id);
				}
			}
			*/
			else{
				$this->grand_id = $person->id;
			}
		}
	}

	public function getPerson($id, $pid = null){
		$person = Family::find($id);
		if($person){
			$partners = Family::where('pids', $person->id)->get();
			$pids = [];
			foreach($partners AS $partner){
				$pids[] = $partner->id;
			} 
			
			$me_arr['id'] = $person->id;
			if(count($pids) > 0){
				$me_arr['pids'] = $pids;
			}
			if($person->mid){
				$me_arr['mid'] = $person->mid;
			}
			if($person->fid){
				$me_arr['fid'] = $person->fid;
			}
			$me_arr['name'] = $person->name;
			$me_arr['surname'] = $person->surname;
			$me_arr['dob'] = $person->dob;
			$me_arr['gender'] = $person->gender;
			$me_arr['relation'] = $person->relation;
			if($person->img){
				$me_arr['img'] = url('/storage/'.$person->img);
			}
			else{
				$me_arr['img'] = url("/img/family-tree/FAMILY TREE ICON.png");
			}
			if($person->color){
				$me_arr['tags'] = [$person->color];
			}

			$add_person = true;
			foreach($this->tree AS $tr){
				if($tr['id'] == $person->id){
					$add_person = false;
					break;
				}
			}
			if($add_person){
				$this->tree[] = $me_arr;
				//Get Partners
				if(count($pids) > 0){
					foreach($pids AS $p){
						if($id != $pid){
							$this->getPerson($p, $person->id);
						}
					}
				}

				//Get Children
				$children = Family::where('mid', $id)->orwhere('fid', $id)->get();
				foreach($children AS $child){
					$this->getPerson($child->id);
				}
			}
		}
		return false; 
	}
}
?>