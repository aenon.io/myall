<?php
namespace App\Lib;

use TCPDF;

class MyPDF extends TCPDF
{
    private $isLastPage = false;

    public function Close()
    {
        $this->isLastPage = true;
        parent::Close();
    }

    public function Header()
    {
        $this->SetFont('helvetica', '', 10);
        $this->Cell(0, 25, 'Page ' . $this->getAliasNumPage(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
    }

    public function isLastPage()
    {
        return $this->isLastPage;
    }

    public function Footer()
    {
        if (!$this->isLastPage()) {
            $this->SetY(-20);
            // $imagePath = 'img/will_footer.png';
            // $this->Image($imagePath, -10, 268, 0, 20);
        } else {
            $footerHTML = '
            <p><u>AS WITNESS:</u></p>
            <table class="w-100" style="border-spacing: 0 20px;">
                <tr>
                    <td>
                        <b>1:</b>......................................................
                    </td>
                    <td>......................................................<br /><b>TESTATOR</b></td>
                </tr>
                <tr>
                    <td>
                        <b>2:</b>......................................................
                    </td>
                    <td></td>
                </tr>
            </table>';
            $this->SetY(-65);
            $this->writeHTML($footerHTML, true, false, false, false, '');

            // $imagePath = 'img/will_footer.png';
            // $this->Image($imagePath, 1, 273, 0, 18);
        }
    }
}
?>
