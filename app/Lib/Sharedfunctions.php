<?php
namespace App\Lib;

use Auth;
use App\Models\WillOption;

use App\Models\Interview;
use App\Models\Review;
use App\Models\Confirmation;

class Sharedfunctions{
	public function getNextWillPage($cur_page){
		$user_opts = WillOption::where('user_id', Auth::user()->id)->first();
		$all_options = [
			'profile', 
			'executor', 
			'spouse', 
			'minor-children-beneficiary',
			'guardian',
			'testamentary-trust', 
			'inheritance', 
			'specific-inheritance', 
			'remainder-inheritance', 
			'simultaneous-death', 
			'funeral-wishes', 
			'witnesses'
		];

		$key = array_search ($cur_page, $all_options);
		$key++;
		for($key; $key < count($all_options); $key++){
			$op = $all_options[$key];
			$tbl_name = str_replace("-", "_", $op);
			if($user_opts->$tbl_name == 1){
				return $op;
			}
		}
	}

	public function isSaIdNumber($value){
		$validated = false;

        if (is_numeric($value) && strlen($value) === 13){
            $errors = false;
            $num_array = str_split($value);
            
            $id_month = $num_array[2] . $num_array[3];
            $id_day = $num_array[4] . $num_array[5];
            if ( $id_month < 1 || $id_month > 12) {
                $errors = true;
            }
            if ( $id_day < 1 || $id_day > 31) {
                $errors = true;
            }
            /**********************************
            Check Digit Verification
            **********************************/
            // Declare the arrays
            $even_digits = array();
            $odd_digits = array();

            // Loop through modified $num_array, storing the keys and their values in the above arrays
            foreach ( $num_array as $index => $digit){
                if ($index === 0 || $index % 2 === 0){
                    $odd_digits[] = $digit; 
                }
                else {
                    $even_digits[] = $digit;
                }
            }

            // use array pop to remove the last digit from $odd_digits and store it in $check_digit
            $check_digit = array_pop($odd_digits);

            //All digits in odd positions (excluding the check digit) must be added together.
            $added_odds = array_sum($odd_digits);

            //All digits in even positions must be concatenated to form a 6 digit number.
            $concatenated_evens = implode('', $even_digits);

            //This 6 digit number must then be multiplied by 2.
            $evensx2 = $concatenated_evens * 2;

            // Add all the numbers produced from the even numbers x 2
            $added_evens = array_sum( str_split($evensx2) );

            $sum = $added_odds + $added_evens;

            // get the last digit of the $sum
            $last_digit = substr($sum, -1);

            /* 10 - $last_digit
            * $verify_check_digit = 10 - (int)$last_digit; (Will break if $last_digit = 0)
            * Edit suggested by Ruan Luies
            * verify check digit is the resulting remainder of
            *  10 minus the last digit divided by 10
            */
            $verify_check_digit = (10 - (int)$last_digit) % 10;

            // test expected last digit against the last digit in $id_number submitted
            if ((int)$verify_check_digit !== (int)$check_digit) {
                $errors = true;
            }

            if (!$errors) {
                $validated = true;
            }
        }
        return $validated;
	}
	
	public function getProgress(){
		$stage = 0;
		$int = Interview::where('user_id', Auth::user()->id)->first();
		if($int){
			$stage = 1;
			$st_2 = $this->checkStage2();
			if($st_2['status'] == true){
				$stage = 2;
				$review = Review::where('user_id', Auth::user()->id)->first();
				if($review){
					$stage = 3;

					$cnf = Confirmation::where('user_id', Auth::user()->id)->first();
					if($cnf){
						if($cnf->step_4){
							$stage = 4;
							if($cnf->step_5){
								$stage = 5;
								if($cnf->step_6){
									$stage = 6;
									if($cnf->step_7){
										$stage = 7;
									}
								}
							}
						}
					}
				}
			}
		}
		//if($stage >= 1){
		//	$stage++;
		//}
		
		$ret = [
			'stage' => round(($stage / 7) * 100),
			'stage_val' => $stage
		];
		//dd($ret);
		return $ret;
	}

	public function checkStage2(){
		$opts = WillOption::where('user_id', Auth::user()->id)->first();
		$user = Auth::user();
		if(!$opts){
			$ret = [
				'status' => false,
				'stage' => "options"
			];
			return $ret;
		}
		else{
			$opts_arr = $opts->toArray();
			foreach($opts_arr AS $k=>$v){
				if($k != "id" && $k != "created_at" && $k != "updated_at"){
					if($v){
						if($k == "executor"){
							if($user->executors->count() == 0){
								$ret = [
									'status' => true,
									'stage' => "executor"
								];
								return $ret;
							}
						}
						if($k == "spouse"){
							if($user->spouses->count() == 0){
								$ret = [
									'status' => true,
									'stage' => "spouse"
								];
								return $ret;
							}
						}
						if($k == "minor_children_beneficiary"){
							if($user->children->count() == 0){
								$ret = [
									'status' => true,
									'stage' => "minor_children_beneficiary"
								];
								return $ret;
							}
						}
						if($k == "guardian"){
							if($user->guardians->count() == 0){
								$ret = [
									'status' => true,
									'stage' => "guardian"
								];
								return $ret;
							}
						}
						if($k == "testamentary_trust"){
							if($user->testamentary_trust->count() == 0){
								$ret = [
									'status' => true,
									'stage' => "testamentary_trust"
								];
								return $ret;
							}
						}
						if($k == "inheritance"){
							if($user->regular_inheritance()->count() == 0){
								$ret = [
									'status' => true,
									'stage' => "inheritance"
								];
								return $ret;
							}
						}
						if($k == "specific_inheritance"){
							if($user->specific_inheritance()->count() == 0){
								$ret = [
									'status' => true,
									'stage' => "specific_inheritance"
								];
								return $ret;
							}
						}
						if($k == "remainder_inheritance"){
							if($user->remainder_inheritance()->count() == 0){
								$ret = [
									'status' => true,
									'stage' => "remainder_inheritance"
								];
								return $ret;
							}
						}
						if($k == "simultaneous_death"){
							if($user->simulteneous_death_inheritance()->count() == 0){
								$ret = [
									'status' => true,
									'stage' => "simultaneous_death"
								];
								return $ret;
							}
						}
						if($k == "funeral_wishes"){
							if(!$user->funeral_wishes){
								$ret = [
									'status' => true,
									'stage' => "funeral_wishes"
								];
								return $ret;
							}
						}
						if($k == "witnesses"){
							if($user->witnesses->count() == 0){
								$ret = [
									'status' => true,
									'stage' => "witnesses"
								];
								return $ret;
							}
						}
					}
				}
			}
			$ret = [
				'status' => true,
				'stage' => 7
			];
			return $ret;
		}
	}
}
?>