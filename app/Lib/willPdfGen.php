<?php
namespace App\Lib;

use App\Models\User;

use App\Lib\MyPDF;

class willPdfGen
{
    public $user;

    function __construct($user_id = 1)
    {
        $this->user = User::find($user_id);
    }

    public function getPdf()
    {
        $html = $this->genPdfHtml();

        $pdf = new MyPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('MyAll');
        $pdf->SetTitle('MyAll Will');

        $pdf->SetMargins(20, 20, 20);
        $pdf->SetAutoPageBreak(true, 68);

        $pdf->AddPage();
        $pdf->writeHTML($html, true, false, true, false, '');

        $pdfContent = $pdf->Output('', 'S');
        return $pdfContent;
    }

    public function genPdfHtml()
    {
        $user = $this->user;
        $html = view('pdf.will2', compact('user'))->render();
        return $html;
    }
}
?>

