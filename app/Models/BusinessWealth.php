<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BusinessWealth extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'business_type', 
        'business_name', 
        'registration_number', 
        'company_url',
        'vat_registered', 
        'vat_number',
        'masters_office', 
        'trust_type', 
        'specific_trust_type', 
        'possessions',
        'buy_sell_agreement', 
        'value_min', 
        'value_max', 
        'equipment', 
        'outstanding_debtors', 
        'goodwill',
        'net_value', 
        'credit', 
        'debt',
        'auditor_name', 
        'auditor_surname', 
        'auditor_tel', 
        'auditor_email', 
        'auditor_address',
        'founding_statement',
        'amended_founding_statement',
        'members_resolution',
        'certificate_of_registration',
        'sars_issued_document',
        'letter_from_auditors',
        'directors_resolution',
        'trust_deed',
        'letter_of_authority',
        'resolution_by_trustee'
    ];

    public function debtors(){
        return $this->hasMany(BusinessWealthDebtor::class);
    }

    public function creditors(){
        return $this->hasMany(BusinessWealthCreditor::class);
    }

    public function shareholders(){
        return $this->hasMany(BusinessWealthMember::class);
    }

    public function trustees(){
        return $this->hasMany(BusinessWealthTrustee::class);
    }
    
    public function my_possessions(){
        return $this->hasMany(Possession::class, 'business_wealth_id');
    }
}
