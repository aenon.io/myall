<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BusinessWealthCreditor extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'business_wealth_id',
        'credit_to',
        'credit_value',
        'credit_insured'
    ];
}
