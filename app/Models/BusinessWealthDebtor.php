<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BusinessWealthDebtor extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'business_wealth_id',
        'debt_to',
        'debt_value',
        'debt_insured',
    ];
}
