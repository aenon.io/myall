<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BusinessWealthMember extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'business_wealth_id',
        'member_name',
        'member_surname',
        'stake_percentage'
    ];
}
