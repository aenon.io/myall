<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BusinessWealthTrustee extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'business_wealth_id',
        'trustee_first_name',
        'trustee_surname',
        'trustee_contact'
    ];
}
