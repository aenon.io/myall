<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Child extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'child_id_number',
        'child_name',
        'middle_name',
        'child_surname',
        'child_date_of_birth',
        'child_birth_certificate',
        'child_adoption_certificate'
    ];

    public function guardian(){
        return $this->hasOne(ChildGuardian::class);
    }
}
