<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ChildGuardian extends Model
{
    use HasFactory;

    protected $fillable = [
        'child_id',
        'guardian_id'
    ];

    public function guardian(){
        return $this->belongsTo(Guardian::class);
    }
}
