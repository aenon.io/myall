<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'name',
        'surname',
        'relation',
        'specify_relation',
        'email',
        'contact_number',
        'contact_type',
        'aa_company_name',
        'aa_contact_details',
        'aa_contact_name',
        'aa_contact_surname',
        'aa_contact_email',
        'aa_contact_number'
    ];
}
