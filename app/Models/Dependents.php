<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Dependents extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'medical_aid_id',
        'funeral_cover_id',
        'user_id',
        'name',
        'surname',
        'id_number'
    ];
}
