<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Executor extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'exec_type',
        'first_name',
        'middle_name',
        'surname',
        'id_number',
        'relation',
        'specify_relation'
    ];
}
