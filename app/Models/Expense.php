<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Expense extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'type', 
        'description', 
        'amount',
        'rate',
        'term',
        'lender',
        'registered_lender',
        'family_lender',
        'payment_date',
        'document'
    ];
}
