<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Family extends Model
{
    use HasFactory;

    protected $table = 'families';

    protected $fillable = [
        'user_id',
        'name',
        'surname',
        'dob',
        'dod',
        'gender',
        'id_number',
        'email',
        'relation',
        'pids',
        'mid',
        'fid',
        'show_on_tree',
        'color',
        'img',
    ];
}
