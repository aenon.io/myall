<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Funeral extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'user_id',
        'policy_details',
        'memorials_service',
        'cremation_details',
        'parlor_details',
        'requests',
        'obituary'
    ];
}
