<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FuneralCover extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'user_id',
        'policy_number',
        'policy_holder',
        'company',
        'coverage_amount'
    ];

    public function dependants(){
        return $this->hasMany(Dependents::class);
    }
}
