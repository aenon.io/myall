<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Funeralwish extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'organ_donor',
        'cremated',
        'special_wishes',
        'berried'
    ];
}
