<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class IndividualBeneficiary extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'inheritance_id',
        'first_name',
        'middle_name',
        'surname',
        'id_number',
        'relation',
        'specify_relation',
        'description',
        'value',
        'percentage'
    ];
}
