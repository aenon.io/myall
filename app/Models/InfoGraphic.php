<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InfoGraphic extends Model
{
    use HasFactory;

    protected $fillable  = [
        "title",
        "stage",
        "path",
        'type',
    ];

    public function infoKey(){
        return $this->hasMany(InfographicKeyword::class);
    }
}
