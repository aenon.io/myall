<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InfographicKeyword extends Model
{
    use HasFactory;

    protected $fillable = [
        "info_graphic_id",
        "key_word_id"
    ];

    public function graphic(){
        return $this->belongsTo(InfoGraphic::class);
    }

    public function word(){
        return $this->belongsTo(KeyWord::class, 'key_word_id');
    }
}
