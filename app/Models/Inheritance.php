<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Inheritance extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'type',
        'sub_type',
        'division_type'
    ];

    public function individual_beneficiary(){
        return $this->hasMany(IndividualBeneficiary::class);
    }

    public function organisation_beneficiery(){
        return $this->hasMany(OrganisationBeneficiary::class);
    } 
}
