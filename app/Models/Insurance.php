<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Insurance extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'insurance_type', 
        'insurer', 
        'policy_number', 
        'contact', 
        'financial_advisor', 
        'outstanding_debt',
        'file',
        'payment_date',
        'payment_terms'
    ];
}
