<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Interview extends Model
{
    use HasFactory;

    protected $fillable = ['user_id', 'have_property_outside_sa', 'read_and_write_capability', 'leave_possesions_to_icapitated_persons', 'exclude_persons_from_will'];
}
