<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Investment extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'investment_type', 
        'value', 
        'file',
        'description',
        'policy_number'
    ];

    public function beneficiaries(){
        return $this->hasMany(InvestmentBeneficiaries::class);
    }
}
