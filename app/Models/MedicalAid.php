<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MedicalAid extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'reference_number',
        'company',
        'medical_aid_certificate',
        'id_number',
        'surname',
        'name',
        'conditions',
        'policy_type'
    ];

    public function dependants(){
        return $this->hasMany(Dependents::class);
    }

    public function sub_conditions(){
        return $this->hasMany(MedicalAidCondition::class);
    }
}
