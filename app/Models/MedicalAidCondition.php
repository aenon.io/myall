<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MedicalAidCondition extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'medical_aid_id',
        'condition'
    ];
}
