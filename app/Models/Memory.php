<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Memory extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'user_id',
        'media_type',
        'memory_type',
        'notes',
        'recipient',
        'media',
        'link',
        
        'name',
        'surname',
        'email',
        'id_number',
        'phone_number'
    ];
    
    public function questionnaire()
    {
        return $this->hasMany(Questionniaire::class, 'memory_id');
    }
}
