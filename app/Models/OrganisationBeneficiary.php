<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrganisationBeneficiary extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'inheritance_id',
        'organisation_name',
        'registration_number',
        'address',
        'city',
        'postal_code',
        'province',
        'organisation_type',
        'organisation_other_type',
        'description',
        'value',
        'percentage'
    ];
}
