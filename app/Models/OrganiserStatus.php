<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrganiserStatus extends Model
{
    protected $table = 'organiser_progress';
    public $timestamps = false;
}
