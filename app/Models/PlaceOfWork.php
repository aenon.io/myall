<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PlaceOfWork extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'employer_name',
        'employer_address',
        'salary_number'
    ];
}
