<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Possession extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'user_id',
        'business_wealth_id',
        'possessions',
        'specify_possession',
        'value'
    ];
}
