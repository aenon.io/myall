<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Questionniaire extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'memory_id',
        'favorite_song', 
        'favorite_movie', 
        'favorite_place', 
        'favorite_food', 
        'special_memory', 
        'biggest_lesson', 
        'regret', 
        'religion_beliefs', 
        'knew_earlier', 
        'thank_someone', 
        'happy_thing', 
        'sad_thing', 
        'best_quality', 
        'bucket_list', 
        'advice', 
        'proud_of', 
        'remember_me_by', 
        'funny_memory',
        'best_part'
    ];
}
