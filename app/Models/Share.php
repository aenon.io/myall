<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Share extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'contact_id',
        'name', 
        'email',
        'contact_number',
        'id_number', 
        'dob', 
        'family', 
        'friends', 
        'professional', 
        'specify_relation', 
        'emergency_contact',
        'color',
        'order_num',
    ];

    public function shareOptions(){
        return $this->hasMany(ShareOption::class);
    }

    public function activeShare(){
        return $this->shareOptions()->orderBy('created_at', 'DESC')->first();
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function contact(){
        return $this->belongsTo(User::class, 'contact_id');
    }
}
