<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Share extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'user_id',
        'name',
        'email',
        'id_number',
        'hash',
        
        'profile',
        'organiser',
        'personal',
        'spouse',
        'work',
        'tax',
        'children',
        'what_to_find',
        'medical_aid',
        'connections',
        'family',
        'personal_connections',
        'business',
        'doctors',
        'emergency',
        'accountant_bookkeeper',
        'private_banker',
        'lawyer',
        'business_wealth',
        'properties_wealth',
        'debt_expenses',
        'social_media',
        'safe_keeping',
        'memories',
        'funeral',
        'family_tree',
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }
}
