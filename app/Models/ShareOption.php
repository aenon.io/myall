<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ShareOption extends Model
{
    use HasFactory;

    protected $fillable = ['share_id', 'profile', 'organiser', 'personal', 'spouse', 'work', 'tax', 'children', 'what_to_find', 'medical_aid', 'connections', 'family', 'personal_connections', 'business', 'doctors', 'emergency', 'accountant_bookkeeper', 'private_banker', 'lawyer', 'business_wealth', 'properties_wealth', 'debt_expenses', 'social_media', 'safe_keeping', 'memories', 'funeral', 'family_tree', 'education', 'funeral_cover', 'friends', 'assets', 'investments_and_policies', 'bank_accounts', 'expenses', 'loans', 'insurance', 'other_expenses', 'income_tax'];
}
