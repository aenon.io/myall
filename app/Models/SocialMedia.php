<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SocialMedia extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'network', 
        'specific_network', 
        'user_name', 
        'link',
        'legacy_contact', 
        'instructions'
    ];
}
