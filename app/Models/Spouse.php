<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Spouse extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'marital_status',
        'spouse_name',
        'spouse_middle_name',
        'spouse_surname', 
        'spouse_id_number', 
        'marriage_certificate', 
        'antenuptial_contract', 
        'date_of_death', 
        'masters_office', 
        'estate_number', 
        'attorney_auditor_trust_contact_details', 
        'estate_wrapping_up_company', 
        'dweling_property', 
        'usufruct',
        'death_certificate', 
        'will', 
        'date_of_divorce', 
        'court_order_and_agreement', 
        'maintenance_order', 
        'proof_of_maintenance_order', 
        'specific_agreements_description', 
        'specific_agreements_file', 
        'comunity_marriage', 
        'accrual',
        'executor',
        'divorce_agreement',
        'domestic_agreement'
    ];
}
