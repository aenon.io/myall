<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Trust extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'user_id',
        'trust_type',
    ];
    
    public function trustees(){
        return $this->hasMany(Trustee::class);
    }
    
    public function beneficiaries(){
        return $this->hasMany(TrustBeneficiary::class);
    }
}
