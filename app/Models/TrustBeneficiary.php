<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TrustBeneficiary extends Model
{
    use HasFactory;
    
    protected $fillable = [
        "trust_id",
        "user_id",
        "name",
        "middle_name",
        "surname"
    ];
}
