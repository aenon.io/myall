<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TrustedContact extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'name', 
        'surname', 
        'nickname', 
        'id_number', 
        'dob', 
        'address', 
        'other_info', 
        'additional_info', 
        'terms_and_conditions'
    ];
}
