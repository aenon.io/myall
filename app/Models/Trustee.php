<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Trustee extends Model
{
    use HasFactory;

    public $fillable = [
        'trust_id',
        'user_id',
        'first_name',
        'middle_name',
        'surname',
        'id_number'
    ];
}
