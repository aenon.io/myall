<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = ['name', 'surname', 'date_of_birth', 'id_number', 'passport_number', 'contact_number', 'gender', 'ethnicity', 'occupation', 'bio', 'email', 'password', 'profile_pic', 'street_address', 'birth_certificate', 'id_document', 'passport_document', 'drivers_license_document', 'power_of_attorney_document', 'separate_power_of_attorney_document', 'middle_name', 'city', 'postal_code', 'province', 'instructions', 'welcome_modal'];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function contacts()
    {
        return $this->hasMany(Contact::class);
    }

    public function groupedContacts()
    {
        return $this->hasMany(Contact::class)
            ->get()
            ->groupBy('contact_type');
    }

    public function contactsByType($type)
    {
        $other = [];
        if ($type == 'contact') {
            $other = ['family', 'personal'];
        } else {
            $other = ['business'];
        }

        if ($type == 'family') {
            return $this->contacts()->whereIn('contact_type', ['family']);
        } elseif ($type == 'professionals') {
            return $this->contacts()->whereIn('contact_type', ['business', 'doctor', 'accountant', 'banker', 'lawyer', 'banks_attorneys']);
        } elseif ($type == 'friend') {
            return $this->contacts()->whereIn('contact_type', ['personal']);
        } elseif ($type == 'other') {
            return $this->contacts()->whereIn('contact_type', ['other']);
        } else {
            return $this->contacts()->where('contact_type', $type);
        }
    }

    public function will_menu_options()
    {
        return $this->hasOne(WillOption::class);
    }

    public function will_spouses()
    {
        return $this->hasMany(willSpouse::class);
    }

    public function executors()
    {
        return $this->hasMany(Executor::class);
    }

    public function executor()
    {
        return $this->executors()->where('exec_type', 'main_exec')->first();
    }

    public function sub_executor()
    {
        return $this->executors()->where('exec_type', 'sub_exec')->first();
    }

    public function children()
    {
        return $this->hasMany(Child::class);
    }

    public function minorChildren()
    {
        $under_eighteen = date('Y-m-d', strtotime('-18 years'));
        //dd($under_eighteen);
        return $this->children()->whereDate('child_date_of_birth', '<', $under_eighteen)->get();
    }

    public function minorCount()
    {
        $under_eighteen = date('Y-m-d', strtotime('-18 years'));
        //dd($under_eighteen);
        return $this->children()->whereDate('child_date_of_birth', '>', $under_eighteen)->count();
    }

    public function guardians()
    {
        return $this->hasMany(Guardian::class);
    }

    public function inheritance()
    {
        return $this->hasMany(Inheritance::class);
    }

    public function specific_inheritance()
    {
        return $this->inheritance()->where('type', 'specific-inheritance')->whereNull('sub_type')->get();
    }

    public function remainder_inheritance()
    {
        return $this->inheritance()->where('type', 'remainder-inheritance')->whereNull('sub_type')->get();
    }

    public function regular_inheritance()
    {
        return $this->inheritance()->where('type', 'Inheritance')->whereNull('sub_type')->get();
    }

    public function simulteneous_death_inheritance()
    {
        return $this->inheritance()->where('type', 'simultaneous-death')->get();
    }

    public function testamentary_trust()
    {
        return $this->hasMany(Trustee::class);
    }

    public function funeral_wishes()
    {
        return $this->hasOne(Funeralwish::class);
    }

    public function witnesses()
    {
        return $this->hasMany(Witness::class);
    }

    public function spouses()
    {
        return $this->hasMany(Spouse::class);
    }

    public function work()
    {
        return $this->hasOne(PlaceOfWork::class);
    }

    public function tax()
    {
        return $this->hasOne(Tax::class);
    }

    public function itemToFind()
    {
        return $this->hasMany(ItemToFind::class);
    }

    public function medicalAids()
    {
        return $this->hasMany(MedicalAid::class);
    }

    public function connections()
    {
        return $this->hasMany(Contact::class);
    }

    public function connectionsByType($type)
    {
        if ($type == 'doctors') {
            return $this->connections()->where('contact_type', 'doctor')->get();
        } elseif ($type == 'accountant_bookkeeper') {
            return $this->connections()->where('contact_type', 'accountant')->get();
        } elseif ($type == 'private_banker') {
            return $this->connections()->where('contact_type', 'banker')->get();
        } else {
            return $this->connections()->where('contact_type', $type)->get();
        }
    }

    public function businessWealths()
    {
        return $this->hasMany(BusinessWealth::class);
    }

    public function assets()
    {
        return $this->hasMany(Asset::class);
    }

    public function investments()
    {
        return $this->hasMany(Investment::class);
    }

    public function banks()
    {
        return $this->hasMany(Bank::class);
    }

    public function insurances()
    {
        return $this->hasMany(Insurance::class);
    }

    public function social_medias()
    {
        return $this->hasMany(SocialMedia::class);
    }

    public function groupedSocialMedia()
    {
        return $this->social_medias()->get()->groupBy('network');
    }

    public function safe_keep()
    {
        return $this->hasMany(SafeKeep::class);
    }

    public function funeral()
    {
        return $this->HasOne(Funeral::class);
    }

    public function funeralCovers()
    {
        return $this->hasMany(FuneralCover::class);
    }

    public function education()
    {
        return $this->hasMany(Education::class);
    }

    public function expenses()
    {
        return $this->hasMany(Expense::class);
    }

    public function memories()
    {
        return $this->hasMany(Memory::class);
    }

    public function questionniare()
    {
        return $this->hasOne(Questionniaire::class);
    }

    public function willConfirmation()
    {
        return $this->hasOne(Confirmation::class);
    }

    public function interview()
    {
        return $this->hasOne(Interview::class);
    }

    public function ind_beneficiaries()
    {
        return $this->hasMany(IndividualBeneficiary::class);
    }

    public function org_beneficiaries()
    {
        return $this->hasMany(OrganisationBeneficiary::class);
    }

    public function trust_beneficiaries()
    {
        return $this->hasMany(TrustBeneficiary::class);
    }

    public function trusts()
    {
        return $this->hasMany(Trust::class);
    }

    public function shareContacts()
    {
        return $this->hasMany(Share::class);
    }

    public function shareContactsByType($type)
    {
        return $this->shareContacts()->whereNotNull($type)->where($type, '<>', '')->orderBy('order_num', 'ASC')->get();
    }
}
