<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserEvent extends Model
{
    protected $table = 'events_all';
    public $timestamps = false;

    public function contact($share_id)
    {
        return User::find($share_id);
    }
}
