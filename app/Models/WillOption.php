<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WillOption extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'profile',
        'executor',
        'spouse',
        'minor_children_beneficiary',
        'guardian',
        'testamentary_trust',
        'inheritance',
        'specific_inheritance',
        'remainder_inheritance',
        'simultaneous_death',
        'funeral_wishes',
        'witnesses'
    ];

}
