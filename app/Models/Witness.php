<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Lib\Sharedfunctions;

class Witness extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'first_name',
        'middle_name',
        'surname',
        'id_number',
        'phone_number',
        'address',
        'city',
        'postal_code',
        'province'
    ];
    
    public function idType(){
        $fnc = new Sharedfunctions();
        if($fnc->isSaIdNumber($this->id_number)){
            return "South African Identity Number";
        }
        else{
            return 'Passport Number';
        }
    }
}
