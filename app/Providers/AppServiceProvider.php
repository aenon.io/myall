<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        Validator::extend('said', function($attribute, $value, $parameters, $validator){
            $validated = false;

            if (is_numeric($value) && strlen($value) === 13){
                $errors = false;
                $num_array = str_split($value);
                
                $id_month = $num_array[2] . $num_array[3];
                $id_day = $num_array[4] . $num_array[5];
                if ( $id_month < 1 || $id_month > 12) {
                    $errors = true;
                }
                if ( $id_day < 1 || $id_day > 31) {
                    $errors = true;
                }
                /**********************************
                Check Digit Verification
                **********************************/
                // Declare the arrays
                $even_digits = array();
                $odd_digits = array();

                // Loop through modified $num_array, storing the keys and their values in the above arrays
                foreach ( $num_array as $index => $digit){
                    if ($index === 0 || $index % 2 === 0){
                        $odd_digits[] = $digit; 
                    }
                    else {
                        $even_digits[] = $digit;
                    }
                }

                // use array pop to remove the last digit from $odd_digits and store it in $check_digit
                $check_digit = array_pop($odd_digits);

                //All digits in odd positions (excluding the check digit) must be added together.
                $added_odds = array_sum($odd_digits);

                //All digits in even positions must be concatenated to form a 6 digit number.
                $concatenated_evens = implode('', $even_digits);

                //This 6 digit number must then be multiplied by 2.
                $evensx2 = $concatenated_evens * 2;

                // Add all the numbers produced from the even numbers x 2
                $added_evens = array_sum( str_split($evensx2) );

                $sum = $added_odds + $added_evens;

                // get the last digit of the $sum
                $last_digit = substr($sum, -1);

                /* 10 - $last_digit
                * $verify_check_digit = 10 - (int)$last_digit; (Will break if $last_digit = 0)
                * Edit suggested by Ruan Luies
                * verify check digit is the resulting remainder of
                *  10 minus the last digit divided by 10
                */
                $verify_check_digit = (10 - (int)$last_digit) % 10;

                // test expected last digit against the last digit in $id_number submitted
                if ((int)$verify_check_digit !== (int)$check_digit) {
                    $errors = true;
                }

                if (!$errors) {
                    $validated = true;
                }
            }
            return $validated;
        });
    }
}
