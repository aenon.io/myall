<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();

            $table->string('name');
            $table->string('middle_name')->nullable();
            $table->string('surname');
            $table->date('date_of_birth')->nullable();
            $table->string('id_number')->nullable();
            $table->string('contact_number')->nullable();
            $table->string('occupation')->nullable();
            $table->text('bio')->nullable();

            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('profile_pic')->nullable();

            $table->string('street_address')->nullable(); 
            $table->string('birth_certificate')->nullable(); 
            $table->string('id_document')->nullable();
            $table->string('passport_document')->nullable(); 
            $table->string('drivers_license_document')->nullable(); 
            $table->string('power_of_attorney_document')->nullable(); 
            $table->string('separate_power_of_attorney_document')->nullable();
            
            $table->string('city')->nullable();
            $table->string('postal_code')->nullable();
            $table->string('province')->nullable();
            $table->string('instructions')->nullable();

            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('users');
    }
};
