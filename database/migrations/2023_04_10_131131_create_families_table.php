<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('families', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id')->nullable();
            $table->string('name');
            $table->string('surname');
            $table->date('dob');
            $table->date('dod')->nullable();
            $table->string('gender')->nullable();
            $table->string('id_number')->nullable();
            $table->string('email')->nullable();
            $table->string('relation')->nullable();
            $table->integer('pids')->nullable();
            $table->integer('mid')->nullable();
            $table->integer('fid')->nullable();
            $table->integer('show_on_tree')->nullable();
            $table->string('color')->nullable();
            $table->string('img')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('families');
    }
};
