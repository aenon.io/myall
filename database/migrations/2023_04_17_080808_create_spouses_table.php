<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('spouses', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->string('marital_status');
            $table->string('spouse_name')->nullable();
            $table->string('spouse_middle_name')->nullable();
            $table->string('spouse_surname')->nullable(); 
            $table->string('spouse_id_number')->nullable(); 
            $table->string('marriage_certificate')->nullable(); 
            $table->string('antenuptial_contract')->nullable();
            $table->string('date_of_death')->nullable();
            $table->string('masters_office')->nullable(); 
            $table->string('estate_number')->nullable();
            $table->string('attorney_auditor_trust_contact_details')->nullable(); 
            $table->string('estate_wrapping_up_company')->nullable();
            $table->string('dweling_property')->nullable();
            $table->string('death_certificate')->nullable(); 
            $table->string('will')->nullable();
            $table->string('date_of_divorce')->nullable(); 
            $table->string('court_order_and_agreement')->nullable(); 
            $table->string('maintenance_order')->nullable();
            $table->string('proof_of_maintenance_order')->nullable(); 
            $table->string('specific_agreements_description')->nullable(); 
            $table->string('specific_agreements_file')->nullable();
            $table->string('comunity_marriage')->nullable();
            $table->string('accrual')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('spouses');
    }
};
