<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('place_of_works', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->string('employer_name');
            $table->string('employer_address');
            $table->string('salary_number');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('place_of_works');
    }
};
