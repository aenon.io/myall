<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('children', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->string('child_id_number')->nullable();
            $table->string('child_name');
            $table->string('middle_name')->nullable();
            $table->string('child_surname');
            $table->string('child_date_of_birth')->nullable();
            $table->string('child_birth_certificate')->nullable();
            $table->string('child_adoption_certificate')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('children');
    }
};
