<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('interviews', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->string('have_property_outside_sa')->nullable();
            $table->string('read_and_write_capability')->nullable();
            $table->string('leave_possesions_to_icapitated_persons')->nullable();
            $table->string('exclude_persons_from_will')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('interviews');
    }
};
