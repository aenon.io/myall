<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('will_options', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->integer('profile');
            $table->integer('executor');
            $table->integer('spouse')->default(0);
            $table->integer('minor_children_beneficiary')->default(0);
            $table->integer('guardian')->default(0);
            $table->integer('testamentary_trust')->default(0);
            $table->integer('inheritance')->default(0);
            $table->integer('specific_inheritance')->default(0);
            $table->integer('remainder_inheritance')->default(0);
            $table->integer('simultaneous_death')->default(0);
            $table->integer('funeral_wishes')->default(0);
            $table->integer('witnesses');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('will_options');
    }
};
