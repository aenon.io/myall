<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('organisation_beneficiaries', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->integer('inheritance_id');
            $table->string('organisation_name');
            $table->string('registration_number');
            $table->text('address');
            $table->string('city');
            $table->string('postal_code');
            $table->string('province');
            $table->string('organisation_type')->nullable();
            $table->text('description')->nullable();
            $table->string('value')->nullable();
            $table->string('percentage')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('organisation_beneficiaries');
    }
};
