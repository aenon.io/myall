<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('business_wealths', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->string('business_type'); 
            $table->string('business_name');
            $table->string('registration_number')->nullable(); 
            $table->string('vat_registered')->nullable();
            $table->string('vat_number')->nullable();
            $table->string('masters_office')->nullable(); 
            $table->string('trust_type')->nullable();
            $table->string('specific_trust_type')->nullable(); 
            $table->string('possessions')->nullable();
            $table->string('buy_sell_agreement')->nullable(); 
            $table->string('value_min')->nullable();
            $table->string('value_max')->nullable();
            $table->string('equipment')->nullable();
            $table->string('outstanding_debtors')->nullable(); 
            $table->string('goodwill')->nullable();
            $table->string('net_value')->nullable(); 
            $table->string('credit')->nullable();
            $table->string('debt')->nullable();
            $table->string('auditor_name')->nullable(); 
            $table->string('auditor_surname')->nullable(); 
            $table->string('auditor_tel')->nullable();
            $table->string('auditor_email')->nullable(); 
            $table->string('auditor_address')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('business_wealths');
    }
};
