<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('business_wealth_debtors', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->integer('business_wealth_id');
            $table->string('debt_to');
            $table->string('debt_value')->nullable();
            $table->string('debt_insured')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('business_wealth_debtors');
    }
};
