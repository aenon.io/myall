<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('insurances', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->string('insurance_type'); 
            $table->string('insurer');
            $table->string('policy_number'); 
            $table->string('contact')->nullable();
            $table->string('financial_advisor')->nullable(); 
            $table->string('outstanding_debt')->nullable()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('insurances');
    }
};
