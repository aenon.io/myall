<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('funerals', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->text('policy_details')->nullable();
            $table->text('memorials_service')->nullable();
            $table->text('cremation_details')->nullable();
            $table->text('parlor_details')->nullable();
            $table->text('requests')->nullable();
            $table->text('obituary')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('funerals');
    }
};
