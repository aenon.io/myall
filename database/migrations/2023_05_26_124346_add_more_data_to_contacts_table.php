<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('contacts', function (Blueprint $table) {
            $table->string('aa_company_name')->nullable()->after('company_info');
            $table->string('aa_contact_details')->nullable()->after('company_info');
            $table->string('aa_contact_name')->nullable()->after('company_info');
            $table->string('aa_contact_surname')->nullable()->after('company_info');
            $table->string('aa_contact_email')->nullable()->after('company_info');
            $table->string('aa_contact_number')->nullable()->after('company_info');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('contacts', function (Blueprint $table) {
            //
        });
    }
};
