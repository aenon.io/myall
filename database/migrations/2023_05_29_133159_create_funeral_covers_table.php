<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('funeral_covers', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->string('policy_number');
            $table->string('policy_holder');
            $table->string('company');
            $table->string('coverage_amount');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('funeral_covers');
    }
};
