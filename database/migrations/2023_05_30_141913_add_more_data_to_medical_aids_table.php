<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('medical_aids', function (Blueprint $table) {
            $table->string('name')->nullable()->after('medical_aid_certificate');
            $table->string('surname')->nullable()->after('medical_aid_certificate');
            $table->string('id_number')->nullable()->after('medical_aid_certificate');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('medical_aids', function (Blueprint $table) {
            //
        });
    }
};
