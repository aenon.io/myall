<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('business_wealths', function (Blueprint $table) {
            $table->string('founding_statement')->nullable()->after('auditor_address');
            $table->string('amended_founding_statement')->nullable()->after('auditor_address');
            $table->string('members_resolution')->nullable()->after('auditor_address');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('business_wealths', function (Blueprint $table) {
            //
        });
    }
};
