<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('business_wealths', function (Blueprint $table) {
            $table->string('certificate_of_registration')->nullable()->after('founding_statement');
            $table->string('sars_issued_document')->nullable()->after('founding_statement');
            $table->string('letter_from_auditors')->nullable()->after('founding_statement');
            $table->string('directors_resolution')->nullable()->after('founding_statement');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('business_wealths', function (Blueprint $table) {
            //
        });
    }
};
