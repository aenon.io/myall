<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('business_wealths', function (Blueprint $table) {
            $table->string('trust_deed')->nullable()->after('certificate_of_registration');
            $table->string('letter_of_authority')->nullable()->after('certificate_of_registration');
            $table->string('resolution_by_trustee')->nullable()->after('certificate_of_registration');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('business_wealths', function (Blueprint $table) {
            //
        });
    }
};
