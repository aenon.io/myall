<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('questionniaires', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->string('favorite_song')->nullable();
            $table->string('favorite_movie')->nullable();
            $table->string('favorite_place')->nullable(); 
            $table->string('favorite_food')->nullable();
            $table->string('special_memory')->nullable();
            $table->string('biggest_lesson')->nullable(); 
            $table->string('regret')->nullable();
            $table->string('miss_most')->nullable();
            $table->string('life_best_part')->nullable();
            $table->string('life_worst_part')->nullable();
            $table->string('religion_beliefs')->nullable(); 
            $table->string('funeral_wish')->nullable(); 
            $table->string('dint_know')->nullable();
            $table->string('hopes_for_someone')->nullable(); 
            $table->string('happy_thing')->nullable(); 
            $table->string('sad_thing')->nullable();
            $table->string('best_quality')->nullable(); 
            $table->string('bucket_list')->nullable();
            $table->string('remember_me_by')->nullable(); 
            $table->string('funny_memory')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('questionniaires');
    }
};
