<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('shares', function (Blueprint $table) {
            $table->integer('assets')->default(0)->after('properties_wealth');
            $table->integer('investments_and_policies')->default(0)->after('properties_wealth');
            $table->integer('bank_accounts')->default(0)->after('properties_wealth');
            $table->integer('expenses')->default(0)->after('debt_expenses');
            $table->integer('loans')->default(0)->after('debt_expenses');
            $table->integer('insurance')->default(0)->after('debt_expenses');
            $table->integer('other_expenses')->default(0)->after('debt_expenses');
            $table->integer('friends')->default(0)->after('family');
            $table->integer('funeral_cover')->default(0)->after('medical_aid');
            $table->integer('income_tax')->default(0)->after('what_to_find');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('shares', function (Blueprint $table) {
            //
        });
    }
};
