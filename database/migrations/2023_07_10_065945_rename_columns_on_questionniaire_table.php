<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('questionniaires', function (Blueprint $table) {
            $table->dropColumn('life_best_part');
            $table->dropColumn('miss_most');
            $table->dropColumn('life_worst_part');
            $table->dropColumn('funeral_wish');
            $table->renameColumn('dint_know', 'knew_earlier');
            $table->string('proud_of')->after('bucket_list')->nullable();
            $table->string('advice')->after('bucket_list')->nullable();
            $table->renameColumn('hopes_for_someone', 'thank_someone');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('questionniares', function (Blueprint $table) {
            //
        });
    }
};
