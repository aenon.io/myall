<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('organisation_beneficiaries', function (Blueprint $table) {
            $table->string('organisation_other_type')->nullable()->after('organisation_type');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('organisation_beneficiaries', function (Blueprint $table) {
            //
        });
    }
};
