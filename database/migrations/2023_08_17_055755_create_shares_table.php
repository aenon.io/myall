<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('shares', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->integer('contact_id')->nullable();
            $table->string('name',);
            $table->string('email');
            $table->string('id_number')->nullable(); 
            $table->date('dob')->nullable();
            $table->string('family')->nullable(); 
            $table->string('friends')->nullable(); 
            $table->string('professional')->nullable(); 
            $table->string('specify_relation')->nullable();
            $table->integer('emergency_contact')->default(0);
            $table->string('color')->nullable();
            $table->integer('order_num')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('shares');
    }
};
