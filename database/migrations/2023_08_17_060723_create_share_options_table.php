<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('share_options', function (Blueprint $table) {
            $table->id();
            $table->integer('share_id');
            $table->integer('profile')->default(0);
            $table->integer('organiser')->default(0);
            $table->integer('personal')->default(0);
            $table->integer('spouse')->default(0);
            $table->integer('work')->default(0);
            $table->integer('tax')->default(0);
            $table->integer('children')->default(0);
            $table->integer('what_to_find')->default(0);
            $table->integer('medical_aid')->default(0);
            $table->integer('connections')->default(0);
            $table->integer('family')->default(0);
            $table->integer('personal_connections')->default(0);
            $table->integer('business')->default(0);
            $table->integer('doctors')->default(0);
            $table->integer('emergency')->default(0);
            $table->integer('accountant_bookkeeper')->default(0);
            $table->integer('private_banker')->default(0);
            $table->integer('lawyer')->default(0);
            $table->integer('business_wealth')->default(0);
            $table->integer('properties_wealth')->default(0);
            $table->integer('debt_expenses')->default(0);
            $table->integer('social_media')->default(0);
            $table->integer('safe_keeping')->default(0);
            $table->integer('memories')->default(0);
            $table->integer('funeral')->default(0);
            $table->integer('family_tree')->default(0);
            $table->integer('education')->default(0);
            $table->integer('funeral_cover')->default(0);
            $table->integer('friends')->default(0);
            $table->integer('assets')->default(0);
            $table->integer('investments_and_policies')->default(0);
            $table->integer('bank_accounts')->default(0);
            $table->integer('expenses')->default(0);
            $table->integer('loans')->default(0);
            $table->integer('insurance')->default(0);
            $table->integer('other_expenses')->default(0);
            $table->integer('income_tax')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('share_options');
    }
};
