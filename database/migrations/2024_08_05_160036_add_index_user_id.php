<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
        public function up(): void
        {
            Schema::table('assets', function (Blueprint $table) {
                $table->index('user_id');
            });

            Schema::table('banks', function (Blueprint $table) {
                $table->index('user_id');
            });

            Schema::table('business_wealths', function (Blueprint $table) {
                $table->index('user_id');
            });

            Schema::table('business_wealth_creditors', function (Blueprint $table) {
                $table->index('user_id');
            });

            Schema::table('business_wealth_debtors', function (Blueprint $table) {
                $table->index('user_id');
            });

            Schema::table('business_wealth_members', function (Blueprint $table) {
                $table->index('user_id');
            });

            Schema::table('business_wealth_trustees', function (Blueprint $table) {
                $table->index('user_id');
            });

            Schema::table('children', function (Blueprint $table) {
                $table->index('user_id');
            });

            Schema::table('contacts', function (Blueprint $table) {
                $table->index('user_id');
            });

            Schema::table('dependents', function (Blueprint $table) {
                $table->index('user_id');
            });

            Schema::table('education', function (Blueprint $table) {
                $table->index('user_id');
            });

            Schema::table('executors', function (Blueprint $table) {
                $table->index('user_id');
            });

            Schema::table('expenses', function (Blueprint $table) {
                $table->index('user_id');
            });

            Schema::table('funerals', function (Blueprint $table) {
                $table->index('user_id');
            });

            Schema::table('funeralwishes', function (Blueprint $table) {
                $table->index('user_id');
            });

            Schema::table('funeral_covers', function (Blueprint $table) {
                $table->index('user_id');
            });

            Schema::table('guardians', function (Blueprint $table) {
                $table->index('user_id');
            });

            Schema::table('individual_beneficiaries', function (Blueprint $table) {
                $table->index('user_id');
            });

            Schema::table('inheritances', function (Blueprint $table) {
                $table->index('user_id');
            });

            Schema::table('insurances', function (Blueprint $table) {
                $table->index('user_id');
            });

            Schema::table('interviews', function (Blueprint $table) {
                $table->index('user_id');
            });

            Schema::table('investments', function (Blueprint $table) {
                $table->index('user_id');
            });

            Schema::table('investment_beneficiaries', function (Blueprint $table) {
                $table->index('user_id');
            });

            Schema::table('medical_aids', function (Blueprint $table) {
                $table->index('user_id');
            });

            Schema::table('medical_aid_conditions', function (Blueprint $table) {
                $table->index('user_id');
            });

            Schema::table('memories', function (Blueprint $table) {
                $table->index('user_id');
            });

            Schema::table('organisation_beneficiaries', function (Blueprint $table) {
                $table->index('user_id');
            });

            Schema::table('place_of_works', function (Blueprint $table) {
                $table->index('user_id');
            });

            Schema::table('possessions', function (Blueprint $table) {
                $table->index('user_id');
            });

            Schema::table('safe_keeps', function (Blueprint $table) {
                $table->index('user_id');
            });

            Schema::table('shares', function (Blueprint $table) {
                $table->index('user_id');
            });

            Schema::table('share_options', function (Blueprint $table) {
                $table->index('share_id');
            });

            // Add other tables as needed
        }

        public function down(): void
        {
            Schema::table('assets', function (Blueprint $table) {
                $table->dropIndex(['user_id']);
            });

            Schema::table('banks', function (Blueprint $table) {
                $table->dropIndex(['user_id']);
            });

            Schema::table('business_wealths', function (Blueprint $table) {
                $table->dropIndex(['user_id']);
            });

            Schema::table('business_wealth_creditors', function (Blueprint $table) {
                $table->dropIndex(['user_id']);
            });

            Schema::table('business_wealth_debtors', function (Blueprint $table) {
                $table->dropIndex(['user_id']);
            });

            Schema::table('business_wealth_members', function (Blueprint $table) {
                $table->dropIndex(['user_id']);
            });

            Schema::table('business_wealth_trustees', function (Blueprint $table) {
                $table->dropIndex(['user_id']);
            });

            Schema::table('children', function (Blueprint $table) {
                $table->dropIndex(['user_id']);
            });

            Schema::table('contacts', function (Blueprint $table) {
                $table->dropIndex(['user_id']);
            });

            Schema::table('dependents', function (Blueprint $table) {
                $table->dropIndex(['user_id']);
            });

            Schema::table('education', function (Blueprint $table) {
                $table->dropIndex(['user_id']);
            });

            Schema::table('executors', function (Blueprint $table) {
                $table->dropIndex(['user_id']);
            });

            Schema::table('expenses', function (Blueprint $table) {
                $table->dropIndex(['user_id']);
            });

            Schema::table('funerals', function (Blueprint $table) {
                $table->dropIndex(['user_id']);
            });

            Schema::table('funeralwishes', function (Blueprint $table) {
                $table->dropIndex(['user_id']);
            });

            Schema::table('funeral_covers', function (Blueprint $table) {
                $table->dropIndex(['user_id']);
            });

            Schema::table('guardians', function (Blueprint $table) {
                $table->dropIndex(['user_id']);
            });

            Schema::table('individual_beneficiaries', function (Blueprint $table) {
                $table->dropIndex(['user_id']);
            });

            Schema::table('inheritances', function (Blueprint $table) {
                $table->dropIndex(['user_id']);
            });

            Schema::table('insurances', function (Blueprint $table) {
                $table->dropIndex(['user_id']);
            });

            Schema::table('interviews', function (Blueprint $table) {
                $table->dropIndex(['user_id']);
            });

            Schema::table('investments', function (Blueprint $table) {
                $table->dropIndex(['user_id']);
            });

            Schema::table('investment_beneficiaries', function (Blueprint $table) {
                $table->dropIndex(['user_id']);
            });

            Schema::table('medical_aids', function (Blueprint $table) {
                $table->dropIndex(['user_id']);
            });

            Schema::table('medical_aid_conditions', function (Blueprint $table) {
                $table->dropIndex(['user_id']);
            });

            Schema::table('memories', function (Blueprint $table) {
                $table->dropIndex(['user_id']);
            });

            Schema::table('organisation_beneficiaries', function (Blueprint $table) {
                $table->dropIndex(['user_id']);
            });

            Schema::table('place_of_works', function (Blueprint $table) {
                $table->dropIndex(['user_id']);
            });

            Schema::table('possessions', function (Blueprint $table) {
                $table->dropIndex(['user_id']);
            });

            Schema::table('safe_keeps', function (Blueprint $table) {
                $table->dropIndex(['user_id']);
            });

            Schema::table('shares', function (Blueprint $table) {
                $table->dropIndex(['user_id']);
            });

            Schema::table('share_options', function (Blueprint $table) {
                $table->dropIndex(['share_id']);
            });

            // Drop other indexes as needed
        }
    };
    // <?php
    //
    // use Illuminate\Database\Migrations\Migration;
    // use Illuminate\Database\Schema\Blueprint;
    // use Illuminate\Support\Facades\Schema;
    //
    // return new class extends Migration
    // {
    //     /**
    //      * Run the migrations.
    //      */
    //     public function up(): void
    //     {
    //         //
    //     }
    //
    //     /**
    //      * Reverse the migrations.
    //      */
    //     public function down(): void
    //     {
    //         //
    //     }
    // };
