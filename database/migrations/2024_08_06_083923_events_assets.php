<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::statement("
CREATE VIEW events_assets AS
SELECT
    'asset_new' AS event_type,
    user_id,
    CONCAT('You added a new asset: ', description) AS description,
    created_at AS timestamp
FROM assets
UNION ALL
SELECT
    'asset_update' AS event_type,
    user_id,
    CONCAT('You updated an asset: ', description) AS description,
    updated_at AS timestamp
FROM assets
WHERE updated_at > created_at;
            ");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        DB::statement('DROP VIEW IF EXISTS events_assets');
    }
};
