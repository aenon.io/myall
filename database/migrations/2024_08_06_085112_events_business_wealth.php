<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::statement("
CREATE VIEW events_business_wealths AS
SELECT
    'business_wealth_new' AS event_type,
    user_id,
    CONCAT('You added new business wealth: ', business_name) AS description,
    created_at AS timestamp
FROM business_wealths
UNION ALL
SELECT
    'business_wealth_update' AS event_type,
    user_id,
    CONCAT('You updated your business wealth: ', business_name) AS description,
    updated_at AS timestamp
FROM business_wealths
WHERE updated_at > created_at;
");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        DB::statement('DROP VIEW IF EXISTS events_business_wealths');
    }
};
