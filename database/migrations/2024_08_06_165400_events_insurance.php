<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::statement("
CREATE VIEW events_insurances AS
SELECT
    'insurance_add' AS event_type,
    user_id,
    CONCAT('You added a new ', insurance_type, ' policy') AS description,
    created_at AS timestamp
FROM insurances
UNION ALL
SELECT
    'insurance_update' AS event_type,
    user_id,
    CONCAT('You updated your ', insurance_type, ' policy') AS description,
    updated_at AS timestamp
FROM insurances
WHERE updated_at > created_at;
");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        DB::statement('DROP VIEW IF EXISTS events_insurances');
    }
};
