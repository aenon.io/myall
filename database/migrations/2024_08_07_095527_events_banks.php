<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        //
        DB::statement("
CREATE VIEW events_banks AS
SELECT
    'banks_new' AS event_type,
    user_id,
    CONCAT('You added a new bank account: ', account_type) AS description,
    created_at AS timestamp
FROM banks
UNION ALL
SELECT
    'banks_update' AS event_type,
    user_id,
    CONCAT('You updated an bank account: ', account_type) AS description,
    updated_at AS timestamp
FROM banks
WHERE updated_at > created_at;
            ");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
        DB::statement('DROP VIEW IF EXISTS events_banks');
    }
};
