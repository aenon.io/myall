<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        //
        DB::statement("
CREATE VIEW events_business_wealth_creditors AS
SELECT
    'business_wealth_creditor_new' AS event_type,
    bwc.user_id,
    CONCAT('You added a new creditor ', bwc.credit_to, ' to ', bw.business_name) AS description,
    bwc.created_at AS timestamp
FROM business_wealth_creditors bwc
JOIN business_wealths bw ON bwc.business_wealth_id = bw.id
UNION ALL
SELECT
    'business_wealth_creditor_update' AS event_type,
    bwc.user_id,
    CONCAT('You updated your creditor ', bwc.credit_to, ' on ', bw.business_name) AS description,
    bwc.updated_at AS timestamp
FROM business_wealth_creditors bwc
JOIN business_wealths bw ON bwc.business_wealth_id = bw.id
WHERE bwc.updated_at > bwc.created_at;
");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
        DB::statement('DROP VIEW IF EXISTS events_business_wealth_creditors');
    }
};
