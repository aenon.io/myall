<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        //
        DB::statement("
CREATE VIEW events_business_wealth_debtors AS
SELECT
    'business_wealth_debtor_new' AS event_type,
    bwd.user_id,
    CONCAT('You added a new debtor ', bwd.debt_to, ' to ', bw.business_name) AS description,
    bwd.created_at AS timestamp
FROM business_wealth_debtors bwd
JOIN business_wealths bw ON bwd.business_wealth_id = bw.id
UNION ALL
SELECT
    'business_wealth_debtor_update' AS event_type,
    bwd.user_id,
    CONCAT('You updated your debtor ', bwd.debt_to, ' on ', bw.business_name) AS description,
    bwd.updated_at AS timestamp
FROM business_wealth_debtors bwd
JOIN business_wealths bw ON bwd.business_wealth_id = bw.id
WHERE bwd.updated_at > bwd.created_at;
            ");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
        DB::statement('DROP VIEW IF EXISTS events_business_wealth_debtors');
    }
};
