<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        //
        DB::statement("
CREATE VIEW events_business_wealth_members AS
SELECT
    'business_wealth_member_new' AS event_type,
    bwm.user_id,
    CONCAT('You added a new member ', bwm.member_name, ' ', bwm.member_surname, ' to ', bw.business_name) AS description,
    bwm.created_at AS timestamp
FROM business_wealth_members bwm
JOIN business_wealths bw ON bwm.business_wealth_id = bw.id
UNION ALL
SELECT
    'business_wealth_member_update' AS event_type,
    bwm.user_id,
    CONCAT('You updated your member ', bwm.member_name, ' ', bwm.member_surname, ' on ', bw.business_name) AS description,
    bwm.updated_at AS timestamp
FROM business_wealth_members bwm
JOIN business_wealths bw ON bwm.business_wealth_id = bw.id
WHERE bwm.updated_at > bwm.created_at;
");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
        DB::statement('DROP VIEW IF EXISTS events_business_wealth_members');
    }
};
