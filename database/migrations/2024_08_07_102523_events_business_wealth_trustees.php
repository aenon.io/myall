<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        //
        DB::statement("
CREATE VIEW events_business_wealth_trustees AS
SELECT
    'business_wealth_trustee_new' AS event_type,
    bwt.user_id,
    CONCAT('You added a new trustee ', bwt.trustee_first_name, ' ', bwt.trustee_surname, ' to ', bw.business_name) AS description,
    bwt.created_at AS timestamp
FROM business_wealth_trustees bwt
JOIN business_wealths bw ON bwt.business_wealth_id = bw.id
UNION ALL
SELECT
    'business_wealth_trustee_update' AS event_type,
    bwt.user_id,
    CONCAT('You updated your trustee ', bwt.trustee_first_name, ' ', bwt.trustee_surname, ' on ', bw.business_name) AS description,
    bwt.updated_at AS timestamp
FROM business_wealth_trustees bwt
JOIN business_wealths bw ON bwt.business_wealth_id = bw.id
WHERE bwt.updated_at > bwt.created_at;
");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
        DB::statement('DROP VIEW IF EXISTS events_business_wealth_trustees');
    }
};
