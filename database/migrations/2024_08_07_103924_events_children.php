<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        //
        DB::statement("
CREATE VIEW events_children AS
SELECT
    'child_new' AS event_type,
    user_id,
    CONCAT('You added a new child to your organiser: ', child_name, ' ', child_surname) AS description,
    created_at AS timestamp
FROM children
UNION ALL
SELECT
    'child_update' AS event_type,
    user_id,
    CONCAT('You updated a child in your organiser: ', child_name, ' ', child_surname) AS description,
    updated_at AS timestamp
FROM children
WHERE updated_at > created_at;
");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
        DB::statement('DROP VIEW IF EXISTS events_children');
    }
};
