<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        //
        DB::statement("
CREATE VIEW events_shares AS
SELECT
    'share_new' AS event_type,
    s.user_id,
    CONCAT('You added a new share contact: ', s.name) AS description,
    s.created_at AS timestamp,
    NULL AS share_from
FROM shares s

UNION ALL

SELECT
    'share_update' AS event_type,
    s.user_id,
    CONCAT('You updated a share contact: ', s.name) AS description,
    s.updated_at AS timestamp,
    NULL AS share_from
FROM shares s
WHERE s.updated_at > s.created_at

UNION ALL

SELECT
    'share_with' AS event_type,
    u.id AS user_id,
    CONCAT(uu.name, ' ', uu.surname, ' shared with you') AS description,
    s.created_at AS timestamp,
    s.user_id AS share_from
FROM shares s
JOIN users u ON s.email = u.email
JOIN users uu ON s.user_id = uu.id
WHERE u.id IS NOT NULL;
");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
        DB::statement('DROP VIEW IF EXISTS events_shares');
    }
};
