<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        //
        DB::statement("
CREATE VIEW organiser_progress AS
SELECT 'personal' AS `organiser_section`, `education`.`user_id` AS `user_id` FROM `education`
UNION
SELECT 'personal' AS `organiser_section`, `spouses`.`user_id` AS `user_id` FROM `spouses`
UNION
SELECT 'personal' AS `organiser_section`, `place_of_works`.`user_id` AS `user_id` FROM `place_of_works`
UNION
SELECT 'personal' AS `organiser_section`, `children`.`user_id` AS `user_id` FROM `children`
UNION
SELECT 'personal' AS `organiser_section`, `medical_aids`.`user_id` AS `user_id` FROM `medical_aids`
UNION
SELECT 'emergency_contacts' AS `organiser_section`, `contacts`.`user_id` AS `user_id` FROM `contacts`
UNION
SELECT 'business_wealth' AS `organiser_section`, `business_wealths`.`user_id` AS `user_id` FROM `business_wealths`
UNION
SELECT 'personal_wealth' AS `organiser_section`, `assets`.`user_id` AS `user_id` FROM `assets`
UNION
SELECT 'personal_wealth' AS `organiser_section`, `investments`.`user_id` AS `user_id` FROM `investments`
UNION
SELECT 'personal_wealth' AS `organiser_section`, `banks`.`user_id` AS `user_id` FROM `banks`
UNION
SELECT 'debt_and_expenses' AS `organiser_section`, `expenses`.`user_id` AS `user_id` FROM `expenses`
UNION
SELECT 'debt_and_expenses' AS `organiser_section`, `insurances`.`user_id` AS `user_id` FROM `insurances`
UNION
SELECT 'social_media' AS `organiser_section`, `social_media`.`user_id` AS `user_id` FROM `social_media`
UNION
SELECT 'safekeeping' AS `organiser_section`, `safe_keeps`.`user_id` AS `user_id` FROM `safe_keeps`
UNION
SELECT 'safekeeping' AS `organiser_section`, `taxes`.`user_id` AS `user_id` FROM `taxes`
UNION
SELECT 'memories' AS `organiser_section`, `memories`.`user_id` AS `user_id` FROM `memories`
UNION
SELECT 'funeral' AS `organiser_section`, `funerals`.`user_id` AS `user_id` FROM `funerals`
UNION
SELECT 'funeral' AS `organiser_section`, `funeralwishes`.`user_id` AS `user_id` FROM `funeralwishes`;
");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
        DB::statement('DROP VIEW IF EXISTS organiser_progress');
    }
};
