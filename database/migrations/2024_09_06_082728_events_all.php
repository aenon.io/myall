<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        //
        DB::statement("
CREATE VIEW events_all AS
SELECT event_type, user_id, description, timestamp, NULL as share_from FROM events_assets
UNION ALL
SELECT event_type, user_id, description, timestamp, NULL as share_from FROM events_insurances
UNION ALL
SELECT event_type, user_id, description, timestamp, NULL as share_from FROM events_banks
UNION ALL
SELECT event_type, user_id, description, timestamp, NULL as share_from FROM events_business_wealths
UNION ALL
SELECT event_type, user_id, description, timestamp, NULL as share_from FROM events_business_wealth_creditors
UNION ALL
SELECT event_type, user_id, description, timestamp, NULL as share_from FROM events_business_wealth_debtors
UNION ALL
SELECT event_type, user_id, description, timestamp, NULL as share_from FROM events_business_wealth_members
UNION ALL
SELECT event_type, user_id, description, timestamp, NULL as share_from FROM events_business_wealth_trustees
UNION ALL
SELECT event_type, user_id, description, timestamp, NULL as share_from FROM events_children
UNION ALL
SELECT event_type, user_id, description, timestamp, share_from FROM events_shares
ORDER BY timestamp desc;
");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
        DB::statement('DROP VIEW IF EXISTS events_all');
    }
};
