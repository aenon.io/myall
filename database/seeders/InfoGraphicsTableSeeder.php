<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

use App\Models\InfoGraphic;
use App\Models\InfographicKeyword;
use App\Models\KeyWord;

class InfoGraphicsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $arr = [
            [
                'title' => 'What to expect during the wrapping up of a deceased estate',
                'stage' => 'stage2',
                'path' =>  'infographics/stage2/1/What to expect during the wrapping up of a deceased estate.jpg',
                'keywords' => ['deceased estate', 'estate', 'administration', 'executor', 'Master']
            ],
            [
                'title' => 'Explanations of common terms relating to Wills',
                'stage' => 'stage2',
                'path' => 'infographics/stage2/1/Explanations of common terms relating to Wills.jpg',
                'keywords' => ['terms', 'beneficiary', 'will', 'executor', 'trustee', 'minor', 'guardian', 'testator', 'legatee','heir', 'intestate', 'signing', 'signature', 'guardian fund', 'master'],
            ],
            [
                'title' => 'What is a deceased estate',
                'stage' => 'stage2',
                'path' => 'infographics/stage2/1/What is a deceased estate.jpg',
                'keywords' => ['executor', 'deceased estate', 'estate', 'administration', 'dispute', 'spouse', 'minor', 'beneficiary', 'Will', 'Master'],
            ],
            [
                'title' => 'South African legal requirements for a Will',
                'stage' => 'stage2',
                'path' => 'infographics/stage2/1/South African legal requirements for a Will.jpg',
                'keywords' => ['formalities', 'invalid', 'valid', 'Will', 'witness', 'testator', 'inheritance', 'requirement', 'beneficiary', 'signing' , 'signature']
            ],
            [
                'title' => 'Navigating situations of an invalid Will',
                'stage' => 'stage2',
                'path' => 'infographics/stage2/1/Navigating situations of an invalid Will.jpg',
                'keywords' => ['formalities', 'invalid', 'valid', 'Will', 'trustee', 'guardian', 'executor', 'inheritance', 'provision', 'requirement', 'signature', 'signing','not signing', 'internet', 'electronic Will', 'computer', 'inherit', 'beneficiary', 'witness']
            ],
            [
                'title' => 'Ensure (liquidity in your estate) that there is enough cash to pay your deceased estates costs and debts',
                'stage' => 'stage2',
                'path' => 'infographics/stage2/1/Ensure liquidity of your estate at the time of your death.jpg',
                'keywords' => ['deceased estate', 'estate', 'administration', 'executor', 'liquidity', 'cash', 'debts', 'creditors', 'administration costs', 'fees', 'funeral', 'executor', 'tax', 'taxes', 'estate duty', 'SARS', 'bond', 'claim', 'overdraft', 'inheritance']
            ],
            [
                'title' => 'Calculating the cash flow of your deceased estate',
                'stage' => 'stage2',
                'path' => 'infographics/stage2/1/Calculating the cash flow of your deceased estate.jpg',
                'keywords' => ['deceased estate', 'estate', 'administration', 'executor', 'liquidity', 'cash', 'debts', 'creditors', 'executor', 'tax', 'taxes', 'estate duty', 'SARS', 'funeral', 'fees', 'bond', 'claim', 'overdraft', 'inheritance']
            ],
            [
                'title' => 'How is Estate Duty calculated',
                'stage' => 'stage2',
                'path' => 'infographics/stage2/1/How is estate duty calculated.jpg',
                'keywords' => ['deceased estate', 'estate', 'administration', 'executor', 'liquidity', 'cash', 'debts', 'creditors', 'executor', 'tax', 'taxes', 'estate duty', 'SARS', 'bond', 'spouse', 'inherit']
            ],
            [
                'title' => 'What happens if I disinherit my spouse or child',
                'stage' => 'Stage2',
                'path' => 'infographics/stage2/spouse/What happens if I disinherit my spouse or child.jpg',
                'keywords' => ['spouse', 'child', 'minor', 'marriage', 'inheritance', 'deceased estate', 'estate', 'inheritance', 'executor', 'Will', 'disinherit']
            ],
            [
                'title' => 'Who qualifies as a spouse for a maintenance claim',
                'stage' => 'Stage2',
                'path' => 'infographics/stage2/spouse/Who qualifies as a spouse for a maintenance claim.jpg',
                'keywords' => ['spouse', 'maintenance', 'claim', 'dispute', 'marriage', 'deceased estate', 'estate', 'inheritance']
            ],
            [
                'title' => 'How to prevent a possible maintenance claim',
                'stage' => 'Stage2',
                'path' => 'infographics/stage2/spouse/How to prevent a possible maintenance claim.jpg',
                'keywords' => ['spouse', 'maintenance', 'claim', 'dispute', 'marriage', 'deceased estate', 'estate', 'inheritance', 'executor', 'minor', 'child', 'disinherit']
            ],
            [
                'title' => 'Reporting a deceased estate to the Master: What you need to know',
                'stage' => 'stage2',
                'path' => 'infographics/stage2/executor/Reporting a deceased estate to the Master: What you need to know.jpg',
                'keywords' => ['executor', 'procedure', 'report', 'deceased estate', 'estate', 'administration', 'dispute', 'spouse', 'minor', 'beneficiary', 'Will', 'Master']
            ],
            [
                'title' => 'Understanding the Executors responsibilities - A short guide.jpg',
                'stage' => 'Stage2',
                'path' => 'infographics/stage2/executor/Understanding the Executors responsibilities - A short guide.jpg',
                'keywords' => ['executor', 'deceased estate', 'estate', 'administration', 'dispute', 'spouse', 'minor', 'beneficiary', 'Will']
            ],
            [
                'title' => 'What does an Executor do - A short guide',
                'stage' => 'Stage2',
                'path' => 'infographics/stage2/executor/What does an Executor do - A short guide.jpg',
                'keywords' => ['executor', 'deceased estate', 'estate', 'administration', 'dispute', 'spouse', 'minor', 'beneficiary', 'Will', 'Master', 'duties']
            ],
            [
                'title' => 'Types of Estate Administrators: A comparison',
                'stage' => 'Stage2',
                'path' => 'infographics/stage2/executor/Types of estate administrators: A comparison.jpg',
                'keywords' => ['executor', 'deceased estate', 'estate', 'administration', 'dispute', 'spouse', 'minor', 'beneficiary', 'Will', 'Master']
            ],
            [
                'title' => 'What is the purpose of the appointed guardian',
                'stage' => 'Stage2',
                'path' => 'infographics/stage2/guardian/What is the purpose of the appointed guardian.jpg',
                'keywords' => ['guardian', 'minor', 'beneficiary', 'parent', 'mother', 'father', 'child', 'Guardian Fund', 'Master', 'trust']
            ],
            [
                'title' => 'What is the Guardian Fund: Relating to the cash inheritance of a minor beneficiary',
                'stage' => 'Stage2',
                'path' => 'infographics/stage2/guardian/What is the Guardian Fund: Relating to the cash inheritance of a minor beneficiary.jpg',
                'keywords' => ['Guardian fund', 'Master', 'guardian', 'minor', 'beneficiary', 'parent', 'mother', 'father', 'child', 'Will']
            ],
            [
                'title' => 'Who is the natural guardian of a minor child',
                'stage' => 'Stage2',
                'path' => 'infographics/stage2/guardian/Who is the natural guardian of a minor child.jpg',
                'keywords' => ['Guardian fund', 'Master', 'guardian', 'minor', 'beneficiary', 'parent', 'mother', 'father', 'child', 'natural guardian']
            ],
            
            [
                'title' => 'Legal guardianship of minors: Who has the sole authority',
                'stage' => 'Stage2',
                'path' => 'infographics/stage2/guardian/Legal guardianship of minors: Who has the sole authority.jpg',
                'keywords' => ['legal guardian', 'Guardian fund', 'Master', 'guardian', 'minor', 'beneficiary', 'parent', 'mother', 'father', 'child']
            ],
            [
                'title' => 'Will the guardian administer the inheritance of my minor child',
                'stage' => 'Stage2',
                'path' => "infographics/stage2/guardian/Will the guardian administer the inheritance of my minor child.jpg",
                'keywords' => ['Guardian fund', 'Master', 'guardian', 'minor', 'beneficiary', 'parent', 'mother', 'father', 'child', 'inheritance', 'inherit', 'administration']
            ],
            [
                'title' => 'Procedures at the Guardian Fund',
                'stage' => 'Stage2',
                'path' => 'infographics/stage2/guardian/Procedures at the Guardian Fund.jpg',
                'keywords' => ['Guardian fund', 'Master', 'guardian', 'minor', 'beneficiary', 'parent', 'mother', 'father', 'child', 'Will']    
            ],
            [
                'title' => 'Examples of items that can be bequeathed',
                'stage' => 'Stage2',
                'path' => 'infographics/interview/leave_possesions/Examples of items that can be bequeathed.jpg',
                'keywords' => ['property', 'possessions', 'Will', 'beneficiary', 'inheritance', 'inherit']
            ],
            [
                'title' => 'Can you say no to an inheritance? - A short guide',
                'stage' => 'Stage2',
                'path' => 'infographics/stage2/5/Can you say no to an inheritance - A short guide.jpg',
                'keywords' => ['executor', 'administration', 'dispute', 'spouse', 'minor', 'beneficiary', 'Will', 'disinherit', 'inherit', 'inheritance', 'refuse', 'refusal', 'heri', 'legatee']
            ],
            [
                'title' => 'Specified inheritance vs entire estate: What is the difference',
                'stage' => 'Stage2',
                'path' => "infographics/stage2/remainder_inheritance/Specified inheritance vs entire estate: What is the difference.jpg",
                'keywords' => ['inheritance', 'inherit', 'Will', 'provision', 'legatee', 'heir', 'beneficiary', 'estate']
            ],
            [
                'title' => 'Specified inheritance: A guide to what it means',
                'stage' => 'Stage2',
                'path' => 'infographics/stage2/remainder_inheritance/Specified inheritance: A guide to what it means.jpg',
                'keywords' => ['inheritance', 'inherit', 'Will', 'provision', 'legatee', 'heir', 'beneficiary', 'estate']
            ],
            [
                'title' => 'Inheritance of entire estate',
                'stage' => 'Stage2',
                'path' => 'infographics/stage2/remainder_inheritance/Inheritance of an entire estate.jpg',
                'keywords' => ['inheritance', 'inherit', 'Will', 'provision', 'legatee', 'heir', 'beneficiary', 'estate']
            ],
            [
                'title' => 'Simultaneous Death and inheritance: What you need to know',
                'stage' => 'Stage2',
                'path' => 'infographics/stage2/simultaneous_death/Simultaneous Death and inheritance: What you need to know.jpg',
                'keywords' => ['family obliteration', 'beneficiary', 'death', 'simultaneous death', 'beneficiary', 'heir', 'executor', 'inheritance', 'inherit', 'Will']
            ],
            [
                'title' => 'Consequences when a Testamentary Trust is not created',
                'stage' => 'Stage2',
                'path' => 'infographics/stage2/testamentary_trust/Consequences when a Testamentary Trust is not created.jpg',
                'keywords' => ['minor', 'child', 'trust', 'trustee', 'beneficiary', 'inheritance', 'inherit', 'testamentary trust', 'Guardian Fund', 'Will']    
            ],
            [
                'title' => 'Difference between a Testamentary Trust and a living trust',
                'stage' => 'Stage2',
                'path' => 'infographics/stage2/testamentary_trust/Difference between a Testamentary Trust and a living trust.jpg',
                'keywords' => ['minor', 'child', 'trust', 'trustee', 'beneficiary', 'inheritance', 'inherit', 'testamentary trust', 'Guardian Fund', 'Will', 'living trust', 'lifetime', 'death']
            ],
            [
                'title' => 'The benefits of a Testamentary Trust for a child',
                'stage' => 'Stage2',
                'path' => 'infographics/stage2/testamentary_trust/The benefits of a Testamentary Trust for a child.jpg',
                'keywords' => ['minor', 'child', 'trust', 'beneficiary', 'inheritance', 'inherit', 'testamentary trust', 'Guardian Fund', 'Will']
            ],
            [
                'title' => 'How a Testamentary Trust benefits major beneficiaries',
                'stage' => 'stage2',
                'path' => 'infographics/stage2/testamentary_trust/How a Testamentary Trust benefits major beneficiaries.jpg',
                'keywords' => ['minor', 'child', 'trust', 'beneficiary', 'inheritance', 'inherit', 'testamentary trust', 'Guardian Fund', 'Will', 'major', 'mental disabilities', 'physical disabilities', 'dementia', 'Alzheimer', 'disease', 'patient', 'illness']    
            ],
            [
                'title' => 'When does a Testamentary Trust come into operation',
                'stage' => 'Stage2',
                'path' => 'infographics/stage2/testamentary_trust/When does a Testamentary Trust come into operation.jpg',
                'keywords' => ['minor', 'child', 'trust', 'beneficiary', 'inheritance', 'inherit', 'testamentary trust', 'Guardian Fund', 'Will']
            ],
            [
                'title' => 'Understanding the role of a trustee in a Testamentary Trust',
                'stage' => 'Stage2',
                'path' => 'infographics/stage2/testamentary_trust/Understanding the role of a trustee in a Testamentary Trust.jpg',
                'keywords' => ['minor', 'child', 'trust', 'beneficiary', 'inheritance', 'inherit', 'testamentary trust', 'Guardian Fund', 'Will']   
            ],
            [
                'title' => 'Who may sign as a witness - A short guide',
                'stage' => 'Stage2',
                'path' => 'infographics/stage2/witness/Who may sign as a witness - A short guide.jpg',
                'keywords' => ['witness', 'signature', 'signing', 'formalities', 'invalid', 'valid', 'Will',  'testator', 'inheritance', 'inherit', 'requirement', 'beneficiary', 'executor', 'trustee', 'guardian']
            ],
            [
                'title' => 'Organ and tissue donation: What does it involve',
                'stage' => 'Stage2',
                'path' => 'infographics/stage2/funeral_wishes/Organ and tissue donation: What does it involve.jpg',
                'keywords' => ['Will', 'funeral wishes', 'funeral', 'organ donor', 'donation', 'death']
            ],
            [
                'title' => 'Cremation or burial: Understanding the meaning of funeral wishes',
                'stage' => 'Stage2',
                'path' => 'infographics/stage2/funeral_wishes/Cremation or burial: Understanding the meaning of funeral wishes.jpg',
                'keywords' => ['Will', 'funeral wishes', 'funeral', 'organ donor', 'donation', 'death', 'cremation', 'burial']
            ],
            [
                'title' => 'Cremation or burial procedures before arranging for funeral',
                'stage' => 'Stage2',
                'path' => 'infographics/stage2/funeral_wishes/Cremation or burial procedures before arranging for funeral.jpg',
                'keywords' => ['Will', 'funeral wishes', 'cremation', 'burial', 'funeral', 'death']
            ],
            [
                'title' => 'Understanding legal competence in making a Will',
                'stage' => 'Stage4',
                'path' => 'infographics/stage2/profile/Understanding legal competence in making a Will.jpg',
                'keywords' => ['formalities', 'Will', 'legal', 'testator', 'valid', 'invalid', 'requirement']
            ],

            //VIDEOS
            [
                'title' => "How to apply for funds from the Guardian Fund",
                'stage' => "Stage",
                'path' =>  "vids/How to apply for funds from the Guardian Fund.mp4",
                'type' => "video",
                'keywords' => ['Guardian fund', 'Master', 'guardian', 'minor', 'beneficiary', 'parent', 'mother', 'father', 'child', 'Will']
            ],
            [
                'title' => "No original Will or only a copy found",
                'stage' => "Stage",
                'path' =>  "vids/No original Will or only a copy found.mp4",
                'type' => "video",
                'keywords' => ['copy', 'Will', 'intestate', 'beneficiary', 'inheritance', 'inherit', 'original', 'e-mail', 'electronic Will', 'computer', 'not signed', 'internet', 'valid', 'invalid']
            ],
            [
                'title' => "South African legal requirements for a valid Will",
                'stage' => "Stage",
                'path' =>  "vids/South African legal requirements for a valid Will.mp4",
                'type' => "video",
                'keywords' => ['formalities', 'Will', 'legal', 'testator', 'valid', 'invalid', 'requirement']
            ],
            [
                'title' => "What is a Will",
                'stage' => "Stage",
                'path' =>  "vids/What is a Will.mp4",
                'type' => "video",
                'keywords' => ['Will', 'inheritance', 'inherit', 'executor', 'minor', 'child', 'guardian', 'trust', 'beneficiary']
            ],
            [
                'title' => "What is the difference between a guardian of a minor and the Guardian Fund",
                'stage' => "Stage",
                'path' =>  "vids/What is the difference between a guardian of a minor and the Guardian Fund.mp4",
                'type' => "video",
                'keywords' => ['guardian', 'minor', 'beneficiary', 'inheritance', 'parent', 'mother', 'father', 'child', 'Guardian Fund', 'Master', 'trust']
            ],
            [
                'title' => "What is the role of the appointed guardian",
                'stage' => "Stage",
                'path' =>  "vids/What is the role of the appointed guardian.mp4",
                'type' => "video",
                'keywords' => ['Guardian fund', 'Master', 'guardian', 'minor', 'inheritance', 'beneficiary', 'parent', 'mother', 'father', 'child', 'inheritance', 'inherit', 'administration']
            ],
            [
                'title' => "When does the nomination of a guardian take effect",
                'stage' => "Stage",
                'path' =>  "vids/When does the nomination of a guardian take effect.mp4",
                'type' => "video",
                'keywords' => ['Guardian fund', 'Master', 'guardian', 'inheritance', 'minor', 'beneficiary', 'parent', 'mother', 'father', 'child', 'Will']
            ],
            [
                'title' => "Who may not inherit as a beneficiary",
                'stage' => "Stage",
                'path' =>  "vids/Who may not inherit as a beneficiary.mp4",
                'type' => "video",
                'keywords' => ['formalities', 'invalid', 'valid', 'Will', 'trustee', 'guardian', 'executor', 'beneficiary', 'inheritance', 'provision', 'requirement', 'signature', 'signing', 'inherit', 'witness']
            ],
            [
                'title' => "Will the guardian administer my minor child's details",
                'stage' => "Stage",
                'path' =>  "vids/Will the guardian administer my minor child's details.mp4",
                'type' => "video",
                'keywords' => ['Guardian fund', 'Master', 'guardian', 'minor', 'inheritance', 'beneficiary', 'parent', 'mother', 'father', 'child', 'inheritance', 'inherit', 'administration']
            ],
        ];

        InfoGraphic::truncate();
        InfographicKeyword::truncate();
        KeyWord::truncate();

        foreach($arr AS $info){
            $type = "image";
            if(isset($info['type'])){
                $type = $info['type'];
            }

            $inf = InfoGraphic::create([
                "title" => $info['title'],
                "stage" => $info['stage'],
                "path" => $info['path'],
                'type' => $type,
            ]);
            foreach($info['keywords'] AS $kw){
                $wrd = KeyWord::where('word', strtolower($kw))->first();
                if(!$wrd){
                    $wrd = KeyWord::create([
                        'word' => strtolower($kw)
                    ]);
                }

                InfographicKeyword::create([
                    "info_graphic_id" => $inf->id,
                    "key_word_id" => $wrd->id
                ]);
            }
        }
    }
}
