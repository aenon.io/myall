<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [['name' => 'Miles', 'surname' => 'Monroe', 'email' => 'test@myall.co.za', 'password' => Hash::make('12341234'), 'status' => 1, 'date_of_birth' => '1964-01-01', 'id_number' => '6401019832082', 'free_account' => 1], ['name' => 'Maria', 'surname' => 'Monroe', 'email' => 'test1@myall.co.za', 'password' => Hash::make('12341234'), 'status' => 1, 'payment_date' => Carbon::now(), 'date_of_birth' => '1964-01-01', 'id_number' => '6401018827182']];

        foreach ($users as $user) {
            User::create($user);
        }
    }
}
