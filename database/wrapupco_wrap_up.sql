-- MariaDB dump 10.19  Distrib 10.6.16-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: wrapupco_wrap_up
-- ------------------------------------------------------
-- Server version	10.6.16-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `wrapupco_wrap_up`
--


--
-- Table structure for table `assets`
--

DROP TABLE IF EXISTS `assets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assets` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `asset_type` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  `outstanding` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `assets`
--

LOCK TABLES `assets` WRITE;
/*!40000 ALTER TABLE `assets` DISABLE KEYS */;
INSERT INTO `assets` (`id`, `user_id`, `asset_type`, `value`, `outstanding`, `description`, `created_at`, `updated_at`) VALUES (1,1,'Art','3000000000','','Mona lisa','2023-09-18 11:32:22','2023-09-18 11:32:22'),(2,1,'Art','3000000000','','Scream','2023-09-18 11:32:33','2023-09-18 11:32:33'),(3,2,'Car','10000.00','15000.00','polo 123','2023-09-18 17:06:18','2023-09-18 17:07:00'),(5,4,'Art','1000000','10000','Skildery','2023-09-19 08:41:55','2023-09-19 08:41:55'),(6,4,'Camera','10000','10000','Nokia','2023-09-19 08:42:09','2023-09-19 08:42:09'),(7,4,'Other','200','','Bookcase','2023-09-19 09:45:19','2023-09-19 09:45:19'),(8,9,'Car','200000','0000000','test','2023-09-28 12:55:06','2023-09-28 12:55:06'),(9,71,'Art','200000','','painting','2023-10-27 10:51:24','2023-10-27 10:51:24');
/*!40000 ALTER TABLE `assets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `banks`
--

DROP TABLE IF EXISTS `banks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banks` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `account_type` varchar(255) NOT NULL,
  `bank` varchar(255) NOT NULL,
  `account_number` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  `message` text DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banks`
--

LOCK TABLES `banks` WRITE;
/*!40000 ALTER TABLE `banks` DISABLE KEYS */;
INSERT INTO `banks` (`id`, `user_id`, `account_type`, `bank`, `account_number`, `value`, `message`, `file`, `created_at`, `updated_at`) VALUES (1,1,'Credit Card','FNB','0987654321','3000000000',NULL,'document/AQ0tNULhpG9YRxNbxRzQnaElj8xxHQH1nSZYWxud.pdf','2023-09-18 11:33:52','2023-09-18 11:33:52'),(2,1,'Cheque Account','Standard Bank','988898765','3000000000',NULL,'document/AVOTMNF9KlCeKkm3QWD3Dfl9f2OHYOl4JfidfVpn.pdf','2023-09-18 11:34:12','2023-09-18 11:34:12'),(3,3,'Cheque Account','FNB','1829910192','10000',NULL,'document/aY7fjpFZElT1SSnnuBX9gVLg4yDBvtpROj0nF3Qu.png','2023-09-18 12:02:07','2023-09-18 12:02:07'),(5,4,'Cheque Account','FNB','2232323','1000000',NULL,'document/taDZc13aUJvgahTU2ef16qmrT3Dp98GuTniO9IIq.pdf','2023-09-19 08:48:36','2023-09-19 08:52:30'),(6,4,'Credit Card','Absa','1112232323','10000',NULL,'document/wq1qKWwRG3Bh8cDeDizDbRbkmyIrXVYk1KgMgSK6.pdf','2023-09-19 08:48:55','2023-09-19 08:48:55'),(7,4,'Loan Account','ABC Loans','2232323','3000000',NULL,'document/lTqVZaiwPUy3ejInu0GqeWTuNCsKCXJpSGwqUSH5.pdf','2023-09-19 08:53:05','2023-09-19 08:53:05'),(8,4,'Money Market Account','FNB Money Market','1112232323','120000',NULL,'document/KglvQaddyOYaqifar7KMBrYToiDyFUaEhyVWKHry.jpg','2023-09-19 08:54:20','2023-09-19 08:54:42'),(9,4,'Other','FNB','2232323','10000',NULL,'document/F8AJPDk7GXZOnCV2sJe9I1JFXJbqesWsuqLDAyaj.pdf','2023-09-19 09:03:48','2023-09-19 09:03:48'),(10,4,'Savings Account','FNB','1112232323','3000000',NULL,'document/VDQr5FtoeXoLGwpYRLgrh7JgeSskdcshunyIhUds.pdf','2023-09-19 09:04:13','2023-09-19 09:44:12'),(11,9,'Cheque Account','Absa','0000000','200000',NULL,'document/HHlDw4cQaOt1aUGptry4r0LBoTB1ljDp5vQqM3Y0.png','2023-09-28 12:55:54','2023-09-28 12:55:54'),(12,71,'Savings Account','Absa','0000000','0',NULL,NULL,'2023-10-27 10:53:31','2023-10-27 10:53:31');
/*!40000 ALTER TABLE `banks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `business_wealth_creditors`
--

DROP TABLE IF EXISTS `business_wealth_creditors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `business_wealth_creditors` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `business_wealth_id` int(11) NOT NULL,
  `credit_to` varchar(255) NOT NULL,
  `credit_value` varchar(255) NOT NULL,
  `credit_insured` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `business_wealth_creditors`
--

LOCK TABLES `business_wealth_creditors` WRITE;
/*!40000 ALTER TABLE `business_wealth_creditors` DISABLE KEYS */;
INSERT INTO `business_wealth_creditors` (`id`, `user_id`, `business_wealth_id`, `credit_to`, `credit_value`, `credit_insured`, `created_at`, `updated_at`) VALUES (1,1,1,'FNB','700,000','yes','2023-09-18 11:32:04','2023-09-18 11:32:04'),(2,2,4,'test','5,000.00','no','2023-09-18 17:03:20','2023-09-18 17:03:20'),(3,4,6,'FNB','1,000','no','2023-09-19 08:03:53','2023-09-19 08:18:56'),(4,4,8,'Absa','1,000','no','2023-09-19 08:14:34','2023-09-19 12:01:51'),(5,4,10,'Absa','1,000','yes','2023-09-19 08:14:50','2023-09-19 08:23:00'),(6,4,6,'Absa','1,000','yes','2023-09-19 08:18:56','2023-09-19 08:18:56'),(7,4,10,'FNB','1,000','no','2023-09-19 08:23:32','2023-09-19 08:23:32'),(8,3,11,'Name','12,879,719','','2023-09-19 10:37:57','2023-09-19 10:37:57'),(9,3,3,'Piet','10,000','yes','2023-09-19 10:43:32','2023-09-19 10:43:32'),(10,2,12,'Busone Creditco','15,000','yes','2023-09-19 11:19:22','2023-09-19 11:19:22'),(11,4,5,'FNB','1,000','no','2023-09-19 11:58:01','2023-09-19 11:58:01'),(12,4,5,'FNB credit','10,000','','2023-09-19 11:58:01','2023-09-19 11:58:01'),(13,4,8,'Absa Cheque','3,000','no','2023-09-19 12:01:51','2023-09-19 12:01:51'),(14,4,9,'FNB Credit','1,855','no','2023-09-19 12:04:31','2023-09-19 12:04:31'),(15,4,9,'ABC ','4,000','no','2023-09-19 12:04:31','2023-09-19 12:04:31'),(16,3,13,'Pieterse','10,000','','2023-09-19 12:23:02','2023-09-19 12:23:02');
/*!40000 ALTER TABLE `business_wealth_creditors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `business_wealth_debtors`
--

DROP TABLE IF EXISTS `business_wealth_debtors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `business_wealth_debtors` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `business_wealth_id` int(11) NOT NULL,
  `debt_to` varchar(255) NOT NULL,
  `debt_value` varchar(255) DEFAULT NULL,
  `debt_insured` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `business_wealth_debtors`
--

LOCK TABLES `business_wealth_debtors` WRITE;
/*!40000 ALTER TABLE `business_wealth_debtors` DISABLE KEYS */;
INSERT INTO `business_wealth_debtors` (`id`, `user_id`, `business_wealth_id`, `debt_to`, `debt_value`, `debt_insured`, `created_at`, `updated_at`) VALUES (1,1,1,'SAB','888,888','yes','2023-09-18 11:32:04','2023-09-18 11:32:04'),(3,4,5,'Pierre van Wyk','200,000','yes','2023-09-19 08:03:53','2023-09-19 08:03:53'),(5,4,8,'Pierre van Wyk','200,000','no','2023-09-19 08:19:34','2023-09-19 08:19:34'),(6,4,10,'Pierre van Wyk','600,000','no','2023-09-19 08:23:00','2023-09-19 08:23:00'),(7,4,10,'Piet van Wyk','500,000','no','2023-09-19 08:23:32','2023-09-19 08:23:32'),(8,3,3,'FNB','10,000','yes','2023-09-19 10:47:45','2023-09-19 10:47:45'),(9,2,12,'Busone Debtco','20,000','yes','2023-09-19 11:19:22','2023-09-19 11:19:22'),(10,4,5,'Piet van Wyk','500,000','no','2023-09-19 11:58:01','2023-09-19 11:58:01'),(11,4,6,'Sannie Burger','300,000','no','2023-09-19 11:59:21','2023-09-19 11:59:21'),(12,4,6,'Pierre van Wyk','200,000','no','2023-09-19 11:59:49','2023-09-19 11:59:49'),(13,4,9,'Sannie Louise Burger','120,000','no','2023-09-19 12:04:31','2023-09-19 12:04:31'),(14,4,9,'Kosie Burger','18,000','no','2023-09-19 12:04:31','2023-09-19 12:04:31'),(15,71,16,'Pierre','300,000','no','2023-10-27 10:51:10','2023-10-27 10:51:10');
/*!40000 ALTER TABLE `business_wealth_debtors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `business_wealth_members`
--

DROP TABLE IF EXISTS `business_wealth_members`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `business_wealth_members` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `business_wealth_id` int(11) NOT NULL,
  `member_name` varchar(255) NOT NULL,
  `member_surname` varchar(255) NOT NULL,
  `stake_percentage` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `business_wealth_members`
--

LOCK TABLES `business_wealth_members` WRITE;
/*!40000 ALTER TABLE `business_wealth_members` DISABLE KEYS */;
INSERT INTO `business_wealth_members` (`id`, `user_id`, `business_wealth_id`, `member_name`, `member_surname`, `stake_percentage`, `created_at`, `updated_at`) VALUES (1,1,1,'Nelson','Ndlovu','','2023-09-18 11:32:04','2023-09-18 11:32:04'),(2,1,1,'Wilson','Ndlovu','','2023-09-18 11:32:04','2023-09-18 11:32:04'),(4,4,5,'Susandra ','van Wyk','','2023-09-19 08:03:53','2023-09-19 08:03:53'),(5,4,5,'Cherene','Cook','','2023-09-19 08:03:53','2023-09-19 08:03:53'),(6,4,8,'Susandra ','van Wyk','50','2023-09-19 08:06:31','2023-09-19 08:06:31'),(7,4,8,'Cherene','Cook','50','2023-09-19 08:07:39','2023-09-19 08:07:39'),(8,4,9,'Susandra ','van Wyk','50','2023-09-19 12:04:31','2023-09-19 12:04:31'),(9,4,9,'Cherene','Cook','50','2023-09-19 12:04:31','2023-09-19 12:04:31'),(10,71,16,'Cherene','Cook','50','2023-10-27 10:51:10','2023-10-27 10:51:10'),(11,71,16,'Susandra','van Wyk','50','2023-10-27 10:51:10','2023-10-27 10:51:10');
/*!40000 ALTER TABLE `business_wealth_members` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `business_wealth_trustees`
--

DROP TABLE IF EXISTS `business_wealth_trustees`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `business_wealth_trustees` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `business_wealth_id` int(11) NOT NULL,
  `trustee_first_name` varchar(255) NOT NULL,
  `trustee_surname` varchar(255) NOT NULL,
  `trustee_contact` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `business_wealth_trustees`
--

LOCK TABLES `business_wealth_trustees` WRITE;
/*!40000 ALTER TABLE `business_wealth_trustees` DISABLE KEYS */;
/*!40000 ALTER TABLE `business_wealth_trustees` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `business_wealths`
--

DROP TABLE IF EXISTS `business_wealths`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `business_wealths` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `business_type` varchar(255) NOT NULL,
  `business_name` varchar(255) NOT NULL,
  `registration_number` varchar(255) DEFAULT NULL,
  `company_url` varchar(255) DEFAULT NULL,
  `vat_registered` varchar(255) DEFAULT NULL,
  `vat_number` varchar(255) DEFAULT NULL,
  `masters_office` varchar(255) DEFAULT NULL,
  `trust_type` varchar(255) DEFAULT NULL,
  `specific_trust_type` varchar(255) DEFAULT NULL,
  `possessions` varchar(255) DEFAULT NULL,
  `buy_sell_agreement` varchar(255) DEFAULT NULL,
  `value_min` varchar(255) DEFAULT NULL,
  `value_max` varchar(255) DEFAULT NULL,
  `equipment` varchar(255) DEFAULT NULL,
  `outstanding_debtors` varchar(255) DEFAULT NULL,
  `goodwill` varchar(255) DEFAULT NULL,
  `net_value` varchar(255) DEFAULT NULL,
  `credit` varchar(255) DEFAULT NULL,
  `debt` varchar(255) DEFAULT NULL,
  `auditor_name` varchar(255) DEFAULT NULL,
  `auditor_surname` varchar(255) DEFAULT NULL,
  `auditor_tel` varchar(255) DEFAULT NULL,
  `auditor_email` varchar(255) DEFAULT NULL,
  `auditor_address` varchar(255) DEFAULT NULL,
  `members_resolution` varchar(255) DEFAULT NULL,
  `amended_founding_statement` varchar(255) DEFAULT NULL,
  `founding_statement` varchar(255) DEFAULT NULL,
  `directors_resolution` varchar(255) DEFAULT NULL,
  `letter_from_auditors` varchar(255) DEFAULT NULL,
  `sars_issued_document` varchar(255) DEFAULT NULL,
  `certificate_of_registration` varchar(255) DEFAULT NULL,
  `resolution_by_trustee` varchar(255) DEFAULT NULL,
  `letter_of_authority` varchar(255) DEFAULT NULL,
  `trust_deed` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `business_wealths`
--

LOCK TABLES `business_wealths` WRITE;
/*!40000 ALTER TABLE `business_wealths` DISABLE KEYS */;
INSERT INTO `business_wealths` (`id`, `user_id`, `business_type`, `business_name`, `registration_number`, `company_url`, `vat_registered`, `vat_number`, `masters_office`, `trust_type`, `specific_trust_type`, `possessions`, `buy_sell_agreement`, `value_min`, `value_max`, `equipment`, `outstanding_debtors`, `goodwill`, `net_value`, `credit`, `debt`, `auditor_name`, `auditor_surname`, `auditor_tel`, `auditor_email`, `auditor_address`, `members_resolution`, `amended_founding_statement`, `founding_statement`, `directors_resolution`, `letter_from_auditors`, `sars_issued_document`, `certificate_of_registration`, `resolution_by_trustee`, `letter_of_authority`, `trust_deed`, `created_at`, `updated_at`) VALUES (1,1,'Partnership','The Office',NULL,'www.theoffice.co.za','no',NULL,NULL,NULL,NULL,NULL,'no','','',NULL,NULL,NULL,'40000000','','','Sindi','Malinga','0987654321','sn@mail.com','21 Jump Street',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2023-09-18 11:32:04','2023-09-18 11:32:04'),(2,3,'Trust','Name',NULL,'www.name.co.za',NULL,NULL,'Cape Town',NULL,NULL,NULL,NULL,'','',NULL,NULL,NULL,'','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'document/ofEX9ks1BABKstRtjJDhlkPZWb9Cr3vYjLEKKbhi.png','document/3RhBVJ54hpqoEoAPxXxwr9nuIioWP3KPVSFLXZS8.pdf','document/Azm9HATmKCa1ftre1yTlbYkOXeaJTyon4HL4ndJ3.jpg','2023-09-18 11:51:53','2023-09-18 11:51:53'),(3,3,'Private Company','John','2022/029283/38','www.name.co.za','yes','2022/029283/38',NULL,NULL,NULL,NULL,'yes','1','12222',NULL,NULL,NULL,'12121222','','','Jim','Parsens','0129282738','John@gmail.com','34 Street, Road',NULL,NULL,NULL,'document/6917LIOd9BEbFG9bQ8Y6hJ6BgQBbIbPA3gc9v0xC.png','document/631GVZpEXJUZR97Xez4J4fMkdXm23mhNO4mD9wfn.pdf','document/iZuH7mWD6DOIfZT5iXkWfwLOP6a4MHuirNWTimgC.jpg','document/9zWBNW6HyD6KczsFan6qaK6OWlnzdHRN3kgl3nZF.pdf',NULL,NULL,NULL,'2023-09-18 11:59:27','2023-09-19 10:43:32'),(5,4,'Partnership','Wrapup Partnership',NULL,NULL,'yes',NULL,NULL,NULL,NULL,NULL,NULL,'','',NULL,NULL,NULL,'1000000','','','Piet ','Odendaal',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2023-09-19 08:03:53','2023-09-19 08:03:53'),(6,4,'Sole Ownership','Susandra van Wyk Attorneys',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'100000','','40,000','5,000','200,000','','','','Dewald ','le Roux',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2023-09-19 08:05:16','2023-09-19 12:00:24'),(8,4,'Closed Corporation','Wrapup Development CC','2019/565840/07',NULL,'yes','122334344444',NULL,NULL,NULL,NULL,'yes','','',NULL,NULL,NULL,'','','','Dewald ','Odendaal',NULL,NULL,NULL,'document/mUBu6wpGxOwlBJLOx6Tk3BWqeR3XZNUeaQP7enlK.pdf','document/a4fw3d3xmCUUaIhgnq0YUyr0lWcRzwmkMEYkirki.pdf','document/VtBoX8FyROagZ9AWV4QCwgBDNXvXGXYyHf6eIJzx.pdf',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2023-09-19 08:06:31','2023-09-19 12:01:51'),(9,4,'Private Company','Wrapup Pty Ltd','2019/565840/07','wwww.wrapup.co.za','no',NULL,NULL,NULL,NULL,NULL,NULL,'','',NULL,NULL,NULL,'','','','Koos','le Roux',NULL,NULL,NULL,NULL,NULL,NULL,'document/JQQhRemHhMwboioJ2uFVBYuI8lwiVG4n8g36ZhcW.pdf','document/ir7ibh5x94mgKNL62WZESbTBN9HJcIVRqfl3vmdg.pdf','document/mZKGSCRwx18lsZslVT3vrkiz8nkNPXu7dBHwccM1.pdf','document/r4nMgdCTNOKWFqT1Jb0PoLRhoA9xbyyYQyb6AH0e.pdf',NULL,NULL,NULL,'2023-09-19 08:10:20','2023-09-19 08:10:20'),(10,4,'Trust','Wrapup Family Trust','IT12345/2020',NULL,NULL,NULL,'Bloemfontein','Family',NULL,NULL,NULL,'','',NULL,NULL,NULL,'','200000','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2023-09-19 08:23:00','2023-09-19 08:23:00'),(11,3,'Partnership','Name',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','',NULL,NULL,NULL,'','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2023-09-19 10:37:57','2023-09-19 10:37:57'),(12,2,'Sole Ownership','Business One',NULL,'busone.com','yes','VATBUS12345',NULL,NULL,NULL,NULL,NULL,'500000','','150,000','100,000','50,000','450000','','','Busoneaudit','Busoneauditsur',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2023-09-19 11:19:22','2023-09-19 11:19:22'),(13,3,'Sole Ownership','Business name',NULL,'name.co.za','no',NULL,NULL,NULL,NULL,NULL,NULL,'10000','',NULL,NULL,NULL,'10000','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2023-09-19 12:23:02','2023-09-19 12:23:02'),(14,3,'Closed Corporation','Business name James',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','',NULL,NULL,NULL,'','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2023-09-19 12:24:07','2023-09-19 12:24:07'),(15,9,'Private Company','Lux Appareal','00000000','www.example.co.za','no',NULL,NULL,NULL,NULL,NULL,NULL,'','',NULL,NULL,NULL,'','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'document/H7BDASNQFoGMkptK2pcgwGLnyJtWow9KaNMvjB1R.png','document/9h9V1McaqxMv4uRsslluE34qHDBe7MJwqUyQdI6T.png','document/o9W8WdREwC8F8bFrUnNBTSHXUC56WJjwPptxfx5T.png','document/nNGvTtbZolARhX4pywymRWRsg8SgG4P0YnJCyL4i.jpg',NULL,NULL,NULL,'2023-09-28 12:54:17','2023-09-28 12:54:17'),(16,71,'Private Company','WrapUp','2019/565840/07','www.wrapup.co.za','no',NULL,NULL,NULL,NULL,NULL,NULL,'','',NULL,NULL,NULL,'','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'document/DK6DIWazwrtUrgnyn751EExLPQSrnqcKtMrf32PM.jpg',NULL,NULL,NULL,'2023-10-27 10:51:10','2023-10-27 10:51:10');
/*!40000 ALTER TABLE `business_wealths` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `child_guardians`
--

DROP TABLE IF EXISTS `child_guardians`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `child_guardians` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `child_id` int(11) NOT NULL,
  `guardian_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `child_guardians`
--

LOCK TABLES `child_guardians` WRITE;
/*!40000 ALTER TABLE `child_guardians` DISABLE KEYS */;
/*!40000 ALTER TABLE `child_guardians` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `children`
--

DROP TABLE IF EXISTS `children`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `children` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `child_id_number` varchar(255) DEFAULT NULL,
  `child_name` varchar(255) NOT NULL,
  `middle_name` varchar(255) DEFAULT NULL,
  `child_surname` varchar(255) NOT NULL,
  `child_date_of_birth` varchar(255) DEFAULT NULL,
  `child_birth_certificate` varchar(255) DEFAULT NULL,
  `child_adoption_certificate` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `children`
--

LOCK TABLES `children` WRITE;
/*!40000 ALTER TABLE `children` DISABLE KEYS */;
INSERT INTO `children` (`id`, `user_id`, `child_id_number`, `child_name`, `middle_name`, `child_surname`, `child_date_of_birth`, `child_birth_certificate`, `child_adoption_certificate`, `created_at`, `updated_at`) VALUES (1,1,'ON1234567','Owen','Mandla','Ndlovu','2016-07-27','documents/sa2feEbe153SkZFDDWUlX6XC9x7haJXbYtYYZ7ob.pdf',NULL,'2023-09-18 11:18:37','2023-09-18 11:18:37'),(2,1,'HL1234567','Haley','Ntombi','Ndlovu','2021-10-19','documents/QLIJ1M5l3dcQ8J8laGTmnQ32zxDthOjMKwdVTDLT.pdf',NULL,'2023-09-18 11:18:37','2023-09-18 11:18:37'),(3,3,'','Name','','Surname','2020-02-03','documents/TYSlpLsAcg5AVO8LPMJkRoeX9mEf9fxMNh2ZNe65.png',NULL,'2023-09-18 11:47:23','2023-09-18 11:47:23'),(4,4,'0411235130084','Louis','','Claassens','2004-11-23','documents/QNJLuBn2fYDJMg8k6gUozZXC1RY74PaYDDmsvAHj.pdf','documents/SnAY7qlLZZOz1X4DVdVddLhOJCydoq0dFJXvccfl.pdf','2023-09-19 07:40:11','2023-09-19 07:40:11'),(5,4,'1601260801083','Sannie','Louise','van Wyk','2016-01-26','documents/D20v7eupjJxpE7Fs5L3eBe9NSwztrbo4tObnqMAT.pdf','documents/WhJp8hxRBO6JweWF1IWV1z1Zd2aj2kYRqRQzoPV1.pdf','2023-09-19 07:41:13','2023-09-19 11:20:50'),(6,71,'000000000000','Jaelyn ','','Cook','2016-01-26',NULL,NULL,'2023-10-27 10:47:56','2023-10-27 10:47:56');
/*!40000 ALTER TABLE `children` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `confirmations`
--

DROP TABLE IF EXISTS `confirmations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `confirmations` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `step_4` int(11) DEFAULT NULL,
  `step_5` varchar(255) DEFAULT NULL,
  `step_6` varchar(255) DEFAULT NULL,
  `step_7` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `confirmations`
--

LOCK TABLES `confirmations` WRITE;
/*!40000 ALTER TABLE `confirmations` DISABLE KEYS */;
INSERT INTO `confirmations` (`id`, `user_id`, `step_4`, `step_5`, `step_6`, `step_7`, `created_at`, `updated_at`) VALUES (1,1,1,'1','1','1','2023-09-18 11:50:37','2023-09-18 11:51:03'),(2,2,1,'1','1','1','2023-09-18 17:43:42','2023-09-18 17:45:49'),(4,4,1,NULL,NULL,NULL,'2023-10-30 05:45:29','2023-10-30 05:45:29');
/*!40000 ALTER TABLE `confirmations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contacts`
--

DROP TABLE IF EXISTS `contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contacts` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `surname` varchar(255) NOT NULL,
  `relation` varchar(255) NOT NULL,
  `specify_relation` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `contact_number` varchar(255) NOT NULL,
  `contact_type` varchar(255) NOT NULL,
  `company_info` varchar(255) DEFAULT NULL,
  `aa_contact_number` varchar(255) DEFAULT NULL,
  `aa_contact_email` varchar(255) DEFAULT NULL,
  `aa_contact_surname` varchar(255) DEFAULT NULL,
  `aa_contact_name` varchar(255) DEFAULT NULL,
  `aa_contact_details` varchar(255) DEFAULT NULL,
  `aa_company_name` varchar(255) DEFAULT NULL,
  `is_connection` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contacts`
--

LOCK TABLES `contacts` WRITE;
/*!40000 ALTER TABLE `contacts` DISABLE KEYS */;
INSERT INTO `contacts` (`id`, `user_id`, `name`, `surname`, `relation`, `specify_relation`, `email`, `contact_number`, `contact_type`, `company_info`, `aa_contact_number`, `aa_contact_email`, `aa_contact_surname`, `aa_contact_name`, `aa_contact_details`, `aa_company_name`, `is_connection`, `created_at`, `updated_at`) VALUES (1,1,'Belinda','Mabuya','Wife','','linda@mail.com','0987654321','family','','','','','','','',1,'2023-09-18 11:22:42','2023-09-18 11:22:42'),(2,1,'Nelson','Ndlovu','Brother','','np@mail.com','0987654321','family','','','','','','','',1,'2023-09-18 11:22:42','2023-09-18 11:22:42'),(3,1,'Ntando','Ndlovu','Friend','','nt@mail.com','0987654321','personal','','','','','','','',1,'2023-09-18 11:23:28','2023-09-18 11:23:28'),(4,1,'Daniel','Shona','Friend','','ds@mail.com','0987654321','personal','','','','','','','',1,'2023-09-18 11:23:28','2023-09-18 11:23:28'),(5,1,'Sima','Kobo','GF Labs','','sm@mail.com','0987654321','business','','0987654321','sm@mail.com','Kobo','Sima','','',1,'2023-09-18 11:24:48','2023-09-18 11:24:48'),(6,1,'Steve','Ndaula','Cryptcode','','sn@mail.com','0987654321','business','','0987654321','sn@mail.com','Ndaula','Steve','','',1,'2023-09-18 11:24:48','2023-09-18 11:24:48'),(7,1,'Witch','Doctor','','','wd@mail.com','0987654321','doctor','','','','','','','',1,'2023-09-18 11:25:29','2023-09-18 11:25:29'),(8,1,'Heart','Doctor','','','hd@mail.com','0987654321','doctor','','','','','','','',1,'2023-09-18 11:25:29','2023-09-18 11:25:29'),(9,1,'Sindi','Malinga','Malinga Co','','sm@mail.com','09887654321','accountant','','','','','','','',1,'2023-09-18 11:26:24','2023-09-18 11:26:24'),(10,1,'Sima','Kobo','GF Labs','','sm@mail.com','0987654321','accountant','','','','','','','',1,'2023-09-18 11:26:24','2023-09-18 11:26:24'),(11,1,'Money','Man','FMB','','mn@mail.com','0987654321','banker','','','','','','','',1,'2023-09-18 11:27:09','2023-09-18 11:27:09'),(12,1,'More','Money','Standard','','mm@mail.com','0987654321','banker','','','','','','','',1,'2023-09-18 11:27:09','2023-09-18 11:27:09'),(13,1,'Chris','Moyo','CSM','','ch@mail.com','0987654321','lawyer','','','','','','','',1,'2023-09-18 11:30:31','2023-09-18 11:30:31'),(14,1,'L','Guy','Law','','l@mail.com','0987654321','lawyer','','','','','','','',1,'2023-09-18 11:30:31','2023-09-18 11:30:31'),(15,1,'Drugs','Inc','Drug Dealer','','drug@mail.com','0987654321','other','','','','','','','',1,'2023-09-18 11:30:56','2023-09-18 11:30:56'),(16,2,'Joyce','Doe','Mother','','joyced@gmail.com','0826782345','family','','','','','','','',1,'2023-09-18 11:52:07','2023-09-18 11:52:17'),(17,1,'Q','','Work Friend',NULL,'quintus@thinktank.co.za','0987654321','personal',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'2023-09-18 11:52:45','2023-09-18 11:52:45'),(18,2,'Janice','Doe','Sister','','janiced@gmail.com','0829875432','family','','','','','','','',1,'2023-09-18 11:52:56','2023-09-18 11:52:56'),(19,2,'Peter','Peterton','Best friend','','peterp@gmail.com','0614561234','personal','','','','','','','',1,'2023-09-18 11:54:44','2023-09-18 11:54:44'),(20,2,'John','Good','The Good Company','','john@good.com','0128761234','business','','0120986543','jimjames@test.com','Jameson','Jim','','',1,'2023-09-18 11:55:55','2023-09-18 11:56:36'),(21,1,'Belinda','','Mother',NULL,'ndlovu28@gmail.com','0987654321','family',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'2023-09-18 11:56:53','2023-09-18 11:56:53'),(22,2,'Dr Feeel','Goood','','','drfeelgood@doctor.com','987654321','doctor','','','','','','','',1,'2023-09-18 11:57:51','2023-09-18 11:57:51'),(23,2,'Lawman','Lawness','The Lawplace','','law@lawness.com','0115671234','lawyer','','','','','','','',1,'2023-09-18 11:58:43','2023-09-18 11:58:43'),(24,2,'test','','Best Friend',NULL,'qvander@gmail.com','012123456','personal',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'2023-09-18 17:48:11','2023-09-18 17:48:11'),(25,4,'Cherene Cook','','Step Daughter',NULL,'cherenecook@gmail.com','0825567051','family',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'2023-09-19 07:49:39','2023-09-19 07:49:39'),(26,4,'Louis Claassens','','Son',NULL,'louisclaassens@icloud.com','0788031314','family',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'2023-09-19 13:57:28','2023-09-19 13:57:28'),(27,4,'Test','test','WrapUp','','test','00000000000','business','','000000000','test@wrapup.co.za','test','test','','',1,'2023-09-19 15:41:49','2023-09-19 15:41:49'),(28,4,'Dr Pierre ','Van wyk','','','Drpierre@gmail.com','0000000000','doctor','','','','','','','',1,'2023-09-19 15:44:14','2023-09-19 15:44:14'),(29,4,'Susandra Test','','Work Friend',NULL,'susandra@icloud.com','0788031314','personal',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'2023-09-20 07:56:22','2023-09-20 07:56:22'),(30,4,'Susandra Test 3','','Lawyer',NULL,'susandra@wrapup.co.za','0788031314','other',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'2023-09-20 12:10:18','2023-09-20 12:10:18'),(31,11,'Susandra van Wyk','','Wife',NULL,'vanwyksusandra@gmail.com','0788031314','family',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'2023-09-28 12:13:08','2023-09-28 12:13:08'),(32,4,'Pierre van Wyk','','Husband',NULL,'drpierrevanwyk@gmail.com','0825567051','family',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'2023-09-28 12:23:25','2023-09-28 12:24:25'),(33,9,'Susandra','','Step Mother',NULL,'vanwyksusandra@gmail.com','0788031314','family',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'2023-09-28 13:01:44','2023-09-28 13:01:44'),(34,71,'Pierre','van wyk','Father','','','0825567051','family','','','','','','','',1,'2023-10-27 10:49:09','2023-10-27 10:49:09'),(35,71,'Cherene','','Friend',NULL,'cherenecook@gmail.com','0640656888','family',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'2023-10-27 10:59:48','2023-10-27 10:59:48');
/*!40000 ALTER TABLE `contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dependents`
--

DROP TABLE IF EXISTS `dependents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dependents` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `medical_aid_id` int(11) DEFAULT NULL,
  `funeral_cover_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `surname` varchar(255) DEFAULT NULL,
  `id_number` varchar(255) DEFAULT NULL,
  `percentage` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `condition` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dependents`
--

LOCK TABLES `dependents` WRITE;
/*!40000 ALTER TABLE `dependents` DISABLE KEYS */;
INSERT INTO `dependents` (`id`, `medical_aid_id`, `funeral_cover_id`, `user_id`, `name`, `surname`, `id_number`, `percentage`, `created_at`, `updated_at`, `condition`) VALUES (1,1,NULL,1,'Owen','Ndlovu','ON1234567',NULL,'2023-09-18 11:19:55','2023-09-18 11:19:55','None'),(2,1,NULL,1,'Haley','Ndlovu','HL1234567',NULL,'2023-09-18 11:19:55','2023-09-18 11:19:55','None'),(3,1,NULL,1,'Belinda','Mabuya','BL1234567',NULL,'2023-09-18 11:19:55','2023-09-18 11:19:55','None'),(4,NULL,1,1,'Owen','Ndlovu','ON1234567','20','2023-09-18 11:43:59','2023-09-18 11:43:59',NULL),(5,NULL,1,1,'Haley','Ndlovu','HL1234567','20','2023-09-18 11:43:59','2023-09-18 11:43:59',NULL),(6,NULL,2,2,'','','','100000000000000000000000000000','2023-09-18 17:32:28','2023-09-18 17:32:28',NULL),(7,5,NULL,4,'Louis','Claassens','0411235130084',NULL,'2023-09-19 07:43:54','2023-09-19 07:43:54','none'),(8,5,NULL,4,'Sannie','van Wyk','',NULL,'2023-09-19 07:43:54','2023-09-19 07:43:54','none'),(9,6,NULL,4,'Pierre','van Wyk','5011095121085',NULL,'2023-09-19 07:43:54','2023-09-19 07:43:54','none'),(10,6,NULL,4,'Louis','Claassens','0411235130084',NULL,'2023-09-19 07:43:54','2023-09-19 07:43:54','none'),(11,6,NULL,4,'Sannie','van Wyk','',NULL,'2023-09-19 07:43:54','2023-09-19 07:43:54','none'),(12,NULL,3,4,'Louis','Claassens','0411235130084','20','2023-09-19 08:50:52','2023-09-19 08:50:52',NULL),(14,NULL,3,4,'Sannie','van Wyk','','40','2023-09-19 08:50:52','2023-09-19 08:50:52',NULL),(15,NULL,3,4,'Koos','van Wyk','','10','2023-09-19 08:50:52','2023-09-19 08:50:52',NULL),(16,NULL,4,4,'','','','10','2023-09-19 13:43:26','2023-09-19 13:43:26',NULL),(17,NULL,4,4,'Sannie','de Klerk','','10','2023-09-19 13:43:26','2023-09-19 13:43:26',NULL),(18,NULL,4,4,'Louis','Claassens','0411235130084','10','2023-09-19 13:43:26','2023-09-19 13:43:26',NULL),(19,NULL,4,4,'Sannie','van Wyk','1601260801083','10','2023-09-19 13:43:26','2023-09-19 13:43:26',NULL),(20,NULL,4,4,'Pietie','de Beer','','60','2023-09-19 13:43:26','2023-09-19 13:43:26',NULL);
/*!40000 ALTER TABLE `dependents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `education`
--

DROP TABLE IF EXISTS `education`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `education` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `qualification` varchar(255) DEFAULT NULL,
  `year` varchar(255) DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `certificate` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `education`
--

LOCK TABLES `education` WRITE;
/*!40000 ALTER TABLE `education` DISABLE KEYS */;
INSERT INTO `education` (`id`, `user_id`, `qualification`, `year`, `comment`, `certificate`, `created_at`, `updated_at`) VALUES (1,1,'Degree','2011','Awesome','documents/lJwCkCDfKIcKlsuMI0noSnrxG2lKuOkxnnYbz7nY.pdf','2023-09-18 11:16:21','2023-09-18 11:16:21'),(2,1,'Diploma','2008','Great','documents/XszvN7FkIfBHXO1iTL28fzQXnN4f4HTiyDWYz8RZ.pdf','2023-09-18 11:16:21','2023-09-18 11:16:21'),(3,2,'Degree','2002','School of Testington','documents/TQUBZX5dGDPNEQwsCwwx1fpvDcDZ7uq3AIpKGZbU.pdf','2023-09-18 11:39:56','2023-09-18 11:39:56'),(4,2,'Other','2010','University of Testonia','documents/kuZO5XdSGyw3eqJUrbi9L2aqmUto8IAJgc9FKd94.jpg','2023-09-18 11:40:37','2023-09-18 11:40:37'),(5,3,'Matric','','','documents/FuD6svPepITp0tBRytop9DRi9yFdNpjgLy9zzqsN.jpg','2023-09-18 11:45:52','2023-09-18 11:45:52'),(6,4,'Degree','1995','BIuris','documents/9UuNtkwXIorr3iezbvMaHBRXqNT82t0g24pNi8H5.pdf','2023-09-19 07:33:11','2023-09-19 07:33:11'),(7,4,'Degree','1999','LLB','documents/s3fOQtjcsCaDi17FB6qZG7qB5hgRQNTHbGttHr4z.jpg','2023-09-19 07:33:11','2023-09-19 07:33:11'),(8,4,'Degree','2002','LLM','documents/wgC6qgXFtnNq30QAqpDKpPzglEPvd9k8RQwQxUrO.pdf','2023-09-19 07:33:11','2023-09-19 07:33:11');
/*!40000 ALTER TABLE `education` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `executors`
--

DROP TABLE IF EXISTS `executors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `executors` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `exec_type` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `middle_name` varchar(255) DEFAULT NULL,
  `surname` varchar(255) NOT NULL,
  `id_number` varchar(255) NOT NULL,
  `relation` varchar(255) NOT NULL,
  `specify_relation` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `executors`
--

LOCK TABLES `executors` WRITE;
/*!40000 ALTER TABLE `executors` DISABLE KEYS */;
INSERT INTO `executors` (`id`, `user_id`, `exec_type`, `first_name`, `middle_name`, `surname`, `id_number`, `relation`, `specify_relation`, `created_at`, `updated_at`) VALUES (1,1,'main_exec','Nelson',NULL,'Ndlovu','NP1234567','Brother',NULL,'2023-09-18 11:46:04','2023-09-18 11:46:04'),(2,2,'main_exec','test','test','test','8202170001089','Sister',NULL,'2023-09-18 17:38:27','2023-09-18 17:38:27'),(3,2,'sub_exec','test','test','test','8202170001089','Cousin',NULL,'2023-09-18 17:38:41','2023-09-18 17:38:41'),(8,3,'main_exec','Rian',NULL,'Lock','84753465','Husband',NULL,'2023-09-19 12:42:26','2023-09-19 12:42:26'),(12,4,'main_exec','Pierre',NULL,'van Wyk','5011095121085','Other','Attorney','2023-10-30 05:41:26','2023-10-30 05:41:26');
/*!40000 ALTER TABLE `executors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `expenses`
--

DROP TABLE IF EXISTS `expenses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `expenses` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `family_lender` varchar(255) DEFAULT NULL,
  `registered_lender` varchar(255) DEFAULT NULL,
  `lender` varchar(255) DEFAULT NULL,
  `term` varchar(255) DEFAULT NULL,
  `rate` varchar(255) DEFAULT NULL,
  `document` varchar(255) DEFAULT NULL,
  `payment_date` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `expenses`
--

LOCK TABLES `expenses` WRITE;
/*!40000 ALTER TABLE `expenses` DISABLE KEYS */;
INSERT INTO `expenses` (`id`, `user_id`, `type`, `description`, `amount`, `family_lender`, `registered_lender`, `lender`, `term`, `rate`, `document`, `payment_date`, `created_at`, `updated_at`) VALUES (1,1,'Communication','Cell phone bill','5000',NULL,NULL,NULL,'monthly',NULL,'document/XNtLBc4AuLwW6mD6etG4Tt5csrkYQiZaCDUcTfoP.pdf','4','2023-09-18 11:36:06','2023-09-18 11:36:06'),(2,1,'Education','Yatal','5000',NULL,NULL,NULL,'monthly',NULL,'document/hRr8jo1dHNxiyJaZOjl2Mu7pKjG5AHvH5OM6FbGb.pdf','4','2023-09-18 11:36:29','2023-09-18 11:36:29'),(3,1,'loan','Loan staff','5000','yes','yes','SIS','monthly',NULL,'document/4KEZjUpp0ah9Qdq8WtsgHvmoudnmpjxJqogiLECK.pdf','4','2023-09-18 11:37:14','2023-09-18 11:37:14'),(4,1,'loan','Loan 2','5000','yes','yes','Helen','monthly',NULL,'document/25LYVzPBuxaLzvRa05CGL9Uay7lph4CmBGMie3Zn.pdf','4','2023-09-18 11:37:39','2023-09-18 11:37:39'),(5,1,'debt','Cell phone bill','5000',NULL,NULL,NULL,'monthly',NULL,'document/4ohbNJeRDOTgVpNFgM1nHWjCHlMlA75zW9XMJGQS.pdf',NULL,'2023-09-18 11:39:07','2023-09-18 11:39:07'),(6,3,'Donations','ajshjbd','10000',NULL,NULL,NULL,'monthly',NULL,'document/Y0CCay9mqtup6cT4or5R06sDZj0WBd99iHx0hGOS.png',NULL,'2023-09-18 12:03:17','2023-09-18 12:03:17'),(7,3,'loan','ajshjbd','1000000','yes','yes','Name','monthly',NULL,'document/3XAu7LDQIdTBMCpaL89iLkkQ6gXFwsEClBQGm6c0.jpg','4','2023-09-18 12:04:43','2023-09-18 12:04:43'),(9,3,'debt','Loan','19000',NULL,NULL,NULL,'monthly',NULL,'document/Y4PhlkirCR5esPNbMGZlkCYkt59xmo9RE60h8tyZ.png','5','2023-09-18 12:11:43','2023-09-18 12:11:43'),(10,1,'debt','Test','5000',NULL,NULL,NULL,'monthly',NULL,'document/o0ZDOhxQLUp9z3W5ihgPJ1mvMcUP9gT7tbfaM1rf.pdf',NULL,'2023-09-18 12:29:21','2023-09-18 12:29:21'),(11,2,'Rent, Levies & Licenses','rent','1500.00',NULL,NULL,NULL,'weekly','1000000000','document/dquCamJjlZNTfATRab6fEPQ3wvWT62QJduEQv2ez.jpg','31','2023-09-18 17:12:23','2023-09-18 17:12:23'),(12,2,'loan','test','5000','no','yes','test','monthly','100000000','document/rtS9QumjGdkv1H5CsbqkhfWHGbzozHqZZXxLsGLD.jpg','32','2023-09-18 17:13:57','2023-09-18 17:13:57'),(13,2,'debt','test','1000.00',NULL,NULL,NULL,'weekly','1000000000','document/lTt8Ss8yAXKBeoX9cnu6H3HYXkyYEjidhw4i81rM.pdf','36','2023-09-18 17:18:07','2023-09-18 17:18:07'),(14,4,'Communication','Cellphone','1800.00',NULL,NULL,NULL,'daily','0','document/w5AoyNCQnSZGoyZyTmKBTVcwt01f0tJndZGgynrk.jpg','end of week','2023-09-19 09:46:48','2023-09-19 09:46:48'),(15,4,'Donations','Church','500',NULL,NULL,NULL,'daily','0','document/hhwMBhQf1b274d8FOhBQ17O61P2Je782bqgFeq5c.pdf','every day','2023-09-19 09:47:21','2023-09-19 09:49:58'),(16,4,'Education','Schoolfees','500',NULL,NULL,NULL,'monthly','0','document/EGOIHz8O59Sd4RwLccHRV4Iq1Stc4wy2qojHZ3bv.pdf','end of month','2023-09-19 09:48:25','2023-09-19 09:48:25'),(17,4,'Donations','Trade Union','100',NULL,NULL,NULL,'monthly','0','document/xju31gKETPG6vqr2mgfgpYIuqwobE8FfIraqRH7o.jpg','end of month','2023-09-19 09:49:33','2023-09-19 09:49:33'),(18,4,'Education','Schoolfees','2000',NULL,NULL,NULL,'quarterly',NULL,'document/2t149oAVmM8yg7r0Umh8Ra27sBki3Qfatnwx9mgg.jpg','last day of month','2023-09-19 09:50:45','2023-09-19 09:50:45'),(19,4,'Entertainment & Subscriptions','DSTV','1800.00',NULL,NULL,NULL,'yearly',NULL,'document/RGeFWCKEo1V6wCNnMiLSCwYmdUa6AEGRJQcTQwVk.jpg','end of year','2023-09-19 09:51:25','2023-09-19 09:51:25'),(20,4,'Household','Meat','3000',NULL,NULL,NULL,'once-off','0','document/WTe8GvmqpoeGoSPyWCu73HMmWxklWnRcFW4Vy0EH.pdf',NULL,'2023-09-19 09:52:29','2023-09-19 09:52:29'),(21,4,'Personal','Hairdresser','1500',NULL,NULL,NULL,'monthly','0','document/KNGF659yuIYb1yhfZ3Abq9HQZyaMsd9FGaT7bvn7.jpg','1st of the month','2023-09-19 09:53:19','2023-09-19 09:53:19'),(22,4,'Professional Services','Accountant','3000',NULL,NULL,NULL,'weekly','0','document/oKyUlMpjfH1Em7szvaS6ToYiBiUJ3kzAkwitLNDT.jpg','last day of week','2023-09-19 09:53:59','2023-09-19 09:55:23'),(23,4,'Rent, Levies & Licenses','Rent','6000.00',NULL,NULL,NULL,'monthly',NULL,'document/io5LzZksTSxIEgWPQMoHKH202altCZdm8FfueCXL.pdf','end of month','2023-09-19 09:54:34','2023-09-19 09:54:34'),(24,4,'Other','Funiture','5000.00',NULL,NULL,NULL,'once-off','4','document/f5x1AQUNluOYX1EX0ZtjrAXgzTPBgJR7w3mLF0qT.pdf','end of year','2023-09-19 09:56:16','2023-09-19 09:56:16'),(25,4,'loan','Personal','5000.00','yes','no','Sannie de Beer','weekly',NULL,'document/K47kAhn7i2vDD4VcLuqGmdgGsyq2cVdCgMNh0X9u.pdf','end of week','2023-09-19 10:02:07','2023-09-19 10:02:07'),(26,4,'loan','Study','5000.00','no','no','ABC Loans','monthly',NULL,'document/AHnxYk8uAO7mMt34LBKLGhInWdtKDqberJhr436F.jpg','end of month','2023-09-19 10:03:19','2023-09-19 10:03:19'),(27,4,'loan','House Renovations','4000','yes','yes','CDE Loans','quarterly','0','document/AcZoEdNE5a9QquwnwhSJk6pVdFQ8b1uf2tuFsA3E.jpg','end of week','2023-09-19 10:04:09','2023-09-19 10:05:05'),(28,4,'loan','Holiday','6000.00','yes','no','ABC Loans','yearly',NULL,'document/fm5WldKosBfSOUnxDkH8usq1CSsbvTlDbA1J7pvQ.pdf','end of year','2023-09-19 10:06:13','2023-09-19 10:06:13'),(29,4,'debt','Absa Credit Card','6000.00',NULL,NULL,NULL,'daily',NULL,'document/GWnxB7BEIXnc1czGBSXQ43EgbzSOuNi7MJTZwoIY.pdf','every day','2023-09-19 10:11:36','2023-09-19 10:11:36'),(30,4,'debt','FNB Home Bond','1500',NULL,NULL,NULL,'weekly','0','document/4NZZGM2e6vtMOWeEmZYRN1d6SrnQDSqYv41UPjb0.jpg','end of each week','2023-09-19 10:12:23','2023-09-19 10:12:23'),(31,4,'debt','ABC Accountants','20000.00',NULL,NULL,NULL,'monthly','0','document/NlJ3rGynbTTodeYHqBMASTLWbdyF8fArLEFSqjLN.jpg','end of month','2023-09-19 10:13:01','2023-09-19 10:13:58'),(32,4,'debt','ABS Shares','5000.00',NULL,NULL,NULL,'quarterly','0','document/qCX8GRVHvNDTx05NVLBl0i02EBvEGau1ENNnP2F2.pdf','end of last month','2023-09-19 10:13:36','2023-09-19 10:13:36'),(33,4,'debt','CDE Furniture','6000.00',NULL,NULL,NULL,'yearly','5','document/1fLt7B7x0IvFJuUgWqm7H9yWdjQidZIcfMHR8W9I.pdf','31st of Dec','2023-09-19 10:14:51','2023-09-19 10:14:51'),(34,4,'debt','Russels','20000.00',NULL,NULL,NULL,'once-off','3','document/MsmVzcOsadmLGvWcIwpriafDL3cWDWsIaGRondza.jpg','immediately','2023-09-19 10:15:32','2023-09-19 10:15:32'),(35,71,'Communication','Cellphone','1000',NULL,NULL,NULL,'monthly',NULL,NULL,NULL,'2023-10-27 10:54:45','2023-10-27 10:54:45');
/*!40000 ALTER TABLE `expenses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) NOT NULL,
  `connection` text NOT NULL,
  `queue` text NOT NULL,
  `payload` longtext NOT NULL,
  `exception` longtext NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `families`
--

DROP TABLE IF EXISTS `families`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `families` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `surname` varchar(255) NOT NULL,
  `dob` date NOT NULL,
  `dod` date DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `id_number` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `relation` varchar(255) DEFAULT NULL,
  `pids` int(11) DEFAULT NULL,
  `mid` int(11) DEFAULT NULL,
  `fid` int(11) DEFAULT NULL,
  `show_on_tree` int(11) DEFAULT NULL,
  `color` varchar(255) DEFAULT NULL,
  `img` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `families`
--

LOCK TABLES `families` WRITE;
/*!40000 ALTER TABLE `families` DISABLE KEYS */;
/*!40000 ALTER TABLE `families` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `funeral_covers`
--

DROP TABLE IF EXISTS `funeral_covers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `funeral_covers` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `policy_number` varchar(255) NOT NULL,
  `policy_holder` varchar(255) NOT NULL,
  `company` varchar(255) NOT NULL,
  `coverage_amount` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `funeral_covers`
--

LOCK TABLES `funeral_covers` WRITE;
/*!40000 ALTER TABLE `funeral_covers` DISABLE KEYS */;
INSERT INTO `funeral_covers` (`id`, `user_id`, `policy_number`, `policy_holder`, `company`, `coverage_amount`, `created_at`, `updated_at`) VALUES (1,1,'DRD123456','Wilson','Faley','4,000,000','2023-09-18 11:43:59','2023-09-18 11:43:59'),(2,2,'01234578','Quintus','test','15,000','2023-09-18 17:32:28','2023-09-18 17:32:28'),(4,4,'12343','Susandra','Avbob','2,000,000','2023-09-19 13:43:26','2023-09-19 13:43:26'),(5,71,'000000','Cherene','Avbob','000,000,000','2023-10-27 10:58:13','2023-10-27 10:58:13');
/*!40000 ALTER TABLE `funeral_covers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `funerals`
--

DROP TABLE IF EXISTS `funerals`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `funerals` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `policy_details` text DEFAULT NULL,
  `memorials_service` text DEFAULT NULL,
  `cremation_details` text DEFAULT NULL,
  `parlor_details` text DEFAULT NULL,
  `requests` text DEFAULT NULL,
  `obituary` text DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `funerals`
--

LOCK TABLES `funerals` WRITE;
/*!40000 ALTER TABLE `funerals` DISABLE KEYS */;
INSERT INTO `funerals` (`id`, `user_id`, `policy_details`, `memorials_service`, `cremation_details`, `parlor_details`, `requests`, `obituary`, `created_at`, `updated_at`) VALUES (1,1,NULL,'Wherever you want ','In the earn','Faley','Bury the earn','Lorem ipsum occaecat non excepteur enim ad in magna pariatur ut nostrud aliquip velit. Do ullamco tempor occaecat qui laboris occaecat esse velit commodo. Nulla tempor voluptate consectetur nulla exercitation dolore do ullamco reprehenderit officia. Sit nulla consectetur esse culpa ea id irure in aute deserunt minim sunt.','2023-09-18 11:44:49','2023-09-18 11:44:49'),(2,2,NULL,'test','0121231234 Test','test','test','test bbbbb','2023-09-18 17:33:28','2023-09-18 17:33:36'),(3,4,NULL,'By die Kerk','Geen cremation','Avbob','geen','Hier is my Obituary.....','2023-09-19 08:51:40','2023-09-19 08:51:40');
/*!40000 ALTER TABLE `funerals` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `funeralwishes`
--

DROP TABLE IF EXISTS `funeralwishes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `funeralwishes` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `organ_donor` varchar(255) NOT NULL,
  `cremated` varchar(255) NOT NULL,
  `special_wishes` text DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `berried` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `funeralwishes`
--

LOCK TABLES `funeralwishes` WRITE;
/*!40000 ALTER TABLE `funeralwishes` DISABLE KEYS */;
INSERT INTO `funeralwishes` (`id`, `user_id`, `organ_donor`, `cremated`, `special_wishes`, `created_at`, `updated_at`, `berried`) VALUES (1,1,'yes','yes',NULL,'2023-09-18 11:48:58','2023-09-18 11:48:58',NULL),(2,2,'no','yes',NULL,'2023-09-18 17:40:29','2023-09-18 17:40:29',NULL),(4,4,'yes','yes',NULL,'2023-10-30 05:43:52','2023-10-30 05:43:52',NULL);
/*!40000 ALTER TABLE `funeralwishes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `guardians`
--

DROP TABLE IF EXISTS `guardians`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `guardians` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `middle_name` varchar(255) DEFAULT NULL,
  `surname` varchar(255) NOT NULL,
  `id_number` varchar(255) DEFAULT NULL,
  `passport_number` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `guardians`
--

LOCK TABLES `guardians` WRITE;
/*!40000 ALTER TABLE `guardians` DISABLE KEYS */;
/*!40000 ALTER TABLE `guardians` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `individual_beneficiaries`
--

DROP TABLE IF EXISTS `individual_beneficiaries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `individual_beneficiaries` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `inheritance_id` int(11) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `middle_name` varchar(255) DEFAULT NULL,
  `surname` varchar(255) NOT NULL,
  `id_number` varchar(255) NOT NULL,
  `relation` varchar(255) DEFAULT NULL,
  `specify_relation` varchar(255) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `percentage` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `individual_beneficiaries`
--

LOCK TABLES `individual_beneficiaries` WRITE;
/*!40000 ALTER TABLE `individual_beneficiaries` DISABLE KEYS */;
INSERT INTO `individual_beneficiaries` (`id`, `user_id`, `inheritance_id`, `first_name`, `middle_name`, `surname`, `id_number`, `relation`, `specify_relation`, `description`, `value`, `percentage`, `created_at`, `updated_at`) VALUES (1,1,1,'Singlette','','Ndlovu','2009-12-25','Nephew','','R3000000',NULL,NULL,'2023-09-18 11:47:38','2023-09-18 11:47:38'),(2,1,2,'Owen','Mandla','Ndlovu','2016-07-27','Son','',NULL,NULL,'','2023-09-18 11:48:22','2023-09-18 11:48:22'),(3,1,2,'Haley','Ntombi','Ndlovu','2021-10-19','Daughter','',NULL,NULL,'','2023-09-18 11:48:22','2023-09-18 11:48:22'),(4,1,3,'Belinda','Ntombiyomusa','Mabuya','1985-12-30','Wife','',NULL,NULL,'','2023-09-18 11:48:52','2023-09-18 11:48:52'),(5,2,4,'test','test','test','2001-01-10','Cousin','',NULL,NULL,'0','2023-09-18 17:40:13','2023-09-18 17:40:13'),(9,3,10,'Rian','','Lock','1990-02-03','Husband','','R4000',NULL,NULL,'2023-09-19 12:43:18','2023-09-19 12:43:18'),(13,3,15,'Steph',NULL,'Dylan','1993-03-02','Aunt','','R2000',NULL,NULL,'2023-09-19 12:45:26','2023-09-19 12:45:45'),(14,3,17,'Bob','','Dylan','1993-02-03','Brother','',NULL,NULL,'','2023-09-19 12:47:18','2023-09-19 12:47:18'),(22,4,32,'Pierre','','van Wyk','5011095121085','Husband','','Toyota Corolla 1999 model',NULL,NULL,'2023-10-30 05:43:06','2023-10-30 05:43:06'),(23,4,33,'Louis','','Claassens','2004-11-23','Son','',NULL,NULL,'','2023-10-30 05:43:46','2023-10-30 05:43:46');
/*!40000 ALTER TABLE `individual_beneficiaries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `info_graphics`
--

DROP TABLE IF EXISTS `info_graphics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `info_graphics` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `stage` varchar(255) NOT NULL,
  `path` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL DEFAULT 'image',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `info_graphics`
--

LOCK TABLES `info_graphics` WRITE;
/*!40000 ALTER TABLE `info_graphics` DISABLE KEYS */;
INSERT INTO `info_graphics` (`id`, `title`, `stage`, `path`, `type`, `created_at`, `updated_at`) VALUES (1,'What to expect during the wrapping up of a deceased estate','stage2','infographics/stage2/1/What to expect during the wrapping up of a deceased estate.jpg','image','2023-09-18 10:53:38','2023-09-18 10:53:38'),(2,'Explanations of common terms relating to Wills','stage2','infographics/stage2/1/Explanations of common terms relating to Wills.jpg','image','2023-09-18 10:53:38','2023-09-18 10:53:38'),(3,'What is a deceased estate','stage2','infographics/stage2/1/What is a deceased estate.jpg','image','2023-09-18 10:53:38','2023-09-18 10:53:38'),(4,'South African legal requirements for a Will','stage2','infographics/stage2/1/South African legal requirements for a Will.jpg','image','2023-09-18 10:53:38','2023-09-18 10:53:38'),(5,'Navigating situations of an invalid Will','stage2','infographics/stage2/1/Navigating situations of an invalid Will.jpg','image','2023-09-18 10:53:38','2023-09-18 10:53:38'),(6,'Ensure (liquidity in your estate) that there is enough cash to pay your deceased estates costs and debts','stage2','infographics/stage2/1/Ensure liquidity of your estate at the time of your death.jpg','image','2023-09-18 10:53:38','2023-09-18 10:53:38'),(7,'Calculating the cash flow of your deceased estate','stage2','infographics/stage2/1/Calculating the cash flow of your deceased estate.jpg','image','2023-09-18 10:53:38','2023-09-18 10:53:38'),(8,'How is Estate Duty calculated','stage2','infographics/stage2/1/How is estate duty calculated.jpg','image','2023-09-18 10:53:38','2023-09-18 10:53:38'),(9,'What happens if I disinherit my spouse or child','Stage2','infographics/stage2/spouse/What happens if I disinherit my spouse or child.jpg','image','2023-09-18 10:53:38','2023-09-18 10:53:38'),(10,'Who qualifies as a spouse for a maintenance claim','Stage2','infographics/stage2/spouse/Who qualifies as a spouse for a maintenance claim.jpg','image','2023-09-18 10:53:38','2023-09-18 10:53:38'),(11,'How to prevent a possible maintenance claim','Stage2','infographics/stage2/spouse/How to prevent a possible maintenance claim.jpg','image','2023-09-18 10:53:38','2023-09-18 10:53:38'),(12,'Reporting a deceased estate to the Master: What you need to know','stage2','infographics/stage2/executor/Reporting a deceased estate to the Master: What you need to know.jpg','image','2023-09-18 10:53:38','2023-09-18 10:53:38'),(13,'Understanding the Executors responsibilities - A short guide.jpg','Stage2','infographics/stage2/executor/Understanding the Executors responsibilities - A short guide.jpg','image','2023-09-18 10:53:38','2023-09-18 10:53:38'),(14,'What does an Executor do - A short guide','Stage2','infographics/stage2/executor/What does an Executor do - A short guide.jpg','image','2023-09-18 10:53:38','2023-09-18 10:53:38'),(15,'Types of Estate Administrators: A comparison','Stage2','infographics/stage2/executor/Types of estate administrators: A comparison.jpg','image','2023-09-18 10:53:38','2023-09-18 10:53:38'),(16,'What is the purpose of the appointed guardian','Stage2','infographics/stage2/guardian/What is the purpose of the appointed guardian.jpg','image','2023-09-18 10:53:38','2023-09-18 10:53:38'),(17,'What is the Guardian Fund: Relating to the cash inheritance of a minor beneficiary','Stage2','infographics/stage2/guardian/What is the Guardian Fund: Relating to the cash inheritance of a minor beneficiary.jpg','image','2023-09-18 10:53:38','2023-09-18 10:53:38'),(18,'Who is the natural guardian of a minor child','Stage2','infographics/stage2/guardian/Who is the natural guardian of a minor child.jpg','image','2023-09-18 10:53:38','2023-09-18 10:53:38'),(19,'Legal guardianship of minors: Who has the sole authority','Stage2','infographics/stage2/guardian/Legal guardianship of minors: Who has the sole authority.jpg','image','2023-09-18 10:53:38','2023-09-18 10:53:38'),(20,'Will the guardian administer the inheritance of my minor child','Stage2','infographics/stage2/guardian/Will the guardian administer the inheritance of my minor child.jpg','image','2023-09-18 10:53:38','2023-09-18 10:53:38'),(21,'Procedures at the Guardian Fund','Stage2','infographics/stage2/guardian/Procedures at the Guardian Fund.jpg','image','2023-09-18 10:53:38','2023-09-18 10:53:38'),(22,'Examples of items that can be bequeathed','Stage2','infographics/interview/leave_possesions/Examples of items that can be bequeathed.jpg','image','2023-09-18 10:53:38','2023-09-18 10:53:38'),(23,'Can you say no to an inheritance? - A short guide','Stage2','infographics/stage2/5/Can you say no to an inheritance - A short guide.jpg','image','2023-09-18 10:53:38','2023-09-18 10:53:38'),(24,'Specified inheritance vs entire estate: What is the difference','Stage2','infographics/stage2/remainder_inheritance/Specified inheritance vs entire estate: What is the difference.jpg','image','2023-09-18 10:53:38','2023-09-18 10:53:38'),(25,'Specified inheritance: A guide to what it means','Stage2','infographics/stage2/remainder_inheritance/Specified inheritance: A guide to what it means.jpg','image','2023-09-18 10:53:38','2023-09-18 10:53:38'),(26,'Inheritance of entire estate','Stage2','infographics/stage2/remainder_inheritance/Inheritance of an entire estate.jpg','image','2023-09-18 10:53:38','2023-09-18 10:53:38'),(27,'Simultaneous Death and inheritance: What you need to know','Stage2','infographics/stage2/simultaneous_death/Simultaneous Death and inheritance: What you need to know.jpg','image','2023-09-18 10:53:38','2023-09-18 10:53:38'),(28,'Consequences when a Testamentary Trust is not created','Stage2','infographics/stage2/testamentary_trust/Consequences when a Testamentary Trust is not created.jpg','image','2023-09-18 10:53:38','2023-09-18 10:53:38'),(29,'Difference between a Testamentary Trust and a living trust','Stage2','infographics/stage2/testamentary_trust/Difference between a Testamentary Trust and a living trust.jpg','image','2023-09-18 10:53:38','2023-09-18 10:53:38'),(30,'The benefits of a Testamentary Trust for a child','Stage2','infographics/stage2/testamentary_trust/The benefits of a Testamentary Trust for a child.jpg','image','2023-09-18 10:53:38','2023-09-18 10:53:38'),(31,'How a Testamentary Trust benefits major beneficiaries','stage2','infographics/stage2/testamentary_trust/How a Testamentary Trust benefits major beneficiaries.jpg','image','2023-09-18 10:53:38','2023-09-18 10:53:38'),(32,'When does a Testamentary Trust come into operation','Stage2','infographics/stage2/testamentary_trust/When does a Testamentary Trust come into operation.jpg','image','2023-09-18 10:53:38','2023-09-18 10:53:38'),(33,'Understanding the role of a trustee in a Testamentary Trust','Stage2','infographics/stage2/testamentary_trust/Understanding the role of a trustee in a Testamentary Trust.jpg','image','2023-09-18 10:53:38','2023-09-18 10:53:38'),(34,'Who may sign as a witness - A short guide','Stage2','infographics/stage2/witness/Who may sign as a witness - A short guide.jpg','image','2023-09-18 10:53:38','2023-09-18 10:53:38'),(35,'Organ and tissue donation: What does it involve','Stage2','infographics/stage2/funeral_wishes/Organ and tissue donation: What does it involve.jpg','image','2023-09-18 10:53:38','2023-09-18 10:53:38'),(36,'Cremation or burial: Understanding the meaning of funeral wishes','Stage2','infographics/stage2/funeral_wishes/Cremation or burial: Understanding the meaning of funeral wishes.jpg','image','2023-09-18 10:53:38','2023-09-18 10:53:38'),(37,'Cremation or burial procedures before arranging for funeral','Stage2','infographics/stage2/funeral_wishes/Cremation or burial procedures before arranging for funeral.jpg','image','2023-09-18 10:53:38','2023-09-18 10:53:38'),(38,'Understanding legal competence in making a Will','Stage4','infographics/stage2/profile/Understanding legal competence in making a Will.jpg','image','2023-09-18 10:53:38','2023-09-18 10:53:38'),(39,'How to apply for funds from the Guardian Fund','Stage','vids/How to apply for funds from the Guardian Fund.mp4','video','2023-09-18 10:53:38','2023-09-18 10:53:38'),(40,'No original Will or only a copy found','Stage','vids/No original Will or only a copy found.mp4','video','2023-09-18 10:53:38','2023-09-18 10:53:38'),(41,'South African legal requirements for a valid Will','Stage','vids/South African legal requirements for a valid Will.mp4','video','2023-09-18 10:53:38','2023-09-18 10:53:38'),(42,'What is a Will','Stage','vids/What is a Will.mp4','video','2023-09-18 10:53:38','2023-09-18 10:53:38'),(43,'What is the difference between a guardian of a minor and the Guardian Fund','Stage','vids/What is the difference between a guardian of a minor and the Guardian Fund.mp4','video','2023-09-18 10:53:38','2023-09-18 10:53:38'),(44,'What is the role of the appointed guardian','Stage','vids/What is the role of the appointed guardian.mp4','video','2023-09-18 10:53:38','2023-09-18 10:53:38'),(45,'When does the nomination of a guardian take effect','Stage','vids/When does the nomination of a guardian take effect.mp4','video','2023-09-18 10:53:38','2023-09-18 10:53:38'),(46,'Who may not inherit as a beneficiary','Stage','vids/Who may not inherit as a beneficiary.mp4','video','2023-09-18 10:53:38','2023-09-18 10:53:38'),(47,'Will the guardian administer my minor child\'s details','Stage','vids/Will the guardian administer my minor child\'s details.mp4','video','2023-09-18 10:53:38','2023-09-18 10:53:38');
/*!40000 ALTER TABLE `info_graphics` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `infographic_keywords`
--

DROP TABLE IF EXISTS `infographic_keywords`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `infographic_keywords` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `info_graphic_id` int(11) NOT NULL,
  `key_word_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=517 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `infographic_keywords`
--

LOCK TABLES `infographic_keywords` WRITE;
/*!40000 ALTER TABLE `infographic_keywords` DISABLE KEYS */;
INSERT INTO `infographic_keywords` (`id`, `info_graphic_id`, `key_word_id`, `created_at`, `updated_at`) VALUES (1,1,1,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(2,1,2,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(3,1,3,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(4,1,4,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(5,1,5,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(6,2,6,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(7,2,7,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(8,2,8,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(9,2,4,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(10,2,9,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(11,2,10,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(12,2,11,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(13,2,12,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(14,2,13,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(15,2,14,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(16,2,15,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(17,2,16,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(18,2,17,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(19,2,18,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(20,2,5,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(21,3,4,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(22,3,1,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(23,3,2,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(24,3,3,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(25,3,19,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(26,3,20,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(27,3,10,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(28,3,7,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(29,3,8,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(30,3,5,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(31,4,21,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(32,4,22,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(33,4,23,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(34,4,8,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(35,4,24,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(36,4,12,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(37,4,25,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(38,4,26,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(39,4,7,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(40,4,16,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(41,4,17,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(42,5,21,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(43,5,22,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(44,5,23,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(45,5,8,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(46,5,9,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(47,5,11,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(48,5,4,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(49,5,25,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(50,5,27,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(51,5,26,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(52,5,17,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(53,5,16,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(54,5,28,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(55,5,29,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(56,5,30,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(57,5,31,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(58,5,32,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(59,5,7,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(60,5,24,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(61,6,1,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(62,6,2,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(63,6,3,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(64,6,4,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(65,6,33,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(66,6,34,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(67,6,35,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(68,6,36,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(69,6,37,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(70,6,38,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(71,6,39,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(72,6,4,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(73,6,40,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(74,6,41,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(75,6,42,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(76,6,43,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(77,6,44,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(78,6,45,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(79,6,46,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(80,6,25,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(81,7,1,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(82,7,2,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(83,7,3,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(84,7,4,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(85,7,33,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(86,7,34,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(87,7,35,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(88,7,36,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(89,7,4,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(90,7,40,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(91,7,41,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(92,7,42,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(93,7,43,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(94,7,39,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(95,7,38,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(96,7,44,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(97,7,45,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(98,7,46,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(99,7,25,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(100,8,1,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(101,8,2,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(102,8,3,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(103,8,4,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(104,8,33,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(105,8,34,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(106,8,35,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(107,8,36,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(108,8,4,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(109,8,40,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(110,8,41,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(111,8,42,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(112,8,43,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(113,8,44,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(114,8,20,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(115,8,32,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(116,9,20,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(117,9,47,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(118,9,10,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(119,9,48,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(120,9,25,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(121,9,1,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(122,9,2,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(123,9,25,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(124,9,4,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(125,9,8,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(126,9,49,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(127,10,20,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(128,10,50,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(129,10,45,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(130,10,19,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(131,10,48,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(132,10,1,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(133,10,2,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(134,10,25,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(135,11,20,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(136,11,50,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(137,11,45,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(138,11,19,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(139,11,48,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(140,11,1,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(141,11,2,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(142,11,25,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(143,11,4,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(144,11,10,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(145,11,47,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(146,11,49,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(147,12,4,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(148,12,51,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(149,12,52,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(150,12,1,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(151,12,2,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(152,12,3,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(153,12,19,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(154,12,20,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(155,12,10,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(156,12,7,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(157,12,8,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(158,12,5,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(159,13,4,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(160,13,1,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(161,13,2,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(162,13,3,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(163,13,19,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(164,13,20,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(165,13,10,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(166,13,7,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(167,13,8,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(168,14,4,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(169,14,1,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(170,14,2,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(171,14,3,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(172,14,19,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(173,14,20,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(174,14,10,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(175,14,7,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(176,14,8,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(177,14,5,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(178,14,53,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(179,15,4,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(180,15,1,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(181,15,2,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(182,15,3,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(183,15,19,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(184,15,20,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(185,15,10,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(186,15,7,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(187,15,8,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(188,15,5,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(189,16,11,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(190,16,10,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(191,16,7,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(192,16,54,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(193,16,55,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(194,16,56,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(195,16,47,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(196,16,18,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(197,16,5,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(198,16,57,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(199,17,18,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(200,17,5,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(201,17,11,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(202,17,10,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(203,17,7,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(204,17,54,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(205,17,55,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(206,17,56,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(207,17,47,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(208,17,8,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(209,18,18,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(210,18,5,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(211,18,11,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(212,18,10,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(213,18,7,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(214,18,54,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(215,18,55,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(216,18,56,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(217,18,47,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(218,18,58,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(219,19,59,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(220,19,18,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(221,19,5,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(222,19,11,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(223,19,10,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(224,19,7,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(225,19,54,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(226,19,55,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(227,19,56,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(228,19,47,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(229,20,18,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(230,20,5,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(231,20,11,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(232,20,10,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(233,20,7,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(234,20,54,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(235,20,55,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(236,20,56,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(237,20,47,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(238,20,25,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(239,20,32,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(240,20,3,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(241,21,18,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(242,21,5,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(243,21,11,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(244,21,10,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(245,21,7,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(246,21,54,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(247,21,55,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(248,21,56,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(249,21,47,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(250,21,8,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(251,22,60,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(252,22,61,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(253,22,8,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(254,22,7,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(255,22,25,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(256,22,32,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(257,23,4,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(258,23,3,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(259,23,19,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(260,23,20,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(261,23,10,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(262,23,7,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(263,23,8,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(264,23,49,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(265,23,32,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(266,23,25,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(267,23,62,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(268,23,63,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(269,23,64,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(270,23,13,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(271,24,25,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(272,24,32,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(273,24,8,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(274,24,27,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(275,24,13,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(276,24,14,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(277,24,7,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(278,24,2,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(279,25,25,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(280,25,32,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(281,25,8,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(282,25,27,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(283,25,13,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(284,25,14,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(285,25,7,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(286,25,2,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(287,26,25,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(288,26,32,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(289,26,8,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(290,26,27,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(291,26,13,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(292,26,14,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(293,26,7,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(294,26,2,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(295,27,65,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(296,27,7,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(297,27,66,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(298,27,67,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(299,27,7,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(300,27,14,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(301,27,4,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(302,27,25,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(303,27,32,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(304,27,8,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(305,28,10,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(306,28,47,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(307,28,57,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(308,28,9,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(309,28,7,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(310,28,25,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(311,28,32,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(312,28,68,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(313,28,18,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(314,28,8,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(315,29,10,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(316,29,47,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(317,29,57,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(318,29,9,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(319,29,7,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(320,29,25,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(321,29,32,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(322,29,68,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(323,29,18,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(324,29,8,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(325,29,69,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(326,29,70,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(327,29,66,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(328,30,10,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(329,30,47,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(330,30,57,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(331,30,7,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(332,30,25,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(333,30,32,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(334,30,68,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(335,30,18,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(336,30,8,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(337,31,10,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(338,31,47,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(339,31,57,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(340,31,7,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(341,31,25,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(342,31,32,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(343,31,68,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(344,31,18,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(345,31,8,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(346,31,71,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(347,31,72,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(348,31,73,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(349,31,74,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(350,31,75,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(351,31,76,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(352,31,77,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(353,31,78,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(354,32,10,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(355,32,47,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(356,32,57,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(357,32,7,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(358,32,25,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(359,32,32,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(360,32,68,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(361,32,18,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(362,32,8,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(363,33,10,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(364,33,47,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(365,33,57,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(366,33,7,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(367,33,25,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(368,33,32,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(369,33,68,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(370,33,18,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(371,33,8,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(372,34,24,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(373,34,17,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(374,34,16,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(375,34,21,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(376,34,22,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(377,34,23,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(378,34,8,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(379,34,12,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(380,34,25,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(381,34,32,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(382,34,26,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(383,34,7,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(384,34,4,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(385,34,9,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(386,34,11,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(387,35,8,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(388,35,79,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(389,35,39,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(390,35,80,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(391,35,81,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(392,35,66,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(393,36,8,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(394,36,79,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(395,36,39,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(396,36,80,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(397,36,81,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(398,36,66,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(399,36,82,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(400,36,83,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(401,37,8,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(402,37,79,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(403,37,82,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(404,37,83,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(405,37,39,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(406,37,66,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(407,38,21,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(408,38,8,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(409,38,84,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(410,38,12,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(411,38,23,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(412,38,22,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(413,38,26,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(414,39,18,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(415,39,5,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(416,39,11,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(417,39,10,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(418,39,7,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(419,39,54,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(420,39,55,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(421,39,56,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(422,39,47,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(423,39,8,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(424,40,85,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(425,40,8,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(426,40,15,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(427,40,7,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(428,40,25,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(429,40,32,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(430,40,86,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(431,40,87,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(432,40,30,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(433,40,31,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(434,40,88,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(435,40,29,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(436,40,23,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(437,40,22,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(438,41,21,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(439,41,8,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(440,41,84,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(441,41,12,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(442,41,23,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(443,41,22,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(444,41,26,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(445,42,8,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(446,42,25,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(447,42,32,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(448,42,4,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(449,42,10,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(450,42,47,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(451,42,11,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(452,42,57,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(453,42,7,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(454,43,11,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(455,43,10,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(456,43,7,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(457,43,25,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(458,43,54,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(459,43,55,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(460,43,56,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(461,43,47,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(462,43,18,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(463,43,5,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(464,43,57,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(465,44,18,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(466,44,5,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(467,44,11,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(468,44,10,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(469,44,25,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(470,44,7,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(471,44,54,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(472,44,55,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(473,44,56,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(474,44,47,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(475,44,25,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(476,44,32,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(477,44,3,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(478,45,18,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(479,45,5,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(480,45,11,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(481,45,25,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(482,45,10,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(483,45,7,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(484,45,54,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(485,45,55,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(486,45,56,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(487,45,47,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(488,45,8,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(489,46,21,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(490,46,22,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(491,46,23,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(492,46,8,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(493,46,9,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(494,46,11,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(495,46,4,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(496,46,7,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(497,46,25,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(498,46,27,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(499,46,26,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(500,46,17,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(501,46,16,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(502,46,32,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(503,46,24,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(504,47,18,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(505,47,5,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(506,47,11,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(507,47,10,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(508,47,25,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(509,47,7,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(510,47,54,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(511,47,55,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(512,47,56,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(513,47,47,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(514,47,25,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(515,47,32,'2023-09-18 10:53:38','2023-09-18 10:53:38'),(516,47,3,'2023-09-18 10:53:38','2023-09-18 10:53:38');
/*!40000 ALTER TABLE `infographic_keywords` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inheritances`
--

DROP TABLE IF EXISTS `inheritances`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inheritances` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `sub_type` varchar(255) DEFAULT NULL,
  `division_type` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inheritances`
--

LOCK TABLES `inheritances` WRITE;
/*!40000 ALTER TABLE `inheritances` DISABLE KEYS */;
INSERT INTO `inheritances` (`id`, `user_id`, `type`, `sub_type`, `division_type`, `created_at`, `updated_at`) VALUES (1,1,'specific-inheritance',NULL,NULL,'2023-09-18 11:47:38','2023-09-18 11:47:38'),(2,1,'remainder-inheritance',NULL,'equal_part','2023-09-18 11:48:22','2023-09-18 11:48:22'),(3,1,'simultaneous-death','inheritance','one_beneficiery','2023-09-18 11:48:52','2023-09-18 11:48:52'),(4,2,'Inheritance',NULL,'equal_part','2023-09-18 17:40:13','2023-09-18 17:40:13'),(10,3,'specific-inheritance',NULL,NULL,'2023-09-19 12:43:18','2023-09-19 12:43:18'),(15,3,'simultaneous-death','specific-inheritance',NULL,'2023-09-19 12:45:26','2023-09-19 12:45:26'),(16,3,'simultaneous-death','remainder-inheritance','one_beneficiery','2023-09-19 12:45:30','2023-09-19 12:45:30'),(17,3,'remainder-inheritance',NULL,'one_beneficiery','2023-09-19 12:47:18','2023-09-19 12:47:18'),(32,4,'specific-inheritance',NULL,NULL,'2023-10-30 05:43:00','2023-10-30 05:43:00'),(33,4,'remainder-inheritance',NULL,'one_beneficiery','2023-10-30 05:43:46','2023-10-30 05:43:46');
/*!40000 ALTER TABLE `inheritances` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `insurances`
--

DROP TABLE IF EXISTS `insurances`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `insurances` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `insurance_type` varchar(255) NOT NULL,
  `insurer` varchar(255) NOT NULL,
  `policy_number` varchar(255) NOT NULL,
  `contact` varchar(255) DEFAULT NULL,
  `financial_advisor` varchar(255) DEFAULT NULL,
  `outstanding_debt` varchar(255) DEFAULT '0',
  `payment_terms` varchar(255) DEFAULT NULL,
  `payment_date` varchar(255) DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `insurances`
--

LOCK TABLES `insurances` WRITE;
/*!40000 ALTER TABLE `insurances` DISABLE KEYS */;
INSERT INTO `insurances` (`id`, `user_id`, `insurance_type`, `insurer`, `policy_number`, `contact`, `financial_advisor`, `outstanding_debt`, `payment_terms`, `payment_date`, `file`, `created_at`, `updated_at`) VALUES (1,1,'Pet Insurance','Bonitas','DRd23','0987654321','Dred','3000','monthly','4','document/Nd86aqAawMA11GyCmq9jj9vYF4HoL4WOXcempcfB.pdf','2023-09-18 11:38:17','2023-09-18 11:38:17'),(2,1,'Property Insurance','Outsurance','DRD45','0987654321','Dred','700','monthly','4','document/vsS1zGqundWImfME8bDz7JdWkBqXPBVqb3PoMZPU.pdf','2023-09-18 11:38:49','2023-09-18 11:38:49'),(3,3,'Pet Insurance','Name','1223455','0797467364',NULL,NULL,'monthly','5','document/MkHLoxb2ZZAteaMQuRClp0bPxBxV5jGYA11cRkFY.jpg','2023-09-18 12:06:57','2023-09-18 12:06:57'),(4,2,'Vehicle Insurance','test','01234578','0121231234 Yvonne ','test','350','monthly','35','document/hVrGB3Fr9EeFItFAPB2vrQagR8VyzPFStkc6jygE.jpg','2023-09-18 17:15:26','2023-09-18 17:15:26'),(5,4,'Pet Insurance','ABC Pet Insurance Pty Ltd','1122432435','Betsie','Makelaars','3000','daily','every day','document/7CHA2xfEAJlBqtjk5ZWAqHEZYChDbRQskbpZYjpl.pdf','2023-09-19 09:57:07','2023-09-19 09:57:07'),(6,4,'Short Term Insurance','Santam','1122432435','Betsie','Makelaars','2000','weekly','end of week','document/GHASNMnMNKZazn2yXv25TH1xWOiLN5e79UXMAQsq.pdf','2023-09-19 09:57:47','2023-09-19 09:57:47'),(7,4,'Property Insurance','Santam Property','112233','Betsie','Makelaars','3500','monthly','2nd','document/SW2JTS7BNBVayBzpSVdxqoS4LSS0kobzctJyRLwB.pdf','2023-09-19 09:58:31','2023-09-19 09:58:31'),(8,4,'Vehicle Insurance','Santam','12343','Betsie','Makelaars','2000','quarterly','end of last month','document/Va08FDykaFASFvztfOIC2Z4JI7mMLUi27LVkj89h.pdf','2023-09-19 09:59:18','2023-09-19 09:59:18'),(9,4,'Short Term Insurance','Old Mutual','1122432435','Sannie','ABC Makelaars','3400','yearly',NULL,'document/CzhX0crdq1Sjnpn6WIvePksD5YEWcdP0ulcHjwA6.pdf','2023-09-19 10:00:19','2023-09-19 10:00:19'),(10,4,'Property Insurance','Discovery','12343','Betsie','CDE Makelaars','1200','once-off','immediately','document/k6NZRAACszB7zLJa78ddNlwqBcA9YWhSMXQVQJW1.jpg','2023-09-19 10:01:05','2023-09-19 10:01:05');
/*!40000 ALTER TABLE `insurances` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `interviews`
--

DROP TABLE IF EXISTS `interviews`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `interviews` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `have_property_outside_sa` varchar(255) DEFAULT NULL,
  `read_and_write_capability` varchar(255) DEFAULT NULL,
  `leave_possesions_to_icapitated_persons` varchar(255) DEFAULT NULL,
  `exclude_persons_from_will` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `interviews`
--

LOCK TABLES `interviews` WRITE;
/*!40000 ALTER TABLE `interviews` DISABLE KEYS */;
INSERT INTO `interviews` (`id`, `user_id`, `have_property_outside_sa`, `read_and_write_capability`, `leave_possesions_to_icapitated_persons`, `exclude_persons_from_will`, `created_at`, `updated_at`) VALUES (1,1,'No','Yes','No','No','2023-09-18 11:08:18','2023-09-18 11:45:25'),(2,2,'No','Yes','No','No','2023-09-18 11:21:46','2023-09-18 17:36:07'),(3,3,'No','Yes','Yes','No','2023-09-18 11:35:27','2023-09-19 05:15:48'),(4,4,'No','Yes','No','No','2023-09-19 06:48:44','2023-10-30 05:40:31'),(5,9,'No','Yes','Yes','Yes','2023-09-20 12:49:13','2023-11-22 03:15:51'),(6,10,'No','Yes',NULL,NULL,'2023-09-20 15:34:00','2023-09-20 15:34:00'),(7,7,'No','Yes',NULL,NULL,'2023-09-21 07:54:00','2023-09-21 07:54:00'),(8,11,'No','Yes',NULL,NULL,'2023-09-21 08:39:25','2023-11-21 11:43:36'),(9,12,'No','Yes',NULL,NULL,'2023-09-21 09:24:20','2023-09-21 09:24:20'),(10,14,'No','Yes',NULL,NULL,'2023-09-21 13:39:14','2023-09-21 13:39:14'),(11,15,'No','Yes',NULL,NULL,'2023-09-22 09:30:16','2023-09-22 09:30:16'),(12,18,'No','Yes',NULL,NULL,'2023-09-28 08:06:29','2023-09-28 08:06:29'),(13,20,'No','Yes',NULL,NULL,'2023-10-04 05:41:04','2023-10-04 05:41:04'),(14,21,'No','Yes',NULL,NULL,'2023-10-04 10:12:08','2023-10-04 10:12:08'),(15,22,'No','Yes',NULL,NULL,'2023-10-10 10:06:12','2023-10-10 10:06:12'),(16,23,'No','Yes',NULL,NULL,'2023-10-11 09:10:23','2023-10-11 09:10:23'),(17,24,'No','Yes',NULL,NULL,'2023-10-11 10:35:07','2023-10-11 10:35:07'),(18,25,'No','Yes',NULL,NULL,'2023-10-12 12:21:26','2023-10-12 12:21:26'),(19,26,'No','Yes',NULL,NULL,'2023-10-12 14:01:37','2023-10-12 14:01:37'),(20,27,'No','Yes',NULL,NULL,'2023-10-12 14:04:38','2023-10-12 14:04:38'),(21,19,'No','Yes',NULL,NULL,'2023-10-13 05:13:34','2023-10-13 05:13:34'),(22,28,'No','Yes',NULL,NULL,'2023-10-13 09:52:06','2023-10-13 09:52:06'),(23,29,'No','Yes',NULL,NULL,'2023-10-14 07:05:34','2023-10-14 07:05:34'),(24,30,'No','Yes',NULL,NULL,'2023-10-22 18:16:17','2023-10-22 18:16:17'),(25,33,'No','Yes',NULL,NULL,'2023-10-23 07:49:40','2023-10-23 07:49:40'),(26,35,'No','Yes',NULL,NULL,'2023-10-24 02:35:28','2023-10-24 02:35:28'),(27,36,'No','Yes',NULL,NULL,'2023-10-24 04:44:03','2023-10-24 04:44:03'),(28,37,'No','Yes',NULL,NULL,'2023-10-24 05:25:00','2023-10-24 05:25:00'),(29,38,'No','Yes',NULL,NULL,'2023-10-24 05:27:34','2023-10-24 05:27:34'),(30,39,'No','Yes',NULL,NULL,'2023-10-24 05:38:34','2023-10-24 05:38:34'),(31,40,'No','Yes',NULL,NULL,'2023-10-24 16:50:27','2023-10-24 16:50:27'),(32,41,'No','Yes',NULL,NULL,'2023-10-24 17:44:25','2023-10-24 17:44:25'),(33,42,'No','Yes',NULL,NULL,'2023-10-24 17:50:56','2023-10-24 17:50:56'),(34,43,'No','Yes',NULL,NULL,'2023-10-24 17:58:30','2023-10-24 17:58:30'),(35,44,'No','Yes',NULL,NULL,'2023-10-24 18:06:40','2023-10-24 18:06:40'),(36,45,'No','Yes',NULL,NULL,'2023-10-25 05:24:45','2023-10-25 05:24:45'),(37,46,'No','Yes',NULL,NULL,'2023-10-25 05:37:40','2023-10-25 05:37:40'),(38,47,'No','Yes','Yes','No','2023-10-25 09:08:50','2023-11-01 06:32:15'),(39,48,'No','Yes',NULL,NULL,'2023-10-25 09:13:00','2023-10-25 09:13:00'),(40,49,'No','Yes',NULL,NULL,'2023-10-25 09:18:43','2023-10-25 09:18:43'),(41,50,'No','Yes',NULL,NULL,'2023-10-25 09:24:31','2023-10-25 09:24:31'),(42,51,'No','Yes',NULL,NULL,'2023-10-25 09:52:52','2023-10-25 09:52:52'),(43,53,'No','Yes',NULL,NULL,'2023-10-25 10:21:17','2023-10-25 10:21:17'),(44,54,'No','Yes',NULL,NULL,'2023-10-25 10:44:26','2023-10-25 10:44:26'),(45,16,'No','Yes',NULL,NULL,'2023-10-25 11:44:33','2023-10-25 11:44:33'),(46,55,'No','Yes',NULL,NULL,'2023-10-25 12:41:50','2023-10-25 12:41:50'),(47,56,'No','Yes',NULL,NULL,'2023-10-25 12:48:28','2023-10-25 12:48:28'),(48,57,'No','Yes',NULL,NULL,'2023-10-25 13:56:24','2023-10-25 13:56:24'),(49,58,'No','Yes',NULL,NULL,'2023-10-25 14:47:54','2023-10-25 14:47:54'),(50,59,'No','Yes',NULL,NULL,'2023-10-26 05:38:09','2023-10-26 05:38:09'),(51,60,'No','Yes',NULL,NULL,'2023-10-26 10:41:30','2023-10-26 10:41:30'),(52,61,'No','Yes',NULL,NULL,'2023-10-26 11:10:56','2023-10-26 11:10:56'),(53,62,'No','Yes',NULL,NULL,'2023-10-26 11:14:37','2023-10-26 11:14:37'),(54,63,'No','Yes',NULL,NULL,'2023-10-26 11:16:58','2023-10-26 11:16:58'),(55,64,'No','Yes',NULL,NULL,'2023-10-26 17:49:36','2023-10-26 17:49:36'),(56,65,'No','Yes',NULL,NULL,'2023-10-27 06:13:31','2023-10-27 06:13:31'),(57,66,'No','Yes',NULL,NULL,'2023-10-27 06:21:43','2023-10-27 06:21:43'),(58,67,'No','Yes',NULL,NULL,'2023-10-27 06:24:53','2023-10-27 06:24:53'),(59,68,'No','Yes',NULL,NULL,'2023-10-27 06:47:06','2023-10-27 06:47:06'),(60,69,'No','Yes',NULL,NULL,'2023-10-27 07:51:48','2023-10-27 07:51:48'),(61,70,'No','Yes',NULL,NULL,'2023-10-27 09:11:19','2023-10-27 09:11:19'),(62,71,'No','Yes',NULL,NULL,'2023-10-27 10:29:50','2023-11-01 05:21:31'),(63,72,'No','Yes',NULL,NULL,'2023-11-01 04:32:34','2023-11-01 04:32:34');
/*!40000 ALTER TABLE `interviews` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `investment_beneficiaries`
--

DROP TABLE IF EXISTS `investment_beneficiaries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `investment_beneficiaries` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `investment_id` bigint(20) unsigned DEFAULT NULL,
  `nominated_beneficiary` varchar(255) DEFAULT NULL,
  `percentage` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `investment_beneficiaries`
--

LOCK TABLES `investment_beneficiaries` WRITE;
/*!40000 ALTER TABLE `investment_beneficiaries` DISABLE KEYS */;
INSERT INTO `investment_beneficiaries` (`id`, `user_id`, `investment_id`, `nominated_beneficiary`, `percentage`, `created_at`, `updated_at`) VALUES (1,1,1,'Owen','50','2023-09-18 11:33:11','2023-09-18 11:33:11'),(2,1,1,'Haley','50','2023-09-18 11:33:11','2023-09-18 11:33:11'),(3,2,3,'test','50','2023-09-18 17:08:35','2023-09-18 17:08:35'),(4,4,4,'Koos du Plessis','50','2023-09-19 08:43:06','2023-09-19 08:43:06'),(5,4,4,'Annie du Preez','50','2023-09-19 08:43:06','2023-09-19 08:43:06'),(6,4,5,'Jannie','50','2023-09-19 08:44:30','2023-09-19 08:44:30'),(7,4,5,'Pietie','50','2023-09-19 08:44:30','2023-09-19 08:44:30'),(8,4,6,'Kosie','40','2023-09-19 08:45:37','2023-09-19 08:45:37'),(9,4,6,'Pietie','40','2023-09-19 08:45:37','2023-09-19 08:45:37'),(10,4,6,'Sarie','20','2023-09-19 08:45:37','2023-09-19 08:45:37'),(11,4,7,'Koos','50','2023-09-19 08:46:48','2023-09-19 08:46:48'),(12,4,7,'Philip','50','2023-09-19 08:46:48','2023-09-19 08:46:48'),(13,4,8,'Willem','100','2023-09-19 08:47:27','2023-09-19 08:47:27'),(14,4,9,'Jan','100','2023-09-19 08:48:07','2023-09-19 08:48:07'),(15,9,10,'joe',NULL,'2023-09-28 12:55:40','2023-09-28 12:55:40'),(16,71,11,'Jaelyn','33','2023-10-27 10:52:22','2023-10-27 10:52:22'),(17,71,11,'Declan','33','2023-10-27 10:52:22','2023-10-27 10:52:22'),(18,71,11,'Tyron','33','2023-10-27 10:52:22','2023-10-27 10:52:22');
/*!40000 ALTER TABLE `investment_beneficiaries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `investments`
--

DROP TABLE IF EXISTS `investments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `investments` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `investment_type` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  `file` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `policy_number` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `investments`
--

LOCK TABLES `investments` WRITE;
/*!40000 ALTER TABLE `investments` DISABLE KEYS */;
INSERT INTO `investments` (`id`, `user_id`, `investment_type`, `value`, `file`, `created_at`, `updated_at`, `description`, `policy_number`) VALUES (1,1,'Share Portfolio','3000000000','document/SmQKsQgGcRT6c9Qw81rx6c4ANevDjhRoqjzfMQXb.pdf','2023-09-18 11:33:11','2023-09-18 11:33:11','Cell phone bill','DD1234567'),(2,1,'Life Policy','3000000','document/0rINFPRzUj5Vhxk2aM6hu1UEUbakHQayOrvL0vi1.pdf','2023-09-18 11:33:32','2023-09-18 11:33:32','Policy','DD56'),(4,4,'Share Portfolio','1000000','document/0kdHNGBQnCcpinIyrY2D9qRrLpdX60pUtzLV5SVl.pdf','2023-09-19 08:43:06','2023-09-19 08:43:32','ABS Shares','1122432435'),(5,4,'Unit Trust','10000','document/nXiVvThiuWanekkgF4nvCntJdEk4HezGuVDFCXvI.pdf','2023-09-19 08:44:30','2023-09-19 08:44:30','GFH Unit Trust','2325464'),(6,4,'Life Policy','10000','document/v5hAb4S6lY3cMII2OhHMMESjhtq5PoAfJLVVAQQS.pdf','2023-09-19 08:45:37','2023-09-19 08:46:01','ASD Life Assurance','2342425'),(7,4,'Education Policy','1000000','document/8GtpACLdJvZTDdyDZOtHhJOqd9D2rfwxpjZ8Ex5o.pdf','2023-09-19 08:46:48','2023-09-19 08:46:48','GRT Education Pty Ltd','1122432435'),(8,4,'Retirement Annuity','1000000','document/fMIVHrYLwJnqCzsS03RyQeUjhDHYHw4NYA5VsjsG.pdf','2023-09-19 08:47:27','2023-09-19 08:47:27','Anuuity Free','12343'),(9,4,'Other','10000','document/nk9zCIln7jIg37lWdlAzug4l1yMK94JhVE7wYR6c.pdf','2023-09-19 08:48:07','2023-09-19 08:48:07','ABC Insurance','23424242'),(10,9,'Share Portfolio','200000','document/8kTUPL70vtOIxfcF3RFuFxTtn2JoaOSkbDkUMpa7.png','2023-09-28 12:55:40','2023-09-28 12:55:40','test','test'),(11,71,'Life Policy','2000000',NULL,'2023-10-27 10:52:22','2023-10-27 10:52:22','momentum','12121212');
/*!40000 ALTER TABLE `investments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_to_finds`
--

DROP TABLE IF EXISTS `item_to_finds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item_to_finds` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `item_name` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_to_finds`
--

LOCK TABLES `item_to_finds` WRITE;
/*!40000 ALTER TABLE `item_to_finds` DISABLE KEYS */;
/*!40000 ALTER TABLE `item_to_finds` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `key_words`
--

DROP TABLE IF EXISTS `key_words`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `key_words` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `word` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=89 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `key_words`
--

LOCK TABLES `key_words` WRITE;
/*!40000 ALTER TABLE `key_words` DISABLE KEYS */;
INSERT INTO `key_words` (`id`, `word`, `created_at`, `updated_at`) VALUES (1,'deceased estate','2023-09-18 10:53:38','2023-09-18 10:53:38'),(2,'estate','2023-09-18 10:53:38','2023-09-18 10:53:38'),(3,'administration','2023-09-18 10:53:38','2023-09-18 10:53:38'),(4,'executor','2023-09-18 10:53:38','2023-09-18 10:53:38'),(5,'master','2023-09-18 10:53:38','2023-09-18 10:53:38'),(6,'terms','2023-09-18 10:53:38','2023-09-18 10:53:38'),(7,'beneficiary','2023-09-18 10:53:38','2023-09-18 10:53:38'),(8,'will','2023-09-18 10:53:38','2023-09-18 10:53:38'),(9,'trustee','2023-09-18 10:53:38','2023-09-18 10:53:38'),(10,'minor','2023-09-18 10:53:38','2023-09-18 10:53:38'),(11,'guardian','2023-09-18 10:53:38','2023-09-18 10:53:38'),(12,'testator','2023-09-18 10:53:38','2023-09-18 10:53:38'),(13,'legatee','2023-09-18 10:53:38','2023-09-18 10:53:38'),(14,'heir','2023-09-18 10:53:38','2023-09-18 10:53:38'),(15,'intestate','2023-09-18 10:53:38','2023-09-18 10:53:38'),(16,'signing','2023-09-18 10:53:38','2023-09-18 10:53:38'),(17,'signature','2023-09-18 10:53:38','2023-09-18 10:53:38'),(18,'guardian fund','2023-09-18 10:53:38','2023-09-18 10:53:38'),(19,'dispute','2023-09-18 10:53:38','2023-09-18 10:53:38'),(20,'spouse','2023-09-18 10:53:38','2023-09-18 10:53:38'),(21,'formalities','2023-09-18 10:53:38','2023-09-18 10:53:38'),(22,'invalid','2023-09-18 10:53:38','2023-09-18 10:53:38'),(23,'valid','2023-09-18 10:53:38','2023-09-18 10:53:38'),(24,'witness','2023-09-18 10:53:38','2023-09-18 10:53:38'),(25,'inheritance','2023-09-18 10:53:38','2023-09-18 10:53:38'),(26,'requirement','2023-09-18 10:53:38','2023-09-18 10:53:38'),(27,'provision','2023-09-18 10:53:38','2023-09-18 10:53:38'),(28,'not signing','2023-09-18 10:53:38','2023-09-18 10:53:38'),(29,'internet','2023-09-18 10:53:38','2023-09-18 10:53:38'),(30,'electronic will','2023-09-18 10:53:38','2023-09-18 10:53:38'),(31,'computer','2023-09-18 10:53:38','2023-09-18 10:53:38'),(32,'inherit','2023-09-18 10:53:38','2023-09-18 10:53:38'),(33,'liquidity','2023-09-18 10:53:38','2023-09-18 10:53:38'),(34,'cash','2023-09-18 10:53:38','2023-09-18 10:53:38'),(35,'debts','2023-09-18 10:53:38','2023-09-18 10:53:38'),(36,'creditors','2023-09-18 10:53:38','2023-09-18 10:53:38'),(37,'administration costs','2023-09-18 10:53:38','2023-09-18 10:53:38'),(38,'fees','2023-09-18 10:53:38','2023-09-18 10:53:38'),(39,'funeral','2023-09-18 10:53:38','2023-09-18 10:53:38'),(40,'tax','2023-09-18 10:53:38','2023-09-18 10:53:38'),(41,'taxes','2023-09-18 10:53:38','2023-09-18 10:53:38'),(42,'estate duty','2023-09-18 10:53:38','2023-09-18 10:53:38'),(43,'sars','2023-09-18 10:53:38','2023-09-18 10:53:38'),(44,'bond','2023-09-18 10:53:38','2023-09-18 10:53:38'),(45,'claim','2023-09-18 10:53:38','2023-09-18 10:53:38'),(46,'overdraft','2023-09-18 10:53:38','2023-09-18 10:53:38'),(47,'child','2023-09-18 10:53:38','2023-09-18 10:53:38'),(48,'marriage','2023-09-18 10:53:38','2023-09-18 10:53:38'),(49,'disinherit','2023-09-18 10:53:38','2023-09-18 10:53:38'),(50,'maintenance','2023-09-18 10:53:38','2023-09-18 10:53:38'),(51,'procedure','2023-09-18 10:53:38','2023-09-18 10:53:38'),(52,'report','2023-09-18 10:53:38','2023-09-18 10:53:38'),(53,'duties','2023-09-18 10:53:38','2023-09-18 10:53:38'),(54,'parent','2023-09-18 10:53:38','2023-09-18 10:53:38'),(55,'mother','2023-09-18 10:53:38','2023-09-18 10:53:38'),(56,'father','2023-09-18 10:53:38','2023-09-18 10:53:38'),(57,'trust','2023-09-18 10:53:38','2023-09-18 10:53:38'),(58,'natural guardian','2023-09-18 10:53:38','2023-09-18 10:53:38'),(59,'legal guardian','2023-09-18 10:53:38','2023-09-18 10:53:38'),(60,'property','2023-09-18 10:53:38','2023-09-18 10:53:38'),(61,'possessions','2023-09-18 10:53:38','2023-09-18 10:53:38'),(62,'refuse','2023-09-18 10:53:38','2023-09-18 10:53:38'),(63,'refusal','2023-09-18 10:53:38','2023-09-18 10:53:38'),(64,'heri','2023-09-18 10:53:38','2023-09-18 10:53:38'),(65,'family obliteration','2023-09-18 10:53:38','2023-09-18 10:53:38'),(66,'death','2023-09-18 10:53:38','2023-09-18 10:53:38'),(67,'simultaneous death','2023-09-18 10:53:38','2023-09-18 10:53:38'),(68,'testamentary trust','2023-09-18 10:53:38','2023-09-18 10:53:38'),(69,'living trust','2023-09-18 10:53:38','2023-09-18 10:53:38'),(70,'lifetime','2023-09-18 10:53:38','2023-09-18 10:53:38'),(71,'major','2023-09-18 10:53:38','2023-09-18 10:53:38'),(72,'mental disabilities','2023-09-18 10:53:38','2023-09-18 10:53:38'),(73,'physical disabilities','2023-09-18 10:53:38','2023-09-18 10:53:38'),(74,'dementia','2023-09-18 10:53:38','2023-09-18 10:53:38'),(75,'alzheimer','2023-09-18 10:53:38','2023-09-18 10:53:38'),(76,'disease','2023-09-18 10:53:38','2023-09-18 10:53:38'),(77,'patient','2023-09-18 10:53:38','2023-09-18 10:53:38'),(78,'illness','2023-09-18 10:53:38','2023-09-18 10:53:38'),(79,'funeral wishes','2023-09-18 10:53:38','2023-09-18 10:53:38'),(80,'organ donor','2023-09-18 10:53:38','2023-09-18 10:53:38'),(81,'donation','2023-09-18 10:53:38','2023-09-18 10:53:38'),(82,'cremation','2023-09-18 10:53:38','2023-09-18 10:53:38'),(83,'burial','2023-09-18 10:53:38','2023-09-18 10:53:38'),(84,'legal','2023-09-18 10:53:38','2023-09-18 10:53:38'),(85,'copy','2023-09-18 10:53:38','2023-09-18 10:53:38'),(86,'original','2023-09-18 10:53:38','2023-09-18 10:53:38'),(87,'e-mail','2023-09-18 10:53:38','2023-09-18 10:53:38'),(88,'not signed','2023-09-18 10:53:38','2023-09-18 10:53:38');
/*!40000 ALTER TABLE `key_words` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mailing_lists`
--

DROP TABLE IF EXISTS `mailing_lists`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mailing_lists` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `contact_number` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mailing_lists`
--

LOCK TABLES `mailing_lists` WRITE;
/*!40000 ALTER TABLE `mailing_lists` DISABLE KEYS */;
INSERT INTO `mailing_lists` (`id`, `name`, `contact_number`, `email`, `created_at`, `updated_at`) VALUES (1,'Susandra','0788031314','info@svwlaw.co.za','2023-09-19 06:00:03','2023-09-19 06:00:03'),(2,'Susandra','van Wyk','info@deceased-estates-selfhelp-programme.co.za','2023-09-19 06:34:21','2023-09-19 06:34:21'),(3,'Susandra van Wyk','0788031314','reporting@wrapup.co.za','2023-09-19 07:13:26','2023-09-19 07:13:26'),(4,'Susandra','0788031314','info@deceased-estates-selfhelp-programme.co.za','2023-09-20 07:51:37','2023-09-20 07:51:37'),(5,'Susandra','0788031314','info@svwlaw.co.za','2023-09-20 07:53:17','2023-09-20 07:53:17'),(6,'Susandra','0788031314','info@svwlaw.co.za','2023-09-20 07:53:40','2023-09-20 07:53:40'),(7,'Steph','0749317766','steph@thinktank.co.za','2023-09-20 12:10:16','2023-09-20 12:10:16'),(8,'Steph','0749317766','steph@thinktank.co.za','2023-09-20 12:12:37','2023-09-20 12:12:37'),(9,'Susandra','0788031314','info@svwlaw.co.za','2023-10-25 10:10:52','2023-10-25 10:10:52');
/*!40000 ALTER TABLE `mailing_lists` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `medical_aid_conditions`
--

DROP TABLE IF EXISTS `medical_aid_conditions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `medical_aid_conditions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `medical_aid_id` int(11) NOT NULL,
  `condition` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `medical_aid_conditions`
--

LOCK TABLES `medical_aid_conditions` WRITE;
/*!40000 ALTER TABLE `medical_aid_conditions` DISABLE KEYS */;
INSERT INTO `medical_aid_conditions` (`id`, `user_id`, `medical_aid_id`, `condition`, `created_at`, `updated_at`) VALUES (1,1,1,'Mone','2023-09-18 11:19:55','2023-09-18 11:19:55'),(2,2,2,'Coronary Issues','2023-09-18 11:46:53','2023-09-18 11:46:53'),(4,2,3,'Mental Issues','2023-09-18 11:49:31','2023-09-18 11:49:31'),(5,2,3,'Mental fatigue','2023-09-18 11:49:51','2023-09-18 11:49:51'),(6,4,5,'none','2023-09-19 07:43:54','2023-09-19 07:43:54'),(7,4,6,'none','2023-09-19 07:43:54','2023-09-19 07:43:54');
/*!40000 ALTER TABLE `medical_aid_conditions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `medical_aids`
--

DROP TABLE IF EXISTS `medical_aids`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `medical_aids` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `reference_number` varchar(255) NOT NULL,
  `company` varchar(255) NOT NULL,
  `medical_aid_certificate` varchar(255) DEFAULT NULL,
  `id_number` varchar(255) DEFAULT NULL,
  `surname` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `policy_type` varchar(255) DEFAULT NULL,
  `conditions` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `medical_aids`
--

LOCK TABLES `medical_aids` WRITE;
/*!40000 ALTER TABLE `medical_aids` DISABLE KEYS */;
INSERT INTO `medical_aids` (`id`, `user_id`, `reference_number`, `company`, `medical_aid_certificate`, `id_number`, `surname`, `name`, `created_at`, `updated_at`, `policy_type`, `conditions`) VALUES (1,1,'DR1234567','Discovery','documents/zSBEIh8SBfsMvsbZGXimT4ajCraOyzg5evOeZB5W.pdf','CN447962','Ndlovu','wilson','2023-09-18 11:19:55','2023-09-18 11:19:55','Medical',NULL),(2,2,'MED012345','MedScheme','documents/myHad5Mh1n29ueh4nPCBzyxL01denwz1KUrGVJWk.jpg','8404025024080','van der Walt','Quintus','2023-09-18 11:46:10','2023-09-18 11:46:10','Hospital Plan',NULL),(3,2,'HED654321','HeadScheme','documents/Rdqb4u1ccygxnEMKlKMIx48yXsvv9QSpsmQ97Onk.pdf','9704043955184','Doe','Jane','2023-09-18 11:49:20','2023-09-18 11:49:20','Life Policy',NULL),(4,3,'345345645','Medihelp','documents/WmK1XkuIEEy1XWhQtCrMFVATw3isdpzwPzpXo0ho.pdf','','Rick','James','2023-09-18 12:24:12','2023-09-18 12:25:47','Plus',NULL),(5,4,'1234567','Bestmed','documents/jguLyQfayvPFbVdnrnw5tcSrLABmQhv8E6jkPuIA.jpg','5011095121085','van Wyk','Pierre','2023-09-19 07:43:54','2023-09-19 07:43:54','Hospital Plan',NULL),(6,4,'12345','ABS Medics','documents/iLObisk5yHkQKaOi5bOPgWQh1LRUzy0w0xH1rEvv.pdf','7101120212088','van Wyk','Susandra','2023-09-19 07:43:54','2023-09-19 07:43:54','Top Up',NULL),(7,71,'00000000','bestmed',NULL,'000000000000','','','2023-10-27 10:48:27','2023-10-27 10:48:27','Beat 1',NULL);
/*!40000 ALTER TABLE `medical_aids` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `memories`
--

DROP TABLE IF EXISTS `memories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `memories` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `media_type` varchar(255) NOT NULL,
  `memory_type` varchar(255) DEFAULT NULL,
  `recipient` varchar(255) NOT NULL,
  `notes` varchar(255) NOT NULL,
  `media` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `surname` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `id_number` varchar(255) DEFAULT NULL,
  `phone_number` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `memories`
--

LOCK TABLES `memories` WRITE;
/*!40000 ALTER TABLE `memories` DISABLE KEYS */;
INSERT INTO `memories` (`id`, `user_id`, `media_type`, `memory_type`, `recipient`, `notes`, `media`, `link`, `created_at`, `updated_at`, `name`, `surname`, `email`, `id_number`, `phone_number`) VALUES (1,1,'Link','','','Lorem ipsum occaecat non excepteur enim ad in magna pariatur ut nostrud aliquip velit. Do ullamco tempor occaecat qui laboris occaecat esse velit commodo. Nulla tempor voluptate consectetur nulla exercitation dolore do ullamco reprehenderit officia. Sit n',NULL,'www.link.co.za','2023-09-18 11:42:53','2023-09-18 11:42:53','Nelson','Ndlovu','np@mail.com','NP1234567','0987654321'),(2,1,'Document','','','Lorem ipsum occaecat non excepteur enim ad in magna pariatur ut nostrud aliquip velit. Do ullamco tempor occaecat qui laboris occaecat esse velit commodo. Nulla tempor voluptate consectetur nulla exercitation dolore do ullamco reprehenderit officia. Sit n','memory/viLXO3RdlEMTxVkzfH5hrH4q5cLzcizchpxQYPou.pdf',NULL,'2023-09-18 11:43:26','2023-09-18 11:43:26','Belinda','Mabuya','linda@mail.com','LN1234567','0987654321'),(3,2,'Questionnaire','','','test',NULL,NULL,'2023-09-18 17:26:39','2023-09-19 05:23:43','test','test','test@gmail.com','7776554444333','1234556777778'),(5,3,'Questionnaire','','','',NULL,NULL,'2023-09-19 05:08:36','2023-09-19 05:14:13','Name','','','','0728373829'),(12,3,'Image','','','','memory/3I9b8AGpvDsdUOcy4tCJ4DnwIFOeW2SnfCjp67g2.jpg',NULL,'2023-09-20 11:02:44','2023-09-20 11:02:44','Steph','Lock','','',''),(13,9,'Video Clip','','','testing',NULL,NULL,'2023-09-28 12:56:48','2023-10-11 10:18:44','Ty','cook','','','test'),(14,9,'Video Clip','','','test',NULL,NULL,'2023-09-28 12:57:48','2023-09-28 12:57:48','test','','','',''),(15,9,'','','','',NULL,NULL,'2023-09-28 12:57:48','2023-09-28 12:57:48','','','','',''),(16,71,'Video Clip','','','',NULL,NULL,'2023-10-27 10:56:52','2023-10-27 10:56:52','Ty','','tyroncook@gmail.com','','');
/*!40000 ALTER TABLE `memories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `memories_accesses`
--

DROP TABLE IF EXISTS `memories_accesses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `memories_accesses` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `surname` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone_number` varchar(255) DEFAULT NULL,
  `id_number` varchar(255) DEFAULT NULL,
  `message` text DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `memories_accesses`
--

LOCK TABLES `memories_accesses` WRITE;
/*!40000 ALTER TABLE `memories_accesses` DISABLE KEYS */;
/*!40000 ALTER TABLE `memories_accesses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=125 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_reset_tokens_table',1),(3,'2014_10_12_200000_add_two_factor_columns_to_users_table',1),(4,'2019_08_19_000000_create_failed_jobs_table',1),(5,'2019_12_14_000001_create_personal_access_tokens_table',1),(6,'2023_04_09_120315_create_contacts_table',1),(7,'2023_04_10_131131_create_families_table',1),(8,'2023_04_17_080808_create_spouses_table',1),(9,'2023_04_17_090042_create_place_of_works_table',1),(10,'2023_04_17_091126_create_taxes_table',1),(11,'2023_04_17_092543_create_children_table',1),(12,'2023_04_17_100918_create_item_to_finds_table',1),(13,'2023_04_17_102224_create_medical_aids_table',1),(14,'2023_04_24_055723_create_interviews_table',1),(15,'2023_04_24_144437_create_will_options_table',1),(16,'2023_05_05_055440_create_executors_table',1),(17,'2023_05_05_144356_create_will_spouses_table',1),(18,'2023_05_10_064535_create_guardians_table',1),(19,'2023_05_10_070635_create_child_guardians_table',1),(20,'2023_05_10_081515_create_trustees_table',1),(21,'2023_05_10_105419_create_inheritances_table',1),(22,'2023_05_10_105830_create_individual_beneficiaries_table',1),(23,'2023_05_10_110247_create_organisation_beneficiaries_table',1),(24,'2023_05_11_064319_create_funeralwishes_table',1),(25,'2023_05_11_074424_create_witnesses_table',1),(26,'2023_05_15_105653_create_business_wealths_table',1),(27,'2023_05_15_111633_create_business_wealth_trustees_table',1),(28,'2023_05_15_112418_create_business_wealth_members_table',1),(29,'2023_05_15_113608_create_business_wealth_debtors_table',1),(30,'2023_05_15_134100_create_assets_table',1),(31,'2023_05_16_073334_create_investments_table',1),(32,'2023_05_16_080149_create_banks_table',1),(33,'2023_05_16_083131_create_insurances_table',1),(34,'2023_05_16_091929_create_expenses_table',1),(35,'2023_05_16_101931_create_social_media_table',1),(36,'2023_05_18_070318_create_will_uploads_table',1),(37,'2023_05_23_050016_create_safe_keeps_table',1),(38,'2023_05_23_053131_create_memories_table',1),(39,'2023_05_23_054526_create_funerals_table',1),(40,'2023_05_24_075050_create_shares_table',1),(41,'2023_05_24_091148_add_more_data_to_memories_table',1),(42,'2023_05_24_110752_add_link_to_memories_table',1),(43,'2023_05_24_130011_add_link_to_social_media_table',1),(44,'2023_05_25_081836_add_file_to_insurances_table',1),(45,'2023_05_25_135659_add_description_to_assets_table',1),(46,'2023_05_26_062322_add_company_url_to_business_wealths_table',1),(47,'2023_05_26_124346_add_more_data_to_contacts_table',1),(48,'2023_05_27_070957_add_more_data_to_expenses_table',1),(49,'2023_05_29_133159_create_funeral_covers_table',1),(50,'2023_05_30_112216_create_dependents_table',1),(51,'2023_05_30_141913_add_more_data_to_medical_aids_table',1),(52,'2023_05_31_084622_create_trust_beneficiaries_table',1),(53,'2023_05_31_110325_create_reviews_table',1),(54,'2023_06_01_072831_create_confirmations_table',1),(55,'2023_06_01_093831_add_fields_to_shares_table',1),(56,'2023_06_05_070050_add_fields_to_users_table',1),(57,'2023_06_05_071901_add_executor_to_spouses_table',1),(58,'2023_06_05_135935_add_more_data_to_business_wealths_table',1),(59,'2023_06_05_142819_create_key_words_table',1),(60,'2023_06_05_143023_create_info_graphics_table',1),(61,'2023_06_05_144217_create_infographic_keywords_table',1),(62,'2023_06_06_061753_add_gender_and_ethinicity_to_user_table',1),(63,'2023_06_06_062424_add_more_files_to_business_wealths_table',1),(64,'2023_06_06_073735_add_more_files_to_business_wealths_table',1),(65,'2023_06_06_094612_create_investment_beneficiaries_table',1),(66,'2023_06_06_110217_add_investment_id_to_investment_beneficiaries_table',1),(67,'2023_06_06_131847_create_questionniaires_table',1),(68,'2023_06_06_135401_add_field_to_funeral_wishes',1),(69,'2023_06_08_110229_create_education_table',1),(70,'2023_06_12_072538_create_mailing_lists_table',1),(71,'2023_06_13_060234_add_welcome_modal_to_users_table',1),(72,'2023_06_13_074926_add_fields_to_medial_aid_table',1),(73,'2023_06_13_113110_add_education_to_shares_table',1),(74,'2023_06_13_114122_add_more_data_to_shares_table',1),(75,'2023_06_13_120645_add_more_data_to_shares_table',1),(76,'2023_06_14_113844_add_agreement_tospouses_table',1),(77,'2023_06_14_115941_add_medacal_aid_todependants_table',1),(78,'2023_06_15_080341_create_medical_aid_conditions_table',1),(79,'2023_06_15_093911_create_pop_ups_table',1),(80,'2023_06_15_111823_create_memories_accesses_table',1),(81,'2023_06_15_132215_add_payment_date_to_expense_table',1),(82,'2023_06_21_133328_add_agreement_file_to_spouses_table',1),(83,'2023_06_23_151127_add_data_to_pop_ups',1),(84,'2023_06_28_075135_add_data_to_dependents_table',1),(85,'2023_06_28_081435_make_medical_aid_id_nullable_in_dependents_table',1),(86,'2023_06_30_141850_add_safe_to_pop_ups',1),(87,'2023_07_03_092900_add_recipient_to_memories',1),(88,'2023_07_03_100358_add_condition_dependants_table',1),(89,'2023_07_03_120638_add_memory_type_to_memories_table',1),(90,'2023_07_03_135528_add_where_to_find_to_safe_keeps',1),(91,'2023_07_03_150053_create_possessions_table',1),(92,'2023_07_04_101754_add_bussiness_wealth_id_in_possessions_table',1),(93,'2023_07_04_142643_add_fields_to_investment_table',1),(94,'2023_07_04_144135_add_file_to_expenses_table',1),(95,'2023_07_07_053956_add_specific_relation_to_contacts_table',1),(96,'2023_07_07_055425_add_specify_relation_to_executors_table',1),(97,'2023_07_07_111855_create_trusts_table',1),(98,'2023_07_07_112206_add_trust_to_trustees_table',1),(99,'2023_07_07_112422_add_trust_trust_beneficiaries_table',1),(100,'2023_07_08_110848_specific_possession_to_possessions_table',1),(101,'2023_07_08_115048_add_value_to_possessions_table',1),(102,'2023_07_10_062539_add_memory_id_to_questionnaires_table',1),(103,'2023_07_10_065945_rename_columns_on_questionniaire_table',2),(104,'2023_07_10_100142_add_best_part_to_questionniaires_table',2),(105,'2023_07_12_083031_add_message_to_banks_table',2),(106,'2023_07_12_131024_modify_value_in_possessions_table',2),(107,'2023_07_12_174935_modify_pay_date_on_expenses_table',2),(108,'2023_07_17_140237_add_type_to_info_graphics_table',2),(109,'2023_07_21_103144_add_field_to_organisation_beneficiaries',2),(110,'2023_07_24_100048_add_usufruct_spouses_table',2),(111,'2023_07_25_081235_add_percentage_to_dependents_table',2),(112,'2023_07_26_113854_change_value_from_double_to_string_on_possessions_table',2),(113,'2023_07_26_121527_change_description_from_varchar_to_long_text_on_assets_table',2),(114,'2023_07_28_141252_add_field_to_insurance_table',2),(115,'2023_08_01_055814_add_fields_to_users_table',2),(116,'2023_08_04_072641_create_business_wealth_creditors_table',2),(117,'2023_08_04_085938_create_trusted_contacts_table',2),(118,'2023_08_10_080117_add_field_to_individual_beneficiaries_tabel',2),(119,'2023_08_17_054537_drop_shares_table',2),(120,'2023_08_17_055755_create_shares_table',2),(121,'2023_08_17_060723_create_share_options_table',2),(122,'2023_08_23_072213_add_field_to_guardians_table',2),(123,'2023_08_24_053338_add_contact_number_to_share_table',2),(124,'2023_08_24_102553_add_passport_to_users_table',2);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `organisation_beneficiaries`
--

DROP TABLE IF EXISTS `organisation_beneficiaries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `organisation_beneficiaries` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `inheritance_id` int(11) NOT NULL,
  `organisation_name` varchar(255) NOT NULL,
  `registration_number` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `city` varchar(255) NOT NULL,
  `postal_code` varchar(255) NOT NULL,
  `province` varchar(255) NOT NULL,
  `organisation_type` varchar(255) DEFAULT NULL,
  `organisation_other_type` varchar(255) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `percentage` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `organisation_beneficiaries`
--

LOCK TABLES `organisation_beneficiaries` WRITE;
/*!40000 ALTER TABLE `organisation_beneficiaries` DISABLE KEYS */;
INSERT INTO `organisation_beneficiaries` (`id`, `user_id`, `inheritance_id`, `organisation_name`, `registration_number`, `address`, `city`, `postal_code`, `province`, `organisation_type`, `organisation_other_type`, `description`, `value`, `percentage`, `created_at`, `updated_at`) VALUES (1,2,4,'test','test123456','74 Jerome street\nLynnwood Glen, Pretoria East','Pretoria','','Gauteng','charity','',NULL,NULL,'0','2023-09-18 17:40:13','2023-09-18 17:40:13');
/*!40000 ALTER TABLE `organisation_beneficiaries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_reset_tokens`
--

DROP TABLE IF EXISTS `password_reset_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_reset_tokens` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_reset_tokens`
--

LOCK TABLES `password_reset_tokens` WRITE;
/*!40000 ALTER TABLE `password_reset_tokens` DISABLE KEYS */;
INSERT INTO `password_reset_tokens` (`email`, `token`, `created_at`) VALUES ('jay.ns1712@gmail.com','$2y$10$/75S63yZV9HoFukLrNA/P.eHWStxXu9ZHii08Kr1k4cfXVbavaPtC','2023-10-25 08:41:40'),('junior.ns1712@gmail.com','$2y$10$7eT7jBYZi6g..zl9YDVrL.FGIg4ZdDO6g/0hFNyJK9pQ3aztPpuuK','2023-10-25 14:38:20'),('nsenga.ns1712@gmail.com','$2y$10$rlotLe4nGQiVP13tUz4iEenTDWIlAWiY6NxGbVofU6g858LJRZL0.','2023-11-01 04:29:20');
/*!40000 ALTER TABLE `password_reset_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `personal_access_tokens`
--

DROP TABLE IF EXISTS `personal_access_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) NOT NULL,
  `tokenable_id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `token` varchar(64) NOT NULL,
  `abilities` text DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `personal_access_tokens`
--

LOCK TABLES `personal_access_tokens` WRITE;
/*!40000 ALTER TABLE `personal_access_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `personal_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `place_of_works`
--

DROP TABLE IF EXISTS `place_of_works`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `place_of_works` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `employer_name` varchar(255) NOT NULL,
  `employer_address` varchar(255) NOT NULL,
  `salary_number` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `place_of_works`
--

LOCK TABLES `place_of_works` WRITE;
/*!40000 ALTER TABLE `place_of_works` DISABLE KEYS */;
INSERT INTO `place_of_works` (`id`, `user_id`, `employer_name`, `employer_address`, `salary_number`, `created_at`, `updated_at`) VALUES (1,1,'aka ThinkTank Creatives','51 Soveriegn Drive','EMP101','2023-09-18 11:17:38','2023-09-18 11:17:38'),(2,2,'Thinktank','51 Sovereign Drive, Centurion, Irene','SAL001','2023-09-18 11:42:35','2023-09-18 11:42:35'),(3,4,'Susandra van Wyk Attorneys','259 Sidney Avenue','1232455','2023-09-19 07:39:09','2023-09-19 07:39:09'),(4,71,'wrapup','12 hgf','0000000','2023-10-27 10:47:19','2023-10-27 10:47:19');
/*!40000 ALTER TABLE `place_of_works` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pop_ups`
--

DROP TABLE IF EXISTS `pop_ups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pop_ups` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `memories` int(11) NOT NULL DEFAULT 0,
  `safe` int(11) NOT NULL DEFAULT 0,
  `social` int(11) NOT NULL DEFAULT 0,
  `tree` int(11) NOT NULL DEFAULT 0,
  `will` int(11) NOT NULL DEFAULT 0,
  `organiser` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pop_ups`
--

LOCK TABLES `pop_ups` WRITE;
/*!40000 ALTER TABLE `pop_ups` DISABLE KEYS */;
INSERT INTO `pop_ups` (`id`, `user_id`, `memories`, `safe`, `social`, `tree`, `will`, `organiser`, `created_at`, `updated_at`) VALUES (1,1,1,1,1,0,1,1,'2023-09-18 11:12:46','2023-09-18 11:44:53'),(2,2,1,1,1,0,1,1,'2023-09-18 11:33:16','2023-09-18 17:34:05'),(3,3,1,1,0,0,1,1,'2023-09-18 11:43:12','2023-09-20 04:14:35'),(4,4,1,1,1,0,1,1,'2023-09-19 07:15:20','2023-09-19 11:07:30'),(5,9,1,1,1,0,1,1,'2023-09-21 03:56:13','2023-10-05 05:47:03'),(6,7,0,0,0,0,0,1,'2023-09-21 07:59:53','2023-09-21 08:03:19'),(7,11,1,1,1,0,1,1,'2023-09-21 08:46:45','2023-10-26 11:13:56'),(8,14,0,0,0,0,1,0,'2023-09-21 13:40:21','2023-09-21 13:40:24'),(9,64,0,0,0,0,0,0,'2023-10-26 17:50:22','2023-10-26 17:50:22'),(10,71,1,1,1,0,1,1,'2023-10-27 10:38:03','2023-10-27 10:56:00'),(11,47,0,0,0,0,1,1,'2023-10-27 10:54:38','2023-10-27 10:55:16');
/*!40000 ALTER TABLE `pop_ups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `possessions`
--

DROP TABLE IF EXISTS `possessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `possessions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `business_wealth_id` int(11) DEFAULT NULL,
  `possessions` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `specify_possession` varchar(255) DEFAULT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `possessions`
--

LOCK TABLES `possessions` WRITE;
/*!40000 ALTER TABLE `possessions` DISABLE KEYS */;
INSERT INTO `possessions` (`id`, `user_id`, `business_wealth_id`, `possessions`, `created_at`, `updated_at`, `specify_possession`, `value`) VALUES (1,3,2,'House','2023-09-18 11:51:53','2023-09-18 11:51:53','12 Street, Road','1,000,000'),(2,4,5,'House','2023-09-19 11:58:01','2023-09-19 11:58:01','Church Street 10, Pretoria','1,000,000'),(3,4,6,'','2023-09-19 11:59:21','2023-09-19 11:59:21','',''),(4,4,8,'Farm','2023-09-19 12:01:51','2023-09-19 12:01:51','Kromdraai','1,000,000'),(5,4,8,'Other','2023-09-19 12:01:51','2023-09-19 12:01:51','Toyota Corolla','120,000'),(6,4,9,'Investment','2023-09-19 12:04:31','2023-09-19 12:04:31','FNB Savings','10,000'),(7,3,13,'','2023-09-19 12:23:02','2023-09-19 12:23:02','',''),(8,3,14,'Car','2023-09-19 12:24:07','2023-09-19 12:24:07','Audi','100,000'),(9,4,5,'Bank Account','2023-09-20 07:43:00','2023-09-20 07:43:00','FNB Savings','30,000'),(10,4,9,'Equipment','2023-09-20 07:44:04','2023-09-20 07:44:04','Equipment','200'),(11,4,8,'Stock','2023-09-20 07:44:39','2023-09-20 07:44:39','Stock','10,000'),(12,9,15,'House','2023-09-28 12:54:17','2023-09-28 12:54:17','Lynnwood','200,000'),(13,71,16,'Car','2023-10-27 10:51:10','2023-10-27 10:51:10','kia rio car','150,000');
/*!40000 ALTER TABLE `possessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `questionniaires`
--

DROP TABLE IF EXISTS `questionniaires`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `questionniaires` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `memory_id` int(11) DEFAULT NULL,
  `favorite_song` varchar(255) DEFAULT NULL,
  `favorite_movie` varchar(255) DEFAULT NULL,
  `favorite_place` varchar(255) DEFAULT NULL,
  `favorite_food` varchar(255) DEFAULT NULL,
  `special_memory` varchar(255) DEFAULT NULL,
  `biggest_lesson` varchar(255) DEFAULT NULL,
  `regret` varchar(255) DEFAULT NULL,
  `religion_beliefs` varchar(255) DEFAULT NULL,
  `dint_know` varchar(255) DEFAULT NULL,
  `hopes_for_someone` varchar(255) DEFAULT NULL,
  `happy_thing` varchar(255) DEFAULT NULL,
  `sad_thing` varchar(255) DEFAULT NULL,
  `best_quality` varchar(255) DEFAULT NULL,
  `bucket_list` varchar(255) DEFAULT NULL,
  `best_part` varchar(255) DEFAULT NULL,
  `advice` varchar(255) DEFAULT NULL,
  `proud_of` varchar(255) DEFAULT NULL,
  `remember_me_by` varchar(255) DEFAULT NULL,
  `funny_memory` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `questionniaires`
--

LOCK TABLES `questionniaires` WRITE;
/*!40000 ALTER TABLE `questionniaires` DISABLE KEYS */;
/*!40000 ALTER TABLE `questionniaires` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reviews`
--

DROP TABLE IF EXISTS `reviews`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reviews` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `executors` varchar(255) DEFAULT NULL,
  `beneficiaries` varchar(255) DEFAULT NULL,
  `witnesses` varchar(255) DEFAULT NULL,
  `guardians` varchar(255) DEFAULT NULL,
  `trustees` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reviews`
--

LOCK TABLES `reviews` WRITE;
/*!40000 ALTER TABLE `reviews` DISABLE KEYS */;
INSERT INTO `reviews` (`id`, `user_id`, `executors`, `beneficiaries`, `witnesses`, `guardians`, `trustees`, `created_at`, `updated_at`) VALUES (1,1,'1','1','1','1','1','2023-09-18 11:49:58','2023-09-18 11:50:24'),(2,2,'1','1','1',NULL,NULL,'2023-09-18 17:42:52','2023-09-18 17:43:14'),(4,4,'1','1','1',NULL,'1','2023-10-30 05:44:49','2023-10-30 05:45:13');
/*!40000 ALTER TABLE `reviews` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `safe_keeps`
--

DROP TABLE IF EXISTS `safe_keeps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `safe_keeps` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `location` varchar(255) DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `safe_keeps`
--

LOCK TABLES `safe_keeps` WRITE;
/*!40000 ALTER TABLE `safe_keeps` DISABLE KEYS */;
INSERT INTO `safe_keeps` (`id`, `user_id`, `title`, `description`, `location`, `file`, `created_at`, `updated_at`) VALUES (1,1,'Bitcoin','Lorem ipsum occaecat non excepteur enim ad in magna pariatur ut nostrud aliquip velit. Do ullamco tempor occaecat qui laboris occaecat esse velit commodo. Nulla tempor voluptate consectetur nulla exercitation dolore do ullamco reprehenderit officia. Sit nulla consectetur esse culpa ea id irure in aute deserunt minim sunt.','Lorem ipsum occaecat non excepteur enim ad in magna pariatur ut nostrud aliquip velit. Do ullamco tempor occaecat qui laboris occaecat esse velit commodo. Nulla tempor voluptate consectetur nulla exercitation dolore do ullamco reprehenderit officia. Sit n','','2023-09-18 11:41:33','2023-09-18 11:41:55'),(2,1,'Other Stuff','Lorem ipsum occaecat non excepteur enim ad in magna pariatur ut nostrud aliquip velit. Do ullamco tempor occaecat qui laboris occaecat esse velit commodo. Nulla tempor voluptate consectetur nulla exercitation dolore do ullamco reprehenderit officia. Sit nulla consectetur esse culpa ea id irure in aute deserunt minim sunt.','Lorem ipsum occaecat non excepteur enim ad in magna pariatur ut nostrud aliquip velit. Do ullamco tempor occaecat qui laboris occaecat esse velit commodo. Nulla tempor voluptate consectetur nulla exercitation dolore do ullamco reprehenderit officia. Sit n','safekeep/uiYmEQ9cVe4t3VHO0uYLO4lQcbtnmGZODjZXVi1w.pdf','2023-09-18 11:41:55','2023-09-18 11:41:55'),(4,4,'test','test','test','','2023-09-19 10:51:45','2023-09-19 13:09:06'),(5,4,'will test','test','test','','2023-09-19 13:09:06','2023-09-20 07:29:54'),(6,3,'Title info','Description info','Where to find info','','2023-09-20 04:15:37','2023-09-20 04:17:57'),(7,3,'Name ','This is a description of something I have uploaded','This item can be found at this location is specified in this sentence','','2023-09-20 04:17:57','2023-09-20 04:21:36'),(8,3,'My Will','The description of my will','Can be found at this location','safekeep/I8XYbnnkzwcIu06x0wFGIXQztjhbj8s5iXax1ynl.jpg','2023-09-20 04:21:36','2023-09-20 04:21:36'),(9,4,'test 3','test3','test3','safekeep/dddVty1ICmY5SxpuKJNn27NA7ie7ELHloErMtXeH.pdf','2023-09-20 07:29:54','2023-09-20 07:29:59'),(10,71,'jewellery','in house','house','','2023-10-27 10:55:51','2023-10-27 10:55:51');
/*!40000 ALTER TABLE `safe_keeps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `share_options`
--

DROP TABLE IF EXISTS `share_options`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `share_options` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `share_id` int(11) NOT NULL,
  `profile` int(11) NOT NULL DEFAULT 0,
  `organiser` int(11) NOT NULL DEFAULT 0,
  `personal` int(11) NOT NULL DEFAULT 0,
  `spouse` int(11) NOT NULL DEFAULT 0,
  `work` int(11) NOT NULL DEFAULT 0,
  `tax` int(11) NOT NULL DEFAULT 0,
  `children` int(11) NOT NULL DEFAULT 0,
  `what_to_find` int(11) NOT NULL DEFAULT 0,
  `medical_aid` int(11) NOT NULL DEFAULT 0,
  `connections` int(11) NOT NULL DEFAULT 0,
  `family` int(11) NOT NULL DEFAULT 0,
  `personal_connections` int(11) NOT NULL DEFAULT 0,
  `business` int(11) NOT NULL DEFAULT 0,
  `doctors` int(11) NOT NULL DEFAULT 0,
  `emergency` int(11) NOT NULL DEFAULT 0,
  `accountant_bookkeeper` int(11) NOT NULL DEFAULT 0,
  `private_banker` int(11) NOT NULL DEFAULT 0,
  `lawyer` int(11) NOT NULL DEFAULT 0,
  `business_wealth` int(11) NOT NULL DEFAULT 0,
  `properties_wealth` int(11) NOT NULL DEFAULT 0,
  `debt_expenses` int(11) NOT NULL DEFAULT 0,
  `social_media` int(11) NOT NULL DEFAULT 0,
  `safe_keeping` int(11) NOT NULL DEFAULT 0,
  `memories` int(11) NOT NULL DEFAULT 0,
  `funeral` int(11) NOT NULL DEFAULT 0,
  `family_tree` int(11) NOT NULL DEFAULT 0,
  `education` int(11) NOT NULL DEFAULT 0,
  `funeral_cover` int(11) NOT NULL DEFAULT 0,
  `friends` int(11) NOT NULL DEFAULT 0,
  `assets` int(11) NOT NULL DEFAULT 0,
  `investments_and_policies` int(11) NOT NULL DEFAULT 0,
  `bank_accounts` int(11) NOT NULL DEFAULT 0,
  `expenses` int(11) NOT NULL DEFAULT 0,
  `loans` int(11) NOT NULL DEFAULT 0,
  `insurance` int(11) NOT NULL DEFAULT 0,
  `other_expenses` int(11) NOT NULL DEFAULT 0,
  `income_tax` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `share_options`
--

LOCK TABLES `share_options` WRITE;
/*!40000 ALTER TABLE `share_options` DISABLE KEYS */;
INSERT INTO `share_options` (`id`, `share_id`, `profile`, `organiser`, `personal`, `spouse`, `work`, `tax`, `children`, `what_to_find`, `medical_aid`, `connections`, `family`, `personal_connections`, `business`, `doctors`, `emergency`, `accountant_bookkeeper`, `private_banker`, `lawyer`, `business_wealth`, `properties_wealth`, `debt_expenses`, `social_media`, `safe_keeping`, `memories`, `funeral`, `family_tree`, `education`, `funeral_cover`, `friends`, `assets`, `investments_and_policies`, `bank_accounts`, `expenses`, `loans`, `insurance`, `other_expenses`, `income_tax`, `created_at`, `updated_at`) VALUES (1,1,1,1,1,0,1,0,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,'2023-09-18 11:52:45','2023-09-18 11:52:45'),(2,2,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,'2023-09-18 11:56:53','2023-09-18 11:56:53'),(3,2,1,1,1,0,1,0,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,'2023-09-18 11:57:45','2023-09-18 11:57:45'),(4,3,1,1,1,0,1,0,0,0,0,1,1,0,0,0,0,0,0,0,1,1,1,1,1,1,1,0,0,0,0,1,0,0,0,1,0,0,0,'2023-09-18 17:48:11','2023-09-18 17:48:11'),(5,4,1,1,1,0,1,0,1,0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,'2023-09-19 07:49:39','2023-09-19 07:49:39'),(6,4,1,1,1,0,1,0,1,0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,'2023-09-19 07:49:48','2023-09-19 07:49:48'),(7,4,1,1,1,0,1,0,1,0,1,1,1,0,0,0,0,0,0,0,1,1,1,1,1,1,1,0,1,0,0,1,1,1,1,1,1,1,1,'2023-09-19 13:44:15','2023-09-19 13:44:15'),(8,4,1,1,1,0,1,0,1,0,1,1,1,0,0,0,0,0,0,0,1,1,1,1,1,1,1,0,1,0,0,1,1,1,1,1,1,1,1,'2023-09-19 13:44:21','2023-09-19 13:44:21'),(9,5,1,1,1,0,1,0,1,0,1,1,1,0,0,0,0,0,0,0,1,1,1,1,1,1,0,0,1,0,0,1,1,1,1,1,1,1,1,'2023-09-19 13:57:28','2023-09-19 13:57:28'),(10,5,1,1,1,0,1,0,1,0,1,1,1,0,0,0,0,0,0,0,1,1,1,1,1,1,0,0,1,0,0,1,1,1,1,1,1,1,1,'2023-09-19 13:57:33','2023-09-19 13:57:33'),(11,4,1,1,1,0,1,0,1,0,1,1,1,0,0,0,0,0,0,0,1,1,1,1,1,1,1,0,1,0,0,1,1,1,1,1,1,1,1,'2023-09-19 15:21:31','2023-09-19 15:21:31'),(12,6,1,1,1,0,1,0,1,0,1,1,1,0,0,0,0,0,0,0,1,1,1,1,1,1,1,0,1,0,0,1,1,1,1,1,1,1,1,'2023-09-20 07:56:22','2023-09-20 07:56:22'),(13,6,1,1,1,0,1,0,1,0,1,1,1,0,0,0,0,0,0,0,1,1,1,1,1,1,1,0,1,0,0,1,1,1,1,1,1,1,1,'2023-09-20 07:56:27','2023-09-20 07:56:27'),(14,7,1,1,1,0,0,0,0,0,1,1,0,0,0,1,0,0,0,1,1,0,1,1,0,1,1,0,0,0,1,0,0,0,1,0,0,0,0,'2023-09-20 11:06:28','2023-09-20 11:06:28'),(15,8,1,1,1,0,1,0,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,0,1,1,1,1,1,1,1,1,1,'2023-09-20 12:10:18','2023-09-20 12:10:18'),(16,8,1,1,1,0,1,0,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,0,1,1,1,1,1,1,1,1,1,'2023-09-20 12:10:28','2023-09-20 12:10:28'),(17,9,1,1,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,'2023-09-20 12:15:09','2023-09-20 12:15:09'),(18,10,1,1,1,0,1,0,0,0,1,1,1,0,0,0,1,1,0,0,1,0,0,1,0,1,1,0,1,0,0,0,0,0,0,0,0,0,0,'2023-09-28 12:13:08','2023-09-28 12:13:08'),(19,4,1,1,1,0,1,0,1,0,1,1,1,0,0,1,0,0,0,0,1,1,1,1,1,1,1,0,1,0,1,1,1,1,1,1,1,1,1,'2023-09-28 12:20:53','2023-09-28 12:20:53'),(20,4,1,1,1,0,1,0,1,0,1,1,1,0,0,1,0,0,0,0,1,1,1,1,1,1,1,0,1,0,1,1,1,1,1,1,1,1,1,'2023-09-28 12:21:27','2023-09-28 12:21:27'),(21,11,1,1,1,0,1,0,1,0,1,1,1,0,1,0,0,1,0,0,1,1,1,1,1,1,1,0,1,0,0,1,1,1,1,1,1,1,1,'2023-09-28 12:23:25','2023-09-28 12:23:25'),(22,11,1,1,1,0,1,0,1,0,1,1,1,0,1,0,0,1,0,0,1,1,1,1,1,1,1,0,1,0,0,1,1,1,1,1,1,1,1,'2023-09-28 12:23:30','2023-09-28 12:23:30'),(23,11,1,1,1,0,1,0,1,0,1,1,1,0,1,0,0,1,0,0,1,1,1,1,1,1,1,0,1,0,0,1,1,1,1,1,1,1,1,'2023-09-28 12:24:25','2023-09-28 12:24:25'),(24,12,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,'2023-09-28 13:01:44','2023-09-28 13:01:44'),(25,13,1,1,1,0,1,0,0,0,1,1,1,0,0,0,0,0,0,0,1,1,1,1,1,1,1,0,1,0,0,1,1,1,1,0,0,0,0,'2023-10-27 10:59:48','2023-10-27 10:59:48');
/*!40000 ALTER TABLE `share_options` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shares`
--

DROP TABLE IF EXISTS `shares`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shares` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `contact_id` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `contact_number` varchar(255) DEFAULT NULL,
  `id_number` varchar(255) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `family` varchar(255) DEFAULT NULL,
  `friends` varchar(255) DEFAULT NULL,
  `professional` varchar(255) DEFAULT NULL,
  `specify_relation` varchar(255) DEFAULT NULL,
  `emergency_contact` int(11) NOT NULL DEFAULT 0,
  `color` varchar(255) DEFAULT NULL,
  `order_num` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shares`
--

LOCK TABLES `shares` WRITE;
/*!40000 ALTER TABLE `shares` DISABLE KEYS */;
INSERT INTO `shares` (`id`, `user_id`, `contact_id`, `name`, `email`, `contact_number`, `id_number`, `dob`, `family`, `friends`, `professional`, `specify_relation`, `emergency_contact`, `color`, `order_num`, `created_at`, `updated_at`) VALUES (1,1,2,'Q','quintus@thinktank.co.za','0987654321','Q1234567890','2121-12-12','','Work Friend','',NULL,1,NULL,0,'2023-09-18 11:52:45','2023-09-18 11:56:26'),(2,1,NULL,'Belinda','ndlovu28@gmail.com','0987654321','BLH1234567','1212-12-12','Mother','','',NULL,1,NULL,0,'2023-09-18 11:56:53','2023-09-18 11:56:53'),(3,2,NULL,'test','qvander@gmail.com','012123456','8404025024080','1984-04-02','','Best Friend','',NULL,1,NULL,0,'2023-09-18 17:48:11','2023-09-18 17:48:11'),(4,4,9,'Cherene Cook','cherenecook@gmail.com','0825567051','9201230176088','1992-01-23','Step Daughter','','',NULL,1,'bg-yellow2',3,'2023-09-19 07:49:39','2023-10-24 06:28:33'),(5,4,14,'Louis Claassens','louisclaassens@icloud.com','0788031314','0411235130084','2004-11-23','Son','','',NULL,1,'yellow-bg',2,'2023-09-19 13:57:28','2023-10-24 06:28:33'),(6,4,6,'Susandra Test','susandra@icloud.com','0788031314','7101120212088','1971-01-12','','Work Friend','',NULL,1,'bg-green',0,'2023-09-20 07:56:22','2023-09-20 09:00:05'),(7,2,3,'Steph','steph@thinktank.co.za','0125670987','9865457888','2000-01-01','','','',NULL,0,NULL,0,'2023-09-20 11:06:28','2023-09-20 11:06:31'),(8,4,8,'Susandra Test 3','susandra@wrapup.co.za','0788031314','7101120212088','1971-01-12','','','Lawyer',NULL,1,NULL,0,'2023-09-20 12:10:18','2023-09-20 12:23:19'),(9,3,3,'Steph','steph@thinktank.co.za','0749317766','5642345765','1990-02-03','Aunt','','',NULL,0,NULL,0,'2023-09-20 12:15:09','2023-09-26 05:04:42'),(10,11,4,'Susandra van Wyk','vanwyksusandra@gmail.com','0788031314','7101120212088','1971-01-12','Wife','','',NULL,1,NULL,0,'2023-09-28 12:13:08','2023-09-28 12:13:50'),(11,4,11,'Pierre van Wyk','drpierrevanwyk@gmail.com','0825567051','5011095121085','1950-11-09','Husband','','',NULL,1,'bg-blue',1,'2023-09-28 12:23:25','2023-10-24 06:28:38'),(12,9,4,'Susandra','vanwyksusandra@gmail.com','0788031314','7101120212088','1971-01-12','Step Mother','','',NULL,1,NULL,1,'2023-09-28 13:01:44','2023-11-08 06:27:47'),(13,71,9,'Cherene','cherenecook@gmail.com','0640656888','9201230176088','1992-01-23','Friend','','',NULL,1,NULL,0,'2023-10-27 10:59:48','2023-10-27 11:01:37');
/*!40000 ALTER TABLE `shares` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `social_media`
--

DROP TABLE IF EXISTS `social_media`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `social_media` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `network` varchar(255) NOT NULL,
  `specific_network` varchar(255) DEFAULT NULL,
  `user_name` varchar(255) NOT NULL,
  `link` varchar(255) DEFAULT NULL,
  `legacy_contact` varchar(255) DEFAULT NULL,
  `instructions` text DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `social_media`
--

LOCK TABLES `social_media` WRITE;
/*!40000 ALTER TABLE `social_media` DISABLE KEYS */;
INSERT INTO `social_media` (`id`, `user_id`, `network`, `specific_network`, `user_name`, `link`, `legacy_contact`, `instructions`, `created_at`, `updated_at`) VALUES (1,1,'Facebook',NULL,'dred28','www.facebook.com','dred28','Lorem ipsum occaecat non excepteur enim ad in magna pariatur ut nostrud aliquip velit. Do ullamco tempor occaecat qui laboris occaecat esse velit commodo. Nulla tempor voluptate consectetur nulla exercitation dolore do ullamco reprehenderit officia. Sit nulla consectetur esse culpa ea id irure in aute deserunt minim sunt.','2023-09-18 11:39:37','2023-09-18 11:39:37'),(2,1,'Twitter',NULL,'dred28','ww.twitter.com',NULL,'Lorem ipsum occaecat non excepteur enim ad in magna pariatur ut nostrud aliquip velit. Do ullamco tempor occaecat qui laboris occaecat esse velit commodo. Nulla tempor voluptate consectetur nulla exercitation dolore do ullamco reprehenderit officia. Sit nulla consectetur esse culpa ea id irure in aute deserunt minim sunt.','2023-09-18 11:40:20','2023-09-18 11:40:20'),(3,1,'LinkedIn',NULL,'dred28','www.linkedin.com',NULL,'Lorem ipsum occaecat non excepteur enim ad in magna pariatur ut nostrud aliquip velit. Do ullamco tempor occaecat qui laboris occaecat esse velit commodo. Nulla tempor voluptate consectetur nulla exercitation dolore do ullamco reprehenderit officia. Sit nulla consectetur esse culpa ea id irure in aute deserunt minim sunt.','2023-09-18 11:41:07','2023-09-18 11:41:07'),(5,4,'Facebook',NULL,'0788031314','wwwww','Sannie ','Maak dit toe','2023-09-19 10:18:30','2023-09-19 10:18:30'),(6,4,'Twitter',NULL,'twitter','link',NULL,'test','2023-09-19 10:18:55','2023-09-19 10:18:55'),(8,4,'LinkedIn',NULL,'test','test',NULL,'test','2023-09-19 10:19:38','2023-09-19 10:19:38'),(9,4,'Instagram',NULL,'test','test',NULL,'test','2023-09-19 10:19:51','2023-09-19 10:19:51'),(10,4,'Other','ttest','test','test',NULL,'test','2023-09-19 10:20:07','2023-09-19 10:20:07'),(11,71,'Facebook',NULL,'Cherenecook',NULL,'chante','memorial page','2023-10-27 10:55:21','2023-10-27 10:55:21');
/*!40000 ALTER TABLE `social_media` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `spouses`
--

DROP TABLE IF EXISTS `spouses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `spouses` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `marital_status` varchar(255) NOT NULL,
  `spouse_name` varchar(255) DEFAULT NULL,
  `spouse_middle_name` varchar(255) DEFAULT NULL,
  `spouse_surname` varchar(255) DEFAULT NULL,
  `spouse_id_number` varchar(255) DEFAULT NULL,
  `marriage_certificate` varchar(255) DEFAULT NULL,
  `antenuptial_contract` varchar(255) DEFAULT NULL,
  `date_of_death` varchar(255) DEFAULT NULL,
  `masters_office` varchar(255) DEFAULT NULL,
  `estate_number` varchar(255) DEFAULT NULL,
  `attorney_auditor_trust_contact_details` varchar(255) DEFAULT NULL,
  `estate_wrapping_up_company` varchar(255) DEFAULT NULL,
  `dweling_property` varchar(255) DEFAULT NULL,
  `usufruct` varchar(255) DEFAULT NULL,
  `death_certificate` varchar(255) DEFAULT NULL,
  `will` varchar(255) DEFAULT NULL,
  `date_of_divorce` varchar(255) DEFAULT NULL,
  `court_order_and_agreement` varchar(255) DEFAULT NULL,
  `maintenance_order` varchar(255) DEFAULT NULL,
  `proof_of_maintenance_order` varchar(255) DEFAULT NULL,
  `specific_agreements_description` varchar(255) DEFAULT NULL,
  `specific_agreements_file` varchar(255) DEFAULT NULL,
  `comunity_marriage` varchar(255) DEFAULT NULL,
  `accrual` varchar(255) DEFAULT NULL,
  `executor` varchar(255) DEFAULT NULL,
  `domestic_agreement` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `divorce_agreement` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `spouses`
--

LOCK TABLES `spouses` WRITE;
/*!40000 ALTER TABLE `spouses` DISABLE KEYS */;
INSERT INTO `spouses` (`id`, `user_id`, `marital_status`, `spouse_name`, `spouse_middle_name`, `spouse_surname`, `spouse_id_number`, `marriage_certificate`, `antenuptial_contract`, `date_of_death`, `masters_office`, `estate_number`, `attorney_auditor_trust_contact_details`, `estate_wrapping_up_company`, `dweling_property`, `usufruct`, `death_certificate`, `will`, `date_of_divorce`, `court_order_and_agreement`, `maintenance_order`, `proof_of_maintenance_order`, `specific_agreements_description`, `specific_agreements_file`, `comunity_marriage`, `accrual`, `executor`, `domestic_agreement`, `created_at`, `updated_at`, `divorce_agreement`) VALUES (1,1,'Now Married','Belinda','Ntombiyomusa','Mabuya','BL1234567','documents/pCe2Z4pJFvk92dG1ZbUCotJdEmAuprVWcI0UqykL.pdf','documents/NCnHNXp5S0IYIizEnXmq8LsTQkWqu8rmrvnHn8hS.pdf','','','','','','','',NULL,NULL,'',NULL,'',NULL,'',NULL,'no','no','',NULL,'2023-09-18 11:17:02','2023-09-18 11:17:02',NULL),(2,2,'Engaged','Jane','Janet','Doe','9704043955184',NULL,NULL,'','','','','','','',NULL,NULL,'',NULL,'',NULL,'',NULL,'','','',NULL,'2023-09-18 11:41:52','2023-09-18 11:41:52',NULL),(3,2,'Now Married','test','test','test','8202170001089',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'no','no',NULL,NULL,'2023-09-18 17:39:13','2023-09-18 17:39:13',NULL),(4,4,'Now Married','Pierre','','van Wyk','5011095121085','documents/sXfvfu4mvLnbJEb34VKSctJNrDOUfrMd7i873OnI.jpg','documents/CD9NwSXpzi1TE9CI3fakmppjyks30CZ9TLeGOUCW.pdf','','','','','','','',NULL,NULL,'',NULL,'',NULL,'',NULL,'no','no','',NULL,'2023-09-19 07:35:00','2023-09-19 11:18:31',NULL),(5,4,'Widowed','Pietie','','Burger','',NULL,NULL,'1995-01-01','Pretoria','12345/1995','','','fideicommissum','usufruct','documents/7iuvurqMrt31DNppQ5ZwUHlMln4mLVILrQCoQYzN.pdf',NULL,'',NULL,'',NULL,'',NULL,'','','Sannie Burger',NULL,'2023-09-19 07:36:05','2023-09-19 07:36:05',NULL),(6,4,'Divorced','Hennie','','Claassens','5211115076083',NULL,NULL,'','','','','','','',NULL,NULL,'2011-11-01','documents/5Pzy7GKaSY9VJgfMcsvFvePICW1GQuBdmpvpRKqU.pdf','yes',NULL,'',NULL,'','','',NULL,'2023-09-19 07:38:19','2023-09-19 07:38:19','documents/4nbkbhYldAVt72q3eV6SQtqIFHIaWnaA9zQjJ7jr.pdf'),(7,3,'Now Married','Rian','','Lock','3804652075345',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'no','yes',NULL,NULL,'2023-09-19 12:42:42','2023-09-19 12:42:42',NULL),(11,71,'Now Married','Tyron','William','Cook','8905115152086','documents/wGSHsyJP5X7GFk314acdiSm1mQtCB7jnyQcsRl0N.jpg',NULL,'','','','','','','',NULL,NULL,'',NULL,'',NULL,'',NULL,'no','yes','',NULL,'2023-10-27 10:45:06','2023-10-27 10:45:06',NULL),(12,11,'Now Married','SUSANDRA','','VAN WYK','7101120212088',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'no','no',NULL,NULL,'2023-11-21 07:50:34','2023-11-21 09:45:06',NULL);
/*!40000 ALTER TABLE `spouses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `taxes`
--

DROP TABLE IF EXISTS `taxes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `taxes` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `tax_number` varchar(255) NOT NULL,
  `tax_assessment` varchar(255) DEFAULT NULL,
  `tax_certificate` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `taxes`
--

LOCK TABLES `taxes` WRITE;
/*!40000 ALTER TABLE `taxes` DISABLE KEYS */;
INSERT INTO `taxes` (`id`, `user_id`, `tax_number`, `tax_assessment`, `tax_certificate`, `created_at`, `updated_at`) VALUES (1,1,'TAX12345','documents/fYrFmR3s5E2nx33VOaj1QTSkJcWz8RaAr9oCz8IN.pdf','documents/cYSZ2ifRhA05kRCNgOlI9C44AhuZN2EIsCYb4YID.pdf','2023-09-18 11:42:13','2023-09-18 11:42:13'),(2,4,'222222222','documents/PtBZgYc1qguMsTwIkbONuxvPuZfj4gMvJDu24uHa.pdf','documents/IX3lcgdC3QgH6S30cC2JOjHSiNe7dUr3DVoS6Wf6.pdf','2023-09-19 10:51:26','2023-09-19 10:51:26');
/*!40000 ALTER TABLE `taxes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trust_beneficiaries`
--

DROP TABLE IF EXISTS `trust_beneficiaries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trust_beneficiaries` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `trust_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `middle_name` varchar(255) DEFAULT NULL,
  `surname` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trust_beneficiaries`
--

LOCK TABLES `trust_beneficiaries` WRITE;
/*!40000 ALTER TABLE `trust_beneficiaries` DISABLE KEYS */;
/*!40000 ALTER TABLE `trust_beneficiaries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trusted_contacts`
--

DROP TABLE IF EXISTS `trusted_contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trusted_contacts` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `surname` varchar(255) NOT NULL,
  `nickname` varchar(255) DEFAULT NULL,
  `id_number` varchar(255) NOT NULL,
  `dob` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `other_info` text DEFAULT NULL,
  `additional_info` text DEFAULT NULL,
  `terms_and_conditions` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trusted_contacts`
--

LOCK TABLES `trusted_contacts` WRITE;
/*!40000 ALTER TABLE `trusted_contacts` DISABLE KEYS */;
INSERT INTO `trusted_contacts` (`id`, `user_id`, `name`, `surname`, `nickname`, `id_number`, `dob`, `address`, `other_info`, `additional_info`, `terms_and_conditions`, `created_at`, `updated_at`) VALUES (1,1,'Nelson','Ndlovu','NP','NP1234567','1985-08-31','22 Jump Street','More Stuff here','some more stuff here','1','2023-09-18 11:15:28','2023-09-18 11:15:28'),(2,2,'John','Jack','Smith','9304049449181','1993-04-04','123 Test Road, Testville','jjs@test.com',NULL,'1','2023-09-18 11:39:00','2023-09-18 11:39:11'),(3,4,'Pierre','van Wyk',NULL,'5011095121085','1950-11-09','259 Sidney Avenue, 26 Waterkloof Village, Waterkloof, Pretoria, 0181','Medical doctor drpierrevanwyk@gmail.com','none','1','2023-09-19 07:28:43','2023-09-19 07:28:46'),(4,11,'SUSANDRA','VAN WYK','SUE','7101120212088','1971-01-12','House nr 26, Waterkloof Village, Sidneystreet\nWATERKLOOF RIDGE','0788031314',NULL,'1','2023-09-21 08:53:16','2023-09-21 08:53:21'),(5,71,'Cherene','van Wyk','Chez','9201230176088','1992-01-23','1 daisy street lynnwood','0640656888','testing this','1','2023-10-27 10:43:38','2023-10-27 10:43:44');
/*!40000 ALTER TABLE `trusted_contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trustees`
--

DROP TABLE IF EXISTS `trustees`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trustees` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `trust_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `middle_name` varchar(255) NOT NULL,
  `surname` varchar(255) NOT NULL,
  `id_number` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trustees`
--

LOCK TABLES `trustees` WRITE;
/*!40000 ALTER TABLE `trustees` DISABLE KEYS */;
INSERT INTO `trustees` (`id`, `trust_id`, `user_id`, `first_name`, `middle_name`, `surname`, `id_number`, `created_at`, `updated_at`) VALUES (1,1,1,'Nelson','','Ndlovu','NP1234567','2023-09-18 11:47:12','2023-09-18 11:47:12'),(2,1,1,'Belinda','Ntombiyomusa','Mabuya','BL1234567','2023-09-18 11:47:12','2023-09-18 11:47:12'),(8,4,4,'Sarie','','Burger','3411060010085','2023-10-30 05:42:23','2023-10-30 05:42:23'),(9,4,4,'Pierre','','van Wyk','5011095121085','2023-10-30 05:42:23','2023-10-30 05:42:23');
/*!40000 ALTER TABLE `trustees` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trusts`
--

DROP TABLE IF EXISTS `trusts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trusts` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `trust_type` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trusts`
--

LOCK TABLES `trusts` WRITE;
/*!40000 ALTER TABLE `trusts` DISABLE KEYS */;
INSERT INTO `trusts` (`id`, `user_id`, `trust_type`, `created_at`, `updated_at`) VALUES (1,1,'minor-children','2023-09-18 11:47:12','2023-09-18 11:47:12'),(4,4,'minor-children','2023-10-30 05:42:23','2023-10-30 05:42:23');
/*!40000 ALTER TABLE `trusts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `status` int(11) NOT NULL DEFAULT 0,
  `free_account` int(11) NOT NULL DEFAULT 0,
  `payment_date` date DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `middle_name` varchar(255) DEFAULT NULL,
  `surname` varchar(255) NOT NULL,
  `date_of_birth` date DEFAULT NULL,
  `id_number` varchar(255) DEFAULT NULL,
  `passport_number` varchar(255) DEFAULT NULL,
  `contact_number` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `ethnicity` varchar(255) DEFAULT NULL,
  `occupation` varchar(255) DEFAULT NULL,
  `bio` text DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `two_factor_secret` text DEFAULT NULL,
  `two_factor_recovery_codes` text DEFAULT NULL,
  `two_factor_confirmed_at` timestamp NULL DEFAULT NULL,
  `profile_pic` varchar(255) DEFAULT NULL,
  `street_address` varchar(255) DEFAULT NULL,
  `birth_certificate` varchar(255) DEFAULT NULL,
  `id_document` varchar(255) DEFAULT NULL,
  `passport_document` varchar(255) DEFAULT NULL,
  `drivers_license_document` varchar(255) DEFAULT NULL,
  `power_of_attorney_document` varchar(255) DEFAULT NULL,
  `separate_power_of_attorney_document` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `postal_code` varchar(255) DEFAULT NULL,
  `province` varchar(255) DEFAULT NULL,
  `instructions` varchar(255) DEFAULT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `welcome_modal` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `status`, `free_account`, `payment_date`, `name`, `middle_name`, `surname`, `date_of_birth`, `id_number`, `passport_number`, `contact_number`, `gender`, `ethnicity`, `occupation`, `bio`, `email`, `email_verified_at`, `password`, `two_factor_secret`, `two_factor_recovery_codes`, `two_factor_confirmed_at`, `profile_pic`, `street_address`, `birth_certificate`, `id_document`, `passport_document`, `drivers_license_document`, `power_of_attorney_document`, `separate_power_of_attorney_document`, `city`, `postal_code`, `province`, `instructions`, `remember_token`, `created_at`, `updated_at`, `welcome_modal`) VALUES (1,1,0,'2023-09-18','Wilson',NULL,'Ndlovu','1987-02-20',NULL,'CN447962',NULL,'Male','Black','Developer','Amet esse tempor incididunt ut anim aute officia in nostrud. Lorem ipsum in veniam dolor proident cupidatat veniam in in dolor. Velit eu ad dolore sit dolor incididunt ullamco culpa tempor eiusmod mollit minim sit qui dolor dolore officia excepteur. ','wilson@thinktank.co.za',NULL,'$2y$10$tto3bjK6jvu5axIp4C3sYuTjobP87/XQM/XgZ92wwJKlpQDu0lXva',NULL,NULL,NULL,'photos/1QhgNutfqtJEBbamVjlsAv0hGtS3W99j07q6ndbg.png','21 Jump Street','documents/ykPIGcCUqq1ziR7H51uYUJpago5NGCRicG16Uoz8.pdf','documents/2RYXxmHxmTEGTGUHTZzgQE2AgFZICMJFPT4yy1aE.pdf','documents/aaut2ZajoCVLR4BvEixLKSKBidcCMOgyzLWuKrqx.pdf','documents/1pj6zsq35UHdAWfw8r9WxsppWtSibsCPm1Bujc3Q.pdf','documents/WMfKmcGHX95cVQphVpxXS98MLzml1JdzoLLVfjZG.pdf','documents/M6fZAdyexNWnteRrju96FSFUHETfOP8ldATLMtB2.pdf','Pretoria',NULL,'Gauteng',NULL,NULL,'2023-09-18 11:08:18','2023-09-22 11:24:41',1),(2,1,0,'2023-09-18','Quintus','Middlename','van der Walt','1984-04-02','8404025024080',NULL,NULL,'Male','White','Worker','This is my bio, with something about myself.','quintus@thinktank.co.za',NULL,'$2y$10$0FX3Xs3YPzAmvq4Qhdfl2eMRn87BLjau1009ywHiUcfKC9TCuuI42',NULL,NULL,NULL,'photos/oOlvVhPcSWHBu9bH5Vj0TpYx27DhOOC1UgN9etwt.png','123 Testing Road','documents/4tTP2mCZALJ1xdeEthOC57b77QYmeuOth1QC4FMN.jpg','documents/b3ojSJ7t1clgxkZULqpyMvapDpS3z4mrOgAqjlp3.jpg','documents/dpacV1uyFS5hFkZjfyJ28Qd0soXOmzuskQAH34H3.jpg','documents/woapvcuKll8p1r5CxORoHahyKC030DEBiWJD9iq1.jpg','documents/B8leZP4B1Mil5AG8ragRaXYLz5jXRnpyikzAOPJW.pdf',NULL,'Testonia',NULL,'Gauteng',NULL,NULL,'2023-09-18 11:21:46','2023-09-18 11:50:48',1),(3,1,0,'2023-09-18','Steph',NULL,'Lock','1993-12-18','9312180143083',NULL,NULL,'Female','White',NULL,NULL,'steph@thinktank.co.za',NULL,'$2y$10$vac9WAr6CpnaBNTHHK2You9F4F7BOJT1r0o4zoRo1VFQg1yzCk0o2',NULL,NULL,NULL,NULL,'12 Street, Road','documents/iZoDpP1GwI3tBuZFVHBhDPvN4bANnGuLz91mhCBz.png','documents/SsG8vDJrXH2rZsAFIyts60WlYycMz725aldJR9qd.jpg','documents/VAIL3qrAWTOz2viyDJe7EmDNzA3dGHskCxyzVqHQ.png','documents/gYsWEbP2Ld7W8TvXU9MoIHyTkNduebdZeB1mTb8Q.pdf','documents/OlIsdh1E6HVoyJOxeUeiC0hHmOavap8vqNcGnQSr.pdf','documents/Qx0YLp7oRhQFDiTp4FBVOcVEkXoknQykbv41BYup.pdf','Pretoria',NULL,'Gauteng',NULL,NULL,'2023-09-18 11:35:27','2023-09-22 10:00:27',1),(4,1,0,'2023-09-19','Susandra','Jacoba','van Wyk','1971-01-12','7101120212088',NULL,NULL,'Female','White',NULL,NULL,'vanwyksusandra@gmail.com',NULL,'$2y$10$HPCZTOwRDfuw/DBXnFt36ePCD.Haqhrc7vT.THEZs2SFyBLdsGn1q',NULL,NULL,NULL,'photos/hnlPolXAJNdlVAkvXZ7lr4Y9Mf3PHHE44jX05Jh7.png','279 Sidney Avenue, 26 Waterkloof Village, Waterkloof','documents/5qsjzp2Hu1rvPvYynU49JDFw9vI6K5TEhDoLlq2K.pdf','documents/EikMh2xTRC9XpUlMTBADVA6b4soySzEwc2crcIcW.pdf','documents/q1lE4Olx1FktkIl6V7Hfkucd3NiR5Y1XkeB37qDs.pdf','documents/pIdcDxp2efyNzOBlqomBwE1UrOMI3GYqi8zBG4Db.pdf','documents/1qypPIRJDT9MvNc0hQirDhfRDJ4U3hA0xJ4I690r.pdf','documents/LOSCwRY51yP0NvxjRvoYnaL2dclnfh2ZHPs3BcLV.pdf','Pretoria',NULL,'Gauteng',NULL,NULL,'2023-09-19 06:48:44','2023-09-20 10:52:46',1),(5,1,1,NULL,'Cherene ',NULL,'Cook','1992-01-23','9201230176088',NULL,NULL,'Female','White',NULL,NULL,'cherenecook@gmail.com1',NULL,'$2y$10$4ytvR9o3djYyaSEjgRtNW./I/OxorlxjF3fsF7fqUTS1cvNX9fis.',NULL,NULL,NULL,'photos/bCb5wBySks2IXHtUjepEpg2B2fKOlUKq6PTFTK2p.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2023-09-19 15:21:05','2023-09-19 15:24:41',1),(6,1,1,NULL,'Susandra Test',NULL,'van Wyk Test','1971-01-12','7101120212088',NULL,NULL,'Female','White',NULL,NULL,'susandra@icloud.com',NULL,'$2y$10$Ytd94w4unZjYA2eZKTa2juinLk8BNlmxAHxdwQkL8BX5Nk2zNi4tC',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2023-09-20 08:59:15','2023-09-20 08:59:52',1),(7,1,0,'2023-09-21','Steph',NULL,'Lock','1993-12-18','','72819273',NULL,'Female','White',NULL,NULL,'Stephlotter5@gmail.com',NULL,'$2y$10$EvlIeAv9KyEUkmydhfsJCuQ6yCRGF2FxpX1cg0q88wtGQ0/7U70hK',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2023-09-20 11:06:57','2023-09-20 11:07:48',1),(8,1,1,NULL,'Susandra Test 2',NULL,'van Wyk','1971-01-12','7101120212088',NULL,NULL,'Female','White',NULL,NULL,'susandra@wrapup.co.za',NULL,'$2y$10$xerg8mePjmLkP9Gp2A55AOO81/hJxCNgpZD2t8C6jpYnphH8gcHzK',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2023-09-20 12:20:28','2023-09-20 12:20:46',1),(9,1,0,'2023-09-20','Cherene',NULL,'Cook','1992-01-23','9201230176088',NULL,NULL,'Female','White',NULL,NULL,'cherenecook@gmail.com',NULL,'$2y$10$zmziEAsIlHPGWouJnMTZ0.UheJdfIodApSGmpDCiDcPuTzEgWv8o6',NULL,NULL,NULL,'photos/o5k98mtoyy3GU3YLX7kn9aDql34kQFccm4TAzJ2t.png','hh','documents/GUXiEpvCZXYX6rF4smbvccm71CHgZzrZiGJLokZa.png',NULL,NULL,NULL,NULL,NULL,'cc',NULL,'Gauteng',NULL,'SV76nLFiaQPRptR0aXnuGWjatnpcjmuRsy08qOltHxImxL9RPSvJfkmkCMU4','2023-09-20 12:44:25','2023-11-22 03:16:28',1),(10,0,0,NULL,'Gabriel','Werner','Keyser','1969-09-21','6904215099083',NULL,NULL,'Male','White',NULL,NULL,'gawie@tribix.co.za',NULL,'$2y$10$/Kea2izaoY818kpSdsSef.vOZ7aoVuaeb04WtdLoSAoCcCvIoKgUO',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2023-09-20 15:34:00','2023-09-20 15:34:00',0),(11,1,0,'2023-09-21','PIERRE',NULL,'VAN WYK','1950-11-09','5011095121085',NULL,NULL,'Male','White','Medical doctor',NULL,'drpierrevanwyk@gmail.com',NULL,'$2y$10$DImQakU4skYhSdbQpBNM6ezQH8RrvVmsSmV3ILqO7x/ojvUcYqdG2',NULL,NULL,NULL,'photos/ekQWg5710lCuFvODvD8qnH8fmhWkEokgOMEh7cI8.jpg','House nr 26, Waterkloof Village, Sidneystreet',NULL,NULL,NULL,NULL,NULL,NULL,'PRETORIA',NULL,'Gauteng',NULL,NULL,'2023-09-21 08:39:25','2023-11-21 11:20:27',1),(12,0,0,NULL,'Steph',NULL,'Lock','1993-12-18','9312180143083',NULL,NULL,'Female','White',NULL,NULL,'stephlock12@gmail.com',NULL,'$2y$10$B22x/Rzgr4G9Uf9ftvH4Oeaz8xRgqlP13euLXI90MLfAu.gk9llo2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2023-09-21 09:24:20','2023-09-21 09:24:20',0),(13,1,1,NULL,'Lorne',NULL,'Janse','1996-04-03',NULL,'728934720364',NULL,'Female','White',NULL,NULL,'lorne@thinktank.co.za',NULL,'$2y$10$/8nektoqNyUH1jRm9pC9xeEIHGCI8/cWGacQzOfQGS2teL9p7vjLS',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2023-09-21 09:33:33','2023-09-21 09:33:33',0),(14,1,0,'2023-09-21','Louis',NULL,'Claassens','2004-11-21','0411235130084',NULL,NULL,'Male','White',NULL,NULL,'louisclaassens@icloud.com',NULL,'$2y$10$TLJtyPG4F1CccsRwQykfk.VgXiN9tpz5LPZsiW/TL/ER2WZUuzh0.',NULL,NULL,NULL,NULL,'17 Graham Street',NULL,NULL,NULL,NULL,NULL,NULL,'Welkom',NULL,'Free State',NULL,NULL,'2023-09-21 13:39:14','2023-09-21 13:40:43',1),(15,0,0,NULL,'Alexia','Jeanne','Van Der Berg','1993-07-02','9307020231080','9307020231080',NULL,'Female','White',NULL,NULL,'alexiavdb93@gmail.com',NULL,'$2y$10$jIdikRImBrq3AUgaQwla5u1xU29h9z4qsqPTUlNvFlswLPgaUJbb2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2023-09-22 09:30:16','2023-09-22 09:30:16',0),(16,1,1,NULL,'Tyron',NULL,'Cook','1989-05-11','8905115152086',NULL,NULL,'Male','White',NULL,NULL,'tyroncook918@gmail.com',NULL,'$2y$10$G2JcLzGD01qOVnDVHCo8LugSNOik/3UoxXeSaASaYKVuG/T/lFKK2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'vHHMiMhySND0VbUf2AKcyxfzfhcm6Ijs473t7JMh2mOVk776iyxhEZTu5HYi','2023-09-22 12:53:35','2023-10-25 11:38:30',1),(17,1,1,NULL,'Alexia','Jeanne','Van Der Berg','1993-07-02','9307020231080','9307020231080',NULL,'Female','White','Sales ',NULL,'alexia@aramex.com',NULL,'$2y$10$Za5lE3zjuFAQc0BqyR0/JuMAq6moEFZYhZv9PBqj/Hk7lYbG4Jtqu',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2023-09-27 05:52:44','2023-09-27 05:52:59',1),(20,1,1,NULL,'Kudz',NULL,'omer','2023-10-04','','EN822781','','Male','Black','','','',NULL,'$2y$10$kJNw5pWtiSwtEEjN/gzEyu/Gg.JNuesNL4DsmW4HoRecFGVHyI5va',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2023-10-01 05:59:06','2023-10-01 06:10:11',1),(21,0,0,NULL,'Rione',NULL,'Mortlock','1994-05-06','9405060074087','9405060074087',NULL,'Female','White',NULL,NULL,'rione.pret@gmail.com',NULL,'$2y$10$VgVe4XPbzpVgZjjuXfnNjeb.ccngPMtKe1Xw9lD8A0fIAbKjJrtHO',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2023-10-04 10:12:08','2023-10-04 10:12:08',0),(22,0,0,NULL,'Susandra',NULL,'van Wyk','1971-01-12','7101120212088',NULL,NULL,'Female','White',NULL,NULL,'info@svwlaw.co.za',NULL,'$2y$10$9sNWH3ELvkY6sdMS40Yu1ehM/5cCFLjaFjK4VxgzTMnY7wb3jvOJ2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2023-10-10 10:06:12','2023-10-10 10:06:12',0),(28,0,0,NULL,'Susandra',NULL,'van Wyk','1971-01-12','7101120212088',NULL,NULL,'Male','White',NULL,NULL,'info@wrapup.co.za',NULL,'$2y$10$Tw31L0sGxBICLz7Xy3EnLeDYR4BIWNmV377KCoZSYvvIi6sLoqUje',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2023-10-13 09:52:06','2023-10-13 09:52:06',0),(29,0,0,NULL,'Tyron',NULL,'Cook','1890-05-11','8905115152086',NULL,NULL,'Male','White',NULL,NULL,'tyron@dotondelivery.co.za',NULL,'$2y$10$qYaNRiWPJvGNvrcLuDEwNOcVxwhYhgYIZZzP.MTXbcCQMkeBBetz2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2023-10-14 07:05:34','2023-10-14 07:05:34',0),(47,1,0,'2023-10-26','junior','wa kuba','Nsenga','1984-11-01','8812176193183','op0392006',NULL,'Male','Black',NULL,NULL,'nsenga.ns1712@gmail.com',NULL,'$2y$10$ummo/KDWAXKEw5x3FX8UFeO0GMV56KQKR7jzj8YDJxuef7ziqvNnK',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2023-10-25 08:44:09','2023-10-25 09:08:45',1),(51,0,0,NULL,'Mariette',NULL,'van Schalkwyk','1961-01-11','6111010022084',NULL,NULL,'Female','White',NULL,NULL,'mariettevanschalkwyk18@gmail.com',NULL,'$2y$10$L5lkInzl6UX8qPPvopVcAOWyBQzKThlBcE2lOt3V/6BGCeg1YZsI.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2023-10-25 09:52:52','2023-10-25 09:52:52',0),(52,1,1,NULL,'Susandra',NULL,'van Wyk','1971-01-12','7101120212088',NULL,NULL,'Female','White',NULL,NULL,'hello@wrapup.co.za',NULL,'$2y$10$xUY2inxi/3iBJ1kK1el9heQDdCkKXlyA9uvEwMGlge.u0Ql2rBiKC',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2023-10-25 10:18:42','2023-10-25 10:18:42',0),(53,0,0,NULL,'Pierre',NULL,'van Wyk','1950-11-09','5011095121085',NULL,NULL,'Male','White',NULL,NULL,'info@deceased-estates-selfhelp-programme.co.za',NULL,'$2y$10$x3g3t/f9tBg2zxhqSFxvEep7wMF5rh//2N2kzcVh3iMmLclQlZ7zy',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2023-10-25 10:21:17','2023-10-25 10:21:17',0),(55,0,0,NULL,'Cherene',NULL,'Cook','1992-01-23','9201230176088',NULL,NULL,'Female','White',NULL,NULL,'cherenevwyk@gmail.com',NULL,'$2y$10$xO2KQgKRyL.zfz.LoeFhLeqAMG0VR4YbCy8KOFV6k6xnlBlxiPUC.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2023-10-25 12:41:50','2023-10-25 12:41:50',0),(70,1,1,NULL,'test',NULL,'test','2023-10-27',NULL,'EN00000',NULL,'Male','Black',NULL,NULL,'test@gmail.com',NULL,'$2y$10$UAcZNGhiYwTH.wG1imUL0urUFvqyu5ZhrghmEiHkHk6MGZPG.qa3u',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2023-10-27 09:11:19','2023-10-27 09:12:52',1),(71,1,0,'2023-10-27','Cherene',NULL,'van Wyk','1992-01-23','9201230176088',NULL,NULL,'Female','White',NULL,NULL,'cookcherene@gmail.com',NULL,'$2y$10$KNi9K2gSxL/gBcMaRG.2oewM2BxxnxI6ii/8/7Ajw4y1IBM.bbPZm',NULL,NULL,NULL,NULL,NULL,'documents/9sVcg12tfAe9ryt5myt3taQOpj9YuPoCtQYNBxO1.jpg','documents/SVupsVr4UWmiIF3OzA8ZlkFoobdWqgLdiaOHbgZQ.jpg',NULL,'documents/IabzqVdPpWr2Xs8IYrK3NTZ5bGjOTSgXlczIJKO1.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2023-10-27 10:28:31','2023-10-27 10:41:31',1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `will_options`
--

DROP TABLE IF EXISTS `will_options`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `will_options` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `profile` int(11) NOT NULL,
  `executor` int(11) NOT NULL,
  `spouse` int(11) NOT NULL DEFAULT 0,
  `minor_children_beneficiary` int(11) NOT NULL DEFAULT 0,
  `guardian` int(11) NOT NULL DEFAULT 0,
  `testamentary_trust` int(11) NOT NULL DEFAULT 0,
  `inheritance` int(11) NOT NULL DEFAULT 0,
  `specific_inheritance` int(11) NOT NULL DEFAULT 0,
  `remainder_inheritance` int(11) NOT NULL DEFAULT 0,
  `simultaneous_death` int(11) NOT NULL DEFAULT 0,
  `funeral_wishes` int(11) NOT NULL DEFAULT 0,
  `witnesses` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `will_options`
--

LOCK TABLES `will_options` WRITE;
/*!40000 ALTER TABLE `will_options` DISABLE KEYS */;
INSERT INTO `will_options` (`id`, `user_id`, `profile`, `executor`, `spouse`, `minor_children_beneficiary`, `guardian`, `testamentary_trust`, `inheritance`, `specific_inheritance`, `remainder_inheritance`, `simultaneous_death`, `funeral_wishes`, `witnesses`, `created_at`, `updated_at`) VALUES (1,1,1,1,1,1,1,1,0,1,1,1,1,1,'2023-09-18 11:45:39','2023-09-18 11:45:39'),(2,2,1,1,1,0,0,0,1,0,0,0,1,1,'2023-09-18 17:36:35','2023-09-18 17:36:35'),(3,3,1,1,1,1,1,0,0,1,1,1,1,1,'2023-09-19 05:15:54','2023-09-19 12:42:51'),(10,4,1,1,1,1,1,1,0,1,1,0,1,1,'2023-10-30 05:40:52','2023-10-30 05:40:52'),(15,9,1,1,1,1,1,1,0,1,1,1,1,1,'2023-11-22 03:16:02','2023-11-22 03:16:02');
/*!40000 ALTER TABLE `will_options` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `will_spouses`
--

DROP TABLE IF EXISTS `will_spouses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `will_spouses` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `middle_name` varchar(255) DEFAULT NULL,
  `surname` varchar(255) NOT NULL,
  `id_number` varchar(255) NOT NULL,
  `marital_in_community_of_property` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `will_spouses`
--

LOCK TABLES `will_spouses` WRITE;
/*!40000 ALTER TABLE `will_spouses` DISABLE KEYS */;
/*!40000 ALTER TABLE `will_spouses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `will_uploads`
--

DROP TABLE IF EXISTS `will_uploads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `will_uploads` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `will_url` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `will_uploads`
--

LOCK TABLES `will_uploads` WRITE;
/*!40000 ALTER TABLE `will_uploads` DISABLE KEYS */;
/*!40000 ALTER TABLE `will_uploads` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `witnesses`
--

DROP TABLE IF EXISTS `witnesses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `witnesses` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `middle_name` varchar(255) DEFAULT NULL,
  `surname` varchar(255) NOT NULL,
  `id_number` varchar(255) NOT NULL,
  `phone_number` varchar(255) DEFAULT NULL,
  `address` text DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `postal_code` varchar(255) DEFAULT NULL,
  `province` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `witnesses`
--

LOCK TABLES `witnesses` WRITE;
/*!40000 ALTER TABLE `witnesses` DISABLE KEYS */;
INSERT INTO `witnesses` (`id`, `user_id`, `first_name`, `middle_name`, `surname`, `id_number`, `phone_number`, `address`, `city`, `postal_code`, `province`, `created_at`, `updated_at`) VALUES (1,1,'Mbekezele','Benjamin','Moyo','MB1234567','0987654321','21 Jump Street','Pretoria','','Gauteng','2023-09-18 11:49:51','2023-09-18 11:49:51'),(2,1,'Joseph','','Katumba','JK12345678','0987654321','22 Jump Street','Pretoria','','Gauteng','2023-09-18 11:49:51','2023-09-18 11:49:51'),(3,2,'test','test','test','12345678911111','0121231234','74 Jerome street\nLynnwood Glen, Pretoria East','Pretoria','','Gauteng','2023-09-18 17:41:29','2023-09-18 17:41:29'),(4,2,'test','test','test','8202170001089','4556677788889','74 Jerome street\nLynnwood Glen, Pretoria East','Pretoria','','Gauteng','2023-09-18 17:41:29','2023-09-19 05:24:41'),(7,4,'Chris','','Burger','Zambia registration number 273245/61/1','','259 Sidney Avenue','Pretoria','','Gauteng','2023-10-30 05:44:30','2023-10-30 05:44:30'),(8,4,'Piet','','Burger','Zambia registration number 273245/61/1','','259 Sidney Avenue','Pretoria','','Gauteng','2023-10-30 05:44:30','2023-10-30 05:44:30');
/*!40000 ALTER TABLE `witnesses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'wrapupco_wrap_up'
--

--
-- Dumping routines for database 'wrapupco_wrap_up'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-11-23  5:37:47
