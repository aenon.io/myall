@extends('layouts.landing')
@section('content')
    <section class="wrapper section-2">
        <div class="container">
            <div class="row mt-10 mb-10 mt-lg-15">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <livewire:landing.partials.contact />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="wrapper">
        <div class="container py-10">
            <div class="row align-center-flex">
                <div class="p-5">
                    <div class="row" style="align-items: center;">
                        <div class="col-md-4">
                            <h3 class="page-title h3-title-style">ABOUT WRAPUP</h3>
                            <p class="text-pink"><b class="text-pink">Get to know the team behind it all.</b></p>
                        </div>
                        <div class="col-md-6">
                            <p class="text-blue">We at MyAll are committed to creating innovative solutions that make a
                                positive impact by simplifying the responsibility for individuals to organise their core
                                documents and legal entities in one efficient space. Our shared commitment drives us to
                                deliver exceptional service and a unique product that not only meets but exceeds our
                                customers' expectations.</p>
                            <p>We look forward to the opportunity to stand beside each of you as unique individuals,
                                supporting you in effectively organising all crucial details and paperwork in one dynamic
                                and efficient space – the MyAll Platform.</p>
                        </div>
                    </div>
                </div>
                <div class="card-box-style rounded p-5 marg-bottom-40">
                    <div class="row">
                        <div class="col-md-4">
                            <pie id="no-border" class="ten">
                                <img class="" src="{{ asset('img/contact/susandra.png') }}" alt="Images">
                                <!--<a href="http://52.73.227.231/profile/edit"><i class="bx bxs-edit header_icons bg-red"></i></a>-->
                            </pie>
                        </div>
                        <div class="col-md-8">
                            <h3 class="page-title h3-title-style">ABOUT DR SUSANDRA VAN WYK</h3>
                            <p class="text-pink"><b class="text-pink">Founder and Chief Executive Officer</b></p>

                            <div class="accordion" id="accordionAbout">
                                <div class="accordion-item marg-bottom">
                                    <h2 class="accordion-header" id="headingOne">
                                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                            data-bs-target="#me" aria-expanded="false" aria-controls="me">
                                            EXPERIENCE AND SPECIALISATION
                                        </button>
                                    </h2>
                                    <div id="me" class="accordion-collapse collapse" aria-labelledby="headingOne"
                                        data-bs-parent="#accordionAbout" wire:ignore.self>
                                        <div class="accordion-body">
                                            <div class="marg-bottom">
                                                <div>Dr. Susandra van Wyk is an experienced attorney, conveyancer, and
                                                    notary of the High Court of South Africa, specialising for 32 years in
                                                    Succession Law, Drafting of Wills, Estate Law, Estate Administration,
                                                    and Estate Planning. </div>
                                                </br>
                                                She earned two of her six degrees, BIuris and LLB, while working as an
                                                estate examiner and acting Assistant Master at the Bloemfontein branch
                                                Master of the High Court. As a fiduciary specialist advising the public and
                                                attorneys on estate law matters, she completed her master’s degree in estate
                                                law and an advanced diploma in insolvency law.
                                                In addition, she pursued her academic interests during her part-time tenure
                                                as a lecturer and researcher, earning the degrees BA Hons, MA, and DLitt et
                                                Phil.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion-item marg-bottom">
                                    <h2 class="accordion-header" id="headingOne">
                                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                            data-bs-target="#edu" aria-expanded="false" aria-controls="me">
                                            ACADEMIC BACKGROUND
                                        </button>
                                    </h2>
                                    <div id="edu" class="accordion-collapse collapse" aria-labelledby="headingOne"
                                        data-bs-parent="#accordionAbout" wire:ignore.self>
                                        <div class="accordion-body">
                                            <div class="marg-bottom">
                                                <div>Dr. van Wyk has published various academic articles, presented lectures
                                                    and conference presentations, and served as an external examiner for
                                                    masters and doctoral students, an editor and reviewer of academic
                                                    journals, and as an external examiner for the deceased estate
                                                    administration LLB module at North-West University.</div>
                                                </br>
                                                Dr. van Wyk finds great satisfaction in the practical and theoretical
                                                application of Estate Law, including administering deceased estates and
                                                drafting Wills, providing legal guidance to clients and fellow lawyers, and
                                                presenting academic articles and talks at universities.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <p style="font-size: 12px;letter-spacing: 1.2px;" class="h3-width">Want to learn more about
                                Susandra or need any legal services? Visit <a href="https://svwlaw.co.za" target="blank"
                                    class="link-font-bold text-blue">svw.co.za</a> for more info.</p>
                            <a href="https://www.svwlaw.co.za" target="_blank" class="btn btn-red rounded-pill">SVW
                                ATTORNEYS</a>
                        </div>
                    </div>
                </div>
                <div class="card-box-style rounded p-5 marg-bottom-40">
                    <div class="row">
                        <div class="col-md-4">
                            <pie id="no-border" class="ten">
                                <img class="" src="{{ asset('img/contact/cherene_II.png') }}" alt="Images">
                                <!--<a href="http://52.73.227.231/profile/edit"><i class="bx bxs-edit header_icons bg-red"></i></a>-->
                            </pie>
                        </div>
                        <div class="col-md-8">
                            <h3 class="page-title h3-title-style">ABOUT CHERENE COOK</h3>
                            <p class="text-pink"><b class="text-pink">Sales & Marketing Director and Partner</b></p>

                            <div class="accordion" id="accordionChery">

                                <div class="accordion-item marg-bottom">
                                    <h2 class="accordion-header" id="headingOne">
                                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                            data-bs-target="#chery" aria-expanded="false" aria-controls="me">
                                            DIVERSE CAREER BACKGROUND
                                        </button>
                                    </h2>
                                    <div id="chery" class="accordion-collapse collapse" aria-labelledby="headingOne"
                                        data-bs-parent="#accordionChery" wire:ignore.self>
                                        <div class="accordion-body">
                                            <div class="marg-bottom">
                                                <p>Cherene Cook interaction with diverse customers and groups across various
                                                    industries has not only enriched her understanding of people's needs and
                                                    desires but has also fuelled her creativity in designing products that
                                                    resonate deeply with users. Cherene's ability to empathise with
                                                    individuals from all walks of life further enhances her capacity to
                                                    create products that cater to a wide spectrum of preferences and
                                                    requirements.</p>
                                                <p>Cherene Cook possesses a deep and unwavering passion for product
                                                    development and design. This fervour has consistently driven her to
                                                    excel in her professional endeavours. Cherene's genuine love for
                                                    crafting and refining products is evident in her commitment to creating
                                                    exceptional user experiences. Whether it's the seamless integration of
                                                    innovative features or the meticulous attention to aesthetic details,
                                                    Cherene's devotion to product development and design shines through in
                                                    every project she undertakes.</p>
                                                <p>Drawing from her extensive background in internet marketing, security,
                                                    logistics, the medical field, and the fashion industry, Cherene's unique
                                                    insights from these diverse domains have uniquely positioned her as a
                                                    versatile force in the realm of product development. Her holistic
                                                    understanding of various sectors allows her to approach design
                                                    challenges from multiple angles, resulting in solutions that are both
                                                    innovative and practical.</p>
                                                <p>With her diverse skill set, a keen eye for detail, and an innate ability
                                                    to capture the essence of what users truly desire, Cherene continues to
                                                    contribute meaningfully to the world of innovation, enriching lives
                                                    through the products she helps bring to fruition.</p>
                                                {{--
                                            <div>With over 13 years of extensive hands-on experience across various industries, I have honed my sales and marketing skills while cultivating a passion for brand building and connecting with diverse individuals.</div>
                                           </br>
                                           This invaluable experience forms the foundation of my professional journey and has provided me with a solid footing to excel in my field. The trust I have earned from clients and colleagues, along with my proven track record of accomplishments, speaks volumes about my expertise and capabilities.
                                            --}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{--
                            <div class="accordion-item marg-bottom">
                                <h2 class="accordion-header" id="headingOne">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#car" aria-expanded="false" aria-controls="me">
                                        DIVERSE CAREER BACKGROUND
                                    </button>
                                </h2>
                                <div id="car" class="accordion-collapse collapse" aria-labelledby="headingOne" data-bs-parent="#accordionChery" wire:ignore.self>
                                    <div class="accordion-body">
                                        <div class="marg-bottom">
                                            <div>Throughout my career, I have had the privilege of working in a range of industries, including internet marketing, security, logistics, the medical field, and the fashion industry. </div>
                                           </br>
                                           <div>
                                           This diverse background has given me a comprehensive understanding of different sectors and allowed me to develop a broad skill set. In the realm of internet marketing, I have acquired expertise in digital marketing strategies, online advertising, and successful customer engagement.
                                           </div>
                                           </br>
                                           <div>In the security industry, I have specialized in risk assessment, loss prevention, and safeguarding individuals and valuable assets. My experience in logistics has provided me with a strong grasp of supply chain management, efficient operations, and meeting demanding deadlines. Furthermore, my time in the medical industry has enabled me to navigate the complexities of healthcare, build strong relationships with healthcare professionals, and effectively promote medical products. Additionally, my exposure to the fashion industry has provided insights into brand development, management, trends, and consumer behaviour.</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-item marg-bottom">
                                <h2 class="accordion-header" id="headingOne">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#some" aria-expanded="false" aria-controls="me">
                                        SOME BACKGROUND
                                    </button>
                                </h2>
                                <div id="some" class="accordion-collapse collapse" aria-labelledby="headingOne" data-bs-parent="#accordionChery" wire:ignore.self>
                                    <div class="accordion-body">
                                        <div class="marg-bottom">
                                            <div>My background in sales and marketing, fuelled by a passion for product development and a deep desire to make a positive difference and impact in people's lives, when combined with Susandra's exceptional legal expertise, creates a dynamic and unique blend of skills.</div>
                                           </br>
                                           We are committed to creating innovative solutions, making a positive impact, and enhancing the lives of individuals. Our shared commitment drives us to deliver exceptional products and services that not only meet but exceed the expectations of our customers.
                                        </div>
                                    </div>
                                </div>
                            </div>
                            --}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>
@endsection
