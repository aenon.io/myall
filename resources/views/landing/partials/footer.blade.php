<footer class="bg-blue mt-auto">
  <div class="pb-13 pb-md-5 container">
    <div class="row gy-6 gy-lg-0 pt-5">
      <div class="col-sm-12 col-md-6 col-lg-3">
        <ul class="navbar-nav align-items-center ms-auto flex-row">
          <li class="nav-item d-none d-md-block">
            <nav class="nav justify-content-start text-start">
              <a href="https://www.facebook.com/wrapuponline" target="_blank"
                class="footer-social-icons facebook"><i
                  class="fa fa-facebook-f"></i></a>
              <a href="https://www.instagram.com/wrapuponline/" target="_blank"
                class="footer-social-icons instagram"><i
                  class="fa fa-instagram"></i></a>
              <a href="https://www.linkedin.com/company/wrapupza/"
                target="_blank" class="footer-social-icons linkedin"><i
                  class="fa fa-linkedin" style="padding: 5px 7px"></i></a>
            </nav>
          </li>
        </ul>
      </div>
      <div
        class="col-sm-12 col-md-6 col-lg-3 footer-logo text-lg-start text-center">
        <img src="{{ asset('img/footer_logo.png') }}">
      </div>
      <div class="col-sm-12 col-md-12 col-lg-6">
        <div class="row">
          <div class="col-md-4 d-grid mb-3">
            <a href="{{ url('login') }}" class="btn btn_turquoise">LOGIN</a>
          </div>
          <div class="col-md-4 d-grid mb-3">
            <a href="{{ url('pricing') }}"
              class="btn btn-red rounded-pill">REGISTER</a>
          </div>
          <div class="col-md-4 text-lg-start pt-lg-2 text-center">
            <p>Not a member yet? <br />Register your profile today.</p>
          </div>
        </div>
      </div>
    </div>
    <div class="row footer-bottom">
      <div class="col-md-9 pt-2">
        <p>&copy; Copyright {{ date('Y') }} MyAll Pty Ltd Registration
          Number 0000/000000/00. All Rights Reserved</p>
      </div>
      <div class="col-md-3 text-lg-end pt-2 text-center">
        <a href="{{ url('docs/MyAll Privacy Policy.pdf') }}"
          target="_blank">Privacy Policy</a>
        &nbsp;&nbsp;
        <a href="{{ url('docs/MyAll Terms and Conditions.pdf') }}"
          target="_blank">Terms & Conditions</a>
        &nbsp;&nbsp;
        <a href="{{ url('docs/MyAll PAIA Manual.pdf') }}"
          target="_blank">PAIA Manual</a>
      </div>
    </div>
  </div>
</footer>
