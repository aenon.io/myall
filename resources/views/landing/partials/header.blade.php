<header class="wrapper">
  <nav class="navbar navbar-expand-lg center-nav transparent caret-none">
    <div class="flex-lg-row align-items-center container flex-nowrap">
      <div class="navbar-brand w-100 mt-3">
        <a href="{{ url('/') }}">
          <img src="{{ asset('img/logo.svg') }}"
            srcset="{{ asset('img/logo.svg') }}" alt="MyAll" />
        </a>
      </div>
      <div class="navbar-collapse offcanvas offcanvas-nav offcanvas-start">
        <div class="offcanvas-header d-lg-none">
          <h3 class="fs-30 mb-0 text-white">
            MyAll
          </h3>
          <button type="button" class="btn-close btn-close-white"
            data-bs-dismiss="offcanvas" aria-label="Close"></button>
        </div>
        <div class="offcanvas-body ms-lg-auto d-flex flex-column h-100">
          <ul class="navbar-nav">
            {{-- <li class="nav-item"> --}}
            {{--     <a class="nav-link" href="{{ url('/') }}">Home</a> --}}
            {{-- </li> --}}
            <li class="nav-item">
              <a class="nav-link" href="#"
                wire:click.prevent="authenticateUser('login')">
                Contact Us
              </a>
            </li>
          </ul>
          <div class="offcanvas-footer d-lg-none">
            <div>
              <nav class="nav social social-white mt-4">
                <a href="" target="_blank">
                  <i class="uil uil-facebook-f"></i>
                </a>
                <a href="" target="_blank">
                  <i class="uil uil-instagram"></i>
                </a>
                <a href="" target="_blank">
                  <i style="padding: 6px 8px;"
                    class="fa-brands fa-linkedin-in"></i>
                </a>
              </nav>
            </div>
          </div>
        </div>
      </div>
      <div class="navbar-other w-100 d-flex">
        <div class="d-none d-md-block ms-5">
          <a href="{{ url('landing/contact-us') }}" class="btn btn-filled-pink">
            LOGIN
          </a>
        </div>
        <ul class="navbar-nav align-items-center ms-auto flex-row">
          <li class="nav-item d-none d-md-block">
            <nav class="nav social justify-content-start text-start">
              <a href="" target="_blank">
                <i class="uil uil-facebook-f"></i>
              </a>
              <a href="" target="_blank">
                <i class="uil uil-instagram"></i>
              </a>
              <a href="" target="_blank">
                <i style="padding: 6px 8px;"
                  class="fa-brands fa-linkedin-in"></i>
              </a>
            </nav>
          </li>
          <li class="nav-item d-lg-none ms-auto">
            <button
              class="hamburger offcanvas-nav-btn ms-auto"><span></span></button>
          </li>
        </ul>
      </div>
    </div>
  </nav>
</header>
