<!doctype html>
<html lang="en">

  <head>
    <meta charset="utf-8">
    <meta name="viewport"
      content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link
      href="https://fonts.googleapis.com/css2?family=Inter:ital,wght@0,100..900;1,100..900&display=swap"
      rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet"
      href="{{ asset('assets/css/owl.theme.default.min.css') }}">
    <link rel="stylesheet"
      href="{{ asset('assets/css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/animate.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/remixicon.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/boxicons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/iconsax.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/metismenu.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/simplebar.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/calendar.css') }}">
    <link rel="stylesheet"
      href="{{ asset('assets/css/sweetalert2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/jbox.all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/editor.css') }}">
    <link rel="stylesheet"
      href="{{ asset('assets/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/loaders.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/header.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/sidebar-menu.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/footer.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
    {{-- <link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}"> --}}
    <link rel="stylesheet" href="{{ asset('assets/css/responsive.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/mya.css') }}">
    @livewireStyles
    @stack('styles')

    <link rel="icon" type="image/svg+xml"
      href="{{ asset('img/logo-icon.svg') }}">
    <title>MyAll</title>

    {{--  Google tag (gtag.js)  --}}
    <script async src=https://www.googletagmanager.com/gtag/js?id=G-11BFMZ4TL7>
    </script>
    <script>
      window.dataLayer = window.dataLayer || [];

      function gtag() {
        dataLayer.push(arguments);
      }
      gtag('js', new Date());
      gtag('config', 'G-11BFMZ4TL7');
    </script>
  </head>

  <body class="@if (Request::is('organiser/*')) bg-white @endif">
    <div class="all-section-area">
      @include('partials.nav')
      <main class="myallMainApp style-two">
        {{-- <main class="main-content-wrap style-two myallMainApp">  --}}
        {{ $slot }}
      </main>
    </div>
    {{-- <div class="go-top">  --}}
    {{--   <i class="ri-arrow-up-s-fill"></i>  --}}
    {{--   <i class="ri-arrow-up-s-fill"></i>  --}}
    {{-- </div>  --}}

    {{-- Jquery Min JS  --}}
    <script src="{{ asset('assets/js/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('assets/js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('assets/js/metismenu.min.js') }}"></script>
    <script src="{{ asset('assets/js/simplebar.min.js') }}"></script>
    <script src="{{ asset('assets/js/geticons.js') }}"></script>
    <script src="{{ asset('assets/js/sweetalert2.all.min.js') }}"></script>
    <script src="{{ asset('assets/js/editor.js') }}"></script>
    <script src="{{ asset('assets/js/form-validator.min.js') }}"></script>
    <script src="{{ asset('assets/js/custom.js') }}"></script>
    {{-- <script src="{{ asset('assets/js/familytreejs/familytree.js') }}"></script> --}}
    @livewireScripts
    <script src="{{ asset('assets/js/easy-number-separator.js') }}"></script>
    @stack('scripts')
    <script
      src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.3/jquery.inputmask.bundle.min.js">
    </script>
    <script>
      $(document).ready(function() {
        $(":input").inputmask();
      });
    </script>
    {{-- INFO: Google Translate implementation --}}
    <script type="text/javascript">
      function googleTranslateElementInit() {
        new google.translate.TranslateElement({
            pageLanguage: 'en'
          },
          'google_translate_element'
        );
      }
    </script>
    <script
      src="https://translate.google.com/translate_a/element.js?cb=googleTranslateElementInit">
    </script>
  </body>

</html>
