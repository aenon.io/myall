<!DOCTYPE html>
<html lang="en">

  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>MyAll</title>
    <link rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="shortcut icon" type="image/svg+xml"
      href="{{ asset('img/logo-icon.svg') }}">
    <title>MyAll</title>

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link
      href="https://fonts.googleapis.com/css2?family=Inter:ital,wght@0,100..900;1,100..900&display=swap"
      rel="stylesheet">

    <link rel="stylesheet"
      href="{{ asset('front_assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('front_assets/css/plugins.css') }}">
    <link rel="stylesheet" href="{{ asset('front_assets/css/style.css') }}">
    <link rel="stylesheet"
      href="{{ asset('front_assets/css/fonts/font.css') }}">
    {{-- <link rel="stylesheet" --}}
    {{--   href="{{ asset('front_assets/css/colors/navy.css') }}"> --}}
    {{-- <link rel="stylesheet" href="{{ asset('front_assets/css/custom.css') }}"> --}}
    <link rel="stylesheet" href="{{ asset('front_assets/css/mya.css') }}">
    @livewireStyles

    {{-- NOTE: Google tag (gtag.js) --}}
    <script async src=https://www.googletagmanager.com/gtag/js?id=G-11BFMZ4TL7>
    </script>
    <script>
      window.dataLayer = window.dataLayer || [];

      function gtag() {
        dataLayer.push(arguments);
      }
      gtag('js', new Date());
      gtag('config', 'G-11BFMZ4TL7');
    </script>
  </head>

  <body>
    <div
      class="content-wrapper @if (Request::path() !== '/') __bg-fill @endif">
      <livewire:landing.partials.header />
      @yield('content')
    </div>

    {{-- @include('landing.partials.footer') --}}
    <script src="{{ asset('front_assets/js/plugins.js') }}"></script>
    <script src="{{ asset('front_assets/js/theme.js') }}"></script>
    <script src="{{ asset('front_assets/js/jquery-3.7.0.min.js') }}"></script>
    {{-- <script src="{{ asset('assets/js/familytreejs/familytree.js') }}"></script> --}}

    {{-- INFO: Google Translate implementation --}}
    <script type="text/javascript">
      function googleTranslateElementInit() {
        new google.translate.TranslateElement({
            pageLanguage: 'en'
          },
          'google_translate_element'
        );
      }
    </script>
    <script
      src="https://translate.google.com/translate_a/element.js?cb=googleTranslateElementInit">
    </script>

    @livewireScripts
    @stack('scripts')
  </body>

</html>
