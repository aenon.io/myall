<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <livewire:account.partials.header />
        </div>
        @if($show_form)
        <div class="col-md-4">
            <div class="card-box-style rounded p-5">
                <div class="d-flex">
                    <div class="">
                        <h4 class="text-navy-blue">My Network</h4>
                        <h6 class="text-red">Relative/Friend Details</h6>
                    </div>
                    <div class="ms-auto">
                        @if($cur_node_id)
                            @if($cur_node_id != $cur_id)
                                <a href="#" wire:click.prevent="deleteNode({{ $cur_node_id }})"><i class="fa fa-trash text-red" style="font-size: 30px"></i></a>
                            @endif
                        @endif
                    </div>
                </div>
                <form wire:submit.prevent="SaveFamilyTreeMember">
                    <div class="row">
                        @if($errors->any())
                            <div class="alert alert-danger">
                                <span>{{ $errors->first() }}</span>
                            </div>
                        @endif
                        <div class="col-md-12">
                            <div class="mb-3">
                                <label class="form-label">Name</label>
                                <input type="text" class="form-control" wire:model.defer="name">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="mb-3">
                                <label class="form-label">Surname</label>
                                <input type="text" class="form-control" wire:model.defer="surname">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="mb-3">
                                <label class="form-label">Relation</label>
                                <select class="form-control" wire:model="relation">
                                    <option value="">Select Option</option>
                                    @foreach($relations AS $rel)
                                    <option value="{{ $rel }}">{{ $rel }}</option>
                                    @endforeach
                                </select>
                            </div>
                            @if($show_children_list)
                                <div class="col-md-12">
                                    <div class="mb-3">
                                        <label class="form-label">{{ $ch_label }}</label>
                                        <select class="form-control" wire:model.defer="link_id">
                                            <option value="">Select Option</option>
                                            @foreach($children_list AS $ch)
                                                <option value="{{ $ch->id }}">{{ $ch->name.' '.$ch->surname }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            @endif
                            @if($show_parent_list)
                                <div class="col-md-12">
                                    <div class="mb-3">
                                        <label class="form-label">Father</label>
                                        <select class="form-control" wire:model.defer="f_id">
                                            <option value="">Select Option</option>
                                            @foreach($children_list AS $ch)
                                                <option value="{{ $ch->id }}">{{ $ch->name.' '.$ch->surname }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="mb-3">
                                        <label class="form-label">Mother</label>
                                        <select class="form-control" wire:model.defer="m_id">
                                            <option value="">Select Option</option>
                                            @foreach($children_list AS $ch)
                                                <option value="{{ $ch->id }}">{{ $ch->name.' '.$ch->surname }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            @endif
                            @if($show_child_spouse)
                            <div class="col-md-12">
                                <div class="mb-3">
                                    <label class="form-label">Married / Partner to:</label>
                                    <select class="form-control" wire:model.defer="p_id">
                                        <option value="">Select Option</option>
                                        @foreach($children_spouses AS $sp)
                                        <option value="{{ $sp->id }}">{{ $sp->name.' '.$sp->surname }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            @endif
                            @if($show_grand_children_parents)
                            <div class="col-md-12">
                                <div class="mb-3">
                                    <label class="form-label">Parent</label>
                                    <select class="form-control" wire:model.defer="cp_id">
                                        <option value="">Select Option</option>
                                        @foreach($grand_children_parents AS $gcp)
                                        <option value="{{ $gcp->id }}">{{ $gcp->name.' '.$gcp->surname }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            @endif
                        </div>
                        <div class="col-md-12">
                            <div class="mb-3">
                                <label class="form-label">Birthday</label>
                                <input type="date" class="form-control" wire:model.defer="dob" max="2999-12-31">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="mb-3">
                                <label class="form-label">ID Number  / Email Address</label>
                                <input type="text" class="form-control" wire:model.defer="id_number">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>
                                        <input type="checkbox" name="living" wire:model.lazy="living"> Living
                                    </label>
                                </div>
                                <div class="col-md-6">
                                    <label>
                                        <input type="checkbox" name="show_on_tree" value="1" wire:model.defer="show_on_tree"> Show On Tree
                                    </label>
                                </div>
                            </div>
                        </div>
                        @if(!$living)
                        <div class="col-md-12 mt-3">
                            <div class="mb-3">
                                <label class="form-label">Date Of Passing</label>
                                <input type="date" class="form-control" wire:model.defer="dod" max="2999-12-31">
                            </div>
                        </div>
                        @endif
                        <div class="col-md-12 d-flex justify-content-between mt-3">
                            <span>Assign Color</span>
                            <label>
                                <input type="radio" name="color" class="yellow-bg" value="Orange" wire:model.defer="color">
                            </label>
                            <label>
                                <input type="radio" name="color" class="bg-blue" value="Blue" wire:model.defer="color">
                            </label>
                            <label>
                                <input type="radio" name="color" class="bg-pink" value="Pink" wire:model.defer="color">
                            </label>
                            <label>
                                <input type="radio" name="color" class="bg-green" value="Green" wire:model.defer="color">
                            </label>
                            <label>
                                <input type="radio" name="color" class="bg-yellow2" value="Yellow" wire:model.defer="color">
                            </label>
                        </div>
                        <div class="col-md-12 mt-3">
                            <label for="upload" style="cursor: pointer;">
                                <b><i class="bx bx-plus"></i> Photo</b>
                                <input type="file" id="upload" style="display:none" wire:model.defer="photo">
                            </label>
                        </div>
                        <div class="col-md-12 d-flex justify-content-between mt-3">
                            <input type="submit" class="btn btn-red" value="Save Changes">
                            <a href="#" class="btn btn-outline-two" wire:click.prevent="hideForm">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        @endif
        <div class="@if($show_form) col-md-8 @else col-md-12 @endif h-100">
            <div class="card-box-style rounded p-5 h-100">
                <div class="mb-3">
                    <p>Add your whole family, starting at the source. Fill in your parents to get started and remember to always add the parents first before adding their linked family (eg. Mother, Brother, Niece, etc.).</p>
                </div>
                <div class="d-flex align-items-center">
                    <span><b>CONTROLS:</b> DRAG: Click and Hold / ZOOM IN: Ctr. & Scroll Up / ZOOM OUT: Ctr. & Scroll Down / ADD PERSON: Click on the '+' / EDIT MEMBER: Click on a member to edit their details</span>
                    <span class="ms-4">
                        <a href="" style="color: #112446; font-size: 40px" wire:click.prevent="processShowForm">
                            <i class="fa fa-plus-circle"></i>
                        </a>
                    </span>
                </div>
                <div style="width:100%; height:100%" id="tree" wire:ignore></div>
            </div>
        </div>
        
         <div class="modal" tabindex="-1" id="init-pop-up">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="d-flex">
                            <h5>My Network</h5>
                            <span class="ms-auto">
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </span>
                        </div>
                        <div class="">
                            <p>
                                Easily build and store your <i>Network</i> for emergency purposes: Ensure quick identification and understanding of your family connections in times of emergency. 
                            </p>
                        </div>
                        <div class="mt-5 text-end">
                            <a href="#" class="btn btn-outline-two" wire:click.prevent="initModalSeen">Continue</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row d-block d-lg-none">
        <div class="col-md-12">
            <div style="margin-bottom: 100px"></div>
        </div>
    </div>

    <div class="modal" id="editNode" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" wire:ignore>
        <div class="modal-dialog modal-lg modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="d-flex mb-3">
                        <h5>Edit</h5>
                        <span class="ms-auto">
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </span>
                    </div>
                    <form wire:submit.prevent="saveEditNode">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label class="form-label">Name</label>
                                    <input type="text" class="form-control" name="name" wire:model.defer="cur_name">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label class="form-label">Surname</label>
                                    <input type="text" class="form-control" name="surname" wire:model.defer="cur_surname">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="mb-3">
                                    <label class="form-label">DOB</label>
                                    <input type="date" class="form-control" name="dob" max="2999-12-31" wire:model.defer="cur_dob">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label class="form-label">ID Number</label>
                                    <input type="text" class="form-control" name="id_number" wire:model.defer="cur_id_number">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label class="form-label">Email</label>
                                    <input type="email" class="form-control" name="email" wire:model.defer="cur_email">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="mb-3">
                                    <label class="form-label">Photo</label>
                                    <input type="file" class="form-control" wire:model="cur_photo">
                                </div>
                            </div>
                            <div class="col-md-12 d-flex justify-content-between mb-3">
                                <span>Assign Color</span>
                                <label>
                                    <input type="radio" name="color" class="yellow-bg" value="Orange" wire:model.defer="cur_color">
                                </label>
                                <label>
                                    <input type="radio" name="color" class="bg-blue" value="Blue" wire:model.defer="cur_color">
                                </label>
                                <label>
                                    <input type="radio" name="color" class="bg-pink" value="Pink" wire:model.defer="cur_color">
                                </label>
                                <label>
                                    <input type="radio" name="color" class="bg-green" value="Green" wire:model.defer="cur_color">
                                </label>
                                <label>
                                    <input type="radio" name="color" class="bg-yellow2" value="Yellow" wire:model.defer="cur_color">
                                </label>
                            </div>
                            <div class="col-md-12">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="" id="show_on_tree" wire:model.defer="show_on_tree">
                                    <label class="form-check-label" for="show_on_tree">
                                        Show On Tree
                                    </label>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-two" data-bs-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-red" wire:click.prevent="saveEditNode">Save</button>
                </div>
            </div>
        </div>
    </div>
    

     @push('scripts')
    <script>
        $(document).ready(function(){
            //loadTree();
            
            window.addEventListener('hide-modals', event => {
                $('.modal').modal('hide');
            });
            window.addEventListener('show-edit-node-modal', event => {
                $('#editNode').modal('show');
            });
            
            window.addEventListener('data-updated', event => {
                location.reload();
            });
        });
        
        document.addEventListener("DOMContentLoaded", () => {
            Livewire.hook('element.updated', (el, component) => {
                //loadTree();
            });
        });
        
        function loadTree(){
            FamilyTree.templates.hugo = Object.assign({}, FamilyTree.templates.base);
            FamilyTree.templates.hugo.defs = 
            `<clipPath id="hugo_img_0">
                <circle id="hugo_img_0_stroke" stroke-width="2" stroke="#fff" x="90" y="-5" rx="25" ry="50%" width="70" height="70"></circle>
            </clipPath>
            <linearGradient id="hugo_grad_female" x1="0%" y1="0%" x2="100%" y2="0%">
                <stop offset="0%" style="stop-color:#FF8024;stop-opacity:1" />
                <stop offset="100%" style="stop-color:#FF46A3;stop-opacity:1" />
            </linearGradient>
            <linearGradient id="hugo_grad_male" x1="0%" y1="0%" x2="100%" y2="0%">
                <stop offset="0%" style="stop-color:#00D3A5;stop-opacity:1" />
                <stop offset="100%" style="stop-color:#00A7D4;stop-opacity:1" />
            </linearGradient>
            <linearGradient id="hugo_grad" x1="0%" y1="0%" x2="100%" y2="0%">
                <stop offset="0%" style="stop-color:#ffd292;stop-opacity:1" />
                <stop offset="100%" style="stop-color:#ffd292;stop-opacity:1" />
            </linearGradient>
            <g id="hugo_up">
                <circle cx="12" cy="12" r="15" fill="transparent"></circle>
                ${FamilyTree.icon.ft(24,24,'#fff', 0, 0)}
            </g>
            <g id="hugo_node_menu" style="cursor:pointer;">
                <rect x="0" y="0" fill="transparent" width="22" height="22"></rect>
                <circle cx="11" cy="4" r="2" fill="#ffffff"></circle><circle cx="11" cy="11" r="2" fill="#ffffff"></circle>
                <circle cx="11" cy="18" r="2" fill="#ffffff"></circle>
            </g>
            <style>
                .{randId} .bft-edit-form-header{
                    background: linear-gradient(90deg, #D0D0D0 0%, #909090 100%);
                }
                .{randId}.male .bft-edit-form-header{
                    background: linear-gradient(90deg, #00D3A5 0%, #00A7D4 100%);
                }
                .{randId}.female .bft-edit-form-header{
                    background: linear-gradient(90deg, #FF8024 0%, #FF46A3 100%);
                }  
                .{randId} .bft-img-button{
                    background-color: #909090;
                }      
                .{randId} .bft-img-button:hover{
                    background-color: #D0D0D0;
                }
                .{randId}.male .bft-img-button{
                    background-color: #00A7D4;
                }      
                .{randId}.male .bft-img-button:hover{
                    background-color: #00D3A5;
                }
                .{randId}.female .bft-img-button{
                    background-color: #FF46A3;
                }      
                .{randId}.female .bft-img-button:hover{
                    background-color: #FF8024;
                }
            </style>`;
            FamilyTree.templates.hugo.img_0 = '<clipPath id="ulaImg">'
            + '<circle cx="40" cy="60" r="30" fill="white"></circle>'
            + '</clipPath>'
            + '<image preserveAspectRatio="xMidYMid slice" clip-path="url(#ulaImg)" xlink:href="{val}" x="10" y="30" width="60" height="60">'
            + '</image>';
            FamilyTree.templates.hugo.field_0 = '<text ' + FamilyTree.attr.width + ' ="230" style="font-size: 18px;font-weight:bold;"  fill="#1c355e" x="80" y="43" text-anchor="left">{val}</text>';
            FamilyTree.templates.hugo.field_1 = '<text ' + FamilyTree.attr.width + ' ="230" style="font-size: 18px;font-weight:bold;"  fill="#1c355e" x="80" y="63" text-anchor="left">{val}</text>';
            FamilyTree.templates.hugo.field_2 = '<text ' + FamilyTree.attr.width + ' ="230" style="font-size: 16px;font-weight:bold;" fill="#1c355e" x="80" y="83" text-anchor="left">{val}</text>';
            FamilyTree.templates.hugo.nodeMenuButton = `<use x="225" y="10" ${FamilyTree.attr.control_node_menu_id}="{id}" xlink:href="#hugo_node_menu" />`; 
            
            FamilyTree.templates.hugo.node = '<rect x="0" y="0" height="{h}" width="{w}" stroke-width="3" fill="#b1cfff" stroke="#b1cfff" rx="40" ry="40"></rect>';
            FamilyTree.templates.hugo.defs = '<filter id="shadow" x="-20%" y="-20%" width="150%" height="150%"><feOffset result="offOut" in="SourceAlpha" dx="2" dy="2" /><feGaussianBlur result="blurOut" in="offOut" stdDeviation="2" /><feBlend in="SourceGraphic" in2="blurOut" mode="normal" /></filter>';

            FamilyTree.templates.hugo_blue = Object.assign({}, FamilyTree.templates.hugo);
            FamilyTree.templates.hugo_blue.node = '<rect x="0" y="0" height="{h}" width="{w}" stroke-width="3" fill="#add8e6" stroke="#add8e6" rx="40" ry="40" filter="url(#shadow)"></rect>';
            FamilyTree.templates.hugo_blue.defs = '<filter id="shadow" x="-20%" y="-20%" width="150%" height="150%"><feOffset result="offOut" in="SourceAlpha" dx="2" dy="2" /><feGaussianBlur result="blurOut" in="offOut" stdDeviation="2" /><feBlend in="SourceGraphic" in2="blurOut" mode="normal" /></filter>';

            FamilyTree.templates.hugo_pink = Object.assign({}, FamilyTree.templates.hugo);
            FamilyTree.templates.hugo_pink.node = '<rect x="0" y="0" height="{h}" width="{w}" stroke-width="3" fill="#ffb1c9" stroke="#ffb1c9" rx="40" ry="40" filter="url(#shadow)"></rect>';
            FamilyTree.templates.hugo_pink.defs = '<filter id="shadow" x="-20%" y="-20%" width="150%" height="150%"><feOffset result="offOut" in="SourceAlpha" dx="2" dy="2" /><feGaussianBlur result="blurOut" in="offOut" stdDeviation="2" /><feBlend in="SourceGraphic" in2="blurOut" mode="normal" /></filter>';

            FamilyTree.templates.hugo_orange = Object.assign({}, FamilyTree.templates.hugo);
            FamilyTree.templates.hugo_orange.node = '<rect x="0" y="0" height="{h}" width="{w}" stroke-width="3" fill="#ffd38e" stroke="#ffd38e" rx="40" ry="40" filter="url(#shadow)"></rect>';
            FamilyTree.templates.hugo_orange.defs = '<filter id="shadow" x="-20%" y="-20%" width="150%" height="150%"><feOffset result="offOut" in="SourceAlpha" dx="2" dy="2" /><feGaussianBlur result="blurOut" in="offOut" stdDeviation="2" /><feBlend in="SourceGraphic" in2="blurOut" mode="normal" /></filter>';

            FamilyTree.templates.hugo_green = Object.assign({}, FamilyTree.templates.hugo);
            FamilyTree.templates.hugo_green.node = '<rect x="0" y="0" height="{h}" width="{w}" stroke-width="3" fill="#c7e599" stroke="#c7e599" rx="40" ry="40" filter="url(#shadow)"></rect>';
            FamilyTree.templates.hugo_green.defs = '<filter id="shadow" x="-20%" y="-20%" width="150%" height="150%"><feOffset result="offOut" in="SourceAlpha" dx="2" dy="2" /><feGaussianBlur result="blurOut" in="offOut" stdDeviation="2" /><feBlend in="SourceGraphic" in2="blurOut" mode="normal" /></filter>';

            FamilyTree.templates.hugo_yellow = Object.assign({}, FamilyTree.templates.hugo);
            FamilyTree.templates.hugo_yellow.node = '<rect x="0" y="0" height="{h}" width="{w}" stroke-width="3" fill="#fce571" stroke="#fce571" rx="40" ry="40" filter="url(#shadow)"></rect>';
            FamilyTree.templates.hugo_yellow.defs = '<filter id="shadow" x="-20%" y="-20%" width="150%" height="150%"><feOffset result="offOut" in="SourceAlpha" dx="2" dy="2" /><feGaussianBlur result="blurOut" in="offOut" stdDeviation="2" /><feBlend in="SourceGraphic" in2="blurOut" mode="normal" /></filter>';

            var tree_json = $.parseJSON('{!! $tree !!}');
            console.log(tree_json);

            var family = new FamilyTree(document.getElementById("tree"), {
                template: "hugo",
                scaleInitial: FamilyTree.match.boundary,
                orientation: FamilyTree.orientation.left,
                showXScroll: false,
                showYScroll: true,
                enableSearch: false,
                interactive: true,
                partnerNodeSeparation: 100,
                mouseScrool: FamilyTree.action.ctrlZoom,
                nodeBinding: {
                    field_0: "name",
                    field_1: "surname",
                    field_2: "relation",
                    img_0: "img",
                },
                editForm: {
                    buttons: {
                        pdf: null,
                        share: null,
                        delete: {
                            icon: FamilyTree.icon.remove(24,24,'#fff'),
                            text: "Remove",
                            hideIfDetailsMode: false,
                        }
                    },
                    /*
                    generateElementsFromFields: false,
                    elements: [
                        { type: 'textbox', label: 'First Name', binding: 'Name'},
                        { type: 'textbox', label: 'Surname Name', binding: 'Name'},
                        { type: 'textbox', label: 'Photo Url', binding: 'ImgUrl', btn: 'Upload'}        
                    ]
                    */
                },    
                tags: {
                    Orange: {
                        template: "hugo_orange"
                    },
                    Green: {
                        template: "hugo_green"
                    },
                    Yellow: {
                        template: "hugo_yellow"
                    },
                    Blue: {
                        template: "hugo_blue"
                    },
                    Pink: {
                        template: "hugo_pink"
                    }
                },
                nodes: tree_json
                // Example of a node with color
                //nodes: [{ id: 4, pids: [1],  name: "Gogo Alfred", relation: "Grand Mother", gender: "female", img: "{{ asset('../img/family-tree/My Network ICON.png') }}", tags: ["hugo_pink"] }]
                
            });
            
            
            family.on('click', function(sender, args){
                var id = args.node.id;
                Livewire.emit('showForm', id);
                return false;
            });

            family.editUI.on('button-click', function (sender, args) {
                var data = family.get(args.nodeId);
                if (args.name == 'delete') {
                    Livewire.emit('delete-node', data.id);
                }
                if(args.name == "edit"){
                    $('.bft-edit-form').hide();
                    Livewire.emit('show-edit-node', data.id);
                    return false;
                }
            });
        }
        
        $(document).ready(function(){
            var show_init_pop = '{{ $show_init_pop_up }}';
            if(show_init_pop){
                $('#init-pop-up').modal('show');
            }
            window.addEventListener('close-modals', event => {
                $('.modal').each(function(){
                    $(this).modal('hide');
                })
            });
            
        });
    </script> 
    @endpush
</div>
