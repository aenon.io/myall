<div class="myallMainAppInner">
  <div class="">
    <div class="col-md-12">
      <livewire:account.partials.header />
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="row mb-5">
        <div class="col-md-12 mb-0 text-center">
          <h4>Shared Info With</h4>
        </div>
        <div class="col-md-12 text-center">
          <h6>Here you can see and manage the connections you've shared information with.</h6>
        </div>
        <div class="col-md-6 offset-md-3">
          <ul class="ms-4">
            <li>You can use colours to group connections.</li>
            <li>You can add, remove, or edit connections at the 'Share Info' page.</li>
            <li>To rearrange the order of the connections, just click and drag them where you want (within the same
              group).</li>
          </ul>
        </div>
        <div class="col-md-12 text-center">
          <a href="{{ url('share-profile') }}" class="btn btn-outline-two">Click here to go to the share info page</a>
        </div>
      </div>
    </div>
    <div class="col-md-12 mb-5">
      <livewire:account.my-network.network :id="$contact_id" />
    </div>
  </div>
</div>
