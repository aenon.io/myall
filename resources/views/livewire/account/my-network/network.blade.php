<div class="row">
  <div class="__header">
    <div class="__heading">
      <h3>Your Contacts</h3>
      {{-- <p>Manage your Contacts below. See who you have Shared with, and who you --}}
      {{--   have Received a share from by using the tabs above.</p> --}}
      <div class="__heading-text">
        <p>Here you can share important information with people you trust:</p>
        <ol>
          <li>Click 'New Share' to choose what to share.</li>
          <li>Send a Request: The recipient can join MyAll for free to view the
            information or create a full profile.</li>
          <li>Accept and Register: Once they accept and register, they become
            your
            contact.</li>
          <li>Manage Sharing: Easily manage what you share or un-share.</li>
        </ol>
        <p>
          This process ensures secure and efficient information sharing, keeping
          everyone in the loop with important matters, whether for emergencies
          or everyday situations.
        </p>
      </div>
    </div>
    <div class="__new-share">
      <a href="#" onclick="Livewire.emit('loadTabContent','tabShared')"
        class="btn btn-gradient btn-lg gradient-1">
        <svg width="20" height="20" viewBox="0 0 20 20" fill="none"
          xmlns="http://www.w3.org/2000/svg">
          <path d="M16.6668
            7.4987V4.9987H15.0002V7.4987H12.5002V9.16536H15.0002V11.6654H16.6668V9.16536H19.1668V7.4987H16.6668ZM7.50016
            9.9987C9.34183 9.9987 10.8335 8.50703 10.8335 6.66536C10.8335 4.8237
            9.34183 3.33203 7.50016 3.33203C5.6585 3.33203 4.16683 4.8237
            4.16683 6.66536C4.16683 8.50703 5.6585 9.9987 7.50016
            9.9987ZM7.50016 4.9987C8.41683 4.9987 9.16683 5.7487 9.16683
            6.66536C9.16683 7.58203 8.41683 8.33203 7.50016 8.33203C6.5835
            8.33203 5.8335 7.58203 5.8335 6.66536C5.8335 5.7487 6.5835 4.9987
            7.50016 4.9987ZM12.8252 12.132C11.4252 11.4154 9.60849 10.832
            7.50016 10.832C5.39183 10.832 3.57516 11.4154 2.17516 12.132C1.34183
            12.557 0.833496 13.4154 0.833496
            14.3487V16.6654H14.1668V14.3487C14.1668 13.4154 13.6585 12.557
            12.8252 12.132ZM12.5002 14.9987H2.50016V14.3487C2.50016 14.032
            2.66683 13.7487 2.9335 13.6154C3.92516 13.107 5.52516 12.4987
            7.50016 12.4987C9.47516 12.4987 11.0752 13.107 12.0668
            13.6154C12.3335 13.7487 12.5002 14.032 12.5002 14.3487V14.9987Z"
            fill="#CF3D60" />
        </svg>
        New Share
      </a>
    </div>
  </div>
  <div class="__island">
    <div class="">
      <div class="__section-heading">
        <h4>Family</h4>
      </div>
      <div class="__contacts-container rounded" drag-root
        wire:sortable="reOrderItems">
        @foreach ($family as $cnt)
          {{-- @php --}}
          {{--   dump($cnt->contact); --}}
          {{-- @endphp --}}
          <div class="d-flex network-content-block {{ $cnt->color }}"
            drag-item="{{ $cnt->id }}"
            wire:sortable.item="{{ $cnt->id }}"
            wire:key="{{ $cnt->id }}">
            <div class="network-cnt-pic" wire:sortable.handle>
              @if ($cnt->contact)
                @if ($cnt->contact->profile_pic)
                  <img
                    src="{{ asset('storage/' . $cnt->contact->profile_pic) }}"
                    class="">
                @else
                  <div class="__avatar">
                    {{ getInitials($cnt->name) }}
                  </div>
                  {{-- <svg width="74" height="74" viewBox="0 0 74 74" --}}
                  {{--   class="__placeholder-user" fill="none" --}}
                  {{--   xmlns="http://www.w3.org/2000/svg"> --}}
                  {{--   <g clip-path="url(#clip0_545_4735)"> --}}
                  {{--     <rect width="74" height="74" /> --}}
                  {{--     <path opacity="0.4" d="M26.3457 26.1424C26.3457 29.0975 27.5197 31.9316 --}}
                  {{--       29.6094 34.0212C31.6991 36.1108 34.5333 37.2847 37.4885 --}}
                  {{--       37.2847C40.4438 37.2847 43.278 36.1108 45.3677 --}}
                  {{--       34.0212C47.4574 31.9316 48.6314 29.0975 48.6314 --}}
                  {{--       26.1424C48.6314 23.1872 47.4574 20.3531 45.3677 --}}
                  {{--       18.2635C43.278 16.1739 40.4438 15 37.4885 15C34.5333 15 --}}
                  {{--       31.6991 16.1739 29.6094 18.2635C27.5197 20.3531 26.3457 --}}
                  {{--       23.1872 26.3457 26.1424Z" /> --}}
                  {{--     <path d="M18 56.9853C18 48.4109 24.9469 41.4644 33.5217 --}}
                  {{--       41.4644H41.4784C50.0531 41.4644 57 48.4109 57 56.9853C57 --}}
                  {{--       58.4129 55.8422 59.5707 54.4145 59.5707H20.5855C19.1578 --}}
                  {{--       59.5707 18 58.4129 18 56.9853Z" /> --}}
                  {{--   </g> --}}
                  {{-- </svg> --}}
                @endif
              @else
                <div class="__avatar">
                  {{ getInitials($cnt->name) }}
                </div>
                {{-- <svg width="74" height="74" viewBox="0 0 74 74" --}}
                {{--   class="__placeholder-user" fill="none" --}}
                {{--   xmlns="http://www.w3.org/2000/svg"> --}}
                {{--   <g clip-path="url(#clip0_545_4735)"> --}}
                {{--     <rect width="74" height="74" /> --}}
                {{--     <path opacity="0.4" d="M26.3457 26.1424C26.3457 29.0975 27.5197 31.9316 --}}
                {{--       29.6094 34.0212C31.6991 36.1108 34.5333 37.2847 37.4885 --}}
                {{--       37.2847C40.4438 37.2847 43.278 36.1108 45.3677 --}}
                {{--       34.0212C47.4574 31.9316 48.6314 29.0975 48.6314 --}}
                {{--       26.1424C48.6314 23.1872 47.4574 20.3531 45.3677 --}}
                {{--       18.2635C43.278 16.1739 40.4438 15 37.4885 15C34.5333 15 --}}
                {{--       31.6991 16.1739 29.6094 18.2635C27.5197 20.3531 26.3457 --}}
                {{--       23.1872 26.3457 26.1424Z" /> --}}
                {{--     <path d="M18 56.9853C18 48.4109 24.9469 41.4644 33.5217 --}}
                {{--       41.4644H41.4784C50.0531 41.4644 57 48.4109 57 56.9853C57 --}}
                {{--       58.4129 55.8422 59.5707 54.4145 59.5707H20.5855C19.1578 --}}
                {{--       59.5707 18 58.4129 18 56.9853Z" /> --}}
                {{--   </g> --}}
                {{-- </svg> --}}
              @endif
            </div>
            <div class="network-cnt-content">
              <div class="__details">
                <div class="" wire:sortable.handle>
                  <div class="__cnt-name"> {{ $cnt->name }} </div>
                  <div class="__cnt-relation">
                    @if ($cnt->family == 'Other')
                      {{ $cnt->specify_relation }}
                    @else
                      {{ $cnt->family }}
                    @endif
                  </div>
                </div>
                <div class="__badge">
                  <span>
                    <svg width="13" height="13" viewBox="0 0 13 13"
                      fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path d="M5.31787 2.75V3.75H8.61287L2.81787 9.545L3.52287
                        10.25L9.31787 4.455V7.75H10.3179V2.75H5.31787Z" />
                    </svg>
                    Shared
                  </span>
                </div>
              </div>
              @if (Auth::user()->id == $user_id)
                <div class="__actions">
                  <div class="__edit-visibility">
                    {{-- <a href="{{ url('share-profile/edit/' . $cnt->id) }}" --}}
                    {{-- <a href="" wire:click="vewShare({{ $cnt->id }})"><i --}}
                    {{--     class="bx bxs-edit" style="font-size: 20px;"></i></a> --}}
                    <button wire:click="viewShare({{ $cnt->id }})">
                      <svg width="17" height="17" viewBox="0 0 17 17"
                        fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M9.94023 6.51333L10.5536 7.12667L4.51356
                      13.1667H3.90023V12.5533L9.94023 6.51333ZM12.3402
                      2.5C12.1736 2.5 12.0002 2.56667 11.8736 2.69333L10.6536
                      3.91333L13.1536 6.41333L14.3736 5.19333C14.6336 4.93333
                      14.6336 4.51333 14.3736 4.25333L12.8136 2.69333C12.6802
                      2.56 12.5136 2.5 12.3402 2.5ZM9.94023 4.62667L2.56689
                      12V14.5H5.06689L12.4402 7.12667L9.94023 4.62667Z"
                          fill="#CF3D60" />
                      </svg>
                      Edit visibility
                    </button>
                  </div>
                  <div class="__color-tools">
                    <div class="__color-selector"></div>
                    <div class="__contact-color-container">
                      <button
                        wire:click.prevent="setColor({{ $cnt->id }}, 'contact-yellow')"
                        class="contact-yellow">
                      </button>
                      <button
                        wire:click.prevent="setColor({{ $cnt->id }}, 'contact-blue')"
                        class="contact-blue">
                      </button>
                      <button
                        wire:click.prevent="setColor({{ $cnt->id }}, 'contact-pink')"
                        class="contact-pink">
                      </button>
                      <button
                        wire:click.prevent="setColor({{ $cnt->id }}, 'contact-green')"
                        class="contact-green">
                      </button>
                      <button
                        wire:click.prevent="setColor({{ $cnt->id }}, 'contact-purple')"
                        class="contact-purple">
                      </button>
                      <button
                        wire:click.prevent="removeColor({{ $cnt->id }})"
                        class="__remove-color">
                        {{-- <svg width="16" height="16" viewBox="0 0 16 16" --}}
                        {{--   fill="none" xmlns="http://www.w3.org/2000/svg"> --}}
                        {{--   <path d="M12.6673 4.27325L11.7273 3.33325L8.00065 --}}
                        {{--     7.05992L4.27398 3.33325L3.33398 4.27325L7.06065 --}}
                        {{--     7.99992L3.33398 11.7266L4.27398 12.6666L8.00065 --}}
                        {{--     8.93992L11.7273 12.6666L12.6673 11.7266L8.94065 --}}
                        {{--     7.99992L12.6673 4.27325Z" /> --}}
                        {{-- </svg> --}}
                      </button>
                    </div>
                  </div>
                </div>
              @endif
            </div>
            @if (Auth::user()->id == $user_id)
            @endif
          </div>
        @endforeach
      </div>
    </div>
    <div class="">
      <div class="__section-heading">
        <h4>Friends</h4>
      </div>
      <div class="__contacts-container rounded" drag-root
        wire:sortable="reOrderItems">
        @foreach ($friends as $cnt)
          <div class="d-flex network-content-block {{ $cnt->color }}"
            drag-item="{{ $cnt->id }}"
            wire:sortable.item="{{ $cnt->id }}"
            wire:key="{{ $cnt->id }}">
            <div class="network-cnt-pic" wire:sortable.handle>
              @if ($cnt->contact)
                @if ($cnt->contact->profile_pic)
                  <img
                    src="{{ asset('storage/' . $cnt->contact->profile_pic) }}"
                    class="">
                @else
                  <div class="__avatar">
                    {{ getInitials($cnt->name) }}
                  </div>
                  {{-- <svg width="74" height="74" viewBox="0 0 74 74" --}}
                  {{--   class="__placeholder-user" fill="none" --}}
                  {{--   xmlns="http://www.w3.org/2000/svg"> --}}
                  {{--   <g clip-path="url(#clip0_545_4735)"> --}}
                  {{--     <rect width="74" height="74" /> --}}
                  {{--     <path opacity="0.4" d="M26.3457 26.1424C26.3457 29.0975 27.5197 31.9316 --}}
                  {{--       29.6094 34.0212C31.6991 36.1108 34.5333 37.2847 37.4885 --}}
                  {{--       37.2847C40.4438 37.2847 43.278 36.1108 45.3677 --}}
                  {{--       34.0212C47.4574 31.9316 48.6314 29.0975 48.6314 --}}
                  {{--       26.1424C48.6314 23.1872 47.4574 20.3531 45.3677 --}}
                  {{--       18.2635C43.278 16.1739 40.4438 15 37.4885 15C34.5333 15 --}}
                  {{--       31.6991 16.1739 29.6094 18.2635C27.5197 20.3531 26.3457 --}}
                  {{--       23.1872 26.3457 26.1424Z" /> --}}
                  {{--     <path d="M18 56.9853C18 48.4109 24.9469 41.4644 33.5217 --}}
                  {{--       41.4644H41.4784C50.0531 41.4644 57 48.4109 57 56.9853C57 --}}
                  {{--       58.4129 55.8422 59.5707 54.4145 59.5707H20.5855C19.1578 --}}
                  {{--       59.5707 18 58.4129 18 56.9853Z" /> --}}
                  {{--   </g> --}}
                  {{-- </svg> --}}
                @endif
              @else
                <div class="__avatar">
                  {{ getInitials($cnt->name) }}
                </div>
                {{-- <svg width="74" height="74" viewBox="0 0 74 74" --}}
                {{--   class="__placeholder-user" fill="none" --}}
                {{--   xmlns="http://www.w3.org/2000/svg"> --}}
                {{--   <g clip-path="url(#clip0_545_4735)"> --}}
                {{--     <rect width="74" height="74" /> --}}
                {{--     <path opacity="0.4" d="M26.3457 26.1424C26.3457 29.0975 27.5197 31.9316 --}}
                {{--       29.6094 34.0212C31.6991 36.1108 34.5333 37.2847 37.4885 --}}
                {{--       37.2847C40.4438 37.2847 43.278 36.1108 45.3677 --}}
                {{--       34.0212C47.4574 31.9316 48.6314 29.0975 48.6314 --}}
                {{--       26.1424C48.6314 23.1872 47.4574 20.3531 45.3677 --}}
                {{--       18.2635C43.278 16.1739 40.4438 15 37.4885 15C34.5333 15 --}}
                {{--       31.6991 16.1739 29.6094 18.2635C27.5197 20.3531 26.3457 --}}
                {{--       23.1872 26.3457 26.1424Z" /> --}}
                {{--     <path d="M18 56.9853C18 48.4109 24.9469 41.4644 33.5217 --}}
                {{--       41.4644H41.4784C50.0531 41.4644 57 48.4109 57 56.9853C57 --}}
                {{--       58.4129 55.8422 59.5707 54.4145 59.5707H20.5855C19.1578 --}}
                {{--       59.5707 18 58.4129 18 56.9853Z" /> --}}
                {{--   </g> --}}
                {{-- </svg> --}}
              @endif
            </div>
            <div class="network-cnt-content">
              <div class="__details">
                <div class="" wire:sortable.handle>
                  <div class="__cnt-name">{{ $cnt->name }}</div>
                  <div class="__cnt-relation">
                    @if ($cnt->friends == 'Other')
                      {{ $cnt->specify_relation }}
                    @else
                      {{ $cnt->friends }}
                    @endif
                  </div>
                </div>
                <div class="__badge">
                  <span>
                    <svg width="13" height="13" viewBox="0 0 13 13"
                      fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path d="M5.31787 2.75V3.75H8.61287L2.81787 9.545L3.52287
                        10.25L9.31787 4.455V7.75H10.3179V2.75H5.31787Z" />
                    </svg>
                    Shared
                  </span>
                </div>
              </div>
              @if (Auth::user()->id == $user_id)
                <div class="__actions">
                  <div class="__edit-visibility">
                    {{-- <a href="{{ url('share-profile/edit/' . $cnt->id) }}" --}}
                    {{-- <a href="" wire:click="vewShare({{ $cnt->id }})"><i --}}
                    {{--     class="bx bxs-edit" style="font-size: 20px;"></i></a> --}}
                    <button wire:click="viewShare({{ $cnt->id }})">
                      <svg width="17" height="17" viewBox="0 0 17 17"
                        fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M9.94023 6.51333L10.5536 7.12667L4.51356
                      13.1667H3.90023V12.5533L9.94023 6.51333ZM12.3402
                      2.5C12.1736 2.5 12.0002 2.56667 11.8736 2.69333L10.6536
                      3.91333L13.1536 6.41333L14.3736 5.19333C14.6336 4.93333
                      14.6336 4.51333 14.3736 4.25333L12.8136 2.69333C12.6802
                      2.56 12.5136 2.5 12.3402 2.5ZM9.94023 4.62667L2.56689
                      12V14.5H5.06689L12.4402 7.12667L9.94023 4.62667Z"
                          fill="#CF3D60" />
                      </svg>
                      Edit visibility
                    </button>
                  </div>
                  <div class="__color-tools">
                    <div class="__color-selector"></div>
                    <div class="__contact-color-container">
                      <button
                        wire:click.prevent="setColor({{ $cnt->id }}, 'contact-yellow')"
                        class="contact-yellow">
                      </button>
                      <button
                        wire:click.prevent="setColor({{ $cnt->id }}, 'contact-blue')"
                        class="contact-blue">
                      </button>
                      <button
                        wire:click.prevent="setColor({{ $cnt->id }}, 'contact-pink')"
                        class="contact-pink">
                      </button>
                      <button
                        wire:click.prevent="setColor({{ $cnt->id }}, 'contact-green')"
                        class="contact-green">
                      </button>
                      <button
                        wire:click.prevent="setColor({{ $cnt->id }}, 'contact-purple')"
                        class="contact-purple">
                      </button>
                      <button
                        wire:click.prevent="removeColor({{ $cnt->id }})"
                        class="__remove-color">
                        {{-- <svg width="16" height="16" --}}
                        {{--   viewBox="0 0 16 16" fill="none" --}}
                        {{--   xmlns="http://www.w3.org/2000/svg"> --}}
                        {{--   <path d="M12.6673 4.27325L11.7273 3.33325L8.00065 --}}
                        {{--     7.05992L4.27398 3.33325L3.33398 4.27325L7.06065 --}}
                        {{--     7.99992L3.33398 11.7266L4.27398 12.6666L8.00065 --}}
                        {{--     8.93992L11.7273 12.6666L12.6673 11.7266L8.94065 --}}
                        {{--     7.99992L12.6673 4.27325Z" /> --}}
                        {{-- </svg> --}}
                      </button>
                    </div>
                  </div>
                </div>
              @endif
            </div>
            @if (Auth::user()->id == $user_id)
            @endif
          </div>
        @endforeach
      </div>
    </div>
    <div class="">
      <div class="__section-heading">
        <h4>Professional</h4>
      </div>
      <div class="__contacts-container rounded" drag-root
        wire:sortable="reOrderItems">
        @foreach ($professional as $cnt)
          <div class="d-flex network-content-block {{ $cnt->color }}"
            drag-item="{{ $cnt->id }}"
            wire:sortable.item="{{ $cnt->id }}"
            wire:key="{{ $cnt->id }}">
            <div class="network-cnt-pic" wire:sortable.handle>
              @if ($cnt->contact)
                @if ($cnt->contact->profile_pic)
                  <img
                    src="{{ asset('storage/' . $cnt->contact->profile_pic) }}"
                    class="">
                @else
                  <div class="__avatar">
                    {{ getInitials($cnt->name) }}
                  </div>
                  {{-- <svg width="74" height="74" viewBox="0 0 74 74" --}}
                  {{--   class="__placeholder-user" fill="none" --}}
                  {{--   xmlns="http://www.w3.org/2000/svg"> --}}
                  {{--   <g clip-path="url(#clip0_545_4735)"> --}}
                  {{--     <rect width="74" height="74" /> --}}
                  {{--     <path opacity="0.4" d="M26.3457 26.1424C26.3457 29.0975 27.5197 31.9316 --}}
                  {{--       29.6094 34.0212C31.6991 36.1108 34.5333 37.2847 37.4885 --}}
                  {{--       37.2847C40.4438 37.2847 43.278 36.1108 45.3677 --}}
                  {{--       34.0212C47.4574 31.9316 48.6314 29.0975 48.6314 --}}
                  {{--       26.1424C48.6314 23.1872 47.4574 20.3531 45.3677 --}}
                  {{--       18.2635C43.278 16.1739 40.4438 15 37.4885 15C34.5333 15 --}}
                  {{--       31.6991 16.1739 29.6094 18.2635C27.5197 20.3531 26.3457 --}}
                  {{--       23.1872 26.3457 26.1424Z" /> --}}
                  {{--     <path d="M18 56.9853C18 48.4109 24.9469 41.4644 33.5217 --}}
                  {{--       41.4644H41.4784C50.0531 41.4644 57 48.4109 57 56.9853C57 --}}
                  {{--       58.4129 55.8422 59.5707 54.4145 59.5707H20.5855C19.1578 --}}
                  {{--       59.5707 18 58.4129 18 56.9853Z" /> --}}
                  {{--   </g> --}}
                  {{-- </svg> --}}
                @endif
              @else
                <div class="__avatar">
                  {{ getInitials($cnt->name) }}
                </div>
                {{-- <svg width="74" height="74" viewBox="0 0 74 74" --}}
                {{--   class="__placeholder-user" fill="none" --}}
                {{--   xmlns="http://www.w3.org/2000/svg"> --}}
                {{--   <g clip-path="url(#clip0_545_4735)"> --}}
                {{--     <rect width="74" height="74" /> --}}
                {{--     <path opacity="0.4" d="M26.3457 26.1424C26.3457 29.0975 27.5197 31.9316 --}}
                {{--       29.6094 34.0212C31.6991 36.1108 34.5333 37.2847 37.4885 --}}
                {{--       37.2847C40.4438 37.2847 43.278 36.1108 45.3677 --}}
                {{--       34.0212C47.4574 31.9316 48.6314 29.0975 48.6314 --}}
                {{--       26.1424C48.6314 23.1872 47.4574 20.3531 45.3677 --}}
                {{--       18.2635C43.278 16.1739 40.4438 15 37.4885 15C34.5333 15 --}}
                {{--       31.6991 16.1739 29.6094 18.2635C27.5197 20.3531 26.3457 --}}
                {{--       23.1872 26.3457 26.1424Z" /> --}}
                {{--     <path d="M18 56.9853C18 48.4109 24.9469 41.4644 33.5217 --}}
                {{--       41.4644H41.4784C50.0531 41.4644 57 48.4109 57 56.9853C57 --}}
                {{--       58.4129 55.8422 59.5707 54.4145 59.5707H20.5855C19.1578 --}}
                {{--       59.5707 18 58.4129 18 56.9853Z" /> --}}
                {{--   </g> --}}
                {{-- </svg> --}}
              @endif
            </div>
            <div class="network-cnt-content">
              <div class="__details">
                <div class="" wire:sortable.handle>
                  <div class="__cnt-name">{{ $cnt->name }}</div>
                  <div class="__cnt-relation">
                    @if ($cnt->professional == 'Other')
                      {{ $cnt->specify_relation }}
                    @else
                      {{ $cnt->professional }}
                    @endif
                  </div>
                </div>
                <div class="__badge">
                  <span>
                    <svg width="13" height="13" viewBox="0 0 13 13"
                      fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path d="M5.31787 2.75V3.75H8.61287L2.81787 9.545L3.52287
                        10.25L9.31787 4.455V7.75H10.3179V2.75H5.31787Z" />
                    </svg>
                    Shared
                  </span>
                </div>
              </div>
              @if (Auth::user()->id == $user_id)
                <div class="__actions">
                  <div class="__edit-visibility">
                    {{-- <a href="{{ url('share-profile/edit/' . $cnt->id) }}" --}}
                    {{-- <a href="" wire:click="vewShare({{ $cnt->id }})"><i --}}
                    {{--     class="bx bxs-edit" style="font-size: 20px;"></i></a> --}}
                    <button wire:click="viewShare({{ $cnt->id }})">
                      <svg width="17" height="17" viewBox="0 0 17 17"
                        fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M9.94023 6.51333L10.5536 7.12667L4.51356
                      13.1667H3.90023V12.5533L9.94023 6.51333ZM12.3402
                      2.5C12.1736 2.5 12.0002 2.56667 11.8736 2.69333L10.6536
                      3.91333L13.1536 6.41333L14.3736 5.19333C14.6336 4.93333
                      14.6336 4.51333 14.3736 4.25333L12.8136 2.69333C12.6802
                      2.56 12.5136 2.5 12.3402 2.5ZM9.94023 4.62667L2.56689
                      12V14.5H5.06689L12.4402 7.12667L9.94023 4.62667Z"
                          fill="#CF3D60" />
                      </svg>
                      Edit visibility
                    </button>
                  </div>
                  <div class="__color-tools">
                    <div class="__color-selector"></div>
                    <div class="__contact-color-container">
                      <button
                        wire:click.prevent="setColor({{ $cnt->id }}, 'contact-yellow')"
                        class="contact-yellow">
                      </button>
                      <button
                        wire:click.prevent="setColor({{ $cnt->id }}, 'contact-blue')"
                        class="contact-blue">
                      </button>
                      <button
                        wire:click.prevent="setColor({{ $cnt->id }}, 'contact-pink')"
                        class="contact-pink">
                      </button>
                      <button
                        wire:click.prevent="setColor({{ $cnt->id }}, 'contact-green')"
                        class="contact-green">
                      </button>
                      <button
                        wire:click.prevent="setColor({{ $cnt->id }}, 'contact-purple')"
                        class="contact-purple">
                      </button>
                      <button
                        wire:click.prevent="removeColor({{ $cnt->id }})"
                        class="__remove-color">
                        {{-- <svg width="16" height="16" --}}
                        {{--   viewBox="0 0 16 16" fill="none" --}}
                        {{--   xmlns="http://www.w3.org/2000/svg"> --}}
                        {{--   <path d="M12.6673 4.27325L11.7273 3.33325L8.00065 --}}
                        {{--     7.05992L4.27398 3.33325L3.33398 4.27325L7.06065 --}}
                        {{--     7.99992L3.33398 11.7266L4.27398 12.6666L8.00065 --}}
                        {{--     8.93992L11.7273 12.6666L12.6673 11.7266L8.94065 --}}
                        {{--     7.99992L12.6673 4.27325Z" /> --}}
                        {{-- </svg> --}}
                      </button>
                    </div>
                  </div>
                </div>
              @endif
            </div>
            @if (Auth::user()->id == $user_id)
            @endif
          </div>
        @endforeach
      </div>
    </div>
  </div>
  @push('scripts')
    <script
      src="https://cdn.jsdelivr.net/gh/livewire/sortable@v0.x.x/dist/livewire-sortable.js">
    </script>
  @endpush
</div>
