@if (Auth::user()->free_account == 1)
  <div class="{{ Auth::user()->free_account == 1 ? '__full-height' : '' }}">
    <div
      style="display: grid; height: 100%; align-items: center; align-items: start;">
      <div class="__empty-state">
        <div>
          <a href="{{ url('register/upgrade/' . Auth::user()->id) }}">
            <svg width="35" height="34" viewBox="0 0 35 34" fill="none"
              xmlns="http://www.w3.org/2000/svg">
              <path d="M27.1667 11.0833H25.5833V7.91667C25.5833 3.54667 22.0367 0
            17.6667 0C13.2967 0 9.75 3.54667 9.75 7.91667V11.0833H8.16667C6.425
            11.0833 5 12.5083 5 14.25V30.0833C5 31.825 6.425 33.25 8.16667
            33.25H27.1667C28.9083 33.25 30.3333 31.825 30.3333
            30.0833V14.25C30.3333 12.5083 28.9083 11.0833 27.1667
            11.0833ZM12.9167 7.91667C12.9167 5.28833 15.0383 3.16667 17.6667
            3.16667C20.295 3.16667 22.4167 5.28833 22.4167
            7.91667V11.0833H12.9167V7.91667ZM27.1667
            30.0833H8.16667V14.25H27.1667V30.0833Z" fill="#CF3D60" />
            </svg>
          </a>
          <div>
            <a href="{{ url('register/upgrade/' . Auth::user()->id) }}">
              Upgrade to access  this feature
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
@endif

@if (Auth::user()->free_account == 0)
  <div class="myallBusinessWealth">
    <div class="">
      <div class="__heading">
        <h3>Business Wealth</h3>
        <p>Do you own any businesses? List them below!</p>
      </div>
      <div class="__island">
        <div class="">
          <div class="">
            <div class="d-flex heading-btn-display mb-3">
              <span class="add-business-btn add-btn-alignment">
                <a href="#" class="btn btn-outline-two outline"
                  wire:click.prevent="showForm">
                  <svg width="16" height="17" viewBox="0 0 16 17"
                    fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path
                      d="M12.6667
                    9.16732H8.66671V13.1673H7.33337V9.16732H3.33337V7.83398H7.33337V3.83398H8.66671V7.83398H12.6667V9.16732Z"
                      fill="#CF3D60" />
                  </svg>
                  Add Business
                </a>
              </span>
            </div>
            @if ($errors->any())
              <div class="alert alert-danger">
                <span>{{ $errors->first() }}</span>
              </div>
            @endif
            @if (session()->has('message'))
              <div class="alert alert-success">
                {{ session('message') }}
              </div>
            @endif

            @if ($cur_view == 'list')
              @include('livewire.account.organiser.business-wealth.list')
            @elseif($cur_view == 'form')
              @include('livewire.account.organiser.business-wealth.form')
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>
@endif

@push('scripts')
  <script>
    window.addEventListener('percentage-overload', event => {
      Swal.fire("Error", event.detail.message, "error");
    });
  </script>

  <script>
    window.addEventListener('reload-js', event => {
      easyNumberSeparator({
        selector: '.number-separator',
        separator: ','
      })
    });
  </script>
@endpush
</div>
