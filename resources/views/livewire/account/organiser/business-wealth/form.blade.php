<form wire:submit.prevent="saveBuysinessWealth">
  <div class="row">
    <div class="col-md-12">
      <div class="mb-3">
        <label class="form-label">Type of Business</label>
        <select class="form-control" name="business_type" wire:model="business_type"
          @if ($view == 'show') disabled @endif>
          <option value="" selected>Select Option</option>
          @foreach ($business_types as $type)
            <option value="{{ $type }}">{{ $type }}</option>
          @endforeach
        </select>
      </div>
    </div>
    <div class="col-md-6">
      <div class="mb-3">
        <label class="form-label">Business Name</label>
        <input type="text" class="form-control" name="business_name" wire:model.defer="business_name"
          @if ($view == 'show') disabled @endif>
      </div>
    </div>
    <div class="col-md-6">
      <div class="mb-3">
        <label class="form-label">Business Website URL</label>
        <input type="text" class="form-control" name="company_url" wire:model.defer="company_url"
          @if ($view == 'show') disabled @endif>
      </div>
    </div>
    @if ($business_type == 'Trust' || $business_type == 'Closed Corporation' || $business_type == 'Private Company')
      <div class="col-md-12">
        <div class="mb-3">
          <label class="form-label">Registration Number</label>
          <input type="text" class="form-control" name="registration_number" wire:model.defer="registration_number"
            @if ($view == 'show') disabled @endif>
        </div>
      </div>
    @endif
  </div>
  <div class="row">
    <div class="col-md-3">
      <div class="mb-3">
        <label class="form-label">VAT Registered</label>
        <div class="">
          <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="vat_registered" id="vat_yes" value="yes"
              wire:model="vat_registered" @if ($view == 'show') disabled @endif>
            <label class="form-check-label" for="vat_yes">Yes</label>
          </div>
          <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="vat_registered" id="vat_no" value="no"
              wire:model="vat_registered" @if ($view == 'show') disabled @endif>
            <label class="form-check-label" for="vat_no">No</label>
          </div>
        </div>
      </div>
    </div>
    @if ($vat_registered == 'yes')
      <div class="col-md-9">
        <div class="mb-3">
          <label class="form-label">VAT Number</label>
          <input type="text" class="form-control" name="vat_number" wire:model.defer="vat_number"
            @if ($view == 'show') disabled @endif>
        </div>
      </div>
    @endif
  </div>
  @if ($business_type == 'Trust')
    <div class="row">
      <div class="col-md-12">
        <div class="mb-3">
          <label class="form-label">Master's Office Branch</label>
          <select class="form-control" name="masters_office" wire:model.defer="masters_office"
            @if ($view == 'show') disabled @endif>
            <option value="" selected>Select Option</option>
            @foreach ($masters_offices as $off)
              <option value="{{ $off }}">{{ $off }}</option>
            @endforeach
          </select>
        </div>
      </div>
      <div class="@if ($trust_type != 'Other') col-md-12 @else col-md-6 @endif">
        <div class="mb-3">
          <label class="form-label">Type of Trust</label>
          <select class="form-control" name="trust_type" wire:model="trust_type"
            @if ($view == 'show') disabled @endif>
            <option value="" selected disabled>Select Option</option>
            @foreach ($trust_types as $trst)
              <option value="{{ $trst }}">{{ $trst }}</option>
            @endforeach
          </select>
        </div>
      </div>
      @if ($trust_type == 'Other')
        <div class="col-md-6">
          <div class="mb-3">
            <label class="form-label">Specify Trust Type</label>
            <input type="text" class="form-control" name="specific_trust_type" wire:model.defer="specific_trust_type"
              @if ($view == 'show') disabled @endif>
          </div>
        </div>
      @endif
    </div>
    <div class="row">
      <div class="col-md-12 d-flex">
        <h5>Trustees</h5>
        <span class="ms-auto">
          @if ($view == 'edit' || is_null($view))
            <a href="#" class="btn btn-outline-two" wire:click.prevent="addTrustee">ADD TRUSTEE</a>
          @endif
        </span>
      </div>
      @foreach ($trustees as $k => $v)
        <div class="col-md-4">
          <div class="mb-3">
            <label class="form-label">First Name</label>
            <input type="text" class="form-control" name="trustee_first_name"
              wire:model.defer="trustees.{{ $k }}.trustee_first_name"
              @if ($view == 'show') disabled @endif>
          </div>
        </div>
        <div class="col-md-4">
          <div class="mb-3">
            <label class="form-label">Surname</label>
            <input type="text" class="form-control" name="trustee_surname"
              wire:model.defer="trustees.{{ $k }}.trustee_surname"
              @if ($view == 'show') disabled @endif>
          </div>
        </div>
        <div class="col-md-4">
          <div class="mb-3">
            <label class="form-label">Contact</label>
            <input type="text" class="form-control" name="trustee_contact"
              wire:model.defer="trustees.{{ $k }}.trustee_contact"
              @if ($view == 'show') disabled @endif>
          </div>
        </div>
        @if (isset($v['id']) && $cur_id)
          <div class="mb-2 mt-1">
            @if ($view == 'edit' || is_null($view))
              <a href="#" target="_blank" class="btn btn-outline-two ms-2"
                wire:click.prevent="removeTrustee({{ $v['id'] }})">Remove Trustee</a>
            @endif
          </div>
        @endif
      @endforeach
    </div>
  @endif
  @if (
      $business_type == 'Closed Corporation' ||
          $business_type == 'Private Company' ||
          $business_type == 'Partnership' ||
          $business_type == 'Trust')
    <div class="row">
      <div class="col-md-12">
        <div class="col-md-12 d-flex">
          <h5>Possessions</h5>
          <span class="mb-2 ms-auto">
            @if ($view == 'edit' || is_null($view))
              <a href="#" class="btn btn-outline-two" wire:click.prevent="addPossession">ADD POSSESSION</a>
            @endif
          </span>
        </div>
        @foreach ($poses as $k => $pose)
          <div class="mb-3">
            <label class="form-label">Type Of Possession</label>
            <select class="form-control" name="possessions" wire:model="poses.{{ $k }}.possessions"
              @if ($view == 'show') disabled @endif>
              <option value="" selected disabled>Select Option</option>
              @foreach ($possessions_arr as $pos)
                <option value="{{ $pos }}">{{ $pos }}</option>
              @endforeach
            </select>
          </div>
          <div class="mb-3">
            <label class="form-label">Specify Possession</label>
            <input type="text" class="form-control" name="specify_possession"
              wire:model="poses.{{ $k }}.specify_possession"
              @if ($view == 'show') disabled @endif>
          </div>
          <div class="mb-3">
            <label class="form-label">Value</label>
            <div class="input-group mb-3">
              <span class="input-group-text">R</span>
              <input type="text" class="form-control number-separator" name="value"
                wire:model="poses.{{ $k }}.value" @if ($view == 'show') disabled @endif>
            </div>
          </div>

          @if (isset($pose['id']) && $cur_id)
            <div class="mb-2 mt-1">
              @if ($view == 'edit' || is_null($view))
                <a href="#" target="_blank" class="btn btn-outline-two ms-2"
                  wire:click.prevent="removePossession({{ $pose['id'] }})">Remove Possession</a>
              @endif
            </div>
          @endif
        @endforeach
      </div>
    </div>
  @endif
  @if ($business_type == 'Closed Corporation' || $business_type == 'Private Company' || $business_type == 'Partnership')
    @php
      if ($business_type == 'Closed Corporation') {
          $label = 'Members';
          $lb_single = 'Member';
          $tp_name = "Member's";
          $cmp_tp = 'CC';
      }
      if ($business_type == 'Private Company') {
          $label = 'Shareholders';
          $lb_single = 'Shareholder';
          $tp_name = "Shareholder's";
          $cmp_tp = 'Company';
      }
      if ($business_type == 'Partnership') {
          $label = 'Partners';
          $lb_single = 'Partner';
          $tp_name = "Partner's";
          $cmp_tp = 'Company';
      }
    @endphp
    <div class="row">
      <div class="col-md-12 d-flex flex-column flex-md-row">
        <h5>{{ $label }}</h5>
        <span class="ms-md-auto mb-md-0 mt-md-0 mb-2 mt-2">
          @if ($view == 'edit' || is_null($view))
            <a href="#" class="btn btn-outline-two" wire:click.prevent="addMember">ADD {{ $lb_single }}</a>
          @endif
        </span>
      </div>
      @foreach ($members as $k => $v)
        <div class="@if ($business_type != 'Partnership') col-md-4 @else col-md-6 @endif">
          <div class="mb-2">
            <label class="form-label">{{ $tp_name }} Name</label>
            <input type="text" class="form-control" name="member_name"
              wire:model.defer="members.{{ $k }}.member_name"
              @if ($view == 'show') disabled @endif>
          </div>
        </div>
        <div class="@if ($business_type != 'Partnership') col-md-4 @else col-md-6 @endif">
          <div class="mb-2">
            <label class="form-label">{{ $tp_name }} Surname</label>
            <input type="text" class="form-control" name="member_surname"
              wire:model.defer="members.{{ $k }}.member_surname"
              @if ($view == 'show') disabled @endif>
          </div>
        </div>
        @if ($business_type != 'Partnership')
          <div class="col-md-4">
            <div class="mb-2">
              <label class="form-label">Percentage Stake</label>
              <div class="input-group">
                <input type="text" class="form-control" name="stake_percentage"
                  wire:model.defer="members.{{ $k }}.stake_percentage"
                  @if ($view == 'show') disabled @endif>
                <span class="input-group-text">%</span>
              </div>
            </div>
          </div>
        @endif
        @if (isset($v['id']) && $cur_id)
          <div class="d-grid d-md-block mb-4 mt-0 gap-2">
            @if ($view == 'edit' || is_null($view))
              <a href="#" target="_blank" class="btn btn-outline-two ms-md-2 ms-0"
                wire:click.prevent="removeMember({{ $v['id'] }})">Remove {{ $lb_single }}</a>
            @endif
          </div>
        @endif
      @endforeach
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="mb-3">
          <label class="form-label">Buy & Sell Agreement</label>
          <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="buy_sell_agreement" id="buy_sell_agreement_yes"
              value="yes" wire:model.defer="buy_sell_agreement"
              @if ($view == 'show') disabled @endif>
            <label class="form-check-label" for="buy_sell_agreement_yes">Yes</label>
          </div>
          <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="buy_sell_agreement" id="buy_sell_agreement_no"
              value="no" wire:model.defer="buy_sell_agreement"
              @if ($view == 'show') disabled @endif>
            <label class="form-check-label" for="buy_sell_agreement_no">No</label>
          </div>
        </div>
      </div>
    </div>
  @endif

  <div class="row">
    <div class="col-md-12">
      <h5>Value</h5>
    </div>
    @if ($business_type == 'Closed Corporation' || $business_type == 'Private Company')
      <div class="col-md-12">
        <div class="mb-3">
          <label class="form-label">Stock Value</label>
          <div class="input-group">
            <input type="text" class="form-control number-separator" placeholder="1.00" name="value_min"
              wire:model.defer="value_min" @if ($view == 'show') disabled @endif>
            <span class="input-group-text"> - </span>
            <input type="text" class="form-control number-separator" placeholder="100 000 000.00"
              name="value_max" wire:model.defer="value_max" @if ($view == 'show') disabled @endif>
          </div>
        </div>
      </div>
    @endif
  </div>
  <div class="row">
    @if ($business_type == 'Sole Ownership')
      <div class="col-md-6">
        <label class="form-label">Value</label>
        <div class="input-group mb-3">
          <span class="input-group-text" id="value">R</span>
          <input type="text" class="form-control number-separator" name="value_min" wire:model.defer="value_min"
            @if ($view == 'show') disabled @endif>
        </div>
      </div>
      <div class="col-md-6">
        <div class="mb-3">
          <label class="form-label">Outstanding Debtors</label>
          <div class="input-group mb-3">
            <span class="input-group-text" id="outstanding_debtors">R</span>
            <input type="text" class="form-control number-separator" name="outstanding_debtors"
              wire:model.defer="outstanding_debtors" @if ($view == 'show') disabled @endif>
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="mb-3">
          <label class="form-label">Equipment</label>
          <div class="input-group mb-3">
            <span class="input-group-text" id="equipment">R</span>
            <input type="text" class="form-control number-separator" name="equipemnt"
              wire:model.defer="equipment" @if ($view == 'show') disabled @endif>
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="mb-3">
          <label class="form-label">Goodwill</label>
          <div class="input-group mb-3">
            <span class="input-group-text" id="foodwill">R</span>
            <input type="text" class="form-control number-separator" name="goodwill" wire:model.defer="goodwill"
              @if ($view == 'show') disabled @endif>
          </div>
        </div>
      </div>
    @endif
    @if ($business_type != 'Trust')
      <div class="col-md-12">
        <div class="mb-3">
          <label class="form-label">Net Value of Business</label>
          <div class="input-group mb-3">
            <span class="input-group-text" id="vet_value_of_company">R</span>
            <input type="text" class="form-control number-separator" name="net_value"
              wire:model.defer="net_value" @if ($view == 'show') disabled @endif>
          </div>
        </div>
      </div>
    @else
      <div class="@if ($business_type == 'Trust') col-md-6 @else col-md-4 @endif">
        <div class="mb-3">
          <label class="form-label">Value of loan account in favor of business (loan granted or provided to
            someone)</label>
          <div class="input-group mb-3">
            <span class="input-group-text" id="loan_value">R</span>
            <input type="text" class="form-control number-separator" name="credit" wire:model.defer="credit"
              @if ($view == 'show') disabled @endif>
          </div>
        </div>
      </div>
      <div class="@if ($business_type == 'Trust') col-md-6 @else col-md-4 @endif">
        <div class="mb-3">
          <label class="form-label">Value of Loan Account Against</label>
          <div class="input-group mb-3">
            <span class="input-group-text" id="loan_value_against">R</span>
            <input type="text" class="form-control number-separator" name="debt" wire:model.defer="debt"
              @if ($view == 'show') disabled @endif>
          </div>
        </div>
      </div>
    @endif
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="d-flex">
        <h5>Loan Account Against My Business</h5>
        <span class="btn-space ms-auto">
          @if ($view == 'edit' || is_null($view))
            <a href="#" class="btn btn-outline-two" wire:click.prevent="addDebtor">Add Loan</a>
          @endif
        </span>
      </div>
    </div>
    @foreach ($debtors as $k => $v)
      <div class="col-md-6">
        <div class="mb-3">
          <label class="form-label">Debt is owed to (money provided by someone)</label>
          <input type="text" class="form-control" name="debt_to"
            wire:model.defer="debtors.{{ $k }}.debt_to"
            @if ($view == 'show') disabled @endif>
        </div>
      </div>
      <div class="col-md-3">
        <div class="mb-3">
          <label class="form-label">Value</label>
          <div class="input-group mb-3">
            <span class="input-group-text" id="debt_value">R</span>
            <input type="text" class="form-control number-separator" name="debt_value"
              wire:model.defer="debtors.{{ $k }}.debt_value"
              @if ($view == 'show') disabled @endif>
          </div>
        </div>
      </div>
      <div class="col-md-3">
        <div class="mb-3">
          <label class="form-label">Is debt insured?</label>
          <div class="">
            <div class="form-check form-check-inline">
              <input class="form-check-input" type="radio" name="debt_insured.{{ $k }}"
                id="insured_yes" value="yes" wire:model.defer="debtors.{{ $k }}.debt_insured"
                @if ($view == 'show') disabled @endif>
              <label class="form-check-label" for="insured_yes">Yes</label>
            </div>
            <div class="form-check form-check-inline">
              <input class="form-check-input" type="radio" name="debt_insured.{{ $k }}"
                id="insured_no" value="no" wire:model.defer="debtors.{{ $k }}.debt_insured"
                @if ($view == 'show') disabled @endif>
              <label class="form-check-label" for="insured_no">No</label>
            </div>
            <div class="form-check form-check-inline">
              @if (isset($v['id']))
                @if ($view == 'edit' || is_null($view))
                  <a href="#" class="text-danger" wire:click.prevent="removeDebtor({{ $v['id'] }})"><i
                      class="bx bxs-trash"></i></a>
                @endif
              @endif
            </div>
          </div>
        </div>
      </div>
    @endforeach
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="d-flex">
        <h5>Creditors</h5>
        <span class="btn-space ms-auto">
          @if ($view == 'edit' || is_null($view))
            <a href="#" class="btn btn-outline-two" wire:click.prevent="addCreditor">Add Creditor</a>
          @endif
        </span>
      </div>
    </div>
    <div class="col-md-12">
      @foreach ($creditors as $key => $credit)
        <div class="row">
          <div class="col-md-6">
            <div class="mb-3">
              <label class="form-label">Credit is provided by</label>
              <input type="text" class="form-control" name="credit_by"
                wire:model.defer="creditors.{{ $key }}.credit_to"
                @if ($view == 'show') disabled @endif>
            </div>
          </div>
          <div class="col-md-3">
            <div class="mb-3">
              <label class="form-label">Value</label>
              <div class="input-group">
                <span class="input-group-text">R</span>
                <input type="text" class="form-control number-separator" name="credit_value"
                  wire:model.defer="creditors.{{ $key }}.credit_value"
                  @if ($view == 'show') disabled @endif>
              </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="mb-3">
              <label class="form-label">Is credit Insured?</label>
              <div class="">
                <div class="form-check form-check-inline">
                  <input class="form-check-input" type="radio" name="credit_insured_{{ $key }}"
                    wire:key="credit_yes_{{ $key }}" id="credit_yes" value="yes"
                    wire:model.defer="creditors.{{ $key }}.credit_insured"
                    @if ($view == 'show') disabled @endif>
                  <label class="form-check-label" for="credit_yes">Yes</label>
                </div>
                <div class="form-check form-check-inline">
                  <input class="form-check-input" type="radio" name="credit_insured_{{ $key }}"
                    wire:key="credit_no_{{ $key }}" id="credit_no" value="no"
                    wire:model.defer="creditors.{{ $key }}.credit_insured"
                    @if ($view == 'show') disabled @endif>
                  <label class="form-check-label" for="credit_no">No</label>
                </div>
              </div>
            </div>
          </div>
        </div>
      @endforeach
    </div>
  </div>


  @if (
      $business_type == 'Closed Corporation' ||
          $business_type == 'Private Company' ||
          $business_type == 'Partnership' ||
          $business_type == 'Sole Ownership')
    <div class="row">
      <div class="col-md-12">
        <h5>Auditor</h5>
      </div>
      <div class="col-md-6">
        <div class="mb-3">
          <label class="form-label">Auditor's Name</label>
          <input type="text" class="form-control" name="auditor_name" wire:model.defer="auditor_name"
            @if ($view == 'show') disabled @endif>
        </div>
      </div>
      <div class="col-md-6">
        <div class="mb-3">
          <label class="form-label">Auditor's Surname</label>
          <input type="text" class="form-control" name="auditor_surname" wire:model.defer="auditor_surname"
            @if ($view == 'show') disabled @endif>
        </div>
      </div>
      <div class="col-md-6">
        <div class="mb-3">
          <label class="form-label">Auditor's Tel</label>
          <input type="text" class="form-control" name="auditor_tel" wire:model.defer="auditor_tel"
            @if ($view == 'show') disabled @endif>
        </div>
      </div>
      <div class="col-md-6">
        <div class="mb-3">
          <label class="form-label">Auditor's Email</label>
          <input type="text" class="form-control" name="auditor_email" wire:model.defer="auditor_email"
            @if ($view == 'show') disabled @endif>
        </div>
      </div>
      <div class="col-md-12">
        <div class="mb-3">
          <label class="form-label">Auditor's Address</label>
          <textarea class="form-control" name="auditor_address" wire:model.defer="auditor_address"
            @if ($view == 'show') disabled @endif></textarea>
        </div>
      </div>
    </div>
  @endif
  @if ($business_type == 'Closed Corporation')
    <div class="row">
      <div class="col-md-12">
        <div class="f-w-input mb-3">
          <label class="form-label">Founding Statement and Certificate of Incorporation (CK1) Signed by the Authorised
            Member</label>
          @if ($founding_statement_file)
            <div class="d-grid d-md-block mt-2 gap-2">
              <a href="{{ url('storage/' . $founding_statement_file) }}" target="_blank"
                class="btn btn btn-outline-two">View File</a>
              @if ($view == 'edit')
                <a href="#" class="btn btn btn-outline-two ms-md-2 ms-0"
                  wire:click.prevent="removeDocument({{ $cur_id }},'founding_statement')">Remove File</a>
              @endif
            </div>
          @else
            <input type="file" class="btm-border-input file-input-rad" wire:model="founding_statement"
              @if ($view == 'show') disabled @endif>
            @error('founding_statement')
              <span class="error text-red">Failed to upload(File must not exceed limit of 2MB and file type: pdf, jpeg,
                png)</span>
            @enderror
            </br>
            <span class="text-muted">File: Max File Size 2MB (pdf, jpeg, png)</span>
          @endif
        </div>
      </div>
      <div class="col-md-12">
        <div class="f-w-input mb-3">
          <label class="form-label">Latest Amended Founding Statement (CK2) if Applicable, Signed by the Authorised
            Member</label>
          @if ($amended_founding_statement_file)
            <div class="d-grid d-md-block mt-2 gap-2">
              <a href="{{ url('storage/' . $amended_founding_statement_file) }}" target="_blank"
                class="btn btn btn-outline-two">View File</a>
              @if ($view == 'edit')
                <a href="#" class="btn btn btn-outline-two ms-md-2 ms-0"
                  wire:click.prevent="removeDocument({{ $cur_id }},'amended_founding_statement')">Remove
                  File</a>
              @endif
            </div>
          @else
            <input type="file" class="btm-border-input file-input-rad" wire:model="amended_founding_statement"
              @if ($view == 'show') disabled @endif>
            @error('amended_founding_statement')
              <span class="error text-red">Failed to upload(File must not exceed limit of 2MB and file type:
                pdf,jpeg,png)</span>
            @enderror
            </br>
            <span class="text-muted">File: Max File Size 2MB (pdf, jpeg, png)</span>
          @endif
        </div>
      </div>
      <div class="col-md-12">
        <div class="f-w-input mb-3">
          <label class="form-label">Members Resolution Appointing the Authorised Representative of the CC</label>
          @if ($members_resolution_file)
            <div class="d-grid d-md-block mt-2 gap-2">
              <a href="{{ url('storage/' . $members_resolution_file) }}" target="_blank"
                class="btn btn btn-outline-two">View File</a>
              @if ($view == 'edit')
                <a href="#" class="btn btn btn-outline-two ms-md-2 ms-0"
                  wire:click.prevent="removeDocument({{ $cur_id }},'members_resolution')">Remove File</a>
              @endif
            </div>
          @else
            <input type="file" class="btm-border-input file-input-rad" wire:model="members_resolution"
              @if ($view == 'show') disabled @endif>
            @error('members_resolution')
              <span class="error text-red">Failed to upload(File must not exceed limit of 2MB and file type: pdf, jpeg,
                png)</span>
            @enderror
            </br>
            <span class="text-muted">File: Max File Size 2MB (pdf, jpeg, png)</span>
          @endif
        </div>
      </div>
    </div>
  @endif
  @if ($business_type == 'Private Company')
    <div class="row">
      <div class="col-md-12">
        <div class="f-w-input mb-3">
          <label class="form-label"> Certificate of Registration, Memorandum of Incorporation, Certificate of Name
            Change</label>
          @if ($certificate_of_registration_file)
            <div class="d-grid d-md-block mt-2 gap-2">
              <a href="{{ url('storage/' . $certificate_of_registration_file) }}" target="_blank"
                class="btn btn btn-outline-two">View File</a>
              @if ($view == 'edit')
                <a href="#" class="btn btn btn-outline-two ms-md-2 ms-0"
                  wire:click.prevent="removeDocument({{ $cur_id }},'certificate_of_registration')">Remove
                  File</a>
              @endif
            </div>
          @else
            <input type="file" class="btm-border-input file-input-rad" wire:model="certificate_of_registration"
              @if ($view == 'show') disabled @endif>
            @error('certificate_of_registration')
              <span class="error text-red">Failed to upload(File must not exceed limit of 2MB and file type:
                pdf,jpeg,png)</span>
            @enderror
            </br>
            <span class="text-muted">File: Max File Size 2MB (pdf, jpeg, png)</span>
          @endif

        </div>
      </div>
      <div class="col-md-12">
        <div class="f-w-input mb-3">
          <label class="form-label"> SARS Issued Document Confirming Income Tax and VAT Registration Number</label>
          @if ($sars_issued_document_file)
            <div class="d-grid d-md-block mt-2 gap-2">
              <a href="{{ url('storage/' . $sars_issued_document_file) }}" target="_blank"
                class="btn btn btn-outline-two">View File</a>
              @if ($view == 'edit')
                <a href="#" class="btn btn btn-outline-two ms-md-2 ms-0"
                  wire:click.prevent="removeDocument({{ $cur_id }},'sars_issued_document')">Remove File</a>
              @endif
            </div>
          @else
            <input type="file" class="btm-border-input file-input-rad" wire:model="sars_issued_document"
              @if ($view == 'show') disabled @endif>
            @error('sars_issued_document')
              <span class="error text-red">Failed to upload(File must not exceed limit of 2MB and file type: pdf, jpeg,
                png)</span>
            @enderror
            </br>
            <span class="text-muted">File: Max File Size 2MB (pdf, jpeg, png)</span>
          @endif
        </div>
      </div>
      <div class="col-md-12">
        <div class="f-w-input mb-3">
          <label class="form-label">Letter From the Auditors Confirming Shareholding</label>
          @if ($letter_from_auditors_file)
            <div class="d-grid d-md-block mt-2 gap-2">
              <a href="{{ url('storage/' . $letter_from_auditors_file) }}" target="_blank"
                class="btn btn btn-outline-two">View File</a>
              @if ($view == 'edit')
                <a href="#" class="btn btn btn-outline-two ms-md-2 ms-0"
                  wire:click.prevent="removeDocument({{ $cur_id }},'letter_from_auditors')">Remove File</a>
              @endif
            </div>
          @else
            <input type="file" class="btm-border-input file-input-rad" wire:model="letter_from_auditors"
              @if ($view == 'show') disabled @endif>
            @error('letter_from_auditors')
              <span class="error text-red">Failed to upload(File must not exceed limit of 2MB and file type: pdf, jpeg,
                png)</span>
            @enderror
            </br>
            <span class="text-muted">File: Max File Size 2MB (pdf, jpeg, png)</span>
          @endif
        </div>
      </div>
      <div class="col-md-12">
        <div class="f-w-input mb-3">
          <label class="form-label">Directors Resolution Appointing the Authorised Representative of the
            Company</label>
          @if ($directors_resolution_file)
            <div class="d-grid d-md-block mt-2 gap-2">
              <a href="{{ url('storage/' . $directors_resolution_file) }}" target="_blank"
                class="btn btn btn-outline-two">View File</a>
              @if ($view == 'edit')
                <a href="#" class="btn btn btn-outline-two ms-md-2 ms-0"
                  wire:click.prevent="removeDocument({{ $cur_id }},'directors_resolution')">Remove File</a>
              @endif
            </div>
          @else
            <input type="file" class="btm-border-input file-input-rad" wire:model="directors_resolution"
              @if ($view == 'show') disabled @endif>
            @error('directors_resolution')
              <span class="error text-red">Failed to upload(File must not exceed limit of 2MB and file type: pdf, jpeg,
                png)</span>
            @enderror
            </br>
            <span class="text-muted">File: Max File Size 2MB (pdf, jpeg, png)</span>
          @endif
        </div>
      </div>
    </div>
  @endif
  @if ($business_type == 'Trust')
    <div class="row">
      <div class="col-md-12">
        <div class="f-w-input mb-3">
          <label class="form-label">Trust Deed</label>
          @if ($trust_deed_file)
            <div class="d-grid d-md-block mt-2 gap-2">
              <a href="{{ url('storage/' . $trust_deed_file) }}" target="_blank" class="btn btn btn-outline-two">View
                File</a>
              @if ($view == 'edit')
                <a href="#" class="btn btn btn-outline-two ms-md-2 ms-0"
                  wire:click.prevent="removeDocument({{ $cur_id }},'trust_deed')">Remove File</a>
              @endif
            </div>
          @else
            <input type="file" class="btm-border-input file-input-rad" wire:model="trust_deed"
              @if ($view == 'show') disabled @endif>
            @error('trust_deed')
              <span class="error text-red">Failed to upload(File must not exceed limit of 2MB and file type: pdf, jpeg,
                png)</span>
            @enderror
            </br>
            <span class="text-muted">File: Max File Size 2MB (pdf, jpeg, png)</span>
          @endif
        </div>
      </div>
      <div class="col-md-12">
        <div class="f-w-input mb-3">
          <label class="form-label">Letter of Authority</label>
          @if ($letter_of_authority_file)
            <div class="d-grid d-md-block mt-2 gap-2">
              <a href="{{ url('storage/' . $letter_of_authority_file) }}" target="_blank"
                class="btn btn btn-outline-two">View File</a>
              @if ($view == 'edit')
                <a href="#" class="btn btn btn-outline-two ms-md-2 ms-0"
                  wire:click.prevent="removeDocument({{ $cur_id }},'letter_of_authority')">Remove File</a>
              @endif
            </div>
          @else
            <input type="file" class="btm-border-input file-input-rad" wire:model="letter_of_authority"
              @if ($view == 'show') disabled @endif>
            @error('letter_of_authority')
              <span class="error text-red">Failed to upload(File must not exceed limit of 2MB and file type: pdf, jpeg,
                png)</span>
            @enderror
            </br>
            <span class="text-muted">File: Max File Size 2MB (pdf, jpeg, png)</span>
          @endif
        </div>
      </div>
      <div class="col-md-12">
        <div class="f-w-input mb-3">
          <label class="form-label">Resolution Passed by Trustees</label>
          @if ($resolution_by_trustee_file)
            <div class="d-grid d-md-block mt-2 gap-2">
              <a href="{{ url('storage/' . $resolution_by_trustee_file) }}" target="_blank"
                class="btn btn btn-outline-two">View File</a>
              @if ($view == 'edit')
                <a href="#" class="btn btn btn-outline-two ms-md-2 ms-0"
                  wire:click.prevent="removeDocument({{ $cur_id }},'resolution_by_trustee')">Remove File</a>
              @endif
            </div>
          @else
            <input type="file" class="btm-border-input file-input-rad" wire:model="resolution_by_trustee"
              @if ($view == 'show') disabled @endif>
            @error('resolution_by_trustee')
              <span class="error text-red">Failed to upload(File must not exceed limit of 2MB and file type: pdf, jpeg,
                png)</span>
            @enderror
            </br>
            <span class="text-muted">File: Max File Size 2MB (pdf, jpeg, png)</span>
          @endif
        </div>
      </div>
    </div>
  @endif
  <div class="row">
    <div class="col-md-12 d-grid d-md-block mt-2 gap-2">
      @if ($view == 'edit' || is_null($view))
        <input type="submit" class="btn btn-red" value="save">
      @else
        <button class="btn btn-red" wire:click.prevent="closeWindow()">Close</button>
      @endif
    </div>
  </div>
</form>

