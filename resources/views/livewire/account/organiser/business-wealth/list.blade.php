<div class="row __list">
  <div class="col-md-12 overflow-auto">
    <table class="table">
      <thead>
        <tr>
          <th>Business Name</th>
          <th>Business Type</th>
          <th>Registration Number</th>
          {{-- <th>VAT No</th> --}}
          {{-- <th></th> --}}
          <th></th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        @foreach ($cmps as $cmp)
          <tr>

            <td class="__business-name">
              <a href="#" class="text-danger"
                wire:click.prevent="showForm('show', {{ $cmp->id }})">
                {{ $cmp->business_name }}
              </a>
            </td>

            <td>{{ $cmp->business_type }}</td>
            <td>{{ $cmp->registration_number }}</td>
            {{-- <td>{{ $cmp->vat_number }}</td> --}}
            {{-- <td> --}}
            {{--   <a href="#" class="text-danger" --}}
            {{--     wire:click.prevent="removeBusiness({{ $cmp->id }})"> --}}
            {{--     <i class="bx bxs-trash"></i> --}}
            {{--     Delete --}}
            {{--   </a> --}}
            {{-- </td> --}}

            <td>
              <div class="__actions __border">
                <a href="#" class="text-danger"
                  wire:click.prevent="showForm('edit', {{ $cmp->id }})">
                  <svg width="20" height="20" viewBox="0 0 20 20"
                    fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M11.7157 7.51667L12.4824 8.28333L4.93236
                    15.8333H4.16569V15.0667L11.7157 7.51667ZM14.7157 2.5C14.5074
                    2.5 14.2907 2.58333 14.1324 2.74167L12.6074 4.26667L15.7324
                    7.39167L17.2574 5.86667C17.5824 5.54167 17.5824 5.01667
                    17.2574 4.69167L15.3074 2.74167C15.1407 2.575 14.9324 2.5
                    14.7157 2.5ZM11.7157 5.15833L2.49902
                    14.375V17.5H5.62402L14.8407 8.28333L11.7157 5.15833Z"
                      fill="#CF3D60" />
                  </svg>
                  Edit
                </a>
              </div>
            </td>

            <td>
              <div class="__actions">
                <a href="#" class="text-danger"
                  wire:click.prevent="removeBusiness({{ $cmp->id }})">
                  <svg width="20" height="20" viewBox="0 0 20 20"
                    fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M13.3334 7.5V15.8333H6.66675V7.5H13.3334ZM12.0834
                    2.5H7.91675L7.08341
                    3.33333H4.16675V5H15.8334V3.33333H12.9167L12.0834
                    2.5ZM15.0001 5.83333H5.00008V15.8333C5.00008 16.75 5.75008
                    17.5 6.66675 17.5H13.3334C14.2501 17.5 15.0001 16.75 15.0001
                    15.8333V5.83333Z" fill="#CF3D60" />
                  </svg>
                  Remove
                </a>
              </div>
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>
