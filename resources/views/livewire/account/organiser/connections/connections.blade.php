<div
  class="myallConnections {{ Auth::user()->free_account == 1 ? '__full-height' : '' }}">

  @if (Auth::user()->free_account == 1)
    <div
      style="display: grid; height: 100%; align-items: center; align-items: start;">
      <div class="__empty-state">
        <div>
          <a href="{{ url('register/upgrade/' . Auth::user()->id) }}">
            <svg width="35" height="34" viewBox="0 0 35 34" fill="none"
              xmlns="http://www.w3.org/2000/svg">
              <path d="M27.1667 11.0833H25.5833V7.91667C25.5833 3.54667 22.0367 0
            17.6667 0C13.2967 0 9.75 3.54667 9.75 7.91667V11.0833H8.16667C6.425
            11.0833 5 12.5083 5 14.25V30.0833C5 31.825 6.425 33.25 8.16667
            33.25H27.1667C28.9083 33.25 30.3333 31.825 30.3333
            30.0833V14.25C30.3333 12.5083 28.9083 11.0833 27.1667
            11.0833ZM12.9167 7.91667C12.9167 5.28833 15.0383 3.16667 17.6667
            3.16667C20.295 3.16667 22.4167 5.28833 22.4167
            7.91667V11.0833H12.9167V7.91667ZM27.1667
            30.0833H8.16667V14.25H27.1667V30.0833Z" fill="#CF3D60" />
            </svg>
          </a>
          <div>
            <a href="{{ url('register/upgrade/' . Auth::user()->id) }}">
              Upgrade to access  this feature
            </a>
          </div>
        </div>
      </div>
    </div>
  @endif

  @if (Auth::user()->free_account == 0)
    <div class="card-box-style">
      <div class="__heading">
        <h3>Emergency Contacts</h3>
        <p>In case your loved ones can't get hold of you or someone needs these
          details in an emergency, add your life's emergency contacts here for
          quick access. Make sure you share it in the 'MyPeople' section with
          the
          relevant people who might need it, as they won't have access to these
          contacts unless you share them.</p>
      </div>
      <div class="">
        <div class="">
          <div class="">
            @if ($errors->any())
              <div class="alert alert-danger">
                <span>{{ $errors->first() }}</span>
              </div>
            @endif
            @if (session()->has('message'))
              <div class="alert alert-success">
                {{ session('message') }}
              </div>
            @endif
            <div class="accordion" id="accordionConnection">
              <div class="accordion-item">
                <h2 class="accordion-header" id="headingOne">
                  <button class="accordion-button collapsed" type="button"
                    data-bs-toggle="collapse" data-bs-target="#family"
                    aria-expanded="false" aria-controls="family">
                    Family
                  </button>
                </h2>
                <div id="family" class="accordion-collapse collapse"
                  aria-labelledby="headingOne"
                  data-bs-parent="#accordionConnection" wire:ignore.self>
                  <div class="accordion-body">
                    <div class="">
                      @include('livewire.account.organiser.connections.forms.family')
                    </div>
                  </div>
                </div>
              </div>
              <div class="accordion-item">
                <h2 class="accordion-header" id="headingTwo">
                  <button class="accordion-button collapsed" type="button"
                    data-bs-toggle="collapse" data-bs-target="#personal"
                    aria-expanded="false" aria-controls="personal">
                    Friends
                  </button>
                </h2>
                <div id="personal" class="accordion-collapse collapse"
                  aria-labelledby="headingTwo"
                  data-bs-parent="#accordionConnection" wire:ignore.self>
                  <div class="accordion-body">
                    <div class="">
                      @include('livewire.account.organiser.connections.forms.personal')
                    </div>
                  </div>
                </div>
              </div>
              <div class="accordion-item">
                <h2 class="accordion-header" id="headingThree">
                  <button class="accordion-button collapsed" type="button"
                    data-bs-toggle="collapse" data-bs-target="#business"
                    aria-expanded="false" aria-controls="business">
                    Business
                  </button>
                </h2>
                <div id="business" class="accordion-collapse collapse"
                  aria-labelledby="headingThree"
                  data-bs-parent="#accordionConnection" wire:ignore.self>
                  <div class="accordion-body">
                    <div class="">
                      @include('livewire.account.organiser.connections.forms.business')
                    </div>
                  </div>
                </div>
              </div>
              <div class="accordion-item">
                <h2 class="accordion-header" id="headingFour">
                  <button class="accordion-button collapsed" type="button"
                    data-bs-toggle="collapse" data-bs-target="#doctor"
                    aria-expanded="false" aria-controls="doctor">
                    Your Doctor's Contact Details
                  </button>
                </h2>
                <div id="doctor" class="accordion-collapse collapse"
                  aria-labelledby="headingFour"
                  data-bs-parent="#accordionConnection" wire:ignore.self>
                  <div class="accordion-body">
                    <div class="">
                      @include('livewire.account.organiser.connections.forms.doctor')
                    </div>
                  </div>
                </div>
              </div>
              {{--
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="headingFive">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#emergency" aria-expanded="false" aria-controls="emergency">
                                    Emergency
                                </button>
                            </h2>
                            <div id="emergency" class="accordion-collapse collapse" aria-labelledby="headingFive" data-bs-parent="#accordionConnection" wire:ignore.self>
                                <div class="accordion-body">
                                    <div class="">
                                        @include('livewire.account.organiser.connections.forms.emergency')
                                    </div>
                                </div>
                            </div>
                        </div>
                        --}}
              <div class="accordion-item">
                <h2 class="accordion-header" id="headingSix">
                  <button class="accordion-button collapsed" type="button"
                    data-bs-toggle="collapse" data-bs-target="#accountant"
                    aria-expanded="false" aria-controls="accountant">
                    Accountant / Bookkeeper
                  </button>
                </h2>
                <div id="accountant" class="accordion-collapse collapse"
                  aria-labelledby="headingSix"
                  data-bs-parent="#accordionConnection" wire:ignore.self>
                  <div class="accordion-body">
                    <div class="">
                      @include('livewire.account.organiser.connections.forms.accountant')
                    </div>
                  </div>
                </div>
              </div>
              <div class="accordion-item">
                <h2 class="accordion-header" id="headingSeven">
                  <button class="accordion-button collapsed" type="button"
                    data-bs-toggle="collapse" data-bs-target="#banker"
                    aria-expanded="false" aria-controls="banker">
                    Private Banker
                  </button>
                </h2>
                <div id="banker" class="accordion-collapse collapse"
                  aria-labelledby="headingSeven"
                  data-bs-parent="#accordionConnection" wire:ignore.self>
                  <div class="accordion-body">
                    <div class="">
                      @include('livewire.account.organiser.connections.forms.banker')
                    </div>
                  </div>
                </div>
              </div>
              <div class="accordion-item">
                <h2 class="accordion-header" id="headingEight">
                  <button class="accordion-button collapsed" type="button"
                    data-bs-toggle="collapse" data-bs-target="#lawyer"
                    aria-expanded="false" aria-controls="lawyer">
                    Lawyer
                  </button>
                </h2>
                <div id="lawyer" class="accordion-collapse collapse"
                  aria-labelledby="headingEight"
                  data-bs-parent="#accordionConnection" wire:ignore.self>
                  <div class="accordion-body">
                    <div class="">
                      @include('livewire.account.organiser.connections.forms.lawyer')
                    </div>
                  </div>
                </div>
              </div>
              <div class="accordion-item">
                <h2 class="accordion-header" id="headingOne">
                  <button class="accordion-button collapsed" type="button"
                    data-bs-toggle="collapse" data-bs-target="#other"
                    aria-expanded="false" aria-controls="other">
                    Other Emergency Contacts
                  </button>
                </h2>
                <div id="other" class="accordion-collapse collapse"
                  aria-labelledby="headingOne"
                  data-bs-parent="#accordionConnection" wire:ignore.self>
                  <div class="accordion-body">
                    <div class="">
                      @include('livewire.account.organiser.connections.forms.other')
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  @endif
</div>
@push('scripts')
  <script>
    $(document).ready(function() {
      window.addEventListener('data-updated', event => {
        Swal.fire("Success", event.detail.message, "success");
      });
    });
  </script>
@endpush
