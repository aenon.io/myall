<form wire:submit.prevent="SaveContacts">
  @foreach ($contacts as $k => $fam)
    @if ($fam['contact_type'] == 'doctor')
      <div class="row __item __contact mb-3">
        @if (isset($fam['id']))
          <div class="col-md-12 d-grid d-md-block mt-2 gap-2 text-end">
            <a href="#" class="btn btn-outline-two"
              wire:click.prevent="remove({{ $fam['id'] }})">
              <svg width="16" height="17" viewBox="0 0 16 17" fill="none"
                xmlns="http://www.w3.org/2000/svg">
                <path
                  d="M10.6668 6.5V13.1667H5.3335V6.5H10.6668ZM9.66683 2.5H6.3335L5.66683 3.16667H3.3335V4.5H12.6668V3.16667H10.3335L9.66683 2.5ZM12.0002 5.16667H4.00016V13.1667C4.00016 13.9 4.60016 14.5 5.3335 14.5H10.6668C11.4002 14.5 12.0002 13.9 12.0002 13.1667V5.16667Z"
                  fill="#CF3D60" />
              </svg>
              Remove
            </a>
          </div>
        @endif
        <div class="col-md-6">
          <div class="f-w-input mb-3">
            <label class="form-label">Name</label>
            <input type="text" class="btm-border-input"
              wire:model.defer="contacts.{{ $k }}.name">
          </div>
        </div>
        <div class="col-md-6">
          <div class="f-w-input mb-3">
            <label class="form-label">Surname</label>
            <input type="text" class="btm-border-input"
              wire:model.defer="contacts.{{ $k }}.surname">
          </div>
        </div>
        <div class="col-md-6">
          <div class="f-w-input mb-3">
            <label class="form-label">Email</label>
            <input type="text" class="btm-border-input"
              wire:model.defer="contacts.{{ $k }}.email">
          </div>
        </div>
        <div class="col-md-6">
          <div class="f-w-input mb-3">
            <label class="form-label">Contact Number</label>
            <input type="text" class="btm-border-input"
              wire:model.defer="contacts.{{ $k }}.contact_number">
          </div>
        </div>
      </div>
    @endif
  @endforeach
  <div class="row">
    <div class="col-md-12 d-grid justify-content-end gap-2 text-end">
      <span class="ms-auto">
        <span class="pe-2">
          <i class="fa fa-info-circle" style="cursor: pointer; font-size: 20px;"
            data-bs-toggle="modal" data-bs-target="#doctor-description"></i>
        </span>
        <a href="#" class="btn btn-outline-two"
          wire:click.prevent="addContact('doctor')">
          <svg width="16" height="17" viewBox="0 0 16 17" fill="none"
            xmlns="http://www.w3.org/2000/svg">
            <path
              d="M12.6666
            9.16536H8.66659V13.1654H7.33325V9.16536H3.33325V7.83203H7.33325V3.83203H8.66659V7.83203H12.6666V9.16536Z"
              fill="#CF3D60" />
          </svg>
          Add
        </a>
      </span>
    </div>
  </div>
  <div class="col-md-12">
    <hr>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div
        class="col-md-12 justify-content-end upload-view-btns d-grid mb-4 gap-2">
        <input type="submit" class="btn btn-red" value="save">
      </div>
    </div>
  </div>
</form>

<div class="modal" tabindex="-1" id="doctor-description">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-body">
        <div class="d-flex">
          <h5>Doctors</h5>
          <span class="ms-auto">
            <button type="button" class="btn-close" data-bs-dismiss="modal"
              aria-label="Close"></button>
          </span>
        </div>
        <div class="mb-5">
          <p>In ‘Doctors’ you can document basic information about your doctor.
            This information will be accessible to
            your executor after your passing and can be crucial in case of an
            emergency. Please share this information
            with the person you believe will need it.</p>
        </div>
      </div>
    </div>
  </div>
</div>
