<div class="">
  <div class="card-box-style">
    <div class="__heading">
      <h3>Debt & Expenses</h3>
      <p>Struggling to remember when to pay what and what money went where? Add
        them here and make it easier!
      </p>
    </div>
    <div class="">
      <div class="">
        <div class="">
          <div class="">
            <div class="d-block d-sm-none mb-2 mt-3">
              <span class="text-danger"><b>NOTE:</b> Please scroll left-right to
                view the info in each section
                below.</span>
            </div>
          </div>
          @if (session()->has('message'))
            <div class="alert alert-success">
              {{ session('message') }}
            </div>
          @endif
          <div class="row">
            <div class="col-md-12">
              <div class="accordion" id="accordionExample">
                @php
                  $count = 0;
                @endphp
                <div class="accordion-item">
                  <h2 class="accordion-header" id="heading-Expenses">
                    <button class="accordion-button collapsed" type="button"
                      data-bs-toggle="collapse" data-bs-target="#expenses"
                      aria-expanded="false" aria-controls="expenses">
                      Expenses
                    </button>
                  </h2>
                  <div id="expenses" class="accordion-collapse __list collapse"
                    aria-labelledby="expenses-heading"
                    data-bs-parent="#accordionExample">
                    <div class="accordion-body">
                      <div class="row">
                        <div class="col-md-12">
                          <div class="d-flex">
                            <h5></h5>
                            <span class="">
                              <a href="#" class="btn btn-outline-two"
                                wire:click.prevent="showModal('expense')">
                                <svg width="16" height="17"
                                  viewBox="0 0 16 17" fill="none"
                                  xmlns="http://www.w3.org/2000/svg">
                                  <path
                                    d="M12.6667
              9.16732H8.66671V13.1673H7.33337V9.16732H3.33337V7.83398H7.33337V3.83398H8.66671V7.83398H12.6667V9.16732Z"
                                    fill="#CF3D60" />
                                </svg>
                                Add Expense
                              </a>
                            </span>
                          </div>
                        </div>
                        <div class="col-md-12 overflow-auto">
                          <table class="table">
                            <thead>
                              <tr>
                                <th>Description</th>
                                <th>Amount</th>
                                <th>Payment Terms</th>
                                <th>Payment Date</th>
                                <th>File</th>
                                <td></td>
                                {{-- <td></td> --}}
                                <td></td>
                              </tr>
                            </thead>
                            <tbody>
                              @foreach ($exps as $k => $e_exp)
                                @if (
                                    $k == 'Expenses' ||
                                        $k == 'Communication' ||
                                        $k == 'Donations' ||
                                        $k == 'Education' ||
                                        $k == 'Entertainment & Subscriptions' ||
                                        $k == 'Household' ||
                                        $k == 'Personal' ||
                                        $k == 'Professional Services' ||
                                        $k == 'Rent, Levies & Licenses' ||
                                        $k == 'Other')
                                  @foreach ($e_exp as $exp)
                                    <tr>
                                      <td>
                                        <a href="#"
                                          wire:click.prevent="showModal('expense', 'show', {{ $exp->id }})">
                                          {{ ucwords($exp->description) }}
                                        </a>
                                      </td>
                                      <td>{{ number_format($exp->amount, 2) }}
                                      </td>
                                      <td>{{ $exp->term }}</td>
                                      <td>{{ $exp->payment_date }}</td>
                                      <td>
                                        @if ($exp->document)
                                          <a href="{{ url('storage/' . $exp->document) }}"
                                            target="_blank"><i
                                              class="bx bxs-file"></i></a>
                                        @endif
                                      </td>

                                      <td>
                                        {{-- <a href="#" --}}
                                        {{--   wire:click.prevent="removeExpense({{ $exp->id }})"> --}}
                                        {{--   <i class="bx bxs-trash"></i> --}}
                                        {{-- </a> --}}
                                        <div class="__actions __border">
                                          <a href="#" class="text-danger"
                                            wire:click.prevent="showModal('expense', 'edit', {{ $exp->id }})">
                                            <svg width="20" height="20"
                                              viewBox="0 0 20 20" fill="none"
                                              xmlns="http://www.w3.org/2000/svg">
                                              <path d="M11.7157 7.51667L12.4824 8.28333L4.93236
                    15.8333H4.16569V15.0667L11.7157 7.51667ZM14.7157 2.5C14.5074
                    2.5 14.2907 2.58333 14.1324 2.74167L12.6074 4.26667L15.7324
                    7.39167L17.2574 5.86667C17.5824 5.54167 17.5824 5.01667
                    17.2574 4.69167L15.3074 2.74167C15.1407 2.575 14.9324 2.5
                    14.7157 2.5ZM11.7157 5.15833L2.49902
                    14.375V17.5H5.62402L14.8407 8.28333L11.7157 5.15833Z"
                                                fill="#CF3D60" />
                                            </svg>
                                            Edit
                                          </a>
                                        </div>
                                      </td>

                                      {{-- <td> --}}
                                      {{--   <a href="#" --}}
                                      {{--     wire:click.prevent="showModal('expense', 'edit', {{ $exp->id }})"> --}}
                                      {{--     <i class="bx bxs-edit"></i> --}}
                                      {{--   </a> --}}
                                      {{-- </td> --}}

                                      <td>
                                        {{-- <a href="#" --}}
                                        {{--   wire:click.prevent="showModal('expense', 'show', {{ $exp->id }})"> --}}
                                        {{--   <i class="bx bxs-show"></i> --}}
                                        {{-- </a> --}}
                                        <div class="__actions">
                                          <a href="#" class="text-danger"
                                            wire:click.prevent="removeExpense({{ $exp->id }})">
                                            <svg width="20" height="20"
                                              viewBox="0 0 20 20" fill="none"
                                              xmlns="http://www.w3.org/2000/svg">
                                              <path d="M13.3334 7.5V15.8333H6.66675V7.5H13.3334ZM12.0834
                    2.5H7.91675L7.08341
                    3.33333H4.16675V5H15.8334V3.33333H12.9167L12.0834
                    2.5ZM15.0001 5.83333H5.00008V15.8333C5.00008 16.75 5.75008
                    17.5 6.66675 17.5H13.3334C14.2501 17.5 15.0001 16.75 15.0001
                    15.8333V5.83333Z" fill="#CF3D60" />
                                            </svg>
                                            Remove
                                          </a>
                                        </div>
                                      </td>
                                    </tr>
                                  @endforeach
                                @endif
                              @endforeach
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="accordion-item">
                  <h2 class="accordion-header" id="Loans-heading">
                    <button class="accordion-button collapsed" type="button"
                      data-bs-toggle="collapse" data-bs-target="#loans"
                      aria-expanded="false" aria-controls="loans">
                      Loans
                    </button>
                  </h2>
                  <div id="loans" class="accordion-collapse __list collapse"
                    aria-labelledby="loans-heading"
                    data-bs-parent="#accordionExample">
                    <div class="accordion-body">
                      <div class="row">
                        <div class="col-md-12">
                          <div class="d-flex">
                            <h5></h5>
                            <span class="">
                              <a href="#" class="btn btn-outline-two"
                                wire:click.prevent="showModal('loan')">
                                <svg width="16" height="17"
                                  viewBox="0 0 16 17" fill="none"
                                  xmlns="http://www.w3.org/2000/svg">
                                  <path
                                    d="M12.6667
              9.16732H8.66671V13.1673H7.33337V9.16732H3.33337V7.83398H7.33337V3.83398H8.66671V7.83398H12.6667V9.16732Z"
                                    fill="#CF3D60" />
                                </svg>
                                Add Loan
                              </a>
                            </span>
                          </div>
                        </div>
                        <div class="col-md-12 overflow-auto">
                          <table class="table">
                            <thead>
                              <tr>
                                <th>Description</th>
                                <th>Amount</th>
                                <!--<th>Interest Rate</th>-->
                                <th>Lender</th>
                                <!--<th>Registered Lender</th>-->
                                <!--<th>Is the lender a family member?</th>-->
                                <th>Payment Terms</th>
                                <th>Payment Date</th>
                                <th>File</th>
                                <td></td>
                                {{-- <td></td> --}}
                                <td></td>
                              </tr>
                            </thead>
                            <tbody>
                              @foreach ($exps as $k => $e_exp)
                                @if ($k == 'loan')
                                  @foreach ($e_exp as $exp)
                                    <tr>
                                      <td>
                                        <a href="#"
                                          wire:click.prevent="showModal('loan', 'show', {{ $exp->id }})">
                                          {{ ucwords($exp->description) }}
                                        </a>
                                      </td>
                                      <td>{{ number_format($exp->amount, 2) }}
                                      </td>
                                      <td>{{ $exp->lender }}</td>
                                      <td>{{ $exp->term }}</td>
                                      <td>{{ $exp->payment_date }}</td>
                                      <td>
                                        @if ($exp->document)
                                          <a href="{{ url('storage/' . $exp->document) }}"
                                            target="_blank"><i
                                              class="bx bxs-file"></i></a>
                                        @endif
                                      </td>

                                      <td>
                                        {{-- <a href="#" --}}
                                        {{--   wire:click.prevent="removeExpense({{ $exp->id }})"> --}}
                                        {{--   <i class="bx bxs-trash"></i> --}}
                                        {{-- </a> --}}
                                        <div class="__actions __border">
                                          <a href="#"
                                            class="text-danger"
                                            wire:click.prevent="showModal('loan', 'edit', {{ $exp->id }})">
                                            <svg width="20" height="20"
                                              viewBox="0 0 20 20"
                                              fill="none"
                                              xmlns="http://www.w3.org/2000/svg">
                                              <path d="M11.7157 7.51667L12.4824 8.28333L4.93236
                    15.8333H4.16569V15.0667L11.7157 7.51667ZM14.7157 2.5C14.5074
                    2.5 14.2907 2.58333 14.1324 2.74167L12.6074 4.26667L15.7324
                    7.39167L17.2574 5.86667C17.5824 5.54167 17.5824 5.01667
                    17.2574 4.69167L15.3074 2.74167C15.1407 2.575 14.9324 2.5
                    14.7157 2.5ZM11.7157 5.15833L2.49902
                    14.375V17.5H5.62402L14.8407 8.28333L11.7157 5.15833Z"
                                                fill="#CF3D60" />
                                            </svg>
                                            Edit
                                          </a>
                                        </div>
                                      </td>

                                      {{-- <td> --}}
                                      {{-- <a href="#" --}}
                                      {{--   wire:click.prevent="showModal('loan', 'edit', {{ $exp->id }})"> --}}
                                      {{--   <i class="bx bxs-edit"></i> --}}
                                      {{-- </a> --}}
                                      {{-- </td> --}}

                                      <td>
                                        {{-- <a href="#" --}}
                                        {{--   wire:click.prevent="showModal('loan', 'show', {{ $exp->id }})"> --}}
                                        {{--   <i class="bx bxs-show"></i> --}}
                                        {{-- </a> --}}
                                        <div class="__actions">
                                          <a href="#"
                                            class="text-danger"
                                            wire:click.prevent="removeExpense({{ $exp->id }})">
                                            <svg width="20" height="20"
                                              viewBox="0 0 20 20"
                                              fill="none"
                                              xmlns="http://www.w3.org/2000/svg">
                                              <path d="M13.3334 7.5V15.8333H6.66675V7.5H13.3334ZM12.0834
                    2.5H7.91675L7.08341
                    3.33333H4.16675V5H15.8334V3.33333H12.9167L12.0834
                    2.5ZM15.0001 5.83333H5.00008V15.8333C5.00008 16.75 5.75008
                    17.5 6.66675 17.5H13.3334C14.2501 17.5 15.0001 16.75 15.0001
                    15.8333V5.83333Z" fill="#CF3D60" />
                                            </svg>
                                            Remove
                                          </a>
                                        </div>
                                      </td>

                                    </tr>
                                  @endforeach
                                @endif
                              @endforeach
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="accordion-item">
                  <h2 class="accordion-header" id="insurance-heading">
                    <button class="accordion-button collapsed" type="button"
                      data-bs-toggle="collapse" data-bs-target="#insurance"
                      aria-expanded="false" aria-controls="insurance">
                      Insurance
                    </button>
                  </h2>
                  <div id="insurance"
                    class="accordion-collapse __list collapse"
                    aria-labelledby="insurance-heading"
                    data-bs-parent="#accordion-property-wealth">
                    <div class="accordion-body">
                      <livewire:account.organiser.property-wealth.pages.insurance />
                    </div>
                  </div>
                </div>
                <div class="accordion-item">
                  <h2 class="accordion-header" id="heading-Ohter">
                    <button class="accordion-button collapsed" type="button"
                      data-bs-toggle="collapse" data-bs-target="#other"
                      aria-expanded="false" aria-controls="other">
                      Debt
                    </button>
                  </h2>
                  <div id="other"
                    class="accordion-collapse __list collapse"
                    aria-labelledby="other-heading"
                    data-bs-parent="#accordionExample">
                    <div class="accordion-body">
                      <div class="row">
                        <div class="col-md-12">
                          <div class="d-flex">
                            <h5></h5>
                            <span class="">
                              <a href="#" class="btn btn-outline-two"
                                wire:click.prevent="showModal('debt')">
                                <svg width="16" height="17"
                                  viewBox="0 0 16 17" fill="none"
                                  xmlns="http://www.w3.org/2000/svg">
                                  <path
                                    d="M12.6667
              9.16732H8.66671V13.1673H7.33337V9.16732H3.33337V7.83398H7.33337V3.83398H8.66671V7.83398H12.6667V9.16732Z"
                                    fill="#CF3D60" />
                                </svg>
                                Add Debt
                              </a>
                            </span>
                          </div>
                        </div>
                        <div class="col-md-12 overflow-auto">
                          <table class="table">
                            <thead>
                              <tr>
                                <th>Description</th>
                                <th class="text-end">Amount</th>
                                <th>Payment Date</th>
                                <th>Payment Term</th>
                                <th>File</th>
                                <th></th>
                                {{-- <th></th> --}}
                                <th></th>
                              </tr>
                            </thead>
                            <tbody>
                              @foreach ($exps as $k => $e_exp)
                                @if ($k == 'debt')
                                  @foreach ($e_exp as $exp)
                                    <tr>
                                      <td>
                                        <a href="#"
                                          wire:click.prevent="showModal('debt', 'show', {{ $exp->id }})">
                                          {{ ucwords($exp->description) }}
                                        </a>
                                      </td>
                                      <td class="text-end">
                                        {{ number_format($exp->amount, 2) }}
                                      </td>
                                      <td>{{ $exp->payment_date }}</td>
                                      <td>{{ $exp->term }}</td>
                                      <td>
                                        @if ($exp->document)
                                          <a href="{{ url('storage/' . $exp->document) }}"
                                            target="_blank"><i
                                              class="bx bxs-file"></i></a>
                                        @endif
                                      </td>

                                      <td>
                                        {{-- <a href="#" --}}
                                        {{--   wire:click.prevent="removeExpense({{ $exp->id }})"> --}}
                                        {{--   <i class="bx bxs-trash"></i> --}}
                                        {{-- </a> --}}
                                        <div class="__actions __border">
                                          <a href="#"
                                            class="text-danger"
                                            wire:click.prevent="showModal('debt', 'edit', {{ $exp->id }})">
                                            <svg width="20" height="20"
                                              viewBox="0 0 20 20"
                                              fill="none"
                                              xmlns="http://www.w3.org/2000/svg">
                                              <path d="M11.7157 7.51667L12.4824 8.28333L4.93236
                    15.8333H4.16569V15.0667L11.7157 7.51667ZM14.7157 2.5C14.5074
                    2.5 14.2907 2.58333 14.1324 2.74167L12.6074 4.26667L15.7324
                    7.39167L17.2574 5.86667C17.5824 5.54167 17.5824 5.01667
                    17.2574 4.69167L15.3074 2.74167C15.1407 2.575 14.9324 2.5
                    14.7157 2.5ZM11.7157 5.15833L2.49902
                    14.375V17.5H5.62402L14.8407 8.28333L11.7157 5.15833Z"
                                                fill="#CF3D60" />
                                            </svg>
                                            Edit
                                          </a>
                                        </div>
                                      </td>

                                      {{-- <td> --}}
                                      {{--   <a href="#" --}}
                                      {{--     wire:click.prevent="showModal('debt', 'edit', {{ $exp->id }})"> --}}
                                      {{--     <i class="bx bxs-edit"></i> --}}
                                      {{--   </a> --}}
                                      {{-- </td> --}}

                                      <td>
                                        {{-- <a href="#" --}}
                                        {{--   wire:click.prevent="showModal('debt', 'show', {{ $exp->id }})"> --}}
                                        {{--   <i class="bx bxs-show"></i> --}}
                                        {{-- </a> --}}
                                        <div class="__actions">
                                          <a href="#"
                                            class="text-danger"
                                            wire:click.prevent="removeExpense({{ $exp->id }})">
                                            <svg width="20" height="20"
                                              viewBox="0 0 20 20"
                                              fill="none"
                                              xmlns="http://www.w3.org/2000/svg">
                                              <path d="M13.3334 7.5V15.8333H6.66675V7.5H13.3334ZM12.0834
                    2.5H7.91675L7.08341
                    3.33333H4.16675V5H15.8334V3.33333H12.9167L12.0834
                    2.5ZM15.0001 5.83333H5.00008V15.8333C5.00008 16.75 5.75008
                    17.5 6.66675 17.5H13.3334C14.2501 17.5 15.0001 16.75 15.0001
                    15.8333V5.83333Z" fill="#CF3D60" />
                                            </svg>
                                            Remove
                                          </a>
                                        </div>

                                      </td>
                                    </tr>
                                  @endforeach
                                @endif
                              @endforeach
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal" tabindex="-1" id="frm-modal" wire:ignore.self>
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">{{ $title }}</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal"
            aria-label="Close" wire:click.prevent="closeModal"></button>
        </div>
        <div class="modal-body">
          @if ($errors->any())
            <div class="alert alert-danger">
              <span>{{ $errors->first() }}</span>
            </div>
          @endif
          <form>
            <div class="row">
              @if ($show_type)
                <div class="col-md-12">
                  <div class="mb-3">
                    <label class="form-label">Type</label>
                    <select class="form-control" id="type"
                      name="type" wire:model="type"
                      @if ($view == 'show') disabled @endif>
                      <option value="" selected disabled>Select Option
                      </option>
                      @foreach ($types as $tp => $v)
                        <option value="{{ $tp }}">
                          {{ $tp }}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
              @endif
              <div class="col-md-12">
                <div class="mb-3">
                  <label class="form-label">Description</label>
                  <input type="text" class="form-control"
                    placeholder="{{ $placeholder }}" name="description"
                    wire:model.defer="description"
                    @if ($view == 'show') disabled @endif>
                </div>
                <div class="mb-3">
                  <label class="form-label">Amount</label>
                  <div class="input-group mb-3">
                    <span class="input-group-text" id="basic-addon1">R</span>
                    <input type="text"
                      class="form-control number-separator" name="amount"
                      wire:model.defer="amount"
                      @if ($view == 'show') disabled @endif>
                  </div>
                </div>
                <div class="mb-3">
                  <label class="form-label">Interest Rate (If Any)</label>
                  <input type="text" class="form-control" name="rate"
                    wire:model.defer="rate"
                    @if ($view == 'show') disabled @endif>
                </div>
                <div class="mb-3">
                  <label class="form-label">Payment Terms</label>
                  <select class="form-control" id="term" name="term"
                    wire:model.defer="term"
                    @if ($view == 'show') disabled @endif>
                    <option value="" selected>Select Option</option>
                    @foreach ($terms as $tm)
                      <option value="{{ $tm }}">{{ $tm }}
                      </option>
                    @endforeach
                  </select>
                </div>
                @if ($type == 'loan')
                  <div class="mb-3" id="lender">
                    <label class="form-label">Full Name / Trading Name of the
                      Lender</label>
                    <input type="text" class="form-control" name="lender"
                      wire:model.defer="lender"
                      @if ($view == 'show') disabled @endif>
                    <div>
                      <label class="form-label mt-3">Is the lender registered
                        as a credit provider at the NCR?</label>
                      <select class="form-control" id="registered_lender"
                        name="registered_lender"
                        wire:model.defer="registered_lender"
                        @if ($view == 'show') disabled @endif>
                        <option value="" selected>Select Option</option>
                        <option value="yes">Yes</option>
                        <option value="no">No</option>
                      </select>
                    </div>
                    <div>
                      <label class="form-label mt-3">Is the lender a family
                        member?</label>
                      <select class="form-control" id="family_lender"
                        name="family_lender" wire:model.defer="family_lender"
                        @if ($view == 'show') disabled @endif>
                        <option value="" selected>Select Option</option>
                        <option value="yes">Yes</option>
                        <option value="no">No</option>
                      </select>
                    </div>
                  </div>
                @endif
                <div class="mb-3">
                  <label class="form-label">Payment Date</label>
                  <input type="text" class="form-control"
                    name="payment_date"
                    placeholder="(e.g 2nd of each month, end of December)"
                    wire:model.defer="payment_date"
                    @if ($view == 'show') disabled @endif>
                </div>
                @if ($document_file && $view == 'edit')
                  <div class="d-grid d-md-block mb-3 gap-2">
                    <a href="#" class="btn btn-outline-two"
                      wire:click.prevent="removeFile({{ $cur_id }})">Remove
                      file</a>
                  </div>
                @else
                  <div class="mb-3">
                    <label class="form-label">Document</label>
                    <input type="file" class="form-control"
                      name="document" wire:model="document"
                      @if ($view == 'show') disabled @endif>
                    @error('document')
                      <span class="error text-red">Failed to upload(File must not
                        exceed limit of 2MB and file type: pdf,
                        jpeg, png)</span></br>
                    @enderror
                    <span class="text-muted">File: Max File Size 2MB (pdf,
                      jpeg, png)</span>
                  </div>
                @endif
              </div>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-outline-two"
            wire:click.prevent="closeModal"
            data-bs-dismiss="modal">Close</button>
          @if ($view == 'edit' || is_null($view))
            <button type="button" class="btn btn-red"
              wire:click.prevent="saveExpense">Save</button>
          @endif
        </div>
      </div>
    </div>
  </div>


  @push('scripts')
    <script>
      window.addEventListener('show-insurance-form-modal', event => {
        $('#frm-modal').modal('show');
      });
      window.addEventListener('close-form-modal', event => {
        $('.modal').modal('hide');
      });

      window.addEventListener('reload-js', event => {
        easyNumberSeparator({
          selector: '.number-separator',
          separator: ','
        })
      });
    </script>

    <script>
      const selectField = document.getElementById('type');
      /*
      selectField.addEventListener('change', function() {
          const lenderField = document.getElementById('lender');
          lenderField.classList.toggle('hidden', this.value !== 'Loans');
      });
      */
    </script>
  @endpush
</div>
