@if (Auth::user()->free_account == 1)
  <div class="{{ Auth::user()->free_account == 1 ? '__full-height' : '' }}">
    <div
      style="display: grid; height: 100%; align-items: center; align-items: start;">
      <div class="__empty-state">
        <div>
          <a href="{{ url('register/upgrade/' . Auth::user()->id) }}">
            <svg width="35" height="34" viewBox="0 0 35 34" fill="none"
              xmlns="http://www.w3.org/2000/svg">
              <path d="M27.1667 11.0833H25.5833V7.91667C25.5833 3.54667 22.0367 0
            17.6667 0C13.2967 0 9.75 3.54667 9.75 7.91667V11.0833H8.16667C6.425
            11.0833 5 12.5083 5 14.25V30.0833C5 31.825 6.425 33.25 8.16667
            33.25H27.1667C28.9083 33.25 30.3333 31.825 30.3333
            30.0833V14.25C30.3333 12.5083 28.9083 11.0833 27.1667
            11.0833ZM12.9167 7.91667C12.9167 5.28833 15.0383 3.16667 17.6667
            3.16667C20.295 3.16667 22.4167 5.28833 22.4167
            7.91667V11.0833H12.9167V7.91667ZM27.1667
            30.0833H8.16667V14.25H27.1667V30.0833Z" fill="#CF3D60" />
            </svg>
          </a>
          <div>
            <a href="{{ url('register/upgrade/' . Auth::user()->id) }}">
              Upgrade to access  this feature
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
@endif

@if (Auth::user()->free_account == 0)
  <div class="">
    <div class="card-box-style">
      <div class="__heading">
        <h3>Funerals <i class="fa fa-info-circle" style="cursor: pointer;"
            data-bs-toggle="modal" data-bs-target="#page-description"></i>
        </h3>
        <p>Make provisions and ensure your final wishes are known when you are
          no
          longer able to share them.</p>
      </div>
      <div class="">
        <div class="">
          <div class="">
            <div class="d-flex">
              <span class="ms-auto">
                <!--<a href="#" class="btn btn-outline-two" wire:click.prevent="addMemory">Add Funerals</a>-->
              </span>
            </div>
            @if (session()->has('message'))
              <div class="alert alert-success">
                {{ session('message') }}
              </div>
            @endif

            <div class="row">
              <div class="col-md-12">
                <div class="accordion" id="accordion-funeral-policies">
                  <div class="accordion-item">
                    <h2 class="accordion-header" id="funeral-heading">
                      <button class="accordion-button collapsed" type="button"
                        data-bs-toggle="collapse" data-bs-target="#funeral"
                        aria-expanded="true" aria-controls="funeral">
                        Funeral Policies
                      </button>
                    </h2>
                    <div id="funeral"
                      class="accordion-collapse __list collapse"
                      aria-labelledby="funeral-heading"
                      data-bs-parent="#accordion-funeral-policies">
                      <div class="accordion-body">
                        <div class="row">
                          <div class="col-md-12">
                            <div class="d-flex">
                              <h5></h5>
                              <span class="">
                                <a href="#" class="btn btn-outline-two"
                                  wire:click.prevent="showModal">
                                  <svg width="16" height="17"
                                    viewBox="0 0 16 17" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path
                                      d="M12.6667
              9.16732H8.66671V13.1673H7.33337V9.16732H3.33337V7.83398H7.33337V3.83398H8.66671V7.83398H12.6667V9.16732Z"
                                      fill="#CF3D60" />
                                  </svg>
                                  Add Policy
                                </a>
                              </span>
                            </div>
                          </div>
                          <div class="col-md-12 overflow-auto">
                            <table class="table">
                              <thead>
                                <tr>
                                  <th>Name of Insurance</th>
                                  <th>Policy number</th>
                                  <th>Value</th>
                                  <th></th>
                                  {{-- <th></th> --}}
                                  <th></th>
                                </tr>
                              </thead>
                              <tbody>
                                @foreach ($funeral_policies as $policy)
                                  <tr>
                                    <td>
                                      <a href="#"
                                        wire:click.prevent="showModal('show', {{ $policy->id }})">
                                        {{ $policy->company }}
                                      </a>
                                    </td>
                                    <td>{{ $policy->policy_number }}</td>
                                    <td>{{ $policy->coverage_amount }}</td>

                                    <td>
                                      {{-- <a href="#" --}}
                                      {{--   wire:click.prevent="showModal('edit', {{ $policy->id }})"><i --}}
                                      {{--     class="bx bxs-edit"></i></a> --}}
                                      <div class="__actions __border">
                                        <a href="#" class="text-danger"
                                          wire:click.prevent="showModal('edit', {{ $policy->id }})">
                                          <svg width="20" height="20"
                                            viewBox="0 0 20 20" fill="none"
                                            xmlns="http://www.w3.org/2000/svg">
                                            <path d="M11.7157 7.51667L12.4824 8.28333L4.93236
                    15.8333H4.16569V15.0667L11.7157 7.51667ZM14.7157 2.5C14.5074
                    2.5 14.2907 2.58333 14.1324 2.74167L12.6074 4.26667L15.7324
                    7.39167L17.2574 5.86667C17.5824 5.54167 17.5824 5.01667
                    17.2574 4.69167L15.3074 2.74167C15.1407 2.575 14.9324 2.5
                    14.7157 2.5ZM11.7157 5.15833L2.49902
                    14.375V17.5H5.62402L14.8407 8.28333L11.7157 5.15833Z"
                                              fill="#CF3D60" />
                                          </svg>
                                          Edit
                                        </a>
                                      </div>
                                    </td>

                                    {{-- <td> --}}
                                    {{--   <a href="#" --}}
                                    {{--     wire:click.prevent="showModal('show', {{ $policy->id }})"><i --}}
                                    {{--       class="fa fa-eye"></i></a> --}}
                                    {{-- </td> --}}

                                    <td>
                                      {{-- <a href="#" --}}
                                      {{--   wire:click.prevent="deleteFuneralPolicy({{ $policy->id }})"><i --}}
                                      {{--     class="bx bxs-trash"></i></a> --}}
                                      <div class="__actions">
                                        <a href="#" class="text-danger"
                                          wire:click.prevent="deleteFuneralPolicy({{ $policy->id }})">
                                          <svg width="20" height="20"
                                            viewBox="0 0 20 20" fill="none"
                                            xmlns="http://www.w3.org/2000/svg">
                                            <path d="M13.3334 7.5V15.8333H6.66675V7.5H13.3334ZM12.0834
                    2.5H7.91675L7.08341
                    3.33333H4.16675V5H15.8334V3.33333H12.9167L12.0834
                    2.5ZM15.0001 5.83333H5.00008V15.8333C5.00008 16.75 5.75008
                    17.5 6.66675 17.5H13.3334C14.2501 17.5 15.0001 16.75 15.0001
                    15.8333V5.83333Z" fill="#CF3D60" />
                                          </svg>
                                          Remove
                                        </a>
                                      </div>
                                    </td>
                                  </tr>
                                @endforeach
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="accordion-item">
                    <h2 class="accordion-header" id="funeralDetails">
                      <button class="accordion-button collapsed" type="button"
                        data-bs-toggle="collapse"
                        data-bs-target="#funeral-details" aria-expanded="false"
                        aria-controls="funeralDetails">
                        Funeral Details
                      </button>
                    </h2>
                    <div id="funeral-details"
                      class="accordion-collapse collapse"
                      aria-labelledby="funeralDetails"
                      data-bs-parent="#accordion-funeral-policies">
                      <div class="accordion-body">
                        <div class="">
                          <form wire:submit.prevent="savefuneralDetails"
                            enctype="multipart/form-data">
                            <div class="f-w-input mb-3">
                              <label class="form-label">Memorial
                                Service</label>
                              <input type="text" class="btm-border-input"
                                wire:model.defer="memorials_service">
                            </div>
                            <div class="f-w-input mb-3">
                              <label class="form-label">Cremation
                                Details</label>
                              <input type="text" class="btm-border-input"
                                wire:model.defer="cremation_details">
                            </div>
                            <div class="f-w-input mb-3">
                              <label class="form-label">Funeral Parlor
                                Details</label>
                              <input type="text" class="btm-border-input"
                                wire:model.defer="parlor_details">
                            </div>
                            <div class="f-w-input mb-3">
                              <label class="form-label">Specific
                                Requests</label>
                              <input type="text" class="btm-border-input"
                                wire:model.defer="requests">
                            </div>
                            <div class="f-w-input mb-3">
                              <label class="form-label">Write Your Own
                                Obituary</label>
                              <textarea class="btm-border-input w-100" wire:model.defer="obituary"
                                rows="5"></textarea>
                            </div>
                            <div class="d-grid justify-content-end mb-3">
                              <input class="btn btn-red" type="submit"
                                value="save">
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endif

<div class="modal" tabindex="-1" id="page-description">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-body">
        <div class="d-flex">
          <h5>Funeral Wishes</h5>
          <span class="ms-auto">
            <button type="button" class="btn-close" data-bs-dismiss="modal"
              aria-label="Close"></button>
          </span>
        </div>
        <div class="mb-5">
          <p>Funeral Wishes is where you can provide detailed instructions
            about what should happen after your
            passing, such as your funeral. arrangements and more. This
            information will be accessible to your executor
            following your passing.</p>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal" tabindex="-1" id="funeral-policy-frm-modal"
  wire:ignore.self>
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        @if ($view == 'show')
          <h5 class="modal-title">Policy Details</h5>
        @elseif($view == 'edit')
          <h5 class="modal-title">Edit Policy</h5>
        @else
          <h5 class="modal-title">Add Policy</h5>
        @endif
        <button type="button" class="btn-close" data-bs-dismiss="modal"
          wire:click.prevent="closeModal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        @if ($errors->any())
          <div class="alert alert-danger">
            <span>{{ $errors->first() }}</span>
          </div>
        @endif
        <form>
          <div class="row">
            <div class="col-md-12 mb-3">
              <div class="d-flex mb-3">
                <h5>Funeral Policy</h5>
              </div>
            </div>
            <div class="col-md-6 col-lg-6">
              <div class="mb-3">
                <label class="form-label">Company</label>
                <input type="text" class="form-control" name="company"
                  wire:model.defer="company"
                  @if ($view == 'show') disabled @endif>
              </div>
            </div>
            <div class="col-md-6 col-lg-6">
              <div class="mb-3">
                <label class="form-label">Policy Number</label>
                <input type="text" class="form-control"
                  name="policy_number" wire:model.defer="policy_number"
                  @if ($view == 'show') disabled @endif>
              </div>
            </div>
            <div class="col-md-6 col-lg-12">
              <div class="mb-3">
                <label class="form-label">Coverage Amount</label>
                <input type="text" class="form-control number-separator"
                  name="coverage_amount" wire:model.defer="coverage_amount"
                  @if ($view == 'show') disabled @endif>
              </div>
            </div>
          </div>
          <div class="row mb-3">
            <div class="col-md-12">
              <div class="d-flex">
                <h6>Nominated Beneficiaries</h6>
                @if ($view != 'show')
                  <span class="ms-auto">
                    <a href="#" class="btn btn-outline-two"
                      wire:click.prevent="addDependent">Add
                      Beneficiary</a>
                  </span>
                @endif
              </div>
            </div>
          </div>
          <div class="row">
            @foreach ($dependents as $dp => $dependent)
              @if (!isset($dependent['id']))
                <div class="mb-3">
                  <label class="form-label">Select Beneficiary</label>
                  <select class="form-control"
                    name="dept_{{ $dp }}"
                    wire:key="sel_{{ $dp }}"
                    wire:model="dependents.{{ $dp }}.key">
                    @foreach ($cur_dependants as $key => $cur_dependant)
                      @if ($cur_dependant['type'] == 'other')
                        <option value="{{ $cur_dependant['key'] }}"
                          wire:key="{{ $dp }}_{{ $key }}">
                          Other</option>
                      @else
                        <option value="{{ $cur_dependant['key'] }}"
                          wire:key="{{ $dp }}_{{ $key }}">
                          {{ $cur_dependant['name'] . ' ' . $cur_dependant['surname'] }}
                        </option>
                      @endif
                    @endforeach
                  </select>
                </div>
              @endif
              @if ($dependent['type'] == 'other')
                <div class="col-md-4">
                  <div class="f-w-input mb-3">
                    <label class="form-label">Name</label>
                    <input type="text" class="form-control"
                      wire:model.defer="dependents.{{ $dp }}.name"
                      @if ($view == 'show') disabled @endif>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="f-w-input mb-3">
                    <label class="form-label">Surname</label>
                    <input type="text" class="form-control"
                      wire:model.defer="dependents.{{ $dp }}.surname"
                      @if ($view == 'show') disabled @endif>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="f-w-input mb-3">
                    <label class="form-label">ID Number</label>
                    <input type="text" class="form-control"
                      wire:model.defer="dependents.{{ $dp }}.id_number"
                      @if ($view == 'show') disabled @endif>
                  </div>
                </div>
              @endif
              <div class="col-md-12">
                <div class="mb-3">
                  <label class="form-label">Percentage to Recieve</label>
                  <div class="input-group">
                    <input type="text" class="form-control"
                      name="percentage"
                      wire:model.defer="dependents.{{ $dp }}.percentage"
                      @if ($view == 'show') disabled @endif>
                    <span class="input-group-text">%</span>
                  </div>
                </div>
              </div>
              <div class="col-md-12">
                @if ($view == 'edit' || is_null($view))
                  <div class="mb-3">
                    @if (isset($dependent['id']))
                      <a href="#" class="btn btn-outline-two"
                        wire:click.prevent="removeDependent({{ $dependent['id'] }})">Remove</a>
                    @endif
                  </div>
                @endif
                <hr />
              </div>
            @endforeach
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-two"
          wire:click.prevent="closeModal" data-bs-dismiss="modal">
          @if ($view == 'show')
            Close
          @else
            Cancel
          @endif
        </button>
        @if ($view == 'edit' || is_null($view))
          <button type="button" class="btn btn-red"
            wire:click.prevent="saveFuneral">Save</button>
        @endif
      </div>
    </div>
  </div>
</div>
@push('scripts')
  <script>
    window.addEventListener('show-policy-form-modal', event => {
      $('#funeral-policy-frm-modal').modal('show');
    });

    window.addEventListener('close-policy-form-modal', event => {
      $('.modal').modal('hide');
    });

    window.addEventListener('reload-js', event => {
      easyNumberSeparator({
        selector: '.number-separator',
        separator: ','
      })
    });
  </script>
@endpush
</div>
