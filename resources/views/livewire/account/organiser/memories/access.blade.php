<div class="mt-5">
  <div class="d-flex">
    <div class="organiser-description-large">
      <h3>Who Must See This</h3>
    </div>
    <div class="ms-auto">
      <a href="#" class="btn btn-outline-two" wire:click.prevent="addContact">Add More</a>
    </div>
  </div>
  @if (session()->has('message'))
    <div class="alert alert-success">
      {{ session('message') }}
    </div>
  @endif
  <form wire:submit.prevent="saveContacts">
    @foreach ($contacts as $k => $cnt)
      <div class="row">
        <div class="col-md-6">
          <div class="mb-3">
            <label class="form-label">Name</label>
            <input type="text" class="form-control" name="name"
              wire:model.defer="contacts.{{ $k }}.name">
          </div>
        </div>
        <div class="col-md-6">
          <div class="mb-3">
            <label class="form-label">Surname</label>
            <input type="text" class="form-control" name="surname"
              wire:model.defer="contacts.{{ $k }}.surname">
          </div>
        </div>
        <div class="col-md-4">
          <div class="mb-3">
            <label class="form-label">Email</label>
            <input type="email" class="form-control" name="email"
              wire:model.defer="contacts.{{ $k }}.email">
          </div>
        </div>
        <div class="col-md-4">
          <div class="mb-3">
            <label class="form-label">Phone Number</label>
            <input type="text" class="form-control" name="phone_number"
              wire:model.defer="contacts.{{ $k }}.phone_number">
          </div>
        </div>
        <div class="col-md-4">
          <div class="mb-3">
            <label class="form-label">ID Number</label>
            <input type="text" class="form-control" name="id_number"
              wire:model.defer="contacts.{{ $k }}.id_number">
          </div>
        </div>
        <div class="col-md-12">
          <div class="mb-3">
            <label class="form-label">Message</label>
            <textarea class="form-control" name="message" wire:model.defer="contacts.{{ $k }}.message"></textarea>
          </div>
        </div>
        <div class="col-md-12">
          @if (isset($cnt['id']))
            <div class="">
              <a href="#" class="btn btn-outline-two"
                wire:click.prevent="removeContact({{ $cnt['id'] }})">Remove</a>
            </div>
          @endif
          <hr />
        </div>
      </div>
    @endforeach
    <input type="submit" class="btn btn-red" value="save">
  </form>
</div>

