@if (Auth::user()->free_account == 1)
  <div class="{{ Auth::user()->free_account == 1 ? '__full-height' : '' }}">
    <div
      style="display: grid; height: 100%; align-items: center; align-items: start;">
      <div class="__empty-state">
        <div>
          <a href="{{ url('register/upgrade/' . Auth::user()->id) }}">
            <svg width="35" height="34" viewBox="0 0 35 34" fill="none"
              xmlns="http://www.w3.org/2000/svg">
              <path d="M27.1667 11.0833H25.5833V7.91667C25.5833 3.54667 22.0367 0
            17.6667 0C13.2967 0 9.75 3.54667 9.75 7.91667V11.0833H8.16667C6.425
            11.0833 5 12.5083 5 14.25V30.0833C5 31.825 6.425 33.25 8.16667
            33.25H27.1667C28.9083 33.25 30.3333 31.825 30.3333
            30.0833V14.25C30.3333 12.5083 28.9083 11.0833 27.1667
            11.0833ZM12.9167 7.91667C12.9167 5.28833 15.0383 3.16667 17.6667
            3.16667C20.295 3.16667 22.4167 5.28833 22.4167
            7.91667V11.0833H12.9167V7.91667ZM27.1667
            30.0833H8.16667V14.25H27.1667V30.0833Z" fill="#CF3D60" />
            </svg>
          </a>
          <div>
            <a href="{{ url('register/upgrade/' . Auth::user()->id) }}">
              Upgrade to access  this feature
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
@endif

@if (Auth::user()->free_account == 0)
  <div
    class="myallMemories {{ Auth::user()->free_account == 1 ? '__full-height' : '' }}">
    <div class="__heading">
      <h3>Memories <i class="fa fa-info-circle" style="cursor: pointer;"
          data-bs-toggle="modal" data-bs-target="#page-description"></i></h3>
      <p>You've built up a lifetime of memories, now keep them safe and
        together for your loved ones to see when
        you are no longer with them. Add photos, video clips, documents
        and more.</p>
    </div>
    <div class="__island">
      <div class="">
        <div class="">
          <div class="d-flex heading-btn-display">
          </div>
          <div class="mb-3">
            <b>
            </b>
          </div>

          @if ($errors->any())
            <div class="alert alert-danger">
              <span>{{ $errors->first() }}</span>
            </div>
          @endif
          @if (session()->has('message'))
            <div class="alert alert-success">
              {{ session('message') }}
            </div>
          @endif
          <form wire:submit.prevent="saveMemory">
            <div class="row">
              <div class="col-md-12">
                @foreach ($memories as $k => $mem)
                  <div class="row __item">
                    <div class="col-md-12">
                      <div
                        class="col-md-12 d-grid justify-content-end mb-4 gap-2 text-end">
                        @if (isset($mem['id']))
                          <a href="#" class="btn btn-remove"
                            wire:click.prevent="removeMemory({{ $mem['id'] }})">
                            <svg width="16" height="17"
                              viewBox="0 0 16 17" fill="none"
                              xmlns="http://www.w3.org/2000/svg">
                              <path d="M10.6666
                              6.5V13.1667H5.33325V6.5H10.6666ZM9.66659
                              2.5H6.33325L5.66659
                              3.16667H3.33325V4.5H12.6666V3.16667H10.3333L9.66659
                              2.5ZM11.9999 5.16667H3.99992V13.1667C3.99992 13.9
                              4.59992 14.5 5.33325 14.5H10.6666C11.3999 14.5
                              11.9999 13.9 11.9999 13.1667V5.16667Z"
                                fill="#CF3D60" />
                            </svg>
                            Remove
                          </a>
                        @endif
                      </div>
                      <div class="mb-3">
                        <label class="form-label">Type of Memory</label>
                        <select class="form-control"
                          wire:model="memories.{{ $k }}.media_type">
                          <option value="">Select Option</option>
                          @foreach ($media_types as $tp)
                            <option value="{{ $tp }}">
                              {{ $tp }}</option>
                          @endforeach
                        </select>
                      </div>
                    </div>
                    <div class="col-md-12">
                      @if (isset($mem['id']) && isset($mem['file_url']))
                        <div class="d-grid d-md-block mb-3 mt-1 gap-2">
                          <a href="{{ url('storage/' . $mem['file_url']) }}"
                            target="_blank"
                            class="btn btn-outline-one text-red">View File</a>
                          <a href="#"
                            class="btn btn-outline-one text-red ms-md-2 ms-0"
                            wire:click.prevent="removeDocument({{ $mem['id'] }})">Remove
                            File</a>
                        </div>
                      @else
                        <div class="mb-3">
                          @if ($memories[$k]['media_type'] == 'Link')
                            <label class="form-label">Link</label>
                            <input type="text" class="form-control"
                              wire:model="memories.{{ $k }}.link_url">
                          @elseif(
                              $memories[$k]['media_type'] == 'Video Clip' ||
                                  $memories[$k]['media_type'] == 'Song' ||
                                  $memories[$k]['media_type'] == 'Image' ||
                                  $memories[$k]['media_type'] == 'Image' ||
                                  $memories[$k]['media_type'] == 'Document')
                            <div class="f-w-input">
                              <label class="form-label">Media</label>
                              <input type="file"
                                class="btm-border-input file-input-rad"
                                wire:model="memories.{{ $k }}.media">
                              @if ($memories[$k]['media_type'] == 'Document')
                                <span class="text-muted">Document: PDF Max File
                                  Size 2MB</span>
                              @elseif($memories[$k]['media_type'] == 'Video Clip')
                                <span class="text-muted">Video: Max File Size
                                  5MB (mp4, mp3, wav, avi)</span>
                              @elseif($memories[$k]['media_type'] == 'Song')
                                <span class="text-muted"> Song: Max File Size
                                  5MB (mp4, mp3, wav, avi)</span>
                              @elseif($memories[$k]['media_type'] == 'Image')
                                <span class="text-muted">Image: Max File Size
                                  1MB (jpeg, png)</span>
                              @endif
                            </div>
                          @endif
                        </div>
                      @endif
                    </div>
                    @if ($memories[$k]['media_type'] == 'Questionnaire')
                      <div class="col-md-12">
                        <div class="">
                          {{-- <label class="form-label">Click below to fill in the --}}
                          {{--   questionnaire</label> --}}
                        </div>
                      </div>
                      <div class="">
                        <div class="accordion mb-3" id="accordion-memories">
                          <div class="accordion-item">
                            <h2 class="accordion-header"
                              id="questionnaire-heading">
                              <button class="accordion-button" type="button"
                                data-bs-toggle="collapse"
                                @if (isset($mem['id'])) data-bs-target="#questionnaire{{ $mem['id'] }}" @else data-bs-target="#questionnaire" @endif
                                aria-expanded="true"
                                aria-controls="questionnaire">
                                Start Questionnaire
                              </button>
                            </h2>
                            <div
                              @if (isset($mem['id'])) id="questionnaire{{ $mem['id'] }}" @else id="questionnaire" @endif
                              class="accordion-collapse show"
                              aria-labelledby="questionnaire-heading"
                              data-bs-parent="#accordion-property-wealth">
                              <div class="accordion-body">
                                <div class="others-title d-flex">
                                  <div class="organiser-description-large">
                                    <p>There are also some questions available
                                      here for you to answer for your
                                      family/friends to see once you die.</p>
                                  </div>
                                </div>
                                <div class="row d-flex justify-content-center">
                                  @foreach ($mem['questionnaires'] as $kk => $quest)
                                    <div class="col-md-6">
                                      <div class="mb-3">
                                        <label class="form-label">What is your
                                          favorite song?</label>
                                        <input type="text"
                                          class="form-control questionnaire-input"
                                          wire:model.defer="memories.{{ $k }}.questionnaires.{{ $kk }}.favorite_song">
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="mb-3">
                                        <label class="form-label">What is a
                                          movie you enjoyed?</label>
                                        <input type="text"
                                          class="form-control questionnaire-input"
                                          wire:model.defer="memories.{{ $k }}.questionnaires.{{ $kk }}.favorite_movie">
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="mb-3">
                                        <label class="form-label">Your favorite
                                          place visited and why?</label>
                                        <input type="text"
                                          class="form-control questionnaire-input"
                                          wire:model.defer="memories.{{ $k }}.questionnaires.{{ $kk }}.favorite_place">
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="mb-3">
                                        <label class="form-label">What would
                                          your last meal request be?</label>
                                        <input type="text"
                                          class="form-control questionnaire-input"
                                          wire:model.defer="memories.{{ $k }}.questionnaires.{{ $kk }}.favorite_food">
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="mb-3">
                                        <label class="form-label">Can you
                                          recall
                                          a special memory?</label>
                                        <input type="text"
                                          class="form-control questionnaire-input"
                                          wire:model.defer="memories.{{ $k }}.questionnaires.{{ $kk }}.special_memory">
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="mb-3">
                                        <label class="form-label">What is the
                                          biggest lesson you've learned in
                                          life?</label>
                                        <input type="text"
                                          class="form-control questionnaire-input"
                                          wire:model.defer="memories.{{ $k }}.questionnaires.{{ $kk }}.biggest_lesson">
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="mb-3">
                                        <label class="form-label">What are your
                                          religious beliefs?</label>
                                        <input type="text"
                                          class="form-control questionnaire-input"
                                          wire:model.defer="memories.{{ $k }}.questionnaires.{{ $kk }}.religion_beliefs">
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="mb-3">
                                        <label class="form-label">What you wish
                                          you knew earlier?</label>
                                        <input type="text"
                                          class="form-control questionnaire-input"
                                          wire:model.defer="memories.{{ $k }}.questionnaires.{{ $kk }}.knew_earlier">
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="mb-3">
                                        <label class="form-label">What are you
                                          most proud of?</label>
                                        <input type="text"
                                          class="form-control questionnaire-input"
                                          wire:model.defer="memories.{{ $k }}.questionnaires.{{ $kk }}.proud_of">
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="mb-3">
                                        <label class="form-label">How would you
                                          like to be remembered?</label>
                                        <input type="text"
                                          class="form-control questionnaire-input"
                                          wire:model.defer="memories.{{ $k }}.questionnaires.{{ $kk }}.remember_me_by">
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="mb-3">
                                        <label class="form-label">What is the
                                          best part of your life?</label>
                                        <input type="text"
                                          class="form-control questionnaire-input"
                                          wire:model.defer="memories.{{ $k }}.questionnaires.{{ $kk }}.best_part">
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="mb-3">
                                        <label class="form-label">Is there
                                          someone you would like to
                                          thank?</label>
                                        <input type="text"
                                          class="form-control questionnaire-input"
                                          wire:model.defer="memories.{{ $k }}.questionnaires.{{ $kk }}.thank_someone">
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="mb-3">
                                        <label class="form-label">What has made
                                          you very happy?</label>
                                        <input type="text"
                                          class="form-control questionnaire-input"
                                          wire:model.defer="memories.{{ $k }}.questionnaires.{{ $kk }}.happy_thing">
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="mb-3">
                                        <label class="form-label">What has made
                                          you very sad?</label>
                                        <input type="text"
                                          class="form-control questionnaire-input"
                                          wire:model.defer="memories.{{ $k }}.questionnaires.{{ $kk }}.sad_thing">
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="mb-3">
                                        <label class="form-label">What is your
                                          best quality?</label>
                                        <input type="text"
                                          class="form-control questionnaire-input"
                                          wire:model.defer="memories.{{ $k }}.questionnaires.{{ $kk }}.best_quality">
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="mb-3">
                                        <label class="form-label">Can you share
                                          a funny memory?</label>
                                        <input type="text"
                                          class="form-control questionnaire-input"
                                          wire:model.defer="memories.{{ $k }}.questionnaires.{{ $kk }}.funny_memory">
                                      </div>
                                    </div>
                                    <div class="col-md-12">
                                      <div class="mb-3">
                                        <label class="form-label">If you died
                                          today, what would you regret not
                                          doing?</label>
                                        <input type="text"
                                          class="form-control questionnaire-input"
                                          wire:model.defer="memories.{{ $k }}.questionnaires.{{ $kk }}.regret">
                                      </div>
                                    </div>
                                    <div class="col-md-12">
                                      <div class="mb-3">
                                        <label class="form-label">What advice
                                          have you received that has made the
                                          greatest impact on your life? And from
                                          whom?</label>
                                        <input type="text"
                                          class="form-control questionnaire-input"
                                          wire:model.defer="memories.{{ $k }}.questionnaires.{{ $kk }}.advice">
                                      </div>
                                    </div>
                                    <div class="col-md-12">
                                      <div class="mb-3">
                                        <label class="form-label">What is on
                                          your bucket list?</label>
                                        <textarea type="text"
                                          class="form-control questionnaire-input area-min-height"
                                          wire:model.defer="memories.{{ $k }}.questionnaires.{{ $kk }}.bucket_list"></textarea>
                                      </div>
                                    </div>
                                  @endforeach
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    @endif
                    <div class="col-md-12">
                      <div cklass="row">
                        <div class="col-md-12">
                          <b>Who Must See This</b>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-4">
                          <div class="mb-3">
                            <label class="form-label">Name</label>
                            <input type="text" class="form-control"
                              wire:model.defer="memories.{{ $k }}.name">
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="mb-3">
                            <label class="form-label">Surname</label>
                            <input type="text" class="form-control"
                              wire:model.defer="memories.{{ $k }}.surname">
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="mb-3">
                            <label class="form-label">Email</label>
                            <input type="text" class="form-control"
                              wire:model.defer="memories.{{ $k }}.email">
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="mb-3">
                            <label class="form-label">Phone Number</label>
                            <input type="text" class="form-control"
                              wire:model.defer="memories.{{ $k }}.phone_number"
                              maxlength="13">
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="mb-3">
                            <label class="form-label">ID Number</label>
                            <input type="text" class="form-control"
                              wire:model.defer="memories.{{ $k }}.id_number"
                              maxlength="13">
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="mb-3">
                        <label class="form-label">Notes / Description</label>
                        <textarea class="form-control area-min-height"
                          wire:model.defer="memories.{{ $k }}.notes"></textarea>
                      </div>
                    </div>
                  </div>
                @endforeach
                <div
                  class="col-md-12 d-grid justify-content-end gap-2 text-end">
                  {{-- <!--<span class="add-business-btn add-btn-alignment">--> --}}
                  <a href="#" class="btn btn-outline-two"
                    wire:click.prevent="addMemory">
                    <svg width="16" height="17" viewBox="0 0 16 17"
                      fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path
                        d="M12.6666
                      9.16536H8.66659V13.1654H7.33325V9.16536H3.33325V7.83203H7.33325V3.83203H8.66659V7.83203H12.6666V9.16536Z"
                        fill="#CF3D60" />
                    </svg>
                    Add Memory
                  </a>
                  {{-- <!--</span>--> --}}
                </div>
                <div class="col-md-12">
                  <hr>
                </div>
                <div class="row">
                  <div
                    class="col-md-12 justify-content-end upload-view-btns d-grid mb-4 gap-2">
                    <input type="submit" class="btn btn-red" value="save">
                  </div>
                </div>
              </div>
            </div>
          </form>

          {{--
                    <div class="">
                        <livewire:account.organiser.memories.questionnaire />
                    </div>

                    <div class="">
                        <livewire:account.organiser.memories.access />
                    </div>
                    --}}
        </div>
      </div>
    </div>
@endif

<div class="modal" tabindex="-1" id="init-pop-up">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <div class="d-flex">
          <h5>Memories</h5>
          <span class="ms-auto">
            <button type="button" class="btn-close" data-bs-dismiss="modal"
              aria-label="Close"></button>
          </span>
        </div>
        <div class="">
          <p>Here you can choose to leave special messages, video clips or
            photos for people to see once you die.</p>
          <p>Here are some reasons why people would choose to leave a special
            message, video clip, photo or song for
            someone to get once you die:
          <ul>
            <li>You want someone to remember something</li>
            <li>An important message, confession or secret you want to tell
              them</li>
            <li>A video for unborn children or small children to watch when
              they are older</li>
            <li>To give thanks or to apologise</li>
            <li>It might help when planning your funeral</li>
            <li>It might help to remember certain things about you</li>
          </ul>
          </p>
        </div>
        <div class="mt-5 text-end">
          <a href="#" class="btn btn-outline-two"
            wire:click.prevent="initModalSeen">Continue</a>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal" tabindex="-1" id="page-description">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-body">
        <div class="d-flex">
          <h5>Memories</h5>
          <span class="ms-auto">
            <button type="button" class="btn-close" data-bs-dismiss="modal"
              aria-label="Close"></button>
          </span>
        </div>
        <div class="mb-5">
          <p>This information will be accessible to your executor following
            your passing.</p>
        </div>
      </div>
    </div>
  </div>
</div>

@push('scripts')
  <script>
    $(document).ready(function() {
      var show_init_pop = '{{ $show_init_pop_up }}';
      if (show_init_pop) {
        $('#init-pop-up').modal('show');
      }
      window.addEventListener('close-modals', event => {
        $('.modal').each(function() {
          $(this).modal('hide');
        })
      });

    });
  </script>
@endpush
</div>
