<div class="mt-5">
  <div class="d-flex">
    <div class="organiser-description-large">
      <h3>Questionnaire</h3>
      <p>There are also some questions available here for you to answer for your family/friends to see once you die.</p>
    </div>
  </div>
  @if (session()->has('message'))
    <div class="alert alert-success">
      {{ session('message') }}
    </div>
  @endif
  <form wire:submit.prevent="saveMemoryQuestionniare">
    <div class="row">
      <div class="col-md-4">
        <div class="mb-3">
          <label class="form-label">Favorite Song</label>
          <input type="text" class="form-control" name="favorite_song" wire:model.defer="favorite_song">
        </div>
      </div>
      <div class="col-md-4">
        <div class="mb-3">
          <label class="form-label">Favorite Movie</label>
          <input type="text" class="form-control" name="favorite_movie" wire:model.defer="favorite_movie">
        </div>
      </div>
      <div class="col-md-4">
        <div class="mb-3">
          <label class="form-label">Favorite Place visited and why</label>
          <input type="text" class="form-control" name="favorite_place" wire:model.defer="favorite_place">
        </div>
      </div>
      <div class="col-md-4">
        <div class="mb-3">
          <label class="form-label">Favorite food</label>
          <input type="text" class="form-control" name="favorite_food" wire:model.defer="favorite_food">
        </div>
      </div>
      <div class="col-md-4">
        <div class="mb-3">
          <label class="form-label">Special memory that comes to mind</label>
          <input type="text" class="form-control" name="special_memory" wire:model.defer="special_memory">
        </div>
      </div>
      <div class="col-md-4">
        <div class="mb-3">
          <label class="form-label">Your biggest lesson in life</label>
          <input type="text" class="form-control" name="biggest_lesson" wire:model.defer="biggest_lesson">
        </div>
      </div>
      <div class="col-md-4">
        <div class="mb-3">
          <label class="form-label">A regret</label>
          <input type="text" class="form-control" name="regret" wire:model.defer="regret">
        </div>
      </div>
      <div class="col-md-4">
        <div class="mb-3">
          <label class="form-label">You will miss the most</label>
          <input type="text" class="form-control" name="miss_most" wire:model.defer="miss_most">
        </div>
      </div>
      <div class="col-md-4">
        <div class="mb-3">
          <label class="form-label">Best part of life</label>
          <input type="text" class="form-control" name="life_best_part" wire:model.defer="life_best_part">
        </div>
      </div>
      <div class="col-md-4">
        <div class="mb-3">
          <label class="form-label">Worst part of life</label>
          <input type="text" class="form-control" name="life_worst_part" wire:model.defer="life_worst_part">
        </div>
      </div>
      <div class="col-md-4">
        <div class="mb-3">
          <label class="form-label">Religion / Beliefs</label>
          <input type="text" class="form-control" name="religion_beliefs" wire:model.defer="religion_beliefs">
        </div>
      </div>
      <div class="col-md-4">
        <div class="mb-3">
          <label class="form-label">Funeral wishes - anything specific</label>
          <input type="text" class="form-control" name="funeral_wish" wire:model.defer="funeral_wish">
        </div>
      </div>
      <div class="col-md-4">
        <div class="mb-3">
          <label class="form-label">What you din't know</label>
          <input type="text" class="form-control" name="dint_know" wire:model.defer="dint_know">
        </div>
      </div>
      <div class="col-md-4">
        <div class="mb-3">
          <label class="form-label">What you hope for someone</label>
          <input type="text" class="form-control" name="hopes_for_someone" wire:model.defer="hopes_for_someone">
        </div>
      </div>
      <div class="col-md-4">
        <div class="mb-3">
          <label class="form-label">This made you very happy</label>
          <input type="text" class="form-control" name="happy_thing" wire:model.defer="happy_thing">
        </div>
      </div>
      <div class="col-md-6">
        <div class="mb-3">
          <label class="form-label">This made you very sad</label>
          <input type="text" class="form-control" name="sad_thing" wire:model.defer="sad_thing">
        </div>
      </div>
      <div class="col-md-6">
        <div class="mb-3">
          <label class="form-label">Your best quality</label>
          <input type="text" class="form-control" name="best_quality" wire:model.defer="best_quality">
        </div>
      </div>
      <div class="col-md-6">
        <div class="mb-3">
          <label class="form-label">What must people remember about you?</label>
          <input type="text" class="form-control" name="remember_me_by" wire:model.defer="remember_me_by">
        </div>
      </div>
      <div class="col-md-6">
        <div class="mb-3">
          <label class="form-label">A funny memory</label>
          <input type="text" class="form-control" name="funny_memory" wire:model.defer="funny_memory">
        </div>
      </div>
      <div class="col-md-12">
        <div class="mb-3">
          <label class="form-label">Bucket list</label>
          <textarea type="text" class="form-control" name="bucket_list" wire:model.defer="bucket_list"></textarea>
        </div>
      </div>
      <div class="col-md-12">
        <input type="submit" class="btn btn-red" value="save">
      </div>
    </div>
  </form>
</div>
