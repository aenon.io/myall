<div
  class="myallWill {{ Auth::user()->free_account == 1 ? '__full-height' : '' }}">

  @if (Auth::user()->free_account == 1)
    <div
      style="display: grid; height: 100%; align-items: center; align-items: start;">
      <div class="__empty-state">
        <div>
          <a href="{{ url('register/upgrade/' . Auth::user()->id) }}">
            <svg width="35" height="34" viewBox="0 0 35 34" fill="none"
              xmlns="http://www.w3.org/2000/svg">
              <path d="M27.1667 11.0833H25.5833V7.91667C25.5833 3.54667 22.0367 0
            17.6667 0C13.2967 0 9.75 3.54667 9.75 7.91667V11.0833H8.16667C6.425
            11.0833 5 12.5083 5 14.25V30.0833C5 31.825 6.425 33.25 8.16667
            33.25H27.1667C28.9083 33.25 30.3333 31.825 30.3333
            30.0833V14.25C30.3333 12.5083 28.9083 11.0833 27.1667
            11.0833ZM12.9167 7.91667C12.9167 5.28833 15.0383 3.16667 17.6667
            3.16667C20.295 3.16667 22.4167 5.28833 22.4167
            7.91667V11.0833H12.9167V7.91667ZM27.1667
            30.0833H8.16667V14.25H27.1667V30.0833Z" fill="#CF3D60" />
            </svg>
          </a>
          <div>
            <a href="{{ url('register/upgrade/' . Auth::user()->id) }}">
              Upgrade to access  this feature
            </a>
          </div>
        </div>
      </div>
    </div>
  @endif

  @if (Auth::user()->free_account == 0)
    @if ($will_view == 'start')
      @include('livewire.account.organiser.online-will.pages.start')
    @elseif($will_view == 'interview')
      <div class="myallWillPage">
        <livewire:account.organiser.online-will.pages.interview />
      </div>
    @elseif($will_view == 'stage-2')
      <div class="myallWillPage">
        <livewire:account.organiser.online-will.pages.stage2 />
      </div>
    @elseif($will_view == 'profile')
      <div class="myallWillPage">
        @include('livewire.account.organiser.partials.will_menu')
        <livewire:account.organiser.online-will.pages.profile />
      </div>
    @elseif($will_view == 'executor')
      <div class="myallWillPage">
        @include('livewire.account.organiser.partials.will_menu')
        <livewire:account.organiser.online-will.pages.executor />
      </div>
    @elseif($will_view == 'spouse')
      <div class="myallWillPage">
        @include('livewire.account.organiser.partials.will_menu')
        <livewire:account.organiser.online-will.pages.spouses />
      </div>
    @elseif($will_view == 'minor-children-beneficiary')
      <div class="myallWillPage">
        @include('livewire.account.organiser.partials.will_menu')
        <livewire:account.organiser.online-will.pages.minor-children-beneficiary />
      </div>
    @elseif($will_view == 'guardian')
      <div class="myallWillPage">
        @include('livewire.account.organiser.partials.will_menu')
        <livewire:account.organiser.online-will.pages.guardians />
      </div>
    @elseif($will_view == 'testamentary-trust')
      <div class="myallWillPage">
        @include('livewire.account.organiser.partials.will_menu')
        <livewire:account.organiser.online-will.pages.testamentary-trust />
      </div>
    @elseif($will_view == 'inheritance')
      <div class="myallWillPage">
        @include('livewire.account.organiser.partials.will_menu')
        <livewire:account.organiser.online-will.pages.inheritances />
      </div>
    @elseif($will_view == 'specific-inheritance')
      <div class="myallWillPage">
        @include('livewire.account.organiser.partials.will_menu')
        <livewire:account.organiser.online-will.pages.specific-inheritances />
      </div>
    @elseif($will_view == 'remainder-inheritance')
      <div class="myallWillPage">
        @include('livewire.account.organiser.partials.will_menu')
        <livewire:account.organiser.online-will.pages.remainder-inheritances />
      </div>
    @elseif($will_view == 'simultaneous-death')
      <div class="myallWillPage">
        @include('livewire.account.organiser.partials.will_menu')
        <livewire:account.organiser.online-will.pages.simultaneous-death />
      </div>
    @elseif($will_view == 'funeral-wishes')
      <div class="myallWillPage">
        @include('livewire.account.organiser.partials.will_menu')
        <livewire:account.organiser.online-will.pages.funeral-wishes />
      </div>
    @elseif($will_view == 'witnesses')
      <div class="myallWillPage">
        @include('livewire.account.organiser.partials.will_menu')
        <livewire:account.organiser.online-will.pages.witnesses />
      </div>
    @elseif($will_view == 'review')
      <div class="myallWillPage">
        @include('livewire.account.organiser.partials.will_menu')
        <livewire:account.organiser.online-will.review :review_page="$review_page" />
      </div>
    @endif
  @endif

  {{-- <div class="modal" tabindex="-1" id="init-pop-up"> --}}
  {{--   <div class="modal-dialog"> --}}
  {{--     <div class="modal-content"> --}}
  {{--       <div class="modal-body"> --}}
  {{--         <div class="d-flex"> --}}
  {{--           <h5>Online will creation or upload</h5> --}}
  {{--           <span class="ms-auto"> --}}
  {{--             <button type="button" class="btn-close" data-bs-dismiss="modal" --}}
  {{--               aria-label="Close"></button> --}}
  {{--           </span> --}}
  {{--         </div> --}}
  {{--         <div class=""> --}}
  {{--           <p> --}}
  {{--             Effortlessly create or upload your Will online: securely create --}}
  {{--             your will or upload an existing one to --}}
  {{--             MyAll for easy access and peace of mind. --}}
  {{--           </p> --}}
  {{--         </div> --}}
  {{--         <div class="mt-5 text-end"> --}}
  {{--           <a href="#" class="btn btn-outline-two" --}}
  {{--             wire:click.prevent="initModalSeen">Continue</a> --}}
  {{--         </div> --}}
  {{--       </div> --}}
  {{--     </div> --}}
  {{--   </div> --}}
  {{-- </div> --}}

  @push('scripts')
    <script>
      $(document).ready(function() {
        {{-- var show_init_pop = '{{ $show_init_pop_up }}'; --}}
        {{-- if (show_init_pop) { --}}
        {{--   $('#init-pop-up').modal('show'); --}}
        {{-- } --}}
        window.addEventListener('close-modals', event => {
          $('.modal').each(function() {
            $(this).modal('hide');
          })
        });

      });
    </script>
  @endpush
</div>
