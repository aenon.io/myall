<div class="__panel order-md-2 order-1">
  {{-- @include('livewire.account.organiser.partials.info') --}}
  <div class="card-box-style h-100 rounded">
    <form wire:submit.prevent="saveExecutor">
      <livewire:account.organiser.partials.steps :enable_save=true />
      <div class="__island">
        @if ($errors->any())
          <div class="alert alert-danger">
            <span>{{ $errors->first() }}</span>
          </div>
        @endif
        <div class="col-md-12 d-flex">
          <h5></h5>
          <span class="ms-auto">
            <a href="#" class="btn btn-outline-two redo"
              wire:click.prevent="clearSectionData">
              <svg width="16" height="17" viewBox="0 0 16 17"
                fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M8.00033 4.50065V1.83398L4.66699 5.16732L8.00033
                  8.50065V5.83398C10.207 5.83398 12.0003 7.62732 12.0003
                  9.83398C12.0003 12.0407 10.207 13.834 8.00033 13.834C5.79366
                  13.834 4.00033 12.0407 4.00033 9.83398H2.66699C2.66699 12.7807
                  5.05366 15.1673 8.00033 15.1673C10.947 15.1673 13.3337 12.7807
                  13.3337 9.83398C13.3337 6.88732 10.947 4.50065 8.00033
                  4.50065Z" fill="#CF3D60" />
              </svg>
              Redo Section
            </a>
          </span>
        </div>
        <div class="__note mb-3">
          <span class="">
            {{-- TODO: Hard coded fill --}}
            <svg width="20" height="20" viewBox="0 0 20 20"
              fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M9.16663 12.4993H10.8333V14.166H9.16663V12.4993ZM9.16663
                5.83268H10.8333V10.8327H9.16663V5.83268ZM9.99163 1.66602C5.39163
                1.66602 1.66663 5.39935 1.66663 9.99935C1.66663 14.5993 5.39163
                18.3327 9.99163 18.3327C14.6 18.3327 18.3333 14.5993 18.3333
                9.99935C18.3333 5.39935 14.6 1.66602 9.99163 1.66602ZM9.99996
                16.666C6.31663 16.666 3.33329 13.6827 3.33329 9.99935C3.33329
                6.31602 6.31663 3.33268 9.99996 3.33268C13.6833 3.33268 16.6666
                6.31602 16.6666 9.99935C16.6666 13.6827 13.6833 16.666 9.99996
                16.666Z" fill="#040A5E" />
            </svg>
            <div>
              In <a href="{{ url('organiser/personal#trusted-contacts') }}"
                class="">My Trusted Contacts</a> you can
              enter
              the details of the person with whom we
              can share all of your entered data and information to continue
              wrapping up your estate upon your passing.
              We strongly recommend that you nominate the executor who you
              have
              chosen to wrapup your estate with your
              passing.</p>
            </div>
          </span>
        </div>
        <div class="row h-75">

          {{-- <div class="col-md-12 mb-3"> --}}
          {{--   <p class="text-danger"><b>NOTE:</b> --}}
          {{--     In <b><a href="{{ url('organiser/personal#trusted-contacts') }}" --}}
          {{--         class="text-danger">My Trusted Contact</a></b> you can enter --}}
          {{--     the details of the person with whom we --}}
          {{--     can share all of your entered data and information to continue --}}
          {{--     wrapping up your estate upon your passing. --}}
          {{--     We strongly recommend that you nominate the executor who you have --}}
          {{--     chosen to wrapup your estate with your --}}
          {{--     passing.</p> --}}
          {{-- </div> --}}

          <div class="col-md-4">
            <div class="mb-3">
              <label class="form-label">First Name<span
                  class="required-input">*</span></label>
              <input type="text" class="form-control" name="first_name"
                wire:model.defer="first_name">
            </div>
          </div>
          <div class="col-md-4">
            <div class="mb-3">
              <label class="form-label">Middle Name</label>
              <input type="text" class="form-control" name="middle_name"
                wire:model.defer="middle_name">
            </div>
          </div>
          <div class="col-md-4">
            <div class="mb-3">
              <label class="form-label">Surname<span
                  class="required-input">*</span></label>
              <input type="text" class="form-control" name="surname"
                wire:model.defer="surname">
            </div>
          </div>
          <div class="col-md-6">
            <div class="mb-3">
              <label class="form-label">ID Type<span
                  class="required-input">*</span></label>
              <div class="">
                <div class="form-check form-check-inline">
                  <input class="form-check-input" type="radio" name="id_type"
                    id="sa_id" value="rsa_id" wire:model="id_type">
                  <label class="form-check-label" for="sa_id">RSA ID</label>
                </div>
                <div class="form-check form-check-inline">
                  <input class="form-check-input" type="radio" name="id_type"
                    id="passport" value="passport" wire:model="id_type">
                  <label class="form-check-label"
                    for="passport">Passport</label>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="mb-3">
              <label class="form-label">
                @if ($id_type == 'passport')
                  Passport Number<span class="required-input">*</span>
                @else
                  RSA ID Number<span class="required-input">*</span>
                @endif
              </label>
              <input type="text" class="form-control" name="id_number"
                wire:model.defer="id_number">
            </div>
          </div>
          <div class="col-md-12">
            <div class="mb-3">
              <label class="form-label">Relation<span
                  class="required-input">*</span></label>
              <select class="form-control" name="relation"
                wire:model="relation">
                <option value="" selected disabled>Select Option</option>
                @foreach ($relations as $rel)
                  <option value="{{ $rel }}">{{ $rel }}
                  </option>
                @endforeach
              </select>
            </div>
          </div>
          @if ($relation == 'Other')
            <div class="col-md-12">
              <div class="mb-3">
                <label class="form-label">Specify Relation<span
                    class="required-input">*</span></label>
                <input type="text" class="form-control"
                  name="specify_relation" wire:model.defer="specify_relation">
              </div>
            </div>
          @endif
        </div>
        <div class="row h-75">
          <div class="col-md-12 mb-3">
            <h5>Substitute Executor (Optional)</h5>
            <p>A person to be appointed as the executor in the event that the
              initially nominated executor passes away
              before or simultaneously with you.</p>
          </div>
          <div class="col-md-4">
            <div class="mb-3">
              <label class="form-label">First Name<span
                  class="required-input">*</span></label>
              <input type="text" class="form-control" name="first_name"
                wire:model.defer="sub_first_name">
            </div>
          </div>
          <div class="col-md-4">
            <div class="mb-3">
              <label class="form-label">Middle Name</label>
              <input type="text" class="form-control" name="middle_name"
                wire:model.defer="sub_middle_name">
            </div>
          </div>
          <div class="col-md-4">
            <div class="mb-3">
              <label class="form-label">Surname<span
                  class="required-input">*</span></label>
              <input type="text" class="form-control" name="surname"
                wire:model.defer="sub_surname">
            </div>
          </div>
          <div class="col-md-6">
            <div class="mb-3">
              <label class="form-label">ID Type<span
                  class="required-input">*</span></label>
              <div class="">
                <div class="form-check form-check-inline">
                  <input class="form-check-input" type="radio"
                    name="sub_id_type" id="sub_sa_id" value="rsa_id"
                    wire:model="sub_id_type">
                  <label class="form-check-label" for="sub_sa_id">RSA
                    ID</label>
                </div>
                <div class="form-check form-check-inline">
                  <input class="form-check-input" type="radio"
                    name="sub_id_type" id="sub_passport" value="passport"
                    wire:model="sub_id_type">
                  <label class="form-check-label"
                    for="sub_passport">Passport</label>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="mb-3">
              <label class="form-label">
                @if ($sub_id_type == 'passport')
                  Passport Number<span class="required-input">*</span>
                @else
                  RSA ID Number<span class="required-input">*</span>
                @endif
              </label>
              <input type="text" class="form-control" name="id_number"
                wire:model.defer="sub_id_number">
            </div>
          </div>
          <div class="col-md-12">
            <div class="mb-3">
              <label class="form-label">Relation<span
                  class="required-input">*</span></label>
              <select class="form-control" name="relation"
                wire:model="sub_relation">
                <option value="" selected disabled>Select Option</option>
                @foreach ($relations as $rel)
                  <option value="{{ $rel }}">{{ $rel }}
                  </option>
                @endforeach
              </select>
            </div>
          </div>
          @if ($sub_relation == 'Other')
            <div class="col-md-12">
              <div class="mb-3">
                <label class="form-label">Specify Relation<span
                    class="required-input">*</span></label>
                <input type="text" class="form-control"
                  name="specify_relation"
                  wire:model.defer="sub_specify_relation">
              </div>
            </div>
          @endif
        </div>

        <div class="col-md-12">
          <hr>
        </div>

        <div class="">
          <div class="col-md-12 d-flex">
            <div class="d-grid d-md-block gap-2">
              {{--   <input type="submit" class="btn btn-red" value="save"> --}}
              <a href="{{ url()->previous() }}" class="btn btn-red outline">
                Back
              </a>
            </div>
            <div class="ms-auto">
              <a href="#" class="btn btn-outline-two"
                wire:click.prevent="saveExecutor('go_to_next')">Next</a>
            </div>
          </div>
        </div>
    </form>
  </div>
  @include('livewire.account.organiser.partials.will_footer')
</div>

<div class="modal" tabindex="-1" id="info-modal">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Nominate an Executor</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal"
          aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <p>The Executor is the person who will administer the estate</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-red"
          data-bs-dismiss="modal">Continue</button>
      </div>
    </div>
  </div>
</div>
@push('scripts')
  <script>
    $(document).ready(function() {
      var name = '{{ $first_name }}';
      if (!name) {
        $('#info-modal').modal('show');
      }

      window.addEventListener('data-updated', event => {
        Swal.fire("Success", event.detail.message, "success");
      });
    });
  </script>
@endpush
</div>
