<div class="__panel order-md-2 order-1">
  {{-- @include('livewire.account.organiser.partials.info') --}}
  <div class="card-box-style h-100 rounded">
    <form wire:submit.prevent="saveFuneralWishes">
      <livewire:account.organiser.partials.steps :enable_save=true />
      <div class="__island">
        @if ($errors->any())
          <div class="alert alert-danger mb-2">
            <span>{{ $errors->first() }}</span>
          </div>
        @endif
        <div class="row">
          <div class="col-md-12">
            <h5>Funeral Wishes</h5>
          </div>
        </div>
        <div class="row mt-3">
          <div class="col-md-12">

            {{-- <div class="form-check"> --}}
            {{--   <input class="form-check-input" type="checkbox" value="yes" --}}
            {{--     id="berried" name="berried" wire:model.defer="berried"> --}}
            {{--   <label class="form-check-label" for="berried"> --}}
            {{--     Berried --}}
            {{--   </label> --}}
            {{-- </div> --}}

            <div class="mb-3">
              <label class="form-label">Cremated</label>
              <div class="">
                <div class="form-check form-check-inline">
                  <input class="form-check-input" type="radio" name="cremated"
                    id="cremated_yes" value="yes"
                    wire:model.defer="cremated">
                  <label class="form-check-label" for="cremated_yes">
                    Yes
                  </label>
                </div>
                <div class="form-check form-check-inline">
                  <input class="form-check-input" type="radio" name="cremated"
                    id="cremated_no" value="no" wire:model.defer="cremated">
                  <label class="form-check-label" for="cremated_no">
                    No
                  </label>
                </div>
              </div>
            </div>
            <div class="mb-3">
              <label class="form-label">Organ Donor</label>
              <div class="">
                <div class="form-check form-check-inline">
                  <input class="form-check-input" type="radio"
                    name="organ_donor" id="yes" value="yes"
                    wire:model.defer="organ_donor">
                  <label class="form-check-label" for="no">
                    Yes
                  </label>
                </div>
                <div class="form-check form-check-inline">
                  <input class="form-check-input" type="radio"
                    name="organ_donor" id="no" value="no"
                    wire:model.defer="organ_donor">
                  <label class="form-check-label" for="no">
                    No
                  </label>
                </div>
              </div>
            </div>
          </div>

          {{-- <div class="col-md-12"> <label class="form-label">Do you have any --}}
          {{--     special funeral wishes</label> --}}
          {{--   <div class=""> --}}
          {{--     <div class="form-check form-check-inline"> <input --}}
          {{--         class="form-check-input" type="radio" --}}
          {{--         name="has_funeral_wishes" id="has_funeral_wishes_yes" --}}
          {{--         value="yes" wire:model="has_funeral_wishes"> <label --}}
          {{--         class="form-check-label" --}}
          {{--         for="has_funeral_wishes_yes">Yes</label> </div> --}}
          {{--     <div class="form-check form-check-inline"> <input --}}
          {{--         class="form-check-input" type="radio" --}}
          {{--         name="has_funeral_wishes" id="has_funeral_wishes_no" --}}
          {{--         value="no" wire:model="has_funeral_wishes"> <label --}}
          {{--         class="form-check-label" --}}
          {{--         for="has_funeral_wishes_no">No</label> </div> --}}
          {{--   </div> --}}
          {{-- </div> --}}

          @if ($has_funeral_wishes == 'yes')
            <div class="col-md-12">
              <div class="mb-3">
                <label class="form-label">Special funeral wishes</label>
                <textarea class="form-control" name="special_wishes"
                  wire:model.defer="special_wishes"></textarea>
              </div>
            </div>
          @endif
        </div>
        <div class="col-md-12">
          <hr>
        </div>
        <div class="row">
          <div class="col-md-12 d-flex">
            <span>
              {{-- <input type="submit" class="btn btn-red" value="save"> --}}
              <a href="{{ url()->previous() }}" class="btn btn-red outline">
                Back
              </a>
            </span>
            <span class="ms-auto">
              <a href="#" class="btn btn-outline-two"
                wire:click.prevent="saveFuneralWishes('go_to_next')">Next</a>
            </span>
          </div>
        </div>
    </form>
  </div>
  @include('livewire.account.organiser.partials.will_footer')
</div>
@push('scripts')
  <script>
    $(document).ready(function() {
      window.addEventListener('data-updated', event => {
        Swal.fire("Success", event.detail.message, "success");
      });
    });
  </script>
@endpush
</div>
