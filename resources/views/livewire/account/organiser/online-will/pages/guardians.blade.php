<div class="__panel order-md-2 order-1">
  {{-- @include('livewire.account.organiser.partials.info') --}}
  <div class="card-box-style h-100 rounded">
    <form wire:submit.prevent="saveGuardians">
      <livewire:account.organiser.partials.steps :enable_save=true />
      <div class="__island">
        @if ($errors->any())
          <div class="alert alert-danger mb-2">
            <span>{{ $errors->first() }}</span>
          </div>
        @endif
        <div class="row h-75">
          <div class="col-md-12 d-flex align-items-center">
            <h5>Nominate Guardians (Optional)</h5>
            <span class="ms-auto">
              <a href="#" class="btn btn-outline-two redo"
                wire:click.prevent="clearSectionData">
                <svg width="16" height="17" viewBox="0 0 16 17"
                  fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path d="M8.00033 4.50065V1.83398L4.66699 5.16732L8.00033
                  8.50065V5.83398C10.207 5.83398 12.0003 7.62732 12.0003
                  9.83398C12.0003 12.0407 10.207 13.834 8.00033 13.834C5.79366
                  13.834 4.00033 12.0407 4.00033 9.83398H2.66699C2.66699 12.7807
                  5.05366 15.1673 8.00033 15.1673C10.947 15.1673 13.3337 12.7807
                  13.3337 9.83398C13.3337 6.88732 10.947 4.50065 8.00033
                  4.50065Z" fill="#CF3D60" />
                </svg>
                Redo Section
              </a>
            </span>
          </div>
        </div>
        @foreach ($children as $k => $child)
          <div class="{{ count($children) > 1 ? '__child' : '' }}">
            <div class="row mt-3">
              <div class="col-md-4">
                <div class="mb-3">
                  <label class="form-label">Child Name<span
                      class="required-input">*</span></label>
                  <input type="text" class="form-control" name="child_name"
                    wire:model.defer="children.{{ $k }}.child_name">
                </div>
              </div>
              <div class="col-md-4">
                <div class="mb-3">
                  <label class="form-label">Child Middle Name</label>
                  <input type="text" class="form-control" name="child_name"
                    wire:model.defer="children.{{ $k }}.child_middle_name">
                </div>
              </div>
              <div class="col-md-4">
                <div class="mb-3">
                  <label class="form-label">Child Surname<span
                      class="required-input">*</span></label>
                  <input type="text" class="form-control" name="child_name"
                    wire:model.defer="children.{{ $k }}.child_surname">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4">
                <div class="mb-3">
                  <label class="form-label">First name of nominated
                    guardian<span class="required-input">*</span></label>
                  <input type="text" class="form-control" name="first_name"
                    wire:model.defer="children.{{ $k }}.guardian_name">
                </div>
              </div>
              <div class="col-md-4">
                <div class="mb-3">
                  <label class="form-label">Middle name of nominated
                    guardian</label>
                  <input type="text" class="form-control" name="middle_name"
                    wire:model.defer="children.{{ $k }}.guardian_middle_name">
                </div>
              </div>
              <div class="col-md-4">
                <div class="mb-3">
                  <label class="form-label">Surname of nominated guardian<span
                      class="required-input">*</span></label>
                  <input type="text" class="form-control" name="surname"
                    wire:model.defer="children.{{ $k }}.guardian_surname">
                </div>
              </div>
              <div class="col-md-12">
                <div class="row">
                  <div class="col-md-6">
                    <div class="mb-3">
                      <label class="form-label">ID Type</label>
                      <div class="">
                        <div class="form-check">
                          <input class="form-check-input" type="radio"
                            name="id_type_{{ $k }}"
                            id="rsa_id_{{ $k }}" value="rsa_id"
                            wire:model="children.{{ $k }}.id_type">
                          <label class="form-check-label"
                            for="rsa_id_{{ $k }}">
                            RSA ID Number
                          </label>
                        </div>
                        <div class="form-check">
                          <input class="form-check-input" type="radio"
                            name="id_type_{{ $k }}"
                            id="passport_{{ $k }}" value="passport"
                            wire:model="children.{{ $k }}.id_type">
                          <label class="form-check-label"
                            for="passport_{{ $k }}">
                            Passport Number
                          </label>
                        </div>
                      </div>
                    </div>
                  </div>
                  @if ($child['id_type'] == 'rsa_id')
                    <div class="col-md-6">
                      <div class="mb-3">
                        <label class="form-label">ID Number of nominated
                          guardian<span class="required-input">*</span></label>
                        <input type="text" class="form-control"
                          name="id_number"
                          wire:model.defer="children.{{ $k }}.guardian_id_number">
                      </div>
                    </div>
                  @else
                    <div class="col-md-6">
                      <div class="mb-3">
                        <label class="form-label">Passport Number of
                          nominated
                          guardian<span class="required-input">*</span></label>
                        <input type="text" class="form-control"
                          name="id_number"
                          wire:model.defer="children.{{ $k }}.guardian_passport_number">
                      </div>
                    </div>
                  @endif
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 text-end">
                  @if ($child['guardian_name'])
                    <a href="#" class="btn btn-outline-two"
                      wire:click.prevent="removeGuadian({{ $k }})">Remove</a>
                  @endif
                </div>
              </div>
            </div>
          </div>
        @endforeach

        <div class="col-md-12">
          <hr>
        </div>

        <div class="row">
          <div class="col-md-12 d-flex">
            <div class="">
              {{-- <input type="submit" class="btn btn-red" value="save"> --}}
              <a href="{{ url()->previous() }}" class="btn btn-red outline">
                Back
              </a>
            </div>
            <div class="ms-auto">
              <a href="#" class="btn btn-outline-two"
                wire:click.prevent="saveGuardians('go_to_next')">Next</a>
            </div>
          </div>
        </div>
    </form>
  </div>
  @include('livewire.account.organiser.partials.will_footer')
</div>
<div class="modal" tabindex="-1" id="info-modal">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Nominate a Guardian</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal"
          aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <p>The guardian is the person who will look after any minor child (a
          child under 18 years).</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-red"
          data-bs-dismiss="modal">Continue</button>
      </div>
    </div>
  </div>
</div>
@push('scripts')
  <script>
    $(document).ready(function() {
      var show_modal = '{{ $show_modal }}';
      if (show_modal) {
        $('#info-modal').modal("show");
      }

      window.addEventListener('data-updated', event => {
        Swal.fire("Success", event.detail.message, "success");
      });
      window.addEventListener('go-to-top', event => {
        $(window).scrollTop(0);
      });
    });
  </script>
@endpush
</div>
