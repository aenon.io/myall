<div class="__panel order-md-2 order-1">
  {{-- @include('livewire.account.organiser.partials.info') --}}
  <div class="card-box-style h-100 rounded">
    <form wire:submit.prevent="saveInheritance">
      <livewire:account.organiser.partials.steps :enable_save=true />
      <div class="__island">
        @if ($errors->any())
          <div class="alert alert-danger mb-2">
            <span>{{ $errors->first() }}</span>
          </div>
        @endif
        <div class="row h-75">
          <div class="col-md-12 d-flex mb-5">
            <h5>Inheritance</h5>
            <span class="ms-auto">
              <a href="#" class="btn btn-outline-two"
                wire:click.prevent="clearData">redo this section</a>
            </span>
          </div>
        </div>
        <div class="row">
          @if (!$has_data)
            <div class="col-md-12">
              <div class="mb-3">
                <label class="form-label">How will you be dividing your
                  inheritance?</label>
                <select class="form-control" name="division_type"
                  wire:model="division_type">
                  <option value="">Select Option</option>
                  <option value="one_beneficiery">Entire estate to one
                    beneficiary</option>
                  <option value="equal_part">In equal parts</option>
                  <option value="percentage_shares">In percentage shares
                  </option>
                </select>
              </div>
            </div>
          @endif
          @if (!$has_data)
            @if ($division_type == 'one_beneficiery')
              <div class="col-md-12">
                <div class="mb-3">
                  <label class="form-label">Beneficiary Type</label>
                  <select class="form-control" name="beneficiary_type"
                    wire:model="beneficiary_type">
                    <option value="">Select Option</option>
                    <option value="individual">Individual</option>
                    <option value="organisation">Organisation</option>
                  </select>
                </div>
              </div>
            @endif
          @endif
          @foreach ($individuals as $k => $ind)
            <div class="row">
              <div class="col-md-4">
                <div class="mb-3">
                  <label class="form-label">First Name<span
                      class="required-input">*</span></label>
                  <input type="text" class="form-control" name="first_name"
                    wire:model.defer="individuals.{{ $k }}.first_name">
                </div>
              </div>
              <div class="col-md-4">
                <div class="mb-3">
                  <label class="form-label">Middle Name</label>
                  <input type="text" class="form-control" name="middle_name"
                    wire:model.defer="individuals.{{ $k }}.middle_name">
                </div>
              </div>
              <div class="col-md-4">
                <div class="mb-3">
                  <label class="form-label">Surname<span
                      class="required-input">*</span></label>
                  <input type="text" class="form-control" name="surname"
                    wire:model.defer="individuals.{{ $k }}.surname">
                </div>
              </div>
              <div class="col-md-12">
                <div class="row">
                  <div class="col-md-4">
                    <div class="form-check">
                      <input class="form-check-input" type="radio"
                        name="id_type.{{ $k }}" id="rsa_id"
                        value="rsa_id"
                        wire:model="individuals.{{ $k }}.id_type">
                      <label class="form-check-label" for="rsa_id">
                        RSA ID Number
                      </label>
                    </div>
                    <div class="form-check">
                      <input class="form-check-input" type="radio"
                        name="id_type.{{ $k }}" id="dob"
                        value="dob"
                        wire:model="individuals.{{ $k }}.id_type">
                      <label class="form-check-label" for="dob">
                        Date of Birth
                      </label>
                    </div>
                  </div>
                  <div class="col-md-8">
                    @if ($ind['id_type'] == 'dob')
                      <div class="mb-3">
                        <label class="form-label">Date of Birth<span
                            class="required-input">*</span></label>
                        <input type="date" class="form-control"
                          name="dob" max="2999-12-31"
                          wire:model.defer="individuals.{{ $k }}.id_number">
                      </div>
                    @else
                      <div class="mb-3">
                        <label class="form-label">ID Number<span
                            class="required-input">*</span></label>
                        <input type="text" class="form-control"
                          name="id_number"
                          wire:model.defer="individuals.{{ $k }}.id_number">
                      </div>
                    @endif
                  </div>
                </div>
              </div>
              <div class="col-md-12">
                <div class="row">
                  <div
                    class="@if ($ind['relation'] == 'Other') col-md-6 @else col-md-12 @endif">
                    <div class="mb-3">
                      <label class="form-label">Relation<span
                          class="required-input">*</span></label>
                      <select class="form-control" name="relation"
                        wire:model="individuals.{{ $k }}.relation">
                        <option value="">Select Option</option>
                        @foreach ($relations as $rel)
                          <option value="{{ $rel }}">
                            {{ $rel }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  @if ($ind['relation'] == 'Other')
                    <div class="col-md-6">
                      <div class="mb-3">
                        <label class="form-label">Specify Relation<span
                            class="required-input">*</span></label>
                        <input type="text" class="form-control"
                          name="specify_relation"
                          wire:model.defer="individuals.{{ $k }}.specify_relation">
                      </div>
                    </div>
                  @endif
                </div>
              </div>
              @if ($division_type == 'percentage_shares')
                <div class="col-md-12">
                  <div class="row">
                    <div class="col-md-12">
                      <label class="form-label">{{ $ind['percentage'] }}% to
                        inherit</label>
                    </div>
                    <div class="col-md-9 pt-2">
                      <input type="range" class="form-range custom-range"
                        min="0" max="100"
                        wire:model="individuals.{{ $k }}.percentage">
                    </div>
                    <div class="col-md-3">
                      <label class="form-label">Enter the Percentage Amount
                        Here</label>
                      <div class="mb-3">
                        <div class="input-group mb-3">
                          <input type="text" class="form-control"
                            wire:model="individuals.{{ $k }}.percentage">
                          <span class="input-group-text"
                            id="basic-addon1">%</span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              @endif
              <div class="col-md-12">
                <hr />
              </div>
            </div>
          @endforeach
          @foreach ($organisations as $k => $org)
            <div class="row">
              <div class="col-md-6">
                <div class="mb-3">
                  <label class="form-label">Name of Organisation<span
                      class="required-input">*</span></label>
                  <input type="text" class="form-control"
                    name="organisation_name"
                    wire:model.defer="organisations.{{ $k }}.organisation_name">
                </div>
              </div>
              <div class="col-md-6">
                <div class="mb-3">
                  <label class="form-label">Company / Trust Registration
                    Number</label>
                  <input type="text" class="form-control"
                    name="registration_number"
                    wire:model.defer="organisations.{{ $k }}.registration_number">
                </div>
              </div>
              <div class="col-md-12">
                <div class="mb-3">
                  <label class="form-label">Address</label>
                  <textarea class="form-control" name="address"
                    wire:model.defer="organisations.{{ $k }}.address"></textarea>
                </div>
              </div>
              <div class="col-md-6">
                <div class="mb-3">
                  <label class="form-label">City</label>
                  <input type="text" class="form-control" name="city"
                    wire:model.defer="organisations.{{ $k }}.city">
                </div>
              </div>

              <div class="col-md-6">
                <div class="mb-3">
                  <label class="form-label">Province<span
                      class="required-input">*</span></label>
                  <select class="form-control" name="province"
                    wire:model.defer="organisations.{{ $k }}.province">
                    <option value="">Select Option</option>
                    @foreach ($provinces as $pr)
                      <option value="{{ $pr }}">{{ $pr }}
                      </option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="col-md-12">
                <div class="mb-3">
                  <label class="form-label">Organisation Type<span
                      class="required-input">*</span></label>
                  <select class="form-control" name="organisation_type"
                    wire:model="organisations.{{ $k }}.organisation_type">
                    <option value="">Select Option</option>
                    @foreach ($org_types as $tp)
                      <option value="{{ $tp }}">{{ ucwords($tp) }}
                      </option>
                    @endforeach
                  </select>
                </div>
                @if ($org['organisation_type'] == 'other')
                  <div class="mb-3">
                    <label class="form-label">Specify<span
                        class="required-input">*</span></label>
                    <input type="text" class="form-control"
                      name="organisation_type"
                      wire:model.defer="organisations.{{ $k }}.organisation_other_type">
                  </div>
                @endif
              </div>

              @if ($division_type == 'percentage_shares')
                <div class="col-md-12">
                  <div class="row">
                    <div class="col-md-12">
                      <label class="form-label">{{ $org['percentage'] }}% to
                        inherit</label>
                    </div>
                    <div class="col-md-9 pt-2">
                      <input type="range" class="form-range custom-range"
                        min="0" max="100"
                        wire:model="organisations.{{ $k }}.percentage">
                    </div>
                    <div class="col-md-3">
                      <div class="mb-3">
                        <label class="form-label">Enter the percentage amount
                          here</label>
                        <div class="input-group mb-3">
                          <input type="text" class="form-control"
                            wire:model="organisations.{{ $k }}.percentage">
                          <span class="input-group-text"
                            id="basic-addon1">%</span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              @endif
              <div class="col-md-12">
                <hr />
              </div>
            </div>
          @endforeach
        </div>
        <div class="row">
          <div class="col-md-12">
            @if ($division_type != 'one_beneficiery' && $division_type != '')
              <div class="mb-2">Click on a button below to add a beneficiary
              </div>
              <a href="#" class="btn btn-outline-two"
                wire:click.prevent="addIndividual">Add Individual</a>
              <a href="#" class="btn btn-outline-two"
                wire:click.prevent="addOrganisation">Add Organisation</a>
            @endif
          </div>
        </div>
        <div class="row mt-5">
          <div class="col-md-12 d-flex">
            <div class="">
              <input type="submit" class="btn btn-red" value="save">
            </div>
            <div class="ms-auto">
              <a href="#" class="btn btn-outline-two"
                wire:click.prevent="saveInheritance('go_to_next')">Next</a>
            </div>
          </div>
        </div>
        @include('livewire.account.organiser.partials.will_footer')
    </form>
  </div>
</div>
<div class="modal" tabindex="-1" id="percentage-shares-modal">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12 text-end">
            <button type="button" class="btn-close" data-bs-dismiss="modal"
              aria-label="Close"></button>
          </div>
          <div class="col-md-12 mt-3">
            <p>It is recommended that you first identify the exact percentage
              allocation for each beneficiary before
              proceeding to enter their details and respective percentage
              allocations. The sum of these allocations
              should add up to a total of 100%</p>
          </div>
          <div class="col-md-12 mt-3 text-center">
            <button type="button" class="btn btn-red"
              data-bs-dismiss="modal">Continue</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@push('scripts')
  <script>
    $(document).ready(function() {
      window.addEventListener('show-percentage-shares-modal', event => {
        $('#percentage-shares-modal').modal('show');
      });
      window.addEventListener('percentage-overload', event => {
        Swal.fire("Error", event.detail.message, "error");
      });
      window.addEventListener('data-updated', event => {
        Swal.fire("Success", event.detail.message, "success");
      });
    });
  </script>
@endpush
</div>
