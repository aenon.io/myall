<div class="__interview">
  <div class="">

    <div class="__note mb-3">

      <div style="display: flex; align-items: center; gap: 0.4rem">
        <svg width="20" height="20" viewBox="0 0 20 20" fill="none"
          xmlns="http://www.w3.org/2000/svg">
          <path d="M9.16675 12.4993H10.8334V14.166H9.16675V12.4993ZM9.16675
          5.83268H10.8334V10.8327H9.16675V5.83268ZM9.99175 1.66602C5.39175
          1.66602 1.66675 5.39935 1.66675 9.99935C1.66675 14.5993 5.39175
          18.3327 9.99175 18.3327C14.6001 18.3327 18.3334 14.5993 18.3334
          9.99935C18.3334 5.39935 14.6001 1.66602 9.99175 1.66602ZM10.0001
          16.666C6.31675 16.666 3.33341 13.6827 3.33341 9.99935C3.33341 6.31602
          6.31675 3.33268 10.0001 3.33268C13.6834 3.33268 16.6667 6.31602
          16.6667 9.99935C16.6667 13.6827 13.6834 16.666 10.0001 16.666Z"
            fill="#040A5E" />
        </svg>
        <span>
          Edit previous sections by clicking on the panel left. Remember
          to click <strong>save</strong> before continuing.</span>
      </div>

      <div>
        <svg width="20" height="20" viewBox="0 0 20 20" fill="none"
          xmlns="http://www.w3.org/2000/svg">
          <path d="M15.8332 5.34102L14.6582 4.16602L9.99984 8.82435L5.3415
            4.16602L4.1665 5.34102L8.82484 9.99935L4.1665 14.6577L5.3415
            15.8327L9.99984 11.1743L14.6582 15.8327L15.8332 14.6577L11.1748
            9.99935L15.8332 5.34102Z" fill="#040A5E" />
        </svg>
      </div>

    </div>

    {{-- <livewire:account.organiser.partials.steps /> --}}

    <div class="__panel">

      <div class="__heading">
        <h4 class="__title">Acknowledgement</h4>
        <button class="__subtitle">
          <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
            xmlns="http://www.w3.org/2000/svg">
            <path d="M7.3335 4.66732H8.66683V6.00065H7.3335V4.66732ZM7.3335
              7.33398H8.66683V11.334H7.3335V7.33398ZM8.00016 1.33398C4.32016
              1.33398 1.3335 4.32065 1.3335 8.00065C1.3335 11.6807 4.32016
              14.6673 8.00016 14.6673C11.6802 14.6673 14.6668 11.6807 14.6668
              8.00065C14.6668 4.32065 11.6802 1.33398 8.00016 1.33398ZM8.00016
              13.334C5.06016 13.334 2.66683 10.9407 2.66683 8.00065C2.66683
              5.06065 5.06016 2.66732 8.00016 2.66732C10.9402 2.66732 13.3335
              5.06065 13.3335 8.00065C13.3335 10.9407 10.9402 13.334 8.00016
              13.334Z" fill="#CF3D60" />
          </svg>
          <span>More Info</span>
        </button>
      </div>

      <div class="__island">

        {{-- <div class="col-md-12 text-center"> --}}
        {{--   <span class=""> --}}
        {{--     <a href="#" data-toggle="tooltip" data-placement="bottom" --}}
        {{--       title="Video infographics"><img src="{{ asset('img/vid.png') }}" --}}
        {{--         wire:click.prevent="showVid()"></a> --}}
        {{--     <a href="#" data-toggle="tooltip" data-placement="bottom" --}}
        {{--       title="Image infographics"><img src="{{ asset('img/info.png') }}" --}}
        {{--         wire:click.prevent="showInfo()"></a> --}}
        {{--   </span> --}}
        {{-- </div> --}}

        <div class="__question">
          @php
            $count = 0;
          @endphp

          @foreach ($questions as $k => $q)
            @if ($count == $cur_question)
              <h3>{{ $k }}</h3>
              <p>{!! $notes[$cur_question] !!}</p>
              @if ($cur_question == 2)
                <div class="">
                  <div class="form-check">
                    <input class="form-check-input" type="checkbox"
                      value="" id="confirmation"
                      wire:model="confirmation">
                    <label class="form-check-label" for="confirmation">
                      I Understand
                    </label>
                  </div>
                </div>
              @elseif ($cur_question == 1 || $cur_question == 3)
                <div class="">
                  <div class="form-check">
                    <input class="form-check-input" type="checkbox"
                      value="" id="confirmation0"
                      wire:model="confirmation0"
                      wire:click.prevent="saveOption('{{ $cur_question }}')">
                    <label class="form-check-label" for="confirmation0">
                      I Understand
                    </label>
                  </div>
                </div>
              @endif
              <hr>
              @if (count($q) > 2)
                <div class="">
                  <select class="form-control" wire:model.lazy="reply">
                    <option value="" disabled selected>
                      Select Option
                    </option>
                    @foreach ($q as $qq)
                      <option value="{{ $qq }}">
                        {{ $qq }}
                      </option>
                    @endforeach
                  </select>
                </div>
              @else
                <div class="__answers d-flex justify-content-center gap-2">
                  @foreach ($q as $qq)
                    <div class="">
                      @if ($cur_question == 2 && $qq == 'Yes')
                        <a href="#"
                          class="btn btn-red {{ !$confirmation ? 'disabled' : '' }}"
                          wire:click.prevent="{{ $confirmation ? "saveOption('$qq')" : '' }}">
                          {{ $qq }}
                        </a>
                      @elseif ($cur_question == 3 && $qq == 'Yes')
                        {{-- <a href="#" --}}
                        {{--   class="btn btn-red {{ !$confirmation0 ? 'disabled' : '' }}" --}}
                        {{--   wire:click.prevent="{{ $confirmation0 ? "saveOption('$qq')" : '' }}"> --}}
                        {{--   {{ $qq }} --}}
                        {{-- </a> --}}
                      @else
                        <a href="#" class="btn btn-red"
                          wire:click.prevent="saveOption('{{ $qq }}')">
                          {{ $qq }}
                        </a>
                      @endif
                    </div>
                  @endforeach
                </div>
              @endif
            @endif

            @php
              $count++;
            @endphp
          @endforeach

        </div>

      </div>

      <div class="">
        <div class="">
          <livewire:account.organiser.partials.questions-footer />
        </div>
      </div>

    </div>
  </div>

  <div class="modal" tabindex="-1" id="note_modal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">NOTE</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal"
            aria-label="Close"></button>
        </div>
        <div class="modal-body">
          @if ($errors->any())
            <div class="mb-3">
              <div class="alert alert-danger">
                <span>{{ $errors->first() }}</span>
              </div>
            </div>
          @endif
          <p>{{ $notes[$cur_question] }}</p>

          @if ($cur_question == 3)
            <div class="mt-3">
              <div class="form-check">
                <input class="form-check-input" type="checkbox" value=""
                  id="confirmation" wire:model.defer="confirmation">
                <label class="form-check-label" for="confirmation">
                  I Understand
                </label>
              </div>
            </div>
          @endif
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-red" data-bs-dismiss="modal"
            wire:click="showNextquestion()">Next</button>
        </div>
      </div>
    </div>
  </div>

  <div class="modal" tabindex="-1" id="images-modal_interview">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-body">
          <div class="text-end"><button type="button" class="btn-close"
              data-bs-dismiss="modal" aria-label="Close"></button></div>
          <div class="my-3">
            <b class="text-blue">Please select Infographics below for helpful
              information</b>
          </div>
          @if (count($stage_images) > 0)
            <div class="list-group">
              @foreach ($stage_images as $img)
                @php
                  $abs_path = $img;
                  $rel_path = substr(
                      $abs_path,
                      strpos($abs_path, 'infographics') + 13,
                  );

                @endphp
                <a href="#"
                  class="list-group-item list-group-item-action"
                  wire:click.prevent="showSingleImage('{{ $rel_path }}')">
                  {{ basename($img, '.jpg') }}
                </a>
              @endforeach
            </div>
          @endif
        </div>
      </div>
    </div>
  </div>

  <div class="modal" tabindex="-1" id="interview-single-image-modal">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-body">
          <div class="text-end"><button type="button" class="btn-close"
              data-bs-dismiss="modal" aria-label="Close"></button></div>
          <img src="" class="img-fluid w-100" id="cur_interview_img">
        </div>
      </div>
    </div>
  </div>

  <div class="modal" tabindex="-1" id="vids-modal-interview">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-body">
          <div class="text-end"><button type="button" class="btn-close"
              data-bs-dismiss="modal" aria-label="Close"></button></div>
          <div class="my-3">
            <b class="text-blue">Please select videos below for helpful
              information</b>
          </div>
          @if (count($stage_vids) > 0)
            <div class="list-group">
              @foreach ($stage_vids as $vid)
                <a href="#"
                  class="list-group-item list-group-item-action"
                  wire:click.prevent='showSingleVid("{{ $vid }}")'>
                  {{ $vid }}
                </a>
              @endforeach
            </div>
          @endif
        </div>
      </div>
    </div>
  </div>

  <div class="modal" tabindex="-1" id="interview-single-vid-modal">
    <div class="modal-dialog h-100">
      <div class="modal-content">
        <div class="modal-body">
          <div class="text-end"><button type="button" class="btn-close"
              data-bs-dismiss="modal" aria-label="Close"></button></div>
          <div class="cur_interviewvid_cont">
            <video controls>
              <source src="" type="video/mp4" id="cur_interview_vid">
              Your browser does not support the video tag.
            </video>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal" tabindex="-1" id="vid-coming-soon">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-body">
          <div class="text-end"><button type="button" class="btn-close"
              data-bs-dismiss="modal" aria-label="Close"></button></div>
          <div class="text-center">
            <h5>Informative video coming soon!</h5>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal" tabindex="-1" id="info-modal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-body">
          <div class="text-end"><button type="button" class="btn-close"
              data-bs-dismiss="modal" aria-label="Close"></button></div>
          <div class="">
            <p>You are proceeding now with the interview questions which will
              create the Stage 2 questionnaire. If you
              wish to change the online Will structure, kindly click on the REDO
              button located on the previous page.
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>

  @push('scripts')
    <script>
      $(document).ready(function() {
        {{-- $('#info-modal').modal("show"); --}}
        $("#interview-single-vid-modal").on("hidden.bs.modal", function() {
          document.querySelectorAll('video').forEach(vid => vid.pause());
        });
      });

      window.addEventListener('show-vid-coming-soon', event => {
        $('#vid-coming-soon').modal('show');
      });

      window.addEventListener('show-interview-single-vid-modal', event => {
        var src = event.detail.name;

        $('.modal').modal('hide');
        $('body').removeClass('modal-open');
        $('.modal-backdrop').remove();

        $('#cur_interview_vid').attr('src', src);
        $(".cur_interviewvid_cont video")[0].load();
        $('#interview-single-vid-modal').modal('show');

      });

      window.addEventListener('show-interview-vids-modal', event => {
        $('#vids-modal-interview').modal('show');
      });

      window.addEventListener('show-interview-images-modal', event => {
        $('#images-modal_interview').modal('show');
      });
      window.addEventListener('show-interview-single-image-modal', event => {
        var src = event.detail.image_path;

        $('.modal').modal('hide');
        $('body').removeClass('modal-open');
        $('.modal-backdrop').remove();

        $("#cur_interview_img").attr('src', src);
        $('#interview-single-image-modal').modal("show");

      });

      window.addEventListener('show-note-modal', event => {
        $('#note_modal').modal("show");
      })
    </script>
  @endpush
</div>
