<div class="__panel order-md-2 order-1">
  {{-- @include('livewire.account.organiser.partials.info') --}}
  <div class="card-box-style h-100 rounded">
    <form wire:submit.prevent="saveChildren">
      <livewire:account.organiser.partials.steps :enable_save=true />
      <div class="__island">
        @if ($errors->any())
          @foreach ($errors->all() as $err)
            <div class="alert alert-danger mb-2">
              <span>{{ $err }}</span>
            </div>
          @endforeach
        @endif
        <div class="__note mb-3">
          <span class="">
            {{-- TODO: Hard coded fill --}}
            <svg width="20" height="20" viewBox="0 0 20 20" fill="none"
              xmlns="http://www.w3.org/2000/svg">
              <path d="M9.16663 12.4993H10.8333V14.166H9.16663V12.4993ZM9.16663
                    5.83268H10.8333V10.8327H9.16663V5.83268ZM9.99163
                    1.66602C5.39163 1.66602 1.66663 5.39935 1.66663
                    9.99935C1.66663 14.5993 5.39163 18.3327 9.99163 18.3327C14.6
                    18.3327 18.3333 14.5993 18.3333 9.99935C18.3333 5.39935 14.6
                    1.66602 9.99163 1.66602ZM9.99996 16.666C6.31663 16.666
                    3.33329 13.6827 3.33329 9.99935C3.33329 6.31602 6.31663
                    3.33268 9.99996 3.33268C13.6833 3.33268 16.6666 6.31602
                    16.6666 9.99935C16.6666 13.6827 13.6833 16.666 9.99996
                    16.666Z" fill="#040A5E" />
            </svg>
            <div>To change these details, please do so in the <a
                href="{{ url('organiser/personal') }}">Organiser</a></div>
          </span>
        </div>

        {{-- <div class="row h-75"> --}}
        {{--   <div class="col-md-12"> --}}
        {{--     <h5>Children Details</h5> --}}
        {{--   </div> --}}
        {{-- </div> --}}

        @foreach ($children as $k => $child)
          <div class="row {{ count($children) > 1 ? '__child' : '' }}">
            <div class="col-md-4">
              <div class="mb-3">
                <div class="form-group">
                  <label>First Name<span class="required-input">*</span></label>
                  <input type="text" class="form-control" name="child_name"
                    wire:model.defer="children.{{ $k }}.child_name">
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="mb-3">
                <div class="form-group">
                  <label>Middle Name</label>
                  <input type="text" class="form-control" name="middle_name"
                    wire:model.defer="children.{{ $k }}.middle_name">
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="mb-3">
                <div class="form-group">
                  <label>Surname<span class="required-input">*</span></label>
                  <input type="text" class="form-control"
                    name="child_surname"
                    wire:model.defer="children.{{ $k }}.child_surname">
                </div>
              </div>
            </div>
            <div class="col-md-12">
              <div class="row">
                <div class="col-md-4">
                  <div class="form-check">
                    <input class="form-check-input" type="radio"
                      name="id_type.{{ $k }}" id="dob"
                      value="dob"
                      wire:model="children.{{ $k }}.id_type">
                    <label class="form-check-label" for="dob">
                      Date of Birth
                    </label>
                  </div>
                  <div class="form-check">
                    <input class="form-check-input" type="radio"
                      name="id_type.{{ $k }}" id="rsa_id"
                      value="rsa_id"
                      wire:model="children.{{ $k }}.id_type">
                    <label class="form-check-label" for="rsa_id">
                      ID Number
                    </label>
                  </div>
                </div>
                <div class="col-md-8">
                  @if ($child['id_type'] == 'dob')
                    <div class="mb-3">
                      <div class="form-group">
                        <label>Date of Birth<span
                            class="required-input">*</span></label>
                        <input type="date" class="form-control"
                          name="child_date_of_birth" max="2999-12-31"
                          wire:key="d_{{ $k }}"
                          wire:model.defer="children.{{ $k }}.child_date_of_birth">
                      </div>
                    </div>
                  @else
                    <div class="mb-3">
                      <div class="form-group">
                        <label>ID Number<span
                            class="required-input">*</span></label>
                        <input type="text" class="form-control"
                          name="child_id_number" data-masked=""
                          data-inputmask="'mask': '999999 9999 999'"
                          wire:model.defer="children.{{ $k }}.child_id_number">
                      </div>
                    </div>
                  @endif
                </div>
              </div>
            </div>
            {{-- <div class="col-md-12"> --}}
            {{--   <hr /> --}}
            {{-- </div> --}}
          </div>
        @endforeach

        <div class="col-md-12">
          <hr>
        </div>

        <div class="row">
          <div class="col-md-12 d-md-flex">
            <div class="d-grid d-md-block mt-2 gap-2">
              {{--   <input type="submit" class="btn btn-red" value="save"> --}}
              <a href="{{ url()->previous() }}" class="btn btn-red outline">
                Back
              </a>
            </div>
            <div class="ms-md-auto d-grid d-md-block mt-2 gap-2">
              <a href="#"
                class="btn btn-outline-two __action-button-outline"
                wire:click.prevent="addChild">
                <svg width="16" height="17" viewBox="0 0 16 17"
                  fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path
                    d="M12.6668
                    9.16732H8.66683V13.1673H7.3335V9.16732H3.3335V7.83398H7.3335V3.83398H8.66683V7.83398H12.6668V9.16732Z"
                    fill="#CF3D60" />
                </svg>
                Add Child
              </a>
              <a href="#" class="btn btn-outline-two"
                wire:click.prevent="saveChildren('go_to_next')">Next</a>
            </div>
          </div>
        </div>
    </form>
  </div>
  @include('livewire.account.organiser.partials.will_footer')
</div>

@push('scripts')
  <script>
    $(document).ready(function() {
      window.addEventListener('data-updated', event => {
        Swal.fire("Success", event.detail.message, "success");
      });
    });
  </script>
@endpush
</div>
