<div class="__panel order-md-2 order-1">
  {{-- @include('livewire.account.organiser.partials.info') --}}
  <div class="card-box-style h-100 rounded">
    <form wire:submit.prevent="saveProfile">
      <livewire:account.organiser.partials.steps :enable_save=true />
      <div class="__island">
        @if ($errors->any())
          <div class="alert alert-danger">
            <span>{{ $errors->first() }}</span>
          </div>
        @endif
        <div class="row h-75">
          <div class="col-md-12">
            <div class="__note mb-3">
              <span class="">
                {{-- TODO: Hard coded fill --}}
                <svg width="20" height="20" viewBox="0 0 20 20"
                  fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path d="M9.16663 12.4993H10.8333V14.166H9.16663V12.4993ZM9.16663
                    5.83268H10.8333V10.8327H9.16663V5.83268ZM9.99163
                    1.66602C5.39163 1.66602 1.66663 5.39935 1.66663
                    9.99935C1.66663 14.5993 5.39163 18.3327 9.99163 18.3327C14.6
                    18.3327 18.3333 14.5993 18.3333 9.99935C18.3333 5.39935 14.6
                    1.66602 9.99163 1.66602ZM9.99996 16.666C6.31663 16.666
                    3.33329 13.6827 3.33329 9.99935C3.33329 6.31602 6.31663
                    3.33268 9.99996 3.33268C13.6833 3.33268 16.6666 6.31602
                    16.6666 9.99935C16.6666 13.6827 13.6833 16.666 9.99996
                    16.666Z" fill="#040A5E" />
                </svg>
                <div>To change your contact details, please <strong>contact
                    us.</strong>
                </div>
              </span>
            </div>
          </div>
          <div class="col-md-4">
            <div class="mb-3">
              <label class="form-label">First Name<span
                  class="required-input">*</span></label>
              <input type="text" class="form-control" name="first_name"
                wire:model.defer="first_name" disabled>
            </div>
          </div>
          <div class="col-md-4">
            <div class="mb-3">
              <label class="form-label">Middle Name</label>
              <input type="text" class="form-control" name="middle_name"
                wire:model.defer="middle_name" disabled>
            </div>
          </div>
          <div class="col-md-4">
            <div class="mb-3">
              <label class="form-label">Surname<span
                  class="required-input">*</span></label>
              <input type="text" class="form-control" name="surname"
                wire:model.defer="surname" disabled>
            </div>
          </div>
          <div class="col-md-4">
            <div class="mb-3">
              <label class="form-label">ID Number<span
                  class="required-input">*</span></label>
              <input type="text" class="form-control" name="id_number"
                data-masked="" data-inputmask="'mask': '999999 9999 999'"
                wire:model.defer="id_number" disabled>
            </div>
          </div>
          <div class="col-md-4">
            <div class="mb-3">
              <label class="form-label">Passport Number<span
                  class="required-input">*</span></label>
              <input type="text" class="form-control" name="passport_number"
                wire:model.defer="passport_number" disabled>
            </div>
          </div>
          <div class="col-md-4">
            <div class="mb-3">
              <label class="form-label">Date of Birth<span
                  class="required-input">*</span></label>
              <input type="date" class="form-control" name="date_of_birth"
                wire:model.defer="date_of_birth" disabled>
            </div>
          </div>
          <div class="col-md-12">
            <div class="mb-3">
              <label class="form-label">Street Address<span
                  class="required-input">*</span></label>
              <input type="text" class="form-control" name="address"
                wire:model.defer="address">
            </div>
          </div>
          <div class="col-md-12">
            <div class="mb-3">
              <label class="form-label">City<span
                  class="required-input">*</span></label>
              <input type="text" class="form-control" name="city"
                wire:model.defer="city">
            </div>
          </div>

          {{-- <div class="col-md-12"> --}}
          {{--   <div class="mb-3"> <label class="form-label">Postal Code<span --}}
          {{--         class="required-input">*</span></label> <input type="text" --}}
          {{--       class="form-control" name="postal_code" --}}
          {{--       wire:model.defer="postal_code"> </div> --}}
          {{-- </div> --}}

          <div class="col-md-12">
            <div class="mb-3">
              <label class="form-label">Province<span
                  class="required-input">*</span></label>
              <select class="form-control" name="province"
                wire:model.defer="province">
                <option value="" disabled>Select Province</option>
                @foreach ($provinces as $province)
                  <option value="{{ $province }}">{{ $province }}
                  </option>
                @endforeach
              </select>
            </div>
          </div>

          {{-- <div class="col-md-12"> --}}
          {{--   <div class="mb-3"> <label class="form-label">Specific Instructions --}}
          {{--       (if you have any)</label> --}}
          {{--     <textarea class="form-control" name="instructions" --}}
          {{--       wire:model.defer="instructions"></textarea> --}}
          {{--   </div> --}}
          {{-- </div> --}}
          <div class="col-md-12">
            <hr>
          </div>
          <div class="col-md-12">
            <div class="d-flex justify-content-end">
              {{-- <div --}}
              {{--   class="col-md-6 upload-view-btns d-grid d-md-block mt-2 gap-2"> --}}
              {{--   <input type="submit" class="btn btn-red" value="save"> --}}
              {{-- </div> --}}
              <div
                class="col-md-6 upload-view-btns d-grid d-md-block mt-2 gap-2 text-end">
                <a href="#" class="btn btn-outline-two"
                  wire:click.prevent="saveProfile('go_to_next')">Next</a>
              </div>
            </div>
          </div>
        </div>
    </form>
  </div>
  @include('livewire.account.organiser.partials.will_footer')
</div>
@push('scripts')
  <script>
    $(document).ready(function() {
      window.addEventListener('data-updated', event => {
        Swal.fire("Success", event.detail.message, "success");
      });
    });
  </script>
@endpush
</div>
