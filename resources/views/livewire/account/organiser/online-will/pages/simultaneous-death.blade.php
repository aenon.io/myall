<div class="__panel order-md-2 __simultaneous order-1">
  {{-- @include('livewire.account.organiser.partials.info') --}}
  <div class="card-box-style h-100 rounded">

    {{-- TODO: Perhaps use the $view_page variable to do some logic with the
    save button. --}}
    <livewire:account.organiser.partials.steps :enable_save_simultaneous="true" />

    <div class="__island">

      @if ($errors->any())
        <div class="col-md-12 mt-5">
          <div class="alert alert-danger mb-2">
            <span>{{ $errors->first() }}</span>
          </div>
        </div>
      @endif

      <div class="col-md-12 justify-content-end d-md-flex mb-3">
        <div class="d-flex gap-2">
          <a href="#" class="btn btn-outline-two redo __margin"
            wire:click.prevent="clearInheritance">
            <svg width="16" height="17" viewBox="0 0 16 17"
              fill="none" xmlns="http://www.w3.org/2000/svg">
              <path
                d="M8.00033 4.50065V1.83398L4.66699 5.16732L8.00033
                8.50065V5.83398C10.207 5.83398 12.0003 7.62732 12.0003
                9.83398C12.0003 12.0407 10.207 13.834 8.00033 13.834C5.79366
                13.834 4.00033 12.0407 4.00033 9.83398H2.66699C2.66699 12.7807
                5.05366 15.1673 8.00033 15.1673C10.947 15.1673 13.3337 12.7807
                13.3337 9.83398C13.3337 6.88732 10.947 4.50065 8.00033 4.50065Z"
                fill="#CF3D60" />
            </svg>
            Redo Section</a>
          <a href="#" class="btn btn-outline-two redo view-inheritance"
            data-bs-toggle="modal" data-bs-target="#allocated-inheritance">
            View inheritance allocations
          </a>
        </div>
      </div>

      <div class="col-md-12">
        @if ($view_page == 'start')
          @include('livewire.account.organiser.online-will.pages.simultaneous-death.start')
        @elseif($view_page == 'specific_inheritance')
          @include('livewire.account.organiser.online-will.pages.simultaneous-death.specific_inheritance')
        @elseif($view_page == 'remainder_inheritance')
          @include('livewire.account.organiser.online-will.pages.simultaneous-death.remainder_inheritance')
        @elseif($view_page == 'inheritance')
          @include('livewire.account.organiser.online-will.pages.simultaneous-death.inheritance')
        @endif
      </div>
    </div>

    <div class="row">
      <div class="col-md-12">
        @include('livewire.account.organiser.partials.will_footer')
      </div>
    </div>
  </div>

  <div class="modal" id="allocated-inheritance" tabindex="-1"
    aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div
      class="modal-dialog modal-lg modal-dialog-centered __allocated-inheritance">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Allocated Inheritances
          </h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal"
            aria-label="Close"></button>
        </div>
        <div class="modal-body">
          @if ($inheritances->count() > 0)
            @php
              $tp = null;
            @endphp
            <table class="tabkle-striped table">
              @foreach ($inheritances as $inh)
                @if ($inh->type != $tp)
                  @php
                    $tp = $inh->type;
                  @endphp
                  <thead>
                    <tr>
                      <th colspan="2">
                        {{ ucwords(str_replace('-', ' ', $tp)) }}</th>
                    </tr>
                  </thead>
                @endif
                @foreach ($inh->individual_beneficiary as $ben)
                  <tr>
                    <td>{{ $ben->first_name . ' ' . $ben->surname }}</td>
                    <td>
                      @if ($inh->type == 'specific-inheritance')
                        {{ $ben->description }}
                      @else
                        @if ($inh->division_type == 'one_beneficiery')
                          All Of My Inheritance
                        @elseif($inh->division_type == 'equal_part')
                          In Equal Parts
                        @elseif($inh->division_type == 'percentage_shares')
                          {{ $ben->percentage }}% Share
                        @endif
                      @endif
                    </td>
                  </tr>
                @endforeach
                @foreach ($inh->organisation_beneficiery as $ben)
                  <tr>
                    <td>{{ $ben->organisation_name }}</td>
                    <td>
                      @if ($inh->type == 'specific-inheritance')
                        {{ $ben->description }}
                      @else
                        @if ($inh->division_type == 'one_beneficiery')
                          All Of My Inheritance
                        @elseif($inh->division_type == 'equal_part')
                          In Equal Parts
                        @elseif($inh->division_type == 'percentage_shares')
                          {{ $ben->percentage }}% Share
                        @endif
                      @endif
                    </td>
                  </tr>
                @endforeach
              @endforeach
            </table>
          @else
            <div class="row">
              <div class="col-md-12 mb-5 mt-5 text-center">
                <h4>You have not allocated any inheritances</h4>
              </div>
            </div>
          @endif
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-red"
            data-bs-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

  <div class="modal" tabindex="-1" id="percentage-shares-modal">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12 text-end">
              <button type="button" class="btn-close" data-bs-dismiss="modal"
                aria-label="Close"></button>
            </div>
            <div class="col-md-12 mt-3">
              <p>It is recommended that you first identify the exact percentage
                allocation for each beneficiary before proceeding to enter their
                details and respective percentage allocations. The sum of these
                allocations should add up to a total of 100%</p>
            </div>
            <div class="col-md-12 mt-3 text-center">
              <button type="button" class="btn btn-red"
                data-bs-dismiss="modal">Continue</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  @push('scripts')
    <script>
      $(document).ready(function() {
        window.addEventListener('show-percentage-shares-modal', event => {
          $('#percentage-shares-modal').modal('show');
        });
        window.addEventListener('error_on_page', event => {
          Swal.fire("Error", event.detail.message, "error");
        });
        window.addEventListener('data-updated', event => {
          Swal.fire("Success", event.detail.message, "success");
        });
        window.addEventListener('go-to-top', event => {
          window.scrollTo(0, 0);
        });
      });
    </script>
  @endpush

</div>
