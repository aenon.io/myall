<div class="col-md-8 order-1 order-md-2">
    @include('livewire.account.organiser.partials.info')
    <div class="card-box-style rounded h-100">
        <livewire:account.organiser.partials.steps />
        <div class="col-md-12 mt-5 h-100">
            @if($errors->any())
                <div class="alert alert-danger mb-2">
                    <span>{{ $errors->first() }}</span>
                </div>
            @endif
            {{--
            <div class="row">
                <div class="col-md-12 d-md-flex">
                    <h5>Simultaneous Death</h5>
                    <div class="ms-md-auto d-grid gap-2 d-md-block mt-2">
                        <a href="#" class="btn btn-outline-two" data-bs-toggle="modal" data-bs-target="#allocated-inheritance">View Inheritance Allocations</a>
                    </div>
                </div>
                <div class="col-md-12 mt-5">
                    <p>In the event that you and your beneficiary die simultaneously (at the same time), how will you be dividing your inheritance.</p>
                </div>
                
                <div class="col-md-10 offset-md-1 mt-5">
                    <div class="row">
                        <div class="col-md-6 text-center">
                            <a href="#" class="btn btn-outline-two" wire:click.prevent="setSimOption('inheritance')">Inheritance</a>
                        </div>
                        <div class="col-md-6 text-center">
                            <a href="#" class="btn btn-outline-two" wire:click.prevent="setSimOption('specific-inheritance')">Specific Inheritance</a>
                        </div>
                    </div>
                </div>
                
            </div>
            --}}
            @foreach($sim_inheritances AS $key=>$inh)
            <div class="row mt-3">
                @if($show_division_type && !$show_remainder_inheritance)
                {{--
                <div class="col-md-12">
                    <div class="mb-3">
                        <label class="form-label">Inheritance Type</label>
                        <select class="form-control" name="inheritance_type" wire:model="sim_inheritances.{{ $key }}.inheritance_type">
                            <option value="">Select Option</option>
                            <option value="inheritance">Inheritance</option>
                            <option value="specific_inheritance">Specific Inheritance</option>
                        </select>
                    </div>
                </div>
                --}}
                @else
                <div class="col-md-12 text-end my-3">
                    <a href="#" class="btn btn-outline-two" wire:click="deleteInheritance">Redo this section</a>
                </div>
                @endif
                <div class="col-md-12">
                    @if($show_remainder_inheritance)
                        @include('livewire.account.organiser.online-will.pages.simultaneous-death.remainder_inheritance')
                    @else
                        @include('livewire.account.organiser.online-will.pages.simultaneous-death1.simultaneous_death')
                        {{--
                        @include('livewire.account.organiser.online-will.pages.simultaneous-death.inheritance')
                        --}}
                    @endif
                </div>
            </div>
            <div class="row mt-5">
                <div class="col-md-12 text-end">
                    @if($show_inheritance_add_btns && $sim_inheritances[$key]['inheritance_type'] == "inheritance" && $sim_inheritances[$key]['division_type'] != "one_beneficiery")
                        <a href="#" class="btn btn-outline-two" wire:click.prevent="addIndividual({{ $key }})">Add Individual</a>
                        <a href="#" class="btn btn-outline-two" wire:click.prevent="addOrganisation({{ $key }})">Add Organisation</a>
                    @endif
                </div>
            </div>
            @endforeach

            <div class="row mt-5">
                <div class="col-md-12 d-md-flex">
                    <div class="d-grid gap-2 d-md-block mt-2">
                        <a href="#" class="btn btn-red" wire:click.prevent="saveInheritance">SAVE</a>
                    </div>
                    <div class="ms-md-auto text-end d-grid gap-2 d-md-inline mt-2">
                        <a href="#" class="btn btn-outline-two" wire:click.prevent="addInheritance">Add Inheritance</a>
                        <a href="#" class="btn btn-outline-two" wire:click.prevent="saveInheritance('go_to_next')">Next</a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    @include('livewire.account.organiser.partials.will_footer')
                </div>
            </div>
        </div>
    </div>
    
    <div class="modal" id="allocated-inheritance" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Allocated Inheritances</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    @if($inheritances->count() > 0)
                        @php
                        $tp = null;
                        @endphp
                        <table class="table tabkle-striped">
                            @foreach($inheritances AS $inh)
                                @if($inh->type != $tp)
                                    @php
                                        $tp = $inh->type;
                                    @endphp
                                    <thead>
                                        <tr>
                                            <th colspan="2">{{ ucwords(str_replace('-', ' ', $tp)) }}</th>
                                        </tr>
                                    </thead>
                                @endif
                                @foreach($inh->individual_beneficiary AS $ben)
                                    <tr>
                                        <td>{{ $ben->first_name.' '.$ben->surname }}</td>
                                        <td>
                                            @if($inh->type == "specific-inheritance")
                                                {{ $ben->description }}
                                            @else
                                                @if($inh->division_type == "one_beneficiery")
                                                    All Of My Inheritance
                                                @elseif($inh->division_type == "equal_part")
                                                    In Equal Parts
                                                @elseif($inh->division_type == "percentage_shares")
                                                    {{ $ben->percentage }}% Share 
                                                @endif
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                @foreach($inh->organisation_beneficiery AS $ben)
                                    <tr>
                                        <td>{{ $ben->organisation_name }}</td>
                                        <td>
                                            @if($inh->type == "specific-inheritance")
                                                {{ $ben->description }}
                                            @else
                                                @if($inh->division_type == "one_beneficiery")
                                                    All Of My Inheritance
                                                @elseif($inh->division_type == "equal_part")
                                                    In Equal Parts
                                                @elseif($inh->division_type == "percentage_shares")
                                                    {{ $ben->percentage }}% Share 
                                                @endif
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            @endforeach
                        </table>
                    @else
                        <div class="row">
                            <div class="col-md-12 mt-5 mb-5 text-center">
                                <h4>You have not allocated any inheritances</h4>
                            </div>
                        </div>
                    @endif
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-red" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    @push('scripts')
    <script>
        $(document).ready(function(){
            window.addEventListener('error_on_page', event => {
                Swal.fire("Error", event.detail.message, "error");
            });
            window.addEventListener('data-updated', event => {
                Swal.fire("Success", event.detail.message, "success");
            });
        });
    </script>
    @endpush
</div>
