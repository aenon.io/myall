@if ($division_type == 'one_beneficiery')
  @if ($show_options)
    <div class="row">
      <div class="col-md-12">
        <div class="mb-3">
          <label class="form-label">Are your leaving your inheritance to an
            Organisation or Individual</label>
          <select class="form-control" name="beneficiary_type"
            wire:model="beneficiary_type">
            <option value="">Select Option</option>
            <option value="individual">Individual</option>
            <option value="organisation">Organisation</option>
          </select>
        </div>
      </div>
    </div>
  @endif
@endif
@foreach ($benefiaciaries as $k => $ben)
  @if ($ben['type'] == 'individual')
    <div class="row">
      <div class="col-md-4">
        <div class="mb-3">
          <label>Name<span class="required-input">*</span></label>
          <input type="text" class="form-control" name="name"
            wire:model.defer="benefiaciaries.{{ $k }}.name">
        </div>
      </div>
      <div class="col-md-4">
        <div class="mb-3">
          <label>Middle Name</label>
          <input type="text" class="form-control" name="middle_name"
            wire:model.defer="benefiaciaries.{{ $k }}.middle_name">
        </div>
      </div>
      <div class="col-md-4">
        <div class="mb-3">
          <label>Surname<span class="required-input">*</span></label>
          <input type="text" class="form-control" name="surname"
            wire:model.defer="benefiaciaries.{{ $k }}.surname">
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-4">
        <div class="form-check">
          <input class="form-check-input" type="radio"
            name="id_type.{{ $k }}" id="rsa_id" value="rsa_id"
            wire:model="benefiaciaries.{{ $k }}.id_type">
          <label class="form-check-label" for="rsa_id">
            RSA ID Number
          </label>
        </div>
        <div class="form-check">
          <input class="form-check-input" type="radio"
            name="id_type.{{ $k }}" id="dob" value="dob"
            wire:model="benefiaciaries.{{ $k }}.id_type">
          <label class="form-check-label" for="dob">
            Date of Birth
          </label>
        </div>
      </div>
      <div class="col-md-8">
        @if ($ben['id_type'] == 'dob')
          <div class="mb-3">
            <label class="form-label">Date of Birth<span
                class="required-input">*</span></label>
            <input type="date" class="form-control" name="dob"
              max="2999-12-31"
              wire:model.defer="benefiaciaries.{{ $k }}.date_of_birth">
          </div>
        @else
          <div class="mb-3">
            <label class="form-label">ID Number<span
                class="required-input">*</span></label>
            <input type="text" class="form-control" name="id_number"
              wire:model.defer="benefiaciaries.{{ $k }}.id_number">
          </div>
        @endif
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="mb-3">
          <label class="form-label">Relation<span
              class="required-input">*</span></label>
          <select class="form-control" name="relation"
            wire:model="benefiaciaries.{{ $k }}.relation">
            <option value="">Select Option</option>
            @foreach ($relations as $rel)
              <option value="{{ $rel }}">{{ $rel }}</option>
            @endforeach
          </select>
        </div>
      </div>
      @if ($ben['relation'] == 'Other')
        <div class="col-md-12">
          <div class="mb-3">
            <label class="form-label">Specify Relation<span
                class="required-input">*</span></label>
            <input type="text" class="form-control" name="specify_relation"
              wire:model.defer="benefiaciaries.{{ $k }}.specify_relation">
          </div>
        </div>
      @endif
    </div>
  @endif
  @if ($ben['type'] == 'organisation')
    <div class="row">
      <div class="col-md-6">
        <div class="mb-3">
          <label class="form-label">Name of Organisation<span
              class="required-input">*</span></label>
          <input type="text" class="form-control" name="organisation_name"
            wire:model.defer="benefiaciaries.{{ $k }}.organisation_name">
        </div>
      </div>
      <div class="col-md-6">
        <div class="mb-3">
          <label class="form-label">Company / Trust Registration Number</label>
          <input type="text" class="form-control" name="reg_number"
            wire:model.defer="benefiaciaries.{{ $k }}.reg_number">
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="mb-3">
          <label class="form-label">Address</label>
          <textarea class="form-control" name="address"
            wire:model.defer="benefiaciaries.{{ $k }}.address"></textarea>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
        <div class="mb-3">
          <label class="form-label">City</label>
          <input type="text" class="form-control" name="city"
            wire:model.defer="benefiaciaries.{{ $k }}.city">
        </div>
      </div>
      <div class="col-md-6">
        <div class="mb-3">
          <label class="form-label">Province<span
              class="required-input">*</span></label>
          <select class="form-control" name="province"
            wire:model.defer="benefiaciaries.{{ $k }}.province">
            <option value="">Select Option</option>
            @foreach ($provinces as $pr)
              <option value="{{ $pr }}">{{ $pr }}
              </option>
            @endforeach
          </select>
        </div>
      </div>
    </div>
    <div class="row">
      <div
        class="@if ($ben['org_type'] == 'other') col-md-6 @else col-md-12 @endif">
        <div class="mb-3">
          <label class="form-label">Organisation Type<span
              class="required-input">*</span></label>
          <select class="form-control" name="org_type"
            wire:model="benefiaciaries.{{ $k }}.org_type">
            <option value="">Select Option</option>
            @foreach ($org_types as $tp)
              <option value="{{ $tp }}">{{ $tp }}
              </option>
            @endforeach
          </select>
        </div>
      </div>
      @if ($ben['org_type'] == 'other')
        <div class="col-md-6">
          <div class="mb-3">
            <label class="form-label">Specify Organisation Type<span
                class="required-input">*</span></label>
            <input type="text" class="form-control"
              name="specify_organisation_type"
              wire:model.defer="benefiaciaries.{{ $k }}.specify_organisation_type">
          </div>
        </div>
      @endif
    </div>
  @endif
  @if ($division_type == 'percentage_shares')
    <div class="row">
      <div class="col-md-7 pt-2">
        <label class="form-label">% to inherit</label>
        <input type="range" class="form-range custom-range" min="0"
          max="100"
          wire:model="benefiaciaries.{{ $k }}.percentage">
      </div>
      <div class="col-md-5">
        <div class="mb-3">
          <label class="form-label">Enter the Percentage Amount Here</label>
          <input type="text" class="form-control"
            wire:model="benefiaciaries.{{ $k }}.percentage">
        </div>
      </div>
    </div>
  @endif
  @if (isset($ben['id']))
    <div class="row mb-3">
      <div class="col-md-12 text-end">
        <a href="#" class="btn btn-outline-two"
          wire:click.prevent="removeBeneficiary('{{ $ben['type'] }}', {{ $ben['id'] }})">Remove</a>
      </div>
    </div>
  @endif
@endforeach

@if ($division_type != 'one_beneficiery' && $division_type != '')
  <div class="row">
    <div class="d-flex justify-content-end mt-3 gap-2">
      <a href="#" class="btn btn-outline-two outline"
        wire:click.prevent="addIndividual">
        <svg width="16" height="17" viewBox="0 0 16 17"
          fill="none" xmlns="http://www.w3.org/2000/svg">
          <path
            d="M12.6663 9.16732H8.66634V13.1673H7.33301V9.16732H3.33301V7.83398H7.33301V3.83398H8.66634V7.83398H12.6663V9.16732Z"
            fill="#CF3D60" />
        </svg>
        Add Individual
      </a>
      <a href="#" class="btn btn-outline-two outline"
        wire:click.prevent="addOrganisation">
        <svg width="16" height="17" viewBox="0 0 16 17"
          fill="none" xmlns="http://www.w3.org/2000/svg">
          <path
            d="M12.6663 9.16732H8.66634V13.1673H7.33301V9.16732H3.33301V7.83398H7.33301V3.83398H8.66634V7.83398H12.6663V9.16732Z"
            fill="#CF3D60" />
        </svg>
        Add Organisation
      </a>
    </div>
  </div>
@endif

<div class="col-md-12">
  <hr />
</div>

<div class="row">
  <div class="col-md-12 d-md-flex">
    {{-- <div class="d-grid d-md-block mt-2 gap-2"> --}}
    {{--   <a href="#" class="btn btn-red" --}}
    {{--     wire:click.prevent="saveInheritance">Save</a> --}}
    {{-- </div> --}}
    <div class="ms-md-auto d-grid d-md-block mt-2 gap-2">
      <a href="#" class="btn btn-outline-two"
        wire:click.prevent="saveInheritance('go_to_next')">Next</a>
    </div>
  </div>
</div>
