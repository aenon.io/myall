<div class="row">
  <div class="col-md-12">
    <h5>Inheritance</h5>
    <p>If there is more than one beneficiary receiving the inheritance, start by
      indicating how the inheritance will be divided: in equal parts or with
      each beneficiary receiving a percentage.</p>
  </div>
</div>
<div class="row mt-3">
  @if ($show_options)
    <div class="col-md-12">
      <div class="mb-3">
        <label class="form-label">How will you be dividing your
          inheritance?</label>
        <select class="form-control" name="division_type"
          wire:model="division_type">
          <option value="">Select Option</option>
          <option value="one_beneficiery">Entire estate to one beneficiary
          </option>
          <option value="equal_part">In equal parts</option>
          <option value="percentage_shares">In percentage shares</option>
        </select>
      </div>
    </div>
  @endif
  <div class="col-md-12">
    @include('livewire.account.organiser.online-will.pages.simultaneous-death.form')
  </div>
</div>

