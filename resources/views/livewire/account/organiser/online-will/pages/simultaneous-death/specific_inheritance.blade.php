<div class="row">
  <div class="col-md-12">
    <h5>Specific Inheritance</h5>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    @foreach ($specific_inheritances as $k => $inh)
      @if (!isset($inh['id']))
        <div class="mb-3">
          <label class="form-label">Are your leaving your inheritance to an
            Organisation or Individual</label>
          <select class="form-control" name="beneficiary_type"
            wire:model="specific_inheritances.{{ $k }}.beneficiary_type">
            <option value="">Select Option</option>
            <option value="individual">Individual</option>
            <option value="organisation">Organisation</option>
          </select>
        </div>
      @endif
      @if ($inh['beneficiary_type'] == 'individual')
        <div class="row">
          <div class="col-md-4">
            <div class="mb-3">
              <label>Name<span class="required-input">*</span></label>
              <input type="text" class="form-control" name="name"
                wire:model.defer="specific_inheritances.{{ $k }}.name">
            </div>
          </div>
          <div class="col-md-4">
            <div class="mb-3">
              <label>Middle Name</label>
              <input type="text" class="form-control" name="middle_name"
                wire:model.defer="specific_inheritances.{{ $k }}.middle_name">
            </div>
          </div>
          <div class="col-md-4">
            <div class="mb-3">
              <label>Surname<span class="required-input">*</span></label>
              <input type="text" class="form-control" name="surname"
                wire:model.defer="specific_inheritances.{{ $k }}.surname">
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4">
            <div class="form-check">
              <input class="form-check-input" type="radio"
                name="id_type.{{ $k }}" id="rsa_id" value="rsa_id"
                wire:model="specific_inheritances.{{ $k }}.id_type">
              <label class="form-check-label" for="rsa_id">
                RSA ID Number
              </label>
            </div>
            <div class="form-check">
              <input class="form-check-input" type="radio"
                name="id_type.{{ $k }}" id="dob" value="dob"
                wire:model="specific_inheritances.{{ $k }}.id_type">
              <label class="form-check-label" for="dob">
                Date of Birth
              </label>
            </div>
          </div>
          <div class="col-md-8">
            @if ($inh['id_type'] == 'dob')
              <div class="mb-3">
                <label class="form-label">Date of Birth<span
                    class="required-input">*</span></label>
                <input type="date" class="form-control" name="dob"
                  max="2999-12-31"
                  wire:model.defer="specific_inheritances.{{ $k }}.date_of_birth">
              </div>
            @else
              <div class="mb-3">
                <label class="form-label">ID Number<span
                    class="required-input">*</span></label>
                <input type="text" class="form-control" name="id_number"
                  wire:model.defer="specific_inheritances.{{ $k }}.id_number">
              </div>
            @endif
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="mb-3">
              <label class="form-label">Relation<span
                  class="required-input">*</span></label>
              <select class="form-control" name="relation"
                wire:model="specific_inheritances.{{ $k }}.relation">
                <option value="">Select Option</option>
                @foreach ($relations as $rel)
                  <option value="{{ $rel }}">{{ $rel }}
                  </option>
                @endforeach
              </select>
            </div>
          </div>
          @if ($inh['relation'] == 'Other')
            <div class="col-md-12">
              <div class="mb-3">
                <label class="form-label">Specify Relation<span
                    class="required-input">*</span></label>
                <input type="text" class="form-control"
                  name="specify_relation"
                  wire:model.defer="specific_inheritances.{{ $k }}.specify_relation">
              </div>
            </div>
          @endif
        </div>
      @elseif($inh['beneficiary_type'] == 'organisation')
        <div class="row">
          <div class="col-md-6">
            <div class="mb-3">
              <label class="form-label">Name of Organisation<span
                  class="required-input">*</span></label>
              <input type="text" class="form-control"
                name="organisation_name"
                wire:model.defer="specific_inheritances.{{ $k }}.organisation_name">
            </div>
          </div>
          <div class="col-md-6">
            <div class="mb-3">
              <label class="form-label">Company / Trust registration
                number</label>
              <input type="text" class="form-control" name="reg_number"
                wire:model.defer="specific_inheritances.{{ $k }}.reg_number">
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="mb-3">
              <label class="form-label">Address</label>
              <textarea class="form-control" name="address"
                wire:model.defer="specific_inheritances.{{ $k }}.address"></textarea>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <div class="mb-3">
              <label class="form-label">City</label>
              <input type="text" class="form-control" name="city"
                wire:model.defer="specific_inheritances.{{ $k }}.city">
            </div>
          </div>
          <div class="col-md-6">
            <div class="mb-3">
              <label class="form-label">Province<span
                  class="required-input">*</span></label>
              <select class="form-control" name="province"
                wire:model.defer="specific_inheritances.{{ $k }}.province">
                <option value="">Select Option</option>
                @foreach ($provinces as $pr)
                  <option value="{{ $pr }}">{{ $pr }}
                  </option>
                @endforeach
              </select>
            </div>
          </div>
        </div>
        <div class="row">
          <div
            class="@if ($inh['org_type'] == 'other') col-md-6 @else col-md-12 @endif">
            <div class="mb-3">
              <label class="form-label">Organisation Type<span
                  class="required-input">*</span></label>
              <select class="form-control" name="org_type"
                wire:model="specific_inheritances.{{ $k }}.org_type">
                <option value="">Select Option</option>
                @foreach ($org_types as $tp)
                  <option value="{{ $tp }}">{{ $tp }}
                  </option>
                @endforeach
              </select>
            </div>
          </div>
          @if ($inh['org_type'] == 'other')
            <div class="col-md-6">
              <div class="mb-3">
                <label class="form-label">Specify Organisation Type<span
                    class="required-input">*</span></label>
                <input type="text" class="form-control"
                  name="specify_organisation_type"
                  wire:model.defer="specific_inheritances.{{ $k }}.specify_organisation_type">
              </div>
            </div>
          @endif
        </div>
      @endif

      @if ($inh['beneficiary_type'])
        <div class="mb-3">
          <label class="form-label">Describe the items / possessions<span
              class="required-input">*</span></label>
          <textarea class="form-control" name="description"
            wire:model.defer="specific_inheritances.{{ $k }}.description"></textarea>
          <span class="form-text">For example: "R2000" or "House situated at 12
            Church Street, Cape Town"</span>
        </div>
      @endif
      <div class="col-md-12 mb-3">
        @if (isset($inh['id']))
          <div class="text-end">
            <a href="#" class="btn btn-outline-two"
              wire:click.prevent="removeBeneficiary('{{ $inh['beneficiary_type'] }}', {{ $inh['id'] }})">Remove</a>
          </div>
        @endif
      </div>
    @endforeach

  </div>
</div>

<div class="row">
  <div class="d-flex justify-content-end mt-3 gap-2">
    <a href="#" class="btn btn-outline-two outline"
      wire:click.prevent="addSpecificInheritance">
      <svg width="16" height="17" viewBox="0 0 16 17" fill="none"
        xmlns="http://www.w3.org/2000/svg">
        <path
          d="M12.6663 9.16732H8.66634V13.1673H7.33301V9.16732H3.33301V7.83398H7.33301V3.83398H8.66634V7.83398H12.6663V9.16732Z"
          fill="#CF3D60" />
      </svg>
      Add Specific Inheritance</a>
  </div>
</div>

<div class="col-md-12">
  <hr />
</div>

<div class="row mt-3">
  <div class="col-md-12 d-md-flex">
    <div class="col-md-4 mb-3">
      <div class="d-grid d-md-block mt-2 gap-2">
        {{--   <a href="#" class="btn btn-red" --}}
        {{--     wire:click.prevent="saveSpecificInheritance">SAVE</a> --}}
      </div>
    </div>
    <div class="col-md-8 d-md-flex mb-3">
      <div class="ms-md-auto d-grid d-md-block mt-2 gap-2">
        <a href="#" class="btn btn-outline-two"
          wire:click.prevent="saveSpecificInheritance('go-to-next')">Next</a>
      </div>
    </div>
  </div>
</div>
