<div class="row">
  {{-- <div class="col-md-12"> --}}
  {{--   <h5>Simultaneous Death</h5> --}}
  {{-- </div> --}}
  <div class="col-md-12">
    <b>You have opted to make provision in the event you and your beneficiary
      die simultaneously (at the same time).</b>
    <ul class="mt-3">
      {{-- <li>Click on the button "View inheritance allocations" to get an overview --}}
      {{--   of the inheritance allocations.</li> --}}
      {{-- <li>Proceed below to select the appropriate option what will happen in the --}}
      {{--   event that both you and the original beneficiary die simultaneously. --}}
      {{-- </li> --}}
      <li>By selecting the option 'specific inheritance', you will then nominate
        a designated beneficiary to receive a specific inheritance in the event
        that both you and the original beneficiary die simultaneously. According
        to the law, a beneficiary or beneficiaries must be nominated to receive
        whatever is left. If not, the remainder of the estate will be
        distributed according to the intestate rules set by South African law.
      </li>
      <li>By selecting the option 'inheritance', it means that you have chosen
        not to give a specific bequest to another designated beneficiary. In
        this case, you will nominate one or more beneficiaries who will inherit
        your entire estate in the event that both you and the original
        beneficiary die simultaneously.</li>
    </ul>
  </div>
  <div class="col-md-12">
    <div class="d-flex justify-content-end mt-3 gap-2">
      <a href="#" class="btn btn-red outline"
        wire:click.prevent="showInheritance('specific_inheritance')">
        <svg width="16" height="17" viewBox="0 0 16 17" fill="none"
          xmlns="http://www.w3.org/2000/svg">
          <path
            d="M12.6663 9.16732H8.66634V13.1673H7.33301V9.16732H3.33301V7.83398H7.33301V3.83398H8.66634V7.83398H12.6663V9.16732Z"
            fill="#CF3D60" />
        </svg>
        Add Specific Inheritance
      </a>
      <a href="#" class="btn btn-red outline"
        wire:click.prevent="showInheritance('inheritance')">
        <svg width="16" height="17" viewBox="0 0 16 17" fill="none"
          xmlns="http://www.w3.org/2000/svg">
          <path
            d="M12.6663 9.16732H8.66634V13.1673H7.33301V9.16732H3.33301V7.83398H7.33301V3.83398H8.66634V7.83398H12.6663V9.16732Z"
            fill="#CF3D60" />
        </svg>
        Add Inheritance
      </a>
    </div>
  </div>
</div>
