<div>
  <form wire:submit.prevent="saveInheritance">
    <div class="row h-75">
      <div class="col-md-12 d-flex mb-3">
        <h5>Remainder Inheritance</h5>
        <span class="ms-auto">
          <a href="#" class="btn btn-outline-two btn-sm" data-bs-toggle="modal"
            data-bs-target="#allocated-inheritance">View Inheritance Allocations</a>
        </span>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="mb-3">
          <p>If there is more than one beneficiary receiving the remainder, start by indicating how the remainder will
            be divided: in equal parts or with each beneficiary receiving a percentage.</p>
        </div>
        <div class="mb-3">
          <label class="form-label">How will you be dividing your remainder inheritance?</label>
          <select class="form-control" name="division_type" wire:model="division_type">
            <option value="">Select Option</option>
            <option value="one_beneficiery">Remainder of estate to one beneficiary</option>
            <option value="equal_part">In equal parts</option>
            <option value="percentage_shares">In percentage shares</option>
          </select>
        </div>
      </div>
      @if ($division_type == 'one_beneficiery')
        <div class="col-md-12">
          <div class="mb-3">
            <label class="form-label">Beneficiary type</label>
            <select class="form-control" name="beneficiary_type" wire:model="beneficiary_type">
              <option value="">Select Option</option>
              <option value="individual">Individual</option>
              <option value="organisation">Organisation</option>
            </select>
          </div>
        </div>
      @endif
      @foreach ($individuals as $k => $ind)
        <div class="row">
          <div class="col-md-4">
            <div class="mb-3">
              <label class="form-label">First Name</label>
              <input type="text" class="form-control" name="first_name"
                wire:model.defer="individuals.{{ $k }}.first_name">
            </div>
          </div>
          <div class="col-md-4">
            <div class="mb-3">
              <label class="form-label">Middle Name</label>
              <input type="text" class="form-control" name="middle_name"
                wire:model.defer="individuals.{{ $k }}.middle_name">
            </div>
          </div>
          <div class="col-md-4">
            <div class="mb-3">
              <label class="form-label">Surname</label>
              <input type="text" class="form-control" name="surname"
                wire:model.defer="individuals.{{ $k }}.surname">
            </div>
          </div>
          <div class="col-md-12">
            <div class="mb-3">
              <label class="form-label">ID Number / Date of Birth</label>
              <input type="text" class="form-control" name="id_number"
                wire:model.defer="individuals.{{ $k }}.id_number">
            </div>
          </div>
          @if ($division_type == 'percentage_shares')
            <div class="col-md-12">
              <div class="row">
                <div class="col-md-12">
                  <label class="form-label">% to inherit</label>
                </div>
                <div class="col-md-9 pt-2">
                  <input type="range" class="form-range custom-range" min="0" max="100"
                    wire:model="individuals.{{ $k }}.percentage">
                </div>
                <div class="col-md-3">
                  <div class="mb-3">
                    <input type="text" class="form-control" wire:model="individuals.{{ $k }}.percentage">
                  </div>
                </div>
              </div>
            </div>
          @endif
          <div class="col-md-12">
            <hr />
          </div>
        </div>
      @endforeach
      @foreach ($organisations as $k => $org)
        <div class="row">
          <div class="col-md-6">
            <div class="mb-3">
              <label class="form-label">Name of Organisation</label>
              <input type="text" class="form-control" name="organisation_name"
                wire:model.defer="organisations.{{ $k }}.organisation_name">
            </div>
          </div>
          <div class="col-md-6">
            <div class="mb-3">
              <label class="form-label">Company / Trust Registration Number</label>
              <input type="text" class="form-control" name="registration_number"
                wire:model.defer="organisations.{{ $k }}.registration_number">
            </div>
          </div>
          <div class="col-md-12">
            <div class="mb-3">
              <label class="form-label">Address</label>
              <textarea class="form-control" name="address" wire:model.defer="organisations.{{ $k }}.address"></textarea>
            </div>
          </div>
          <div class="col-md-4">
            <div class="mb-3">
              <label class="form-label">City</label>
              <input type="text" class="form-control" name="city"
                wire:model.defer="organisations.{{ $k }}.city">
            </div>
          </div>
          <div class="col-md-4">
            <div class="mb-3">
              <label class="form-label">Postal Code</label>
              <input type="text" class="form-control" name="postal_code"
                wire:model.defer="organisations.{{ $k }}.postal_code">
            </div>
          </div>
          <div class="col-md-4">
            <div class="mb-3">
              <label class="form-label">Province</label>
              <input type="text" class="form-control" name="province"
                wire:model.defer="organisations.{{ $k }}.province">
            </div>
          </div>
          <div class="col-md-12">
            <div class="mb-3">
              <label class="form-label">Organisation Type</label>
              <input type="text" class="form-control" name="organisation_type"
                wire:model.defer="organisations.{{ $k }}.organisation_type">
            </div>
          </div>
          @if ($division_type == 'percentage_shares')
            <div class="col-md-12">
              <div class="row">
                <div class="col-md-12">
                  <label class="form-label">% to inherit</label>
                </div>
                <div class="col-md-9 pt-2">
                  <input type="range" class="form-range custom-range" min="0" max="100"
                    wire:model="organisations.{{ $k }}.percentage">
                </div>
                <div class="col-md-3">
                  <div class="mb-3">
                    <input type="text" class="form-control"
                      wire:model="organisations.{{ $k }}.percentage">
                  </div>
                </div>
              </div>
            </div>
          @endif
          <div class="col-md-12">
            <hr />
          </div>
        </div>
      @endforeach
    </div>
    <div class="row mt-5">
      <div class="col-md-12 d-flex">
        <div class="">
          <input type="submit" class="btn btn-red" value="save">
        </div>
        <div class="ms-auto">
          @if ($division_type != 'one_beneficiery' && $division_type != '')
            <a href="#" class="btn btn-outline-two" wire:click.prevent="addIndividual">Add Individual</a>
            <a href="#" class="btn btn-outline-two" wire:click.prevent="addOrganisation">Add Organisation</a>
          @endif
          <a href="#" class="btn btn-outline-two" wire:click.prevent="saveInheritance('go_to_next')">Next</a>
        </div>
      </div>
    </div>
  </form>
  @push('scripts')
    <script>
      $(document).ready(function() {
        var show_modal = '{{ $show_modal }}';
        if (show_modal) {
          $('#info-modal').modal('show');
        }
        window.addEventListener('percentage-overload', event => {
          Swal.fire("Error", event.detail.message, "error");
        });
        window.addEventListener('data-updated', event => {
          Swal.fire("Success", event.detail.message, "success");
        });
      });
    </script>
  @endpush
</div>

