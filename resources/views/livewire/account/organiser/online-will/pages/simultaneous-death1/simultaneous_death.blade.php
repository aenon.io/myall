<div class="col-md-12 mt-5">
    <div class="row">
        <div class="col-md-12">
            <h5>Simultaneous Death</h5>
        </div>
        <div class="col-md-12 mt-3">
            <p>In the event that you and your beneficiary die simultaneously (at the same time), how will you be dividing your inheritance?</p>
            <p>Please select <b>only one</b> of the following</p>
        </div>
        <div class="col-md-12 mt-3">
            <div class="mb-3">
                <label class="form-label">Select Option</label>
                <select class="form-control" name="sim_option" wire:model="sim_option">
                    <option value="">Select Option</option>
                    <option value="inheritance">Inheritance</option>
                    <option value="specific-inheritance">Specific Inheritance</option>
                </select>
            </div>
        </div>
        @if($sim_option == "inheritance")
            <livewire:account.organiser.online-will.pages.simultaneous-death.inheritance />
        @elseif($sim_option == "specific-inheritance")
            <livewire:account.organiser.online-will.pages.simultaneous-death.specific-inheritance />
        @elseif($sim_option == "remainder-inheritance")
            <livewire:account.organiser.online-will.pages.simultaneous-death.remainder-inheritance />
        @endif
    </div>
</div>