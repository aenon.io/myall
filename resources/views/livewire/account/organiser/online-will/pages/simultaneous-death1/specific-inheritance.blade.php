<div>
  <form wire:submit.prevent="saveInheritance">
    <div class="row h-75">
      <div class="col-md-12 d-flex mb-3">
        <h5>Specific Inheritance</h5>
        <span class="ms-auto">
          <a href="#" class="btn btn-outline-two btn-sm" data-bs-toggle="modal"
            data-bs-target="#allocated-inheritance">View Inheritance Allocations</a>
        </span>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="mb-3">
          <label class="form-label">Are your leaving your inheritance to an Organisation or Individual</label>
          <select class="form-control" name="beneficiary_type" wire:model="beneficiary_type">
            <option value="">Select Option</option>
            <option value="individual">Individual</option>
            <option value="organisation">Organisation</option>
          </select>
        </div>
      </div>
    </div>
    @if ($beneficiary_type == 'individual')
      <div class="row">
        <div class="col-md-4">
          <div class="mb-3">
            <label class="form-label">First Name</label>
            <input type="text" class="form-control" name="first_name" wire:model.defer="first_name">
          </div>
        </div>
        <div class="col-md-4">
          <div class="mb-3">
            <label class="form-label">Middle Name</label>
            <input type="text" class="form-control" name="middle_name" wire:model.defer="middle_name">
          </div>
        </div>
        <div class="col-md-4">
          <div class="mb-3">
            <label class="form-label">Surname</label>
            <input type="text" class="form-control" name="surname" wire:model.defer="surname">
          </div>
        </div>
        <div class="col-md-12">
          <div class="mb-3">
            <label class="form-label">ID Number / Date Of Birth</label>
            <input type="text" class="form-control" name="id_number" wire:model.defer="id_number">
          </div>
        </div>
        <div class="col-md-12">
          <div class="mb-3">
            <label class="form-label">Relation</label>
            <input type="text" class="form-control" name="relation" wire:model.defer="relation">
          </div>
        </div>
        <div class="col-md-12">
          <div class="mb-3">
            <label class="form-label">Describe the items / possessions</label>
            <textarea class="form-control" name="description" wire:model.defer="description"></textarea>
            <span class="form-text">
              For example: "R2000" or "House situated at 12 Church Street, Cape Town"
            </span>
          </div>
        </div>
        {{--
            <div class="col-md-12">
                <div class="mb-3">
                    <label class="form-label">Sum of inheritance in Rands</label>
                    <div class="input-group mb-3">
                        <span class="input-group-text" id="basic-addon1">ZAR</span>
                        <input type="text" class="form-control" name="value" wire:model.defer="value">
                    </div>
                </div>
            </div>
            --}}
      </div>
    @endif
    @if ($beneficiary_type == 'organisation')
      <div class="row">
        <div class="col-md-6">
          <div class="mb-3">
            <label class="form-label">Name of Organisation</label>
            <input type="text" class="form-control" name="organisation_name" wire:model.defer="organisation_name">
          </div>
        </div>
        <div class="col-md-6">
          <div class="mb-3">
            <label class="form-label">Company / Trust registration number</label>
            <input type="text" class="form-control" name="registration_number"
              wire:model.defer="registration_number">
          </div>
        </div>
        <div class="col-md-12">
          <div class="mb-3">
            <label class="form-label">Address</label>
            <textarea class="form-control" name="address" wire:model.defer="address"></textarea>
          </div>
        </div>
        <div class="col-md-4">
          <div class="mb-3">
            <label class="form-label">City</label>
            <input type="text" class="form-control" name="city" wire:model.defer="city">
          </div>
        </div>
        <div class="col-md-4">
          <div class="mb-3">
            <label class="form-label">Postal Code</label>
            <input type="text" class="form-control" name="postal_code" wire:model.defer="postal_code">
          </div>
        </div>
        <div class="col-md-4">
          <div class="mb-3">
            <label class="form-label">Province</label>
            <input type="text" class="form-control" name="province" wire:model.defer="province">
          </div>
        </div>
        <div class="col-md-12">
          <div class="mb-3">
            <label class="form-label">Describe the items / possessions</label>
            <textarea class="form-control" name="description" wire:model.defer="description"></textarea>
            <span class="form-text">
              For example: "R2000" or "House situated at 12 Church Street, Cape Town"
            </span>
          </div>
        </div>
        {{--
            <div class="col-md-12">
                <div class="mb-3">
                    <label class="form-label">Sum of inheritance in Rands</label>
                    <div class="input-group mb-3">
                        <span class="input-group-text" id="basic-addon1">ZAR</span>
                        <input type="text" class="form-control" name="value" wire:model.defer="value">
                    </div>
                </div>
            </div>
            --}}
      </div>
    @endif
    <div class="row mt-5">
      <div class="col-md-12 d-flex">
        <div class="">
          <input type="submit" class="btn btn-red" value="save">
        </div>
        <div class="ms-auto">
          <a href="#" class="btn btn-outline-two" wire:click.prevent="saveInheritance('go_to_next')">Next</a>
        </div>
      </div>
    </div>
  </form>
  @push('scripts')
    <script>
      $(document).ready(function() {
        window.addEventListener('data-updated', event => {
          Swal.fire("Success", event.detail.message, "success");
        });
      });
    </script>
  @endpush
</div>

