<div class="row">
    @if($inh['inheritance_type'] == "inheritance")
    @if($show_division_type)
    <div class="col-md-12">
        <div class="mb-3">
            <label class="form-label">How will you be dividing your inheritance?</label>
            <select class="form-control" name="division_type" wire:model="sim_inheritances.{{ $key }}.division_type">
                <option value="">Select Option</option>
                <option value="one_beneficiery">Entire estate to one beneficiary</option>
                <option value="equal_part">In equal parts</option>
                <option value="percentage_shares">In percentage shares</option>
            </select>
        </div>
    </div>
    @endif

    @endif
    @if($inh['division_type'] == "one_beneficiery" || $inh['inheritance_type'] == "specific_inheritance")
        <div class="col-md-12">
            <div class="mb-3">
                <label class="form-label">Beneficiary type</label>
                <select class="form-control" name="beneficiary_type" wire:model="sim_inheritances.{{ $key }}.beneficiary_type">
                    <option value="">Select Option</option>
                    <option value="individual">Individual</option>
                    <option value="organisation">Organisation</option>
                </select>
            </div>
        </div>
    @endif

    @foreach($sim_inheritances[$key]['beneficiaries'] AS $kk=>$ben)
        @if($ben['type'] == "individual")
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-4">
                    <div class="mb-3">
                        <label class="form-label">Name</label>
                        <input type="text" class="form-control" name="first_name" wire:model.defer="sim_inheritances.{{ $key }}.beneficiaries.{{ $kk }}.first_name">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="mb-3">
                        <label class="form-label">Middle Name</label>
                        <input type="text" class="form-control" name="middle_name" wire:model.defer="sim_inheritances.{{ $key }}.beneficiaries.{{ $kk }}.middle_name">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="mb-3">
                        <label class="form-label">Surname</label>
                        <input type="text" class="form-control" name="surname" wire:model.defer="sim_inheritances.{{ $key }}.beneficiaries.{{ $kk }}.surname">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="id_type.{{ $key.'.'.$kk }}" id="rsa_id" value="rsa_id" wire:model="sim_inheritances.{{ $key }}.beneficiaries.{{ $kk }}.id_type">
                        <label class="form-check-label" for="rsa_id">
                            RSA ID Number
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="id_type.{{ $key.'.'.$kk }}" id="dob" value="dob" wire:model="sim_inheritances.{{ $key }}.beneficiaries.{{ $kk }}.id_type">
                        <label class="form-check-label" for="dob">
                            Date Of Birth
                        </label>
                    </div>
                </div>
                <div class="col-md-8">
                    @if($ben['id_type'] == "dob")
                    <div class="mb-3">
                        <label class="form-label">Date of Birth</label>
                        <input type="date" class="form-control" name="dob" max="2999-12-31" wire:model.defer="sim_inheritances.{{ $key }}.beneficiaries.{{ $kk }}.date_of_birth">
                    </div>
                    @else
                    <div class="mb-3">
                        <label class="form-label">ID Number</label>
                        <input type="text" class="form-control" name="id_number" wire:model.defer="sim_inheritances.{{ $key }}.beneficiaries.{{ $kk }}.id_number">
                    </div>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="mb-3">
                        <label class="form-label">Relation</label>
                        <select class="form-control" name="relation" wire:model.defer="sim_inheritances.{{ $key }}.beneficiaries.{{ $kk }}.relation">
                            <option value="">Select Option</option>
                            @foreach($relations AS $relation)
                            <option value="{{ $relation }}">{{ $relation }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            @if($inh['division_type'] == "percentage_shares")
            <div class="row">
                <div class="col-md-12">
                    <div class="mb-3">
                        <label class="form-label">% to inherit</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-9 pt-2">
                    <input type="range" class="form-range custom-range" min="0" max="100" wire:model="sim_inheritances.{{ $key }}.beneficiaries.{{ $kk }}.percentage">
                </div>
                <div class="col-md-3">
                    <label class="form-label">Enter the percentage amount here</label>
                    <input type="text" class="form-control" wire:model="sim_inheritances.{{ $key }}.beneficiaries.{{ $kk }}.percentage">
                </div>
            </div>
            @endif
            @if($inh['inheritance_type'] == "specific_inheritance")
            <div class="row">
                <div class="col-md-12">
                    <div class="mb-3">
                        <label class="form-label">Describe the items / possessions</label>
                        <textarea class="form-control" name="description" wire:model.defer="sim_inheritances.{{ $key }}.beneficiaries.{{ $kk }}.description"></textarea>
                        <span class="form-text">
                            For example: "R2000" or "House situated at 12 Church Street, Cape Town"
                        </span>
                    </div>
                </div>
            </div>
            @endif
            @if(isset($ben['id']))
            <div class="row">
                <div class="col-md-12 text-end mt-3">
                    <a href="#" class="btn btn-outline-two" wire:click.prevent="removeIndividual({{ $ben['id'] }})">Remove</a>
                </div>
            </div>
            @endif
            <div class="row">
                <div class="col-md-12">
                    <hr />
                </div>
            </div>
        </div>
        @elseif($ben['type'] == "organisation")
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6">
                    <div class="mb-3">
                        <label class="form-label">Name Of Organisation</label>
                        <input type="text" class="form-control" name="organisation_name" wire:model="sim_inheritances.{{ $key }}.beneficiaries.{{ $kk }}.organisation_name">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="mb-3">
                        <label class="form-label">Company / Trust Registration Number</label>
                        <input type="text" class="form-control" name="registration_number" wire:model="sim_inheritances.{{ $key }}.beneficiaries.{{ $kk }}.registration_number">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="mb-3">
                        <label class="form-label">Address</label>
                        <textarea class="form-control" name="address" wire:model="sim_inheritances.{{ $key }}.beneficiaries.{{ $kk }}.address"></textarea>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="mb-3">
                        <label class="form-label">City</label>
                        <input type="text" class="form-control" name="city" wire:model="sim_inheritances.{{ $key }}.beneficiaries.{{ $kk }}.city">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="mb-3">
                        <label class="form-label">Postal Code</label>
                        <input type="text" class="form-control" name="postal_code" wire:model="sim_inheritances.{{ $key }}.beneficiaries.{{ $kk }}.postal_code">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="mb-3">
                        <label class="form-label">Province</label>
                        <select class="form-control" name="province" wire:model="sim_inheritances.{{ $key }}.beneficiaries.{{ $kk }}.province">
                            <option value="">Select Option</option>
                            @foreach($provinces AS $province)
                            <option value="{{ $province }}">{{ $province }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="mb-3">
                        <label class="form-label">Organisation Type</label>
                        <select class="form-control" name="organisation_type" wire:model="sim_inheritances.{{ $key }}.beneficiaries.{{ $kk }}.organisation_type">
                            <option value="">Select Option</option>
                            @foreach($org_types AS $tp)
                            <option value="{{ $tp }}">{{ ucwords($tp) }}</option>
                            @endforeach
                        </select>
                    </div>
                    @if($ben['organisation_type'] == "other")
                    <div class="mb-3">
                        <label class="form-label">Specify</label>
                        <input type="text" class="form-control" name="organisation_type" wire:model="sim_inheritances.{{ $key }}.beneficiaries.{{ $kk }}.organisation_other_type">
                    </div>
                    @endif
                </div>

            </div>
            @if($inh['division_type'] == "percentage_shares")
            <div class="row">
                <div class="col-md-12">
                    <div class="mb-3">
                        <label class="form-label">% to inherit</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-9 pt-2">
                    <input type="range" class="form-range custom-range" min="0" max="100" wire:model="sim_inheritances.{{ $key }}.beneficiaries.{{ $kk }}.percentage">
                </div>
                <div class="col-md-3">
                    <label class="form-label">Enter the percentage amount here</label>
                    <input type="text" class="form-control" wire:model="sim_inheritances.{{ $key }}.beneficiaries.{{ $kk }}.percentage">
                </div>
            </div>
            @endif
            @if($inh['inheritance_type'] == "specific_inheritance")
            <div class="row">
                <div class="col-md-12">
                    <div class="mb-3">
                        <label class="form-label">Describe the items / possessions</label>
                        <textarea class="form-control" name="description" wire:model.defer="sim_inheritances.{{ $key }}.beneficiaries.{{ $kk }}.description"></textarea>
                        <span class="form-text">
                            For example: "R2000" or "House situated at 12 Church Street, Cape Town"
                        </span>
                    </div>
                </div>
            </div>
            @endif
            @if(isset($ben['id']))
            <div class="row">
                <div class="col-md-12 text-end mt-3">
                    <a href="#" class="btn btn-outline-two" wire:click.prevent="removeOrganisation({{ $ben['id'] }})">Remove</a>
                </div>
            </div>
            @endif
            <div class="row">
                <div class="col-md-12">
                    <hr />
                </div>
            </div>
        </div>
        @endif
    @endforeach
</div