<div class="__panel order-md-2 order-1">
  {{-- @include('livewire.account.organiser.partials.info') --}}
  <div class="card-box-style h-100 rounded">
    <form wire:submit.prevent="saveInheritance">
      <livewire:account.organiser.partials.steps :enable_save=true />
      <div class="__island">
        @if ($errors->any())
          <div class="alert alert-danger mb-2">
            <span>{{ $errors->first() }}</span>
          </div>
        @endif
        <div class="row h-75">
          <div class="col-md-12 mb-3">
            <div class="d-md-flex justify-content-between align-items-center">
              <h5></h5>
              <a href="#" class="btn btn-outline-two redo"
                wire:click.prevent="redoSection">
                <svg width="16" height="17" viewBox="0 0 16 17"
                  fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path d="M8.00033 4.50065V1.83398L4.66699 5.16732L8.00033
                  8.50065V5.83398C10.207 5.83398 12.0003 7.62732 12.0003
                  9.83398C12.0003 12.0407 10.207 13.834 8.00033 13.834C5.79366
                  13.834 4.00033 12.0407 4.00033 9.83398H2.66699C2.66699 12.7807
                  5.05366 15.1673 8.00033 15.1673C10.947 15.1673 13.3337 12.7807
                  13.3337 9.83398C13.3337 6.88732 10.947 4.50065 8.00033
                  4.50065Z" fill="#CF3D60" />
                </svg>
                Redo section</a>
            </div>
            <p>Do you want to leave particular items or sums of money to
              individuals or organisations?  Select the relevant options in the
              “Specific Inheritance” to begin providing the details.</p>
          </div>
        </div>
        @foreach ($benefiaciaries as $k => $beneficiary)
          <div
            class="row {{ count($benefiaciaries) > 1 ? '__beneficiary' : '' }}">
            <div class="row">
              <div class="col-md-12">
                <div class="mb-3">
                  <label class="form-label">Are your leaving your inheritance to
                    an Organisation or Individual?</label>
                  <select class="form-control" name="beneficiary_type"
                    wire:model="benefiaciaries.{{ $k }}.beneficiary_type">
                    <option value="">Select Option</option>
                    <option value="individual">Individual</option>
                    <option value="organisation">Organisation</option>
                  </select>
                </div>
              </div>
            </div>

            @if ($beneficiary['beneficiary_type'] == 'individual')
              <div class="row">
                <div class="col-md-4">
                  <div class="mb-3">
                    <label class="form-label">First Name<span
                        class="required-input">*</span></label>
                    <input type="text" class="form-control" name="first_name"
                      wire:model.defer="benefiaciaries.{{ $k }}.first_name">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="mb-3">
                    <label class="form-label">Middle Name</label>
                    <input type="text" class="form-control"
                      name="middle_name"
                      wire:model.defer="benefiaciaries.{{ $k }}.middle_name">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="mb-3">
                    <label class="form-label">Surname<span
                        class="required-input">*</span></label>
                    <input type="text" class="form-control" name="surname"
                      wire:model.defer="benefiaciaries.{{ $k }}.surname">
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="row">
                    <div class="col-md-4">
                      <div class="form-check">
                        <input class="form-check-input" type="radio"
                          name="id_type.{{ $k }}" id="dob"
                          value="dob"
                          wire:model="benefiaciaries.{{ $k }}.id_type">
                        <label class="form-check-label" for="dob">
                          Date of Birth
                        </label>
                      </div>
                      <div class="form-check">
                        <input class="form-check-input" type="radio"
                          name="id_type.{{ $k }}" id="id_no"
                          value="rsa_id"
                          wire:model="benefiaciaries.{{ $k }}.id_type">
                        <label class="form-check-label" for="id_no">
                          ID Number
                        </label>
                      </div>
                    </div>
                    <div class="col-md-8">
                      @if ($beneficiary['id_type'] == 'dob')
                        <div class="mb-3">
                          <div class="form-group">
                            <label>Date of Birth<span
                                class="required-input">*</span></label>
                            <input type="date" class="form-control"
                              name="child_date_of_birth" max="2999-12-31"
                              wire:model.defer="benefiaciaries.{{ $k }}.id_number">
                          </div>
                        </div>
                      @else
                        <div class="mb-3">
                          <div class="form-group">
                            <label>ID Number<span
                                class="required-input">*</span></label>
                            <input type="text" class="form-control"
                              name="child_id_number" data-masked=""
                              data-inputmask="'mask': '999999 9999 999'"
                              wire:model.defer="benefiaciaries.{{ $k }}.id_number">
                          </div>
                        </div>
                      @endif
                    </div>
                  </div>
                </div>

                {{-- <div class="col-md-12"> --}}
                {{--   <div class="mb-3"> <label class="form-label">ID Number / Date --}}
                {{--       of Birth</label> <input type="text" class="form-control" --}}
                {{--       name="id_number" --}}
                {{--       wire:model.defer="benefiaciaries.{{ $k }}.id_number"> --}}
                {{--   </div> --}}
                {{-- </div> --}}

                <div class="col-md-12">
                  <div class="mb-3">
                    <label class="form-label">Relation<span
                        class="required-input">*</span></label>
                    <select class="form-control" name="relation"
                      wire:model="benefiaciaries.{{ $k }}.relation">
                      <option value="">Select Option</option>
                      @foreach ($relations as $rel)
                        <option value="{{ $rel }}">
                          {{ $rel }}
                        </option>
                      @endforeach
                    </select>
                  </div>
                </div>
                @if ($beneficiary['relation'] == 'Other')
                  <div class="col-md-12">
                    <div class="mb-3">
                      <label class="form-label">Specify Relation<span
                          class="required-input">*</span></label>
                      <input type="text" class="form-control"
                        name="specify_relation"
                        wire:model.defer="benefiaciaries.{{ $k }}.specify_relation">
                    </div>
                  </div>
                @endif
                <div class="col-md-12">
                  <div class="mb-3">
                    <label class="form-label">Describe the Items /
                      Possessions<span class="required-input">*</span></label>
                    <textarea class="form-control" name="description"
                      wire:model.defer="benefiaciaries.{{ $k }}.description"></textarea>
                    <span class="form-text">
                      For example: "R2000" or "House situated at 12 Church
                      Street,
                      Cape Town"
                    </span>
                  </div>
                </div>
              </div>
            @endif
            @if ($beneficiary['beneficiary_type'] == 'organisation')
              <div class="row">
                <div class="col-md-6">
                  <div class="mb-3">
                    <label class="form-label">Name of Organisation<span
                        class="required-input">*</span></label>
                    <input type="text" class="form-control"
                      name="organisation_name"
                      wire:model.defer="benefiaciaries.{{ $k }}.organisation_name">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="mb-3">
                    <label class="form-label">Company / Trust registration
                      Number</label>
                    <input type="text" class="form-control"
                      name="registration_number"
                      wire:model.defer="benefiaciaries.{{ $k }}.registration_number">
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="mb-3">
                    <label class="form-label">Address</label>
                    <textarea class="form-control" name="address"
                      wire:model.defer="benefiaciaries.{{ $k }}.address"></textarea>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="mb-3">
                    <label class="form-label">City</label>
                    <input type="text" class="form-control" name="city"
                      wire:model.defer="benefiaciaries.{{ $k }}.city">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="mb-3">
                    <label class="form-label">Province<span
                        class="required-input">*</span></label>
                    <select class="form-control" name="province"
                      wire:model.defer="benefiaciaries.{{ $k }}.province">
                      <option value="">Select Option</option>
                      @foreach ($provinces as $pr)
                        <option value="{{ $pr }}">
                          {{ $pr }}
                        </option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="mb-3">
                    <label class="form-label">Organisation Type<span
                        class="required-input">*</span></label>
                    <select class="form-control" name="organisation_type"
                      wire:model="benefiaciaries.{{ $k }}.organisation_type">
                      <option value="">Select Option</option>
                      @foreach ($org_types as $tp)
                        <option value="{{ $tp }}">
                          {{ ucwords($tp) }}
                        </option>
                      @endforeach
                    </select>
                  </div>
                  @if ($beneficiary['organisation_type'] == 'other')
                    <div class="mb-3">
                      <label class="form-label">Specify<span
                          class="required-input">*</span></label>
                      <input type="text" class="form-control"
                        name="organisation_type"
                        wire:model.defer="benefiaciaries.{{ $k }}.organisation_other_type">
                    </div>
                  @endif
                </div>
                <div class="col-md-12">
                  <div class="mb-3">
                    <label class="form-label">Describe the Items /
                      Possessions<span class="required-input">*</span></label>
                    <textarea class="form-control" name="description"
                      wire:model.defer="benefiaciaries.{{ $k }}.description"></textarea>
                    <span class="form-text">
                      For example: "R2000" or "House situated at 12 Church
                      Street,
                      Cape Town"
                    </span>
                  </div>
                </div>
              </div>
            @endif
            @if (isset($beneficiary['id']))
              <div class="col-md-12">
                <a href="#" class="btn btn-outline-two"
                  wire:click.prevent="removeBeneficiary('{{ $beneficiary['beneficiary_type'] }}', {{ $beneficiary['id'] }})">Remove</a>
              </div>
            @endif
          </div>
        @endforeach

        <div class="row">
          <div class="col-md-12 d-flex">
            <div class="ms-auto">
              <a href="#" class="btn btn-outline-two outline"
                wire:click.prevent="addBeneficiary">
                <svg width="16" height="17" viewBox="0 0 16 17"
                  fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path
                    d="M12.6663
                    9.16732H8.66634V13.1673H7.33301V9.16732H3.33301V7.83398H7.33301V3.83398H8.66634V7.83398H12.6663V9.16732Z"
                    fill="#CF3D60" />
                </svg>
                Add Specific Inheritance
              </a>
            </div>
          </div>
        </div>

        <div class="col-md-12 mb-3">
          <hr />
        </div>

        <div class="row">
          <div class="col-md-12 d-flex">
            <div class="">
              {{-- <input type="submit" class="btn btn-red" value="save"> --}}
              <a href="{{ url()->previous() }}" class="btn btn-red outline">
                Back
              </a>
            </div>
            <div class="ms-auto">
              <a href="#" class="btn btn-outline-two"
                wire:click.prevent="saveInheritance('go_to_next')">Next</a>
            </div>
          </div>
        </div>
    </form>

  </div>
  @include('livewire.account.organiser.partials.will_footer')
</div>

<div class="modal" tabindex="-1" id="info-modal">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Specific Inheritance With Remainder
          Inheritance</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal"
          aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <p>Do you want to leave particular items or sums of money to specific
          individuals or organisations?</p>
        <p>Select the relevant options in “Specific Inheritance” to begin
          providing the details.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-red"
          data-bs-dismiss="modal">Continue</button>
      </div>
    </div>
  </div>
</div>
@push('scripts')
  <script>
    $(document).ready(function() {
      var show_modal = '{{ $show_modal }}';
      if (show_modal) {
        {{-- $('#info-modal').modal('show'); --}}
      }

      window.addEventListener('data-updated', event => {
        Swal.fire("Success", event.detail.message, "success");
      });

      window.addEventListener('has-error', event => {
        Swal.fire("Error", event.detail.message, "error");
      });
    });
  </script>
@endpush
</div>
