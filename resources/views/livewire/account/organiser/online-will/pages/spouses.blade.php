<div class="__panel order-md-2 order-1">
  {{-- @include('livewire.account.organiser.partials.info') --}}
  <div class="card-box-style h-100 rounded">
  <form wire:submit.prevent="saveSpouse">
    <livewire:account.organiser.partials.steps :enable_save=true />

    <div class="__island">
      @if ($errors->any())
        @foreach ($errors->all() as $err)
          <div class="alert alert-danger mb-2">
            <span>{{ $err }}</span>
          </div>
        @endforeach
      @endif

        <div class="row h-75"> <div class="col-md-12">

            {{-- <h5> --}}
            {{--   Spouse's Details --}}
            {{--   @if (Auth::user()->gender == 'Male' && --}}
            {{--           (Auth::user()->ethnicity == 'Black' || --}}
            {{--               Auth::user()->ethnicity == 'Indian')) --}}
            {{--     <a href="#" style="color: #1c355e"> --}}
            {{--       <i class="fa fa-info-circle" style="cursor: pointer;" --}}
            {{--         data-bs-toggle="modal" data-bs-target="#spouse-info"></i> --}}
            {{--     </a> --}}
            {{--   @endif --}}
            {{-- </h5> --}}

            <div class="__note mb-3">
              <span class="">
                {{-- TODO: Hard coded fill --}}
                <svg width="20" height="20" viewBox="0 0 20 20"
                  fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path d="M9.16663 12.4993H10.8333V14.166H9.16663V12.4993ZM9.16663
                    5.83268H10.8333V10.8327H9.16663V5.83268ZM9.99163
                    1.66602C5.39163 1.66602 1.66663 5.39935 1.66663
                    9.99935C1.66663 14.5993 5.39163 18.3327 9.99163 18.3327C14.6
                    18.3327 18.3333 14.5993 18.3333 9.99935C18.3333 5.39935 14.6
                    1.66602 9.99163 1.66602ZM9.99996 16.666C6.31663 16.666
                    3.33329 13.6827 3.33329 9.99935C3.33329 6.31602 6.31663
                    3.33268 9.99996 3.33268C13.6833 3.33268 16.6666 6.31602
                    16.6666 9.99935C16.6666 13.6827 13.6833 16.666 9.99996
                    16.666Z" fill="#040A5E" />
                </svg>
                <div>To change these details, please do so in the <a
                    href="{{ url('organiser/personal') }}">Organiser</a></div>
              </span>
            </div>
          </div>
        </div>

        @foreach ($spouses as $k => $sp)
          <div class="row {{ count($spouses) > 1 ? '__spouse' : '' }}">
            @if (count($spouses) > 1)
              <div class="col-md-12 text-end">
                <a href="#" class="btn btn-outline-two __action-button"
                  wire:click.prevent="removeSpouse({{ $k }})">
<svg width="16" height="17" viewBox="0 0 16 17" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M10.6668 6.5V13.1667H5.3335V6.5H10.6668ZM9.66683 2.5H6.3335L5.66683 3.16667H3.3335V4.5H12.6668V3.16667H10.3335L9.66683 2.5ZM12.0002 5.16667H4.00016V13.1667C4.00016 13.9 4.60016 14.5 5.3335 14.5H10.6668C11.4002 14.5 12.0002 13.9 12.0002 13.1667V5.16667Z" fill="#CF3D60"/>
</svg>
                  Remove
                  </a>
              </div>
            @endif
            <div class="col-md-4">
              <div class="mb-3">
                <label class="form-label">First Name<span
                    class="required-input">*</span></label>
                <input type="text" class="form-control" name="first_name"
                  wire:model.defer="spouses.{{ $k }}.first_name">
              </div>
            </div>
            <div class="col-md-4">
              <div class="mb-3">
                <label class="form-label">Middle Name</label>
                <input type="text" class="form-control" name="middle_name"
                  wire:model.defer="spouses.{{ $k }}.middle_name">
              </div>
            </div>
            <div class="col-md-4">
              <div class="mb-3">
                <label class="form-label">Surname<span
                    class="required-input">*</span></label>
                <input type="text" class="form-control" name="surname"
                  wire:model.defer="spouses.{{ $k }}.surname">
              </div>
            </div>
            <div class="col-md-6">
              <div class="mb-3">
                <label class="form-label">ID Type</label>
                <div class="">
                  <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio"
                      name="id_type_{{ $k }}" id="sub_sa_id"
                      value="rsa_id"
                      wire:model="spouses.{{ $k }}.id_type">
                    <label class="form-check-label" for="sub_sa_id">RSA
                      ID</label>
                  </div>
                  <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio"
                      name="id_type_{{ $k }}" id="sub_passport"
                      value="passport"
                      wire:model="spouses.{{ $k }}.id_type">
                    <label class="form-check-label"
                      for="sub_passport">Passport</label>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="mb-3">
                <label class="form-label">
                  @if ($sp['id_type'] == 'rsa_id')
                    ID Number
                  @else
                    Passport Number
                  @endif
                </label>
                <input type="text" class="form-control" name="id_number"
                  wire:model.defer="spouses.{{ $k }}.id_number">
              </div>
            </div>

            <div class="col-md-12">
              <div class="mb-3">
                <div class="form-check">
                  <input class="form-check-input" type="radio"
                    name="comunity_marriage.{{ $k }}"
                    id="in_comunity" value="yes"
                    wire:model="spouses.{{ $k }}.comunity_marriage">
                  <label class="form-check-label" for="in_comunity">
                    In Community
                  </label>
                </div>
                <div class="form-check">
                  <input class="form-check-input" type="radio"
                    name="comunity_marriage.{{ $k }}"
                    id="out_of_community" value="no"
                    wire:model="spouses.{{ $k }}.comunity_marriage">
                  <label class="form-check-label" for="out_of_community">
                    Out of Community
                  </label>
                </div>
              </div>
              @if ($sp['comunity_marriage'] == 'no')
                <div class="mb-3">
                  <div class="form-check">
                    <input class="form-check-input" type="radio"
                      name="accrual.{{ $k }}" id="yes_accrual"
                      value="yes"
                      wire:model.defer="spouses.{{ $k }}.accrual">
                    <label class="form-check-label" for="yes_accrual">
                      With Accrual
                    </label>
                  </div>
                  <div class="form-check">
                    <input class="form-check-input" type="radio"
                      name="accrual.{{ $k }}" id="no_accrual"
                      value="no"
                      wire:model.defer="spouses.{{ $k }}.accrual">
                    <label class="form-check-label" for="no_accrual">
                      Without Accrual
                    </label>
                  </div>
                </div>
              @endif
            </div>
          </div>
        @endforeach

        <div class="col-md-12">
          <hr>
        </div>

        <div class="">
          <div class="col-md-12 d-md-flex">
            <div class="d-grid d-md-block mt-2 gap-2">
            {{--   <input type="submit" class="btn btn-red" value="save"> --}}
      <a href="{{ url()->previous() }}" class="btn btn-red outline">
        Back
      </a>
      </div>
            <div class="d-grid d-md-block ms-auto mt-2 gap-2">
              @if (Auth::user()->gender == 'Male' &&
                      (Auth::user()->ethnicity == 'Black' ||
                          Auth::user()->ethnicity == 'Indian'))
                <a href="#" class="btn btn-outline-two __action-button-outline"
                  wire:click.prevent="addSpouse">
<svg width="16" height="17" viewBox="0 0 16 17" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M12.6668 9.16732H8.66683V13.1673H7.3335V9.16732H3.3335V7.83398H7.3335V3.83398H8.66683V7.83398H12.6668V9.16732Z" fill="#CF3D60"/>
</svg>
                  Add Spouse
                  </a>
              @endif
              <a href="#" class="btn btn-outline-two"
                wire:click.prevent="saveSpouse('go_to_next')">Next</a>
            </div>
          </div>
        </div>
      </form>
    </div>
    @include('livewire.account.organiser.partials.will_footer')
  </div>


  <div class="modal" tabindex="-1" id="spouse-info">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-body">
          <div class="d-flex">
            <h5>Multiple spouses</h5>
            <span class="ms-auto">
              <button type="button" class="btn-close"
                data-bs-dismiss="modal" aria-label="Close"></button>
            </span>
          </div>
          @if (Auth::user()->ethnicity == 'Black')
            <p>In SA law, customary marriages are allowed. A customary marriage
              is a union that is formed in terms of
              indigenous African customary law. This is law that allows a Black
              South African man to have more than one
              wife.</p>
          @else
            <p>In SA Law persons married in terms of Muslim and Hindu religious
              rites are regarded as spouses for
              purposes of intestate succession.</p>
          @endif
        </div>
        <div class="modal-footer">
          <!--<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>-->
          <button type="button" class="btn btn-outline-two"
            data-bs-dismiss="modal">Continue</button>
        </div>
      </div>
    </div>
  </div>

  @push('scripts')
    <script>
      $(document).ready(function() {
        window.addEventListener('data-updated', event => {
          Swal.fire("Success", event.detail.message, "success");
        });
        window.addEventListener('show-spouse-modal', event => {
          $('#spouse-info').modal('show');
        });
      });
    </script>
  @endpush
</div>
