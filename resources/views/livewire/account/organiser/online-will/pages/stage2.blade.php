<div class="__interview">

  <div class="__note mb-3">

    <div style="display: flex; align-items: center; gap: 0.4rem">
      <svg width="20" height="20" viewBox="0 0 20 20" fill="none"
        xmlns="http://www.w3.org/2000/svg">
        <path d="M9.16675 12.4993H10.8334V14.166H9.16675V12.4993ZM9.16675
          5.83268H10.8334V10.8327H9.16675V5.83268ZM9.99175 1.66602C5.39175
          1.66602 1.66675 5.39935 1.66675 9.99935C1.66675 14.5993 5.39175
          18.3327 9.99175 18.3327C14.6001 18.3327 18.3334 14.5993 18.3334
          9.99935C18.3334 5.39935 14.6001 1.66602 9.99175 1.66602ZM10.0001
          16.666C6.31675 16.666 3.33341 13.6827 3.33341 9.99935C3.33341 6.31602
          6.31675 3.33268 10.0001 3.33268C13.6834 3.33268 16.6667 6.31602
          16.6667 9.99935C16.6667 13.6827 13.6834 16.666 10.0001 16.666Z"
          fill="#040A5E" />
      </svg>
      <span>
        Edit previous sections by clicking on the panel left. Remember
        to click <strong>save</strong> before continuing.</span>
    </div>

    <div>
      <svg width="20" height="20" viewBox="0 0 20 20" fill="none"
        xmlns="http://www.w3.org/2000/svg">
        <path d="M15.8332 5.34102L14.6582 4.16602L9.99984 8.82435L5.3415
            4.16602L4.1665 5.34102L8.82484 9.99935L4.1665 14.6577L5.3415
            15.8327L9.99984 11.1743L14.6582 15.8327L15.8332 14.6577L11.1748
            9.99935L15.8332 5.34102Z" fill="#040A5E" />
      </svg>
    </div>

  </div>

  <div class="__panel">

    <livewire:account.organiser.partials.steps />

    <div class="__island">

      <div class="__question">

        {{-- <div class=""> --}}
        {{--   <span class=""> --}}
        {{--     <a href="#" data-toggle="tooltip" data-placement="bottom" --}}
        {{--       title="Video infographics"><img src="{{ asset('img/vid.png') }}" --}}
        {{--         wire:click.prevent="showVid()"></a> --}}
        {{--     <a href="#" data-toggle="tooltip" data-placement="bottom" --}}
        {{--       title="Image infographics"><img src="{{ asset('img/info.png') }}" --}}
        {{--         wire:click.prevent="showInfographics()"></a> --}}
        {{--   </span> --}}
        {{-- </div> --}}

        <h3>{!! $question !!}</h3>
        @if (isset($info['description']))
          <p>{!! $info['description'] !!}</p>
        @endif
        <hr>
        <div class="__answers d-flex justify-content-center gap-2">
          <div class="">
            <a href="#" class="btn btn-red save-option-btns"
              wire:click.prevent="saveOption('yes')">Yes</a>
          </div>
          <div class="">
            <a href="#" class="btn btn-red save-option-btns"
              wire:click.prevent="saveOption('no')">No</a>
          </div>
        </div>

      </div>
    </div>

    <div class="">
      <livewire:account.organiser.partials.questions-footer />
    </div>

  </div>

  <div class="modal" tabindex="-1" id="info-modal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">
            @if (isset($info['title']))
              {{ $info['title'] }}
            @endif
          </h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal"
            aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <p>
            @if (isset($info['description']))
              {!! $info['description'] !!}
            @endif
          </p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-red"
            data-bs-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

  <div class="modal" tabindex="-1" id="images-modal_interview">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-body">
          <div class="text-end"><button type="button" class="btn-close"
              data-bs-dismiss="modal" aria-label="Close"></button></div>
          <div class="my-3">
            <b class="text-blue">Please select Infographics below for helpful
              information</b>
          </div>
          @if (count($stage_images) > 0)
            <div class="list-group">
              @foreach ($stage_images as $img)
                @php
                  $abs_path = $img;
                  $rel_path = substr(
                      $abs_path,
                      strpos($abs_path, 'infographics') + 13,
                  );

                @endphp
                <a href="#" class="list-group-item list-group-item-action"
                  wire:click.prevent="showSingleImage('{{ $rel_path }}')">
                  {{ basename($img, '.jpg') }}
                </a>
              @endforeach
            </div>
          @endif
        </div>
      </div>
    </div>
  </div>

  <div class="modal" tabindex="-1" id="interview-single-image-modal">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-body">
          <div class="text-end"><button type="button" class="btn-close"
              data-bs-dismiss="modal" aria-label="Close"></button></div>
          <img src="" class="img-fluid w-100" id="cur_interview_img">
        </div>
      </div>
    </div>
  </div>

  <div class="modal" tabindex="-1" id="vids-modal-interview">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-body">
          <div class="text-end"><button type="button" class="btn-close"
              data-bs-dismiss="modal" aria-label="Close"></button></div>
          <div class="my-3">
            <b class="text-blue">Please select videos below for helpful
              information</b>
          </div>
          @if (count($stage_vids) > 0)
            <div class="list-group">
              @foreach ($stage_vids as $vid)
                <a href="#" class="list-group-item list-group-item-action"
                  wire:click.prevent='showSingleVid("{{ $vid }}")'>
                  {{ $vid }}
                </a>
              @endforeach
            </div>
          @endif
        </div>
      </div>
    </div>
  </div>

  <div class="modal" tabindex="-1" id="interview-single-vid-modal">
    <div class="modal-dialog h-100">
      <div class="modal-content">
        <div class="modal-body">
          <div class="text-end"><button type="button" class="btn-close"
              data-bs-dismiss="modal" aria-label="Close"></button></div>
          <div class="cur_interviewvid_cont">
            <video controls>
              <source src="" type="video/mp4" id="cur_interview_vid">
              Your browser does not support the video tag.
            </video>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal" tabindex="-1" id="vid-coming-soon">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-body">
          <div class="text-end"><button type="button" class="btn-close"
              data-bs-dismiss="modal" aria-label="Close"></button></div>
          <div class="text-center">
            <h5>Informative video coming soon!</h5>
          </div>
        </div>
      </div>
    </div>
  </div>

  @push('scripts')
    <script>
      $(document).ready(function() {
        window.addEventListener('show-info-modal', event => {
          $("#info-modal").modal("show");
        });

        $(document).ready(function() {
          $("#interview-single-vid-modal").on("hidden.bs.modal",
            function() {
              document.querySelectorAll('video').forEach(vid => vid
                .pause());
            });
        });
      });

      window.addEventListener('show-vid-coming-soon', event => {
        $('#vid-coming-soon').modal('show');
      });

      window.addEventListener('show-interview-single-vid-modal', event => {
        var src = event.detail.name;

        $('.modal').modal('hide');
        $('body').removeClass('modal-open');
        $('.modal-backdrop').remove();

        $('#cur_interview_vid').attr('src', src);
        $(".cur_interviewvid_cont video")[0].load();
        $('#interview-single-vid-modal').modal('show');

      });

      window.addEventListener('show-interview-vids-modal', event => {
        $('#vids-modal-interview').modal('show');
      });

      window.addEventListener('show-interview-images-modal', event => {
        $('#images-modal_interview').modal('show');
      });
      window.addEventListener('show-interview-single-image-modal', event => {
        var src = event.detail.image_path;

        $('.modal').modal('hide');
        $('body').removeClass('modal-open');
        $('.modal-backdrop').remove();

        $("#cur_interview_img").attr('src', src);
        $('#interview-single-image-modal').modal("show");

      });

      {{-- window.onload = function() { --}}
      {{--   var info = '{{ $init_modal }}'; --}}
      {{--   if (info) { --}}
      {{--     $("#info-modal").modal("show"); --}}
      {{--   } --}}
      {{-- } --}}
    </script>
  @endpush
</div>
