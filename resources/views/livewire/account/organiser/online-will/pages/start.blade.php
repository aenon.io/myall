<div class="">
  <div class="">
    <div class="card-box-style rounded">

      <div class="__will-start-heading">
        <div class="mb-3">
          <h3 class="page-title h3-title-style">Online Will</h3>
          <p>Upload your existing Will, or create and manage a new one online.
          </p>
        </div>
        <div class="__note mb-3">
          <span class="">
            {{-- TODO: Hard coded fill --}}
            <svg width="20" height="20" viewBox="0 0 20 20" fill="none"
              xmlns="http://www.w3.org/2000/svg">
              <path d="M9.16663 12.4993H10.8333V14.166H9.16663V12.4993ZM9.16663
                    5.83268H10.8333V10.8327H9.16663V5.83268ZM9.99163
                    1.66602C5.39163 1.66602 1.66663 5.39935 1.66663
                    9.99935C1.66663 14.5993 5.39163 18.3327 9.99163 18.3327C14.6
                    18.3327 18.3333 14.5993 18.3333 9.99935C18.3333 5.39935 14.6
                    1.66602 9.99163 1.66602ZM9.99996 16.666C6.31663 16.666
                    3.33329 13.6827 3.33329 9.99935C3.33329 6.31602 6.31663
                    3.33268 9.99996 3.33268C13.6833 3.33268 16.6666 6.31602
                    16.6666 9.99935C16.6666 13.6827 13.6833 16.666 9.99996
                    16.666Z" fill="#040A5E" />
            </svg>
            <div>
              A signed hardcopy of your Will takes president over all physical
              or online copies. Once signed, please store in a safe place while
              uploading a copy to our Safekeeping section(left) as a backup.
              Consider leaving instruction to where you hardcopy is stored.
            </div>
          </span>
        </div>
      </div>

      <div class="__will-start-options">

        <div class="__will-option">

          <div>
            <img src="{{ asset('img/485d74209a.jpg') }}"
              alt="Create a new valid Will">
          </div>

          <div class="__inner">
            <div class="mb-3">
              <h6>Create a new valid Will</h6>
            </div>

            @if (!$opts)
              <div class="__links">
                <a href="{{ url('organiser/online-will/interview') }}"
                  class="btn btn-gradient btn-lg gradient-1">
                  <svg width="20" height="20" viewBox="0 0 20 20"
                    fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M16.25 2.91602L15 1.66602L13.75 2.91602L12.5
                      1.66602L11.25 2.91602L10 1.66602L8.75 2.91602L7.5
                      1.66602L6.25 2.91602L5 1.66602V13.3327H2.5V15.8327C2.5
                      17.216 3.61667 18.3327 5 18.3327H15C16.3833 18.3327 17.5
                      17.216 17.5 15.8327V1.66602L16.25 2.91602ZM12.5
                      16.666H5C4.54167 16.666 4.16667 16.291 4.16667
                      15.8327V14.9993H12.5V16.666ZM15.8333 15.8327C15.8333
                      16.291 15.4583 16.666 15 16.666C14.5417 16.666 14.1667
                      16.291 14.1667
                      15.8327V13.3327H6.66667V4.16602H15.8333V15.8327Z"
                      fill="white" />
                    <path d="M12.5 5.83268H7.5V7.49935H12.5V5.83268Z"
                      fill="white" />
                    <path d="M15 5.83268H13.3333V7.49935H15V5.83268Z"
                      fill="white" />
                    <path d="M12.5 8.33268H7.5V9.99935H12.5V8.33268Z"
                      fill="white" />
                    <path d="M15 8.33268H13.3333V9.99935H15V8.33268Z"
                      fill="white" />
                  </svg>
                  Create Online Will
                </a>
              </div>
            @else
              <div class="__links">
                <div class="">
                  <a href="{{ url('organiser/online-will/profile') }}"
                    class="btn btn-gradient btn-lg gradient-1">
                    <svg width="21" height="20" viewBox="0 0 21 20"
                      fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path d="M12.2157 7.51667L12.9824 8.28333L5.43236
                        15.8333H4.66569V15.0667L12.2157 7.51667ZM15.2157
                        2.5C15.0074 2.5 14.7907 2.58333 14.6324 2.74167L13.1074
                        4.26667L16.2324 7.39167L17.7574 5.86667C18.0824 5.54167
                        18.0824 5.01667 17.7574 4.69167L15.8074 2.74167C15.6407
                        2.575 15.4324 2.5 15.2157 2.5ZM12.2157 5.15833L2.99902
                        14.375V17.5H6.12402L15.3407 8.28333L12.2157 5.15833Z"
                        fill="white" />
                    </svg>
                    Edit Online Will</a>
                </div>
                <div class="__redo">


                  <button type="button"
                    class="btn btn-gradient btn-lg gradient-1"
                    data-bs-toggle="modal" data-bs-target="#redo-will-modal">
                    <svg width="20" height="20" viewBox="0 0 20 20"
                      fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path
                        d="M10.0002 4.99935V1.66602L5.8335 5.83268L10.0002
                        9.99935V6.66602C12.7585 6.66602 15.0002 8.90768 15.0002
                        11.666C15.0002 14.4243 12.7585 16.666 10.0002
                        16.666C7.24183 16.666 5.00016 14.4243 5.00016
                        11.666H3.3335C3.3335 15.3493 6.31683 18.3327 10.0002
                        18.3327C13.6835 18.3327 16.6668 15.3493 16.6668
                        11.666C16.6668 7.98268 13.6835 4.99935 10.0002 4.99935Z"
                        fill="#CF3D60" />
                    </svg>
                    Redo Online Will
                  </button>

                  {{-- <button type="button" --}}
                  {{--   class="btn btn-gradient btn-lg gradient-1" --}}
                  {{--   data-bs-toggle="modal" --}}
                  {{--   data-bs-target="#confirmationUploadModal"> --}}
                  {{--   Go to Safekeeping --}}
                  {{-- </button> --}}

                </div>
              </div>
            @endif

          </div>
        </div>

        <div class="__will-option">

          <div>
            <img src="{{ asset('img/1cfbb9a7ab.jpg') }}"
              alt="Upload an existing Will">
          </div>

          <div class="__inner">
            <div class="mb-3">
              <h6>Have an existing Will?</h6>
              <p>Upload your existing Will in our Safekeeping section</p>
            </div>
            <div class="__links">
              <div class="__redo">

                {{-- <a href="#" class="btn btn-gradient btn-lg gradient-1" --}}
                {{--   wire:click.prevent="showUploadWillModal"><i --}}
                {{--     class="bx bxs-cloud-upload"></i> Upload</a> --}}
                {{-- <a href="#" id="upload_btn" --}}
                {{--   class="btn btn-gradient btn-lg gradient-1" data-toggle="modal" --}}
                {{--   data-target="#confirmationModal"><i --}}
                {{--     class="bx bxs-cloud-upload"></i> Upload</a> --}}

                <button type="button"
                  class="btn btn-gradient btn-lg gradient-1"
                  data-bs-toggle="modal"
                  data-bs-target="#confirmationUploadModal">
                  <svg width="20" height="20" viewBox="0 0 20 20"
                    fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M10.0002 1.66602L3.3335 4.16602V9.24102C3.3335 13.4493
                      6.17516 17.3743 10.0002 18.3327C13.8252 17.3743 16.6668
                      13.4493 16.6668 9.24102V4.16602L10.0002 1.66602ZM15.0002
                      9.24102C15.0002 12.5743 12.8752 15.6577 10.0002
                      16.5993C7.12516 15.6577 5.00016 12.5827 5.00016
                      9.24102V5.25768L10.0002 3.49102L15.0002
                      5.25768V9.24102ZM7.35016 8.82435L6.16683 9.99935L9.11683
                      12.9493L13.8335 8.23268L12.6585 7.05768L9.12516
                      10.591L7.35016 8.82435Z" fill="#CF3D60" />
                  </svg>
                  Go to Safekeeping
                </button>

                {{-- <label for="upload"> --}}
                {{--   <span class="btn btn-gradient btn-lg gradient-1" --}}
                {{--     aria-hidden="true"><i class="bx bxs-cloud-upload"></i> --}}
                {{--     Upload</span> --}}
                {{--   <input type="file" id="upload" style="display:none" --}}
                {{--     name="will_upload" wire:model="will_upload"> --}}
                {{-- </label> --}}

              </div>

              {{-- @if ($uploaded) --}}
              {{--   <div class="col-md-6 d-grid d-md-block gap-2 text-start"> --}}
              {{--     <a href="{{ url('storage/' . $uploaded->will_url) }}" --}}
              {{--       target="_blank" --}}
              {{--       class="btn btn-gradient btn-lg gradient-1"><i --}}
              {{--         class="bx bxs-folder-open"></i> View Will</a> --}}
              {{--   </div> --}}
              {{-- @endif --}}

            </div>
          </div>
        </div>

      </div>
    </div>
  </div>

  <div class="modal" tabindex="-1" id="redo-will-modal" wire:ignore.self>
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Redo Online Will</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal"
            aria-label="Close"></button>
        </div>
        <div class="modal-body">
          @if ($errors->any())
            <div class="alert alert-danger">
              <span>{{ $errors->first() }}</span>
            </div>
          @endif
          <div>
            <p>By redoing your current Online Will, all current information will
              be
              removed and you will not be able to regain it.</p>
            <p> If you currently
              have
              an existing Will uploaded in Safekeeping, remember to replace it
              with your new valid Will.</p>
          </div>
          {{-- <form wire:submit.prevent="saveUploadWill"> --}}
          {{--   <div class="row"> --}}
          {{--     <div class="col-md-12"> --}}
          {{--       <div class="mb-3"> --}}
          {{--         <label class="form-label">Select File</label> --}}
          {{--         <input type="file" class="form-control" --}}
          {{--           wire:model.defer="will_upload"> --}}
          {{--         <div id="emailHelp" class="form-text">Document must not --}}
          {{--           exceed 5MB.</div> --}}
          {{--       </div> --}}
          {{--     </div> --}}
          {{--     <div class="col-md-12"> --}}
          {{--       <div class="form-check"> --}}
          {{--         <input class="form-check-input" type="checkbox" value="" --}}
          {{--           id="terms" wire:model.defer="terms_and_conditions"> --}}
          {{--         <label class="form-check-label" for="terms"> --}}
          {{--           I have read and accepted <a --}}
          {{--             href="{{ url('docs/MyAll Terms and Conditions.pdf') }}" --}}
          {{--             class="text-red" target="_blank">Terms & Conditions</a> --}}
          {{--         </label> --}}
          {{--       </div> --}}
          {{--     </div> --}}
          {{--   </div> --}}
          {{-- </form> --}}

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn btn-outline"
            data-bs-dismiss="modal">
            Cancel
          </button>
          <button type="button" class="btn btn-red"
            wire:click.prevent="removeWillData">
            Continue
          </button>
        </div>
      </div>
    </div>
  </div>

  <div class="modal" tabindex="-1" id="will-upload-modal" wire:ignore.self>
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Upload Existing Will</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal"
            aria-label="Close"></button>
        </div>
        <div class="modal-body">
          @if ($errors->any())
            <div class="alert alert-danger">
              <span>{{ $errors->first() }}</span>
            </div>
          @endif
          <form wire:submit.prevent="saveUploadWill">
            <div class="row">
              <div class="col-md-12">
                <div class="mb-3">
                  <label class="form-label">Select File</label>
                  <input type="file" class="form-control"
                    wire:model.defer="will_upload">
                  <div id="emailHelp" class="form-text">Document must not
                    exceed 5MB.</div>
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-check">
                  <input class="form-check-input" type="checkbox"
                    value="" id="terms"
                    wire:model.defer="terms_and_conditions">
                  <label class="form-check-label" for="terms">
                    I have read and accepted <a
                      href="{{ url('docs/MyAll Terms and Conditions.pdf') }}"
                      class="text-red" target="_blank">Terms & Conditions</a>
                  </label>
                </div>
              </div>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn btn-outline-two"
            data-bs-dismiss="modal">Close</button>
          <button type="button" class="btn btn-red"
            wire:click.prevent="saveUploadWill">Save</button>
        </div>
      </div>
    </div>
  </div>

  <div class="modal" id="confirmationUploadModal" tabindex="-1"
    role="dialog" aria-labelledby="confirmationUploadModalLabel"
    aria-hidden="true" wire:ignore.self>
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="confirmationUploadModalLabel">Uploading
            Your Will</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal"
            aria-label="Close"></button>
        </div>
        <div class="modal-body">
          @if ($errors->any())
            <div class="alert alert-danger mb-2 text-center">
              <span>{{ $errors->first() }}</span>
            </div>
          @endif
          <p>To upload your existing Will to this system, you must provide
            information in the Safekeeping section about
            the location of your original Will. Your original Will must be
            signed by you and two witnesses in wet ink.
            After your passing and the submission of your Will to the Master's
            office, the Master can only legally
            accept your original signed Will. Copies or certified copies will
            not be considered as valid substitutes.
          </p>
          <div class="mt-3">
            <div class="form-check">
              <input class="form-check-input" type="checkbox" value="accept"
                id="read_and_accept_terms"
                wire:model.defer="read_and_accept_terms">
              <label class="form-check-label" for="read_and_accept_terms">
                I Understand
              </label>
            </div>
          </div>
        </div>
        <div class="modal-footer d-flex justify-content-center">
          <a href="#" class="btn btn-outline-two"
            data-bs-dismiss="modal">Cancel</a>
          <button type="button" class="btn btn-red" id="confirmwillUpload"
            wire:click.prevent="confirmwillUpload">Continue</button>
        </div>
      </div>
    </div>
  </div>

  @push('scripts')
    <script>
      document.addEventListener('DOMContentLoaded', function() {

        window.addEventListener('show-redo-will-modal', function(event) {
          var redoWillModal = document.getElementById(
            'redo-will-modal');
          var bootstrapModal = new bootstrap.Modal(redoWillModal);
          bootstrapModal.show();
        });

        window.addEventListener('show-upload-will-modal', function(event) {
          var willUploadModal = document.getElementById(
            'will-upload-modal');
          var bootstrapModal = new bootstrap.Modal(willUploadModal);
          bootstrapModal.show();
        });

        window.addEventListener('close-modals', function(event) {
          var modals = document.querySelectorAll('.modal');
          modals.forEach(function(modal) {
            var bootstrapModal = bootstrap.Modal.getInstance(modal);
            if (bootstrapModal) {
              bootstrapModal.hide();
            }
          });
        });

        window.addEventListener('data-updated', function(event) {
          Swal.fire("Success", event.detail.message, "success");
        });

        {{-- document.querySelectorAll('.redo_btn').forEach(function(button) { --}}
        {{--   button.addEventListener('click', function(e) { --}}
        {{--     e.preventDefault(); --}}
        {{--     Swal.fire({ --}}
        {{--         title: 'Are you sure?', --}}
        {{--         text: "Your data will be removed and you will not be able to regain it.", --}}
        {{--         icon: 'warning', --}}
        {{--         showCancelButton: true, --}}
        {{--         confirmButtonColor: '#eb0045', --}}
        {{--         cancelButtonColor: '#122545', --}}
        {{--         confirmButtonText: 'Yes, delete my current Online Will!' --}}
        {{--       }) --}}
        {{--       .then((result) => { --}}
        {{--         if (result.isConfirmed) { --}}
        {{--           Livewire.emit('deleteOnlinewill'); --}}
        {{--         } --}}
        {{--       }); --}}
        {{--   }); --}}
        {{-- }); --}}

      });
    </script>

    <script>
      // $('#confirmwillUpload').on('click', function() {
      //     window.location.href = "{{ url('organiser/safekeeping') }}";
      // });
    </script>
  @endpush
</div>
