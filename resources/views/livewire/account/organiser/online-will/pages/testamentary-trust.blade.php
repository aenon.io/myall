<div class="__panel order-md-2 order-1">
  {{-- @include('livewire.account.organiser.partials.info') --}}
  <div class="card-box-style h-100 rounded">
    <form wire:submit.prevent="saveTrustees">
      <livewire:account.organiser.partials.steps :enable_save=true />
      <div class="__island">
        @if ($errors->any())
          @foreach ($errors->all() as $err)
            <div class="alert alert-danger mb-2">
              <span>{{ $err }}</span>
            </div>
          @endforeach
        @endif

        @if (!$show_form)
          <div class="row h-75">
            <div class="col-md-12">
              <h5>Do you wish to create a testamentary trust?</h5>
            </div>
          </div>


          <div class="__trust-options">
            <div>
              <button class="btn btn-red outline"
                wire:click.prevent="showTrustFrom('minor')">Trust for a Minor
                Beneficiary
              </button>
            </div>
            <div>
              <button class="btn btn-red outline"
                wire:click.prevent="showTrustFrom('incapacity')">Trust for an
                Impaired Adult
              </button>
            </div>
            <div>
              <button class="btn btn-red outline"
                wire:click.prevent="showTrustFrom('minor_incapacity')"> Both
                of the above
              </button>
            </div>
            <div>
              <button class="btn btn-red outline"
                wire:click.prevent="removeMenu">Not Interested
              </button>
            </div>
          </div>

          {{-- <div class="row h-75"> --}}
          {{--   <div class="col-md-8 offset-md-2 mb-3 mt-3 text-center"> --}}
          {{--     <p>Do you wish to create a testamentary trust for your minor --}}
          {{--       beneficiary or a person who cannot look after --}}
          {{--       their inheritance.</p> --}}
          {{--   </div> --}}
          {{--   <div class="col-md-12 text-center"> --}}
          {{--     <a href="#" class="btn btn-red" --}}
          {{--       wire:click.prevent="showTrustFrom('minor')">Trust for a Minor --}}
          {{--       Beneficiary</a> --}}
          {{--   </div> --}}
          {{--   <div class="col-md-12 mt-3 text-center"> --}}
          {{--     <a href="#" class="btn btn-red" --}}
          {{--       wire:click.prevent="showTrustFrom('incapacity')">Trust for an --}}
          {{--       impaired Adult beneficiary</a> --}}
          {{--   </div> --}}
          {{--   <div class="col-md-12 mb-3 mt-3 text-center"> --}}
          {{--     <a href="#" class="btn btn-red" --}}
          {{--       wire:click.prevent="showTrustFrom('minor_incapacity')">Both Of --}}
          {{--       The --}}
          {{--       Above</a> --}}
          {{--   </div> --}}
          {{--   <div class="col-md-8 offset-md-2 mb-3 mt-3 text-center"> --}}
          {{--     <p>If you do not wish to create a Testamentary Trust-provision or --}}
          {{--       this does not apply to your --}}
          {{--       circumstances, proceed to click on "Not Interested" below.</p> --}}
          {{--   </div> --}}
          {{--   <div class="col-md-12 text-center"> --}}
          {{--     <a href="#" class="btn btn-outline-two" --}}
          {{--       wire:click.prevent="removeMenu">Not Interested</a> --}}
          {{--   </div> --}}
          {{-- </div> --}}
        @else
          <div class="row">
            <div class="col-md-12 d-flex justify-content-end text-end">
              <a href="#" class="btn btn-outline-two redo"
                wire:click.prevent="clearData()">
                <svg width="16" height="17" viewBox="0 0 16 17"
                  fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path d="M8.00033 4.50065V1.83398L4.66699 5.16732L8.00033
                  8.50065V5.83398C10.207 5.83398 12.0003 7.62732 12.0003
                  9.83398C12.0003 12.0407 10.207 13.834 8.00033 13.834C5.79366
                  13.834 4.00033 12.0407 4.00033 9.83398H2.66699C2.66699 12.7807
                  5.05366 15.1673 8.00033 15.1673C10.947 15.1673 13.3337 12.7807
                  13.3337 9.83398C13.3337 6.88732 10.947 4.50065 8.00033
                  4.50065Z" fill="#CF3D60" />
                </svg>
                Redo this section</a>
            </div>
          </div>
          @if ($beneficiary_type == 'minor')
            @include('livewire.account.organiser.online-will.pages.testamentary-trust.minor')
          @elseif($beneficiary_type == 'incapacity')
            @include('livewire.account.organiser.online-will.pages.testamentary-trust.incapacity')
          @else
            @include('livewire.account.organiser.online-will.pages.testamentary-trust.minor')
            @include('livewire.account.organiser.online-will.pages.testamentary-trust.incapacity')
          @endif

          <div class="row">
            <div class="col-md-12">
              <hr>
            </div>
          </div>

          <div class="row">
            <div class="col-md-12 d-flex">
              <div class="">
                {{-- <input type="submit" class="btn btn-red" value="save"> --}}
                <a href="{{ url()->previous() }}" class="btn btn-red outline">
                  Back
                </a>
              </div>
              <div class="ms-auto">
                <a href="#" class="btn btn-outline-two"
                  wire:click.prevent="saveTrustees('go_to_next')">Next</a>
              </div>
            </div>
          </div>
        @endif
    </form>
  </div>
  @include('livewire.account.organiser.partials.will_footer')
</div>
@push('scripts')
  <script>
    $(document).ready(function() {
      window.addEventListener('data-updated', event => {
        Swal.fire("Success", event.detail.message, "success");
      });
    });
  </script>
@endpush
</div>
