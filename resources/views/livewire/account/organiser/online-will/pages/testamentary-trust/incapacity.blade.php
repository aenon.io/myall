<div class="row mt-3">
  <div class="col-md-12 @if (!$show_form) text-center @endif">
    <h5>Testamentary Trust for impaired adult beneficiaries who cannot look
      after their own finances</h5>
  </div>
  <div class="col-md-12 mb-5">
    <p>Please note that you have chosen to create a Testamentary Trust in your
      Will. This type of trust is specifically designed to provide for the
      administration of inheritance benefits for individuals with a mental or
      physical "disability" as defined in the Income Tax Act. To qualify as a
      "Special Trust Type-A" trust with SARS, the circumstances must meet
      certain criteria, and this type of trust also provides relief from Capital
      Gains Tax.</p>
    <p>The term "disability" refers to a moderate to severe limitation in a
      person's ability to function or perform daily activities due to a
      physical, sensory, communication, intellectual, or mental impairment. This
      disability must be diagnosed by a registered medical practitioner and
      expected to last for more than one year.</p>
    <p>The trustees of this trust have the authority to terminate it if they all
      agree and inform the Master accordingly. By not specifying a termination
      date, the trustees are not required to end the trust on a specific date,
      which allows for flexibility in case of unforeseen circumstances.</p>
    <div class="form-check">
      <input class="form-check-input" type="checkbox" value="yes"
        id="incapacity_testamentary_trust_understand"
        wire:model.defer="incapacity_testamentary_trust_understand">
      <label class="form-check-label"
        for="incapacity_testamentary_trust_understand">
        I Understand
      </label>
    </div>
  </div>
  <div class="col-md-12">
    <div class="row">
      <div class="col-md-12 d-flex align-items-center mb-3">
        <h5>Beneficiary Details</h5>
        <span class="ms-auto">
          <a href="#" class="btn btn-outline-two outline"
            wire:click.prevent="addBeneficiary">
            <svg width="16" height="17" viewBox="0 0 16 17"
              fill="none" xmlns="http://www.w3.org/2000/svg">
              <path
                d="M12.6668 9.16732H8.66683V13.1673H7.3335V9.16732H3.3335V7.83398H7.3335V3.83398H8.66683V7.83398H12.6668V9.16732Z"
                fill="#CF3D60" />
            </svg>
            Add Beneficiary</a>
        </span>
      </div>
      <div class="col-md-12">
        @foreach ($beneficiaries as $k => $ben)
          <div class="row">
            <div class="col-md-4">
              <div class="mb-3">
                <label class="form-label">Name<span
                    class="required-input">*</span></label>
                <input type="text" class="form-control" name="name"
                  wire:model.defer="beneficiaries.{{ $k }}.beneficiary_name">
              </div>
            </div>
            <div class="col-md-4">
              <div class="mb-3">
                <label class="form-label">Middle Name</label>
                <input type="text" class="form-control" name="middle_name"
                  wire:model.defer="beneficiaries.{{ $k }}.beneficiary_middle_name">
              </div>
            </div>
            <div class="col-md-4">
              <div class="mb-3">
                <label class="form-label">Surname<span
                    class="required-input">*</span></label>
                <input type="text" class="form-control" name="surname"
                  wire:model.defer="beneficiaries.{{ $k }}.beneficiary_surname">
              </div>
            </div>
          </div>
        @endforeach
      </div>
      {{-- <div class="col-md-12"> --}}
      {{--   <hr /> --}}
      {{-- </div> --}}
    </div>
  </div>
  @if ($beneficiary_type == 'minor_incapacity')
    @if (!$has_data)
      <div class="col-md-12 mb-3">
        <div class="form-check ms-auto">
          <input class="form-check-input" type="checkbox" value="same_trustees"
            id="same_trustees" wire:model="same_trustees">
          <label class="form-check-label" for="same_trustees">
            Use same Trustees as above (If you want to use the same Trustees,
            first enter all of the above Trustees correctly and then tick off
            this box.)
          </label>
        </div>
      </div>
      {{-- <hr /> --}}
    @endif
  @endif
  <div class="col-md-12">
    @foreach ($incapacity_trustees as $k => $trustee)
      <div class="row __trustee">
        <div class="col-md-12">
          @php
            $number = $k + 1;
            $locale = 'en_US';
            $nf = new \NumberFormatter($locale, \NumberFormatter::ORDINAL);
            $trust_num = $nf->format($number);
          @endphp
          <h5>{{ $trust_num }} Trustee @if ($trust_num == '1st' || $trust_num == '2nd')
              (Compulsory)
            @elseif($trust_num == '3rd')
              (Optional)
            @endif
          </h5>
        </div>
        <div class="col-md-4">
          <div class="mb-3">
            <label class="form-label">First Name<span
                class="required-input">*</span></label>
            <input type="text" class="form-control" name="first_name"
              wire:model.defer="incapacity_trustees.{{ $k }}.first_name">
          </div>
        </div>
        <div class="col-md-4">
          <div class="mb-3">
            <label class="form-label">Middle Name<span
                class="required-input">*</span></label>
            <input type="text" class="form-control" name="middle_name"
              wire:model.defer="incapacity_trustees.{{ $k }}.middle_name">
          </div>
        </div>
        <div class="col-md-4">
          <div class="mb-3">
            <label class="form-label">Surname<span
                class="required-input">*</span></label>
            <input type="text" class="form-control" name="surname"
              wire:model.defer="incapacity_trustees.{{ $k }}.surname">
          </div>
        </div>
        <div class="col-md-6">
          <div class="mb-3">
            <label class="form-label">ID Type<span
                class="required-input">*</span></label>
            <div class="">
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio"
                  name="id_type_{{ $k }}" id="sub_sa_id"
                  value="rsa_id"
                  wire:model="incapacity_trustees.{{ $k }}.id_type">
                <label class="form-check-label" for="sub_sa_id">RSA ID</label>
              </div>
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio"
                  name="id_type_{{ $k }}" id="sub_passport"
                  value="passport"
                  wire:model="incapacity_trustees.{{ $k }}.id_type">
                <label class="form-check-label"
                  for="sub_passport">Passport</label>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="mb-3">
            <label class="form-label">
              @if ($trustee['id_type'] == 'passport')
                Passport Number<span class="required-input">*</span>
              @else
                ID Number<span class="required-input">*</span>
              @endif
            </label>
            <input type="text" class="form-control" name="id_number"
              wire:model.defer="incapacity_trustees.{{ $k }}.id_number">
          </div>
        </div>
        @if ($k == 2 && isset($trustee['id']))
          <div class="col-md-12 text-end">
            <a href="#" class="btn btn-outline-two"
              wire:click.prevent="removeTrustee({{ $trustee['id'] }})">Remove</a>
          </div>
        @endif
      </div>
    @endforeach
    @if (count($incapacity_trustees) < 3)
      <div class="row mb-3">
        <div class="col-md-12 text-end">
          <a href="#" class="btn btn-outline-two outline"
            wire:click.prevent="addIncapacityTrustees()">
            <svg width="16" height="17" viewBox="0 0 16 17"
              fill="none" xmlns="http://www.w3.org/2000/svg">
              <path
                d="M12.6668 9.16732H8.66683V13.1673H7.3335V9.16732H3.3335V7.83398H7.3335V3.83398H8.66683V7.83398H12.6668V9.16732Z"
                fill="#CF3D60" />
            </svg>
            Add Trustee</a>
        </div>
      </div>
    @endif
  </div>
</div>
