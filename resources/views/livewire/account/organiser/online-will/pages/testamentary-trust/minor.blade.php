<div class="row mt-3">
  <div class="col-md-12 @if (!$show_form) text-center @endif">
    <h5>Testamentary Trust for Minor/s Beneficiaries</h5>
  </div>
  <div class="col-md-12 mb-3">
    <p>Please note that you have chosen to create a Testamentary Trust in your
      Will. This applies when a beneficiary of your estate is a minor under 18
      years old at the time of your passing. The trustees will be responsible
      for managing the minor's inheritance. They have the authority to terminate
      the trust if they all agree and inform the Master accordingly. If the
      minor is still under 18, the Master will appoint a curator or tutor to
      administer the minor's inheritance. By not specifying a termination date,
      the trustees are not obligated to end the trust on a specific date, which
      allows for flexibility in unforeseen circumstances.</p>
    <div class="form-check">
      <input class="form-check-input" type="checkbox" value="yes"
        id="minor_testamentary_trust_understand"
        wire:model.defer="minor_testamentary_trust_understand">
      <label class="form-check-label" for="minor_testamentary_trust_understand">
        I Understand
      </label>
    </div>
  </div>
  <div class="col-md-12">
    @foreach ($minor_trustees as $k => $trustee)
      <div class="row __trustee">
        <div class="col-md-12">
          @php
            $number = $k + 1;
            $locale = 'en_US';
            $nf = new \NumberFormatter($locale, \NumberFormatter::ORDINAL);
            $trust_num = $nf->format($number);
          @endphp
          <h5>{{ $trust_num }} Trustee @if ($trust_num == '1st' || $trust_num == '2nd')
              (Compulsory)
            @elseif($trust_num == '3rd')
              (Optional)
            @endif
          </h5>
        </div>
        <div class="col-md-4">
          <div class="mb-3">
            <label class="form-label">First Name<span
                class="required-input">*</span></label>
            <input type="text" class="form-control" name="first_name"
              wire:model.defer="minor_trustees.{{ $k }}.first_name">
          </div>
        </div>
        <div class="col-md-4">
          <div class="mb-3">
            <label class="form-label">Middle Name</label>
            <input type="text" class="form-control" name="middle_name"
              wire:model.defer="minor_trustees.{{ $k }}.middle_name">
          </div>
        </div>
        <div class="col-md-4">
          <div class="mb-3">
            <label class="form-label">Surname<span
                class="required-input">*</span></label>
            <input type="text" class="form-control" name="surname"
              wire:model.defer="minor_trustees.{{ $k }}.surname">
          </div>
        </div>
        <div class="col-md-6">
          <div class="mb-3">
            <label class="form-label">ID Type<span
                class="required-input">*</span></label>
            <div class="">
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio"
                  name="id_type_m_{{ $k }}" id="sub_sa_id"
                  value="rsa_id"
                  wire:model="minor_trustees.{{ $k }}.id_type">
                <label class="form-check-label" for="sub_sa_id">RSA ID</label>
              </div>
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio"
                  name="id_type_m_{{ $k }}" id="sub_passport"
                  value="passport"
                  wire:model="minor_trustees.{{ $k }}.id_type">
                <label class="form-check-label"
                  for="sub_passport">Passport</label>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="mb-3">
            <label class="form-label">
              @if ($trustee['id_type'] == 'passport')
                Passport Number<span class="required-input">*</span>
              @else
                ID Number<span class="required-input">*</span>
              @endif
            </label>
            <input type="text" class="form-control" name="id_number"
              wire:model.defer="minor_trustees.{{ $k }}.id_number">
          </div>
        </div>
        @if ($k == 2 && isset($trustee['id']))
          <div class="col-md-12 text-end">
            <a href="#" class="btn btn-outline-two"
              wire:click.prevent="removeTrustee({{ $trustee['id'] }})">Remove</a>
          </div>
        @endif
      </div>
    @endforeach
    @if (count($minor_trustees) < 3)
      <div class="row mb-3">
        <div class="col-md-12 text-end">
          <a href="#" class="btn btn-outline-two outline"
            wire:click.prevent="addMinorTrustee()">
            <svg width="16" height="17" viewBox="0 0 16 17"
              fill="none" xmlns="http://www.w3.org/2000/svg">
              <path
                d="M12.6668 9.16732H8.66683V13.1673H7.3335V9.16732H3.3335V7.83398H7.3335V3.83398H8.66683V7.83398H12.6668V9.16732Z"
                fill="#CF3D60" />
            </svg>
            Add Trustee</a>
        </div>
      </div>
    @endif
  </div>
</div>
