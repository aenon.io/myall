<div class="__panel order-md-2 order-1">
  {{-- @include('livewire.account.organiser.partials.info') --}}
  <div class="card-box-style h-100 rounded">
    <form wire:submit.prevent="saveWitnesses">
      <livewire:account.organiser.partials.steps :enable_save=true />
      <div class="__island">
        @if ($errors->any())
          @foreach ($errors->all() as $err)
            <div class="alert alert-danger mb-2">
              <span>{{ $err }}</span>
            </div>
          @endforeach
        @endif
        <div class="row h-75">
          {{-- <div class="col-md-12"> --}}
          {{--   <h5>Witnesses</h5> --}}
          {{-- </div> --}}
          <div class="col-md-12">
            @foreach ($witnesses as $k => $witness)
              <div class="row">
                @php
                  $number = $k + 1;
                  $locale = 'en_US';
                  $nf = new \NumberFormatter(
                      $locale,
                      \NumberFormatter::ORDINAL,
                  );
                  $trust_num = $nf->format($number);
                @endphp
                <h5>{{ $trust_num }} Witness</h5>
                <div class="col-md-4">
                  <div class="mb-3">
                    <label class="form-label">First Name<span
                        class="required-input">*</span></label>
                    <input type="text" class="form-control" name="first_name"
                      wire:model.defer="witnesses.{{ $k }}.first_name">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="mb-3">
                    <label class="form-label">Middle Name</label>
                    <input type="text" class="form-control"
                      name="middle_name"
                      wire:model.defer="witnesses.{{ $k }}.middle_name">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="mb-3">
                    <label class="form-label">Surname<span
                        class="required-input">*</span></label>
                    <input type="text" class="form-control" name="surname"
                      wire:model.defer="witnesses.{{ $k }}.surname">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="mb-3">
                    <label class="form-label">ID Type<span
                        class="required-input">*</span></label>
                    <div class="">
                      <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio"
                          name="id_type_{{ $k }}" id="sub_sa_id"
                          value="rsa_id"
                          wire:model="witnesses.{{ $k }}.id_type">
                        <label class="form-check-label" for="sub_sa_id">RSA
                          ID</label>
                      </div>
                      <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio"
                          name="id_type_{{ $k }}" id="sub_passport"
                          value="passport"
                          wire:model="witnesses.{{ $k }}.id_type">
                        <label class="form-check-label"
                          for="sub_passport">Passport</label>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="mb-3">
                    <label class="form-label">
                      @if ($witness['id_type'] == 'passport')
                        Passport Number<span class="required-input">*</span>
                      @else
                        ID Number<span class="required-input">*</span>
                      @endif
                    </label>
                    <input type="text" class="form-control" name="id_number"
                      wire:model.defer="witnesses.{{ $k }}.id_number">
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="mb-3">
                    <label class="form-label">Phone Number</label>
                    <input type="text" class="form-control"
                      name="phone_number" maxlength="13"
                      wire:model.defer="witnesses.{{ $k }}.phone_number">
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="mb-3">
                    <label class="form-label">Address<span
                        class="required-input">*</span></label>
                    <textarea class="form-control" name="address"
                      wire:model.defer="witnesses.{{ $k }}.address"></textarea>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="mb-3">
                    <label class="form-label">City<span
                        class="required-input">*</span></label>
                    <input type="text" class="form-control" name="city"
                      wire:model.defer="witnesses.{{ $k }}.city">
                  </div>
                </div>

                {{-- <div class="col-md-4"> --}}
                {{--   <div class="mb-3"> --}}
                {{--     <label class="form-label">Postal Code</label> --}}
                {{--     <input type="text" class="form-control" --}}
                {{--       name="postal_code" --}}
                {{--       wire:model.defer="witnesses.{{ $k }}.postal_code"> --}}
                {{--   </div> --}}
                {{-- </div> --}}

                <div class="col-md-6">
                  <div class="mb-3">
                    <label class="form-label">Province<span
                        class="required-input">*</span></label>
                    <select class="form-control" name="province"
                      wire:model.defer="witnesses.{{ $k }}.province">
                      <option value="">Select Option</option>
                      @foreach ($provinces as $pr)
                        <option value="{{ $pr }}">
                          {{ $pr }}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                @if (isset($witness['id']))
                  <div class="col-md-12">
                    <a href="#" class="btn btn-outline-two"
                      wire:click.prevent="deleteWitness({{ $witness['id'] }})">Remove</a>
                  </div>
                @endif
                <div class="col-md-12">
                  <hr />
                </div>
              </div>
            @endforeach
            <div class="row">
              <div class="col-md-12 d-flex">
                <span class="">
                  {{-- <input type="submit" class="btn btn-red" value="save"> --}}
                  <a href="{{ url()->previous() }}"
                    class="btn btn-red outline">
                    Back
                  </a>
                </span>
                <span class="ms-auto">
                  @if (count($witnesses) > 0)
                    <a href="#" class="btn btn-outline-two"
                      wire:click.prevent="saveWitnesses('review')">Next</a>
                  @endif
                </span>
              </div>
            </div>
          </div>
        </div>
    </form>
  </div>
  @include('livewire.account.organiser.partials.will_footer')
</div>

<div class="modal" tabindex="-1" id="info-modal">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Witness Details</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal"
          aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <p>The function of the witnesses is to witness you signing the Will,
          thereby confirming that it is your
          signature on the page. Both witnesses must sign the Will in the
          presence of each other and yourself. </p>
        <ul>
          <li>The witnesses must be 14 years or older.</li>
          <li>No witness may benefit from the Will.</li>
          <li>No person who is named as a beneficiary, executor, trustee or
            guardian may sign as a witness.</li>
        </ul>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-red"
          data-bs-dismiss="modal">Continue</button>
      </div>
    </div>
  </div>
</div>

@push('scripts')
  <script>
    $(document).ready(function() {
      var show_modal = '{{ $show_modal }}';
      if (show_modal) {
        $('#info-modal').modal('show');
      }

      window.addEventListener('data-updated', event => {
        Swal.fire("Success", event.detail.message, "success");
      });
    });
  </script>
@endpush

</div>
