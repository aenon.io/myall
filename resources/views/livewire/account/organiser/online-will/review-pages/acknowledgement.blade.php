<div class="card-box-style rounded">
  <livewire:account.organiser.partials.steps />
  <div class="col-md-12 __island">
    @if ($errors->any())
      <div class="alert alert-danger">
        <span>{{ $errors->first() }}</span>
      </div>
    @endif
    <div class="col-md-12">
      <div class="row">
        <div class="col-md-12">
          {{-- <h5>Acknowledgement</h5> --}}
          <p>Ticking off all of these statements means that you understand the
            nature of a Will and that you are legally competent to make a Will.
          </p>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12 mt-3">
          <div class="form-check mb-3">
            <input class="form-check-input" type="checkbox" value="1"
              id="ack_check_one" wire:model.defer="ack_check_one">
            <label class="form-check-label" for="ack_check_one">
              I am 16 years and older.
            </label>
          </div>
          <div class="form-check mb-3">
            <input class="form-check-input" type="checkbox" value="1"
              id="ack_check_two" wire:model.defer="ack_check_two">
            <label class="form-check-label" for="ack_check_two">
              I understand that my original signed Will regulates my deceased
              estate from the date of my death.
            </label>
          </div>
          <div class="form-check mb-3">
            <input class="form-check-input" type="checkbox" value="1"
              id="ack_check_three" wire:model.defer="ack_check_three">
            <label class="form-check-label" for="ack_check_three">
              I understand that under South African Law I have the freedom to
              leave my possessions to whomever I please (as my beneficiaries)
              and that I must name these beneficiaries in my Will.
            </label>
          </div>
          {{--
                    <div class="form-check mb-3">
                        <input class="form-check-input" type="checkbox" value="1" id="ack_check_four" wire:model.defer="ack_check_four">
                        <label class="form-check-label" for="ack_check_four">
                            I understand that the people or organisations I nominate as beneficiaries will inherit my South African possessions.
                        </label>
                    </div>
                    <div class="form-check mb-3">
                        <input class="form-check-input" type="checkbox" value="1" id="ack_check_five" wire:model.defer="ack_check_five">
                        <label class="form-check-label" for="ack_check_five">
                            I understand, that if I disinherit or to a large degree disinherit my spouse, minor biological, and/or legally adopted children, such a person may submit a maintenance claim against my deceased estate.
                        </label>
                    </div>
                    <div class="form-check mb-3">
                        <input class="form-check-input" type="checkbox" value="1" id="ack_check_six" wire:model.defer="ack_check_six">
                        <label class="form-check-label" for="ack_check_six">
                            I have the mental capacity to draft and sign my Will in so far as that I understand the nature and effect of my will at the time of signing of my Will.
                        </label>
                    </div>
                    --}}
          <div class="form-check mb-3">
            <input class="form-check-input" type="checkbox" value="1"
              id="ack_check_seven" wire:model.defer="ack_check_seven">
            <label class="form-check-label" for="ack_check_seven">
              I confirm that I voluntarily and on my own accord completed this
              Online Will application.
            </label>
          </div>
          <div class="form-check mb-3">
            <input class="form-check-input" type="checkbox" value="1"
              id="ack_check_eight" wire:model.defer="ack_check_eight">
            <label class="form-check-label" for="ack_check_eight">
              I understand that at any time after the signing of my Will, I may
              change the details of my Will with another signed and later dated
              Will.
            </label>
          </div>
          <div class="form-check mb-3">
            <input class="form-check-input" type="checkbox" value="1"
              id="ack_check_nine" wire:model.defer="ack_check_nine">
            <label class="form-check-label" for="ack_check_nine">
              I understand that my Will must be written and signed on paper and
              must be original (not a photocopy or scanned copy).
            </label>
          </div>
          <div class="form-check mb-3">
            <input class="form-check-input" type="checkbox" value="1"
              id="ack_check_ten" wire:model.defer="ack_check_ten">
            <label class="form-check-label" for="ack_check_ten">
              I understand that my Will is only valid as an original signed
              paper document and NOT as a video, email, computer file or any
              other electronic document or format.
            </label>
          </div>
          {{--
                    <div class="form-check mb-3">
                        <input class="form-check-input" type="checkbox" value="1" id="ack_check_eleven" wire:model.defer="ack_check_eleven">
                        <label class="form-check-label" for="ack_check_eleven">
                            I undertake to follow the procedures set out in Stages 3 to 7 to ensure that my Will meets the conditions required by the Master of the High Court to accept my Will as legally enforceable.
                        </label>
                    </div>
                    --}}
          <div class="form-check mb-3">
            <input class="form-check-input" type="checkbox" value="1"
              id="ack_check_twelve" wire:model.defer="ack_check_twelve">
            <label class="form-check-label" for="ack_check_twelve">
              I understand that the required witnesses, nor their spouses, for
              the signing of this Will CANNOT be beneficiaries, executors,
              guardians, trustees, or anyone named in my Will.
            </label>
          </div>
          <div class="form-check mb-3">
            <input class="form-check-input" type="checkbox" value="1"
              id="ack_check_thirteen" wire:model.defer="ack_check_thirteen">
            <label class="form-check-label" for="ack_check_thirteen">
              I understand that SA law does not allow a Will to be signed with a
              digital signature. This means a Will still has to be physically
              signed in "wet ink" (where a signature is manually signed, using
              the liquid ink of a pen).
            </label>
          </div>

          {{-- <div class="form-check mb-3"> <input class="form-check-input" --}}
          {{--     type="checkbox" value="1" id="ack_check_fourteen" --}}
          {{--     wire:model.defer="ack_check_fourteen"> --}}
          {{--   <label class="form-check-label" for="ack_check_fourteen"> --}}
          {{--     I can read my drafted Will. --}}
          {{--   </label> --}}
          {{-- </div> --}}

        </div>

        <div class="col-md-12">
          <hr />
        </div>

        <div class="col-md-12 d-flex gap-2">
          <span class="">
            {{-- <input type="submit" class="btn btn-red" value="save"> --}}
            <a href="{{ url()->previous() }}" class="btn btn-red outline">
              Back
            </a>
          </span>
          <span class="ms-auto">
            <a href="" class="btn btn-red"
              wire:click.prevent="checkAknowledgement">Continue</a>
        </div>
      </div>

    </div>
  </div>
  @include('livewire.account.organiser.partials.will_footer')
</div>
<div class="modal" tabindex="-1" id="ack-alert">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12 text-end">
            <button type="button" class="btn-close" data-bs-dismiss="modal"
              aria-label="Close"></button>
          </div>
          <div class="col-md-12">
            <p>Follow the information and procedures set out in Stages 4 to 7
              to ensure that your Will meets the conditions required by the
              Master of the High Court to accept your Will as legally
              enforceable.</p>
          </div>
          <div class="col-md-12 mt-3 text-center">
            <a href="#" class="btn btn-red"
              data-bs-dismiss="modal">Continue</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@push('scripts')
  <script>
    $(document).ready(function() {
      $('#ack-alert').modal('show');
    });
  </script>
@endpush
</div>
