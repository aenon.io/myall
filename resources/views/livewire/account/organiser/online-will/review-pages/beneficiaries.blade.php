<div class="card-box-style rounded">
  <livewire:account.organiser.partials.steps />
  <div class="col-md-12 __island">
    @if ($errors->any())
      <div class="alert alert-danger">
        <span>{{ $errors->first() }}</span>
      </div>
    @endif
    <div class="col-md-12">
      <div class="row">
        {{-- <div class="col-md-12"> --}}
        {{--   <h5>Beneficiary Details</h5> --}}
        {{-- </div> --}}
        @if (Auth::user()->inheritance->count() > 0)
          <div class="col-md-12">
            @foreach (Auth::user()->inheritance as $inheritance)
              <ul class="list-group mb-3">
                <li class="list-group-item active" aria-current="true">
                  <b
                    class="">{{ ucwords(str_replace('-', ' ', $inheritance->type)) }}</b>
                  @if ($inheritance->type == 'simultaneous-death')
                    -
                    {{ ucwords(str_replace('-', ' ', $inheritance->sub_type)) }}
                  @endif
                </li>
                @foreach ($inheritance->individual_beneficiary as $ind)
                  <li class="list-group-item">
                    <div class="d-flex">
                      <span class="text-muted">Beneficiary Type</span>
                      <span class="ms-auto"><b>Individual</b></span>
                    </div>
                    <div class="d-flex">
                      <span class="text-muted">Name</span>
                      <span
                        class="ms-auto"><b>{{ $ind->first_name . ' ' . $ind->middle_name . ' ' . $ind->surname }}</b></span>
                    </div>
                    <div class="d-flex">
                      <span class="text-muted">ID Number / DOB</span>
                      <span class="ms-auto"><b>{{ $ind->id_number }}</b></span>
                    </div>
                    @if ($ind->relation)
                      <div class="d-flex">
                        <span class="text-muted">Relation</span>
                        <span class="ms-auto">
                          <b>
                            @if ($ind->relation == 'Other')
                              {{ $ind->specify_relation }}
                            @else
                              {{ $ind->relation }}
                            @endif
                          </b>
                        </span>
                      </div>
                    @endif
                    @if ($inheritance->division_type)
                      <div class="d-flex">
                        <span class="text-muted">Inheritance Type</span>
                        <span class="ms-auto">
                          <b>
                            @if ($inheritance->division_type == 'one_beneficiery')
                              Inheritance of entire estate
                            @elseif($inheritance->division_type == 'percentage_shares')
                              Inheritance of percentage shares
                              ({{ $ind->percentage . '%' }})
                            @elseif($inheritance->division_type == 'equal_part')
                              Inheritance of equal shares
                            @endif
                          </b>
                        </span>
                      </div>
                    @endif
                    @if ($ind->description)
                      <div class="">
                        <span class="text-muted">Description</span>
                        <div><b>{{ $ind->description }}</b></div>
                      </div>
                    @endif
                  </li>
                @endforeach
                @foreach ($inheritance->organisation_beneficiery as $org)
                  <li class="list-group-item">
                    <div class="d-flex">
                      <span class="text-muted">Beneficiary Type</span>
                      <span class="ms-auto"><b>Organisation</b></span>
                    </div>
                    <div class="d-flex">
                      <span class="text-muted">Organisation Name</span>
                      <span
                        class="ms-auto"><b>{{ $org->organisation_name }}</b></span>
                    </div>
                    @if ($org->registration_number)
                      <div class="d-flex">
                        <span class="text-muted">Registration Number</span>
                        <span
                          class="ms-auto"><b>{{ $org->registration_number }}</b></span>
                      </div>
                    @endif
                    @if ($org->address || $org->city || $org->province)
                      <div class="d-flex">
                        <span class="text-muted">Address</span>
                        <span class="ms-auto">
                          <b>
                            @if ($org->address)
                              {{ $org->address . ', ' }}
                            @endif
                            @if ($org->city)
                              {{ $org->city . ', ' }}
                            @endif
                            @if ($org->province)
                              {{ $org->province }}
                            @endif
                          </b>
                        </span>
                      </div>
                    @endif
                    @if ($org->organisation_type)
                      <div class="d-flex">
                        <span class="text-muted">Organisation Type</span>
                        <span class="ms-auto">
                          <b>
                            @if ($org->organisation_type == 'Other' || $org->organisation_type == 'other')
                              {{ $org->organisation_other_type }}
                            @else
                              {{ ucwords($org->organisation_type) }}
                            @endif
                          </b>
                        </span>
                      </div>
                    @endif
                    @if ($inheritance->division_type)
                      <div class="d-flex">
                        <span class="text-muted">Inheritance Type</span>
                        <span class="ms-auto">
                          <b>
                            @if ($inheritance->division_type == 'one_beneficiery')
                              Inheritance of entire estate
                            @elseif($inheritance->division_type == 'percentage_shares')
                              Inheritance of percentage shares
                              ({{ $org->percentage . '%' }})
                            @elseif($inheritance->division_type == 'equal_part')
                              Inheritance of equal shares
                            @endif
                          </b>
                        </span>
                      </div>
                    @endif
                    @if ($org->description)
                      <div class="">
                        <span class="text-muted">Description</span>
                        <div><b>{{ $org->description }}</b></div>
                      </div>
                    @endif
                  </li>
                @endforeach
              </ul>
            @endforeach
          </div>
        @endif
      </div>
      <div class="row">
        <div class="col-md-12 mt-3">
          <label class="form-label">Please check if the following details are
            correct</label>
          <div class="form-check">
            <input class="form-check-input" type="checkbox" value="1"
              id="inh_name_check" wire:model.defer="inh_name_check">
            <label class="form-check-label" for="inh_name_check">
              Correct spelling of full names and surnames
            </label>
          </div>
          <div class="form-check">
            <input class="form-check-input" type="checkbox" value="1"
              id="inh_id_check" wire:model.defer="inh_id_check">
            <label class="form-check-label" for="inh_id_check">
              Correct ID numbers / Dates of birth
            </label>
          </div>
          <div class="form-check">
            <input class="form-check-input" type="checkbox" value="1"
              id="inh_org_check" wire:model.defer="inh_org_check">
            <label class="form-check-label" for="inh_org_check">
              If applicable, full and proper identification of organisation
              beneficiaries
            </label>
          </div>
          <div class="form-check">
            <input class="form-check-input" type="checkbox" value="1"
              id="inh_allocation_check" wire:model.defer="inh_allocation_check">
            <label class="form-check-label" for="inh_allocation_check">
              Each beneficiary's inheritance is correctly allocated to such
              beneficiary
            </label>
          </div>
        </div>

        <div class="col-md-12">
          <hr />
        </div>

        <div class="col-md-12 d-flex gap-2">
          <span class="">
            {{-- <input type="submit" class="btn btn-red" value="save"> --}}
            <a href="{{ url()->previous() }}" class="btn btn-red outline">
              Back
            </a>
          </span>
          <span class="ms-auto">
            <a href="" class="btn btn-red"
              wire:click.prevent="checkBeneficiaries">
              Next
            </a>
          </span>
        </div>

      </div>
    </div>
  </div>
  @include('livewire.account.organiser.partials.will_footer')
</div>
