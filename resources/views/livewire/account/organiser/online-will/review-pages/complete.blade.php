<div class="card-box-style rounded">
  <livewire:account.organiser.partials.steps />
  <div class="col-md-12 __island">
    @if ($errors->any())
      <div class="alert alert-danger">
        <span>{{ $errors->first() }}</span>
      </div>
    @endif
    <div class="col-md-12">
      <div class="row">
        <div class="col-md-12 text-center">
          <h5>Congratulations</h5>
          <div class="mt-3">
            <p><b>Your MyAll Will is now complete.</b></p>
          </div>
          <div class="mt-3">
            <p>We recommend you upload a copy of your signed Will in our
              Safekeeping section. </p>
          </div>
        </div>
        <div class="col-md-12 d-grid d-md-inline mt-3 gap-2 text-center">
          <a href="{{ url('organiser/safekeeping') }}" class="btn btn-red">
            <svg width="20" height="20" viewBox="0 0 20 20"
              fill="none" xmlns="http://www.w3.org/2000/svg"
              style="vertical-align: text-bottom">
              <path d="M10.0002 1.66602L3.3335 4.16602V9.24102C3.3335 13.4493
                6.17516 17.3743 10.0002 18.3327C13.8252 17.3743 16.6668 13.4493
                16.6668 9.24102V4.16602L10.0002 1.66602ZM15.0002 9.24102C15.0002
                12.5743 12.8752 15.6577 10.0002 16.5993C7.12516 15.6577 5.00016
                12.5827 5.00016 9.24102V5.25768L10.0002 3.49102L15.0002
                5.25768V9.24102ZM7.35016 8.82435L6.16683 9.99935L9.11683
                12.9493L13.8335 8.23268L12.6585 7.05768L9.12516 10.591L7.35016
                8.82435Z" fill="white" />
            </svg>
            Go to Safekeeping
          </a>
        </div>
      </div>
    </div>
    <div class="col-md-12">
    </div>
  </div>
  @include('livewire.account.organiser.partials.will_footer')
</div>
