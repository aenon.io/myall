<div class="card-box-style rounded">
  <livewire:account.organiser.partials.steps />
  <div class="col-md-12 __island">
    @if ($errors->any())
      <div class="alert alert-danger">
        <span>{{ $errors->first() }}</span>
      </div>
    @endif
    <div class="col-md-12">
      <div class="row">
        <div class="col-md-12">
          <h5>Draft Will Complete</h5>
          <div class="row mb-3">
            <div class="mt-3">
              <p>A draft of your Will is now available to be downloaded.
              </p>
            </div>
          </div>
          <div class="__note mb-3">
            <span class="">
              {{-- TODO: Hard coded fill --}}
              <svg width="20" height="20" viewBox="0 0 20 20"
                fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M9.16663 12.4993H10.8333V14.166H9.16663V12.4993ZM9.16663
                    5.83268H10.8333V10.8327H9.16663V5.83268ZM9.99163
                    1.66602C5.39163 1.66602 1.66663 5.39935 1.66663
                    9.99935C1.66663 14.5993 5.39163 18.3327 9.99163 18.3327C14.6
                    18.3327 18.3333 14.5993 18.3333 9.99935C18.3333 5.39935 14.6
                    1.66602 9.99163 1.66602ZM9.99996 16.666C6.31663 16.666
                    3.33329 13.6827 3.33329 9.99935C3.33329 6.31602 6.31663
                    3.33268 9.99996 3.33268C13.6833 3.33268 16.6666 6.31602
                    16.6666 9.99935C16.6666 13.6827 13.6833 16.666 9.99996
                    16.666Z" fill="#040A5E" />
              </svg>
              <div>Consider sending a copy of their downloaded Will to
                user@myall.co.za for system verification. This is not legal
                advice, and your wills confidentiality is respected.</div>
            </span>
          </div>
          <div class="d-grid d-md-inline gap-2">
            <a href="#" class="btn btn-red" id="download-link"
              wire:click.prevent="downloadPdf">
              <svg width="16" height="17" viewBox="0 0 16 17"
                fill="none" xmlns="http://www.w3.org/2000/svg"
                style="vertical-align: text-top;">
                <path d="M12.0001
                  10.4993V12.4993H4.00008V10.4993H2.66675V12.4993C2.66675
                  13.2327 3.26675 13.8327 4.00008 13.8327H12.0001C12.7334
                  13.8327 13.3334 13.2327 13.3334
                  12.4993V10.4993H12.0001ZM11.3334 7.83268L10.3934
                  6.89268L8.66675 8.61268V3.16602H7.33341V8.61268L5.60675
                  6.89268L4.66675 7.83268L8.00008 11.166L11.3334 7.83268Z"
                  fill="#ffffff" />
              </svg>
              Download PDF
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
  @include('livewire.account.organiser.partials.will_footer')
</div>

<script>
  document.getElementById('download-link').addEventListener('click', function(
    event) {
    setTimeout(function() {
      window.location.href = '/organiser/online-will/review/sign-will';
    }, 3000);
  });
</script>
