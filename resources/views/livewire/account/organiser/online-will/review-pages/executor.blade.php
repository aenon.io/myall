<div class="card-box-style rounded">
  <livewire:account.organiser.partials.steps />
  <div class="__island col-md-12">
    @if ($errors->any())
      <div class="alert alert-danger">
        <span>{{ $errors->first() }}</span>
      </div>
    @endif
    <div class="col-md-12">
      <div class="row">
        @if (Auth::user()->executor())

          {{-- <div class="col-md-12"> --}}
          {{--   <h5>Executor Details</h5> --}}
          {{-- </div> --}}

          <div class="col-md-12">
            <ul class="list-group">
              <li class="list-group-item">
                <small>First Name</small><br />
                <b>{{ Auth::user()->executor()->first_name }}</b>
              </li>
              @if (Auth::user()->executor()->middle_name)
                <li class="list-group-item">
                  <small>Middle Name</small><br />
                  <b>{{ Auth::user()->executor()->middle_name }}</b>
                </li>
              @endif
              <li class="list-group-item">
                <small>Surname</small><br />
                <b>{{ Auth::user()->executor()->surname }}</b>
              </li>
              <li class="list-group-item">
                <small>
                  @if (strlen(Auth::user()->executor()->id_number) == 13)
                    ID Number
                  @else
                    Passport Number
                  @endif
                </small><br />
                <b>{{ Auth::user()->executor()->id_number }}</b>
              </li>
              <li class="list-group-item">
                <small>Relation</small><br />
                <b>
                  @if (Auth::user()->executor()->relation == 'Other')
                    {{ Auth::user()->executor()->specify_relation }}
                  @else
                    {{ Auth::user()->executor()->relation }}
                  @endif
                </b>
              </li>
            </ul>
          </div>
        @endif

        @if (Auth::user()->sub_executor())
          <div class="col-md-12 mt-3">
            <h5>Substitute Executor Details</h5>
          </div>
          <div class="col-md-12 mt-3">
            <ul class="list-group">
              <li class="list-group-item">
                <small>First Name</small><br />
                <b>{{ Auth::user()->sub_executor()->first_name }}</b>
              </li>
              @if (Auth::user()->sub_executor()->middle_name)
                <li class="list-group-item">
                  <small>Middle Name</small><br />
                  <b>{{ Auth::user()->sub_executor()->middle_name }}</b>
                </li>
              @endif
              <li class="list-group-item">
                <small>Surname</small><br />
                <b>{{ Auth::user()->sub_executor()->surname }}</b>
              </li>
              <li class="list-group-item">
                <small>
                  @if (strlen(Auth::user()->sub_executor()->id_number) == 13)
                    ID Number
                  @else
                    Passport Number
                  @endif
                </small><br />
                <b>{{ Auth::user()->sub_executor()->id_number }}</b>
              </li>
              <li class="list-group-item">
                <small>Relation</small><br />
                <b>
                  @if (Auth::user()->sub_executor()->relation == 'Other')
                    {{ Auth::user()->sub_executor()->specify_relation }}
                  @else
                    {{ Auth::user()->sub_executor()->relation }}
                  @endif
                </b>
              </li>
            </ul>
          </div>
        @endif
        <div class="col-md-12 mt-3">
          <label class="form-label">Please check if the following details are
            correct</label>
          <div class="form-check">
            <input class="form-check-input" type="checkbox" value="1"
              id="name_check" wire:model.defer="exec_name_check">
            <label class="form-check-label" for="name_check">
              Correct spelling of full name and surname
            </label>
          </div>
          <div class="form-check">
            <input class="form-check-input" type="checkbox" value="1"
              id="id_check" wire:model.defer="exec_id_check">
            <label class="form-check-label" for="id_check">
              Correct identity number
            </label>
          </div>
        </div>

        <div class="col-md-12">
          <hr />
        </div>

        <div class="col-md-12 d-flex gap-2">
          <span class="">
            {{-- <input type="submit" class="btn btn-red" value="save"> --}}
            <a href="{{ url()->previous() }}" class="btn btn-red outline">
              Back
            </a>
          </span>
          <span class="ms-auto">
            <a href="" class="btn btn-red"
              wire:click.prevent="checkExecutors">
              Next
            </a>
          </span>
        </div>

        <div class="col-md-12">
        </div>
      </div>
    </div>
  </div>
  @include('livewire.account.organiser.partials.will_footer')
</div>
