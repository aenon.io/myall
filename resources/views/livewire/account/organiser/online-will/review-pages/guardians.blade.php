<div class="card-box-style rounded">
  <livewire:account.organiser.partials.steps />
  <div class="col-md-12 __island">
    @if ($errors->any())
      <div class="alert alert-danger">
        <span>{{ $errors->first() }}</span>
      </div>
    @endif
    <div class="col-md-12">
      <div class="row">
        {{-- <div class="col-md-12"> --}}
        {{--   <h5>Guardian Details</h5> --}}
        {{-- </div> --}}
        <div class="col-md-12">
          @if (Auth::user()->children->count() > 0)
            @foreach (Auth::user()->children as $child)
              @if ($child->guardian)
                @if ($child->guardian->guardian)
                  <ul class="list-group mt-3">
                    <li class="list-group-item">
                      <small>Child First Name</small><br />
                      <b>{{ $child->child_name }}</b>
                    </li>
                    @if ($child->middle_name)
                      <li class="list-group-item">
                        <small>Child Middle Name</small><br />
                        <b>{{ $child->middle_name }}</b>
                      </li>
                    @endif
                    <li class="list-group-item">
                      <small>Child Surname</small><br />
                      <b>{{ $child->child_surname }}</b>
                    </li>
                    <li class="list-group-item">
                      <small>Child ID Number / Date of Birth</small><br />
                      <b>
                        @if ($child->child_id_number)
                          {{ $child->child_id_number }}
                        @else
                          {{ $child->child_date_of_birth }}
                        @endif
                      </b>
                    </li>
                    <li class="list-group-item">
                      <small>Guardian First Name</small><br />
                      <b>{{ $child->guardian->guardian->first_name }}</b>
                    </li>
                    @if ($child->guardian->guardian->middle_name)
                      <li class="list-group-item">
                        <small>Guardian Middle Name</small><br />
                        <b>{{ $child->guardian->guardian->middle_name }}</b>
                      </li>
                    @endif
                    <li class="list-group-item">
                      <small>Guardian Surname</small><br />
                      <b>{{ $child->guardian->guardian->surname }}</b>
                    </li>
                    <li class="list-group-item">
                      <small>
                        @if ($child->guardian->guardian->id_number)
                          Guardian ID Number
                        @else
                          Guardian Passport Number
                        @endif
                      </small><br />
                      <b>
                        @if ($child->guardian->guardian->id_number)
                          {{ $child->guardian->guardian->id_number }}
                        @else
                          {{ $child->guardian->guardian->passport_number }}
                        @endif
                      </b>
                    </li>
                  </ul>
                @endif
              @endif
            @endforeach
          @endif
        </div>
      </div>
      <div class="row">
        <div class="col-md-12 mt-3">
          <label class="form-label">Please check if the following details are
            correct</label>
          <div class="form-check">
            <input class="form-check-input" type="checkbox" value="1"
              id="wit_name_check" wire:model.defer="gud_name_check">
            <label class="form-check-label" for="wit_name_check">
              Correct spelling of full names and surnames
            </label>
          </div>
          <div class="form-check">
            <input class="form-check-input" type="checkbox" value="1"
              id="wit_id_check" wire:model.defer="gud_id_check">
            <label class="form-check-label" for="wit_id_check">
              Correct ID numbers or date of birth of minor and guardian
            </label>
          </div>
        </div>

        <div class="col-md-12">
          <hr />
        </div>

        <div class="col-md-12 d-flex gap-2">
          <span class="">
            {{-- <input type="submit" class="btn btn-red" value="save"> --}}
            <a href="{{ url()->previous() }}" class="btn btn-red outline">
              Back
            </a>
          </span>
          <span class="ms-auto">
            <a href="" class="btn btn-red"
              wire:click.prevent="checkGuardians">
              Next
            </a>
          </span>
        </div>

      </div>
    </div>
  </div>
  @include('livewire.account.organiser.partials.will_footer')
</div>
