<div class="card-box-style rounded">
  <livewire:account.organiser.partials.steps />
  <div class="col-md-12 __island">
    @if ($errors->any())
      <div class="alert alert-danger">
        <span>{{ $errors->first() }}</span>
      </div>
    @endif
    <div class="col-md-12">
      <div class="row">
        <div class="col-md-12">
          {{-- <h5>Stage 5</h5> --}}
          <div class="">
            <div class="mt-3">
              <p><b>Before proceeding with the signing of the Will, please
                  confirm that you have understood the following:</b></p>
            </div>
          </div>
        </div>
        <div class="col-md-12">
          <div class="form-check mt-3">
            <input class="form-check-input" type="checkbox" value=""
              id="check_all_pages_printed"
              wire:model.defer="check_all_pages_printed">
            <label class="form-check-label" for="check_all_pages_printed">
              Did you check that each page is printed out and following the
              numerical sequence, e.g. page 1, 2, 3, 4?
            </label>
          </div>
          <div class="form-check mt-3">
            <input class="form-check-input" type="checkbox" value=""
              id="check_will_read" wire:model.defer="check_will_read">
            <label class="form-check-label" for="check_will_read">
              Did you read through the Will and ensure that all the details are
              correct and that your Will reflects your wishes and instructions?
            </label>
          </div>
        </div>

        <div class="col-md-12">
          <hr />
        </div>

        <div class="col-md-12 d-flex gap-2">
          <span class="">
            <a href="{{ url()->previous() }}" class="btn btn-red outline">
              Back
            </a>
          </span>
          <span class="ms-auto">
            <a href="" class="btn btn-red"
              wire:click.prevent="checkStage5">
              Next
            </a>
          </span>
        </div>

      </div>
    </div>
  </div>
  @include('livewire.account.organiser.partials.will_footer')
</div>
