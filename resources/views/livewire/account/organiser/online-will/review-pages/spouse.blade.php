<div class="card-box-style rounded">
  <livewire:account.organiser.partials.steps />
  <div class="col-md-12 __island">
    @if ($errors->any())
      <div class="alert alert-danger">
        <span>{{ $errors->first() }}</span>
      </div>
    @endif
    <div class="col-md-12">
      <div class="row">

        {{-- <div class="col-md-12"> --}}
        {{--   <h5>Spouse Details</h5> --}}
        {{-- </div> --}}

        <div class="col-md-12 ms-1">
          @if (Auth::user()->spouses->count() > 0)
            @php
              $sps = Auth::user()
                  ->spouses()
                  ->where('marital_status', 'Now Married')
                  ->get();
            @endphp
            @foreach ($sps as $sp)
              <ul class="list-group">
                <li class="list-group-item">
                  <small>First Name</small><br />
                  <b>{{ $sp->spouse_name }}</b>
                </li>
                <li class="list-group-item">
                  <small>Middle Name</small><br />
                  <b>{{ $sp->spouse_middle_name }}</b>
                </li>
                <li class="list-group-item">
                  <small>Surname</small><br />
                  <b>{{ $sp->spouse_surname }}</b>
                </li>
                <li class="list-group-item">
                  <small>ID Number</small><br />
                  <b>{{ $sp->spouse_id_number }}</b>
                </li>
                <li class="list-group-item">
                  <small>In Community</small><br />
                  <b>{{ ucwords($sp->comunity_marriage) }}</b>
                </li>
                @if ($sp->accrual)
                  <li class="list-group-item">
                    <small>With Accrual</small><br />
                    <b>{{ ucwords($sp->accrual) }}</b>
                  </li>
                @endif
              </ul>
            @endforeach
          @endif
        </div>
        <div class="col-md-12 mt-3">
          <label class="form-label">Please check if the following details are
            correct</label>
          <div class="form-check">
            <input class="form-check-input" type="checkbox" value="1"
              id="name_check" wire:model.defer="sp_name_check">
            <label class="form-check-label" for="name_check">
              Correct spelling of full name and surname
            </label>
          </div>
          <div class="form-check">
            <input class="form-check-input" type="checkbox" value="1"
              id="id_check" wire:model.defer="sp_id_check">
            <label class="form-check-label" for="id_check">
              Correct identity number
            </label>
          </div>
        </div>

        <div class="col-md-12">
          <hr />
        </div>

        <div class="col-md-12 d-flex gap-2">
          <span class="">
            {{-- <input type="submit" class="btn btn-red" value="save"> --}}
            <a href="{{ url()->previous() }}" class="btn btn-red outline">
              Back
            </a>
          </span>
          <span class="ms-auto">
            <a href="" class="btn btn-red"
              wire:click.prevent="checkSpouse">
              Next
            </a>
          </span>
        </div>

      </div>
    </div>
  </div>
  @include('livewire.account.organiser.partials.will_footer')
</div>
