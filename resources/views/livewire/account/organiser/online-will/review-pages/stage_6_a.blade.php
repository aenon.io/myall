<div class="card-box-style rounded">
  <livewire:account.organiser.partials.steps />
  <div class="col-md-12 __island"
    @if ($errors->any()) <div class="alert alert-danger">
                <span>{{ $errors->first() }}</span>
            </div> @endif
    <div class="col-md-12">
    <div class="row">
      <div class="col-md-12">
        {{-- <h5>Stage 6</h5> --}}
        <p>Once you have signed the Will, please confirm that you have
          understood the following:</p>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12 mt-3">
        <div class="form-check mb-3">
          <input class="form-check-input" type="checkbox" value="1"
            id="check_prev_revoke" wire:model.defer="check_prev_revoke">
          <label class="form-check-label" for="check_prev_revoke">
            Your most recent valid signed Will revokes all your previous Wills.
          </label>
        </div>
        <div class="form-check mb-3">
          <input class="form-check-input" type="checkbox" value="1"
            id="check_valid_date" wire:model.defer="check_valid_date">
          <label class="form-check-label" for="check_valid_date">
            The date of a valid signed Will indicates which valid Will is your
            most recent Will.
          </label>
        </div>
        <div class="form-check mb-3">
          <input class="form-check-input" type="checkbox" value="1"
            id="check_sig_conf" wire:model.defer="check_sig_conf">
          <label class="form-check-label" for="check_sig_conf">
            Your signature confirms that the contents of your Will reflect your
            wishes and intentions.
          </label>
        </div>
        <div class="form-check mb-3">
          <input class="form-check-input" type="checkbox" value="1"
            id="check_witness_presen" wire:model.defer="check_witness_presen">
          <label class="form-check-label" for="check_witness_presen">
            The two witnesses only serve to confirm that you have signed the
            Will in their presence.
          </label>
        </div>
        <div class="form-check mb-3">
          <input class="form-check-input" type="checkbox" value="1"
            id="check_witness_irrel" wire:model.defer="check_witness_irrel">
          <label class="form-check-label" for="check_witness_irrel">
            The content of your Will is irrelevant for the witnesses.
          </label>
        </div>
      </div>

      <div class="col-md-12">
        <hr />
      </div>

      <div class="col-md-12 d-flex gap-2">
        <span class="">
          <a href="{{ url()->previous() }}" class="btn btn-red outline">
            Back
          </a>
        </span>
        <span class="ms-auto">
          <a href="" class="btn btn-red"
            wire:click.prevent="checkStage6A">
            Next
          </a>
        </span>
      </div>

    </div>
  </div>
  @include('livewire.account.organiser.partials.will_footer')
</div>
</div>
