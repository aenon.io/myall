<div class="card-box-style rounded">
  <livewire:account.organiser.partials.steps />
  <div class="col-md-12 __island">
    @if ($errors->any())
      <div class="alert alert-danger">
        <span>{{ $errors->first() }}</span>
      </div>
    @endif
    <div class="col-md-12">
      <div class="row">
        <div class="col-md-12">
          {{-- <h5>Stage 6</h5> --}}
          <p>Once you have signed the Will, please confirm that you have
            understood the following:</p>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12 mt-3">
          <div class="form-check mb-3">
            <input class="form-check-input" type="checkbox" value="1"
              id="check_terms_read" wire:model.defer="check_terms_read">
            <label class="form-check-label" for="check_terms_read">
              Did you read the full Terms and Conditions, as well as the Legal
              Disclaimer of the Online Will Service before signing the Will?
            </label>
          </div>
          <div class="form-check mb-3">
            <input class="form-check-input" type="checkbox" value="1"
              id="check_two_coppies" wire:model.defer="check_two_coppies">
            <label class="form-check-label" for="check_two_coppies">
              Make at least two copies of the Will to be signed.
            </label>
          </div>
          <div class="form-check mb-3">
            <input class="form-check-input" type="checkbox" value="1"
              id="check_sign_with_witness"
              wire:model.defer="check_sign_with_witness">
            <label class="form-check-label" for="check_sign_with_witness">
              Ensure that you sign your Will in the presence of the identified
              two witnesses.
            </label>
          </div>
          <div class="form-check mb-3">
            <input class="form-check-input" type="checkbox" value="1"
              id="check_witness_sign_together"
              wire:model.defer="check_witness_sign_together">
            <label class="form-check-label" for="check_witness_sign_together">
              Ensure that the same two witnesses sign your Will in your
              presence.
            </label>
          </div>
          <div class="form-check mb-3">
            <input class="form-check-input" type="checkbox" value="1"
              id="check_sign_place_date"
              wire:model.defer="check_sign_place_date">
            <label class="form-check-label" for="check_sign_place_date">
              Write down the full date {day, month and year} and the name of the
              town or city where you signed your Will: e.g 12 January 2021 at
              Cape Town.
            </label>
          </div>
        </div>

        <div class="col-md-12">
          <hr />
        </div>

        <div class="col-md-12 d-flex gap-2">
          <span class="">
            <a href="{{ url()->previous() }}" class="btn btn-red outline">
              Back
            </a>
          </span>
          <span class="ms-auto">
            <a href="" class="btn btn-red"
              wire:click.prevent="checkStage6B">
              Next
            </a>
          </span>
        </div>

      </div>
    </div>
  </div>
  @include('livewire.account.organiser.partials.will_footer')
</div>
