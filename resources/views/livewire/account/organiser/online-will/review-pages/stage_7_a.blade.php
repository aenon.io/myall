<div class="card-box-style rounded">
  <livewire:account.organiser.partials.steps />
  <div class="col-md-12 __island">
    @if ($errors->any())
      <div class="alert alert-danger">
        <span>{{ $errors->first() }}</span>
      </div>
    @endif
    <div class="col-md-12">
      <div class="row">
        <div class="col-md-12">
          {{-- <h5>Stage 7</h5> --}}
          <p>Once your Will is printed and signed, please confirm that you have
            understood and checked the following:</p>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12 mt-3">
          <div class="form-check mb-3">
            <input class="form-check-input" type="checkbox" value="1"
              id="check_revoke_cur_will"
              wire:model.defer="check_revoke_cur_will">
            <label class="form-check-label" for="check_revoke_cur_will">
              You may revoke your signed Will by explicitly mentioning such
              revocation in a later dated original signed Will or by destroying
              all ORIGINAL copies of such a Will.
            </label>
          </div>
          <div class="form-check mb-3">
            <input class="form-check-input" type="checkbox" value="1"
              id="check_destroy_types" wire:model.defer="check_destroy_types">
            <label class="form-check-label" for="check_destroy_types">
              Destroying your Will means tearing it up, burning it, or cutting
              it into pieces.
            </label>
          </div>
          <div class="form-check mb-3">
            <input class="form-check-input" type="checkbox" value="1"
              id="check_will_storage" wire:model.defer="check_will_storage">
            <label class="form-check-label" for="check_will_storage">
              Did you put your original signed Will in a safe place?
            </label>
          </div>
          <div class="form-check mb-3">
            <input class="form-check-input" type="checkbox" value="1"
              id="check_notify_family" wire:model.defer="check_notify_family">
            <label class="form-check-label" for="check_notify_family">
              Have you told your spouse, family members or beneficiaries where
              they can find your Will?
            </label>
          </div>
        </div>

        <div class="col-md-12">
          <hr />
        </div>

        <div class="col-md-12 d-flex gap-2">
          <span class="">
            <a href="{{ url()->previous() }}" class="btn btn-red outline">
              Back
            </a>
          </span>
          <span class="ms-auto">
            <a href="" class="btn btn-red"
              wire:click.prevent="checkStage7A">
              Next
            </a>
          </span>
        </div>

      </div>
    </div>
  </div>
  @include('livewire.account.organiser.partials.will_footer')
</div>
