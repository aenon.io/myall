<div class="card-box-style h-100 rounded">
  <livewire:account.organiser.partials.steps />
  <div class="col-md-12 h-100 mt-5">
    @if ($errors->any())
      <div class="alert alert-danger">
        <span>{{ $errors->first() }}</span>
      </div>
    @endif
    <div class="col-md-12 __stage-3 __island text-center">
      <div class="row">
        <div class="col-md-12">
          <h3>Stage 3</h3>
        </div>
        <div class="col-md-8 offset-md-2 mt-3">
          <p>Please click continue to review details and confirm all information
            supplied is correct.</p>
        </div>
        <div class="col-md-12 d-grid d-md-inline mt-5 gap-2">
          <a href="{{ url('organiser/online-will/review/executor') }}"
            class="btn btn-red">
            Continue
          </a>
        </div>
      </div>
    </div>
    <div class="col-md-12">
    </div>
  </div>
  @include('livewire.account.organiser.partials.will_footer')
</div>
