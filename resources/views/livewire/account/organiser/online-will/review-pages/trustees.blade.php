<div class="card-box-style rounded">
  <livewire:account.organiser.partials.steps />
  <div class="col-md-12 __island">
    @if ($errors->any())
      <div class="alert alert-danger">
        <span>{{ $errors->first() }}</span>
      </div>
    @endif
    <div class="col-md-12">
      <div class="row">
        {{-- <div class="col-md-12"> --}}
        {{--   <h5>Trustee of the Testamentary Trust details</h5> --}}
        {{-- </div> --}}
        <div class="col-md-12">
          @if (Auth::user()->trusts->count() > 0)
            @foreach (Auth::user()->trusts as $trust)
              <ul class="list-group mb-3">
                <li class="list-group-item active" aria-current="true">
                  @if ($trust->trust_type == 'minor-children')
                    Testamentary Trust for minor/s beneficiaries
                  @else
                    Testamentary Trust for persons who cannot take care of their
                    inheritance
                  @endif
                </li>
                @if ($trust->trust_type == 'minor-children')
                  @php
                    $count = 1;
                  @endphp
                  @foreach ($trust->trustees as $trustee)
                    <li class="list-group-item">
                      @php
                        $locale = 'en_US';
                        $nf = new \NumberFormatter(
                            $locale,
                            \NumberFormatter::ORDINAL,
                        );
                        $trust_num = $nf->format($count);
                        $count++;
                      @endphp
                      <b>{{ $trust_num }} Trustee</b>
                      <div class="d-flex">
                        Name: <span
                          class="ms-auto">{{ $trustee->first_name }}</span>
                      </div>
                      @if ($trustee->middle_name)
                        <div class="d-flex">
                          Middle Name: <span
                            class="ms-auto">{{ $trustee->middle_name }}</span>
                        </div>
                      @endif
                      <div class="d-flex">
                        Surname: <span
                          class="ms-auto">{{ $trustee->surname }}</span>
                      </div>
                      <div class="d-flex">
                        <span>
                          @if (strlen($trustee->id_number) == 13)
                            ID Number:
                          @else
                            Passport Number:
                          @endif
                        </span>
                        <span class="ms-auto">{{ $trustee->id_number }}</span>
                      </div>
                    </li>
                  @endforeach
                @endif
                @if ($trust->trust_type == 'incapacity')
                  <li class="list-group-item list-group-item-dark">Beneficiaries
                  </li>
                  @foreach ($trust->beneficiaries as $ben)
                    <li class="list-group-item">
                      <div class="d-flex">
                        Name: <span class="ms-auto">{{ $ben->name }}</span>
                      </div>
                      @if ($ben->middle_name)
                        <div class="d-flex">
                          Middle Name: <span
                            class="ms-auto">{{ $ben->middle_name }}</span>
                        </div>
                      @endif
                      <div class="d-flex">
                        Surname: <span
                          class="ms-auto">{{ $ben->surname }}</span>
                      </div>
                    </li>
                  @endforeach
                  <li class="list-group-item list-group-item-dark">Trustees</li>
                  @php
                    $count = 1;
                  @endphp
                  @foreach ($trust->trustees as $trustee)
                    <li class="list-group-item">
                      @php
                        $locale = 'en_US';
                        $nf = new \NumberFormatter(
                            $locale,
                            \NumberFormatter::ORDINAL,
                        );
                        $trust_num = $nf->format($count);
                        $count++;
                      @endphp
                      <b>{{ $trust_num }} Trustee</b>
                      <div class="d-flex">
                        Name: <span
                          class="ms-auto">{{ $trustee->first_name }}</span>
                      </div>
                      @if ($trustee->middle_name)
                        <div class="d-flex">
                          Middle Name: <span
                            class="ms-auto">{{ $trustee->middle_name }}</span>
                        </div>
                      @endif
                      <div class="d-flex">
                        Surname: <span
                          class="ms-auto">{{ $trustee->surname }}</span>
                      </div>
                      <div class="d-flex">
                        @if (strlen($trustee->id_number) == 13)
                          ID Number:
                        @else
                          Passport Number:
                        @endif
                        <span class="ms-auto">{{ $trustee->id_number }}</span>
                      </div>
                    </li>
                  @endforeach
                @endif
              </ul>
            @endforeach
          @endif
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <label class="form-label">Please check if the details bellow are
            correct</label>
          <div class="form-check">
            <input class="form-check-input" type="checkbox" value="1"
              id="wit_name_check" wire:model.defer="trs_name_check">
            <label class="form-check-label" for="wit_name_check">
              Correct spelling of full names and surnames
            </label>
          </div>
          <div class="form-check">
            <input class="form-check-input" type="checkbox" value="1"
              id="wit_id_check" wire:model.defer="trs_id_check">
            <label class="form-check-label" for="wit_id_check">
              Correct ID numbers of each trustee
            </label>
          </div>
        </div>


        <div class="col-md-12">
          <hr />
        </div>

        <div class="col-md-12 d-flex gap-2">
          <span class="">
            {{-- <input type="submit" class="btn btn-red" value="save"> --}}
            <a href="{{ url()->previous() }}" class="btn btn-red outline">
              Back
            </a>
          </span>
          <span class="ms-auto">
            <a href="" class="btn btn-red"
              wire:click.prevent="checkTrustees">
              Next
            </a>
          </span>
        </div>

      </div>
    </div>
  </div>
  @include('livewire.account.organiser.partials.will_footer')
</div>
