<div class="card-box-style rounded">
  <livewire:account.organiser.partials.steps />
  <div class="col-md-12 __island">
    @if ($errors->any())
      <div class="alert alert-danger">
        <span>{{ $errors->first() }}</span>
      </div>
    @endif
    <div class="col-md-12">
      <div class="row">
        {{-- <div class="col-md-12"> --}}
        {{--   <h5>Witness Details</h5> --}}
        {{-- </div> --}}
        <div class="col-md-12">
          @if (Auth::user()->witnesses->count() > 0)
            @php
              $count = 1;
            @endphp
            @foreach (Auth::user()->witnesses as $witness)
              <ul class="list-group mt-3">
                <li class="list-group-item">
                  @php
                    $locale = 'en_US';
                    $nf = new \NumberFormatter(
                        $locale,
                        \NumberFormatter::ORDINAL,
                    );
                    $trust_num = $nf->format($count);
                    $count++;
                  @endphp
                  <small>Witness</small><br />
                  <b>{{ $trust_num }} Witness</b>
                </li>
                <li class="list-group-item">
                  <small>First Name</small><br />
                  <b>{{ $witness->first_name }}</b>
                </li>
                @if ($witness->middle_name)
                  <li class="list-group-item">
                    <small>Middle Name</small><br />
                    <b>{{ $witness->middle_name }}</b>
                  </li>
                @endif
                <li class="list-group-item">
                  <small>Surname</small><br />
                  <b>{{ $witness->surname }}</b>
                </li>
                <li class="list-group-item">
                  <small>
                    @if (strlen($witness->id_number) == 13)
                      ID Number
                    @else
                      Passport Number
                    @endif
                  </small><br />
                  <b>{{ $witness->id_number }}</b>
                </li>
                <li class="list-group-item">
                  <small>Phone</small><br />
                  <b>{{ $witness->phone_number }}</b>
                </li>
                <li class="list-group-item">
                  <small>Address</small><br />
                  <b>{{ $witness->address }}</b>
                </li>
                <li class="list-group-item">
                  <small>City</small><br />
                  <b>{{ $witness->city }}</b>
                </li>
                @if ($witness->postal_code)
                  <li class="list-group-item">
                    <small>Postal Code</small><br />
                    <b>{{ $witness->postal_code }}</b>
                  </li>
                @endif
                <li class="list-group-item">
                  <small>Province</small><br />
                  <b>{{ $witness->province }}</b>
                </li>
              </ul>
            @endforeach
          @endif
        </div>
      </div>
      <div class="row">
        <div class="col-md-12 mt-3">
          <label class="form-label">Please check if the details below are
            correct</label>
          <div class="form-check">
            <input class="form-check-input" type="checkbox" value="1"
              id="wit_name_check" wire:model.defer="wit_name_check">
            <label class="form-check-label" for="wit_name_check">
              Correct spelling of full names and surnames
            </label>
          </div>
          <div class="form-check">
            <input class="form-check-input" type="checkbox" value="1"
              id="wit_id_check" wire:model.defer="wit_id_check">
            <label class="form-check-label" for="wit_id_check">
              Correct ID numbers
            </label>
          </div>
          <div class="form-check">
            <input class="form-check-input" type="checkbox" value="1"
              id="wit_contact_check" wire:model.defer="wit_contact_check">
            <label class="form-check-label" for="wit_contact_check">
              Correct contact details
            </label>
          </div>
          <div class="form-check">
            <input class="form-check-input" type="checkbox" value="1"
              id="wit_age_check" wire:model.defer="wit_age_check">
            <label class="form-check-label" for="wit_age_check">
              Each witness is 14 years or older
            </label>
          </div>
          <div class="form-check">
            <input class="form-check-input" type="checkbox" value="1"
              id="wit_write_check" wire:model.defer="wit_write_check">
            <label class="form-check-label" for="wit_write_check">
              Each witness can sign the Will in his / her own handwriting
            </label>
          </div>
          <div class="form-check">
            <input class="form-check-input" type="checkbox" value="1"
              id="wit_in_will_check" wire:model.defer="wit_in_will_check">
            <label class="form-check-label" for="wit_in_will_check">
              Neither each witness nor their spouse is a beneficiary, guardian,
              trustee, or executor in your Will.
            </label>
          </div>
        </div>

        <div class="col-md-12">
          <hr />
        </div>

        <div class="col-md-12 d-flex gap-2">
          <span class="">
            {{-- <input type="submit" class="btn btn-red" value="save"> --}}
            <a href="{{ url()->previous() }}" class="btn btn-red outline">
              Back
            </a>
          </span>
          <span class="ms-auto">
            <a href="" class="btn btn-red"
              wire:click.prevent="checkWitnesses">
              Next
            </a>
          </span>
        </div>

      </div>
    </div>
  </div>
  @include('livewire.account.organiser.partials.will_footer')
</div>
