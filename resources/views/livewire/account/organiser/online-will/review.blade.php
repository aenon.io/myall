<div class="__panel order-md-2 order-1">
  {{-- @include('livewire.account.organiser.partials.info') --}}

  @if (!$review_page)
    @include('livewire.account.organiser.online-will.review-pages.start')
  @elseif($review_page == 'executor')
    @include('livewire.account.organiser.online-will.review-pages.executor')
  @elseif($review_page == 'spouse')
    @include('livewire.account.organiser.online-will.review-pages.spouse')
  @elseif($review_page == 'beneficiaries')
    @include('livewire.account.organiser.online-will.review-pages.beneficiaries')
  @elseif($review_page == 'witnesses')
    @include('livewire.account.organiser.online-will.review-pages.witnesses')
  @elseif($review_page == 'guardians')
    @include('livewire.account.organiser.online-will.review-pages.guardians')
  @elseif($review_page == 'trustees')
    @include('livewire.account.organiser.online-will.review-pages.trustees')
  @elseif($review_page == 'start-stage-four')
    @include('livewire.account.organiser.online-will.review-pages.start_stage_four')
  @elseif($review_page == 'acknowledgement')
    @include('livewire.account.organiser.online-will.review-pages.acknowledgement')
  @elseif($review_page == 'download-will')
    @include('livewire.account.organiser.online-will.review-pages.download_will')
  @elseif($review_page == 'sign-will')
    @include('livewire.account.organiser.online-will.review-pages.sign_will')
  @elseif($review_page == 'stage-6-a')
    @include('livewire.account.organiser.online-will.review-pages.stage_6_a')
  @elseif($review_page == 'stage-6-b')
    @include('livewire.account.organiser.online-will.review-pages.stage_6_b')
  @elseif($review_page == 'stage-7-a')
    @include('livewire.account.organiser.online-will.review-pages.stage_7_a')
  @elseif($review_page == 'complete')
    @include('livewire.account.organiser.online-will.review-pages.complete')
  @endif

  <div class="modal" tabindex="-1" id="notify_modal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="notify_title"></h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal"
            aria-label="Close"></button>
        </div>
        <div class="modal-body text-center">
          <p><b id="notify_message"></b></p>
        </div>
        <div class="modal-footer">
          <a href="{{ url('organiser/online-will/review/sign-will') }}"
            class="btn btn-red">Continue</a>
        </div>
      </div>
    </div>
  </div>

  @push('scripts')
    <script>
      window.addEventListener('go-to-top', event => {
        window.scrollTo(0, 0);
      });
      window.addEventListener('notify', event => {
        var title = event.detail.title;
        var message = event.detail.message;

        $('#notify_title').html(title);
        $('#notify_message').html(message);
        $("#notify_modal").modal("show");
      });
    </script>
  @endpush
</div>
