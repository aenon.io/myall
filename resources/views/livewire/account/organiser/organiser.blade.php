<div class="myallOrganiserMain">
  @include('livewire.account.organiser.partials.org_menu')
  <div class="myallOrganiserMainInner">

    <livewire:account.partials.my-search-bar />
    @if ($view == 'personal')
      <livewire:account.organiser.personal.personal />
    @elseif($view == 'connections')
      <livewire:account.organiser.connections.connections />
      @elseif($view == 'online-will')
      @if ($comingSoon)
        <!-- Coming Soon Banner -->
        <div class="coming-soon-banner">
          <h2>Coming Soon!</h2>
          <p>We are working on this feature, stay tuned for updates.</p>
        </div>
      @else
        <div class="col">
          <livewire:account.organiser.online-will.online-will :stage="$stage" :review_page="$review_page" />
        </div>
      @endif
    @elseif($view == 'business-wealth')
      <livewire:account.organiser.business-wealth.business-wealth />
    @elseif($view == 'properties-wealth')
      <livewire:account.organiser.property-wealth.property-wealths />
    @elseif($view == 'debt-expenses')
      <livewire:account.organiser.expenses.expenses />
    @elseif($view == 'social-media')
      <livewire:account.organiser.social-medias.social-medias />
    @elseif($view == 'memories')
      <livewire:account.organiser.memories.memories />
    @elseif($view == 'funeral')
      <livewire:account.organiser.funerals.funerals />
    @elseif($view == 'safekeeping')
      <livewire:account.organiser.safe-keeping.safe-keeping />
    @endif

  </div>
</div>

@push('styles')
    <style>
        .coming-soon-banner {
            background-color: #f7f7f7;
            border: 1px solid #ccc;
            border-radius: 25px;
            padding: 20px;
            text-align: center;
            margin-top: 20px;
        }

        .coming-soon-banner h2 {
            font-size: 2em;
            color: #333;
            margin-bottom: 10px;
        }

        .coming-soon-banner p {
            font-size: 1.2em;
            color: #666;
        }
    </style>
    
@push('scripts')
  <script>
    $(document).ready(function() {
      var show_init_pop = '{{ $show_init_pop_up }}';
      if (show_init_pop) {
        $('#init-pop-up').modal('show');
      }
      window.addEventListener('close-modals', event => {
        $('.modal').each(function() {
          $(this).modal('hide');
        })
      });

    });
  </script>
@endpush
