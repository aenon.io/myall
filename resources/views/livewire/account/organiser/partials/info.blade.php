@php
  $cur_page = Request::segment(3);
@endphp
@if ($cur_page != 'interview' && $cur_page != 'stage-2')
  <div class="row">
    <div class="col-md-12">
      <div class="mb-3">
        <div class="card-box-style h-100 bg-dark-blue rounded">
          <span>Think you made a mistake or forgot to add something? Click on the relevant category on your left to
            return to that section to update your information.</span>
          <div class="mt-2">
            <b class="text-white">Upon editing, please remember to click save before you proceed to review.</b>
          </div>
        </div>
      </div>
    </div>
  </div>
@endif

