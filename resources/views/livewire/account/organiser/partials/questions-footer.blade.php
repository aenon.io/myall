<div class="">

  <div class="__footer d-flex justify-content-between">
    <div class="">
      <a href="#" class="btn btn-red" wire:click.prevent="redoSesction">
        Redo Questions
      </a>
    </div>
    <div class="">
      <a href="#" class="btn btn-outline-two" data-bs-toggle="modal"
        data-bs-target="#disclaimer-modal">Legal Disclaimer</a>
    </div>
  </div>

  <div class="modal" tabindex="-1" id="disclaimer-modal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-body">
          <div class="d-flex">
            <h5 class="modal-title">Legal Content Disclaimer</h5>
            <span class="ms-auto">
              <button type="button" class="btn-close" data-bs-dismiss="modal"
                aria-label="Close"></button>
            </span>
          </div>
          <div class="mt-3">
            <p>The information contained on this website is aimed at providing
              members of the public with guidance on the law in South Africa.
            </p>
            <p>This information has not been provided to meet the individual
              requirements of a specific person and MyAll insists that legal
              advice be obtained to address a person’s unique circumstances.</p>
            <p>It is important to remember that the law is constantly changing
              and although MyAll strives to keep the information up to date,
              MyAll cannot guarantee that the information will be updated
              and/or be without errors or omissions.</p>
            <p>As a result, MyAll, its employees, independent contractors,
              associates or third parties will under no circumstances accept
              liability or be held liable for any actions or omissions by MyAll
              which may result in any harm or liability flowing from the use of
              or the inability to use the information provided.</p>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-outline-two"
            data-bs-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
</div>
