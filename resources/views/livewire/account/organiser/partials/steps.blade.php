<div class="">
  <div class="">
    <div class="__heading">
      <h4 class="__title">{{ $title }}</h4>

      <div class="d-flex align-items-center gap-2">

        @if ($enable_save)
          <div class="__steps-save">
            <input type="submit" class="btn btn-red" value="save">
          </div>
        @endif

        @if ($enable_save_simultaneous)
          <div class="__steps-save">
            @if ($inheritance_type == 'inheritance')
              <button class="btn btn-red save"
                wire:click.prevent="$emit('saveInheritance')">
                Save
              </button>
            @endif
            @if ($inheritance_type == 'specific_inheritance')
              <button class="btn btn-red save"
                wire:click.prevent="$emit('saveSpecificInheritance')">
                Save
              </button>
            @endif
          </div>
        @endif

        <button class="__subtitle">
          <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
            xmlns="http://www.w3.org/2000/svg">
            <path d="M7.3335 4.66732H8.66683V6.00065H7.3335V4.66732ZM7.3335
              7.33398H8.66683V11.334H7.3335V7.33398ZM8.00016 1.33398C4.32016
              1.33398 1.3335 4.32065 1.3335 8.00065C1.3335 11.6807 4.32016
              14.6673 8.00016 14.6673C11.6802 14.6673 14.6668 11.6807 14.6668
              8.00065C14.6668 4.32065 11.6802 1.33398 8.00016 1.33398ZM8.00016
              13.334C5.06016 13.334 2.66683 10.9407 2.66683 8.00065C2.66683
              5.06065 5.06016 2.66732 8.00016 2.66732C10.9402 2.66732 13.3335
              5.06065 13.3335 8.00065C13.3335 10.9407 10.9402 13.334 8.00016
              13.334Z" fill="#CF3D60" />
          </svg>
          <span>More Info</span>
        </button>
      </div>
    </div>
    @php
      $cur_page = Request::segment(3);
    @endphp

    {{-- @if ($cur_page !== 'interview' && $cur_page != 'stage-2') --}}
    {{--   <span class="order-0 order-md-1 mb-md-0 mb-3 ms-auto"> --}}
    {{--     @if (count($stage_vids) > 0) --}}
    {{--       <a href="#" data-toggle="tooltip" data-placement="bottom" --}}
    {{--         title="Video infographics"><img src="{{ asset('img/vid.png') }}" --}}
    {{--           wire:click.prevent="showVid({{ $stage_val }})"></a> --}}
    {{--     @else --}}
    {{--       <a href="#" data-bs-toggle="modal" --}}
    {{--         data-bs-target="#vid-coming-soon"><img --}}
    {{--           src="{{ asset('img/vid.png') }}"></a> --}}
    {{--     @endif --}}
    {{--     <a href="#" data-toggle="tooltip" data-placement="bottom" --}}
    {{--       title="Image infographics"><img src="{{ asset('img/info.png') }}" --}}
    {{--         wire:click.prevent="showInfo({{ $stage_val }})"></a> --}}
    {{--   </span> --}}
    {{-- @endif --}}

  </div>

  {{-- <div class="row h-75" wire:ignore> --}}
  {{--   <div class="col-md-12"> --}}
  {{--     <div class="progress-container"> --}}
  {{--       <div class="num-progress" id="num-progress"> </div> --}}
  {{--       <div class="circle active-step">1</div> --}}
  {{--       <div class="circle">2</div> --}}
  {{--       <div class="circle">3</div> --}}
  {{--       <div class="circle">4</div> --}}
  {{--       <div class="circle">5</div> --}}
  {{--       <div class="circle">6</div> --}}
  {{--       <div class="circle">7</div> --}}
  {{--     </div> --}}
  {{--     <div class="progress" wire:ignore> --}}
  {{--       <div class="progress-bar bg-danger-gradiant" id="bg-danger-gradiant" --}}
  {{--         role="progressbar" aria-label="Danger example" style="width: 1%" --}}
  {{--         aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div> --}}
  {{--     </div> --}}
  {{--   </div> --}}
  {{-- </div> --}}

  <div class="modal" tabindex="-1" id="images-modal">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-body">
          <div class="text-end"><button type="button" class="btn-close"
              data-bs-dismiss="modal" aria-label="Close"></button></div>
          <div class="my-3">
            <b class="text-blue">Please select Infographics below for helpful
              information</b>
          </div>
          @if (count($stage_images) > 0)
            <div class="list-group">
              @foreach ($stage_images as $img)
                @php
                  $abs_path = $img;
                  $rel_path = substr(
                      $abs_path,
                      strpos($abs_path, 'infographics') + 13,
                  );
                @endphp
                <a href="#" class="list-group-item list-group-item-action"
                  wire:click.prevent="showSingleImage('{{ $rel_path }}')">
                  {{ basename($img, '.jpg') }}
                </a>
              @endforeach
            </div>
          @endif
        </div>
      </div>
    </div>
  </div>
  <div class="modal" tabindex="-1" id="single-img-modal">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-body">
          <div class="text-end"><button type="button" class="btn-close"
              data-bs-dismiss="modal" aria-label="Close"></button></div>
          <img src="" class="img-fluid w-100" id="cur_img">
        </div>
      </div>
    </div>
  </div>

  <div class="modal" tabindex="-1" id="vids-modal">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-body">
          <div class="text-end"><button type="button" class="btn-close"
              data-bs-dismiss="modal" aria-label="Close"></button></div>
          <div class="my-3">
            <b class="text-blue">Please select videos below for helpful
              information</b>
          </div>
          @if (count($stage_vids) > 0)
            <div class="list-group">
              @foreach ($stage_vids as $vid)
                <a href="#" class="list-group-item list-group-item-action"
                  wire:click.prevent='showSingleVid("{{ $vid }}")'>
                  {{ $vid }}
                </a>
              @endforeach
            </div>
          @endif
        </div>
      </div>
    </div>
  </div>
  <div class="modal" tabindex="-1" id="single-vid-modal">
    <div class="modal-dialog h-100">
      <div class="modal-content">
        <div class="modal-body">
          <div class="text-end"><button type="button" class="btn-close"
              data-bs-dismiss="modal" aria-label="Close"></button></div>
          <div class="cur_vid_cont">
            <video controls>
              <source src="" type="video/mp4" id="cur_vid">
              Your browser does not support the video tag.
            </video>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="modal" tabindex="-1" id="vid-coming-soon">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-body">
          <div class="text-end"><button type="button" class="btn-close"
              data-bs-dismiss="modal" aria-label="Close"></button></div>
          <div class="text-center">
            <h5>Informative video coming soon!</h5>
          </div>
        </div>
      </div>
    </div>
  </div>

  @push('scripts')
    <script>
      window.addEventListener('show-single-vid-modal', event => {
        var src = event.detail.name;

        $('.modal').modal('hide');
        $('body').removeClass('modal-open');
        $('.modal-backdrop').remove();

        $('#cur_vid').attr('src', src);
        $(".cur_vid_cont video")[0].load();
        $('#single-vid-modal').modal('show');

      });
      window.addEventListener('show-vids-modal', event => {
        $('#vids-modal').modal('show');
      });

      window.addEventListener('show-images-modal', event => {
        $('#images-modal').modal('show');
      });
      window.addEventListener('show-single-image-modal', event => {
        var src = event.detail.image_path;

        $('.modal').modal('hide');
        $('body').removeClass('modal-open');
        $('.modal-backdrop').remove();

        $("#cur_img").attr('src', src);
        $('#single-img-modal').modal("show");

      });

      $(document).ready(function() {
        $("#single-vid-modal").on("hidden.bs.modal", function() {
          document.querySelectorAll('video').forEach(vid => vid.pause());
        });

        const progress = document.getElementById("num-progress");
        const stepCircles = document.querySelectorAll(".circle");
        let currentActive = 1;

        update({{ $stage_val }});

        function update(currentActive) {
          stepCircles.forEach((circle, i) => {
            if (i < currentActive) {
              circle.classList.add("active-step");
            } else {
              circle.classList.remove("active-step");
            }
          });

          const activeCircles = document.querySelectorAll(".active-step");
          progress.style.width = ((activeCircles.length - 1) / (stepCircles
            .length - 1)) * 100 + "%";
          const progre_line = document.getElementById('bg-danger-gradiant');
          progre_line.style.width = ((activeCircles.length - 1) / (stepCircles
            .length - 1)) * 100 + "%";
        }
      });
    </script>
  @endpush
</div>
