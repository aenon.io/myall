<div class="myallFooter row border-top mt-3 pb-3 pt-3">

  @php
    $cur_page = Request::segment(3);
    $cur_section = Request::segment(2);
  @endphp

  {{-- <div class="col-md-6 previous-legal-btns d-grid d-md-block mt-2 gap-2"> --}}
  {{--   @if ($cur_page != 'profile' && $cur_page != 'interview' && $cur_page != 'stage-2') --}}
  {{--   @if ( --}}
  {{--       $cur_page != 'profile' && --}}
  {{--           $cur_page != 'interview' && --}}
  {{--           $cur_page != 'stage-2' && --}}
  {{--           $cur_section != 'online-will') --}}
  {{--     <a href="{{ url()->previous() }}" class="btn btn-red"> --}}
  {{--       BACK --}}
  {{--     </a> --}}
  {{--   @endif --}}
  {{-- </div> --}}

  {{-- <div class="col-md-6 previous-legal-btns d-grid d-md-block mt-2 gap-2 text-end"> --}}
  <div class="col-md-6 d-grid d-md-block mt-2 gap-2 text-end">
    <a href="#" class="btn btn-outline-two" data-bs-toggle="modal"
      data-bs-target="#disclaimer-modal">
      Legal Disclaimer
    </a>
  </div>

</div>

{{-- @if ($cur_page != 'interview' && $cur_page != 'stage-2' && $cur_page != 'review') --}}
{{--   <div class="row" wire:ignore:self> --}}
{{--     <div class="col-md-12"> --}}
{{--       <p>Think you made a mistake or forgot to add something? Click on the --}}
{{--         relevant category on your left to return to that section to update your --}}
{{--         information. </p> --}}
{{--     </div> --}}
{{--   </div> --}}
{{-- @endif --}}

<div class="modal" tabindex="-1" id="disclaimer-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <div class="d-flex">
          <h5 class="modal-title">Legal Content Disclaimer</h5>
          <span class="ms-auto">
            <button type="button" class="btn-close" data-bs-dismiss="modal"
              aria-label="Close"></button>
          </span>
        </div>
        <div class="mt-3">
<p>The content on MyAll is designed to offer general legal guidance to the public regarding South African law. This information is not tailored to any individual's specific needs, and it is strongly recommended to rather seek personalised legal advice for unique situations.</p>
<p>Please note that laws are subject to change. While MyAll strives to ensure the information is current, we cannot guarantee it is always up-to-date or free from errors and omissions. Consequently, MyAll, its employees, contractors, partners, or affiliates cannot be held liable for any harm or damages resulting from the use or inability to use the information provided.</p>
          {{-- <p>The information contained on this website is aimed at providing --}}
          {{--   members of the public with guidance on the law in South Africa.</p> --}}
          {{-- <p>This information has not been provided to meet the individual --}}
          {{--   requirements of a specific person and MyAll insists that legal --}}
          {{--   advice be obtained to address a person’s unique circumstances.</p> --}}
          {{-- <p>It is important to remember that the law is constantly changing and --}}
          {{--   although MyAll strives to keep the information up to date, MyAll --}}
          {{--   cannot guarantee that the information will be updated and/or be --}}
          {{--   without errors or omissions.</p> --}}
          {{-- <p>As a result, MyAll, its employees, independent contractors, --}}
          {{--   associates or third parties will under no circumstances accept --}}
          {{--   liability or be held liable for any actions or omissions by MyAll --}}
          {{--   which may result in any harm or liability flowing from the use of or --}}
          {{--   the inability to use the information provided.</p> --}}
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-two"
          data-bs-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
