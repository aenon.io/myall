<div class="col-md-4 order-md-1 order-2">
  <div class="card-box-style h-100 d-none d-lg-block rounded">
    @if (Auth::user()->will_menu_options)
      <div class="__step-heading">
        {{-- <h3>Step 2: Profile</h3> --}}
        <h3>Profile</h3>
      </div>

      <div class="__menu_items">

        <a class="org_menu_link"
          href="{{ url('organiser/online-will/profile') }}">
          @if (Auth::user())
            <div class="org_menu_item __completed">
              <div class="__marker"></div>
              <div class="org_menu_name">Your Details</div>
            </div>
          @else
            <div class="org_menu_item __todo">
              <svg width="16" height="17" viewBox="0 0 16 17"
                fill="none" xmlns="http://www.w3.org/2000/svg">
                <rect x="0.5" y="1" width="15" height="15" rx="7.5"
                  stroke="#929BCA" />
              </svg>
              <div class="org_menu_name">Your Details</div>
            </div>
          @endif
        </a>

        @if (Auth::user()->executors->count() > 0)
          <a class="org_menu_link"
            href="{{ url('organiser/online-will/executor') }}">
        @endif
        @if (Auth::user()->executors->count() > 0)
          <div class="org_menu_item __completed">
            <div class="__marker"></div>
            <div class="org_menu_name">Executor</div>
          </div>
        @else
          <div class="org_menu_item __todo">
            <svg width="16" height="17" viewBox="0 0 16 17"
              fill="none" xmlns="http://www.w3.org/2000/svg">
              <rect x="0.5" y="1" width="15" height="15" rx="7.5"
                stroke="#929BCA" />
            </svg>
            <div class="org_menu_name">Executor</div>
          </div>
        @endif
        @if (Auth::user()->executors->count() > 0)
          </a>
        @endif

        @if (Auth::user()->will_menu_options->spouse)
          @if (Auth::user()->spouses->count() > 0)
            <a class="org_menu_link"
              href="{{ url('organiser/online-will/spouse') }}">
          @endif
          @if (Auth::user()->spouses->count() > 0)
            <div class="org_menu_item __completed">
              <div class="__marker"></div>
              <div class="org_menu_name">Spouse</div>
            </div>
          @else
            <div class="org_menu_item __todo">
              <svg width="16" height="17" viewBox="0 0 16 17"
                fill="none" xmlns="http://www.w3.org/2000/svg">
                <rect x="0.5" y="1" width="15" height="15" rx="7.5"
                  stroke="#929BCA" />
              </svg>
              <div class="org_menu_name">Spouse</div>
            </div>
          @endif
          @if (Auth::user()->spouses->count() > 0)
            </a>
          @endif
        @endif

        @if (Auth::user()->will_menu_options->minor_children_beneficiary)
          @if (Auth::user()->minorCount() > 0)
            <a class="org_menu_link"
              href="{{ url('organiser/online-will/minor-children-beneficiary') }}">
          @endif
          @if (Auth::user()->minorCount() > 0)
            <div class="org_menu_item __completed">
              <div class="__marker"></div>
              <div class="org_menu_name">Minor Children</div>
            </div>
          @else
            <div class="org_menu_item __todo">
              <svg width="16" height="17" viewBox="0 0 16 17"
                fill="none" xmlns="http://www.w3.org/2000/svg">
                <rect x="0.5" y="1" width="15" height="15" rx="7.5"
                  stroke="#929BCA" />
              </svg>
              <div class="org_menu_name">Minor Children</div>
            </div>
          @endif
          @if (Auth::user()->children->count() > 0)
            </a>
          @endif
        @endif

        @if (Auth::user()->will_menu_options->guardian)
          @if (Auth::user()->guardians()->count() > 0)
            <a class="org_menu_link"
              href="{{ url('organiser/online-will/guardian') }}">
          @endif
          @if (Auth::user()->guardians()->count() > 0)
            <div class="org_menu_item __completed">
              <div class="__marker"></div>
              <div class="org_menu_name">Guardians</div>
            </div>
          @else
            <div class="org_menu_item __todo">
              <svg width="16" height="17" viewBox="0 0 16 17"
                fill="none" xmlns="http://www.w3.org/2000/svg">
                <rect x="0.5" y="1" width="15" height="15" rx="7.5"
                  stroke="#929BCA" />
              </svg>
              <div class="org_menu_name">Guardians</div>
            </div>
          @endif
          @if (Auth::user()->guardians()->count() > 0)
            </a>
          @endif
        @endif

        @if (Auth::user()->will_menu_options->testamentary_trust)
          @if (Auth::user()->testamentary_trust->count() > 0)
            <a class="org_menu_link"
              href="{{ url('organiser/online-will/testamentary-trust') }}">
          @endif
          @if (Auth::user()->testamentary_trust->count() > 0)
            <div class="org_menu_item __completed">
              <div class="__marker"></div>
              <div class="org_menu_name">Testamentary Trust</div>
            </div>
          @else
            <div class="org_menu_item __todo">
              <svg width="16" height="17" viewBox="0 0 16 17"
                fill="none" xmlns="http://www.w3.org/2000/svg">
                <rect x="0.5" y="1" width="15" height="15" rx="7.5"
                  stroke="#929BCA" />
              </svg>
              <div class="org_menu_name">Testamentary Trust</div>
            </div>
          @endif
          @if (Auth::user()->testamentary_trust->count() > 0)
            </a>
          @endif
        @endif

        @if (Auth::user()->will_menu_options->inheritance)
          @if (Auth::user()->regular_inheritance()->count() > 0)
            <a class="org_menu_link"
              href="{{ url('organiser/online-will/inheritance') }}">
          @endif
          @if (Auth::user()->regular_inheritance()->count() > 0)
            <div class="org_menu_item __completed">
              <div class="__marker"></div>
              <div class="org_menu_name">Inheritance</div>
            </div>
          @else
            <div class="org_menu_item __todo">
              <svg width="16" height="17" viewBox="0 0 16 17"
                fill="none" xmlns="http://www.w3.org/2000/svg">
                <rect x="0.5" y="1" width="15" height="15"
                  rx="7.5" stroke="#929BCA" />
              </svg>
              <div class="org_menu_name">Inheritance</div>
            </div>
          @endif
          @if (Auth::user()->regular_inheritance()->count() > 0)
            </a>
          @endif
        @endif

        @if (Auth::user()->will_menu_options->specific_inheritance)
          @if (Auth::user()->specific_inheritance()->count() > 0)
            <a class="org_menu_link"
              href="{{ url('organiser/online-will/specific-inheritance') }}">
          @endif
          @if (Auth::user()->specific_inheritance()->count() > 0)
            <div class="org_menu_item __completed">
              <div class="__marker"></div>
              <div class="org_menu_name">Specific Inheritance</div>
            </div>
          @else
            <div class="org_menu_item __todo">
              <svg width="16" height="17" viewBox="0 0 16 17"
                fill="none" xmlns="http://www.w3.org/2000/svg">
                <rect x="0.5" y="1" width="15" height="15"
                  rx="7.5" stroke="#929BCA" />
              </svg>
              <div class="org_menu_name">Specific Inheritance</div>
            </div>
          @endif
          @if (Auth::user()->specific_inheritance()->count() > 0)
            </a>
          @endif
        @endif

        @if (Auth::user()->will_menu_options->remainder_inheritance)
          @if (Auth::user()->remainder_inheritance()->count() > 0)
            <a class="org_menu_link"
              href="{{ url('organiser/online-will/remainder-inheritance') }}">
          @endif
          @if (Auth::user()->remainder_inheritance()->count() > 0)
            <div class="org_menu_item __completed">
              <div class="__marker"></div>
              <div class="org_menu_name">Remainder Inheritance</div>
            </div>
          @else
            <div class="org_menu_item __todo">
              <svg width="16" height="17" viewBox="0 0 16 17"
                fill="none" xmlns="http://www.w3.org/2000/svg">
                <rect x="0.5" y="1" width="15" height="15"
                  rx="7.5" stroke="#929BCA" />
              </svg>
              <div class="org_menu_name">Remainder Inheritance</div>
            </div>
          @endif
          @if (Auth::user()->remainder_inheritance()->count() > 0)
            </a>
          @endif
        @endif

        @if (Auth::user()->will_menu_options->simultaneous_death)
          @if (Auth::user()->simulteneous_death_inheritance()->count() > 0)
            <a class="org_menu_link"
              href="{{ url('organiser/online-will/simultaneous-death') }}">
          @endif
          @if (Auth::user()->simulteneous_death_inheritance()->count() > 0)
            <div class="org_menu_item __completed">
              <div class="__marker"></div>
              <div class="org_menu_name">Simultaneous Death</div>
            </div>
          @else
            <div class="org_menu_item __todo">
              <svg width="16" height="17" viewBox="0 0 16 17"
                fill="none" xmlns="http://www.w3.org/2000/svg">
                <rect x="0.5" y="1" width="15" height="15"
                  rx="7.5" stroke="#929BCA" />
              </svg>
              <div class="org_menu_name">Simultaneous Death</div>
            </div>
          @endif
          @if (Auth::user()->simulteneous_death_inheritance()->count() > 0)
            </a>
          @endif
        @endif

        @if (Auth::user()->will_menu_options->funeral_wishes)
          @if (Auth::user()->funeral_wishes)
            <a class="org_menu_link"
              href="{{ url('organiser/online-will/funeral-wishes') }}">
          @endif
          @if (Auth::user()->funeral_wishes)
            <div class="org_menu_item __completed">
              <div class="__marker"></div>
              <div class="org_menu_name">Funeral Wishes</div>
            </div>
          @else
            <div class="org_menu_item __todo">
              <svg width="16" height="17" viewBox="0 0 16 17"
                fill="none" xmlns="http://www.w3.org/2000/svg">
                <rect x="0.5" y="1" width="15" height="15"
                  rx="7.5" stroke="#929BCA" />
              </svg>
              <div class="org_menu_name">Funeral Wishes</div>
            </div>
          @endif
          @if (Auth::user()->funeral_wishes)
            </a>
          @endif
        @endif

        @if (Auth::user()->witnesses->count() > 0)
          <a class="org_menu_link"
            href="{{ url('organiser/online-will/witnesses') }}">
        @endif
        @if (Auth::user()->witnesses->count() > 0)
          <div class="org_menu_item __completed">
            <div class="__marker"></div>
            <div class="org_menu_name">Witnesses</div>
          </div>
        @else
          <div class="org_menu_item __todo">
            <svg width="16" height="17" viewBox="0 0 16 17"
              fill="none" xmlns="http://www.w3.org/2000/svg">
              <rect x="0.5" y="1" width="15" height="15"
                rx="7.5" stroke="#929BCA" />
            </svg>
            <div class="org_menu_name">Witnesses</div>
          </div>
        @endif
        @if (Auth::user()->witnesses->count() > 0)
          </a>
        @endif


      </div>
      @if (Auth::user()->witnesses->count() > 0)
        <div class="__step-heading mt-3">
          {{-- <a href="{{ url('organiser/online-will/review') }}" class=""> --}}
          <a href="{{ url('organiser/online-will/review/start-stage-four') }}"
            class="">
            <h3>Review</h3>
          </a>
        </div>
      @endif

    @endif
  </div>
</div>

@push('scripts')
  <script>
    document.addEventListener('DOMContentLoaded', function() {
      var url =
        "{{ Request::root() . '/' . Request::segment(1) . '/' . Request::segment(2) . '/' . Request::segment(3) }}";
      var links = document.querySelectorAll('.org_menu_link');
      console.log('\tURL: ' + url)

      links.forEach(function(link) {
        var href = link.getAttribute('href');
        if (href === url) {
          var div = link.querySelector('div');
          if (div) {
            div.classList.add('active');
          }
          var img = link.querySelector('img');
          if (img) {
            var imgLink = img.getAttribute('data-active');
            if (imgLink) {
              img.setAttribute('src', imgLink);
            }
          }
        }
      });
    });
  </script>
@endpush
