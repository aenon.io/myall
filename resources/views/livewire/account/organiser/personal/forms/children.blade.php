<form wire:submit.prevent="saveChildren">
  @foreach ($children as $k => $child)
    <div class="row child-cont __item __child mb-3">
      @if (isset($child['id']))
        <div class="col-md-12 d-grid d-md-block mt-2 gap-2 text-end">
          <a href="#" class="btn btn-outline-two"
            wire:click.prevent="removeChild({{ $child['id'] }})">
            <svg width="16" height="17" viewBox="0 0 16 17" fill="none"
              xmlns="http://www.w3.org/2000/svg">
              <path d="M10.6668 6.5V13.1667H5.3335V6.5H10.6668ZM9.66683
                        2.5H6.3335L5.66683
                        3.16667H3.3335V4.5H12.6668V3.16667H10.3335L9.66683
                        2.5ZM12.0002 5.16667H4.00016V13.1667C4.00016 13.9
                        4.60016 14.5 5.3335 14.5H10.6668C11.4002 14.5 12.0002
                        13.9 12.0002 13.1667V5.16667Z" fill="#CF3D60" />
            </svg>
            Remove
          </a>
        </div>
      @endif
      <div class="col-md-4">
        <div class="f-w-input mb-3">
          <label class="form-label">Name<span
              class="required_input">*</span></label>
          <input type="text" class="btm-border-input"
            wire:model.defer="children.{{ $k }}.child_name">
        </div>
      </div>
      <div class="col-md-4">
        <div class="f-w-input mb-3">
          <label class="form-label">Middle Name</label>
          <input type="text" class="btm-border-input"
            wire:model.defer="children.{{ $k }}.middle_name">
        </div>
      </div>
      <div class="col-md-4">
        <div class="f-w-input mb-3">
          <label class="form-label">Surname<span
              class="required_input">*</span></label>
          <input type="text" class="btm-border-input"
            wire:model.defer="children.{{ $k }}.child_surname">
        </div>
      </div>
      <div class="col-md-12">
        <div class="f-w-input mb-3">
          <label class="form-label">ID Number</label>
          <input type="text" class="btm-border-input"
            wire:model.defer="children.{{ $k }}.child_id_number">
        </div>
      </div>
      <div class="col-md-12">
        <div class="f-w-input mb-3">
          <label class="form-label">Date of Birth<span
              class="required_input">*</span></label>
          <input type="date" class="btm-border-input" max="2999-12-31"
            wire:model.defer="children.{{ $k }}.child_date_of_birth">
        </div>
      </div>
      <div class="col-md-12">
        <div class="f-w-input mb-3">
          <label class="form-label">Copy of Birth Certificate</label>
          @if (isset($child['child_birth_certificate_file']))
            <div class="d-grid d-md-block mt-2 gap-2">
              <a href="{{ url('storage/' . $child['child_birth_certificate_file']) }}"
                target="_blank"
                class="btn btn-outline-one upload-view-btns">View File</a>
              <a href="#" target="_blank"
                class="btn btn-outline-one ms-md-2 upload-view-btns ms-0"
                wire:click.prevent="removeChildDocument('child_birth_certificate', {{ $child['id'] }})">Remove
                File</a>
            </div>
          @else
            <input type="file" class="btm-border-input file-input-rad"
              wire:model="children.{{ $k }}.child_birth_certificate">
            @error('children.' . $k . '.child_birth_certificate')
              <span class="error text-red">Failed to upload(File must not exceed
                limit of 2MB and file type: pdf, jpeg,
                png)</span>
            @enderror
            </br>
            <span class="text-muted">File: Max File Size 2MB (pdf, jpeg,
              png)</span>
          @endif
        </div>
      </div>
      <div class="col-md-12">
        <div class="f-w-input mb-3">
          <label class="form-label">Copy of Adoption Certificate</label>
          @if (isset($child['child_adoption_certificate_file']) && isset($child['id']))
            <div class="d-grid d-md-block mt-2 gap-2">
              <a href="{{ url('storage/' . $child['child_adoption_certificate_file']) }}"
                target="_blank"
                class="btn btn-outline-one upload-view-btns">View File</a>
              <a href="#" target="_blank"
                class="btn btn-outline-one ms-md-2 upload-view-btns ms-0"
                wire:click.prevent="removeChildDocument('child_adoption_certificate', {{ $child['id'] }})">Remove
                File</a>
            </div>
          @else
            <input type="file" class="btm-border-input file-input-rad"
              wire:model="children.{{ $k }}.child_adoption_certificate">
            @error('children.' . $k . '.child_adoption_certificate')
              <span class="error text-red">Failed to upload(File must not exceed
                limit of 2MB and file type: pdf, jpeg,
                png)</span>
            @enderror
            </br>
            <span class="text-muted">File: Max File Size 2MB (pdf, jpeg,
              png)</span>
          @endif
        </div>
      </div>
    </div>
  @endforeach
  <div class="row mb-3">
    <div class="col-md-12 d-flex">
      <span class="ms-auto">
        <a href="#" class="btn btn-outline-two"
          wire:click.prevent="addChild">
          <svg width="16" height="17" viewBox="0 0 16 17" fill="none"
            xmlns="http://www.w3.org/2000/svg">
            <path
              d="M12.6666
            9.16536H8.66659V13.1654H7.33325V9.16536H3.33325V7.83203H7.33325V3.83203H8.66659V7.83203H12.6666V9.16536Z"
              fill="#CF3D60" />
          </svg>
          Add Child
        </a>
      </span>
    </div>
  </div>
  <div class="col-md-12">
    <hr>
  </div>
  <div class="row">
    <div class="col-md-12 myallButtons">
      <div class="__cancel">
        <button class="btn">Cancel</button>
      </div>
      <div class="d-grid __save mb-3">
        <input type="submit" class="btn btn-red" value="save">
      </div>
    </div>
  </div>
</form>
