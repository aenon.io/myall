<form wire:submit.prevent="saveEducation" enctype="multipart/form-data">
  <div class="row">
    @foreach ($educations as $e => $course)
      <div class="__item __education mb-3">
        @if (isset($course['id']))
          <div class="__item-header">
            <div class="__item-title">Education</div>
            <a href="#" class="btn btn-outline-two"
              wire:click.prevent="removeEducation({{ $course['id'] }})">
              <svg width="16" height="17" viewBox="0 0 16 17"
                fill="none" xmlns="http://www.w3.org/2000/svg">
                <path
                  d="M10.6668 6.5V13.1667H5.3335V6.5H10.6668ZM9.66683 2.5H6.3335L5.66683 3.16667H3.3335V4.5H12.6668V3.16667H10.3335L9.66683 2.5ZM12.0002 5.16667H4.00016V13.1667C4.00016 13.9 4.60016 14.5 5.3335 14.5H10.6668C11.4002 14.5 12.0002 13.9 12.0002 13.1667V5.16667Z"
                  fill="#CF3D60" />
              </svg>
              Remove
            </a>
          </div>
        @endif
        <div class="__inputs">
          <div class="col-md-6">
            <div class="f-w-input mb-3">
              <label class="form-label">Qualification</label>
              <select class="btm-border-input"
                wire:model.lazy="educations.{{ $e }}.qualification">
                <option value="">Select Option</option>
                @foreach ($qualifications as $qualification)
                  <option value="{{ $qualification }}">{{ $qualification }}
                  </option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="col-md-6">
            <div class="f-w-input mb-3">
              <label class="form-label">Year of Completion</label>
              <select class="btm-border-input"
                wire:model.lazy="educations.{{ $e }}.year">
                <option value="">Select Option</option>
                <option value="current">Current</option>
                @php
                  $currentYear = date('Y');
                  $startYear = 1900;
                @endphp

                @for ($year = $currentYear; $year >= $startYear; $year--)
                  <option value="{{ $year }}">{{ $year }}
                  </option>
                @endfor
              </select>
            </div>
          </div>
          <div class="col-md-12">
            <div class="f-w-input mb-3">
              <label class="form-label">Comment</label>
              <input type="text" class="btm-border-input"
                wire:model.defer="educations.{{ $e }}.comment">
            </div>
          </div>
          <div class="col-md-12">
            <div class="f-w-input mb-3">
              <label class="form-label">Certificate</label>
              @if ($course['certificate_file'] && isset($course['id']))
                <div class="d-grid d-md-block mt-2 gap-2">
                  <a href="{{ url('storage/' . $course['certificate_file']) }}"
                    target="_blank" class="btn btn-outline-one">View File</a>
                  <a href="#" target="_blank"
                    class="btn btn-outline-one ms-md-2 ms-0"
                    wire:click.prevent="removeCertificate({{ $course['id'] }})">Remove
                    File</a>
                </div>
              @else
                <input type="file" class="btm-border-input file-input-rad"
                  wire:model="educations.{{ $e }}.certificate">
                @error('educations.' . $e . '.certificate')
                  <span class="error text-red">Failed to upload(File must not
                    exceed limit of 2MB and file type: pdf,
                    jpeg,
                    png)</span>
                @enderror
                </br>
                <span class="text-muted">File: Max File Size 2MB (pdf, jpeg,
                  png)</span>
              @endif

            </div>
          </div>
        </div>

      </div>
    @endforeach
    <div class="col-md-12 d-grid justify-content-end gap-2 text-end">
      <span class="ms-auto">
        <a href="#" class="btn btn-outline-two"
          wire:click.prevent="addEducation">
          <svg width="16" height="17" viewBox="0 0 16 17" fill="none"
            xmlns="http://www.w3.org/2000/svg">
            <path
              d="M12.6666
            9.16536H8.66659V13.1654H7.33325V9.16536H3.33325V7.83203H7.33325V3.83203H8.66659V7.83203H12.6666V9.16536Z"
              fill="#CF3D60" />
          </svg>
          Add Education
        </a>
      </span>
    </div>
  </div>
  <div class="col-md-12">
    <hr>
  </div>
  <div class="row">
    <div class="col-md-12 myallButtons">
      <div class="__cancel">
        <button class="btn">Cancel</button>
      </div>
      <div class="d-grid __save mb-3">
        <input type="submit" class="btn btn-red" value="save">
      </div>
    </div>
  </div>
</form>
