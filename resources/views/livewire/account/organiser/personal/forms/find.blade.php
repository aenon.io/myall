<form wire:submit.prevent="saveItems">
  <div class="row">
    <div class="col-md-12 d-flex">
      <span class="ms-auto">
        <span class="pe-2">
          <i class="fa fa-info-circle" style="cursor: pointer; font-size: 20px;" data-bs-toggle="modal"
            data-bs-target="#page-description-sub"></i>
        </span>
        <a href="#" class="btn btn-outline-two" wire:click.prevent="addItemsTofind">Add More</a>
      </span>
    </div>
  </div>
  @foreach ($items_to_find as $k => $item)
    <div class="row">
      <div class="col-md-12">
        <div class="f-w-input mb-3">
          <label class="form-label">What to Find</label>
          <input type="text" class="btm-border-input" wire:model.defer="items_to_find.{{ $k }}.item_name">
        </div>
        <div class="f-w-input mb-3">
          <label class="form-label">Location</label>
          <input type="text" class="btm-border-input" wire:model.defer="items_to_find.{{ $k }}.location">
        </div>
        <div>
          @if (isset($item['id']))
            <a href="#" class="btn btn-outline-two"
              wire:click.prevent="removeFind({{ $item['id'] }})">Remove</a>
          @endif
        </div>
        <hr />
      </div>
    </div>
  @endforeach
  <div class="d-grid mb-3">
    <input type="submit" class="btn btn-red" value="save">
  </div>
</form>
<div class="modal" tabindex="-1" id="page-description-sub">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-body">
        <div class="d-flex">
          <h5>Where to Find</h5>
          <span class="ms-auto">
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </span>
        </div>
        <div class="mb-5">
          <p>What to Find allows you to document the location of any other important documentation. This information
            will be accessible to your executor after your passing.</p>
        </div>
      </div>
    </div>
  </div>
</div>

