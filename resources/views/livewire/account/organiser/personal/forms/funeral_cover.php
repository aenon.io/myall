<form wire:submit.prevent="saveFuneralCover">
  <div class="f-w-input mb-3">
    <label class="form-label">Company</label>
    <input type="text" class="btm-border-input" wire:model.defer="f_company">
  </div>
  <div class="f-w-input mb-3">
    <label class="form-label">Policy Number</label>
    <input type="text" class="btm-border-input" wire:model.defer="policy_number">
  </div>
  <div class="f-w-input mb-3">
    <label class="form-label">Policy Holder</label>
    <input type="text" class="btm-border-input" wire:model.defer="policy_holder">
  </div>
  <div class="f-w-input mb-3">
    <label class="form-label">Coverage Amount</label>
    <input type="text" class="btm-border-input number-separator" wire:model.defer="coverage_amount">
  </div>
  <div class="d-grid mb-3">
    <input type="submit" class="btn btn-red" value="save">
  </div>
</form>

