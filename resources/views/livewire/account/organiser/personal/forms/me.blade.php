<form wire:submit.prevent="saveMe">
  <div class="__note mb-3">
    <span class="">
      {{-- TODO: Hard coded fill --}}
      <svg width="20" height="20" viewBox="0 0 20 20" fill="none"
        xmlns="http://www.w3.org/2000/svg">
        <path d="M9.16663 12.4993H10.8333V14.166H9.16663V12.4993ZM9.16663
                    5.83268H10.8333V10.8327H9.16663V5.83268ZM9.99163
                    1.66602C5.39163 1.66602 1.66663 5.39935 1.66663
                    9.99935C1.66663 14.5993 5.39163 18.3327 9.99163 18.3327C14.6
                    18.3327 18.3333 14.5993 18.3333 9.99935C18.3333 5.39935 14.6
                    1.66602 9.99163 1.66602ZM9.99996 16.666C6.31663 16.666
                    3.33329 13.6827 3.33329 9.99935C3.33329 6.31602 6.31663
                    3.33268 9.99996 3.33268C13.6833 3.33268 16.6666 6.31602
                    16.6666 9.99935C16.6666 13.6827 13.6833 16.666 9.99996
                    16.666Z" fill="#040A5E" />
      </svg>
      <div>To change your contact details, please <strong>contact us.</strong>
      </div>
    </span>
  </div>
  <div class="f-w-input capped-width mb-3">
    <label class="form-label">Name</label>
    <input type="text" class="btm-border-input" wire:model.defer="name"
      disabled>
    @error('name')
      <span class="error text-red">{{ $message }}</span>
    @enderror
  </div>
  <div class="f-w-input capped-width mb-3">
    <label class="form-label">Surname</label>
    <input type="text" class="btm-border-input" wire:model.defer="surname"
      disabled>
    @error('surname')
      <span class="error text-red">{{ $message }}</span>
    @enderror
  </div>
  <div class="f-w-input capped-width mb-3">
    <label class="form-label">ID Number</label>
    <input type="text" class="btm-border-input" wire:model.defer="id_number"
      disabled>
    @error('id_number')
      <span class="error text-red">{{ $message }}</span>
    @enderror
  </div>
  <div class="f-w-input capped-width mb-3">
    <label class="form-label">Gender <i class="fa fa-info-circle"
        wire:click.prevent="showTipModal('gender')"></i></label>
    <select class="btm-border-input" name="gender" wire:model.defer="gender"
      disabled>
      <option value="Male">Male</option>
      <option value="Female">Female</option>
    </select>
    @error('gender')
      <span class="error text-red">{{ $message }}</span>
    @enderror
  </div>
  <div class="f-w-input capped-width mb-3">
    <label class="form-label">Original Birth Certificate (Certified
      Copy)</label>
    @if (!auth()->user()->birth_certificate)
      <input type="file" class="btm-border-input file-input-rad"
        wire:model="birth_certificate">
      @error('birth_certificate')
        <span class="error text-red">Failed to upload (File must not exceed limit
          of 2MB and file type: pdf, jpeg,
          png)</span>
      @enderror
      </br>
      <span class="text-muted">File: Max File Size 2MB (pdf, jpeg, png)</span>
    @endif
    @if (Auth::user()->birth_certificate)
      <div class="d-grid d-md-block mt-2 gap-2">
        <a href="{{ url('storage/' . Auth::user()->birth_certificate) }}"
          target="_blank" class="btn btn-outline-one">View
          File</a>
        <a href="#" class="btn btn-outline-one ms-md-2 ms-0"
          wire:click.prevent="removeDocument('birth_certificate')">Remove
          File</a>
      </div>
    @endif
  </div>
  <div class="f-w-input capped-width mb-3">
    <label class="form-label">Original ID Document (Certified Copy. Front & Back
      if you have smart card)</label>
    @if (!auth()->user()->id_document)
      <input type="file" class="btm-border-input file-input-rad"
        wire:model="id_document">
      @error('id_document')
        <span class="error text-red">Failed to upload(File must not exceed limit
          of 2MB and file type: pdf, jpeg,
          png)</span>
      @enderror
      </br>
      <span class="text-muted">File: Max File Size 2MB (pdf, jpeg, png)</span>
    @endif
    @if (Auth::user()->id_document)
      <div class="d-grid d-md-block mt-2 gap-2">
        <a href="{{ url('storage/' . Auth::user()->id_document) }}"
          target="_blank" class="btn btn-outline-one">View
          File</a>
        <a href="#" class="btn btn-outline-one ms-md-2 ms-0"
          wire:click.prevent="removeDocument('id_document')">Remove File</a>
      </div>
    @endif
  </div>
  <div class="f-w-input capped-width mb-3">
    <label class="form-label">Latest Original Passport (Certified Copy)</label>
    @if (!Auth::user()->passport_document)
      <input type="file" class="btm-border-input file-input-rad"
        wire:model="passport_document">
      @error('passport_document')
        <span class="error text-red">Failed to upload(File must not exceed limit
          of 2MB and file type: pdf, jpeg,
          png)</span>
      @enderror
      </br>
      <span class="text-muted">File: Max File Size 2MB (pdf, jpeg, png)</span>
    @endif
    @if (Auth::user()->passport_document)
      <div class="d-grid d-md-block mt-2 gap-2">
        <a href="{{ url('storage/' . Auth::user()->passport_document) }}"
          target="_blank" class="btn btn-outline-one">View File</a>
        <a href="#" class="btn btn-outline-one ms-md-2 ms-0"
          wire:click.prevent="removeDocument('passport_document')">Remove
          File</a>
      </div>
    @endif
  </div>
  <div class="f-w-input capped-width mb-3">
    <label class="form-label">Driver's License (Certified Copy)</label>
    @if (!Auth::user()->drivers_license_document)
      <input type="file" class="btm-border-input file-input-rad"
        wire:model="drivers_license_document">
      @error('drivers_license_document')
        <span class="error text-red">Failed to upload(File must not exceed limit
          of 2MB and file type: pdf, jpeg,
          png)</span>
      @enderror
      </br>
      <span class="text-muted">File: Max File Size 2MB (pdf, jpeg, png)</span>
    @endif
    @if (Auth::user()->drivers_license_document)
      <div class="d-grid d-md-block mt-2 gap-2">
        <a href="{{ url('storage/' . Auth::user()->drivers_license_document) }}"
          target="_blank" class="btn btn-outline-one">View File</a>
        <a href="#" class="btn btn-outline-one ms-md-2 ms-0"
          wire:click.prevent="removeDocument('drivers_license_document')">Remove
          File</a>
      </div>
    @endif
  </div>
  <div class="f-w-input capped-width mb-3">
    <label class="form-label">Power of Attorney</label>
    @if (!Auth::user()->power_of_attorney_document)
      <input type="file" class="btm-border-input file-input-rad"
        wire:model="power_of_attorney_document">
      @error('power_of_attorney_document')
        <span class="error text-red">Failed to upload(File must not exceed limit
          of 2MB and file type: pdf, jpeg,
          png)</span>
      @enderror
      </br>
      <span class="text-muted">File: Max File Size 2MB (pdf, jpeg, png)</span>
    @endif
    @if (Auth::user()->power_of_attorney_document)
      <div class="d-grid d-md-block mt-2 gap-2">
        <a href="{{ url('storage/' . Auth::user()->power_of_attorney_document) }}"
          target="_blank" class="btn btn-outline-one">View File</a>
        <a href="#" class="btn btn-outline-one ms-md-2 ms-0"
          wire:click.prevent="removeDocument('power_of_attorney_document')">Remove
          File</a>
      </div>
    @endif
  </div>
  <div class="f-w-input capped-width mb-3">
    <label class="form-label">Separate Power of Attorney for your Bank using
      their Forms</label>
    @if (!Auth::user()->separate_power_of_attorney_document)
      <input type="file" class="btm-border-input file-input-rad"
        wire:model="separate_power_of_attorney_document">
      @error('separate_power_of_attorney_document')
        <span class="error text-red">Failed to upload (File must not exceed limit
          of 2MB and file type: pdf, jpeg,
          png)</span>
      @enderror
      </br>
      <span class="text-muted">File: Max File Size 2MB (pdf, jpeg, png)</span>
    @endif
    @if (Auth::user()->separate_power_of_attorney_document)
      <div class="d-grid d-md-block mt-2 gap-2">
        <a href="{{ url('storage/' . Auth::user()->separate_power_of_attorney_document) }}"
          target="_blank" class="btn btn-outline-one">View File</a>
        <a href="#" class="btn btn-outline-one ms-md-2 ms-0"
          wire:click.prevent="removeDocument('separate_power_of_attorney_document')">Remove
          File</a>
      </div>
    @endif
  </div>
  <div class="d-grid mb-3">
    <input class="btn btn-red" type="submit" value="save">
  </div>
  <div class="modal" tabindex="-1" id="tip-modal">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-body">
          <div class="d-flex">
            <h5>{{ $tip_title }}</h5>
            <span class="ms-auto">
              <button type="button" class="btn-close close-modal"
                data-bs-dismiss="modal" aria-label="Close"></button>
            </span>
          </div>
          <div class="mb-3">
            <p class="tip-body">{{ $tip_body }}</p>
          </div>
        </div>
      </div>
    </div>
  </div>
  @push('scripts')
    <script>
      $(document).ready(function() {
        $('.close-modal').on('click', function() {
          $('.modal').modal('hide');
          $('.modal-backdrop').remove();

        });
        window.addEventListener('show-tip-modal', event => {
          $('#tip-modal').modal('show');
        });
      });
    </script>
  @endpush
</form>
