<form wire:submit.prevent="saveMedicalAid">
  @foreach ($medical_aids as $k => $aid)
    <div class="f-w-input mb-3">
      <label class="form-label">Company</label>
      <input type="text" class="btm-border-input"
        wire:model.defer="medical_aids.{{ $k }}.company">
    </div>
    <div class="row">
      <div class="col-md-6">
        <div class="f-w-input mb-3">
          <label class="form-label">Medical Aid Number</label>
          <input type="text" class="btm-border-input"
            wire:model.defer="medical_aids.{{ $k }}.reference_number">
        </div>
      </div>
      <div class="col-md-6">
        <div class="f-w-input mb-3">
          <label class="form-label">Policy Type</label>
          <input type="text" class="btm-border-input"
            wire:model.defer="medical_aids.{{ $k }}.policy_type">
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
        <div class="f-w-input mb-3">
          <label class="form-label">Main Member Name</label>
          <input type="text" class="btm-border-input"
            wire:model.defer="medical_aids.{{ $k }}.name">
        </div>
      </div>
      <div class="col-md-6">
        <div class="f-w-input mb-3">
          <label class="form-label">Main Member Surname</label>
          <input type="text" class="btm-border-input"
            wire:model.defer="medical_aids.{{ $k }}.surname">
        </div>
      </div>
    </div>
    <div class="col-md-12">
      <div class="f-w-input mb-3">
        <label class="form-label">Main Member ID Number</label>
        <input type="text" class="btm-border-input"
          wire:model.defer="medical_aids.{{ $k }}.id_number">
      </div>
    </div>

    <div class="f-w-input mb-3">
      <label class="form-label">Copy of Medical Aid Certificate</label>
      @if (isset($aid['medical_aid_certificate_file']) && isset($aid['id']))
        <div class="d-grid d-md-block mt-2 gap-2">
          <a href="{{ url('storage/' . $aid['medical_aid_certificate_file']) }}"
            target="_blank" class="btn btn-outline-one upload-view-btns">View
            File</a>
          <a href="#" target="_blank"
            class="btn btn-outline-one ms-md-2 upload-view-btns ms-0"
            wire:click.prevent="removeMedicalAidDocument('medical_aid_certificate', {{ $aid['id'] }})">Remove
            File</a>
        </div>
      @else
        <input type="file" class="btm-border-input file-input-rad"
          wire:model.defer="medical_aids.{{ $k }}.medical_aid_certificate">
        @error('medical_aids.' . $k . '.medical_aid_certificate')
          <span class="error text-red">Failed to upload(File must not exceed limit
            of 2MB and file type: pdf, jpeg,
            png)</span>
        @enderror
        </br>
        <span class="text-muted">File: Max File Size 2MB (pdf, jpeg, png)</span>
      @endif
    </div>

    <div class="col-md-12">
      <div class="row">
        <div class="col-md-12">
          <hr />
        </div>
        <div class="col-md-12 d-flex">
          <p class="">Conditions</p>
          <span class="ms-auto">
            <a href="#" class="btn btn-outline-two"
              wire:click.prevent="addCondition({{ $k }})">
              Add Condition
            </a>
          </span>
        </div>
        @foreach ($aid['conditions'] as $kkk => $cnd)
          <div class="col-md-12">
            <div class="f-w-input mb-3">
              <label class="form-label">
                Conditions: Describe the person's condition(s) in your own
                words
              </label>
              <br />
              <textarea class="btm-border-input w-100"
                wire:model.defer="medical_aids.{{ $k }}.conditions.{{ $kkk }}.condition"></textarea>
            </div>
          </div>
          @if (isset($cnd['id']))
            <div class="col-md-12 d-grid d-md-block mt-2 gap-2">
              <a href="#" class="btn btn-outline-one"
                wire:click.prevent="deleteCondition({{ $cnd['id'] }})">Remove</a>
            </div>
          @endif
        @endforeach
        <div class="col-md-12">
          <hr />
        </div>
      </div>
    </div>

    @if (isset($aid['dependents']))
      @if (count($aid['dependents']) > 0)
        <div class="mt-2">
          <p>
            Dependents
          </p>
          <span class="text-danger"><b>NOTE:</b> To add a specific dependent,
            please complete the Proof of Marriage and
            Children sections in your Organiser, or select Other below to
            specify.</span>
        </div>
        <div class="__dependents mb-3">
          @foreach ($aid['dependents'] as $kk => $dependent)
            <div class="row __dependent">
              @if (isset($dependent['id']))
                <div class="col-md-6">
                  <div class="f-w-input mb-3">
                    <label class="form-label">Name</label>
                    <input type="text" class="btm-border-input"
                      wire:model.defer="medical_aids.{{ $k }}.dependents.{{ $kk }}.name">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="f-w-input mb-3">
                    <label class="form-label">Surname</label>
                    <input type="text" class="btm-border-input"
                      wire:model.defer="medical_aids.{{ $k }}.dependents.{{ $kk }}.surname">
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="f-w-input mb-3">
                    <label class="form-label">ID Number</label>
                    <input type="text" class="btm-border-input"
                      wire:model.defer="medical_aids.{{ $k }}.dependents.{{ $kk }}.id_number">
                  </div>

                  <div class="mb-3">
                    <label class="form-label">Condition</label>
                    <br />
                    <input class="btm-border-input w-100" name="condition"
                      wire:model.lazy="medical_aids.{{ $k }}.dependents.{{ $kk }}.condition">
                  </div>

                  <div class="d-grid d-md-block mt-2 gap-2">
                    @if (isset($dependent['id']))
                      <a href="#" class="btn btn-outline-two"
                        wire:click.prevent="removeDependent({{ $dependent['id'] }})">Remove</a>
                    @endif
                  </div>
                  <hr />
                </div>
              @else
                <div class="col-md-12">
                  <div class="f-w-input mb-3">
                    <label class="form-label">Dependent Name & Surname</label>
                    <select class="btm-border-input" name="select_id"
                      :wire:key="new_ben_{{ $kk }}_sel"
                      wire:model.lazy="medical_aids.{{ $k }}.dependents.{{ $kk }}.select_id">
                      <option value="">Select Option</option>
                      @foreach ($dependent_select as $key => $select)
                        <option value="{{ $select['option_id'] }}"
                          :wire:key="new_ben_op_{{ $key }}_sel">
                          {{ $select['option_name'] }}
                          {{ $select['option_surname'] }}</option>
                      @endforeach
                      <option value="other">Other</option>
                    </select>
                  </div>
                </div>
                @if ($medical_aids[$k]['dependents'][$kk]['select_id'] == 'other')
                  <div class="col-md-6">
                    <div class="f-w-input mb-3">
                      <label class="form-label">Name</label>
                      <input type="text" class="btm-border-input"
                        wire:model.defer="medical_aids.{{ $k }}.dependents.{{ $kk }}.name">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="f-w-input mb-3">
                      <label class="form-label">Surname</label>
                      <input type="text" class="btm-border-input"
                        wire:model.defer="medical_aids.{{ $k }}.dependents.{{ $kk }}.surname">
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="f-w-input mb-3">
                      <label class="form-label">ID Number</label>
                      <input type="text" class="btm-border-input"
                        wire:model.defer="medical_aids.{{ $k }}.dependents.{{ $kk }}.id_number">
                    </div>
                  </div>
                @endif
                <div class="col-md-12">
                  <div class="mb-3">
                    <label class="form-label">Condition</label>
                    <br />
                    <textarea class="btm-border-input w-100" name="condition"
                      wire:model.lazy="medical_aids.{{ $k }}.dependents.{{ $kk }}.condition"></textarea>
                  </div>
                </div>
              @endif
            </div>
          @endforeach
        </div>
      @endif
    @endif
    <div class="row mb-3">
      <div class="col-md-12 d-flex">
        <span class="ms-auto">
          <a href="#" class="btn btn-outline-two"
            wire:click.prevent="addDependent({{ $k }})">
            <svg width="16" height="17" viewBox="0 0 16 17"
              fill="none" xmlns="http://www.w3.org/2000/svg">
              <path
                d="M12.6666
            9.16536H8.66659V13.1654H7.33325V9.16536H3.33325V7.83203H7.33325V3.83203H8.66659V7.83203H12.6666V9.16536Z"
                fill="#CF3D60" />
            </svg>
            Add Dependent
          </a>
        </span>
      </div>
    </div>
  @endforeach
  <div class="row mb-3">
    <hr />
    <div class="col-md-12 d-flex">
      <span class="ms-auto">
        <span class="pe-2">
          <i class="fa fa-info-circle"
            style="cursor: pointer; font-size: 20px;" data-bs-toggle="modal"
            data-bs-target="#medical-aid-description"></i>
        </span>
        <a href="#" class="btn btn-outline-two"
          wire:click.prevent="addMedicalaid">
          <svg width="16" height="17" viewBox="0 0 16 17"
            fill="none" xmlns="http://www.w3.org/2000/svg">
            <path
              d="M12.6666
            9.16536H8.66659V13.1654H7.33325V9.16536H3.33325V7.83203H7.33325V3.83203H8.66659V7.83203H12.6666V9.16536Z"
              fill="#CF3D60" />
          </svg>
          Add Policy
        </a>
      </span>
    </div>
  </div>
  <div
    class="col-md-12 justify-content-end upload-view-btns d-grid mb-4 gap-2">
    <input type="submit" class="btn btn-red" value="save">
  </div>
</form>
<div class="modal" tabindex="-1" id="medical-aid-description">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-body">
        <div class="d-flex">
          <h5>Medical Aid</h5>
          <span class="ms-auto">
            <button type="button" class="btn-close" data-bs-dismiss="modal"
              aria-label="Close"></button>
          </span>
        </div>
        <div class="mb-5">
          <p>The 'Medical Fund' section enables you to document your medical
            fund details and those of the dependents
            covered by the fund. This information can be shared with your loved
            ones whom you believe may need it in
            case of an emergency.</p>
        </div>
      </div>
    </div>
  </div>
</div>
