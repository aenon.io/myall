<div class="pe-3 text-end">
  <i class="fa fa-info-circle" style="cursor: pointer; font-size: 20px;" data-bs-toggle="modal"
    data-bs-target="#work-description"></i>
</div>
<form wire:submit.prevent="savePlaceOfWork">
  <div class="f-w-input mb-3">
    <label class="form-label">Name of Employer</label>
    <input type="text" class="btm-border-input" wire:model.defer="employer_name">
    @error('employer_name')
      <span class="error text-red">{{ $message }}</span>
    @enderror
  </div>
  <div class="f-w-input mb-3">
    <label class="form-label">Address</label>
    <input type="text" class="btm-border-input" wire:model.defer="employer_address">
    @error('employer_address')
      <span class="error text-red">{{ $message }}</span>
    @enderror
  </div>
  <div class="f-w-input mb-3">
    <label class="form-label">Salary Number</label>
    <input type="text" class="btm-border-input" wire:model.defer="salary_number">
    @error('salary_number')
      <span class="error text-red">{{ $message }}</span>
    @enderror
  </div>
  <div class="row">
    <div class="col-md-12 myallButtons">
      <div class="d-grid __save mb-3">
        <input type="submit" class="btn btn-red" value="save">
      </div>
    </div>
  </div>
</form>

<div class="modal" tabindex="-1" id="work-description">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-body">
        <div class="d-flex">
          <h5>Work</h5>
          <span class="ms-auto">
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </span>
        </div>
        <div class="mb-5">
          <p>In 'Work Details,' you can document basic information about your employer. This information will be
            accessible to your executor after your passing and can be crucial in case of an emergency. Please share this
            information with the person you believe will need it.</p>
        </div>
      </div>
    </div>
  </div>
</div>
