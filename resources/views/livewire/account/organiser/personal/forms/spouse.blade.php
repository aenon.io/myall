<form wire:submit.prevent="saveSpouse">
  @foreach ($spouses as $s => $spouse)
    <div class="f-w-input mb-3">
      <label class="form-label">Marital Status</label>
      <select class="btm-border-input"
        wire:model.lazy="spouses.{{ $s }}.marital_status">
        <option value="">Select Option</option>
        @php
          $found_cur_sp_status = false;
        @endphp
        @if ($marital_statuses)
          @foreach ($marital_statuses as $st)
            @foreach ($st as $title => $status)
              @php
                if ($spouse['marital_status'] == $status) {
                    $found_cur_sp_status = true;
                }
              @endphp
              <option value="{{ $status }}" title="{{ $title }}">
                {{ $status }}</option>
            @endforeach
          @endforeach
        @endif
        @if (isset($spouse['id']))
          @if (!$found_cur_sp_status)
            <option value"{{ $spouse['marital_status'] }}" selected>
              {{ $spouse['marital_status'] }}</option>
          @endif
        @endif
      </select>
      @if ($spouse['marital_status'])
        @php
          $status_des = null;
          foreach ($marital_statuses as $mar_st) {
              if (array_search($spouse['marital_status'], $mar_st)) {
                  $status_des = array_search(
                      $spouse['marital_status'],
                      $mar_st,
                  );
              }
          }
        @endphp
        <div class="form-text">{{ $status_des }}</div>
      @endif
      @error('marital_status')
        <span class="error text-red">{{ $message }}</span>
      @enderror
    </div>

    @if ($spouse['marital_status'] != 'Never Married')
      <div class="mb-3">
        <b>Spouse Details</b>
      </div>
      <div class="f-w-input mb-3">
        @if ($spouse['marital_status'] == 'Widowed')
          <label class="form-label">Late Spouse’s Name</label>
        @else
          <label class="form-label">Name</label>
        @endif
        <input type="text" class="btm-border-input"
          wire:model.defer="spouses.{{ $s }}.spouse_name">
      </div>
      <div class="f-w-input mb-3">
        @if ($spouse['marital_status'] == 'Widowed')
          <label class="form-label">Late Spouse’s Middle Name</label>
        @else
          <label class="form-label">Middle Name</label>
        @endif
        <input type="text" class="btm-border-input"
          wire:model.defer="spouses.{{ $s }}.spouse_middle_name">
      </div>
      <div class="f-w-input mb-3">
        @if ($spouse['marital_status'] == 'Widowed')
          <label class="form-label">Late Spouse's Surname</label>
        @else
          <label class="form-label">Surname</label>
        @endif
        <input type="text" class="btm-border-input"
          wire:model.defer="spouses.{{ $s }}.spouse_surname">
      </div>
      <div class="f-w-input mb-3">
        @if ($spouse['marital_status'] == 'Widowed')
          <label class="form-label">Late Spouse's ID Number</label>
        @else
          <label class="form-label">ID Number</label>
        @endif
        <input type="text" class="btm-border-input"
          wire:model.defer="spouses.{{ $s }}.spouse_id_number">
      </div>
      @if ($spouse['marital_status'] == 'Now Married')
        <div class="f-w-input mb-3">
          <label class="form-label">Marriage Certificate</label>
          @if (isset($spouse['marriage_certificate_file']) && isset($spouse['id']))
            <div class="d-grid d-md-block mt-2 gap-2">
              <a href="{{ url('storage/' . $spouse['marriage_certificate_file']) }}"
                target="_blank"
                class="btn btn-outline-one upload-view-btns">View File</a>
              <a href="#" target="_blank"
                class="btn btn-outline-one ms-md-2 upload-view-btns ms-0"
                wire:click.prevent="removeSposeDocument('marriage_certificate',{{ $spouse['id'] }})">Remove
                File</a>
            </div>
          @else
            <input type="file" class="btm-border-input file-input-rad"
              wire:key="{{ $s }}.marriage_certificate"
              wire:model="spouses.{{ $s }}.marriage_certificate">
            @error('spouses.' . $s . '.marriage_certificate')
              <span class="error text-red">Failed to upload(File must not exceed
                limit of 2MB and file type: pdf, jpeg,
                png)</span>
            @enderror
            </br>
            <span class="text-muted">File: Max File Size 2MB (pdf, jpeg,
              png)</span>
          @endif
        </div>
        <div class="f-w-input mb-3">
          <label class="form-label">Antenuptial Contract</label>
          @if (isset($spouse['antenuptial_contract_file']) && isset($spouse['id']))
            <div class="d-grid d-md-block mt-2 gap-2">
              <a href="{{ url('storage/' . $spouse['antenuptial_contract_file']) }}"
                target="_blank"
                class="btn btn-outline-one upload-view-btns">View File</a>
              <a href="#" target="_blank"
                class="btn btn-outline-one ms-md-2 upload-view-btns ms-0"
                wire:click.prevent="removeSposeDocument('antenuptial_contract',{{ $spouse['id'] }})">Remove
                File</a>
            </div>
          @else
            <input type="file" class="btm-border-input file-input-rad"
              wire:key="{{ $s }}.antenuptial_contract"
              wire:model="spouses.{{ $s }}.antenuptial_contract">
            @error('spouses.' . $s . '.antenuptial_contract')
              <span class="error text-red">Failed to upload(File must not exceed
                limit of 2MB and file type: pdf, jpeg,
                png)</span>
            @enderror
            </br>
            <span class="text-muted">File: Max File Size 2MB (pdf, jpeg,
              png)</span>
          @endif
        </div>
      @endif
    @endif
    @if ($spouse['marital_status'] == 'Widowed')
      <div class="f-w-input mb-3">
        <label class="form-label">Late Spouse's Date of Death</label>
        <input type="date" class="btm-border-input" max="2999-12-31"
          wire:model.defer="spouses.{{ $s }}.date_of_death">
      </div>
      <div class="f-w-input mb-3">
        <label class="form-label">Masters Office(where the estate of the late
          spouse was registered)</label>
        <select class="btm-border-input" name="masters_office"
          wire:model.defer="spouses.{{ $s }}.masters_office">
          <option value="" selected>Select Option</option>
          @foreach ($masters_offices as $off)
            <option value="{{ $off }}">{{ $off }}</option>
          @endforeach
        </select>
      </div>
      <div class="f-w-input mb-3">
        <label class="form-label">Estate Number of Late Spouse's Estate</label>
        <input type="text" class="btm-border-input"
          wire:model.defer="spouses.{{ $s }}.estate_number">
      </div>
      <div class="f-w-input mb-3">
        <label class="form-label">Executor of the Late Spouse’s Estate</label>
        <input type="text" class="btm-border-input"
          wire:model.defer="spouses.{{ $s }}.executor">
      </div>
      <div class="mb-3">
        <div class="form-check">
          <input class="form-check-input" type="checkbox" name="usufruct"
            id="usufruct" value="usufruct"
            wire:model.defer="spouses.{{ $s }}.usufruct">
          <label class="form-check-label" for="usufruct">
            Was a Usufruct created in the Late Spouse’s Will?
          </label>
        </div>
        <div class="form-check">
          <input class="form-check-input" type="checkbox"
            name="dweling_property" id="fideicommissum"
            value="fideicommissum"
            wire:model.defer="spouses.{{ $s }}.dweling_property">
          <label class="form-check-label" for="fideicommissum">
            Was a Fideicommisssum created in the Late Spouse’s Will?
          </label>
        </div>
      </div>
      <div class="f-w-input mb-3">
        <label class="form-label">Death Certificate</label>
        @if (isset($spouse['death_certificate_file']) && isset($spouse['id']))
          <div class="d-grid d-md-block mt-2 gap-2">
            <a href="{{ url('storage/' . $spouse['death_certificate_file']) }}"
              target="_blank"
              class="btn btn-outline-one upload-view-btns">View File</a>
            <a href="#" target="_blank"
              class="btn btn-outline-one ms-md-2 upload-view-btns ms-0"
              wire:click.prevent="removeSposeDocument('death_certificate',{{ $spouse['id'] }})">Remove
              File</a>
          </div>
        @else
          <input type="file" class="btm-border-input file-input-rad"
            wire:key="{{ $s }}.death_certificate"
            wire:model="spouses.{{ $s }}.death_certificate">
          @error('spouses.' . $s . '.death_certificate')
            <<span class="error text-red">Failed to upload(File must not exceed
              limit of 2MB and file type: pdf, jpeg,
              png)</span>
            @enderror
            </br>
            <span class="text-muted">File: Max File Size 2MB (pdf, jpeg,
              png)</span>
        @endif
      </div>
    @endif
    @if ($spouse['marital_status'] == 'Domestic Partnership')
      <div class="f-w-input mb-3">
        <label class="form-label">Agreement</label>
        @if (isset($spouse['domestic_agreement_file']) && isset($spouse['id']))
          <div class="d-grid d-md-block mt-2 gap-2">
            <a href="{{ url('storage/' . $spouse['domestic_agreement_file']) }}"
              target="_blank"
              class="btn btn-outline-one upload-view-btns">View File</a>
            <a href="#" target="_blank"
              class="btn btn-outline-one ms-md-2 upload-view-btns ms-0"
              wire:click.prevent="removeSposeDocument('domestic_agreement',{{ $spouse['id'] }})">Remove
              File</a>
          </div>
        @else
          <input type="file" class="btm-border-input file-input-rad"
            wire:key="{{ $s }}.domestic_agreement"
            wire:model="spouses.{{ $s }}.domestic_agreement">
          @error('spouses.' . $s . '.domestic_agreement')
            <span class="error text-red">Failed to upload(File must not exceed
              limit of 2MB and file type: pdf, jpeg,
              png)</span>
          @enderror
          </br>
          <span class="text-muted">File: Max File Size 2MB (pdf, jpeg,
            png)</span>
        @endif
      </div>
    @endif
    {{--
        @if ($spouse['marital_status'] == 'Now Married')
            <div class="mb-3 f-w-input">
                <label class="form-label">Specific Agreements</label>
                <input type="text" class="btm-border-input" wire:model.defer="spouses.{{ $s }}.specific_agreements_description">
            </div>
            <div class="mb-3 f-w-input">
                <label class="form-label">Specific Agreements</label>
                <input type="file" class="btm-border-input file-input-rad" wire:key="{{ $s }}.specific_agreements_file" wire:model.defer="spouses.{{ $s }}.specific_agreements_file">
                @if (isset($spouse['specific_agreements_file_file']))
                <div class="mt-1">
                    <a href="{{ url('storage/'.$spouse['specific_agreements_file_file']) }}" target="_blank" class="btn btn-outline-one">View File</a>
                    <a href="#" target="_blank" class="btn btn-outline-one ms-0 ms-md-2" wire:click.prevent="removeSposeDocument('specific_agreements_file',{{ $spouse['id'] }})">Remove File</a>
                </div>
                @endif
            </div>
        @endif
        --}}
    @if ($spouse['marital_status'] == 'Divorced')
      <div class="f-w-input mb-3">
        <label class="form-label">Date of Divorce</label>
        <input type="date" class="btm-border-input" max="2999-12-31"
          wire:model.defer="spouses.{{ $s }}.date_of_divorce">
      </div>
      <div class="f-w-input mb-3">
        <label class="form-label">Court Order</label>
        @if (isset($spouse['court_order_and_agreement_file']) && isset($spouse['id']))
          <div class="d-grid d-md-block mt-2 gap-2">
            <a href="{{ url('storage/' . $spouse['court_order_and_agreement_file']) }}"
              target="_blank"
              class="btn btn-outline-one upload-view-btns">View File</a>
            <a href="#" target="_blank"
              class="btn btn-outline-one ms-md-2 upload-view-btns ms-0"
              wire:click.prevent="removeSposeDocument('court_order_and_agreement',{{ $spouse['id'] }})">Remove
              File</a>
          </div>
        @else
          <input type="file" class="btm-border-input file-input-rad"
            wire:key="{{ $s }}.court_order_and_agreement"
            wire:model="spouses.{{ $s }}.court_order_and_agreement">
          @error('spouses.' . $s . '.court_order_and_agreement')
            <span class="error text-red">Failed to upload(File must not exceed
              limit of 2MB and file type: pdf, jpeg,
              png)</span>
          @enderror
          </br>
          <span class="text-muted">File: Max File Size 2MB (pdf, jpeg,
            png)</span>
        @endif
      </div>
      <div class="f-w-input mb-3">
        <label class="form-label">Agreement</label>
        @if (isset($spouse['divorce_agreement_file']) && isset($spouse['id']))
          <div class="d-grid d-md-block mt-2 gap-2">
            <a href="{{ url('storage/' . $spouse['divorce_agreement_file']) }}"
              target="_blank"
              class="btn btn-outline-one upload-view-btns">View File</a>
            <a href="#" target="_blank"
              class="btn btn-outline-one ms-md-2 upload-view-btns ms-0"
              wire:click.prevent="removeSposeDocument('divorce_agreement',{{ $spouse['id'] }})">Remove
              File</a>
          </div>
        @else
          <input type="file" class="btm-border-input file-input-rad"
            wire:key="{{ $s }}.divorce_agreement"
            wire:model="spouses.{{ $s }}.divorce_agreement">
          @error('spouses.' . $s . '.divorce_agreement')
            <span class="error text-red">Failed to upload(File must not exceed
              limit of 2MB and file type: pdf, jpeg,
              png)</span>
          @enderror
          </br>
          <span class="text-muted">File: Max File Size 2MB (pdf, jpeg,
            png)</span>
        @endif
      </div>

      <div class="mb-3">
        <div class="form-check">
          <input class="form-check-input" type="radio"
            name="maintenance_order" id="yes_maintenance_order"
            value="yes"
            wire:model.lazy="spouses.{{ $s }}.maintenance_order">
          <label class="form-check-label" for="yes_maintenance_order">
            With Maintenance Order
          </label>
        </div>
        <div class="form-check">
          <input class="form-check-input" type="radio"
            name="maintenance_order" id="no_maintenance_order" value="no"
            wire:model.lazy="spouses.{{ $s }}.maintenance_order">
          <label class="form-check-label" for="no_maintenance_order">
            Without Maintenance Order
          </label>
        </div>
      </div>
      @if ($spouse['maintenance_order'] == 'yes')
        <div class="f-w-input mb-3">
          <label class="form-label">Proof of Maintenance Order</label>
          @if (!isset($spouse['proof_of_maintenance_order_file']))
            <input type="file" class="btm-border-input file-input-rad"
              wire:key="{{ $s }}.proof_of_maintenance_order"
              wire:model="spouses.{{ $s }}.proof_of_maintenance_order">
            @error('spouses.' . $s . '.proof_of_maintenance_order')
              <span class="error text-red">Failed to upload(File must not exceed
                limit of 2MB and file type: pdf, jpeg,
                png)</span>
            @enderror
            </br>
            <span class="text-muted">File: Max File Size 2MB (pdf, jpeg,
              png)</span>
          @endif
          @if (isset($spouse['proof_of_maintenance_order_file']) && isset($spouse['id']))
            <div class="d-grid d-md-block mt-2 gap-2">
              <a href="{{ url('storage/' . $spouse['proof_of_maintenance_order_file']) }}"
                target="_blank"
                class="btn btn-outline-one upload-view-btns">View File</a>
              <a href="#" target="_blank"
                class="btn btn-outline-one upload-view-btns ms-2"
                wire:click.prevent="removeSposeDocument('proof_of_maintenance_order',{{ $spouse['id'] }})">Remove
                File</a>
            </div>
          @endif
        </div>
      @endif
    @endif
    @if ($spouse['marital_status'] == 'Now Married')
      <div class="mb-3">
        <div class="form-check">
          <input class="form-check-input" type="radio"
            name="comunity_marriage_{{ $s }}"
            id="in_comunity_{{ $s }}" value="yes"
            wire:model="spouses.{{ $s }}.comunity_marriage">
          <label class="form-check-label"
            for="in_comunity_{{ $s }}">
            In Community
          </label>
        </div>
        <div class="form-check">
          <input class="form-check-input" type="radio"
            name="comunity_marriage_{{ $s }}"
            id="out_of_community_{{ $s }}" value="no"
            wire:model="spouses.{{ $s }}.comunity_marriage">
          <label class="form-check-label"
            for="out_of_community_{{ $s }}">
            Out of Community
          </label>
        </div>
      </div>
      @if ($spouse['comunity_marriage'] == 'no')
        <div class="mb-3">
          <div class="form-check">
            <input class="form-check-input" type="radio"
              name="accrual_{{ $s }}"
              id="yes_accrual_{{ $s }}" value="yes"
              wire:model.defer="spouses.{{ $s }}.accrual">
            <label class="form-check-label"
              for="yes_accrual_{{ $s }}">
              With Accrual
            </label>
          </div>
          <div class="form-check">
            <input class="form-check-input" type="radio"
              name="accrual_{{ $s }}"
              id="no_accrual_{{ $s }}" value="no"
              wire:model.defer="spouses.{{ $s }}.accrual">
            <label class="form-check-label"
              for="no_accrual_{{ $s }}">
              Without Accrual
            </label>
          </div>
        </div>
      @endif
    @endif
    @if (isset($spouse['id']))
      <div class="col-md-12 d-grid d-md-block mt-2 gap-2 text-end">
        <a href="#" class="btn btn-outline-two"
          wire:click.prevent="removeSpouse({{ $spouse['id'] }})">Remove
          Spouse</a>
      </div>
    @endif
    <div class="col-md-12">
      <hr />
    </div>
  @endforeach
  <div class="row mb-3">
    @if (!$isNeverMarried)
      <div class="col-md-12 d-flex">
        <span class="ms-auto">
          <i class="fa fa-info-circle me-3 ms-3"
            wire:click.prevent="showTipModal('spouse')"></i>
          <a href="#" class="btn btn-outline-two"
            wire:click.prevent="addSpouse">
            <svg width="16" height="17" viewBox="0 0 16 17"
              fill="none" xmlns="http://www.w3.org/2000/svg">
              <path
                d="M12.6666
            9.16536H8.66659V13.1654H7.33325V9.16536H3.33325V7.83203H7.33325V3.83203H8.66659V7.83203H12.6666V9.16536Z"
                fill="#CF3D60" />
            </svg>
            Add Spouse
          </a>
        </span>
      </div>
    @endif
  </div>
  <div class="d-grid mb-3">
    <input type="submit" class="btn btn-red" value="save">
  </div>
</form>
<div class="modal" tabindex="-1" id="spouse-info">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-body">
        <div class="d-flex">
          <h5>Multiple spouses</h5>
          <span class="ms-auto">
            <button type="button" class="btn-close" data-bs-dismiss="modal"
              aria-label="Close"></button>
          </span>
        </div>
        <p>In SA law, customary marriages are allowed. A customary marriage is a
          union that is formed in terms of
          indigenous African customary law. This is law that allows a Black
          South African man to have more than one
          wife.</p>
      </div>
      <div class="modal-footer">
        <!--<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>-->
        <button type="button" class="btn btn-outline-two"
          data-bs-dismiss="modal">Continue</button>
      </div>
    </div>
  </div>
</div>

<div class="modal" tabindex="-1" id="spouse-tip-modal">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-body">
        <div class="d-flex">
          <h5>{{ $tip_title }}</h5>
          <span class="ms-auto">
            <button type="button" class="btn-close" data-bs-dismiss="modal"
              aria-label="Close" id="close-tip-modal"></button>
          </span>
        </div>
        <div class="mb-3">
          <p class="tip-body">{{ $tip_body }}</p>
        </div>
      </div>
    </div>
  </div>
</div>

@push('scripts')
  <script>
    $(document).ready(function() {
      // Function to remove modal backdrop when the modal is closed
      function removeBackdrop() {
        $('.modal-backdrop').remove();
        $('body').css("overflow", "scroll");
      }

      window.addEventListener('show-tip-modal', event => {
        $('#spouse-tip-modal').modal('show');
      });

      // When the modal is hidden (closed), remove the backdrop manually
      $('#spouse-tip-modal').on('hidden.bs.modal', removeBackdrop);

      // Manually hide the modal and remove the backdrop when the close button is clicked
      $('#close-tip-modal').click(function() {
        $('#spouse-tip-modal').modal('hide');
        removeBackdrop();
      });
    });
  </script>
@endpush
