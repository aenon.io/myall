<div>
  <div class="row">
    <div class="col-md-12 text-end">
      <a href="#" wire:click.prevent="showTipModal('spouse')"><i
          class="fa fa-info-circle text-blue me-3 ms-3"></i></a>
    </div>
  </div>
  <form wire:submit.prevent="saveSpouse">
    @foreach ($spouses as $key => $spouse)
      <div class="__item __spouse mb-3">
        @if (isset($spouse['id']))
          <div class="row">
            <div class="col-md-12 d-grid d-md-block mt-2 gap-2 text-end">
              <a href="#" class="btn btn-outline-two"
                wire:click.prevent="removeSpouse({{ $spouse['id'] }})">
                <svg width="16" height="17" viewBox="0 0 16 17"
                  fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path
                    d="M10.6668 6.5V13.1667H5.3335V6.5H10.6668ZM9.66683 2.5H6.3335L5.66683 3.16667H3.3335V4.5H12.6668V3.16667H10.3335L9.66683 2.5ZM12.0002 5.16667H4.00016V13.1667C4.00016 13.9 4.60016 14.5 5.3335 14.5H10.6668C11.4002 14.5 12.0002 13.9 12.0002 13.1667V5.16667Z"
                    fill="#CF3D60" />
                </svg>
                Remove
              </a>
            </div>
          </div>
        @endif
        <div class="row">
          <div class="col-md-12">
            <div class="f-w-input mb-3">
              <label class="form-label">Marital Status</label>
              <select class="btm-border-input"
                wire:model.lazy="spouses.{{ $key }}.marital_status">
                <option value="">Select Option</option>
                @foreach ($marital_statuses as $status => $des)
                  @if (count($filter_marital_status) > 0 && $spouse['marital_status'] != $status)
                    @if (!in_array($status, $filter_marital_status))
                      <option value="{{ $status }}">{{ $status }}
                      </option>
                    @endif
                  @else
                    <option value="{{ $status }}">{{ $status }}
                    </option>
                  @endif
                @endforeach
              </select>
              @if ($spouse['marital_status'])
                <div class="form-text">
                  {{ $marital_statuses[$spouse['marital_status']] }}</div>
              @endif
            </div>
          </div>
        </div>
        @if ($spouse['marital_status'] != 'Never Married')
          @php
            $late = '';
            if ($spouse['marital_status'] == 'Widowed') {
                $late = "Late Spouse's";
            }
          @endphp
          <div class="row">
            <div class="col-md-12">
              <div class="mb-3">
                <b>Spouse Details</b>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="f-w-input mb-3">
                <label class="form-label">{{ $late }} Name</label>
                <input type="text" class="btm-border-input"
                  wire:model.defer="spouses.{{ $key }}.spouse_name">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="f-w-input mb-3">
                <label class="form-label">{{ $late }} Middle
                  Name</label>
                <input type="text" class="btm-border-input"
                  wire:model.defer="spouses.{{ $key }}.spouse_middle_name">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="f-w-input mb-3">
                <label class="form-label">{{ $late }} Surname</label>
                <input type="text" class="btm-border-input"
                  wire:model.defer="spouses.{{ $key }}.spouse_surname">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="f-w-input mb-3">
                <label class="form-label">{{ $late }} ID Number</label>
                <input type="text" class="btm-border-input"
                  wire:model.defer="spouses.{{ $key }}.spouse_id_number">
              </div>
            </div>
          </div>
          @if ($spouse['marital_status'] == 'Now Married')
            <div class="row">
              <div class="col-md-12">
                <div class="f-w-input mb-3">
                  <label class="form-label">Marriage Certificate</label>
                  @if ($spouse['marriage_certificate_file'])
                    <div class="d-grid d-md-block mt-2 gap-2">
                      <a href="{{ url('storage/' . $spouse['marriage_certificate_file']) }}"
                        target="_blank"
                        class="btn btn-outline-one upload-view-btns">View
                        File</a>
                      <a href="#" target="_blank"
                        class="btn btn-outline-one ms-md-2 upload-view-btns ms-0"
                        wire:click.prevent="removeSposeDocument('marriage_certificate',{{ $spouse['id'] }})">Remove
                        File</a>
                    </div>
                  @else
                    <input type="file"
                      class="btm-border-input file-input-rad"
                      wire:key="{{ $key }}.marriage_certificate"
                      wire:model="spouses.{{ $key }}.marriage_certificate">
                    <span class="text-muted">File: Max File Size 2MB (pdf, jpeg,
                      png)</span>
                  @endif
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="f-w-input mb-3">
                  <label class="form-label">Antenuptial Contract</label>
                  @if ($spouse['antenuptial_contract_file'])
                    <div class="d-grid d-md-block mt-2 gap-2">
                      <a href="{{ url('storage/' . $spouse['antenuptial_contract_file']) }}"
                        target="_blank"
                        class="btn btn-outline-one upload-view-btns">View
                        File</a>
                      <a href="#" target="_blank"
                        class="btn btn-outline-one ms-md-2 upload-view-btns ms-0"
                        wire:click.prevent="removeSposeDocument('antenuptial_contract',{{ $spouse['id'] }})">Remove
                        File</a>
                    </div>
                  @else
                    <input type="file"
                      class="btm-border-input file-input-rad"
                      wire:key="{{ $key }}.antenuptial_contract"
                      wire:model="spouses.{{ $key }}.antenuptial_contract">
                    <span class="text-muted">File: Max File Size 2MB (pdf, jpeg,
                      png)</span>
                  @endif
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="mb-3">
                  <div class="form-check">
                    <input class="form-check-input" type="radio"
                      name="comunity_marriage_{{ $key }}"
                      id="in_comunity_{{ $key }}" value="yes"
                      wire:model="spouses.{{ $key }}.comunity_marriage">
                    <label class="form-check-label"
                      for="in_comunity_{{ $key }}">
                      In Community
                    </label>
                  </div>
                  <div class="form-check">
                    <input class="form-check-input" type="radio"
                      name="comunity_marriage_{{ $key }}"
                      id="out_of_community_{{ $key }}"
                      value="no"
                      wire:model="spouses.{{ $key }}.comunity_marriage">
                    <label class="form-check-label"
                      for="out_of_community_{{ $key }}">
                      Out of Community
                    </label>
                  </div>
                </div>
              </div>
            </div>
            @if ($spouse['comunity_marriage'] == 'no')
              <div class="row">
                <div class="col-md-12">
                  <div class="mb-3">
                    <div class="form-check">
                      <input class="form-check-input" type="radio"
                        name="accrual_{{ $key }}"
                        id="yes_accrual_{{ $key }}" value="yes"
                        wire:model.defer="spouses.{{ $key }}.accrual">
                      <label class="form-check-label"
                        for="yes_accrual_{{ $key }}">
                        With Accrual
                      </label>
                    </div>
                    <div class="form-check">
                      <input class="form-check-input" type="radio"
                        name="accrual_{{ $key }}"
                        id="no_accrual_{{ $key }}" value="no"
                        wire:model.defer="spouses.{{ $key }}.accrual">
                      <label class="form-check-label"
                        for="no_accrual_{{ $key }}">
                        Without Accrual
                      </label>
                    </div>
                  </div>
                </div>
              </div>
            @endif
          @endif
          @if ($spouse['marital_status'] == 'Domestic Partnership')
            <div class="row">
              <div class="col-md-12">
                <div class="f-w-input mb-3">
                  <label class="form-label">Agreement</label>
                  @if ($spouse['domestic_agreement_file'])
                    <div class="d-grid d-md-block mt-2 gap-2">
                      <a href="{{ url('storage/' . $spouse['domestic_agreement_file']) }}"
                        target="_blank"
                        class="btn btn-outline-one upload-view-btns">View
                        File</a>
                      <a href="#" target="_blank"
                        class="btn btn-outline-one ms-md-2 upload-view-btns ms-0"
                        wire:click.prevent="removeSposeDocument('domestic_agreement',{{ $spouse['id'] }})">Remove
                        File</a>
                    </div>
                  @else
                    <input type="file"
                      class="btm-border-input file-input-rad"
                      wire:key="{{ $key }}.domestic_agreement"
                      wire:model="spouses.{{ $key }}.domestic_agreement">
                    <span class="text-muted">File: Max File Size 2MB (pdf,
                      jpeg, png)</span>
                  @endif
                </div>
              </div>
            </div>
          @endif
          @if ($spouse['marital_status'] == 'Divorced')
            <div class="row">
              <div class="col-md-12">
                <div class="f-w-input mb-3">
                  <label class="form-label">Date of Divorce</label>
                  <input type="date" class="btm-border-input"
                    max="2999-12-31"
                    wire:model.defer="spouses.{{ $key }}.date_of_divorce">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="f-w-input mb-3">
                  <label class="form-label">Court Order</label>
                  @if ($spouse['court_order_and_agreement_file'])
                    <div class="d-grid d-md-block mt-2 gap-2">
                      <a href="{{ url('storage/' . $spouse['court_order_and_agreement_file']) }}"
                        target="_blank"
                        class="btn btn-outline-one upload-view-btns">View
                        File</a>
                      <a href="#" target="_blank"
                        class="btn btn-outline-one ms-md-2 upload-view-btns ms-0"
                        wire:click.prevent="removeSposeDocument('court_order_and_agreement',{{ $spouse['id'] }})">Remove
                        File</a>
                    </div>
                  @else
                    <input type="file"
                      class="btm-border-input file-input-rad"
                      wire:key="{{ $key }}.court_order_and_agreement"
                      wire:model="spouses.{{ $key }}.court_order_and_agreement">
                    <span class="text-muted">File: Max File Size 2MB (pdf,
                      jpeg, png)</span>
                  @endif
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="f-w-input mb-3">
                  <label class="form-label">Agreement</label>
                  @if ($spouse['divorce_agreement_file'])
                    <div class="d-grid d-md-block mt-2 gap-2">
                      <a href="{{ url('storage/' . $spouse['divorce_agreement_file']) }}"
                        target="_blank"
                        class="btn btn-outline-one upload-view-btns">View
                        File</a>
                      <a href="#" target="_blank"
                        class="btn btn-outline-one ms-md-2 upload-view-btns ms-0"
                        wire:click.prevent="removeSposeDocument('divorce_agreement',{{ $spouse['id'] }})">Remove
                        File</a>
                    </div>
                  @else
                    <input type="file"
                      class="btm-border-input file-input-rad"
                      wire:key="{{ $key }}.divorce_agreement"
                      wire:model.defer="spouses.{{ $key }}.divorce_agreement">
                    <span class="text-muted">File: Max File Size 2MB (pdf,
                      jpeg, png)</span>
                  @endif
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="mb-3">
                  <div class="form-check">
                    <input class="form-check-input" type="radio"
                      name="maintenance_order_{{ $key }}"
                      id="yes_maintenance_order_{{ $key }}"
                      value="yes"
                      wire:model.lazy="spouses.{{ $key }}.maintenance_order">
                    <label class="form-check-label"
                      for="yes_maintenance_order_{{ $key }}">
                      With Maintenance Order
                    </label>
                  </div>
                  <div class="form-check">
                    <input class="form-check-input" type="radio"
                      name="maintenance_order_{{ $key }}"
                      id="no_maintenance_order_{{ $key }}"
                      value="no"
                      wire:model.lazy="spouses.{{ $key }}.maintenance_order">
                    <label class="form-check-label"
                      for="no_maintenance_order_{{ $key }}">
                      Without Maintenance Order
                    </label>
                  </div>
                </div>
              </div>
            </div>
            @if ($spouse['maintenance_order'] == 'yes')
              <div class="row">
                <div class="col-md-12">
                  <div class="f-w-input mb-3">
                    <label class="form-label">Proof of Maintenance
                      Order</label>
                    @if ($spouse['proof_of_maintenance_order_file'])
                      <div class="d-grid d-md-block mt-2 gap-2">
                        <a href="{{ url('storage/' . $spouse['proof_of_maintenance_order_file']) }}"
                          target="_blank"
                          class="btn btn-outline-one upload-view-btns">View
                          File</a>
                        <a href="#" target="_blank"
                          class="btn btn-outline-one upload-view-btns ms-2"
                          wire:click.prevent="removeSposeDocument('proof_of_maintenance_order',{{ $spouse['id'] }})">Remove
                          File</a>
                      </div>
                    @else
                      <input type="file"
                        class="btm-border-input file-input-rad"
                        wire:key="{{ $key }}.proof_of_maintenance_order"
                        wire:model="spouses.{{ $key }}.proof_of_maintenance_order">
                      <span class="text-muted">File: Max File Size 2MB (pdf,
                        jpeg, png)</span>
                    @endif
                  </div>
                </div>
              </div>
            @endif
          @endif
          @if ($spouse['marital_status'] == 'Widowed')
            <div class="row">
              <div class="col-md-12">
                <div class="f-w-input mb-3">
                  <label class="form-label">Late Spouse's Date of Death</label>
                  <input type="date" class="btm-border-input"
                    max="2999-12-31"
                    wire:model.defer="spouses.{{ $key }}.date_of_death">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="f-w-input mb-3">
                  <label class="form-label">Masters Office(where the estate of
                    the late spouse was registered)</label>
                  <select class="btm-border-input" name="masters_office"
                    wire:model.defer="spouses.{{ $key }}.masters_office">
                    <option value="" selected>Select Option</option>
                    @foreach ($masters_offices as $off)
                      <option value="{{ $off }}">{{ $off }}
                      </option>
                    @endforeach
                  </select>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="f-w-input mb-3">
                  <label class="form-label">Estate Number of Late Spouse's
                    Estate</label>
                  <input type="text" class="btm-border-input"
                    wire:model.defer="spouses.{{ $key }}.estate_number">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="f-w-input mb-3">
                  <label class="form-label">Executor of the Late Spouse’s
                    Estate</label>
                  <input type="text" class="btm-border-input"
                    wire:model.defer="spouses.{{ $key }}.executor">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="mb-3">
                  <div class="form-check">
                    <input class="form-check-input" type="checkbox"
                      name="usufruct" id="usufruct" value="usufruct"
                      wire:model.defer="spouses.{{ $key }}.usufruct">
                    <label class="form-check-label" for="usufruct">
                      Was a Usufruct created in the Late Spouse’s Will?
                    </label>
                  </div>
                  <div class="form-check">
                    <input class="form-check-input" type="checkbox"
                      name="dweling_property" id="fideicommissum"
                      value="fideicommissum"
                      wire:model.defer="spouses.{{ $key }}.dweling_property">
                    <label class="form-check-label" for="fideicommissum">
                      Was a Fideicommisssum created in the Late Spouse’s Will?
                    </label>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="f-w-input mb-3">
                  <label class="form-label">Death Certificate</label>
                  @if ($spouse['death_certificate_file'])
                    <div class="d-grid d-md-block mt-2 gap-2">
                      <a href="{{ url('storage/' . $spouse['death_certificate_file']) }}"
                        target="_blank"
                        class="btn btn-outline-one upload-view-btns">View
                        File</a>
                      <a href="#" target="_blank"
                        class="btn btn-outline-one ms-md-2 upload-view-btns ms-0"
                        wire:click.prevent="removeSposeDocument('death_certificate',{{ $spouse['id'] }})">Remove
                        File</a>
                    </div>
                  @else
                    <input type="file"
                      class="btm-border-input file-input-rad"
                      wire:key="{{ $key }}.death_certificate"
                      wire:model="spouses.{{ $key }}.death_certificate">
                    <span class="text-muted">File: Max File Size 2MB (pdf,
                      jpeg, png)</span>
                  @endif
                </div>
              </div>
            </div>
          @endif
        @endif
      </div>
    @endforeach
    <div class="row mb-3">
      @if (!$isNeverMarried)
        <div class="col-md-12 d-flex">
          <span class="ms-auto">
            @if (
                (Auth::user()->ethnicity == 'Indian' ||
                    Auth::user()->ethnicity == 'Black') &&
                    Auth::user()->gender == 'Male')
              <i class="fa fa-info-circle me-3 ms-3" data-bs-toggle="modal"
                data-bs-target="#spouse-info"></i>
            @endif
            <a href="#" class="btn btn-outline-two"
              wire:click.prevent="addSpouse">
              <svg width="16" height="17" viewBox="0 0 16 17"
                fill="none" xmlns="http://www.w3.org/2000/svg">
                <path
                  d="M12.6666
            9.16536H8.66659V13.1654H7.33325V9.16536H3.33325V7.83203H7.33325V3.83203H8.66659V7.83203H12.6666V9.16536Z"
                  fill="#CF3D60" />
              </svg>
              Add Spouse
            </a>
          </span>
        </div>
      @endif
    </div>
    <div class="col-md-12">
      <hr>
    </div>
    <div class="row">
      <div class="col-md-12 myallButtons">
        <div class="__cancel">
          <button class="btn">Cancel</button>
        </div>
        <div class="d-grid __save mb-3">
          <input type="submit" class="btn btn-red" value="save">
        </div>
      </div>
    </div>
  </form>

  <div class="modal" tabindex="-1" id="spouse-info">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-body">
          <div class="d-flex">
            <h5>Multiple spouses</h5>
            <span class="ms-auto">
              <button type="button" class="btn-close"
                data-bs-dismiss="modal" aria-label="Close"></button>
            </span>
          </div>
          @if (Auth::user()->ethnicity == 'Black')
            <p>In SA law, customary marriages are allowed. A customary marriage
              is a union that is formed in terms of
              indigenous African customary law. This is law that allows a Black
              South African man to have more than one
              wife.</p>
          @else
            <p>In SA Law persons married in terms of Muslim and Hindu religious
              rites are regarded as spouses for
              purposes of intestate succession.</p>
          @endif
        </div>
        <div class="modal-footer">
          <!--<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>-->
          <button type="button" class="btn btn-outline-two"
            data-bs-dismiss="modal">Continue</button>
        </div>
      </div>
    </div>
  </div>

  <div class="modal" tabindex="-1" id="spouse-tip-modal">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-body">
          <div class="d-flex">
            <h5>{{ $tip_title }}</h5>
            <span class="ms-auto">
              <button type="button" class="btn-close"
                data-bs-dismiss="modal" aria-label="Close"
                id="close-tip-modal"></button>
            </span>
          </div>
          <div class="mb-3">
            <p class="tip-body">{{ $tip_body }}</p>
          </div>
        </div>
      </div>
    </div>
  </div>

  @push('scripts')
    <script>
      $(document).ready(function() {
        // Function to remove modal backdrop when the modal is closed
        function removeBackdrop() {
          $('.modal-backdrop').remove();
          $('body').css("overflow", "scroll");
        }

        window.addEventListener('show-tip-modal', event => {
          $('#spouse-tip-modal').modal('show');
        });

        window.addEventListener('spouse-updated', event => {
          Swal.fire("Success", event.detail.message, "success").then((
            result) => {
            if (result.isConfirmed) {
              window.location
                .reload();
            }
          });
        });

        // When the modal is hidden (closed), remove the backdrop manually
        $('#spouse-tip-modal').on('hidden.bs.modal', removeBackdrop);

        // Manually hide the modal and remove the backdrop when the close button is clicked
        $('#close-tip-modal').click(function() {
          $('#spouse-tip-modal').modal('hide');
          removeBackdrop();
        });
        window.addEventListener('show-spouse-info-modal', event => {
          $('#spouse-info').modal("show");
        });
      });
    </script>
  @endpush

</div>
