<form wire:submit.prevent="saveTax" class="myallIncomeTax">
  <div class="f-w-input mb-3">
    <label class="form-label">Tax Number</label>
    <input type="text" class="btm-border-input" wire:model.defer="tax_number">
    @error('tax_number')
      <span class="error text-red">{{ $message }}</span>
    @enderror
  </div>
  <div class="f-w-input mb-3">
    <label class="form-label">Latest Assessment</label>
    @if ($tax_assesment_file_path)
      <div class="d-grid d-md-block mt-2 gap-2">
        <a href="{{ url('storage/' . $tax_assesment_file_path) }}"
          target="_blank" class="btn btn-outline-one">View
          File</a>
        <a href="#" target="_blank"
          class="btn btn-outline-one ms-md-2 ms-0"
          wire:click.prevent="removeTaxDocument('tax_assessment')">Remove
          File</a>
      </div>
    @else
      <input type="file" class="btm-border-input file-input-rad"
        wire:model.defer="tax_assessment">
      @error('tax_assessment')
        <span class="error text-red">Failed to upload(File must not exceed limit
          of 2MB and file type: pdf, jpeg,
          png)</span>
      @enderror
      </br>
      <span class="text-muted">File: Max File Size 2MB (pdf, jpeg,
        png)</b></span>
    @endif
  </div>
  <div class="f-w-input mb-3">
    <label class="form-label">VAT Registration Certificate</label>
    @if ($tax_file_path)
      <div class="d-grid d-md-block mt-1 gap-2">
        <a href="{{ url('storage/' . $tax_file_path) }}" target="_blank"
          class="btn btn-outline-one">View File</a>
        <a href="#" target="_blank"
          class="btn btn-outline-one ms-md-2 ms-0"
          wire:click.prevent="removeTaxDocument('tax_certificate')">Remove
          File</a>
      </div>
    @else
      <input type="file" class="btm-border-input file-input-rad"
        wire:model.defer="tax_certificate">
      @error('tax_certificate')
        <span class="error text-red">Failed to upload(File must not exceed limit
          of 2MB and file type: pdf, jpeg,
          png)</span>
      @enderror
      </br>
      <span class="text-muted">File: Max File Size 2MB (pdf, jpeg, png)</span>
    @endif
  </div>
  <div class="d-grid justify-content-end mb-3">
    <input type="submit" class="btn btn-red" value="save">
  </div>
</form>
