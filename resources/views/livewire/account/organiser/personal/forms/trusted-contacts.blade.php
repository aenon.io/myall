<div class="row">
  <div class="col-md-12">
    <p>Your “Trusted Representative” is the person with whom we can share all of
      your entered data and information to
      continue wrapping up your estate upon your passing. We strongly recommend
      that you nominate the executor who you
      have chosen to wrap up your estate with your passing.</p>
    <div class="__note mb-3">
      <span class="">
        <svg width="20" height="20" viewBox="0 0 20 20" fill="none"
          xmlns="http://www.w3.org/2000/svg">
          <path
            d="M9.16663 12.4993H10.8333V14.166H9.16663V12.4993ZM9.16663 5.83268H10.8333V10.8327H9.16663V5.83268ZM9.99163 1.66602C5.39163 1.66602 1.66663 5.39935 1.66663 9.99935C1.66663 14.5993 5.39163 18.3327 9.99163 18.3327C14.6 18.3327 18.3333 14.5993 18.3333 9.99935C18.3333 5.39935 14.6 1.66602 9.99163 1.66602ZM9.99996 16.666C6.31663 16.666 3.33329 13.6827 3.33329 9.99935C3.33329 6.31602 6.31663 3.33268 9.99996 3.33268C13.6833 3.33268 16.6666 6.31602 16.6666 9.99935C16.6666 13.6827 13.6833 16.666 9.99996 16.666Z"
            fill="#040A5E" />
        </svg>
        Your selected trusted person can be the executor of your latest original
        signed Will or the one who aids the
        executor in the wrapping up of your estate.</span>
    </div>
  </div>
  <div class="col-md-12 mb-3 mt-3">
    @if ($errors->any())
      <div class="alert alert-danger">
        <span>{{ $errors->first() }}</span>
      </div>
    @endif
  </div>
  <!-- TODO: There is profile photo functionality needed here. -->
  <div class="col-md-12">
    <form wire:submit.prevent="saveContact">
      <div class="row">
        <div class="col-md-4">
          <div class="f-w-input mb-3">
            <label class="form-label">Name</label>
            <input type="text" class="btm-border-input" name="name"
              wire:model.defer="name">
          </div>
        </div>
        <div class="col-md-4">
          <div class="f-w-input mb-3">
            <label class="form-label">Surname</label>
            <input type="text" class="btm-border-input" name="surname"
              wire:model.defer="surname">
          </div>
        </div>
        <div class="col-md-4">
          <div class="f-w-input mb-3">
            <label class="form-label">Nickname</label>
            <input type="text" class="btm-border-input" name="nickname"
              wire:model.defer="nickname">
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="f-w-input mb-3">
            <label class="form-label">ID / Passport Number</label>
            <input type="text" class="btm-border-input" name="id_number"
              wire:model.defer="id_number">
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="f-w-input mb-3">
            <label class="form-label">Date of Birth</label>
            <input type="date" class="btm-border-input" name="dob"
              max="2999-12-31" wire:model.defer="dob">
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="f-w-input mb-3">
            <label class="form-label">Physical Address</label><br />
            <textarea class="btm-border-input w-100" name="address"
              wire:model.defer="address"></textarea>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="f-w-input mb-3">
            <label class="form-label">Please provide any other information that
              can help us identify the nominated
              person, such as their contact number or email
              address</label><br />
            <textarea class="btm-border-input w-100" name="other_info"
              wire:model.defer="other_info"></textarea>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="f-w-input mb-3">
            <label class="form-label">Any additional information or
              instruction</label><br />
            <textarea class="btm-border-input w-100" name="additional_info"
              wire:model.defer="additional_info"></textarea>
          </div>
        </div>
      </div>
      <div class="row mb-3">
        <div class="col-md-12">
          <p>We will not notify the nominated person. However, if you choose to
            do so, you can personally inform the
            nominated person that they can contact MyAll upon your passing.
            Please note that for the nominated person
            to gain access, they will need to reach out to us via email. Certain
            questions will be asked, and
            documentation, including a certified copy of your death certificate,
            will be requested.</p>
          <b>You can update the details at any time by re-entering the amended
            information and clicking on the button
            'Notify MyAll.'</b>
        </div>
      </div>
      <div class="row mb-3">
        <div class="col-md-12">
          <div class="mb-3">
            <div class="form-check">
              <input class="form-check-input" type="checkbox" value=""
                id="terms" wire:model.defer="terms_and_conditions">
              <label class="form-check-label" for="terms">
                I agree that MyAll's legal team may grant access to my chosen
                nominated person upon my passing, and I
                confirm that I have read the
                <a target="_blank"
                  href="{{ url('docs/MyAll Terms and Conditions.pdf') }}"><b
                    class="text-blue">T&Cs</b></a>,
                <a target="_blank"
                  href="{{ url('docs/MyAll Privacy Policy.pdf') }}"><b
                    class="text-blue">Privacy Policy</b></a> and
                <a target="_blank"
                  href="{{ url('/docs/MyAll FAQs & Password Policy.pdf') }}"><b
                    class="text-blue">FAQs</b></a>
                on Providing Info to Executor on User's Passing.
              </label>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12 myallButtons">
          <div class="__cancel">
            <button class="btn">Cancel</button>
          </div>
          <div class="d-grid __save mb-3">
            <input type="submit" class="btn btn-red" value="save">
          </div>
        </div>
      </div>
      {{-- <!-- <div class="row"> --> --}}
      {{-- <!--   <div class="col-md-12"> --> --}}
      {{-- <!--     <div class="d-grid mb-3"> --> --}}
      {{-- <!--       <a href="#" class="btn btn-outline-two" wire:click.prevent="saveContact('notify')">Notify --> --}}
      {{-- <!--         MyAll</a> --> --}}
      {{-- <!--     </div> --> --}}
      {{-- <!--   </div> --> --}}
      {{-- <!-- </div> --> --}}
    </form>
  </div>
</div>
@push('scripts')
  <script>
    $(document).ready(function() {
      window.addEventListener('trusted-rep-updated', event => {
        Swal.fire("Success", event.detail.message, "success");
      });
    });
  </script>
@endpush
