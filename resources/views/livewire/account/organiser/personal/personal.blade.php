<div
  class="myallPersonal {{ Auth::user()->free_account == 1 ? '__full-height' : '' }}">

  @if (Auth::user()->free_account == 1)
    <div
      style="display: grid; height: 100%; align-items: center; align-items: start;">
      <div class="__empty-state">
        <div>
          <a href="{{ url('register/upgrade/' . Auth::user()->id) }}">
            <svg width="35" height="34" viewBox="0 0 35 34" fill="none"
              xmlns="http://www.w3.org/2000/svg">
              <path d="M27.1667 11.0833H25.5833V7.91667C25.5833 3.54667 22.0367 0
            17.6667 0C13.2967 0 9.75 3.54667 9.75 7.91667V11.0833H8.16667C6.425
            11.0833 5 12.5083 5 14.25V30.0833C5 31.825 6.425 33.25 8.16667
            33.25H27.1667C28.9083 33.25 30.3333 31.825 30.3333
            30.0833V14.25C30.3333 12.5083 28.9083 11.0833 27.1667
            11.0833ZM12.9167 7.91667C12.9167 5.28833 15.0383 3.16667 17.6667
            3.16667C20.295 3.16667 22.4167 5.28833 22.4167
            7.91667V11.0833H12.9167V7.91667ZM27.1667
            30.0833H8.16667V14.25H27.1667V30.0833Z" fill="#CF3D60" />
            </svg>
          </a>
          <div>
            <a href="{{ url('register/upgrade/' . Auth::user()->id) }}">
              Upgrade to access  this feature
            </a>
          </div>
        </div>
      </div>
    </div>
  @endif

  @if (Auth::user()->free_account == 0)
    <div class="card-box-style">
      <div class="__heading">
        <h3>Personal</h3>
        <p>Add some details about yourself</p>
      </div>

      {{-- TODO: There are too many empty divs here --}}
      <div class="">
        <div class="">
          <div class="">
            @if ($errors->any())
              <div class="alert alert-danger">
                <span>{{ $errors->first() }}</span>
              </div>
              @endif @if (session()->has('message'))
                <div class="alert alert-success">
                  {{ session('message') }}
                </div>
              @endif
              <div class="accordion" id="accordionPersonal">
                <div class="accordion-item">
                  <h2 class="accordion-header" id="headingOne">
                    <button class="accordion-button collapsed" type="button"
                      data-bs-toggle="collapse" data-bs-target="#me"
                      aria-expanded="false" aria-controls="me">
                      Me
                    </button>
                  </h2>
                  <div id="me" class="accordion-collapse collapse"
                    aria-labelledby="headingOne"
                    data-bs-parent="#accordionPersonal" wire:ignore.self>
                    <div class="accordion-body">
                      <div class="">
                        @include('livewire.account.organiser.personal.forms.me')
                      </div>
                    </div>
                  </div>
                </div>
                <div class="accordion-item">
                  <h2 class="accordion-header" id="headingEight">
                    <button class="accordion-button collapsed" type="button"
                      data-bs-toggle="collapse"
                      data-bs-target="#trusted-contacts" aria-expanded="false"
                      aria-controls="trusted-contacts">Trusted
                      Representative (Important)</button>
                  </h2>
                  <div id="trusted-contacts" class="accordion-collapse collapse"
                    aria-labelledby="headingEight"
                    data-bs-parent="#accordionPersonal" wire:ignore.self>
                    <div class="accordion-body">
                      <div class="">
                        <livewire:account.organiser.personal.forms.trusted-contacts />
                      </div>
                    </div>
                  </div>
                </div>
                <div class="accordion-item">
                  <h2 class="accordion-header" id="headingEdu">
                    <button class="accordion-button collapsed" type="button"
                      data-bs-toggle="collapse" data-bs-target="#education"
                      aria-expanded="false" aria-controls="education">
                      Education
                    </button>
                  </h2>
                  <div id="education" class="accordion-collapse collapse"
                    aria-labelledby="headingEdu"
                    data-bs-parent="#accordionPersonal" wire:ignore.self>
                    <div class="accordion-body">
                      <div class="">
                        @include('livewire.account.organiser.personal.forms.education')
                      </div>
                    </div>
                  </div>
                </div>
                <div class="accordion-item">
                  <h2 class="accordion-header" id="headingTwo">
                    <button class="accordion-button collapsed" type="button"
                      data-bs-toggle="collapse" data-bs-target="#partner"
                      aria-expanded="false" aria-controls="partner">
                      Proof of Marriage / Spouse / Life Partner
                    </button>
                  </h2>
                  <div id="partner" class="accordion-collapse collapse"
                    aria-labelledby="headingTwo"
                    data-bs-parent="#accordionPersonal" wire:ignore.self>
                    <div class="accordion-body">
                      <div class="">
                        <livewire:account.organiser.personal.forms.spouses />
                        {{--
                                        @include('livewire.account.organiser.personal.forms.spouse')
                                        --}}
                      </div>
                    </div>
                  </div>
                </div>
                <div class="accordion-item">
                  <h2 class="accordion-header" id="headingThree">
                    <button class="accordion-button collapsed" type="button"
                      data-bs-toggle="collapse" data-bs-target="#place_of_work"
                      aria-expanded="false" aria-controls="place_of_work">
                      Current Place of Work
                    </button>
                  </h2>
                  <div id="place_of_work" class="accordion-collapse collapse"
                    aria-labelledby="headingThree"
                    data-bs-parent="#accordionPersonal" wire:ignore.self>
                    <div class="accordion-body">
                      <div class="">
                        @include('livewire.account.organiser.personal.forms.place_of_work')
                      </div>
                    </div>
                  </div>
                </div>
                <div class="accordion-item">
                  <h2 class="accordion-header" id="headingFive">
                    <button class="accordion-button collapsed" type="button"
                      data-bs-toggle="collapse" data-bs-target="#children"
                      aria-expanded="false" aria-controls="children">
                      Children Details
                    </button>
                  </h2>
                  <div id="children" class="accordion-collapse collapse"
                    aria-labelledby="headingFive"
                    data-bs-parent="#accordionPersonal" wire:ignore.self>
                    <div class="accordion-body">
                      <div class="">
                        @include('livewire.account.organiser.personal.forms.children')
                      </div>
                    </div>
                  </div>
                </div>
                <div class="accordion-item">
                  <h2 class="accordion-header" id="headingSeven">
                    <button class="accordion-button collapsed" type="button"
                      data-bs-toggle="collapse" data-bs-target="#medical_aid"
                      aria-expanded="false" aria-controls="medical_aid">
                      Medical Aid
                    </button>
                  </h2>
                  <div id="medical_aid" class="accordion-collapse collapse"
                    aria-labelledby="headingSeven"
                    data-bs-parent="#accordionPersonal" wire:ignore.self>
                    <div class="accordion-body">
                      <div class="">
                        @include('livewire.account.organiser.personal.forms.medical_aid')
                      </div>
                    </div>
                  </div>
                </div>
              </div>
          </div>
        </div>
      </div>
    </div>
  @endif
  <div class="modal" tabindex="-1" id="spouse-note">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-body">
          <div class="d-flex">
            <span class="ms-auto">
              <button type="button" class="btn-close"
                data-bs-dismiss="modal" aria-label="Close"></button>
            </span>
          </div>
          <p>It's essential to provide all your marital details, including
            past
            marriages, divorces, and the death of a spouse. This
            will help your loved ones when reporting your estate to
            the Master of the High Court, as they will require this
            information upon your passing</p>
        </div>
        <div class="modal-footer">
          <!--<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>-->
          <button type="button" class="btn btn-outline-two"
            data-bs-dismiss="modal">Continue</button>
        </div>
      </div>
    </div>
  </div>
</div>

@push('scripts')
  <!-- <script src="{{ asset('assets/js/easy-number-separator.js') }}"></script> -->
  <script>
    document.addEventListener('DOMContentLoaded', function() {
      // window.addEventListener('show-spouse-info-modal', function(event) {
      //     document.getElementById('spouse-info').classList.add('show');
      // });

      window.addEventListener('data-updated', function(event) {
        Swal.fire("Success", event.detail.message, "success");
      });

      // easyNumberSeparator({
      //     selector: '.number-separator',
      //     separator: ','
      // });

      if (window.location.hash) {
        var hash = window.location.hash;
        var element = document.querySelector(hash);
        if (element) {
          element.classList.add('show');
        }
      }
    });
  </script>
@endpush
