<div class="row __list">
  <div class="col-md-12">
    <div class="d-flex">
      <h5></h5>
      <span class="">
        <a href="#" class="btn btn-outline-two"
          wire:click.prevent="showModal">
          <svg width="16" height="17" viewBox="0 0 16 17" fill="none"
            xmlns="http://www.w3.org/2000/svg">
            <path
              d="M12.6667
              9.16732H8.66671V13.1673H7.33337V9.16732H3.33337V7.83398H7.33337V3.83398H8.66671V7.83398H12.6667V9.16732Z"
              fill="#CF3D60" />
          </svg>
          Add Bank
        </a>
      </span>
    </div>
  </div>
  <div class="col-md-12 overflow-auto">
    <table class="table">
      <thead>
        <tr>
          <th>Account Type</th>
          <th>Bank</th>
          <th>Account Number</th>
          <th>Value</th>
          <th>File</th>
          <th></th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        @foreach ($bnks as $bnk)
          <tr>
            <td>
              <a href="#"
                wire:click.prevent="showModal('show', {{ $bnk->id }})">
                {{ $bnk->account_type }}
              </a>
            </td>
            <td>{{ $bnk->bank }}</td>
            <td>{{ $bnk->account_number }}</td>
            <td>{{ number_format($bnk->value, 2) }}</td>
            <td>
              @if ($bnk->file)
                <a href="{{ url('storage/' . $bnk->file) }}" target="_blank"><i
                    class="bx bxs-file"></i></a>
              @endif
            </td>

            <td>
              <div class="__actions __border">
                <a href="#" class="text-danger"
                  wire:click.prevent="showModal('edit', {{ $bnk->id }})">
                  <svg width="20" height="20" viewBox="0 0 20 20"
                    fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M11.7157 7.51667L12.4824 8.28333L4.93236
                    15.8333H4.16569V15.0667L11.7157 7.51667ZM14.7157 2.5C14.5074
                    2.5 14.2907 2.58333 14.1324 2.74167L12.6074 4.26667L15.7324
                    7.39167L17.2574 5.86667C17.5824 5.54167 17.5824 5.01667
                    17.2574 4.69167L15.3074 2.74167C15.1407 2.575 14.9324 2.5
                    14.7157 2.5ZM11.7157 5.15833L2.49902
                    14.375V17.5H5.62402L14.8407 8.28333L11.7157 5.15833Z"
                      fill="#CF3D60" />
                  </svg>
                  Edit
                </a>
              </div>
            </td>

            <td>
              <div class="__actions">
                <a href="#" class="text-danger"
                  wire:click.prevent="deleteBank({{ $bnk->id }})">
                  <svg width="20" height="20" viewBox="0 0 20 20"
                    fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M13.3334 7.5V15.8333H6.66675V7.5H13.3334ZM12.0834
                    2.5H7.91675L7.08341
                    3.33333H4.16675V5H15.8334V3.33333H12.9167L12.0834
                    2.5ZM15.0001 5.83333H5.00008V15.8333C5.00008 16.75 5.75008
                    17.5 6.66675 17.5H13.3334C14.2501 17.5 15.0001 16.75 15.0001
                    15.8333V5.83333Z" fill="#CF3D60" />
                  </svg>
                  Remove
                </a>
              </div>
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
  </div>
  <div class="modal" tabindex="-1" id="bnk-frm-modal" wire:ignore.self>
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">{{ $modal_title }}</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal"
            wire:click.prevent="closeModal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          @if ($errors->any())
            <div class="alert alert-danger">
              <span>{{ $errors->first() }}</span>
            </div>
          @endif
          <form>
            <div class="row">
              <div class="col-md-12">
                <div class="mb-3">
                  <label class="form-label">Asset Type</label>
                  <select class="form-control" name="asset_type"
                    wire:model.defer="account_type"
                    @if ($view == 'show') disabled @endif>
                    <option value="" selected>Select Option</option>
                    @foreach ($account_types as $tp)
                      <option value="{{ $tp }}">{{ $tp }}
                      </option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="col-md-12">
                <div class="mb-3">
                  <label class="form-label">Bank Name</label>
                  <input type="text" class="form-control" name="bank"
                    wire:model.defer="bank"
                    @if ($view == 'show') disabled @endif>
                </div>
              </div>
              <div class="col-md-12">
                <div class="mb-3">
                  <label class="form-label">Account Number</label>
                  <input type="text" class="form-control"
                    name="account_number" wire:model.defer="account_number"
                    @if ($view == 'show') disabled @endif>
                </div>
              </div>
              <div class="col-md-12">
                <div class="mb-3">
                  <label class="form-label">Value</label>
                  <div class="input-group mb-3">
                    <span class="input-group-text" id="basic-addon1">R</span>
                    <input type="text" class="form-control number-separator"
                      name="value" wire:model.defer="value"
                      @if ($view == 'show') disabled @endif>
                  </div>
                </div>
              </div>
              {{--
                            <div class="col-md-12">
                                <div class="mb-3">
                                    <label class="form-label">Message</label>
                                    <textarea class="form-control" name="message" wire:model.defer="message"></textarea>
                                </div>
                            </div>
                            --}}
              <div class="col-md-12">
                @if ($file_path)
                  <div class="d-grid d-md-block mb-3 gap-2">
                    @if ($view == 'edit')
                      <a href="#"
                        wire:click.prevent="removeFile({{ $cur_id }})"
                        class="btn btn-outline-two">Remove file</a>
                    @endif
                  </div>
                @else
                  <div class="mb-3">
                    <label class="form-label">Document</label>
                    <input type="file" class="form-control" name="file"
                      wire:model="file"
                      @if ($view == 'show') disabled @endif>
                    @error('file')
                      <span class="error text-red">Failed to upload(File must not
                        exceed limit of 2MB and file type: pdf,
                        jpeg, png)</span></br>
                    @enderror
                    <span class="text-muted">File: Max File Size 2MB (pdf, jpeg,
                      png)</span>
                  </div>
                @endif
              </div>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-outline-two"
            wire:click.prevent="closeModal" data-bs-dismiss="modal">
            @if ($view == 'show')
              Close
            @else
              Cancel
            @endif
          </button>
          @if ($view == 'edit' || is_null($view))
            <button type="button" class="btn btn-red"
              wire:click.prevent="saveBank">Save</button>
          @endif
        </div>
      </div>
    </div>
  </div>
  @push('scripts')
    <script>
      window.addEventListener('show-form-modal', event => {
        $('#bnk-frm-modal').modal('show');
      });
      window.addEventListener('close-form-modal', event => {
        $('.modal').modal('hide');
      });
    </script>
  @endpush
</div>
