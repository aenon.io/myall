@if (Auth::user()->free_account == 1)
  <div class="{{ Auth::user()->free_account == 1 ? '__full-height' : '' }}">
    <div
      style="display: grid; height: 100%; align-items: center; align-items: start;">
      <div class="__empty-state">
        <div>
          <a href="{{ url('register/upgrade/' . Auth::user()->id) }}">
            <svg width="35" height="34" viewBox="0 0 35 34" fill="none"
              xmlns="http://www.w3.org/2000/svg">
              <path d="M27.1667 11.0833H25.5833V7.91667C25.5833 3.54667 22.0367 0
            17.6667 0C13.2967 0 9.75 3.54667 9.75 7.91667V11.0833H8.16667C6.425
            11.0833 5 12.5083 5 14.25V30.0833C5 31.825 6.425 33.25 8.16667
            33.25H27.1667C28.9083 33.25 30.3333 31.825 30.3333
            30.0833V14.25C30.3333 12.5083 28.9083 11.0833 27.1667
            11.0833ZM12.9167 7.91667C12.9167 5.28833 15.0383 3.16667 17.6667
            3.16667C20.295 3.16667 22.4167 5.28833 22.4167
            7.91667V11.0833H12.9167V7.91667ZM27.1667
            30.0833H8.16667V14.25H27.1667V30.0833Z" fill="#CF3D60" />
            </svg>
          </a>
          <div>
            <a href="{{ url('register/upgrade/' . Auth::user()->id) }}">
              Upgrade to access  this feature
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
@endif

@if (Auth::user()->free_account == 0)
  <div class="">
    <div class="card-box-style">
      <div class="__heading">
        <h3>Personal Wealth</h3>
        <p>Add your assets, Investments, Insurance and Accounts here to keep
          track
        </p>
      </div>
      <div class="">
        <div class="">
          <div class="">
            <div class="">
              <div class="d-block d-sm-none mb-2 mt-3">
                <span class="text-danger"><b>NOTE:</b> Please scroll left-right
                  to
                  view the info in each section
                  below.</span>
              </div>
            </div>
            @if ($errors->any())
              <div class="alert alert-danger">
                <span>{{ $errors->first() }}</span>
              </div>
            @endif
            @if (session()->has('message'))
              <div class="alert alert-success">
                {{ session('message') }}
              </div>
            @endif

            <div class="row">
              <div class="col-md-12">
                <div class="accordion" id="accordion-property-wealth">
                  <div class="accordion-item">
                    <h2 class="accordion-header" id="assets-heading">
                      <button class="accordion-button" type="button"
                        data-bs-toggle="collapse" data-bs-target="#assets"
                        aria-expanded="true" aria-controls="assets">
                        Assets
                      </button>
                    </h2>
                    <div id="assets" class="accordion-collapse show collapse"
                      aria-labelledby="assets-heading"
                      data-bs-parent="#accordion-property-wealth">
                      <div class="accordion-body">
                        <livewire:account.organiser.property-wealth.pages.assets />
                      </div>
                    </div>
                  </div>
                  <div class="accordion-item">
                    <h2 class="accordion-header" id="investment-heading">
                      <button class="accordion-button collapsed" type="button"
                        data-bs-toggle="collapse" data-bs-target="#investment"
                        aria-expanded="false" aria-controls="investment">
                        Investments and Policies
                      </button>
                    </h2>
                    <div id="investment" class="accordion-collapse collapse"
                      aria-labelledby="investment-heading"
                      data-bs-parent="#accordion-property-wealth">
                      <div class="accordion-body">
                        <livewire:account.organiser.property-wealth.pages.investments />
                      </div>
                    </div>
                  </div>
                  <div class="accordion-item">
                    <h2 class="accordion-header" id="banks-heading">
                      <button class="accordion-button collapsed" type="button"
                        data-bs-toggle="collapse" data-bs-target="#banks"
                        aria-expanded="false" aria-controls="banks">
                        Bank Accounts
                      </button>
                    </h2>
                    <div id="banks" class="accordion-collapse collapse"
                      aria-labelledby="banks-heading"
                      data-bs-parent="#accordion-property-wealth">
                      <div class="accordion-body">
                        <livewire:account.organiser.property-wealth.pages.banks />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endif

@push('scripts')
  <script>
    window.addEventListener('show-form-modal', event => {
      $('#frm-modal').modal('show');
    });
    window.addEventListener('close-form-modal', event => {
      $('.modal').modal('hide');
    });

    window.addEventListener('reload-js', event => {
      easyNumberSeparator({
        selector: '.number-separator',
        separator: ','
      })
    });
  </script>
@endpush
</div>
