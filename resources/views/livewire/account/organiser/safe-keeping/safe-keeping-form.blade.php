<style>
  .full-width {
    width: 100%;
  }
</style>
<form wire:submit.prevent="saveSafeKeeping " class="myallSafeKeeping">
  @foreach ($items as $k => $itm)
    <div class="row __item">
      <div class="col-md-12">
        @if (isset($itm['id']))
          <div class="col-md-12 d-grid d-md-block gap-2 text-end">
            <a href="#" class="btn"
              wire:click.prevent="removeSafeKeeping({{ $itm['id'] }})">
              <svg width="20" height="20" viewBox="0 0 20 20"
                fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M13.3334 7.5V15.8333H6.66675V7.5H13.3334ZM12.0834
                  2.5H7.91675L7.08341
                  3.33333H4.16675V5H15.8334V3.33333H12.9167L12.0834 2.5ZM15.0001
                  5.83333H5.00008V15.8333C5.00008 16.75 5.75008 17.5 6.66675
                  17.5H13.3334C14.2501 17.5 15.0001 16.75 15.0001
                  15.8333V5.83333Z" fill="#CF3D60" />
              </svg>
              Remove Item
            </a>
          </div>
        @endif
        <div class="mb-3">
          <label class="form-label">Title / Item Name</label>
          <input type="text" class="btm-border-input full-width"
            wire:model.defer="items.{{ $k }}.title">
        </div>
      </div>
      <div class="col-md-12">
        <div class="mb-3 overflow-auto">
          <label class="form-label">Description</label>
          <textarea class="btm-border-input full-width area-min-height"
            style="width: 99.5%; margin-left: 1px"
            wire:model.defer="items.{{ $k }}.description"></textarea>
        </div>
      </div>
      <div class="col-md-12">
        <div class="mb-3">
          <label class="form-label">Where to Find</label>
          <textarea class="btm-border-input full-width area-min-height"
            wire:model.defer="items.{{ $k }}.location"></textarea>
        </div>
      </div>
      <div class="col-md-12">
        <div class="f-w-input mb-3">
          <label class="form-label">File</label>
          @if (isset($itm['file_path']))
            <div class="d-grid d-md-block mt-1 gap-2">
              <a href="{{ url('storage/' . $itm['file_path']) }}"
                target="_blank" class="btn btn-outline-one text-red">View
                File</a>
              @if (isset($itm['id']))
                <a href="#"
                  class="btn btn-outline-one ms-md-2 text-red ms-0"
                  wire:click.prevent="removeFile({{ $itm['id'] }})">Remove
                  File</a>
              @endif
            </div>
          @else
            <input type="file" class="btm-border-input file-input-rad"
              wire:model="items.{{ $k }}.file">
            @error('items.' . $k . '.file')
              <span class="error text-red">Failed to upload(File must not exceed
                limit of 2MB and file type: pdf, jpeg,
                png)</span>
            @enderror
            </br>
            <span class="text-muted">File: Max File Size 2MB (pdf, jpeg,
              png)</span>
          @endif
        </div>
      </div>
    </div>
  @endforeach
  <div class="justify-content-end d-grid">
    <a href="#" class="btn btn-outline-two" wire:click.prevent="addMore">
      <svg width="16" height="17" viewBox="0 0 16 17" fill="none"
        xmlns="http://www.w3.org/2000/svg">
        <path
          d="M12.6666
          9.16536H8.66659V13.1654H7.33325V9.16536H3.33325V7.83203H7.33325V3.83203H8.66659V7.83203H12.6666V9.16536Z"
          fill="#CF3D60" />
      </svg>
      Add
    </a>
  </div>
  <div class="col-md-12">
    <hr>
  </div>
  <div class="d-grid justify-content-end mb-3 gap-2">
    <input type="submit" class="btn btn-red" value="save">
  </div>
</form>
