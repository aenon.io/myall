<div class="">
  <div class="card-box-style">
    <div class="__heading">
      <h3>Safekeeping - What would you like to keep safe <i
          class="fa fa-info-circle" style="cursor: pointer;" data-bs-toggle="modal"
          data-bs-target="#page-description"></i></h3>
      <p>Need to store other important documents or information? It’s like your
        own digital vault!</p>
    </div>
    <div class="">
      <div class="">
        <div class="">
          <div class="d-flex">
          </div>
          @if ($errors->any())
            <div class="alert alert-danger">
              <span>{{ $errors->first() }}</span>
            </div>
          @endif
          @if (session()->has('message'))
            <div class="alert alert-success">
              {{ session('message') }}
            </div>
          @endif

          <div class="accordion" id="accordionExample">
            <div class="accordion-item">
              <h2 class="accordion-header" id="headingFive">
                <button class="accordion-button collapsed" type="button"
                  data-bs-toggle="collapse" data-bs-target="#safekeep"
                  aria-expanded="false" aria-controls="safekeep">
                  Safekeeping
                </button>
              </h2>
              <div id="safekeep" class="accordion-collapse collapse"
                aria-labelledby="headingFive" data-bs-parent="#accordionExample"
                wire:ignore.self>
                <div class="accordion-body">
                  <div class="">
                    @include('livewire.account.organiser.safe-keeping.safe-keeping-form')
                  </div>
                </div>
              </div>
            </div>
            {{--
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="headingSix">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#find" aria-expanded="false" aria-controls="find">
                                    Where to Find
                                </button>
                            </h2>
                            <div id="find" class="accordion-collapse collapse" aria-labelledby="headingSix" data-bs-parent="#accordionPersonal" wire:ignore.self>
                                <div class="accordion-body">
                                    <div class="">
                                        @include('livewire.account.organiser.personal.forms.find')
                                    </div>
                                </div>
                            </div>
                        </div>
                        --}}
            <div class="accordion-item">
              <h2 class="accordion-header" id="headingFour">
                <button class="accordion-button collapsed" type="button"
                  data-bs-toggle="collapse" data-bs-target="#tax"
                  aria-expanded="false" aria-controls="tax">
                  Income Tax
                </button>
              </h2>
              <div id="tax" class="accordion-collapse collapse"
                aria-labelledby="headingFour"
                data-bs-parent="#accordionPersonal" wire:ignore.self>
                <div class="accordion-body">
                  <div class="">
                    @include('livewire.account.organiser.personal.forms.tax')
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal" tabindex="-1" id="will-safe-keep-pop-up">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-body">
          <div class="text-end">
            <button type="button" class="btn-close" data-bs-dismiss="modal"
              aria-label="Close"></button>
          </div>
          <p><b>Preserve Your Valid Will for Future Reference</b></p>
          <p>To ensure the proper handling of your testamentary wishes upon your
            passing, we kindly request that you
            store your original signed Will in a secure location. By doing so,
            this document will be recognised as your
            official and valid Will. To register the whereabouts of your Will
            for safekeeping, we offer a convenient
            safekeeping function. If you pass away and no one can find your Will
            then your nominated executor will get
            access to this section to see where you’ve kept it safe.</p>
          <p>Please take a moment to provide the necessary details regarding the
            storage location of your Will.
            Maintaining the integrity of your testamentary intentions is of
            utmost importance to us, and by entrusting
            your Will to a secure place, you contribute to the smooth execution
            of your final wishes. Thank you for your
            cooperation in this matter.</p>
        </div>
      </div>
    </div>
  </div>

  <div class="modal" tabindex="-1" id="page-description">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-body">
          <div class="d-flex">
            <h5>Safekeeping</h5>
            <span class="ms-auto">
              <button type="button" class="btn-close" data-bs-dismiss="modal"
                aria-label="Close"></button>
            </span>
          </div>
          <div class="mb-5">
            <p>Safekeeping allows you to document the location of any other
              important documentation. This information
              will be accessible to your executor after your passing. This can
              be updated as much as you want.</p>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal" tabindex="-1" id="init-pop-up">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-body">
          <div class="d-flex">
            <h5>Safekeeping</h5>
            <span class="ms-auto">
              <button type="button" class="btn-close" data-bs-dismiss="modal"
                aria-label="Close"></button>
            </span>
          </div>
          <div class="">
            <p>
              Need to store other important documents or information? It's like
              having your own digital vault! MyAll's
              safekeeping section: peace of mind made easy!
            </p>
            <p>Secure your valuables and provide crucial information about their
              location to your loved ones with
              MyAll's safekeeping section. Store descriptions and specific
              locations for items like passports,
              insurance policies, jewelry, and more. No more frantic searches or
              moments of panic!</p>
            <p>Enjoy peace of mind, knowing your loved ones can easily access
              important information and documents in
              times of need.</p>
          </div>
          <div class="mt-5 text-end">
            <a href="#" class="btn btn-outline-two"
              wire:click.prevent="initModalSeen">Continue</a>
          </div>
        </div>
      </div>
    </div>
  </div>

  @push('scripts')
    <script>
      $(document).ready(function() {
        var prev_route = '{{ url()->previous() }}';
        var will_complete_url =
          "{{ url('/organiser/online-will/review/complete') }}";
        if (prev_route == will_complete_url) {
          $('#will-safe-keep-pop-up').modal('show');
        }
      });
    </script>

    <script>
      $(document).ready(function() {
        var show_init_pop = '{{ $show_init_pop_up }}';
        if (show_init_pop) {
          $('#init-pop-up').modal('show');
        }
        window.addEventListener('close-modals', event => {
          $('.modal').each(function() {
            $(this).modal('hide');
          })
        });

      });
    </script>
  @endpush
</div>
