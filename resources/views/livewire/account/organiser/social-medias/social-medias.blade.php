<div
  class="myallSocialMedia {{ Auth::user()->free_account == 1 ? '__full-height' : '' }}">

  @if (Auth::user()->free_account == 1)
    <div
      style="display: grid; height: 100%; align-items: center; align-items: start;">
      <div class="__empty-state">
        <div>
          <a href="{{ url('register/upgrade/' . Auth::user()->id) }}">
            <svg width="35" height="34" viewBox="0 0 35 34" fill="none"
              xmlns="http://www.w3.org/2000/svg">
              <path d="M27.1667 11.0833H25.5833V7.91667C25.5833 3.54667 22.0367 0
            17.6667 0C13.2967 0 9.75 3.54667 9.75 7.91667V11.0833H8.16667C6.425
            11.0833 5 12.5083 5 14.25V30.0833C5 31.825 6.425 33.25 8.16667
            33.25H27.1667C28.9083 33.25 30.3333 31.825 30.3333
            30.0833V14.25C30.3333 12.5083 28.9083 11.0833 27.1667
            11.0833ZM12.9167 7.91667C12.9167 5.28833 15.0383 3.16667 17.6667
            3.16667C20.295 3.16667 22.4167 5.28833 22.4167
            7.91667V11.0833H12.9167V7.91667ZM27.1667
            30.0833H8.16667V14.25H27.1667V30.0833Z" fill="#CF3D60" />
            </svg>
          </a>
          <div>
            <a href="{{ url('register/upgrade/' . Auth::user()->id) }}">
              Upgrade to access  this feature
            </a>
          </div>
        </div>
      </div>
    </div>
  @endif

  @if (Auth::user()->free_account == 0)
    <div class="card-box-style">
      <div class="__title">
        <div class="__heading">
          <h3>Social Media <i class="fa fa-info-circle" style="cursor: pointer;"
              data-bs-toggle="modal" data-bs-target="#page-description"></i>
          </h3>
          <p>Always forgetting which social media accounts you have? Add them
            here,
            and provide instructions for
            when you can no longer access them.</p>
        </div>
        <div class="d-flex heading-btn-display" style="flex-shrink: 0">
          <span class="add-business-btn add-btn-alignment">
            <a href="#" class="btn btn-outline"
              wire:click.prevent="showModal()">
              <svg width="20" height="20" viewBox="0 0 20 20"
                fill="none" xmlns="http://www.w3.org/2000/svg">
                <path
                  d="M15.8334
                10.8346H10.8334V15.8346H9.16675V10.8346H4.16675V9.16797H9.16675V4.16797H10.8334V9.16797H15.8334V10.8346Z"
                  fill="#CF3D60" />
              </svg>
              Add Social Media
            </a>
          </span>
        </div>
      </div>
      <div class="">
        <div class="">
          <div class="">
            @if ($errors->any())
              <div class="alert alert-danger">
                <span>{{ $errors->first() }}</span>
              </div>
            @endif
            @if (session()->has('message'))
              <div class="alert alert-success">
                {{ session('message') }}
              </div>
            @endif
            <div class="row">
              <div class="col-md-12">
                <div class="accordion" id="accordionSocialMedia">
                  @php
                    $count = 0;
                  @endphp
                  @foreach ($medias as $key => $media)
                    <div class="accordion-item">
                      <h2 class="accordion-header"
                        id="heading_{{ strtolower(str_replace(' ', '_', $key)) }}">
                        <button
                          class="accordion-button @if ($count > 0) collapsed @endif"
                          type="button" data-bs-toggle="collapse"
                          data-bs-target="#collapse_{{ strtolower(str_replace(' ', '_', $key)) }}"
                          aria-expanded="@if ($count == 0) true @else false @endif"
                          aria-controls="collapse_{{ strtolower(str_replace(' ', '_', $key)) }}">
                          {{ ucwords($key) }}
                        </button>
                      </h2>
                      <div
                        id="collapse_{{ strtolower(str_replace(' ', '_', $key)) }}"
                        class="accordion-collapse @if ($count == 0) show @endif collapse"
                        aria-labelledby="heading_{{ strtolower(str_replace(' ', '_', $key)) }}"
                        data-bs-parent="#accordionSocialMedia">
                        <div class="accordion-body">
                          @foreach ($media as $md)
                            <div class="__sm-item">
                              <div class="">
                                <div class="__sm-body">
                                  <div class="__details">
                                    <div class="__sm-username">
                                      <svg width="20" height="20"
                                        viewBox="0 0 20 20" fill="none"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <path
                                          d="M9.99996 1.66797C5.39996 1.66797
                                        1.66663 5.4013 1.66663 10.0013C1.66663
                                        14.6013 5.39996 18.3346 9.99996
                                        18.3346C14.6 18.3346 18.3333 14.6013
                                        18.3333 10.0013C18.3333 5.4013 14.6
                                        1.66797 9.99996 1.66797ZM6.12496
                                        15.418C7.21663 14.6346 8.54996 14.168
                                        9.99996 14.168C11.45 14.168 12.7833
                                        14.6346 13.875 15.418C12.7833 16.2013
                                        11.45 16.668 9.99996 16.668C8.54996
                                        16.668 7.21663 16.2013 6.12496
                                        15.418ZM15.1166 14.268C13.7083 13.168
                                        11.9333 12.5013 9.99996 12.5013C8.06663
                                        12.5013 6.29163 13.168 4.88329
                                        14.268C3.91663 13.1096 3.33329 11.6263
                                        3.33329 10.0013C3.33329 6.31797 6.31663
                                        3.33464 9.99996 3.33464C13.6833 3.33464
                                        16.6666 6.31797 16.6666 10.0013C16.6666
                                        11.6263 16.0833 13.1096 15.1166 14.268Z"
                                          fill="#4B5563" />
                                        <path d="M9.99996 5.0013C8.39163 5.0013
                                        7.08329 6.30964 7.08329 7.91797C7.08329
                                        9.5263 8.39163 10.8346 9.99996
                                        10.8346C11.6083 10.8346 12.9166 9.5263
                                        12.9166 7.91797C12.9166 6.30964 11.6083
                                        5.0013 9.99996 5.0013ZM9.99996
                                        9.16797C9.30829 9.16797 8.74996 8.60964
                                        8.74996 7.91797C8.74996 7.2263 9.30829
                                        6.66797 9.99996 6.66797C10.6916 6.66797
                                        11.25 7.2263 11.25 7.91797C11.25 8.60964
                                        10.6916 9.16797 9.99996 9.16797Z"
                                          fill="#4B5563" />
                                      </svg>

                                      {{ $md->user_name }}
                                    </div>
                                    <div class="__sm-edit">
                                      <a href="#" class=""
                                        wire:click.prevent="showEdit({{ $md->id }})">
                                        <svg width="20" height="20"
                                          viewBox="0 0 20 20" fill="none"
                                          xmlns="http://www.w3.org/2000/svg">
                                          <path d="M11.7157 7.51667L12.4824
                                          8.28333L4.93236
                                          15.8333H4.16569V15.0667L11.7157
                                          7.51667ZM14.7157 2.5C14.5074 2.5
                                          14.2907 2.58333 14.1324
                                          2.74167L12.6074 4.26667L15.7324
                                          7.39167L17.2574 5.86667C17.5824
                                          5.54167 17.5824 5.01667 17.2574
                                          4.69167L15.3074 2.74167C15.1407 2.575
                                          14.9324 2.5 14.7157 2.5ZM11.7157
                                          5.15833L2.49902
                                          14.375V17.5H5.62402L14.8407
                                          8.28333L11.7157 5.15833Z"
                                            fill="#CF3D60" />
                                        </svg>
                                        Edit
                                      </a>
                                      <span></span>
                                      <a href="#" class=""
                                        wire:click.prevent="remove({{ $md->id }})">
                                        <svg width="20" height="20"
                                          viewBox="0 0 20 20" fill="none"
                                          xmlns="http://www.w3.org/2000/svg">
                                          <path d="M13.3334
                                          7.5V15.8333H6.66675V7.5H13.3334ZM12.0834
                                          2.5H7.91675L7.08341
                                          3.33333H4.16675V5H15.8334V3.33333H12.9167L12.0834
                                          2.5ZM15.0001
                                          5.83333H5.00008V15.8333C5.00008 16.75
                                          5.75008 17.5 6.66675
                                          17.5H13.3334C14.2501 17.5 15.0001
                                          16.75 15.0001 15.8333V5.83333Z"
                                            fill="#CF3D60" />
                                        </svg>
                                        Remove
                                      </a>
                                    </div>
                                  </div>
                                  <div class="__grid">
                                    <div>
                                      <span class="__label">
                                        Link
                                      </span>
                                      <a href="{{ $md->link }}"
                                        target="_blank">
                                        Open Link
                                        <svg width="16" height="16"
                                          viewBox="0 0 16 16" fill="none"
                                          xmlns="http://www.w3.org/2000/svg">
                                          <path d="M12.6667
                                          12.6667H3.33333V3.33333H8V2H3.33333C2.59333
                                          2 2 2.6 2 3.33333V12.6667C2 13.4
                                          2.59333 14 3.33333 14H12.6667C13.4 14
                                          14 13.4 14
                                          12.6667V8H12.6667V12.6667ZM9.33333
                                          2V3.33333H11.7267L5.17333
                                          9.88667L6.11333 10.8267L12.6667
                                          4.27333V6.66667H14V2H9.33333Z"
                                            fill="#CF3D60" />
                                        </svg>
                                      </a>
                                    </div>
                                    @if ($md->legacy_contact)
                                      <div class="">
                                        <span class="__label">
                                          Legacy Contact
                                        </span>
                                        <span class="__data">
                                          {{ $md->legacy_contact }}
                                        </span>
                                      </div>
                                    @endif
                                    <div>
                                      <span class="__label">
                                        Instructions
                                      </span>
                                      <span class="__data">
                                        {{ $md->instructions }}
                                      </span>
                                    </div>
                                  </div>
                                </div>
                                {{-- <h5> --}}
                                {{--   @if ($md->network != 'Other') --}}
                                {{--     {{ $md->network }} --}}
                                {{--   @else --}}
                                {{--     {{ $md->specific_network }} --}}
                                {{--   @endif --}}
                                {{-- </h5> --}}
                                {{-- <ul class="list-group"> --}}
                                {{--   <li class="list-group-item d-flex"> --}}
                                {{--     Username --}}
                                {{--     <span class="ms-auto"> --}}
                                {{--       {{ $md->user_name }} --}}
                                {{--     </span> --}}
                                {{--   </li> --}}
                                {{--   <li class="list-group-item d-flex"> --}}
                                {{--     Link --}}
                                {{--     <span class="ms-auto"> --}}
                                {{--       <a href="{{ $md->link }}" --}}
                                {{--         target="_blank">{{ $md->link }}</a> --}}
                                {{--     </span> --}}
                                {{--   </li> --}}
                                {{--   @if ($md->legacy_contact) --}}
                                {{--     <li class="list-group-item d-flex"> --}}
                                {{--       Legacy Contact --}}
                                {{--       <span class="ms-auto"> --}}
                                {{--         {{ $md->legacy_contact }} --}}
                                {{--       </span> --}}
                                {{--     </li> --}}
                                {{--   @endif --}}
                                {{--   <li class="list-group-item"> --}}
                                {{--     Instructions<br /> --}}
                                {{--     <b> --}}
                                {{--       {{ $md->instructions }} --}}
                                {{--     </b> --}}
                                {{--   </li> --}}
                                {{-- </ul> --}}
                              </div>
                              <div class="d-block d-sm-none mb-3">
                                <h5>
                                  @if ($md->network != 'Other')
                                    {{ $md->network }}
                                  @else
                                    {{ $md->specific_network }}
                                  @endif
                                </h5>
                                <div class="col-md-12 overflow-auto">
                                  <table class="table">
                                    <tbody>
                                      <tr>
                                        <td><b>Userame</b></td>
                                        <td>{{ $md->user_name }}</td>
                                      </tr>
                                      <tr>
                                        <td><b>Link</b></td>
                                        <td><a href="{{ $md->link }}"
                                            target="_blank">{{ $md->link }}</a>
                                        </td>
                                      </tr>
                                      @if ($md->legacy_contact)
                                        <tr>
                                          <td><b>Legacy Contact</b></td>
                                          <td>{{ $md->legacy_contact }}</td>
                                        </tr>
                                      @endif
                                      <tr>
                                        <td><b>Instructions</b></td>
                                        <td>{{ $md->instructions }}</td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </div>
                              </div>
                            </div>
                          @endforeach
                        </div>
                      </div>
                    </div>
                    @php
                      $count++;
                    @endphp
                  @endforeach
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  @endif

  <div class="modal" tabindex="-1" id="frm-modal" wire:ignore.self>
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Social Media</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal"
            aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <form>
            <div class="row">
              <div class="col-md-12">
                <div class="mb-3">
                  <label class="form-label">Network</label>
                  <select class="form-control" name="network"
                    wire:model="network">
                    <option value="">Select Option</option>
                    @foreach ($networks as $ntwrk)
                      <option value="{{ $ntwrk }}">{{ $ntwrk }}
                      </option>
                    @endforeach
                  </select>
                </div>
              </div>
              @if ($network == 'Other')
                <div class="col-md-12">
                  <div class="mb-3">
                    <label class="form-label">Specify</label>
                    <input type="text" class="form-control"
                      name="specific_network"
                      wire:model.defer="specific_network">
                  </div>
                </div>
              @endif
              <div class="col-md-12">
                <div class="mb-3">
                  <label class="form-label">Username @if ($network == 'Facebook')
                      / Cell Number
                    @endif
                  </label>
                  <input type="text" class="form-control" name="user_name"
                    wire:model.defer="user_name">
                </div>
              </div>
              <div class="col-md-12">
                <div class="mb-3">
                  <label class="form-label">Link</label>
                  <input type="text" class="form-control" name="link"
                    wire:model.defer="link">
                </div>
              </div>
              @if ($network == 'Facebook')
                <div class="col-md-12">
                  <div class="mb-3">
                    <label class="form-label">Legacy Contact</label>
                    <input type="text" class="form-control"
                      name="legacy_contact" wire:model.defer="legacy_contact">
                  </div>
                </div>
              @endif
              <div class="col-md-12">
                <div class="mb-3">
                  <label class="form-label">Instruction @if ($network && $network != 'Other')
                      of what happens to {{ $network }}
                    @endif
                  </label>
                  <textarea class="form-control area-min-height" name="instructions"
                    wire:model.defer="instructions"></textarea>
                </div>
              </div>

            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-outline-two"
            data-bs-dismiss="modal">Close</button>
          <button type="button" class="btn btn-red"
            wire:click.prevent="saveSocialMedia">Save</button>
        </div>
      </div>
    </div>
  </div>

  <div class="modal" tabindex="-1" id="page-description">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-body">
          <div class="d-flex">
            <h5>Social Media</h5>
            <span class="ms-auto">
              <button type="button" class="btn-close"
                data-bs-dismiss="modal" aria-label="Close"></button>
            </span>
          </div>
          <div class="mb-5">
            <p>Social Media Wishes is where you can provide detailed
              instructions about what should happen after your
              passing, with your social media. This information will be
              accessible to your executor following your
              passing.</p>
          </div>
        </div>
      </div>
    </div>
  </div>

  {{-- <div class="modal" tabindex="-1" id="init-pop-up"> --}}
  {{--   <div class="modal-dialog"> --}}
  {{--     <div class="modal-content"> --}}
  {{--       <div class="modal-body"> --}}
  {{--         <div class="d-flex"> --}}
  {{--           <h5>Social Media Legacy Contact</h5> --}}
  {{--           <span class="ms-auto"> --}}
  {{--             <button type="button" class="btn-close" --}}
  {{--               data-bs-dismiss="modal" aria-label="Close"></button> --}}
  {{--           </span> --}}
  {{--         </div> --}}
  {{--         <div class=""> --}}
  {{--           <p>A feature that must be set up in Facebook that allows users to --}}
  {{--             designate someone to manage their account --}}
  {{--             after they pass away. The legacy contact is granted certain --}}
  {{--             permissions to carry out specific tasks, such --}}
  {{--             as memorialising the account or responding to friend requests. --}}
  {{--             This feature is designed to help friends --}}
  {{--             and family members honor the memory of the deceased individual and --}}
  {{--             manage their digital presence on the --}}
  {{--             platform. It gives the legacy contact some control over the --}}
  {{--             account without providing full access to the --}}
  {{--             deceased person's private messages or login credentials.</p> --}}
  {{--         </div> --}}
  {{--         <div class="mt-5 text-end"> --}}
  {{--           <a href="#" class="btn btn-outline-two" --}}
  {{--             wire:click.prevent="initModalSeen">Continue</a> --}}
  {{--         </div> --}}
  {{--       </div> --}}
  {{--     </div> --}}
  {{--   </div> --}}
  {{-- </div> --}}

  @push('scripts')
    <script>
      window.addEventListener('show-form-modal', event => {
        $('#frm-modal').modal('show');
      });
      window.addEventListener('close-form-modal', event => {
        $('.modal').modal('hide');
      });
    </script>

    <script>
      $(document).ready(function() {
        var show_init_pop = '{{ $show_init_pop_up }}';
        var isFreeAccount =
          {{ Auth::user()->free_account == 1 ? 'true' : 'false' }};

        {{-- if (show_init_pop && isFreeAccount) { --}}
        {{--   $('#init-pop-up').modal('show'); --}}
        {{-- } --}}
        window.addEventListener('close-modals', event => {
          $('.modal').each(function() {
            $(this).modal('hide');
          })
        });

      });
    </script>
  @endpush
</div>
