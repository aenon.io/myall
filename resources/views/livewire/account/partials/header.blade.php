<div class="card-box-style d-none d-lg-block rounded">
  <style>
    .dropdown-anchor {
      color: #ee0041;
      font-weight: 600;
      font-size: 14px;
    }
  </style>
  <!-- <div class="row"> -->
  <!--   <span class="d-flex">Hi, {{ Auth::user()->name }}</span> -->
  <!--   @if (Auth::user()->profile_pic)
-->
  <!--     <img class="rounded-50-lg" src="{{ asset('storage/' . Auth::user()->profile_pic) }}" alt="Images"> -->
  <!--
@else
-->
  <!--     <img class="rounded-50-lg" src="{{ asset('img/placeholder.png') }}" alt="Images"> -->
  <!--
@endif -->
  <!-- </div> -->
  <div class="row">
    <div class="col-md-4 text-nav-blue-col">
      <h4 class="text-navy-blue">Hi, {{ Auth::user()->name }}</h4>
    </div>
    <div class="col-md-8 d-flex">
      <div class="me-2 ms-auto">
        <!--<a href="#" data-toggle="tooltip" data-placement="top" title="Notifications">-->
        <!--    <i class="bx bxs-bell header_icons text-red"></i>-->
        <!--</a>-->
      </div>
      <div class="me-2">
        <a href="{{ url('share-profile') }}" data-toggle="tooltip" data-placement="bottom" title="Share">
          <i class="bx bx bxs-share-alt header_icons text-red"></i>
        </a>
      </div>
      <div class="me-2">
        <a href="{{ url('/') }}" data-toggle="tooltip" data-placement="bottom" title="Back to home page">
          <i class="bx bxs-home header_icons text-red"></i>
        </a>
      </div>
      <div class="dropdown me-2">
        <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown"
          aria-haspopup="true" aria-expanded="false" data-toggle="tooltip" data-placement="bottom" title="Settings">
          <i class="bx bxs-cog header_icons bg-red" alt="Settings"></i>
        </a>

        <div class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdownMenuLink">
          <a class="dropdown-item dropdown-anchor" href="{{ url('profile/edit') }}">Edit Profile</a>
          <a class="dropdown-item dropdown-anchor" href="{{ url('logout') }}">Logout</a>
          @if (Auth::user()->free_account == 0)
            <a class="dropdown-item dropdown-anchor" href="#" data-bs-toggle="modal"
              data-bs-target="#headVidModal">Explore Features and Functionality</a>
          @endif
        </div>
      </div>

      <div class="">
        <div class="searchBar">
          <form class="search-bar d-flex" method="get" action="{{ url('search') }}">
            <img src="{{ asset('img/search_icon.png') }}" alt="search-normal">
            <input class="form-control" type="search" placeholder="Need any help?" aria-label="Search" name="s">
          </form>
        </div>
      </div>
    </div>
  </div>
  <div class="modal" id="headVidModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-body">
          <div class="text-end">
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="cur_vid_cont">
            <video controls>
              <source src="{{ asset('vids/A little sneak peak!.mp4') }}" type="video/mp4">
              Your browser does not support the video tag.
            </video>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
