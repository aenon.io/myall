<div class="myallSearchFormContainer">
  {{-- <form class="myallSearchForm d-flex" method="get" action="{{ url('search') }}"> --}}
  {{--   <input class="" type="search" placeholder="Search info" --}}
  {{--     aria-label="Search" name="s"> --}}
  {{--   <svg viewBox="0 0 17 17" fill="none" xmlns="http://www.w3.org/2000/svg"> --}}
  {{--     <path d="M11.0711 10.0033H10.5445L10.3578 9.82326C11.0111 9.06326 11.4045 --}}
  {{--         8.07659 11.4045 7.00326C11.4045 4.60992 9.46446 2.66992 7.07113 --}}
  {{--         2.66992C4.67779 2.66992 2.73779 4.60992 2.73779 7.00326C2.73779 --}}
  {{--         9.39659 4.67779 11.3366 7.07113 11.3366C8.14446 11.3366 9.13113 --}}
  {{--         10.9433 9.89113 10.2899L10.0711 10.4766V11.0033L13.4045 --}}
  {{--         14.3299L14.3978 13.3366L11.0711 10.0033ZM7.07113 10.0033C5.41113 --}}
  {{--         10.0033 4.07113 8.66326 4.07113 7.00326C4.07113 5.34326 5.41113 --}}
  {{--         4.00326 7.07113 4.00326C8.73113 4.00326 10.0711 5.34326 10.0711 --}}
  {{--         7.00326C10.0711 8.66326 8.73113 10.0033 7.07113 10.0033Z" --}}
  {{--       fill="#FFADBC" /> --}}
  {{--   </svg> --}}
  {{-- </form> --}}

  <div class="myallProfile">

    <div id="myallProfileLanguage">
      <div class="__content">
        <svg width="16" height="17" viewBox="0 0 16 17" fill="none"
          xmlns="http://www.w3.org/2000/svg">
          <path d="M7.99992 1.83398C4.31992 1.83398 1.33325 4.82065 1.33325
            8.50065C1.33325 12.1807 4.31992 15.1673 7.99992 15.1673C11.6799
            15.1673 14.6666 12.1807 14.6666 8.50065C14.6666 4.82065 11.6799
            1.83398 7.99992 1.83398ZM2.66659 8.50065C2.66659 8.09398 2.71992
            7.69398 2.80659 7.31398L5.99325 10.5007V11.1673C5.99325 11.9007
            6.59325 12.5007 7.32659 12.5007V13.7873C4.70659 13.454 2.66659
            11.214 2.66659 8.50065ZM11.9266 12.1007C11.7533 11.5607 11.2599
            11.1673 10.6599 11.1673H9.99325V9.16732C9.99325 8.80065 9.69325
            8.50065 9.32659 8.50065H5.32659V7.16732H6.65992C7.02659 7.16732
            7.32659 6.86732 7.32659 6.50065V5.16732H8.65992C9.39325 5.16732
            9.99325 4.56732 9.99325 3.83398V3.56065C11.9466 4.34732 13.3333
            6.26732 13.3333 8.50065C13.3333 9.88732 12.7933 11.154 11.9266
            12.1007Z" fill="white" />
        </svg>
        Language
      </div>
      <div class="__menu">
        <div id="google_translate_element"></div>
      </div>
    </div>

    <div class="myallProfile-text">
      Hi, <strong>{{ Auth::user()->name }}</strong>
    </div>

    {{-- <div class="myallProfile-wrapper"> --}}
    {{--   <div class="myallProfile-img"> --}}
    {{--     <a href="{{ url('profile/edit') }}"> --}}
    {{--       @if (auth::user()->profile_pic) --}}
    {{--         <img class="" --}}
    {{--           src="{{ asset('storage/' . Auth::user()->profile_pic) }}" --}}
    {{--           alt="Images"> --}}
    {{--       @else --}}
    {{--         <img class="" src="{{ asset('img/placeholder.png') }}" --}}
    {{--           alt="Images"> --}}
    {{--       @endif --}}
    {{--     </a> --}}
    {{--   </div> --}}
    {{-- </div> --}}

    <div id="profile-burger">
      <div class="myallProfile-img">
        <a href="{{ url('profile/edit') }}">
          @if (auth::user()->profile_pic)
            <img class=""
              src="{{ asset('storage/' . Auth::user()->profile_pic) }}"
              alt="Images">
          @else
            <img class="" src="{{ asset('img/placeholder.png') }}"
              alt="Images">
          @endif
        </a>
      </div>
      {{-- <svg width="16" height="17" viewBox="0 0 16 17" fill="none" --}}
      {{--   xmlns="http://www.w3.org/2000/svg"> --}}
      {{--   <path d="M7.99998 13.3465C7.72499 13.3465 7.48958 13.2486 7.29375 --}}
      {{--       13.0528C7.09792 12.857 7 12.6216 7 12.3466C7 12.0716 7.09792 11.8362 --}}
      {{--       7.29375 11.6403C7.48958 11.4445 7.72499 11.3466 7.99998 --}}
      {{--       11.3466C8.27497 11.3466 8.51038 11.4445 8.70622 11.6403C8.90205 --}}
      {{--       11.8362 8.99997 12.0716 8.99997 12.3466C8.99997 12.6216 8.90205 --}}
      {{--       12.857 8.70622 13.0528C8.51038 13.2486 8.27497 13.3465 7.99998 --}}
      {{--       13.3465ZM7.99998 9.5004C7.72499 9.5004 7.48958 9.40248 7.29375 --}}
      {{--       9.20665C7.09792 9.01081 7 8.7754 7 8.50041C7 8.22543 7.09792 7.99001 --}}
      {{--       7.29375 7.79418C7.48958 7.59835 7.72499 7.50043 7.99998 --}}
      {{--       7.50043C8.27497 7.50043 8.51038 7.59835 8.70622 7.79418C8.90205 --}}
      {{--       7.99001 8.99997 8.22543 8.99997 8.50041C8.99997 8.7754 8.90205 --}}
      {{--       9.01081 8.70622 9.20665C8.51038 9.40248 8.27497 9.5004 7.99998 --}}
      {{--       9.5004ZM7.99998 5.65425C7.72499 5.65425 7.48958 5.55633 7.29375 --}}
      {{--       5.3605C7.09792 5.16467 7 4.92926 7 4.65426C7 4.37928 7.09792 4.14386 --}}
      {{--       7.29375 3.94803C7.48958 3.75221 7.72499 3.6543 7.99998 --}}
      {{--       3.6543C8.27497 3.6543 8.51038 3.75221 8.70622 3.94803C8.90205 --}}
      {{--       4.14386 8.99997 4.37928 8.99997 4.65426C8.99997 4.92926 8.90205 --}}
      {{--       5.16467 8.70622 5.3605C8.51038 5.55633 8.27497 5.65425 7.99998 --}}
      {{--       5.65425Z" fill="white" /> --}}
      {{-- </svg> --}}
      <div id="profile-burger-menu">
        {{-- <div class="links"> --}}
        {{--   <a class="links-buttons" href=""> --}}
        {{--     Our Features --}}
        {{--   </a> --}}
        {{-- </div> --}}
        <div class="links">
          <a class="links-buttons" href="{{ url('profile/edit') }}">
            Edit Profile
          </a>
        </div>
        <div class="links">
          <a class="links-buttons" href="{{ url('logout') }}">
            Sign out
          </a>
        </div>
      </div>
    </div>

  </div>
</div>

@push('scripts')
  <script>
    document.addEventListener('DOMContentLoaded', function() {
      const profileBurger = document.getElementById('profile-burger');
      const profileBurgerMenu = document.getElementById(
        'profile-burger-menu');
      const myallProfileLanguage = document.querySelector(
        '#myallProfileLanguage .__content');
      const languageMenu = document.querySelector(
        '#myallProfileLanguage .__menu');

      profileBurger.addEventListener('mouseenter', () => {
        profileBurgerMenu.style.display = 'block';
      });

      profileBurgerMenu.addEventListener('mouseleave', () => {
        profileBurgerMenu.style.display = 'none';
      });

      profileBurgerMenu.querySelectorAll('a').forEach(link => {
        link.addEventListener('click', () => {
          profileBurgerMenu.style.display = 'none';
        });
      });

      myallProfileLanguage.addEventListener('click', (event) => {
        languageMenu.style.display = languageMenu.style.display ===
          'block' ? 'none' : 'block';
        event.stopPropagation();
      });

      document.addEventListener('click', (event) => {
        if (!languageMenu.contains(event.target) && event.target !==
          myallProfileLanguage) {
          languageMenu.style.display = 'none';
        }
      });
    });
  </script>
@endpush
