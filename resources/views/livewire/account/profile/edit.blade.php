<livewire:account.partials.my-search-bar />

<div class="myallEditProfileContainer">
  <div class="__heading">
    <h3>Your Profile</h3>
    <div>
      <a href="{{ url('profile') }}" class="btn __home">
        <svg width="16" height="17" viewBox="0 0 16 17" fill="none"
          xmlns="http://www.w3.org/2000/svg">
          <path d="M8.00016 4.62732L11.3335
            7.62732V12.834H10.0002V8.83398H6.00016V12.834H4.66683V7.62732L8.00016
            4.62732ZM8.00016 2.83398L1.3335
            8.83398H3.3335V14.1673H7.3335V10.1673H8.66683V14.1673H12.6668V8.83398H14.6668L8.00016
            2.83398Z" fill="#CF3D60" />
        </svg>
        Back Home
      </a>
    </div>
  </div>
  <div class="myallEditProfileContent">
    <div class="__sidebar">
      <aside>

        @if (empty($img_preview))
          <div>Empty Image</div>
        @endif

        <form wire:submit.prevent="updateUser('profile_pic')"
          id="form-profile-pic">
          <div class="__avatar-container">
            <img src="{{ asset($img_preview) }}" class="__avatar w-100">
          </div>
          <div class="__user-fullname">
            {{ Auth::user()->name }} {{ Auth::user()->surname }}
          </div>
          <hr>
          <div class="__inputs">
            <div class="__upload-btn">
              @if (!$file_selected)
                <label class="btn form-label __custom-label" for="profile_pic">
                  Upload image
                </label>
              @else
                <label class="btn form-label __custom-label __save"
                  for="profile_pic">
                  <svg width="21" height="20" viewBox="0 0 21 20"
                    fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M14.6667 2.5H4.66667C3.74167 2.5 3 3.25 3
                      4.16667V15.8333C3 16.75 3.74167 17.5 4.66667
                      17.5H16.3333C17.25 17.5 18 16.75 18
                      15.8333V5.83333L14.6667 2.5ZM16.3333
                      15.8333H4.66667V4.16667H13.975L16.3333 6.525V15.8333ZM10.5
                      10C9.11667 10 8 11.1167 8 12.5C8 13.8833 9.11667 15 10.5
                      15C11.8833 15 13 13.8833 13 12.5C13 11.1167 11.8833 10
                      10.5 10ZM5.5 5H13V8.33333H5.5V5Z" fill="white" />
                  </svg>
                  Save
                </label>
              @endif
            </div>
            <input type="file" class="form-control __file-input"
              id="profile_pic" name="profile_pic" wire:model="profile_pic">
            <p class="__subtext">Max Size: 1MB (jpeg/png)</p>
            @error('profile_pic')
              <span class="error text-red">
                Failed to upload(Image must not exceed limit of 1MB and must be of
                type png or jpeg)
              </span></br>
            @enderror
          </div>
        </form>
      </aside>
    </div>
    <div class="__content">
      <section>

        <form wire:submit.prevent="updateUser('details')">
          <div class="row">
            <div class="col-md-12">
              <div class="__title mb-3">
                Your Details
              </div>
            </div>
            <div class="col-md-12">
              <div class="__note mb-3">
                <svg width="20" height="20" viewBox="0 0 20 20"
                  fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path d="M9.16663 12.4993H10.8333V14.166H9.16663V12.4993ZM9.16663
                    5.83268H10.8333V10.8327H9.16663V5.83268ZM9.99163
                    1.66602C5.39163 1.66602 1.66663 5.39935 1.66663
                    9.99935C1.66663 14.5993 5.39163 18.3327 9.99163 18.3327C14.6
                    18.3327 18.3333 14.5993 18.3333 9.99935C18.3333 5.39935 14.6
                    1.66602 9.99163 1.66602ZM9.99996 16.666C6.31663 16.666
                    3.33329 13.6827 3.33329 9.99935C3.33329 6.31602 6.31663
                    3.33268 9.99996 3.33268C13.6833 3.33268 16.6666 6.31602
                    16.6666 9.99935C16.6666 13.6827 13.6833 16.666 9.99996
                    16.666Z" fill="#040A5E" />
                </svg>
                <div>
                  To change your initial registration details as shown here,
                  please
                  <strong>contact us.
                  </strong>
                </div>
              </div>
            </div>

            {{-- <div class="col-md-4"> --}}
            {{--   <div class="mb-3"> --}}
            {{--     <label class="form-label">Name</label> --}}
            {{--     <input type="text" class="form-control" name="name" --}}
            {{--       wire:model.defer="name" disabled> --}}
            {{--   </div> --}}
            {{-- </div> --}}

            {{-- <div class="col-md-4"> --}}
            {{--   <div class="mb-3"> --}}
            {{--     <label class="form-label">Middle Name</label> --}}
            {{--     <input type="text" class="form-control" name="middle_name" --}}
            {{--       wire:model.defer="middle_name"> --}}
            {{--   </div> --}}
            {{-- </div> --}}

            {{-- <div class="col-md-4"> --}}
            {{--   <div class="mb-3"> --}}
            {{--     <label class="form-label">Surname</label> --}}
            {{--     <input type="text" class="form-control" name="surname" --}}
            {{--       wire:model.defer="surname" disabled> --}}
            {{--   </div> --}}
            {{-- </div> --}}
            <div class="col-md-4">
              <div class="mb-3">
                <label class="form-label">Email</label>
                <input type="email" class="form-control" name="email"
                  wire:model.defer="email" disabled>
              </div>
            </div>
            <div class="col-md-4">
              <div class="mb-3">
                <label class="form-label">ID Number</label>
                <input type="text" class="form-control" name="id_number"
                  wire:model.defer="id_number" disabled>
              </div>
            </div>
            <div class="col-md-4">
              <div class="mb-3">
                <label class="form-label">Passport Number</label>
                <input type="text" class="form-control"
                  name="passport_number" wire:model.defer="passport_number"
                  disabled>
              </div>
            </div>
            <hr>
            <div class="col-md-4">
              <div class="mb-3">
                <label class="form-label">Contact Number</label>
                <input type="text" class="form-control" name="contact_number"
                  wire:model.defer="contact_number">
              </div>
            </div>
            <div class="col-md-4">
              <div class="mb-3">
                <label class="form-label">Date of Birth</label>
                <input type="date" class="form-control" name="date_of_birth"
                  wire:model.defer="date_of_birth">
              </div>
            </div>
            <div class="col-md-4">
              <div class="mb-3">
                <label class="form-label">Occupation</label>
                <input type="text" class="form-control" name="occupation"
                  wire:model.defer="occupation">
              </div>
            </div>
            <div class="col-md-12">
              <div class="mb-3">
                <label class="form-label">Bio</label>
                <textarea class="form-control" name="bio" wire:model="bio" maxlength="250"
                  placeholder="Tell us a bit about yourself"></textarea>
                <small id="emailHelp" class="form-text text-muted">250
                  characters <b>{{ strlen($bio) }} / 250</b></small>
              </div>
            </div>
            <div class="col-md-12">
              <div class="text-end">
                <a href="{{ url('profile') }}"
                  class="btn btn-outline-two">Cancel</a>
                <input type="submit" class="btn btn-red btn-lg"
                  value="save">
              </div>
            </div>



        </form>
      </section>
    </div>
  </div>
</div>


@push('scripts')
  <script>
    document.addEventListener('DOMContentLoaded', function() {
      const profilePicFileInput = document.getElementById('profile_pic');
      const profilePicLabel = document.querySelector('.__custom-label');
      const profilePicForm = document.getElementById('form-profile-pic');

      profilePicLabel.addEventListener('click', function(event) {
        event.preventDefault();

        if (profilePicFileInput.files.length === 0) {
          profilePicFileInput.click();
        } else {
          profilePicLabel.textContent = 'Uploading...';
          setTimeout(() => {
            profilePicForm.dispatchEvent(new Event('submit', {
              bubbles: true
            }));
          }, 100);
        }
      });

      {{-- profilePicFileInput.addEventListener('change', function() { --}}
      {{--   console.log('File input change event triggered.'); --}}
      {{--   if (profilePicFileInput.files.length > 0) { --}}
      {{--     console.log('File input change event triggered. Added save'); --}}
      {{--     profilePicLabel.innerHTML = --}}
      {{--       'Save'; // Change to 'Save' after file is selected --}}
      {{--   } else { --}}
      {{--     console.log( --}}
      {{--       'File input change event triggered. Added Select File'); --}}
      {{-- --}}
      {{--     profilePicLabel.innerHTML = --}}
      {{--       'Select File'; // Revert if no file selected --}}
      {{--   } --}}
      {{-- }); --}}
    });
  </script>
@endpush

{{-- <div class="container-fluid"> --}}
{{--   <div class="row"> --}}
{{--     <div class="col-md-12"> --}}
{{-- --}}
{{-- --}}
{{--       <div class="card-box-style rounded p-5"> --}}
{{--         @if ($errors->any()) --}}
{{--           <div class="alert alert-danger"> --}}
{{--             <span>{{ $errors->first() }}</span> --}}
{{--           </div> --}}
{{--         @endif --}}
{{-- --}}
{{--         <form wire:submit.prevent="updateUser"> --}}
{{--           <div class="row"> --}}
{{--             <div class="col-md-4"> --}}
{{--               <img src="{{ asset($img_preview) }}" class="w-100"> --}}
{{--               <div class="mb-3"> --}}
{{--                 <label class="form-label">Upload Profile Pic</label> --}}
{{--                 <input type="file" class="form-control" name="profile_pic" --}}
{{--                   wire:model.defer="profile_pic"> --}}
{{--                 @error('profile_pic') --}}
{{--                   <span class="error text-red">Failed to upload(Image must not --}}
{{--                     exceed limit of 1MB and must be of type png --}}
{{--                     or jpeg)</span></br> --}}
{{--                 @enderror --}}
{{--                 <span class="text-muted">Images: Max File Size 1MB (jpeg, --}}
{{--                   png)</span> --}}
{{--               </div> --}}
{{--             </div> --}}
{{--             <div class="col-md-8"> --}}
{{--               <div class="row"> --}}
{{--                 <div class="col-md-12 mb-3"> --}}
{{--                   <p class="text-danger"><b>NOTE:</b> To change your contact --}}
{{--                     details, please contact us.</p> --}}
{{--                 </div> --}}
{{--                 <div class="col-md-4"> --}}
{{--                   <div class="mb-3"> --}}
{{--                     <label class="form-label">Name</label> --}}
{{--                     <input type="text" class="form-control" name="name" --}}
{{--                       wire:model.defer="name" disabled> --}}
{{--                   </div> --}}
{{--                 </div> --}}
{{--                 <div class="col-md-4"> --}}
{{--                   <div class="mb-3"> --}}
{{--                     <label class="form-label">Middle Name</label> --}}
{{--                     <input type="text" class="form-control" --}}
{{--                       name="middle_name" wire:model.defer="middle_name"> --}}
{{--                   </div> --}}
{{--                 </div> --}}
{{--                 <div class="col-md-4"> --}}
{{--                   <div class="mb-3"> --}}
{{--                     <label class="form-label">Surname</label> --}}
{{--                     <input type="text" class="form-control" name="surname" --}}
{{--                       wire:model.defer="surname" disabled> --}}
{{--                   </div> --}}
{{--                 </div> --}}
{{--                 <div class="col-md-6"> --}}
{{--                   <div class="mb-3"> --}}
{{--                     <label class="form-label">Email</label> --}}
{{--                     <input type="email" class="form-control" name="email" --}}
{{--                       wire:model.defer="email" disabled> --}}
{{--                   </div> --}}
{{--                 </div> --}}
{{--                 <div class="col-md-6"> --}}
{{--                   <div class="mb-3"> --}}
{{--                     <label class="form-label">Contact Number</label> --}}
{{--                     <input type="text" class="form-control" --}}
{{--                       name="contact_number" wire:model.defer="contact_number"> --}}
{{--                   </div> --}}
{{--                 </div> --}}
{{--                 <div class="col-md-4"> --}}
{{--                   <div class="mb-3"> --}}
{{--                     <label class="form-label">ID Number</label> --}}
{{--                     <input type="text" class="form-control" name="id_number" --}}
{{--                       wire:model.defer="id_number" disabled> --}}
{{--                   </div> --}}
{{--                 </div> --}}
{{--                 <div class="col-md-4"> --}}
{{--                   <div class="mb-3"> --}}
{{--                     <label class="form-label">Passport Number</label> --}}
{{--                     <input type="text" class="form-control" --}}
{{--                       name="passport_number" wire:model.defer="passport_number" --}}
{{--                       disabled> --}}
{{--                   </div> --}}
{{--                 </div> --}}
{{--                 <div class="col-md-4"> --}}
{{--                   <div class="mb-3"> --}}
{{--                     <label class="form-label">Date of Birth</label> --}}
{{--                     <input type="date" class="form-control" --}}
{{--                       name="date_of_birth" wire:model.defer="date_of_birth"> --}}
{{--                   </div> --}}
{{--                 </div> --}}
{{--                 <div class="col-md-12"> --}}
{{--                   <div class="mb-3"> --}}
{{--                     <label class="form-label">Occupation</label> --}}
{{--                     <input type="text" class="form-control" name="occupation" --}}
{{--                       wire:model.defer="occupation"> --}}
{{--                   </div> --}}
{{--                 </div> --}}
{{--                 <div class="col-md-12"> --}}
{{--                   <div class="mb-3"> --}}
{{--                     <label class="form-label">Bio</label> --}}
{{--                     <textarea class="form-control" name="bio" wire:model="bio" maxlength="250" --}}
{{--                       placeholder="Tell us a bit about yourself"></textarea> --}}
{{--                     <small id="emailHelp" class="form-text text-muted">250 --}}
{{--                       characters <b>{{ strlen($bio) }} / --}}
{{--                         250</b></small> --}}
{{--                   </div> --}}
{{--                 </div> --}}
{{--                 <div class="col-md-12"> --}}
{{--                   <div class="text-center"> --}}
{{--                     <input type="submit" class="btn btn-red btn-lg" --}}
{{--                       value="save"> --}}
{{--                     <a href="{{ url('profile') }}" --}}
{{--                       class="btn btn-outline-two">Cancel</a> --}}
{{--                   </div> --}}
{{--                 </div> --}}
{{--               </div> --}}
{{--             </div> --}}
{{--         </form> --}}
{{--       </div> --}}
{{--       </form> --}}
{{--     </div> --}}
{{--   </div> --}}
{{--   <div class="container-fluid"> --}}
{{--     <div class="row"> --}}
{{--       <div class="col-md-12"> --}}
{{--         <div class="card-box-style rounded p-5"> --}}
{{--           <form> --}}
{{--             <div class="row"> --}}
{{--               <div class="col-md-12"> --}}
{{--                 <h5>Update Password</h5> --}}
{{--               </div> --}}
{{--               <div class="col-md-6"> --}}
{{--                 <div class="mb-3"> --}}
{{--                   <label class="form-label">New Password</label> --}}
{{--                   <input type="password" class="form-control" name="password" --}}
{{--                     wire:model.defer="password"> --}}
{{--                 </div> --}}
{{--               </div> --}}
{{--               <div class="col-md-6"> --}}
{{--                 <div class="mb-3"> --}}
{{--                   <label class="form-label">Confirm New Password</label> --}}
{{--                   <input type="password" class="form-control" --}}
{{--                     name="password_confirmation" --}}
{{--                     wire:model.defer="password_confirmation"> --}}
{{--                 </div> --}}
{{--               </div> --}}
{{--               <div class="col-md-12"> --}}
{{--                 <div class="text-center"> --}}
{{--                   <input type="submit" class="btn btn-red" value="save"> --}}
{{--                 </div> --}}
{{--               </div> --}}
{{--             </div> --}}
{{--           </form> --}}
{{--         </div> --}}
{{--       </div> --}}
{{--     </div> --}}
{{--   </div> --}}
