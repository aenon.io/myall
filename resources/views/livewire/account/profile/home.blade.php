{{-- <!-- OVER HERE --> --}}
{{-- <div class="myallMainAppInner"> --}}

<livewire:account.partials.my-search-bar />

<div class="myallProfileContainer">
  <livewire:account.profile.partials.contacts :activeTab="$activeTab" />
  {{-- <section class="timeline-section"></section> --}}
  <livewire:account.profile.partials.timeline />
  <livewire:account.profile.partials.organiser />

  {{-- <div class="row d-block d-lg-none"> --}}
  {{--   <div class="col-md-12"> --}}
  {{--     <div style="margin-bottom: 100px"></div> --}}
  {{--   </div> --}}
  {{-- </div> --}}

  {{-- </div> --}}

  <div class="modal" tabindex="-1" id="contact_modal" wire:ignore.self>
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Add Contact</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal"
            aria-label="Close"></button>
        </div>
        <div class="modal-body">
          @if ($errors->any())
            <div class="alert alert-danger">
              <span>{{ $errors->first() }}</span>
            </div>
          @endif
          <form wire:submit.prevent="saveContact">
            <div class="mb-3">
              <label class="form-label">Name</label>
              <input type="text" class="form-control" name="name"
                wire:model.defer="contact_name">
            </div>
            <div class="mb-3">
              <label class="form-label">Surname</label>
              <input type="text" class="form-control" name="surname"
                wire:model.defer="contact_surname">
            </div>
            <div class="mb-3">
              <label class="form-label">Email</label>
              <input type="email" class="form-control" name="email"
                wire:model.defer="contact_email">
            </div>
            <div class="mb-3">
              <label class="form-label">Contact Number</label>
              <input type="text" class="form-control" name="contact_number"
                wire:model.defer="contact_number">
            </div>
            @if ($contact_type == 'contact')
              <div class="mb-3">
                <label class="form-label">Relation</label>
                <!--<input type="text" class="form-control" name="relation" wire:model.defer="relation">-->
                <select class="form-control" wire:model="relation">
                  <option value="">Select Option</option>
                  @foreach ($relations as $rel)
                    <option value="{{ $rel }}">{{ $rel }}
                    </option>
                  @endforeach
                </select>
              </div>
            @else
              <div class="mb-3">
                <label class="form-label">Organisation / Company</label>
                <input type="text" class="form-control"
                  name="organisation_or_company"
                  wire:model.defer="organisation_or_company">
              </div>
            @endif
            <div class="mb-3 text-center">
              <input type="submit" class="btn btn-red" value="SAVE">
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>

  <div wire:ignore.self class="modal" id="welcomeModal" tabindex="-1"
    role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-body">
          <div class="row mt-3" style="justify-content: center;">
            <div class="col-md-12" style="width: 93%;">
              <h4 class="text-navy-blue">Welcome to MyAll!</h4>
              <h4 class="text-red">
                {{ Auth::user()->name . ' ' . Auth::user()->surname }}</h4>
              <div class="mb-3 mt-2">
                @if (Auth::user()->free_account == 1)
                  <p>
                    This free subscription allows you to only view what one Full
                    Access user has shared with you. Upgrade anytime for full
                    access!</p>
                @else
                  <p>
                    Create your <i>Personal Profile</i> for convenient life
                    management, document storage,
                    Will creation, and to create your online <i>Network</i>.
                    Choose what to share and simplify your life with MyAll's
                    easy-to-use platform.
                  </p>
                @endif
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-lg btn-gradient gradient-1"
            wire:click.prevent="closeWelcomeModal"
            data-bs-dismiss="modal">Continue</button>
          {{-- @if (Auth::user()->free_account == 0) --}}
          {{--   <button type="button" class="btn btn-lg btn-gradient gradient-1" --}}
          {{--     wire:click.prevent="closeWelcomeModal('pay_vid')" --}}
          {{--     data-bs-dismiss="modal">Continue and play --}}
          {{--     video</button> --}}
          {{-- @endif --}}
        </div>
      </div>
    </div>
  </div>
  <div class="modal" id="welcomeVidModal" tabindex="-1"
    aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-body">
          <div class="text-end">
            <button type="button" class="btn-close" data-bs-dismiss="modal"
              aria-label="Close"></button>
          </div>
          <div class="cur_vid_cont">
            <video controls>
              <source src="{{ asset('vids/A little sneak peak!.mp4') }}"
                type="video/mp4">
              Your browser does not support the video tag.
            </video>
          </div>
        </div>
      </div>
    </div>
  </div>
  @push('scripts')
    <script>
      window.addEventListener('show-emploer-vid-modal', event => {
        $('.modal').modal('hide');
        $('#welcomeVidModal').modal('show');
      });
      window.addEventListener('show-contact-modal', event => {
        $('#contact_modal').modal('show');
      });
      window.addEventListener('close-contact-modal', event => {
        $('.modal').modal('hide');
      });

      document.addEventListener('livewire:load', function() {
        var welcome_modal = "{{ Auth::user()->welcome_modal }}";
        if (welcome_modal == 0) {
          $('#welcomeModal').modal('show');
        }
      });
    </script>
  @endpush
