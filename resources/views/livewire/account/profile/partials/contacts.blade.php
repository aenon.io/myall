@php

  function hashString($name)
  {
      $hash = 0;
      for ($i = 0; $i < strlen($name); $i++) {
          $hash = ord($name[$i]) + (($hash << 5) - $hash);
          $hash = $hash & $hash; // Convert to 32bit integer
      }
      return $hash;
  }

  function getColorScheme($name, $colorSchemes)
  {
      $hash = hashString($name);
      $index = abs($hash) % count($colorSchemes);
      return $colorSchemes[$index];
  }

  $colorSchemes = [
      ['fill' => '#EBEEFF', 'border' => '#929BCA', 'font' => '#040A5E'],
      ['fill' => '#FDE9FF', 'border' => '#C692CA', 'font' => '#4D1552'],
      ['fill' => '#E9FFE8', 'border' => '#93CA92', 'font' => '#144D13'],
      ['fill' => '#FFF9ED', 'border' => '#EAC57B', 'font' => '#5B4417'],
      ['fill' => '#ECF9FF', 'border' => '#7DC1DF', 'font' => '#104358'],
  ];

  $contactTypes = ['family', 'friend', 'professionals', 'other'];
@endphp

<div class="contacts-section">

  <div class="__heading">
    <h3>My Contacts</h3>
    @if (Auth::user()->free_account == 0)
      <button class="btn" onclick="Livewire.emit('emitNewShare','tabShared')">
        <svg width="17" height="17" viewBox="0 0 17 17" fill="none"
          xmlns="http://www.w3.org/2000/svg">
          <path d="M13.901
          6.50008V4.50008H12.5677V6.50008H10.5677V7.83341H12.5677V9.83341H13.901V7.83341H15.901V6.50008H13.901ZM6.56771
          8.50008C8.04104 8.50008 9.23437 7.30675 9.23437 5.83341C9.23437
          4.36008 8.04104 3.16675 6.56771 3.16675C5.09437 3.16675 3.90104
          4.36008 3.90104 5.83341C3.90104 7.30675 5.09437 8.50008 6.56771
          8.50008ZM6.56771 4.50008C7.30104 4.50008 7.90104 5.10008 7.90104
          5.83341C7.90104 6.56675 7.30104 7.16675 6.56771 7.16675C5.83437
          7.16675 5.23437 6.56675 5.23437 5.83341C5.23437 5.10008 5.83437
          4.50008 6.56771 4.50008ZM10.8277 10.2067C9.70771 9.63341 8.25437
          9.16675 6.56771 9.16675C4.88104 9.16675 3.42771 9.63341 2.30771
          10.2067C1.64104 10.5467 1.23438 11.2334 1.23438
          11.9801V13.8334H11.901V11.9801C11.901 11.2334 11.4944 10.5467 10.8277
          10.2067ZM10.5677 12.5001H2.56771V11.9801C2.56771 11.7267 2.70104
          11.5001 2.91437 11.3934C3.70771 10.9867 4.98771 10.5001 6.56771
          10.5001C8.14771 10.5001 9.42771 10.9867 10.221 11.3934C10.4344 11.5001
          10.5677 11.7267 10.5677 11.9801V12.5001Z" fill="white" />
        </svg>
        New Share
      </button>
    @endif
  </div>

  <div class="__tabs">
    <button class="__tab @if ($activeTab == 'tabShared') active @endif"
      onclick="Livewire.emit('loadTabContentProfile','tabShared')">
      <div>
        <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
          xmlns="http://www.w3.org/2000/svg">
          <path d="M6.33333 3V4.33333H10.7267L3 12.06L3.94 13L11.6667
            5.27333V9.66667H13V3H6.33333Z" />
        </svg>
        <span>Shared with</span>
      </div>
    </button>
    <button class="__tab @if ($activeTab == 'tabReceived') active @endif"
      onclick="Livewire.emit('loadTabContentProfile','tabReceived')">
      <div>
        <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
          xmlns="http://www.w3.org/2000/svg">
          <path d="M13 3.94L12.06 3L4.33333
            10.7267V6.33333H3V13H9.66667V11.6667H5.27333L13 3.94Z" />
        </svg>
        <span>Received from</span>
      </div>
    </button>
  </div>
  <section class="__tab-content"
    style="@if ($activeTab == 'tabShared') border-radius: 0px 20px 20px 20px; @else border-radius: 20px 0px 20px 20px; @endif">
    @if ($activeTab == 'tabShared')
      @if ($shared_with->isEmpty())
        <p>Not sent any shares</p>
      @else
        @foreach ($shared_with as $share)
          <button
            wire:click.prevent="loadViewShare({{ $share->id }},'tabShared')">
            <div class="__share {{ $share->color }} d-flex">
              <div class="__details">
                <div class="__avatar">
                  @if ($share->contact)
                    @if ($share->contact->profile_pic)
                      <img
                        src="{{ asset('storage/' . $share->contact->profile_pic) }} "
                        alt="">
                    @else
                      {{ getInitials($share->name) }}
                    @endif
                  @else
                    {{ getInitials($share->name) }}
                  @endif
                </div>
                <div class="__text">
                  <div class="__name">
                    <span>{{ $share->name }}</span>
                  </div>
                  <div class="__relation">
                    @if (!empty($share->family))
                      {{ $share->family }}
                    @elseif (!empty($share->friends))
                      {{ $share->friends }}
                    @elseif (!empty($share->professional))
                      {{ $share->professional }}
                    @endif
                  </div>
                </div>
              </div>
              <div>
                <svg width="33" height="32" viewBox="0 0 33 32"
                  fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path d="M13.5089 8L11.6289 9.88L17.7356 16L11.6289 22.12L13.5089
                  24L21.5089 16L13.5089 8Z" />
                </svg>
              </div>
            </div>
          </button>
        @endforeach
      @endif
    @endif
    @if ($activeTab == 'tabReceived')
      @if ($shared_with_me->isEmpty())
        <p>Not received any shares</p>
      @else
        @foreach ($shared_with_me as $share)
          <button
            wire:click.prevent="loadViewShare({{ $share->id }},
          'tabReceived')">
            <div class="__share {{ $share->color }} d-flex">
              <div class="__details">
                <div class="__avatar">
                  @if ($share->contact)
                    @if ($share->user->profile_pic)
                      <img
                        src="{{ asset('storage/' . $share->user->profile_pic) }} "
                        alt="">
                    @else
                      {{ getInitials($share->name) }}
                    @endif
                  @else
                    {{ getInitials($share->name) }}
                  @endif
                </div>
                <div class="__text">
                  <div class="__name">
                    <span>{{ $share->user->name . ' ' . $share->user->surname }}</span>
                  </div>
                  {{-- <div class="__relation"> --}}
                  {{--   @if (!empty($share->family)) --}}
                  {{--     {{ $share->family }} --}}
                  {{--   @elseif (!empty($share->friends)) --}}
                  {{--     {{ $share->friends }} --}}
                  {{--   @elseif (!empty($share->professional)) --}}
                  {{--     {{ $share->professional }} --}}
                  {{--   @endif --}}
                  {{-- </div> --}}
                </div>
              </div>
              <div>
                <svg width="33" height="32" viewBox="0 0 33 32"
                  fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path d="M13.5089 8L11.6289 9.88L17.7356 16L11.6289 22.12L13.5089
                  24L21.5089 16L13.5089 8Z" />
                </svg>
              </div>
            </div>
          </button>
        @endforeach
      @endif
    @endif
  </section>

  {{-- @if (Auth::user()->free_account == 0) --}}
  {{--   <div>{{ $activeTab }}</div> --}}
  {{--   @foreach ($contactTypes as $type) --}}
  {{--     @foreach (Auth::user()->contactsByType($type)->get() as $contact) --}}
  {{-- $contact->id --}}
  {{-- $contact->name --}}
  {{-- $contact->surname --}}
  {{-- $contact->relation --}}
  {{-- $contact->contact_number --}}
  {{-- $contact->email --}}
  {{-- $contact->name --}}
  {{-- --}}
  {{--       @php --}}
  {{--         $name = $contact->name . ' ' . $contact->surname; --}}
  {{--         $colorScheme = getColorScheme($name, $colorSchemes); --}}
  {{--       @endphp --}}
  {{-- --}}
  {{--       <div class="__contact" --}}
  {{--         style="--_fill: {{ $colorScheme['fill'] }}; --}}
  {{--       --_border: {{ $colorScheme['border'] }}; --_font: {{ $colorScheme['font'] }}"> --}}
  {{--         <div class="__avatar"> --}}
  {{--           {{ getInitials($name) }} --}}
  {{--         </div> --}}
  {{--         <div class="__content"> --}}
  {{--           <div class="__name-container"> --}}
  {{--             <div class="__name">{{ $name }}</div> --}}
  {{--             <div class="__badge"> --}}
  {{--               <div class="__type">{{ $type }}</div> --}}
  {{--             </div> --}}
  {{--           </div> --}}
  {{--           <div class="__relation">{{ $contact->relation }}</div> --}}
  {{--         </div> --}}
  {{--       </div> --}}
  {{--     @endforeach --}}
  {{--   @endforeach --}}



  {{-- <div class="scroll-bar-content card-box-style"> --}}
  {{-- --}}
  {{--   <div class="others-title"> --}}
  {{--     <h3><b>Emergency Contacts</b></h3> --}}
  {{--     <p class="mt-2" style="line-height: 1.2">You can edit these --}}
  {{--       emergency contact details in the WrapUp --}}
  {{--       Organiser.</p> --}}
  {{--   </div> --}}
  {{-- --}}
  {{--   <div class="content-height style-two" data-simplebar="init"> --}}
  {{--     <div class="simplebar-wrapper" style="margin: 0px;"> --}}
  {{--       <div class="simplebar-height-auto-observer-wrapper"> --}}
  {{--         <div class="simplebar-height-auto-observer"> --}}
  {{--         </div> --}}
  {{--       </div> --}}
  {{--       <div class="simplebar-mask"> --}}
  {{--         <div class="simplebar-offset" style="right: -15px; bottom: 0px;"> --}}
  {{--           <div class="simplebar-content-wrapper" --}}
  {{--             style="height: 100%; overflow: hidden scroll;"> --}}
  {{--             <div class="simplebar-content" style="padding: 0px;"> --}}
  {{--               <div class=""> --}}
  {{--                 <div class="d-flex"> --}}
  {{--                   <b class="text-red">Family</b> --}}
  {{-- <span class="ms-auto"> --}}
  {{--   <a href="#" --}}
  {{--     wire:click.prevent="showAddContactModal('contact')"><i --}}
  {{--       class="bx bx-plus contacts-icon"></i></a> --}}
  {{-- </span> --}}
  {{--                 </div> --}}
  {{--                 <div class=""> --}}
  {{--                   <div class="accordion mt-3" id="accordionFamilyFriends"> --}}
  {{--                     @php --}}
  {{--                       $count = 1; --}}
  {{--                     @endphp --}}
  {{--                     @foreach (Auth::user()->contactsByType('family')->get() as $contact) --}}
  {{--                       <div class="accordion-item"> --}}
  {{--                         <h2 class="accordion-header" --}}
  {{--                           id="heading{{ $contact->id }}"> --}}
  {{--                           <button --}}
  {{--                             class="accordion-button d-flex @if ($count > 0) collapsed @endif" --}}
  {{--                             type="button" data-bs-toggle="collapse" --}}
  {{--                             data-bs-target="#collapse{{ $contact->id }}" --}}
  {{--                             aria-expanded="@if ($count == 0) true @else false @endif" --}}
  {{--                             aria-controls="collapse{{ $contact->id }}" --}}
  {{--                             wire:click="showActionButtons({{ $contact->id }}, 'fam')" --}}
  {{--                             wire:ignore.self> --}}
  {{--                             <span> --}}
  {{--                               <i --}}
  {{--                                 class="bx bxs-user accordion-icon"></i>{{ $contact->name . ' ' . $contact->surname }} --}}
  {{--                             </span> --}}
  {{--                           </button> --}}
  {{--                         </h2> --}}
  {{--                         <div id="collapse{{ $contact->id }}" --}}
  {{--                           class="accordion-collapse @if ($count == 0) show @endif collapse" --}}
  {{--                           aria-labelledby="heading{{ $contact->id }}" --}}
  {{--                           data-bs-parent="#accordionFamilyFriends" --}}
  {{--                           style="" wire:ignore.self> --}}
  {{--                           <div class="accordion-body contact-info"> --}}
  {{--                             <div class=""> --}}
  {{--                               <p><b --}}
  {{--                                   class="text-red">{{ $contact->relation }}</b> --}}
  {{--                               </p> --}}
  {{--                             </div> --}}
  {{--                             <div class="d-flex"> --}}
  {{--                               <i class="bx bxs-phone" --}}
  {{--                                 style="color: #1c355e;"></i> --}}
  {{--                               <p>{{ $contact->contact_number }}</p> --}}
  {{--                             </div> --}}
  {{--                             <div class="d-flex"> --}}
  {{--                               <i class="bx bx bxs-envelope" --}}
  {{--                                 style="color: #1c355e"></i> --}}
  {{--                               <p>{{ $contact->email }}</p> --}}
  {{--                             </div> --}}
  {{-- <div class="d-flex"> @if ($fm_acc && $fm_acc_id == $contact->id) <!--<span class="mt-2 contact-action-icons">--> <!--<a href="#" data-bs-toggle="collapse" data-bs-target="" wire:click.prevent="showEditContactModal({{ $contact->id }})"><i class="bx bxs-edit text-red"></i></a>--> <!--<a href="#" data-bs-toggle="collapse" data-bs-target="" wire:click.prevent="deleteContact({{ $contact->id }})"><i class="bx bxs-trash text-red"></i></a>--> <!--<a href="#" data-bs-toggle="collapse" data-bs-target=""><i class="bx bx bxs-lock text-red"></i></a>--> <!--</span>--> @endif </div> --}}
  {{--                           </div> --}}
  {{--                         </div> --}}
  {{--                       </div> --}}
  {{--                       @php --}}
  {{--                         $count++; --}}
  {{--                       @endphp --}}
  {{--                     @endforeach --}}
  {{--                   </div> --}}
  {{--                 </div> --}}
  {{--               </div> --}}
  {{--               <div class="mt-5"> --}}
  {{--                 <div class="d-flex"> --}}
  {{--                   <b class="text-red">Friends</b> --}}
  {{--                   <span class="ms-auto"> --}}
  {{--                     <!--<a href="#" wire:click.prevent="showAddContactModal('contact')"><i class="bx bx-plus contacts-icon"></i></a>--> --}}
  {{--                   </span> --}}
  {{--                 </div> --}}
  {{--                 <div class=""> --}}
  {{--                   <div class="accordion mt-3" id="accordionFriends"> --}}
  {{--                     @php --}}
  {{--                       $count = 1; --}}
  {{--                     @endphp --}}
  {{--                     @foreach (Auth::user()->contactsByType('friend')->get() as $contact) --}}
  {{--                       <div class="accordion-item"> --}}
  {{--                         <h2 class="accordion-header" --}}
  {{--                           id="heading{{ $contact->id }}"> --}}
  {{--                           <button --}}
  {{--                             class="accordion-button d-flex @if ($count > 0) collapsed @endif" --}}
  {{--                             type="button" data-bs-toggle="collapse" --}}
  {{--                             data-bs-target="#collapse{{ $contact->id }}" --}}
  {{--                             aria-expanded="@if ($count == 0) true @else false @endif" --}}
  {{--                             aria-controls="collapse{{ $contact->id }}" --}}
  {{--                             wire:click="showActionButtons({{ $contact->id }}, 'fam')" --}}
  {{--                             wire:ignore.self> --}}
  {{--                             <span> --}}
  {{--                               <i --}}
  {{--                                 class="bx bxs-user accordion-icon"></i>{{ $contact->name . ' ' . $contact->surname }} --}}
  {{--                             </span> --}}
  {{--                           </button> --}}
  {{--                         </h2> --}}
  {{--                         <div id="collapse{{ $contact->id }}" --}}
  {{--                           class="accordion-collapse @if ($count == 0) show @endif collapse" --}}
  {{--                           aria-labelledby="heading{{ $contact->id }}" --}}
  {{--                           data-bs-parent="#accordionFamilyFriends" --}}
  {{--                           style="" wire:ignore.self> --}}
  {{--                           <div class="accordion-body contact-info"> --}}
  {{--                             <div class=""> --}}
  {{--                               <p><b --}}
  {{--                                   class="text-red">{{ $contact->relation }}</b> --}}
  {{--                               </p> --}}
  {{--                             </div> --}}
  {{--                             <div class="d-flex"> --}}
  {{--                               <i class="bx bxs-phone" --}}
  {{--                                 style="color: #1c355e;"></i> --}}
  {{--                               <p>{{ $contact->contact_number }}</p> --}}
  {{--                             </div> --}}
  {{--                             <div class="d-flex"> --}}
  {{--                               <i class="bx bx bxs-envelope" --}}
  {{--                                 style="color: #1c355e"></i> --}}
  {{--                               <p>{{ $contact->email }}</p> --}}
  {{--                             </div> --}}
  {{--                           </div> --}}
  {{--                         </div> --}}
  {{--                       </div> --}}
  {{--                       @php --}}
  {{--                         $count++; --}}
  {{--                       @endphp --}}
  {{--                     @endforeach --}}
  {{--                   </div> --}}
  {{--                 </div> --}}
  {{--               </div> --}}
  {{--               <div class="mt-5"> --}}
  {{--                 <div class="d-flex"> --}}
  {{--                   <b class="text-red">Professionals</b> --}}
  {{--                   <span class="ms-auto"> --}}
  {{--                     <!--<a href="#" wire:click.prevent="showAddContactModal('banks_attorneys')"><i class="bx bx-plus contacts-icon"></i></a>--> --}}
  {{--                   </span> --}}
  {{--                 </div> --}}
  {{--                 <div class=""> --}}
  {{--                   <div class="accordion mt-3" id="accordionBanksAttorneys"> --}}
  {{--                     @php --}}
  {{--                       $count = 1; --}}
  {{--                     @endphp --}}
  {{--                     @foreach (Auth::user()->contactsByType('professionals')->get() as $contact) --}}
  {{--                       <div class="accordion-item"> --}}
  {{--                         <h2 class="accordion-header" --}}
  {{--                           id="heading{{ $contact->id }}"> --}}
  {{--                           <button --}}
  {{--                             class="accordion-button d-flex @if ($count > 0) collapsed @endif" --}}
  {{--                             type="button" data-bs-toggle="collapse" --}}
  {{--                             data-bs-target="#collapse{{ $contact->id }}" --}}
  {{--                             aria-expanded="@if ($count == 0) true @else false @endif" --}}
  {{--                             aria-controls="collapse{{ $contact->id }}" --}}
  {{--                             wire:click="showActionButtons({{ $contact->id }}, 'cmp')" --}}
  {{--                             wire:ignore.self> --}}
  {{--                             <span> --}}
  {{--                               <i --}}
  {{--                                 class="bx bxs-briefcase accordion-icon"></i>{{ $contact->name . ' ' . $contact->surname }} --}}
  {{--                             </span> --}}
  {{--                           </button> --}}
  {{--                         </h2> --}}
  {{--                         <div id="collapse{{ $contact->id }}" --}}
  {{--                           class="accordion-collapse @if ($count == 0) show @endif collapse" --}}
  {{--                           aria-labelledby="heading{{ $contact->id }}" --}}
  {{--                           data-bs-parent="#accordionBanksAttorneys" --}}
  {{--                           style="" wire:ignore.self> --}}
  {{--                           <div class="accordion-body contact-info"> --}}
  {{--                             <div class=""> --}}
  {{--                               <p><b class="text-red"> --}}
  {{--                                   @if ($contact->contact_type == 'doctor') --}}
  {{--                                     {{ $contact->contact_type }} --}}
  {{--                                   @else --}}
  {{--                                     {{ $contact->relation }} --}}
  {{--                                   @endif --}}
  {{--                                 </b></p> --}}
  {{--                             </div> --}}
  {{--                             <div class="d-flex"> --}}
  {{--                               <i class="bx bxs-phone"></i> --}}
  {{--                               <p>{{ $contact->contact_number }}</p> --}}
  {{--                             </div> --}}
  {{--                             <div class="d-flex"> --}}
  {{--                               <i class="bx bx bxs-envelope"></i> --}}
  {{--                               <p>{{ $contact->email }}</p> --}}
  {{--                             </div> --}}
  {{--                             {{-- --}}
  {{--                                                             <div class="d-flex"> --}}
  {{--                                                                 @if ($cmp_acc && $cmp_acc_id == $contact->id) --}}
  {{--                                                                     <span class="contact-action-icons"> --}}
  {{--                                                                         <!--<a href="#" data-bs-toggle="collapse" data-bs-target="" wire:click.prevent="showEditContactModal({{ $contact->id }})"><i class="bx bxs-edit text-red"></i></a>--> --}}
  {{--                                                                         <a href="#" data-bs-toggle="collapse" data-bs-target="" wire:click.prevent="deleteContact({{ $contact->id }})"><i class="bx bxs-trash text-red"></i></a> --}}
  {{--                                                                         <!--<a href="#" data-bs-toggle="collapse" data-bs-target=""><i class="bx bx bxs-lock text-red"></i></a>--> --}}
  {{--                                                                     </span> --}}
  {{--                                                                 @endif --}}
  {{--                                                             </div> --}}
  {{--                           </div> --}}
  {{--                         </div> --}}
  {{--                       </div> --}}
  {{--                       @php --}}
  {{--                         $count++; --}}
  {{--                       @endphp --}}
  {{--                     @endforeach --}}
  {{--                   </div> --}}
  {{--                 </div> --}}
  {{--               </div> --}}
  {{--               <div class="mt-5"> --}}
  {{--                 <div class="d-flex"> --}}
  {{--                   <b class="text-red">Other</b> --}}
  {{--                   <span class="ms-auto"> --}}
  {{--                     <!--<a href="#" wire:click.prevent="showAddContactModal('contact')"><i class="bx bx-plus contacts-icon"></i></a>--> --}}
  {{--                   </span> --}}
  {{--                 </div> --}}
  {{--                 <div class=""> --}}
  {{--                   <div class="accordion mt-3" id="accordionOther"> --}}
  {{--                     @php --}}
  {{--                       $count = 1; --}}
  {{--                     @endphp --}}
  {{--                     @foreach (Auth::user()->contactsByType('other')->get() as $contact) --}}
  {{--                       <div class="accordion-item"> --}}
  {{--                         <h2 class="accordion-header" --}}
  {{--                           id="heading{{ $contact->id }}"> --}}
  {{--                           <button --}}
  {{--                             class="accordion-button d-flex @if ($count > 0) collapsed @endif" --}}
  {{--                             type="button" data-bs-toggle="collapse" --}}
  {{--                             data-bs-target="#collapse{{ $contact->id }}" --}}
  {{--                             aria-expanded="@if ($count == 0) true @else false @endif" --}}
  {{--                             aria-controls="collapse{{ $contact->id }}" --}}
  {{--                             wire:click="showActionButtons({{ $contact->id }}, 'fam')" --}}
  {{--                             wire:ignore.self> --}}
  {{--                             <span> --}}
  {{--                               <i --}}
  {{--                                 class="bx bxs-user accordion-icon"></i>{{ $contact->name . ' ' . $contact->surname }} --}}
  {{--                             </span> --}}
  {{--                           </button> --}}
  {{--                         </h2> --}}
  {{--                         <div id="collapse{{ $contact->id }}" --}}
  {{--                           class="accordion-collapse @if ($count == 0) show @endif collapse" --}}
  {{--                           aria-labelledby="heading{{ $contact->id }}" --}}
  {{--                           data-bs-parent="#accordionFamilyFriends" --}}
  {{--                           style="" wire:ignore.self> --}}
  {{--                           <div class="accordion-body contact-info"> --}}
  {{--                             <div class=""> --}}
  {{--                               <p><b --}}
  {{--                                   class="text-red">{{ $contact->relation }}</b> --}}
  {{--                               </p> --}}
  {{--                             </div> --}}
  {{--                             <div class="d-flex"> --}}
  {{--                               <i class="bx bxs-phone" --}}
  {{--                                 style="color: #1c355e;"></i> --}}
  {{--                               <p>{{ $contact->contact_number }}</p> --}}
  {{--                             </div> --}}
  {{--                             <div class="d-flex"> --}}
  {{--                               <i class="bx bx bxs-envelope" --}}
  {{--                                 style="color: #1c355e"></i> --}}
  {{--                               <p>{{ $contact->email }}</p> --}}
  {{--                             </div> --}}
  {{--                           </div> --}}
  {{--                         </div> --}}
  {{--                       </div> --}}
  {{--                       @php --}}
  {{--                         $count++; --}}
  {{--                       @endphp --}}
  {{--                     @endforeach --}}
  {{--                   </div> --}}
  {{--                 </div> --}}
  {{--               </div> --}}
  {{--             </div> --}}
  {{--           </div> --}}
  {{--         </div> --}}
  {{--       </div> --}}
  {{--       <div class="simplebar-placeholder" --}}
  {{--         style="width: auto; height: 1125px;"></div> --}}
  {{--     </div> --}}
  {{--     <div class="simplebar-track simplebar-horizontal" --}}
  {{--       style="visibility: hidden;"> --}}
  {{--       <div class="simplebar-scrollbar" --}}
  {{--         style="transform: translate3d(0px, 0px, 0px); display: none;"> --}}
  {{--       </div> --}}
  {{--     </div> --}}
  {{--     <div class="simplebar-track simplebar-vertical" --}}
  {{--       style="visibility: visible;"> --}}
  {{--       <div class="simplebar-scrollbar" --}}
  {{--         style="height: 222px; transform: translate3d(0px, 0px, 0px); display: block;"> --}}
  {{--       </div> --}}
  {{--     </div> --}}
  {{--   </div> --}}
  {{-- </div> --}}

  {{-- @endif --}}

</div>
