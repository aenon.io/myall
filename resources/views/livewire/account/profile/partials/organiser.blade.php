<section class="organiser-section">
  <h3>Organiser</h3>
  @if (Auth::user()->free_account == 1)
    <div class="__empty-state">
      <div>
        <a href="{{ url('register/upgrade/' . Auth::user()->id) }}">
          <svg width="35" height="34" viewBox="0 0 35 34" fill="none"
            xmlns="http://www.w3.org/2000/svg">
            <path d="M27.1667 11.0833H25.5833V7.91667C25.5833 3.54667 22.0367 0
            17.6667 0C13.2967 0 9.75 3.54667 9.75 7.91667V11.0833H8.16667C6.425
            11.0833 5 12.5083 5 14.25V30.0833C5 31.825 6.425 33.25 8.16667
            33.25H27.1667C28.9083 33.25 30.3333 31.825 30.3333
            30.0833V14.25C30.3333 12.5083 28.9083 11.0833 27.1667
            11.0833ZM12.9167 7.91667C12.9167 5.28833 15.0383 3.16667 17.6667
            3.16667C20.295 3.16667 22.4167 5.28833 22.4167
            7.91667V11.0833H12.9167V7.91667ZM27.1667
            30.0833H8.16667V14.25H27.1667V30.0833Z" fill="#CF3D60" />
          </svg>
        </a>
        <div>
          <a href="{{ url('register/upgrade/' . Auth::user()->id) }}">
            Upgrade to access  this feature
          </a>
        </div>
      </div>
    </div>
  @else
    <div>
      <div class="__organiser-progress __status-indicator">
        <div class="__heading">
          <svg width="20" height="20" viewBox="0 0 20 20" fill="none"
            xmlns="http://www.w3.org/2000/svg">
            <path d="M5.83333 13.3327H11.6667V14.9993H5.83333V13.3327ZM5.83333
          9.99935H14.1667V11.666H5.83333V9.99935ZM5.83333
          6.66602H14.1667V8.33268H5.83333V6.66602ZM15.8333 3.33268H12.35C12
          2.36602 11.0833 1.66602 10 1.66602C8.91667 1.66602 8 2.36602 7.65
          3.33268H4.16667C4.05 3.33268 3.94167 3.34102 3.83333 3.36602C3.50833
          3.43268 3.21667 3.59935 2.99167 3.82435C2.84167 3.97435 2.71667
          4.15768 2.63333 4.35768C2.55 4.54935 2.5 4.76602 2.5
          4.99935V16.666C2.5 16.891 2.55 17.116 2.63333 17.316C2.71667 17.516
          2.84167 17.691 2.99167 17.8493C3.21667 18.0744 3.50833 18.241 3.83333
          18.3077C3.94167 18.3243 4.05 18.3327 4.16667 18.3327H15.8333C16.75
          18.3327 17.5 17.5827 17.5 16.666V4.99935C17.5 4.08268 16.75 3.33268
          15.8333 3.33268ZM10 3.12435C10.3417 3.12435 10.625 3.40768 10.625
          3.74935C10.625 4.09102 10.3417 4.37435 10 4.37435C9.65833 4.37435
          9.375 4.09102 9.375 3.74935C9.375 3.40768 9.65833 3.12435 10
          3.12435ZM15.8333 16.666H4.16667V4.99935H15.8333V16.666Z"
              fill="#040A5E" />
          </svg>
          <h4>Organiser Status</h4>
        </div>
        <hr>
        <div class="__progress-container"
          data-progress="{{ $organiser_percentage }}">
          <svg class="__progress-circle" viewBox="0 0 36 36">
            <defs>
              <linearGradient id="gradient" x1="80%" y1="25%"
                x2="0%" y2="100%">
                <stop offset="0%" stop-color="#040a5e" />
                <stop offset="33%" stop-color="#822384" />
                <stop offset="49%" stop-color="#9c2c78" />
                <stop offset="82%" stop-color="#cf3d60" />
                <stop offset="98%" stop-color="#f1a2b1" />
                <stop offset="100%" stop-color="#f5adbaff" />
              </linearGradient>
            </defs>
            <path class="__circle-bg" d="M18 2.0845 a 15.9155 15.9155 0
                      0 1 0 31.831 a 15.9155 15.9155 0 0 1 0 -31.831" />
            <path class="__circle" stroke-dasharray="100, 100" d="M18 2.0845 a
                      15.9155 15.9155 0 0 1 0 31.831 a 15.9155 15.9155 0 0 1 0
                      -31.831" />
          </svg>
          <div class="__progress-text">
            <span class="__progress-number">0%</span>
            <span class="__progress-label">Completed</span>
          </div>
        </div>
        <div class="__options">
          <a class="__organiser-link" href="{{ url('organiser/personal') }}">
            <svg width="20" height="21" viewBox="0 0 20 21"
              fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M10 5.41732C10.9667 5.41732 11.75 6.20065 11.75 7.16732C11.75
            8.13398 10.9667 8.91732 10 8.91732C9.03337 8.91732 8.25004 8.13398
            8.25004 7.16732C8.25004 6.20065 9.03337 5.41732 10 5.41732ZM10
            12.9173C12.475 12.9173 15.0834 14.134 15.0834
            14.6673V15.584H4.91671V14.6673C4.91671 14.134 7.52504 12.9173 10
            12.9173ZM10 3.83398C8.15837 3.83398 6.66671 5.32565 6.66671
            7.16732C6.66671 9.00898 8.15837 10.5007 10 10.5007C11.8417 10.5007
            13.3334 9.00898 13.3334 7.16732C13.3334 5.32565 11.8417 3.83398 10
            3.83398ZM10 11.334C7.77504 11.334 3.33337 12.4507 3.33337
            14.6673V17.1673H16.6667V14.6673C16.6667 12.4507 12.225 11.334 10
            11.334Z" />
            </svg>
            Personal
          </a>
          <a class="__organiser-link" href="{{ url('organiser/connections') }}">
            <svg width="20" height="21" viewBox="0 0 20 21"
              fill="none" xmlns="http://www.w3.org/2000/svg">
              <g clip-path="url(#clip0_447_242)">
                <path d="M8.41358 13.7409L9.59692 12.5576C7.32192 10.5326 5.83025
              9.16588 5.83025 7.86589C5.83025 6.99089 6.49692 6.32422 7.37192
              6.32422C8.29692 6.32422 8.65525 6.86588 9.60525
              7.99088H10.3803C11.3136 6.89922 11.6553 6.32422 12.6136
              6.32422C13.3386 6.32422 13.9053 6.77422 14.0886 7.42422C14.3803
              7.39088 14.6553 7.37422 14.9219 7.37422C15.2219 7.37422 15.5053
              7.39922 15.7803 7.44089C15.5803 5.84922 14.2719 4.65755 12.6219
              4.65755C12.5219 4.65755 12.4303 4.68255 12.3303 4.69089C12.4303
              4.41588 12.4969 4.13255 12.4969 3.82422C12.4969 2.44089 11.3803
              1.32422 9.99692 1.32422C8.61358 1.32422 7.49692 2.44089 7.49692
              3.82422C7.49692 4.13255 7.56358 4.41588 7.66358 4.69089C7.56358
              4.68255 7.47192 4.65755 7.37192 4.65755C5.57192 4.65755 4.16358
              6.06589 4.16358 7.86589C4.16358 9.88255 5.86359 11.4576 8.41358
              13.7409Z" />
                <path d="M18.7469 14.0242C18.4803 13.8742 18.1969 13.7826 17.9136
              13.7326C17.9719 13.6492 18.0386 13.5826 18.0886 13.4992C18.9886
              11.9409 18.4719 10.0159 16.9136 9.11588C15.1719 8.10755 12.9469
              8.79088 9.70525 9.86588L10.1386 11.4826C13.0303 10.5326 14.9636
              9.91588 16.0886 10.5659C16.8469 11.0076 17.0886 11.9076 16.6553
              12.6742C16.1886 13.4742 15.5469 13.5076 14.0969 13.7742L13.7053
              14.4492C14.1886 15.7992 14.5136 16.3909 14.0303 17.2159C13.5886
              17.9742 12.6886 18.2159 11.9219 17.7826C11.8719 17.7576 11.8303
              17.7076 11.7803 17.6742C11.5303 18.2326 11.2469 18.7076 10.9219
              19.1159C10.9803 19.1492 11.0303 19.1909 11.0886 19.2325C12.6469
              20.1325 14.5719 19.6159 15.4719 18.0576C15.5219 17.9742 15.5469
              17.8826 15.5886 17.7909C15.7719 18.0159 15.9886 18.2159 16.2553
              18.3659C17.4469 19.0576 18.9803 18.6492 19.6719 17.4492C20.3636
              16.2492 19.9386 14.7076 18.7469 14.0242Z" />
                <path d="M10.2636 12.1659C9.64692 15.1492 9.20525 17.1242 8.08025
              17.7742C7.32192 18.2159 6.41358 17.9742 5.97192 17.2076C5.50525
              16.4075 5.80525 15.8242 6.29692 14.4409L5.91358 13.7659C4.49692
              13.5076 3.83025 13.4909 3.35525 12.6659C2.91358 11.9076 3.15525
              10.9992 3.92192 10.5576C3.99692 10.5159 4.08025 10.4909 4.16358
              10.4659C3.87192 9.99922 3.63025 9.49088 3.48025 8.92422C3.34692
              8.98255 3.21358 9.04088 3.08025 9.11588C1.52192 10.0159 1.00525
              11.9409 1.90525 13.4992C1.95525 13.5826 2.02192 13.6492 2.08025
              13.7326C1.79692 13.7826 1.51358 13.8742 1.24692 14.0242C0.0552513
              14.7159 -0.361415 16.2409 0.330251 17.4409C1.02192 18.6409 2.54692
              19.0492 3.74692 18.3576C4.01358 18.2076 4.23025 18.0076 4.41358
              17.7826C4.45525 17.8742 4.48025 17.9659 4.53025 18.0492C5.43025
              19.6075 7.35525 20.1242 8.91358 19.2242C10.6553 18.2159 11.1719
              15.9492 11.8719 12.6076L10.2636 12.1659Z" />
              </g>
              <defs>
                <clipPath id="clip0_447_242">
                  <rect width="20" height="20" fill="white"
                    transform="translate(0 0.5)" />
                </clipPath>
              </defs>
            </svg>
            Emergency Contacts
          </a>
          <a class="__organiser-link"
            href="{{ url('organiser/business-wealth') }}">
            <svg width="20" height="21" viewBox="0 0 20 21"
              fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M11.6666 5.91732V4.25065H8.33329V5.91732H11.6666ZM3.33329
            7.58398V16.7507H16.6666V7.58398H3.33329ZM16.6666 5.91732C17.5916
            5.91732 18.3333 6.65898 18.3333 7.58398V16.7507C18.3333 17.6757
            17.5916 18.4173 16.6666 18.4173H3.33329C2.40829 18.4173 1.66663
            17.6757 1.66663 16.7507L1.67496 7.58398C1.67496 6.65898 2.40829
            5.91732 3.33329 5.91732H6.66663V4.25065C6.66663 3.32565 7.40829
            2.58398 8.33329 2.58398H11.6666C12.5916 2.58398 13.3333 3.32565
            13.3333 4.25065V5.91732H16.6666Z" />
            </svg>
            Business Wealth
          </a>
          <a class="__organiser-link"
            href="{{ url('organiser/properties-wealth') }}">
            <svg width="20" height="21" viewBox="0 0 20 21"
              fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M17.9167 5.91667V12.1667H16.25V6.75L12.0833 3.83333L7.91667
              6.75V8H6.25V5.91667L12.0833 1.75L17.9167 5.91667ZM13.3333
              6.33333H12.5V7.16667H13.3333V6.33333ZM11.6667
              6.33333H10.8333V7.16667H11.6667V6.33333ZM13.3333
              8H12.5V8.83333H13.3333V8ZM11.6667
              8H10.8333V8.83333H11.6667V8ZM16.25 13.8333H14.5833C14.5833 12.8333
              13.9583 11.9333 13.025 11.5833L7.89167
              9.66667H1.25V18.8333H6.25V17.6333L12.0833 19.25L18.75
              17.1667V16.3333C18.75 14.95 17.6333 13.8333 16.25 13.8333ZM2.91667
              17.1667V11.3333H4.58333V17.1667H2.91667ZM12.0583 17.5083L6.25
              15.9V11.3333H7.59167L12.4417 13.1417C12.725 13.25 12.9167 13.525
              12.9167 13.8333C12.9167 13.8333 11.2583 13.7917 11 13.7083L9.01667
              13.05L8.49167 14.6333L10.475 15.2917C10.9 15.4333 11.3417 15.5083
              11.7917 15.5083H16.25C16.575 15.5083 16.8667 15.7 17
              15.975L12.0583 17.5083Z" />
            </svg>
            Personal Wealth
          </a>
          <a class="__organiser-link"
            href="{{ url('organiser/debt-expenses') }}">
            <svg width="20" height="21" viewBox="0 0 20 21"
              fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M17.0834 6.56667V4.66667C17.0834 3.75 16.3334 3 15.4167
            3H3.75004C2.82504 3 2.08337 3.75 2.08337 4.66667V16.3333C2.08337
            17.25 2.82504 18 3.75004 18H15.4167C16.3334 18 17.0834 17.25 17.0834
            16.3333V14.4333C17.575 14.1417 17.9167 13.6167 17.9167 13V8C17.9167
            7.38333 17.575 6.85833 17.0834 6.56667ZM16.25
            8V13H10.4167V8H16.25ZM3.75004
            16.3333V4.66667H15.4167V6.33333H10.4167C9.50004 6.33333 8.75004
            7.08333 8.75004 8V13C8.75004 13.9167 9.50004 14.6667 10.4167
            14.6667H15.4167V16.3333H3.75004Z" />
              <path d="M12.9167 11.75C13.6071 11.75 14.1667 11.1904 14.1667 10.5C14.1667
            9.80964 13.6071 9.25 12.9167 9.25C12.2264 9.25 11.6667 9.80964
            11.6667 10.5C11.6667 11.1904 12.2264 11.75 12.9167 11.75Z" />
            </svg>
            Debt & Expenses
          </a>
          <a class="__organiser-link"
            href="{{ url('organiser/social-media') }}">
            <svg width="20" height="21" viewBox="0 0 20 21"
              fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M7.50004 18.8327H15C15.6917 18.8327 16.2834 18.416 16.5334
            17.816L19.05 11.941C19.125 11.7493 19.1667 11.5493 19.1667
            11.3327V9.66602C19.1667 8.74935 18.4167 7.99935 17.5
            7.99935H12.2417L13.0334 4.19102L13.0584 3.92435C13.0584 3.58268
            12.9167 3.26602 12.6917 3.04102L11.8084 2.16602L6.31671
            7.65768C6.01671 7.95768 5.83337 8.37435 5.83337
            8.83268V17.166C5.83337 18.0827 6.58337 18.8327 7.50004
            18.8327ZM7.50004 8.83268L11.1167 5.21602L10 9.66602H17.5V11.3327L15
            17.166H7.50004V8.83268ZM0.833374
            8.83268H4.16671V18.8327H0.833374V8.83268Z" />
            </svg>
            Social Media
          </a>
          <a class="__organiser-link" href="{{ url('organiser/safekeeping') }}">
            <svg width="20" height="21" viewBox="0 0 20 21"
              fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M10 2.16602L3.33337 4.66602V9.74102C3.33337 13.9493 6.17504
            17.8743 10 18.8327C13.825 17.8743 16.6667 13.9493 16.6667
            9.74102V4.66602L10 2.16602ZM15 9.74102C15 13.0743 12.875 16.1577 10
            17.0993C7.12504 16.1577 5.00004 13.0827 5.00004 9.74102V5.75768L10
            3.99102L15 5.75768V9.74102ZM7.35004 9.32435L6.16671 10.4993L9.11671
            13.4493L13.8334 8.73268L12.6584 7.55768L9.12504 11.091L7.35004
            9.32435Z" />
            </svg>
            Safekeeping
          </a>
          <a class="__organiser-link" href="{{ url('organiser/memories') }}">
            <svg width="20" height="21" viewBox="0 0 20 21"
              fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M13.75 2.85352C12.3 2.85352 10.9083 3.52852 9.99996
            4.59518C9.09163 3.52852 7.69996 2.85352 6.24996 2.85352C3.68329
            2.85352 1.66663 4.87018 1.66663 7.43685C1.66663 10.5868 4.49996
            13.1535 8.79163 17.0535L9.99996 18.1452L11.2083 17.0452C15.5 13.1535
            18.3333 10.5868 18.3333 7.43685C18.3333 4.87018 16.3166 2.85352
            13.75 2.85352ZM10.0833 15.8118L9.99996 15.8952L9.91663
            15.8118C5.94996 12.2202 3.33329 9.84518 3.33329 7.43685C3.33329
            5.77018 4.58329 4.52018 6.24996 4.52018C7.53329 4.52018 8.78329
            5.34518 9.22496 6.48685H10.7833C11.2166 5.34518 12.4666 4.52018
            13.75 4.52018C15.4166 4.52018 16.6666 5.77018 16.6666
            7.43685C16.6666 9.84518 14.05 12.2202 10.0833 15.8118Z" />
            </svg>
            Memories
          </a>
          <a class="__organiser-link" href="{{ url('organiser/funeral') }}">
            <svg width="20" height="21" viewBox="0 0 20 21"
              fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M3.33333 14.2507C4.25381 14.2507 5 13.5045 5 12.584C5 11.6635
            4.25381 10.9173 3.33333 10.9173C2.41286 10.9173 1.66667 11.6635
            1.66667 12.584C1.66667 13.5045 2.41286 14.2507 3.33333 14.2507Z" />
              <path d="M1.01667 15.5673C0.4 15.834 0 16.434 0
            17.109V18.4173H3.75V17.0757C3.75 16.384 3.94167 15.734 4.275
            15.1673C3.96667 15.1173 3.65833 15.084 3.33333 15.084C2.50833 15.084
            1.725 15.259 1.01667 15.5673Z" />
              <path d="M16.6667 14.2507C17.5871 14.2507 18.3333 13.5045 18.3333
            12.584C18.3333 11.6635 17.5871 10.9173 16.6667 10.9173C15.7462
            10.9173 15 11.6635 15 12.584C15 13.5045 15.7462 14.2507 16.6667
            14.2507Z" />
              <path d="M18.9833 15.5673C18.275 15.259 17.4917 15.084 16.6667
            15.084C16.3417 15.084 16.0333 15.1173 15.725 15.1673C16.0583 15.734
            16.25 16.384 16.25 17.0757V18.4173H20V17.109C20 16.434 19.6 15.834
            18.9833 15.5673Z" />
              <path d="M13.5333 14.7923C12.5583 14.359 11.3583 14.0423 10
            14.0423C8.64167 14.0423 7.44167 14.3673 6.46667 14.7923C5.56667
            15.1923 5 16.0923 5 17.0757V18.4173H15V17.0757C15 16.0923 14.4333
            15.1923 13.5333 14.7923ZM6.725 16.7507C6.8 16.559 6.95 16.4007
            7.13333 16.3173C8.05 15.909 9.01667 15.709 9.99167 15.709C10.975
            15.709 11.9333 15.9173 12.85 16.3173C13.0417 16.4007 13.1833 16.559
            13.2583 16.7507H6.725Z" />
              <path d="M7.5 10.9173C7.5 12.3007 8.61667 13.4173 10 13.4173C11.3833
            13.4173 12.5 12.3007 12.5 10.9173C12.5 9.53398 11.3833 8.41732 10
            8.41732C8.61667 8.41732 7.5 9.53398 7.5 10.9173ZM10.8333
            10.9173C10.8333 11.3757 10.4583 11.7507 10 11.7507C9.54167 11.7507
            9.16667 11.3757 9.16667 10.9173C9.16667 10.459 9.54167 10.084 10
            10.084C10.4583 10.084 10.8333 10.459 10.8333 10.9173Z" />
              <path d="M2.06667 9.96732C1.80833 9.33398 1.66667 8.71732 1.66667
            8.08398C1.66667 5.93398 3.35 4.25065 5.5 4.25065C7.73333 4.25065
            8.68333 5.70065 10 7.24232C11.3083 5.71732 12.25 4.25065 14.5
            4.25065C16.65 4.25065 18.3333 5.93398 18.3333 8.08398C18.3333
            8.71732 18.1917 9.33398 17.9333 9.96732C18.475 10.2257 18.9167
            10.6507 19.2083 11.1673C19.7083 10.1673 20 9.15065 20 8.08398C20
            5.00065 17.5833 2.58398 14.5 2.58398C12.7583 2.58398 11.0917 3.39232
            10 4.67565C8.90833 3.39232 7.24167 2.58398 5.5 2.58398C2.41667
            2.58398 0 5.00065 0 8.08398C0 9.15065 0.291667 10.1673 0.8
            11.1673C1.09167 10.6507 1.53333 10.2257 2.06667 9.96732Z" />
            </svg>
            Funeral
          </a>
        </div>
      </div>
      <div class="__will-progress __status-indicator">
        <div class="__heading">
          <svg width="20" height="20" viewBox="0 0 20 20"
            fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M16.25 2.91602L15 1.66602L13.75 2.91602L12.5 1.66602L11.25
          2.91602L10 1.66602L8.75 2.91602L7.5 1.66602L6.25 2.91602L5
          1.66602V13.3327H2.5V15.8327C2.5 17.216 3.61667 18.3327 5
          18.3327H15C16.3833 18.3327 17.5 17.216 17.5 15.8327V1.66602L16.25
          2.91602ZM12.5 16.666H5C4.54167 16.666 4.16667 16.291 4.16667
          15.8327V14.9993H12.5V16.666ZM15.8333 15.8327C15.8333 16.291 15.4583
          16.666 15 16.666C14.5417 16.666 14.1667 16.291 14.1667
          15.8327V13.3327H6.66667V4.16602H15.8333V15.8327Z" fill="#040A5E" />
            <path d="M12.5 5.83268H7.5V7.49935H12.5V5.83268Z" fill="#040A5E" />
            <path d="M15 5.83268H13.3333V7.49935H15V5.83268Z" fill="#040A5E" />
            <path d="M12.5 8.33268H7.5V9.99935H12.5V8.33268Z" fill="#040A5E" />
            <path d="M15 8.33268H13.3333V9.99935H15V8.33268Z" fill="#040A5E" />
          </svg>
          <h4>Online Will Status</h4>
        </div>
        <hr>
        <div class="__progress-container"
          data-progress="{{ $cur_progress }}">
          <svg class="__progress-circle" viewBox="0 0 36 36">
            <defs>
              <linearGradient id="gradient" x1="80%" y1="25%"
                x2="0%" y2="100%">
                <stop offset="0%" stop-color="#040a5e" />
                <stop offset="33%" stop-color="#822384" />
                <stop offset="49%" stop-color="#9c2c78" />
                <stop offset="82%" stop-color="#cf3d60" />
                <stop offset="98%" stop-color="#f1a2b1" />
                <stop offset="100%" stop-color="#f5adbaff" />
              </linearGradient>
            </defs>
            <path class="__circle-bg" d="M18 2.0845 a 15.9155 15.9155 0
                      0 1 0 31.831 a 15.9155 15.9155 0 0 1 0 -31.831" />
            <path class="__circle" stroke-dasharray="100, 100" d="M18 2.0845 a
                      15.9155 15.9155 0 0 1 0 31.831 a 15.9155 15.9155 0 0 1 0
                      -31.831" />
          </svg>
          <div class="__progress-text">
            <span class="__progress-number">0%</span>
            <span class="__progress-label">Completed</span>
          </div>
        </div>
        <div class="__options">
          <a href="{{ url('organiser/online-will/profile') }}"
            class="">
            <svg width="17" height="16" viewBox="0 0 17 16"
              fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M9.87236 6.01333L10.4857 6.62667L4.44569
            12.6667H3.83236V12.0533L9.87236 6.01333ZM12.2724 2C12.1057 2 11.9324
            2.06667 11.8057 2.19333L10.5857 3.41333L13.0857 5.91333L14.3057
            4.69333C14.5657 4.43333 14.5657 4.01333 14.3057 3.75333L12.7457
            2.19333C12.6124 2.06 12.4457 2 12.2724 2ZM9.87236 4.12667L2.49902
            11.5V14H4.99902L12.3724 6.62667L9.87236 4.12667Z"
                fill="#CF3D60" />
            </svg>
            Edit Will
          </a>
        </div>
      </div>
    </div>
  @endif

  {{-- class="@if (Auth::user()->free_account == 0) col-md-12 col-lg-8 @else col-md-12 @endif"> --}}
  {{-- <livewire:account.partials.header /> --}}
  {{-- <div class="col-md-12"> --}}
  {{--   <h6>Profile Data</h6> --}}
  {{-- </div> --}}

  {{-- @if (Auth::user()->free_account == 1) --}}
  {{--   <div class="row mb-5"> --}}
  {{--     <div class="col-12 col-lg-8"> --}}
  {{-- @endif --}}

  {{-- PROFILE SECTION --}}
  {{--     <div --}}
  {{--       class="card-box-style @if (Auth::user()->free_account == 1) h-100 @endif rounded p-5"> --}}
  {{--       <div class="row"> --}}
  {{--         <div --}}
  {{--           class="col-12 col-md-6 col-xxl-6 text-md-start text-lg-start text-center"> --}}
  {{--           <pie class="ten"> --}}
  {{--             @if (Auth::user()->profile_pic) --}}
  {{--               <img class="rounded-50-lg" --}}
  {{--                 src="{{ asset('storage/' . Auth::user()->profile_pic) }}" --}}
  {{--                 alt="Images"> --}}
  {{--             @else --}}
  {{--               <img class="rounded-50-lg" --}}
  {{--                 src="{{ asset('img/placeholder.png') }}" alt="Images"> --}}
  {{--             @endif --}}
  {{--             <!--<a href="http://52.73.227.231/profile/edit"><i class="bx bxs-edit header_icons bg-red"></i></a>--> --}}
  {{--           </pie> --}}
  {{--         </div> --}}
  {{--         <div class="col-12 col-md-6 col-xxl-6"> --}}
  {{--           <h4 class="text-navy-blue"> --}}
  {{--             {{ Auth::user()->name . ' ' . Auth::user()->surname }}</h4> --}}
  {{--           <div class="mb-3 mt-2"> --}}
  {{--             <b class="text-red">{{ Auth::user()->occupation }}</b> --}}
  {{--             <p>{{ mb_substr(Auth::user()->bio, 0, 300) }}</p> --}}
  {{--           </div> --}}
  {{--           @if (Auth::user()->free_account == 0) --}}
  {{--             <a href="{{ url('organiser') }}" --}}
  {{--               class="btn btn-lg btn-gradient gradient-1">LET’S START --}}
  {{--               ORGANISING</a> --}}
  {{--           @endif --}}
  {{--           <div class="mt-3"> --}}
  {{--             <a href="{{ url('profile/edit') }}" --}}
  {{--               class="btn btn-outline-one text-red">EDIT PROFILE</a> --}}
  {{--           </div> --}}
  {{--         </div> --}}
  {{--       </div> --}}
  {{--     </div> --}}
  {{--     @if (Auth::user()->free_account == 1) --}}
  {{--   </div> --}}
  {{--   <div class="col-12 col-lg-4 mt-lg-0 mt-5"> --}}
  {{--     <div class="card-box-style h-100 bg-fm-upgrade rounded"> --}}
  {{--       <div class="row h-100"> --}}
  {{--         <div class="col-md-12 h-100"> --}}
  {{--           <div --}}
  {{--             class="h-100 d-flex flex-column aligns-items-center justify-content-center"> --}}
  {{--             <div class="mt-3 text-center"> --}}
  {{--               <h5 class="text-teal">GET FULL ACCESS</h5> --}}
  {{--             </div> --}}
  {{--             <div class="text-center"> --}}
  {{--               <b class="text-white">Upgrade to get full access on everything --}}
  {{--                 WrapUp has to offer!</b> --}}
  {{--             </div> --}}
  {{--             <div class="mt-3 text-center"> --}}
  {{--               <a href="{{ url('register') }}" --}}
  {{--                 class="btn btn_turquoise">Upgrade Now</a> --}}
  {{--             </div> --}}
  {{--           </div> --}}
  {{--         </div> --}}
  {{--       </div> --}}
  {{--     </div> --}}
  {{--   </div> --}}
  {{-- </div> --}}
  {{-- @endif --}}
  {{-- FREE ACCESS END --}}

  {{-- @if (Auth::user()->free_account == 0) --}}
  {{--   <div class="row mb-5"> --}}
  {{--     <div class="col-md-12"> --}}
  {{--       <h6>WrapUp Online Will</h6> --}}
  {{--     </div> --}}
  {{--     <div class="col-md-6"> --}}
  {{--       <div class="card-box-style h-100 rounded"> --}}
  {{--         <div class="row"> --}}
  {{--           <div class="col-md-12 text-center"> --}}
  {{--             <h6 class="mobile-text-left">{{ $cur_progress_text }}</h6> --}}
  {{-- --}}
  {{-- <div class="progress mt-3"> --}}
  {{--   <div class="progress-bar gradient-1" role="progressbar" --}}
  {{--     aria-label="Example with label" --}}
  {{--     style="width: {{ $cur_progress }}%;" --}}
  {{--     aria-valuenow="{{ $cur_progress }}" aria-valuemin="0" --}}
  {{--     aria-valuemax="100"></div> --}}
  {{-- </div> --}}
  {{-- --}}
  {{-- --}}
  {{-- @php --}}
  {{--   dump($cur_progress); --}}
  {{-- @endphp --}}
  {{--             <div class="progress-container" --}}
  {{--               data-progress="{{ $cur_progress }}"> --}}
  {{--               <svg class="progress-circle" viewBox="0 0 36 36"> --}}
  {{--                 <defs> --}}
  {{--                   <linearGradient id="gradient" x1="80%" y1="25%" --}}
  {{--                     x2="0%" y2="100%"> --}}
  {{--                     <stop offset="0%" stop-color="#040a5e" /> --}}
  {{--                     <stop offset="33%" stop-color="#822384" /> --}}
  {{--                     <stop offset="49%" stop-color="#9c2c78" /> --}}
  {{--                     <stop offset="82%" stop-color="#cf3d60" /> --}}
  {{--                     <stop offset="98%" stop-color="#f1a2b1" /> --}}
  {{--                     <stop offset="100%" stop-color="#f5adbaff" /> --}}
  {{--                   </linearGradient> --}}
  {{--                 </defs> --}}
  {{--                 <path class="circle-bg" d="M18 2.0845 a 15.9155 15.9155 0 --}}
  {{--                     0 1 0 31.831 a 15.9155 15.9155 0 0 1 0 -31.831" /> --}}
  {{--                 <path class="circle" stroke-dasharray="100, 100" d="M18 2.0845 a --}}
  {{--                     15.9155 15.9155 0 0 1 0 31.831 a 15.9155 15.9155 0 0 1 0 --}}
  {{--                     -31.831" /> --}}
  {{--               </svg> --}}
  {{--               <div class="progress-text"> --}}
  {{--                 <span class="progress-number">0%</span> --}}
  {{--                 <span class="progress-label">Completed</span> --}}
  {{--               </div> --}}
  {{--             </div> --}}
  {{-- --}}
  {{--             <div class="mobile-text-left mt-3"> --}}
  {{--               <span>WrapUp Online Will Progress:</span><br /> --}}
  {{--               <b>{{ $cur_progress }}% complete</b> --}}
  {{--             </div> --}}
  {{--             @if ($cur_progress < 100) --}}
  {{--               <div class="mobile-text-left mt-3"> --}}
  {{--                 <a href="{{ url('organiser/online-will/profile') }}" --}}
  {{--                   class="btn btn-red">CONTINUE</a> --}}
  {{--               </div> --}}
  {{--             @endif --}}
  {{--             @if ($cur_progress == 100) --}}
  {{--               <div class="mt-3"> --}}
  {{--                 <a href="#" class="btn btn-red" --}}
  {{--                   wire:click.prevent="downloadPdf">Download Your --}}
  {{--                   Online Will</a> --}}
  {{--               </div> --}}
  {{--             @endif --}}
  {{--           </div> --}}
  {{--         </div> --}}
  {{--       </div> --}}
  {{--     </div> --}}
  {{--     <div class="col-md-6 mt-md-0 mt-5"> --}}
  {{--       <div class="card-box-style h-100 bg-fm-tree rounded"> --}}
  {{--         <div class="row"> --}}
  {{--           <div class="col-md-12 text-center"> --}}
  {{--             <div class="mt-3"> --}}
  {{--               <i class="bx bx-sitemap fm-icon"></i> --}}
  {{--             </div> --}}
  {{--             <div class="mt-3"> --}}
  {{--               <b class="text-white">Create and organise<br /> your --}}
  {{--                 Network</b> --}}
  {{--             </div> --}}
  {{--             <div class="mt-3"> --}}
  {{--               <a href="{{ url('my-network') }}" class="btn btn_turquoise">My --}}
  {{--                 Network</a> --}}
  {{--             </div> --}}
  {{--           </div> --}}
  {{--         </div> --}}
  {{--       </div> --}}
  {{--     </div> --}}
  {{--   </div> --}}
  {{-- @endif --}}
  {{-- @php --}}
  {{--   @dump(get_defined_vars()); --}}
  {{-- @endphp --}}
  {{-- END ONLINE WILL --}}
  {{-- --}}
  {{-- <div class="card-box-style rounded p-5"> --}}
  {{--   <div class="accordion" id="accordionExample"> --}}
  {{--     <div class="accordion-item"> --}}
  {{--       <h2 class="accordion-header" id="headingOne"> --}}
  {{--         <button class="accordion-button" type="button" --}}
  {{--           data-bs-toggle="collapse" data-bs-target="#collapseOne" --}}
  {{--           aria-expanded="true" aria-controls="collapseOne"> --}}
  {{--           HOW WILL MY DATA BE STORED AND PROTECTED? --}}
  {{--         </button> --}}
  {{--       </h2> --}}
  {{--       <div id="collapseOne" class="accordion-collapse show collapse" --}}
  {{--         aria-labelledby="headingOne" data-bs-parent="#accordionExample"> --}}
  {{--         <div class="accordion-body"> --}}
  {{--           <p>At WrapUP, we take your data security seriously. All --}}
  {{--             information --}}
  {{--             is encrypted with a BCrypt --}}
  {{--             hashing algorithm meeting industry-standard encryption, strong --}}
  {{--             password policies, and --}}
  {{--             regular security measures to safeguard your information, --}}
  {{--             ensuring --}}
  {{--             a safe and trustworthy --}}
  {{--             experience on our platform. Want to know more? Send us an --}}
  {{--             email to --}}
  {{--             <a href="mailto:hello@wrapup.co.za">hello@wrapup.co.za</a> --}}
  {{--             with --}}
  {{--             your questions. --}}
  {{--           </p> --}}
  {{--         </div> --}}
  {{--       </div> --}}
  {{--     </div> --}}
  {{--     <div class="accordion-item"> --}}
  {{--       <h2 class="accordion-header" id="headingTwo"> --}}
  {{--         <button class="accordion-button collapsed" type="button" --}}
  {{--           data-bs-toggle="collapse" data-bs-target="#collapseTwo" --}}
  {{--           aria-expanded="false" aria-controls="collapseTwo"> --}}
  {{--           CAN WRAPUP SEE ANY INFORMATION ON MY PROFILE? --}}
  {{--         </button> --}}
  {{--       </h2> --}}
  {{--       <div id="collapseTwo" class="accordion-collapse collapse" --}}
  {{--         aria-labelledby="headingTwo" data-bs-parent="#accordionExample"> --}}
  {{--         <div class="accordion-body"> --}}
  {{--           <p>We only have access to your registration details, so we can --}}
  {{--             change them for you if needed. --}}
  {{--             However, we cannot access your password. You can reset it --}}
  {{--             whenever --}}
  {{--             you wish or in case you --}}
  {{--             have forgotten it. </p> --}}
  {{--         </div> --}}
  {{--       </div> --}}
  {{--     </div> --}}
  {{--     <div class="accordion-item"> --}}
  {{--       <h2 class="accordion-header" id="headingThree"> --}}
  {{--         <button class="accordion-button collapsed" type="button" --}}
  {{--           data-bs-toggle="collapse" data-bs-target="#collapseThree" --}}
  {{--           aria-expanded="false" aria-controls="collapseThree"> --}}
  {{--           I HAVE MORE QUESTIONS ABOUT MY PRIVACY, WHO CAN I CONTACT? --}}
  {{--         </button> --}}
  {{--       </h2> --}}
  {{--       <div id="collapseThree" class="accordion-collapse collapse" --}}
  {{--         aria-labelledby="headingThree" data-bs-parent="#accordionExample"> --}}
  {{--         <div class="accordion-body"> --}}
  {{--           <p>Send us an email to <a --}}
  {{--               href="mailto:hello@wrapup.co.za">hello@wrapup.co.za</a> --}}
  {{--             with --}}
  {{--             your --}}
  {{--             questions.</p> --}}
  {{--         </div> --}}
  {{--       </div> --}}
  {{--     </div> --}}
  {{--   </div> --}}
  {{-- --}}
  {{--   <!--<b><p>Your data will be safely secured and stored in accordance with the POPI Act and we take all necessary precautions to treat it with utmost sensitivity and respect.</p>--> --}}
  {{--   <!--<p>All info is encrypted with a BCrypt hashing algorithm, meeting all ISO27001 standards. Want to know more? Send us an email to <a href="mailto:hello@wrapup.co.za">hello@wrapup.co.za</a> with your questions.</p></b>--> --}}
  {{-- </div> --}}

</section>

@push('scripts')
  <script>
    document.addEventListener("DOMContentLoaded", () => {
      const statusIndicators = document.querySelectorAll(
        '.__status-indicator');

      statusIndicators.forEach((statusIndicator) => {
        const progressContainer = statusIndicator.querySelector(
          ".__progress-container");
        const progressCircle = statusIndicator.querySelector(
          ".__progress-circle .__circle");
        const progressText = statusIndicator.querySelector(
          ".__progress-number");
        const progress = progressContainer.getAttribute("data-progress");

        const circumference = 2 * Math.PI * 15.9155;

        progressCircle.style.strokeDasharray =
          `${circumference} ${circumference}`;
        progressCircle.style.strokeDashoffset = circumference;

        const offset = circumference - (progress / 100) * circumference;
        progressCircle.style.strokeDashoffset = offset;

        progressText.textContent = progress + "%";
      });
    });
  </script>
@endpush
