<section class="timeline-section">
  <div class="__heading">
    <h3>Timeline</h3>
  </div>
  @if (Auth::user()->free_account == 1)
    <div class="__empty-state">
      <div>
        <a href="{{ url('register/upgrade/' . Auth::user()->id) }}">
          <svg width="35" height="34" viewBox="0 0 35 34" fill="none"
            xmlns="http://www.w3.org/2000/svg">
            <path d="M27.1667 11.0833H25.5833V7.91667C25.5833 3.54667 22.0367 0
            17.6667 0C13.2967 0 9.75 3.54667 9.75 7.91667V11.0833H8.16667C6.425
            11.0833 5 12.5083 5 14.25V30.0833C5 31.825 6.425 33.25 8.16667
            33.25H27.1667C28.9083 33.25 30.3333 31.825 30.3333
            30.0833V14.25C30.3333 12.5083 28.9083 11.0833 27.1667
            11.0833ZM12.9167 7.91667C12.9167 5.28833 15.0383 3.16667 17.6667
            3.16667C20.295 3.16667 22.4167 5.28833 22.4167
            7.91667V11.0833H12.9167V7.91667ZM27.1667
            30.0833H8.16667V14.25H27.1667V30.0833Z" fill="#CF3D60" />
          </svg>
        </a>
        <div>
          <a href="{{ url('register/upgrade/' . Auth::user()->id) }}">
            Upgrade to access  this feature
          </a>
        </div>
      </div>
    </div>
  @else
    <div class="__events-grouped"
      @if ($groupedEvents->isEmpty()) style="height:calc(100% - 2rem);" @endif>
      @if ($groupedEvents->isEmpty())
        <div class="__empty-state">
          <div>
            <svg width="35" height="34" viewBox="0 0 35 34"
              fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M23.0269 3.03481C21.2806 2.3511 19.4414 2 17.5279
                2V2.01848C15.4471 2.01848 13.5336 2.38805 11.7873 3.14569C10.041
                3.88484 8.51762 4.91965 7.21717 6.23164C5.91672 7.54364 4.91351
                9.07738 4.1704 10.8144C3.42729 12.5514 3.03716 14.4177 3
                16.395V28.663C3 29.6979 3.39013 30.5664 4.18898 31.287C4.98783
                32.0077 5.89814 32.3588 6.9385 32.3588C7.97886 32.3588 8.88917
                31.9708 9.68802 31.1946C10.4869 30.4185 10.877 29.5316 10.877
                28.4967V21.715L10.8956 21.4397V16.0808C10.8956 15.2123 11.0814
                14.3808 11.4343 13.5862C11.7873 12.8101 12.2703 12.1264 12.8648
                11.572C13.4779 11.0177 14.1653 10.5742 14.9641 10.2415C15.763
                9.90892 16.599 9.74261 17.4721 9.74261C18.4196 9.74261 19.3113
                9.9274 20.1288 10.2785C20.9462 10.6296 21.6707 11.11 22.2838
                11.7014C22.8969 12.2927 23.3799 12.9949 23.7143 13.8079C24.0487
                14.621 24.2159 15.4895 24.2159 16.395L24.2345
                16.3765V21.4397L24.2159 21.715V28.663C24.2159 29.7348 24.606
                30.6218 25.3863 31.3055C26.1666 31.9892 27.0954 32.3403 28.173
                32.3403C29.2505 32.3034 30.1422 31.9153 30.8853 31.1577C31.6284
                30.4001 32 29.5131 32 28.4783V15.9145C32 13.9373 31.6099 12.0894
                30.811 10.3894C30.0122 8.68932 28.9532 7.21102 27.6342
                5.95446C26.3152 4.6979 24.7732 3.73701 23.0269 3.03481Z"
                fill="#CF3D60" fill-opacity="0.8" />
              <path d="M17.657 25.9556C19.8331 25.9556 21.5972 24.1915 21.5972
                22.0154C21.5972 19.8393 19.8331 18.0752 17.657 18.0752C15.4809
                18.0752 13.7168 19.8393 13.7168 22.0154C13.7168 24.1915 15.4809
                25.9556 17.657 25.9556Z" fill="#CF3D60" fill-opacity="0.8" />
            </svg>
            <div>
              <a href="{{ url('organiser/personal') }}">
                Get started with your MyAll Organiser
              </a>
            </div>
          </div>
        </div>
      @endif
      @if ($groupedEvents->isNotEmpty())
        @foreach ($groupedEvents as $date => $events)
          <div class="__events">
            <div class="__date-group">
              {{ $date }}
              <span></span>
            </div>
            @foreach ($events as $event)
              <div>
              </div>
              <div
                class="__event-container @if ($event->share_from) __shared @endif">
                <div class="__avatar">
                  @if ($event->contact($event->share_from))
                    <img
                      src="{{ asset('storage/' . $event->contact($event->share_from)->profile_pic) }}"
                      alt="{{ Auth::user()->name . ' ' . Auth::user()->surname }}">
                  @elseif (Auth::user()->profile_pic)
                    <img
                      src="{{ asset('storage/' . Auth::user()->profile_pic) }}"
                      alt="{{ Auth::user()->name . ' ' . Auth::user()->surname }}">
                  @else
                    {{ getInitials(Auth::user()->name . ' ' . Auth::user()->surname) }}
                  @endif
                </div>
                <div class="__details">
                  <div class="__description">{{ $event->description }}</div>
                  <div class="__timestamp">
                    {{ \Carbon\Carbon::parse($event->timestamp)->diffForHumans() }}
                  </div>
                </div>
              </div>
            @endforeach
          </div>
        @endforeach
      @endif
    </div>
  @endif
</section>
