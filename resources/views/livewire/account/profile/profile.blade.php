<div class="myallMainAppInner">

  {{-- <div wire:loading> --}}
  {{--   Processing... --}}
  {{-- </div> --}}

  @if ($view == 'home')
    @include('livewire.account.profile.home')
  @elseif($view == 'edit')
    @include('livewire.account.profile.edit')
  @endif

</div>
