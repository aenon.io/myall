<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <livewire:account.partials.header />
            <div class="row">
                <div class="col-md-12">
                    <div class="card-box-style rounded p-3">
                        <div class="others-title organiser-description-medium">
                            <h3>Search</h3>
                            <hr />
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                @if(count($results) == 0)
                                    <div class="my-5 text-center">
                                        <h2>No Results found</h2>
                                    </div>
                                @else
                                <div class="mb-3">
                                    <b>Please select infographics / videos below for helpful information</b>
                                </div>
                                <table class="table">
                                    @foreach($results AS $result)
                                        <tr>
                                            <td>
                                                <a href="{{ $result->path }}" target="_blank">{{ $result->title }}</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
