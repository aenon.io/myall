<div class="row">
  <div class="col-md-12">
    <div class="accordion" id="accordionBusWealth">
      @foreach ($cur_contact->businessWealths as $key => $bus)
        <div class="accordion-item">
          <h2 class="accordion-header" id="heading_{{ $key }}">
            <button class="accordion-button collapsed" type="button"
              data-bs-toggle="collapse"
              data-bs-target="#collapse_{{ $key }}"
              aria-expanded="false"
              aria-controls="collapse_{{ $key }}">
              {{ $bus->business_name }}
            </button>
          </h2>
          <div id="collapse_{{ $key }}"
            class="accordion-collapse collapse"
            aria-labelledby="heading_{{ $key }}"
            data-bs-parent="#accordionBusWealth">
            <div class="accordion-body">
              <table class="table">
                <tbody>
                  <tr>
                    <td>Business Type</td>
                    <th>{{ $bus->business_type }}</th>
                  </tr>
                  <tr>
                    <td>Business Name</td>
                    <th>{{ $bus->business_name }}</th>
                  </tr>
                  @if ($bus->registration_number)
                    <tr>
                      <td>Registration Number</td>
                      <th>{{ $bus->registration_number }}</th>
                    </tr>
                  @endif
                  @if ($bus->company_url)
                    <tr>
                      <td>Website</td>
                      <th>{{ $bus->company_url }}</th>
                    </tr>
                  @endif
                  @if ($bus->vat_number)
                    <tr>
                      <td>VAT Number</td>
                      <th>{{ $bus->vat_number }}</th>
                    </tr>
                  @endif
                  @if ($bus->masters_office)
                    <tr>
                      <td>Masters Office</td>
                      <th>{{ $bus->masters_office }}</th>
                    </tr>
                  @endif
                  @if ($bus->trust_type)
                    <tr>
                      <td>Trust Type</td>
                      <th>{{ $bus->trust_type }}</th>
                    </tr>
                  @endif
                  @if ($bus->buy_sell_agreement)
                    <tr>
                      <td>Buy Sell Agreement</td>
                      <th>{{ $bus->buy_sell_agreement }}</th>
                    </tr>
                  @endif
                  @if ($bus->value_min)
                    <tr>
                      <td>Value Min</td>
                      <th>{{ $bus->value_min }}</th>
                    </tr>
                  @endif
                  @if ($bus->value_max)
                    <tr>
                      <td>Value Max</td>
                      <th>{{ $bus->value_max }}</th>
                    </tr>
                  @endif
                  @if ($bus->equipment)
                    <tr>
                      <td>Equipment</td>
                      <th>{{ $bus->equipment }}</th>
                    </tr>
                  @endif
                  @if ($bus->outstanding_debtors)
                    <tr>
                      <td>Outstanding Debtors</td>
                      <th>{{ $bus->outstanding_debtors }}</th>
                    </tr>
                  @endif
                  @if ($bus->goodwill)
                    <tr>
                      <td>Goodwill</td>
                      <th>{{ $bus->goodwill }}</th>
                    </tr>
                  @endif
                  @if ($bus->net_value)
                    <tr>
                      <td>Net Value</td>
                      <th>{{ $bus->net_value }}</th>
                    </tr>
                  @endif
                  @if ($bus->credit)
                    <tr>
                      <td>Credit</td>
                      <th>{{ $bus->credit }}</th>
                    </tr>
                  @endif
                  @if ($bus->debt)
                    <tr>
                      <td>Debt</td>
                      <th>{{ $bus->debt }}</th>
                    </tr>
                  @endif
                  @if ($bus->auditor_name)
                    <tr>
                      <td>Auditor's Name</td>
                      <th>
                        {{ $bus->auditor_name . ' ' . $bus->auditor_surname }}
                      </th>
                    </tr>
                  @endif
                  @if ($bus->auditor_tel)
                    <tr>
                      <td>Auditor's Tel</td>
                      <th>{{ $bus->auditor_tel }}</th>
                    </tr>
                  @endif
                  @if ($bus->auditor_email)
                    <tr>
                      <td>Auditor's Email</td>
                      <th>{{ $bus->auditor_email }}</th>
                    </tr>
                  @endif
                  @if ($bus->auditor_address)
                    <tr>
                      <td>Auditor's Address</td>
                      <th>{{ $bus->auditor_address }}</th>
                    </tr>
                  @endif
                  @if ($bus->members_resolution)
                    <tr>
                      <td>Members Resolution</td>
                      <th><a
                          href="{{ url('storage/' . $bus->members_resolution) }}">View</a>
                      </th>
                    </tr>
                  @endif
                  @if ($bus->amended_founding_statement)
                    <tr>
                      <td>Amended Founding Statement</td>
                      <th><a
                          href="{{ url('storage/' . $bus->amended_founding_statement) }}">View</a>
                      </th>
                    </tr>
                  @endif
                  @if ($bus->founding_statement)
                    <tr>
                      <td>Founding Statement</td>
                      <th><a
                          href="{{ url('storage/' . $bus->founding_statement) }}">View</a>
                      </th>
                    </tr>
                  @endif
                  @if ($bus->directors_resolution)
                    <tr>
                      <td>Directors Resolution</td>
                      <th><a
                          href="{{ url('storage/' . $bus->directors_resolution) }}">View</a>
                      </th>
                    </tr>
                  @endif
                  @if ($bus->letter_from_auditors)
                    <tr>
                      <td>Letter From Auditors</td>
                      <th><a
                          href="{{ url('storage/' . $bus->letter_from_auditors) }}">View</a>
                      </th>
                    </tr>
                  @endif
                  @if ($bus->sars_issued_document)
                    <tr>
                      <td>SARS Issued Document</td>
                      <th><a
                          href="{{ url('storage/' . $bus->sars_issued_document) }}">View</a>
                      </th>
                    </tr>
                  @endif
                  @if ($bus->certificate_of_registration)
                    <tr>
                      <td>Certificate Of Registration</td>
                      <th><a
                          href="{{ url('storage/' . $bus->certificate_of_registration) }}">View</a>
                      </th>
                    </tr>
                  @endif
                  @if ($bus->resolution_by_trustee)
                    <tr>
                      <td>Resolution By Trustee</td>
                      <th><a
                          href="{{ url('storage/' . $bus->resolution_by_trustee) }}">View</a>
                      </th>
                    </tr>
                  @endif
                  @if ($bus->letter_of_authority)
                    <tr>
                      <td>Letter Of Authority</td>
                      <th><a
                          href="{{ url('storage/' . $bus->letter_of_authority) }}">View</a>
                      </th>
                    </tr>
                  @endif
                  @if ($bus->trust_deed)
                    <tr>
                      <td>Trust Deed</td>
                      <th><a
                          href="{{ url('storage/' . $bus->trust_deed) }}">View</a>
                      </th>
                    </tr>
                  @endif
                </tbody>
              </table>
              @if ($bus->debtors->count() > 0)
                <h5>Debtors</h5>
                <table class="table">
                  <thead>
                    <tr>
                      <th>Debt To</th>
                      <th>Value</th>
                      <th>Insured Debt</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($bus->debtors as $debtor)
                      <tr>
                        <td>{{ $debtor->debt_to }}</td>
                        <td>{{ $debtor->debt_value }}</td>
                        <td>{{ $debtor->debt_insured }}</td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              @endif
              @if ($bus->shareholders->count() > 0)
                <h5>Shareholders / Partners</h5>
                <table class="table">
                  <thead>
                    <tr>
                      <th>Name</th>
                      <th>Stakeholder %</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($bus->shareholders as $ptn)
                      <tr>
                        <td>
                          {{ $ptn->member_name . ' ' . $ptn->member_surname }}
                        </td>
                        <td>{{ $ptn->stake_percentage }}</td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              @endif
              @if ($bus->trustees->count() > 0)
                <h5>Trustees</h5>
                <table class="table">
                  <thead>
                    <tr>
                      <th>Name</th>
                      <th>Contact</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($bus->trustees as $trst)
                      <tr>
                        <td>
                          {{ $trst->trustee_first_name . ' ' . $trst->trustee_surname }}
                        </td>
                        <td>{{ $trst->trustee_contact }}</td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              @endif
              @if ($bus->my_possessions->count() > 0)
                <h5>Possessions</h5>
                <table class="table">
                  <thead>
                    <tr>
                      <th>Possession</th>
                      <th>Description</th>
                      <th>Value</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($bus->my_possessions as $pos)
                      <tr>
                        <td>{{ $pos->possessions }}</td>
                        <td>{{ $pos->specify_possession }}</td>
                        <td>{{ $pos->value }}</td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              @endif
            </div>
          </div>
        </div>
      @endforeach
    </div>
  </div>
</div>
