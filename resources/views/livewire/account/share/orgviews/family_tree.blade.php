<div class="row">
  <div class="col-md-12">
    <div class="text-center">
      <b>{{ ucwords($view) }}</b>
    </div>
    <div class="card-box-style rounded" drag-root wire:sortable="reOrderItems">
      @foreach ($cur_contact->shareContactsByType($view) as $cnt)
        <div class="d-flex network-content-block {{ $cnt->color }}"
          drag-item="{{ $cnt->id }}" wire:sortable.item="{{ $cnt->id }}"
          draggable="true" wire:key="{{ $cnt->id }}">
          <div class="network-cnt-pic">
            @if ($cnt->contact)
              @if ($cnt->contact->profile_pic)
                <img src="{{ asset('storage/' . $cnt->contact->profile_pic) }}">
              @else
                <img src="{{ asset('img/placeholder.png') }}">
              @endif
            @else
              <img src="{{ asset('img/placeholder.png') }}">
            @endif
          </div>
          <div class="network-cnt-content">
            <div class="">
              <b class="text-blue">{{ $cnt->name }}</b><br />
              <p>{{ $cnt->dob }}</p>
              <p>{{ $cnt->family }}</p>
              <p>{{ $cnt->email }}</p>
            </div>
          </div>
        </div>
      @endforeach
    </div>
  </div>
</div>

