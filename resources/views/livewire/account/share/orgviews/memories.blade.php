<div class="row">
  <div class="col-md-12">
    <div class="accordion" id="accordionMemory">
      @foreach ($cur_contact->memories as $memory)
        <div class="accordion-item">
          <h2 class="accordion-header" id="heading_{{ $memory->id }}">
            <button class="accordion-button collapsed" type="button"
              data-bs-toggle="collapse"
              data-bs-target="#collapse_{{ $memory->id }}"
              aria-expanded="false"
              aria-controls="collapse_{{ $memory->id }}">
              {{ $memory->media_type }} @if ($memory->name)
                {{ ' - ' . $memory->name . ' ' . $memory->surname }}
              @endif
            </button>
          </h2>
          <div id="collapse_{{ $memory->id }}"
            class="accordion-collapse collapse"
            aria-labelledby="heading_{{ $memory->id }}"
            data-bs-parent="#accordionMemory">
            <div class="accordion-body">
              <ul class="list-group">
                <li class="list-group-item d-flex">
                  <span>Memory Type</span>
                  <b class="ms-auto">{{ $memory->media_type }}</b>
                </li>
                @if ($memory->media_type != 'Questionnaire' && $memory->media_type != 'Link')
                  <li class="list-group-item d-flex">
                    <span>File</span>
                    <b class="ms-auto">
                      <a href="{{ url('storage/' . $memory->media) }}">View
                        Media</a>
                    </b>
                  </li>
                @elseif($memory->media_type == 'Link')
                  <li class="list-group-item d-flex">
                    <span>Link</span>
                    <b class="ms-auto">
                      <a href="{{ $memory->link }}">Open Link</a>
                    </b>
                  </li>
                @elseif($memory->media_type == 'Questionnaire')
                  <li class="list-group-item">
                    <div>Questionnaire</div>
                    @if ($memory->questionnaire->count() > 0)
                      @foreach ($memory->questionnaire as $qtn)
                        <table class="table">
                          <tbody>
                            @foreach ($questionnaire_questions as $key => $q)
                              <tr>
                                <td>{{ $q }}</td>
                                <td>{{ $qtn->$key }}</td>
                              </tr>
                            @endforeach
                          </tbody>
                        </table>
                      @endforeach
                    @endif
                  </li>
                @endif
                @if ($memory->name)
                  <li class="list-group-item d-flex">
                    <span>Intended For</span>
                    <b
                      class="ms-auto">{{ $memory->name . ' ' . $memory->surname }}</b>
                  </li>
                  <li class="list-group-item d-flex">
                    <span>Email</span>
                    <b class="ms-auto">{{ $memory->email }}</b>
                  </li>
                  <li class="list-group-item d-flex">
                    <span>ID Number</span>
                    <b class="ms-auto">{{ $memory->id_number }}</b>
                  </li>
                  <li class="list-group-item d-flex">
                    <span>Phone Number</span>
                    <b class="ms-auto">{{ $memory->phone_number }}</b>
                  </li>
                @endif
                @if ($memory->notes)
                  <li class="list-group-item">
                    <div>Notes</div>
                    <p>{{ $memory->notes }}</p>
                  </li>
                @endif
              </ul>
            </div>
          </div>
        </div>
      @endforeach
    </div>
  </div>
</div>

