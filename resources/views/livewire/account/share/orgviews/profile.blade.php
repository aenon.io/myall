<div class="__orgview-profile">
  <div class="__content">
    <div>
      <div class="__avatar-container">
        <div class="__avatar">
          @if ($cur_contact->profile_pic)
            <img class="rounded-50-lg"
              src="{{ asset('storage/' . $cur_contact->profile_pic) }}"
              alt="Images">
          @else
            <img class="rounded-50-lg" src="{{ asset('img/placeholder.png') }}"
              alt="Images">
          @endif
        </div>
        <div class="__name">
          {{ $cur_contact->name . ' ' . $cur_contact->surname }}
        </div>
      </div>
    </div>
    <div class="__details">
      <div class="__c-2">
        <div class="__item">
          <span class="__heading">Email</span>
          <span class="__data">{{ $cur_contact->email }}</span>
        </div>
        <div class="__item">
          <span class="__heading">Contact Number</span>
          <span class="__data">{{ $cur_contact->contact }}</span>
        </div>
      </div>
      <div class="__c-3">
        <div class="__item">
          <span class="__heading">ID Number</span>
          <span class="__data">{{ $cur_contact->id_number }}</span>
        </div>
        <div class="__item">
          <span class="__heading">Passport Number</span>
          <span class="__data">{{ $cur_contact->passport_number }}</span>
        </div>
        <div class="__item">
          <span class="__heading">Date of Birth</span>
          <span class="__data">{{ $cur_contact->date_of_birth }}</span>
        </div>
      </div>
      <div class="__c-1">
        <div class="__item">
          <span class="__heading">Bio</span>
          <span class="__data">{{ $cur_contact->bio }}</span>
        </div>
      </div>
    </div>
  </div>
</div>
