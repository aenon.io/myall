<div class="row">
  <div class="col-md-12">
    <div class="accordion" id="accordionSafeKeep">
      <div class="accordion-item">
        <h2 class="accordion-header" id="headingsafe">
          <button class="accordion-button collapsed" type="button"
            data-bs-toggle="collapse" data-bs-target="#collapsesafe"
            aria-expanded="false" aria-controls="collapsesafe">
            Safekeeping
          </button>
        </h2>
        <div id="collapsesafe" class="accordion-collapse collapse"
          aria-labelledby="headingsafe" data-bs-parent="#accordionSafeKeep">
          <div class="accordion-body">
            <table class="table">
              @foreach ($cur_contact->safe_keep as $item)
                <tr>
                  <td>
                    <ul class="list-group">
                      <li class="list-group-item">
                        <b>Title / Item name</b>
                        <p>{{ $item->title }}</p>
                      </li>
                      <li class="list-group-item">
                        <b>Description</b>
                        <p>{{ $item->description }}</p>
                      </li>
                      <li class="list-group-item">
                        <b>Where to find</b>
                        <p>{{ $item->location }}</p>
                      </li>
                      @if ($item->file)
                        <li class="list-group-item">
                          <b>File</b>
                          <p><a
                              href="{{ url('storage/' . $item->file) }}">View</a>
                          </p>
                        </li>
                      @endif
                    </ul>
                  </td>
                </tr>
              @endforeach
            </table>
          </div>
        </div>
      </div>
      <div class="accordion-item">
        <h2 class="accordion-header" id="headingtax">
          <button class="accordion-button collapsed" type="button"
            data-bs-toggle="collapse" data-bs-target="#collapsetax"
            aria-expanded="false" aria-controls="collapsetax">
            Income Tax
          </button>
        </h2>
        <div id="collapsetax" class="accordion-collapse collapse"
          aria-labelledby="headingtax" data-bs-parent="#accordionSafeKeep">
          <div class="accordion-body">
            @if ($cur_contact->tax)
              <ul class="list-group">
                <li class="list-group-item">
                  <b>Tax Number</b>
                  <p>{{ $cur_contact->tax->tax_number }}</p>
                </li>
                @if ($cur_contact->tax->tax_assessment)
                  <li class="list-group-item">
                    <b>Latest Assessment</b>
                    <p><a
                        href="{{ url('storage/' . $cur_contact->tax->tax_assessment) }}">View</a>
                    </p>
                  </li>
                @endif
                @if ($cur_contact->tax->tax_certificate)
                  <li class="list-group-item">
                    <b>VAT Registration Certificate</b>
                    <p><a
                        href="{{ url('storage/' . $cur_contact->tax->tax_certificate) }}">View</a>
                    </p>
                  </li>
                @endif
              </ul>
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

