<div class="row">
  <div class="col-md-12">
    <div class="accordion" id="accordionSocial">
      @foreach ($cur_contact->groupedSocialMedia() as $key => $grp)
        <div class="accordion-item">
          <h2 class="accordion-header" id="heading_{{ strtolower($key) }}">
            <button class="accordion-button collapsed" type="button"
              data-bs-toggle="collapse"
              data-bs-target="#collapse_{{ strtolower($key) }}"
              aria-expanded="false"
              aria-controls="collapse_{{ strtolower($key) }}">
              {{ $key }}
            </button>
          </h2>
          <div id="collapse_{{ strtolower($key) }}"
            class="accordion-collapse collapse"
            aria-labelledby="heading_{{ strtolower($key) }}"
            data-bs-parent="#accordionSocial">
            <div class="accordion-body">
              @foreach ($grp as $md)
                <div class="d-none d-sm-block mb-3">
                  {{-- <h5> --}}
                  {{--   @if ($md->network != 'Other') --}}
                  {{--     {{ $md->network }} --}}
                  {{--   @else --}}
                  {{--     {{ $md->specific_network }} --}}
                  {{--   @endif --}}
                  {{-- </h5> --}}
                  <ul class="list-group">
                    <li class="list-group-item d-flex">
                      Username
                      <span class="ms-auto">
                        {{ $md->user_name }}
                      </span>
                    </li>
                    <li class="list-group-item d-flex">
                      Link
                      <span class="ms-auto">
                        <a href="{{ $md->link }}"
                          target="_blank">{{ $md->link }}</a>
                      </span>
                    </li>
                    @if ($md->legacy_contact)
                      <li class="list-group-item d-flex">
                        Legacy Contact
                        <span class="ms-auto">
                          {{ $md->legacy_contact }}
                        </span>
                      </li>
                    @endif
                    <li class="list-group-item">
                      Instructions<br />
                      <b>
                        {{ $md->instructions }}
                      </b>
                    </li>
                  </ul>
                </div>
                <div class="d-block d-sm-none mb-3">
                  <h5>
                    @if ($md->network != 'Other')
                      {{ $md->network }}
                    @else
                      {{ $md->specific_network }}
                    @endif
                  </h5>
                  <div class="col-md-12 overflow-auto">
                    <table class="table">
                      <tbody>
                        <tr>
                          <td><b>Userame</b></td>
                          <td>{{ $md->user_name }}</td>
                        </tr>
                        <tr>
                          <td><b>Link</b></td>
                          <td><a href="{{ $md->link }}"
                              target="_blank">{{ $md->link }}</a></td>
                        </tr>
                        @if ($md->legacy_contact)
                          <tr>
                            <td><b>Legacy Contact</b></td>
                            <td>{{ $md->legacy_contact }}</td>
                          </tr>
                        @endif
                        <tr>
                          <td><b>Instructions</b></td>
                          <td>{{ $md->instructions }}</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              @endforeach
            </div>
          </div>
        </div>
      @endforeach
    </div>
  </div>
</div>

