{{-- @php --}}
{{--   @dump(get_defined_vars()); --}}
{{-- @endphp --}}
<div class="">
  <div class="col-md-12 p-3">
    <div class="row">
      <div class="col-md-12">
        @if ($errors->any())
          <div class="alert alert-danger">
            <span>{{ $errors->first() }}</span>
          </div>
        @endif
        @if (session()->has('message'))
          <div class="alert alert-success">
            {{ session('message') }}
          </div>
        @endif
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
        <div class="mb-3">
          <label class="form-label">Name</label>
          <input type="text" class="form-control" name="name"
            wire:model.defer="name">
        </div>
      </div>
      <div class="col-md-6">
        <div class="mb-3">
          <label class="form-label">ID / Passport Number</label>
          <input type="text" class="form-control" name="id_number"
            wire:model.defer="id_number">
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
        <div class="mb-3">
          <label class="form-label">Email</label>
          <input type="email" class="form-control" name="email"
            wire:model.defer="email">
        </div>
      </div>
      <div class="col-md-6">
        <div class="mb-3">
          <label class="form-label">Contact Number</label>
          <input type="text" class="form-control" name="contact_number"
            wire:model.defer="contact_number">
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-4">
        <div class="mb-3">
          <label class="form-label">Date Of Birth</label>
          <input type="date" class="form-control" max="2999-12-31"
            name="dob" wire:model.defer="dob">
        </div>
      </div>
      <div class="col-md-8">
        <label class="form-label">Relation</label>
        <div class="row mb-3">
          <div class="col">
            <select class="form-control" name="family" wire:model="family">
              <option value="" disabled selected>Family</option>
              @foreach ($families as $val)
                <option value="{{ $val }}">{{ $val }}</option>
              @endforeach
            </select>
          </div>
          <div class="col">
            <select class="form-control" name="friends" wire:model="friends">
              <option value="" disabled selected>Friends</option>
              @foreach ($friends_arr as $val)
                <option value="{{ $val }}">{{ $val }}</option>
              @endforeach
            </select>
          </div>
          <div class="col">
            <select class="form-control" name="professional"
              wire:model="professional">
              <option value="" disabled selected>Professional</option>
              @foreach ($professional_arr as $val)
                <option value="{{ $val }}">{{ $val }}</option>
              @endforeach
            </select>
          </div>
        </div>
      </div>
    </div>
    @if ($show_specify)
      <div class="row">
        <div class="col-md-12">
          <div class="mb-3">
            <label class="form-label">Specify Relation</label>
            <input type="text" class="form-control" name="specify_relation"
              wire:model.defer="specify_relation">
          </div>
        </div>
      </div>
    @endif
    <div class="row">
      <div class="col-md-12">
        <div class="mb-3">
          @if ($share_id && $emergency_contact)
            <p>If you want to remove this contact from emergency contacts, do it
              in your organiser.</p>
          @else
            <div class="form-check">
              <input class="form-check-input" type="checkbox" value="yes"
                id="emergency_contact" wire:model.defer="emergency_contact">
              <label class="form-check-label" for="emergency_contact">
                Add this contact as an Emergency contact
              </label>
            </div>
          @endif
        </div>
      </div>
    </div>
    {{-- @if ($cur_share) --}}
    {{--   @if ($cur_share->shareOptions->count() > 1) --}}
    {{--     <div class="row my-3"> --}}
    {{--       <div class="col-md-12"> --}}
    {{--         <div class="accordion" id="accordionShareHistory" wire:ignore> --}}
    {{--           <div class="accordion-item"> --}}
    {{--             <h2 class="accordion-header" id="headingShareHistory"> --}}
    {{--               <button class="accordion-button collapsed" type="button" --}}
    {{--                 data-bs-toggle="collapse" --}}
    {{--                 data-bs-target="#collapseShareHistory" aria-expanded="false" --}}
    {{--                 aria-controls="collapseShareHistory"> --}}
    {{--                 Share History --}}
    {{--               </button> --}}
    {{--             </h2> --}}
    {{--             <div id="collapseShareHistory" --}}
    {{--               class="accordion-collapse collapse" --}}
    {{--               aria-labelledby="headingShareHistory" --}}
    {{--               data-bs-parent="#accordionShareHistory"> --}}
    {{--               <div class="accordion-body"> --}}
    {{--                 <table class="table"> --}}
    {{--                   @foreach ($cur_share->shareOptions as $sh) --}}
    {{--                     <tr> --}}
    {{--                       <td><a href="#" --}}
    {{--                           wire:click.prevent="showShareHistoryModal({{ $sh->id }})">{{ date('F d, Y H:i', strtotime($sh->created_at)) }}</a> --}}
    {{--                       </td> --}}
    {{--                     </tr> --}}
    {{--                   @endforeach --}}
    {{--                 </table> --}}
    {{--               </div> --}}
    {{--             </div> --}}
    {{--           </div> --}}
    {{--         </div> --}}
    {{--       </div> --}}
    {{--     </div> --}}
    {{--   @endif --}}
    {{-- @endif --}}

    <div class="row">
      <div class="col-md-12">
        @foreach ($sections as $k => $section)
          @if (is_array($section))
            <div class="mt-3">
              <label
                class="form-label"><b>{{ ucwords(str_replace('_', ' ', $k)) }}</b></label>
              <div class="row ms-2">
                @foreach ($section as $kk => $sec)
                  @if (!is_array($sec))
                    <div class="col-md-3">
                      <div class="form-check">
                        <input class="form-check-input {{ $k }}"
                          type="checkbox" value="{{ $sec }}"
                          wire:model.defer="share_sections">
                        <label class="form-check-label" for="flexCheckDefault">
                          @if ($sec == 'safe_keeping')
                            Safekeeping
                          @elseif($sec == 'debt_expenses')
                            Debt & Expenses
                          @else
                            {{ ucwords(str_replace('_', ' ', $sec)) }}
                          @endif
                        </label>
                      </div>
                    </div>
                  @else
                    <div class="col-md-12 mt-3">
                      <div class="row">
                        <div class="col-md-12">
                          <hr />
                        </div>
                        <div class="col-md-12">
                          <label class="form-label">
                            @if ($kk == 'connections')
                              Emergency Contacts
                            @elseif($kk == 'properties_wealth')
                              Personal Wealth
                            @elseif($kk == 'debt_expenses')
                              Debt & Expenses
                            @elseif($kk == 'safe_keeping')
                              Safekeeping
                            @else
                              {{ ucwords(str_replace('_', ' ', $kk)) }}
                            @endif
                          </label>
                        </div>
                        @foreach ($sec as $ss)
                          <div class="col-md-3 ms-2">
                            <div class="form-check">
                              <input
                                class="form-check-input {{ $kk }}"
                                type="checkbox" value="{{ $ss }}"
                                wire:model.defer="share_sections">
                              <label class="form-check-label"
                                for="flexCheckDefault">
                                @if ($ss == 'accountant_bookkeeper')
                                  Accountant / Bookkeeper
                                @elseif($ss == 'safe_keeping')
                                  Safekeeping
                                @elseif($ss == 'children')
                                  Children <a href="#"
                                    data-bs-toggle="modal"
                                    data-bs-target="#minorModal"><i
                                      class="fa fa-info-circle"
                                      style="color: #1c355e"></i></a>
                                @else
                                  {{ ucwords(str_replace('_', ' ', $ss)) }}
                                @endif
                              </label>
                            </div>
                          </div>
                        @endforeach
                      </div>
                    </div>
                  @endif
                @endforeach

              </div>
            </div>
          @else
            <div class="form-check">
              <input class="form-check-input" type="checkbox"
                value="{{ $section }}"
                wire:model.defer="share_sections">
              <label class="form-check-label" for="flexCheckDefault">
                @if ($section == 'family_tree')
                  <b>My Network</b>
                @else
                  <b>{{ ucwords(str_replace('_', ' ', $section)) }}</b>
                @endif
              </label>
            </div>
          @endif
        @endforeach
      </div>
    </div>
    <div class="row">
      <div class="col-md-12 mt-3">
        <div class="mb-3">
          <div class="form-check">
            <input class="form-check-input" type="checkbox" value="yes"
              id="accept_share_terms" wire:model.defer="accept_share_terms">
            <label class="form-check-label" for="accept_share_terms">
              I have carefully reviewed the <a
                href="{{ url('docs/WrapUp Guide to the POPIA Act_29Aug2023.pdf') }}"
                target="_blank"><b>"MyAll Protection of Personal Information
                  Act Policy"</b></a> and hereby authorise MyAll to share and
              manage my data in accordance with the POPI Act, as well as
              following my instructions when sharing my selected personal and
              financial information with my selected connections. I am aware
              that if I choose not to accept the terms and conditions outlined
              in this policy and consent declaration, it is my responsibility to
              immediately notify MyAll. Failure to do so will imply my
              acceptance of and agreement to the terms and conditions as
              stipulated in the Policy, <a
                href="{{ url('docs/MyAll Terms and Conditions.pdf') }}"
                target="_blank"><b>Terms & Conditions</b></a> and <a
                href="{{ url('docs/MyAll Privacy Policy.pdf') }}"
                target="_blank"><b>Privacy Policy</b></a>.
            </label>
          </div>
        </div>
      </div>
    </div>

    {{-- @php --}}
    {{--   @dump(get_defined_vars()); --}}
    {{-- @endphp --}}
    <div class="__save-share-wrapper">
      <div class="__save-share-content">
        {{-- @if (Request::is('share-profile/edit/*')) --}}
        <div class="">
          <a href="#" class="btn btn-outline-two"
            data-bs-toggle="modal" data-bs-target="#delete-modal">
            <svg width="20" height="20" viewBox="0 0 20 20"
              fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M13.3332 7.5V15.8333H6.6665V7.5H13.3332ZM12.0832
                2.5H7.9165L7.08317
                3.33333H4.1665V5H15.8332V3.33333H12.9165L12.0832 2.5ZM14.9998
                5.83333H4.99984V15.8333C4.99984 16.75 5.74984 17.5 6.6665
                17.5H13.3332C14.2498 17.5 14.9998 16.75 14.9998
                15.8333V5.83333Z" fill="#CF3D60" />
            </svg>
            Delete Share
          </a>
        </div>
        <div class="">
          <a href="#" type="button" class="btn btn-outline-two"
            wire:click.prevent="saveShare">
            <svg width="20" height="20" viewBox="0 0 20 20"
              fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M14.1667 2.5H4.16667C3.24167 2.5 2.5 3.25 2.5
                4.16667V15.8333C2.5 16.75 3.24167 17.5 4.16667
                17.5H15.8333C16.75 17.5 17.5 16.75 17.5 15.8333V5.83333L14.1667
                2.5ZM15.8333 15.8333H4.16667V4.16667H13.475L15.8333
                6.525V15.8333ZM10 10C8.61667 10 7.5 11.1167 7.5 12.5C7.5 13.8833
                8.61667 15 10 15C11.3833 15 12.5 13.8833 12.5 12.5C12.5 11.1167
                11.3833 10 10 10ZM5 5H12.5V8.33333H5V5Z" fill="#CF3D60" />
            </svg>
            Update
          </a>
        </div>
        {{-- @endif --}}
        {{-- @if (Request::is('share-profile/form')) --}}
        <div class="">
          <a href="#" type="button" class="btn btn-red"
            data-bs-toggle="modal" data-bs-target="#share-modal"
            wire:loading.attr="disabled">
            <svg width="20" height="20" viewBox="0 0 20 20"
              fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M2.925 5.025L9.18333 7.70833L2.91667 6.875L2.925 5.025ZM9.175
                12.2917L2.91667 14.975V13.125L9.175 12.2917ZM1.25833 2.5L1.25
                8.33333L13.75 10L1.25 11.6667L1.25833 17.5L18.75 10L1.25833
                2.5Z" fill="white" />
            </svg>
            Save & Share
          </a>
        </div>
        {{-- @endif --}}
      </div>
    </div>

  </div>

  <div class="modal" id="delete-modal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <p><b>Warning:</b> By clicking the delete button, all information
                shared with this specific contact will be permanently removed
                from this contact's profile and cannot be recovered or
                reinstated but you can however choose to share info with them at
                another time.</p>
              <div class="mt-3 text-center">
                <a href="#" class="btn btn-red"
                  wire:click.prevent="deleteShare">Delete</a>
                <a href="#" class="btn btn-outline-two"
                  data-bs-dismiss="modal">Cancel</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal" id="minorModal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="text-navy-blue">Children</h4>
          <button type="button" class="btn-close" data-bs-dismiss="modal"
            aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <p>According to the POPI Act, only a person who is legally
                competent to do so, which includes a person who is the legal
                guardian of their minor child, may consent to the sharing of
                general information about their minor children.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal" tabindex="-1" id="share-modal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Share Profile</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal"
            aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <div class="">
            <p>You are about to share Personal and Financial information - to
              proceed, kindly indicate that you understand and you are certain
              that you want to do so.</p>
            <input class="form-check-input" type="checkbox"
              id="consent_check" wire:model.defer="share_consent">
            <label class="form-check-label share-label" for="consent_check">
              I understand
            </label>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-outline-two"
            data-bs-dismiss="modal">Close</button>
          <button type="button" class="btn btn-red"
            wire:click.prevent="shareProfile">Continue</button>
        </div>
      </div>
    </div>
  </div>

  <div class="modal" tabindex="-1" id="shared-options-modal">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Shared Options</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal"
            aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <div class="">
            <table class="table">
              @php
                $ignore = ['id', 'share_id', 'created_at', 'updated_at'];
              @endphp
              @foreach ($shared_opts as $key => $val)
                @if ($val)
                  @if (!in_array($key, $ignore))
                    <tr>
                      <td>
                        @if ($key == 'family_tree')
                          My Network
                        @else
                          {{ ucwords(str_replace('_', ' ', $key)) }}
                        @endif
                      </td>
                    </tr>
                  @endif
                @endif
              @endforeach
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  @push('scripts')
    <script>
      window.addEventListener('close-share-modal', event => {
        $('.modal').modal('hide');
        $(window).scrollTop(0);
      });
      window.addEventListener('alert-error', event => {
        Swal.fire("Error", event.detail.message, "error");
      });
      window.addEventListener('show-shared-options-modal', event => {
        $('#shared-options-modal').modal('show');
      });
    </script>
  @endpush
</div>
