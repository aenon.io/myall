<div class="__shared-with-me">
  <div class="" style="padding: 0rem 1rem">
    @if ($cur_share)
      <div class="">
        <div class="">
          {{-- <span class="text-muted"> --}}
          {{--   Click on a category below to view the info --}}
          {{-- </span> --}}
        </div>
        <div class="">
          <nav class="navbar navbar-expand-lg navbar-light"
            style="justify-content: center">
            <div class="">
              <button class="navbar-toggler" type="button"
                data-bs-toggle="collapse"
                data-bs-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false"
                aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>
              <div class="shar-nav navbar-collapse collapse"
                id="navbarSupportedContent">
                <ul class="navbar-nav mb-lg-0 mb-2 me-auto">
                  @if ($cur_share->profile)
                    <li class="nav-item">
                      <a class="nav-link @if ($cur_view == 'profile') active @endif"
                        aria-current="page" href="#"
                        wire:click.prevent="showPage('profile', 'profile')">Profile</a>
                    </li>
                  @endif
                  @if ($cur_share->organiser)
                    <li class="nav-item dropdown">
                      <a class="nav-link dropdown-toggle @if ($cur_view == 'organiser') active @endif"
                        href="#" id="navbarDropdown" role="button"
                        data-bs-toggle="dropdown" aria-expanded="false">
                        Organiser
                        <i class="bx bx-chevron-down"></i>
                      </a>
                      <ul class="dropdown-menu burger-menu"
                        aria-labelledby="navbarDropdown">
                        @if ($cur_share->personal)
                          <li>
                            <a class="dropdown-item" href="#"
                              wire:click.prevent="showPage('organiser', 'personal')">
                              Personal
                            </a>
                          </li>
                        @endif
                        @if ($cur_share->emergency)
                          <li>
                            <a class="dropdown-item" href="#"
                              wire:click.prevent="showPage('organiser', 'emergency_contacts')">
                              Emergency Contacts
                            </a>
                          </li>
                        @endif
                        @if ($cur_share->business_wealth)
                          <li><a class="dropdown-item" href="#"
                              wire:click.prevent="showPage('organiser', 'business_wealth')">Business
                              Wealth</a></li>
                        @endif
                        @if ($cur_share->properties_wealth)
                          <li><a class="dropdown-item" href="#"
                              wire:click.prevent="showPage('organiser', 'properties_wealth')">Personal
                              Wealth</a></li>
                        @endif
                        @if ($cur_share->debt_expenses)
                          <li><a class="dropdown-item" href="#"
                              wire:click.prevent="showPage('organiser', 'debt_expenses')">Debt
                              & Expenses</a></li>
                        @endif
                        @if ($cur_share->social_media)
                          <li><a class="dropdown-item" href="#"
                              wire:click.prevent="showPage('organiser', 'social_media')">Social
                              Media</a></li>
                        @endif
                        @if ($cur_share->safe_keeping)
                          <li><a class="dropdown-item" href="#"
                              wire:click.prevent="showPage('organiser', 'safe_keeping')">Safekeeping</a>
                          </li>
                        @endif
                        @if ($cur_share->memories)
                          <li><a class="dropdown-item" href="#"
                              wire:click.prevent="showPage('organiser', 'memories')">Memories</a>
                          </li>
                        @endif
                        @if ($cur_share->funeral)
                          <li><a class="dropdown-item" href="#"
                              wire:click.prevent="showPage('organiser', 'funeral')">Funeral</a>
                          </li>
                        @endif
                      </ul>
                    </li>
                  @endif
                  @if ($cur_share->family_tree)
                    {{--
                                <li class="nav-item">
                                    <a class="nav-link @if ($cur_view == 'family_tree') active @endif" href="#" wire:click.prevent="showPage('family_tree', 'family_tree')">My Network</a>
                                </li>
                                --}}
                    <li class="nav-item dropdown">
                      <a class="nav-link dropdown-toggle @if ($cur_view == 'family_tree') active @endif"
                        href="#" id="navbarDropdown" role="button"
                        data-bs-toggle="dropdown" aria-expanded="false">
                        My Network <i class="bx bx-chevron-down"></i>
                      </a>
                      <ul class="dropdown-menu"
                        aria-labelledby="navbarDropdown">
                        <li><a class="dropdown-item" href="#"
                            wire:click.prevent="showPage('family_tree', 'family')">Family</a>
                        </li>
                        <li><a class="dropdown-item" href="#"
                            wire:click.prevent="showPage('family_tree', 'friends')">Friends</a>
                        </li>
                        <li><a class="dropdown-item" href="#"
                            wire:click.prevent="showPage('family_tree', 'professional')">Professional</a>
                        </li>
                      </ul>
                    </li>
                  @endif
                </ul>
              </div>
            </div>
          </nav>
          <div>
            <hr class="__hr">
          </div>
        </div>
      </div>
      <div class="__org-views">
        @if ($view == 'profile')
          @include('livewire.account.share.orgviews.profile')
        @elseif($view == 'personal')
          @include('livewire.account.share.orgviews.personal')
        @elseif($view == 'emergency_contacts')
          @include('livewire.account.share.orgviews.emergency_contacts')
        @elseif($view == 'business_wealth')
          @include('livewire.account.share.orgviews.business_wealth')
        @elseif($view == 'properties_wealth')
          @include('livewire.account.share.orgviews.properties_wealth')
        @elseif($view == 'debt_expenses')
          @include('livewire.account.share.orgviews.debt_expenses')
        @elseif($view == 'social_media')
          @include('livewire.account.share.orgviews.social_media')
        @elseif($view == 'safe_keeping')
          @include('livewire.account.share.orgviews.safe_keeping')
        @elseif($view == 'memories')
          @include('livewire.account.share.orgviews.memories')
        @elseif($view == 'funeral')
          @include('livewire.account.share.orgviews.funeral')
        @elseif($view == 'family' || $view == 'friends' || $view == 'professional')
          @include('livewire.account.share.orgviews.family_tree')
        @endif
      </div>
    @else
      <div class="col-md-12">
        <h3 style="color: var(--theme-blue)">Empty State</h3>
        {{-- <div class="mt-5 text-center"> --}}
        {{--   <h2 class="text-pink">Share Info</h2> --}}
        {{--   <p style="font-size: 16px"><b>View what others have shared with you --}}
        {{--       here.</b></p> --}}
        {{--   @if (Auth::user()->free_account == 1) --}}
        {{--     <p style="font-size: 16px">Upgrade your account to get full access --}}
        {{--       to everything WrapUp has to offer and share info with others.</p> --}}
        {{--     <div class="mt-3"> --}}
        {{--       <a href="{{ url('register') }}" class="btn btn-red">Upgrade --}}
        {{--         Account</a> --}}
        {{--     </div> --}}
        {{--   @else --}}
        {{--     <div class="mt-3"> --}}
        {{--       <p style="font-size: 16px">To view what information others have --}}
        {{--         shared with you or what you have shared with them, simply click --}}
        {{--         on a contact listed under the <b>'Shared Info With'</b> or --}}
        {{--         <b>'Shared With Me'</b> section. --}}
        {{--       </p> --}}
        {{--     </div> --}}
        {{--   @endif --}}
        {{-- </div> --}}
      </div>
    @endif
  </div>
  @if ($contact_id)
    <div class="col-md-12 __delete mt-auto">
      <div>
        <hr class="__hr">
      </div>
      <div class="d-grid justify-content-end mb-3 mt-3">
        <a href="#" class="btn btn-outline-two" data-bs-toggle="modal"
          data-bs-target="#shared-with-me-modal">
          <svg width="21" height="20" viewBox="0 0 21 20" fill="none"
            xmlns="http://www.w3.org/2000/svg">
            <path d="M13.9006 7.5V15.8333H7.23389V7.5H13.9006ZM12.6506
              2.5H8.48389L7.65055
              3.33333H4.73389V5H16.4006V3.33333H13.4839L12.6506 2.5ZM15.5672
              5.83333H5.56722V15.8333C5.56722 16.75 6.31722 17.5 7.23389
              17.5H13.9006C14.8172 17.5 15.5672 16.75 15.5672 15.8333V5.83333Z"
              fill="#CF3D60" />
          </svg>
          Delete Share
        </a>
      </div>
    </div>
  @endif
  <div class="modal" id="shared-with-me-modal" tabindex="-1"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <p><b>Warning:</b> By clicking the delete button, all information
                shared with you from this specific contact will be permanently
                removed from your profile and cannot be recovered or reinstated.
                They can however choose to share info with you at another time.
              </p>
              <div class="mt-3 text-center">
                <a href="#" class="btn btn-red"
                  wire:click.prevent="deleteShare">Delete</a>
                <a href="#" class="btn btn-outline-two"
                  data-bs-dismiss="modal">Cancel</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
