@push('styles')
<style>
    .info .accordion-item {
        background-color: #eff1f4;
    }
    .info .accordion-button {
        border: 1px solid #1c355e ; 
        background-color: #1c355e ; 
        color: #FFF;
    }
    .info .accordion-button::after {
        width: 10px;
        height: 10px;
        background: transparent;
        text-indent: -9999px;
        border-top: 2px solid #FFF;
        border-left: 2px solid #FFF;
        transition: all 250ms ease-in-out;
        text-decoration: none;
        color: transparent;
        transform: rotate(220deg);
    }
    .info .accordion-button::after {
        margin-left: auto;
        color: #FFF !important;
    }
    .info .accordion-button:not(.collapsed) {
        background-color: #FFF;
    }
    .info .accordion-item .accordion-body {
        background-color: #FFF;
    }
    .share-label{
        margin-left: 0.2rem;
    }
    .shar-nav .nav-item{
        font-weight: 700;
        margin-right: 20px;
    }
    .shar-nav .nav-item .nav-link{
        color: #1c355e;
    }
    .shar-nav .nav-item .nav-link.active{
        color: #ee0041;
    }
    .line-top{
        border-top: 1px solid #CCCCCC;
    }
</style>
@endpush
<div class="accordion info" id="accordionExample">
    <div class="accordion-item">
        <h2 class="accordion-header" id="headingOne">
            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                How it works
            </button>
        </h2>
        <div id="collapseOne" class="accordion-collapse collapse" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
            <div class="accordion-body">
                @if(Auth::user()->free_account == 1)
                <div class="mb-3">
                    <p>(This awesome feature is only available in our paid package. Want to unlock it? Upgrade your account <a href="{{ url('register') }}"><b class="text-blue">here</b></a>)</p>
                </div>
                @endif
                <p>Discover the convenience and flexibility of effortlessly sharing essential documents. Streamline access for family members, loved ones or professionals by using our 'Share Info' feature. With this functionality, you can securely share documents with anyone, including non-members.</p>
                <b>How it works</b>
                <ol>
                    <li>Add recipient's info.</li>
                    <li>Choose what you want to share with them from your MyAll Organiser.</li>
                    <li>The recipient will receive an email containing a link to the MyAll website, where they can proceed to register (with the option to choose between free or full access). After completing the registration, they will gain the ability to access the shared information.</li>
                </ol>
                <b>Features and benefits of using the MyAll  'Share info' function:</b>
                <ol>
                    <li>Available 24/7: Trusted contacts can access docs anytime for emergencies.</li>
                    <li>Convenient: Access shared info from any device, no need for physical copies.</li>
                    <li>Be ready for emergencies: Contacts can use critical details like medical records.</li>
                    <li>Accessible anywhere: Trusted contacts can get info even from far away.</li>
                    <li>Collaborate: Smoothly work together on finances, planning, docs.</li>
                    <li>Secure sharing: Strong encryption and control over access.</li>
                </ol>
                <p>Simplify with MyAll Life Organiser: Strengthen your support network, ensure info access anywhere.</p>
                {{--
                <p>Simplify access to important information and enhance communication with our new sharing function.</p>                                                   
                <p>With this feature, you can securely share information with anyone, including non-MyAll members.</p>                                                    
                <p>Experience the convenience and flexibility of easily sharing specific documents, insurance policies, and personal details.</p>                                                
                <p>Here's how it works:</p>
                <ol>                                                  
                    <li>Enter the recipient's name, email address, and ID number.</li>                                             
                    <li>Select the specific information you want to share from your MyAll Organiser. </li>                                                
                    <li>The recipient will receive an email containing a secure link. To access the shared information, the recipient must verify their identity by using their ID number.</li>
                </ol>
                --}}
            </div>
        </div>
    </div>
    {{--
    <div class="accordion-item">
        <h2 class="accordion-header" id="headingTwo">
            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                Benefits of Sharing your info
            </button>
        </h2>
        <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
            <div class="accordion-body">
                @if(Auth::user()->free_account == 1)
                <div class="mb-3">
                    <p>(This awesome feature is only available in our paid package. Want to unlock it? Upgrade your account <a href="{{ url('register') }}"><b class="text-blue">here</b></a>)</p>
                </div>
                @endif
                <p><b>24/7 Access:</b> Ensure your trusted contacts have continuous access to important documents, providing peace of mind during emergencies or time-sensitive matters.</p>
                <p><b>Convenience:</b> Access shared information from any device, eliminating the need for physical copies or proximity to saved documents.</p>
                <p><b>Emergency Preparedness:</b> Empower trusted contacts to act on your behalf during emergencies by quickly accessing critical details like medical records or emergency contacts.</p>
                <p><b>Remote Accessibility:</b> Extend your support network even when physically distant, allowing trusted contacts to access shared information from anywhere.</p>
                <p><b>Collaborative Planning:</b> Foster smoother collaboration among family members or trusted individuals for financial matters, estate planning, or organising important documents.</p>
                <p><b>Secure and Controlled Sharing:</b> Prioritise data security with robust encryption and industry-leading protocols. Maintain control by removing access or updating shared content at any time.</p>

                <p><i>Embrace the power of sharing and simplify your life with the MyAll Smart Life Organiser. Enhance your support network and ensure your trusted contacts always have essential information, regardless of their location.</i></p>
            </div>
        </div>
    </div>
    --}}
</div>