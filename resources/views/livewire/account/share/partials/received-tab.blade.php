{{-- @php --}}
{{--   @dump(get_defined_vars()); --}}
{{-- @endphp --}}
<div class="__header">
  <div class="__heading">
    <h3>Received</h3>
    <p>View information that others have shared with you.</p>
  </div>
</div>

<div class="__content">

  <div class="__shares">
    <h4>Contacts</h4>
    <div class="__share-wrapper">
      @foreach ($shared_with_me as $contact)
        <a href="{{ url('share-profile/view/' . $contact->user->id) }}">
          {{-- <div>{{ $contact }}</div> --}}
          <div
            class="__share {{ $contact->color }} d-flex @if ($contact_id == $contact->user->id) active @endif">

            <div class="__avatar">
              @if ($contact->contact)
                <img src="{{ asset('storage/' . $contact->user->profile_pic) }} "
                  alt="{{ $contact->user->name . ' ' . $contact->user->middle_name . ' ' . $contact->user->surname }}">
              @else
                {{-- TODO:RTR: Name does not go here. --}}
                {{ $contact->user->name . ' ' . $contact->user->middle_name . ' ' . $contact->user->surname }}
              @endif
            </div>

            <div class="__text">
              <div class="__name">
                {{ $contact->user->name . ' ' . $contact->user->middle_name . ' ' . $contact->user->surname }}
              </div>
            </div>
          </div>
        </a>
        {{-- <div>{{ $contact }}</div> --}}
        {{-- <div --}}
        {{--   class="share-link d-flex @if ($contact_id == $contact->user->id) active @endif"> --}}
        {{--   <a href="{{ url('share-profile/view/' . $contact->user->id) }}"><b --}}
        {{--       class="text-blue">{{ $contact->user->name . ' ' . $contact->user->middle_name . ' ' . $contact->user->surname }}</b></a> --}}
        {{-- </div> --}}
      @endforeach
    </div>
  </div>
  <div class="__details">

    @if ($view == 'form' || ($view == 'edit' && $activeTab != 'tabReceived'))
      <livewire:account.share.pages.form :id="$share_id" />
    @elseif($view == 'view')
      <livewire:account.share.pages.shared-with-me :id="$contact_id" />
    @endif

  </div>
</div>

{{-- <div class="card-box-style rounded p-3"> --}}
{{--   <div class="others-title organiser-description-medium mb-0"> --}}
{{--     <h3 class="text-pink">Shared With Me</h3> --}}
{{--   </div> --}}
{{--   <div class=""> --}}
{{--     <span class="text-muted text-blue"> --}}
{{--       Click on the connection to view shared information with you. --}}
{{--       The --}}
{{--       details will appear on the right side of your screen. --}}
{{--     </span> --}}
{{--   </div> --}}
{{--   <ul class="list-group"> --}}
{{--     @foreach ($shared_with_me as $contact) --}}
{{--       <div --}}
{{--         class="share-link d-flex @if ($contact_id == $contact->user->id) active @endif"> --}}
{{--         <a --}}
{{--           href="{{ url('share-profile/view/' . $contact->user->id) }}?tab=received"><b --}}
{{--             class="text-blue">{{ $contact->user->name . ' ' . $contact->user->middle_name . ' ' . $contact->user->surname }}</b></a> --}}
{{--       </div> --}}
{{--     @endforeach --}}
{{--   </ul> --}}
{{-- </div> --}}
