<div class="__header">
  {{-- @php --}}
  {{--   dump($shared_with); --}}
  {{-- @endphp --}}
  <div class="__heading">
    <h3>Shared</h3>
    <p>Manage who you share your info and what info is visible to them.</p>
  </div>
  <div class="__new-share">
    <a href="{{ url('share-profile/form') }}"
      class="btn btn-gradient btn-lg gradient-1">
      <svg width="20" height="20" viewBox="0 0 20 20" fill="none"
        xmlns="http://www.w3.org/2000/svg">
        <path d="M16.6668
            7.4987V4.9987H15.0002V7.4987H12.5002V9.16536H15.0002V11.6654H16.6668V9.16536H19.1668V7.4987H16.6668ZM7.50016
            9.9987C9.34183 9.9987 10.8335 8.50703 10.8335 6.66536C10.8335 4.8237
            9.34183 3.33203 7.50016 3.33203C5.6585 3.33203 4.16683 4.8237
            4.16683 6.66536C4.16683 8.50703 5.6585 9.9987 7.50016
            9.9987ZM7.50016 4.9987C8.41683 4.9987 9.16683 5.7487 9.16683
            6.66536C9.16683 7.58203 8.41683 8.33203 7.50016 8.33203C6.5835
            8.33203 5.8335 7.58203 5.8335 6.66536C5.8335 5.7487 6.5835 4.9987
            7.50016 4.9987ZM12.8252 12.132C11.4252 11.4154 9.60849 10.832
            7.50016 10.832C5.39183 10.832 3.57516 11.4154 2.17516 12.132C1.34183
            12.557 0.833496 13.4154 0.833496
            14.3487V16.6654H14.1668V14.3487C14.1668 13.4154 13.6585 12.557
            12.8252 12.132ZM12.5002 14.9987H2.50016V14.3487C2.50016 14.032
            2.66683 13.7487 2.9335 13.6154C3.92516 13.107 5.52516 12.4987
            7.50016 12.4987C9.47516 12.4987 11.0752 13.107 12.0668
            13.6154C12.3335 13.7487 12.5002 14.032 12.5002 14.3487V14.9987Z"
          fill="#CF3D60" />
      </svg>
      New Share</a>
  </div>
</div>

<div class="__content">
  <div class="__shares">
    <h4>Contacts</h4>
    <div class="__share-wrapper">
      @foreach ($shared_with as $share)
        <a href="{{ url('share-profile/edit/' . $share->id) }}">
          <div
            class="__share {{ $share->color }} d-flex @if ($share_id == $share->id) active @endif">
            <div class="__avatar">
              @if ($share->contact)
                @if ($share->contact->profile_pic)
                  <img
                    src="{{ asset('storage/' . $share->contact->profile_pic) }} "
                    alt="">
                @else
                  {{ getInitials($share->name) }}
                @endif
              @else
                {{ getInitials($share->name) }}
              @endif
            </div>
            <div class="__text">
              <div class="__name">
                <span>{{ $share->name }}</span>
                {{-- <div class="__badge" --}}
                {{--   style="background-color:{{ $colorScheme['fill'] }}; color:{{ $colorScheme['font'] }}"> --}}
                {{--   <svg width="13" height="13" viewBox="0 0 13 13" --}}
                {{--     fill="none" xmlns="http://www.w3.org/2000/svg"> --}}
                {{--     <path --}}
                {{--       d="M5.31775 2.75V3.75H8.61275L2.81775 9.545L3.52275 10.25L9.31775 4.455V7.75H10.3177V2.75H5.31775Z" --}}
                {{--       fill="{{ $colorScheme['font'] }}" /> --}}
                {{--   </svg> --}}
                {{--   <span>Shared</span> --}}
                {{-- </div> --}}
              </div>
              <div class="__relation">
                @if (!empty($share->family))
                  {{ $share->family }}
                @elseif (!empty($share->friends))
                  {{ $share->friends }}
                @elseif (!empty($share->professional))
                  {{ $share->professional }}
                @endif
              </div>
            </div>
          </div>
        </a>
      @endforeach
    </div>


  </div>

  <div class="__details">

    @if ($view == 'form' || $view == 'edit')
      <livewire:account.share.pages.form :id="$share_id" />
    @elseif($view == 'view')
      <livewire:account.share.pages.shared-with-me :id="$contact_id" />
    @endif

  </div>
</div>


{{-- <div class="card-box-style rounded p-3"> --}}
{{--   <div class="others-title organiser-description-medium d-flex"> --}}
{{--     <h3 class="text-pink">Shared info with</h3> --}}
{{--     <span class="ms-auto"> --}}
{{--       <a href="{{ url('share-profile/form') }}" --}}
{{--         class="btn btn-outline-one text-red">New Share</a> --}}
{{--     </span> --}}
{{--   </div> --}}
{{--   <div class=""> --}}
{{--     <p>Click on a name below to see the info you have shared with --}}
{{--       them.</p> --}}
{{--   </div> --}}
{{-- --}}
{{--   <div class="__share-wrapper"> --}}
{{--     @foreach ($shared_with as $share) --}}
{{--       <a --}}
{{--         href="{{ url('share-profile/edit/' . $share->id) }}?tab=shared"> --}}
{{--         <div --}}
{{--           class="__share d-flex @if ($share_id == $share->id) active @endif"> --}}
{{--           @php --}}
{{--             $colorScheme = getColorScheme( --}}
{{--                 $share->name, --}}
{{--                 $colorSchemes, --}}
{{--             ); --}}
{{--           @endphp --}}
{{--           <div class="__avatar" --}}
{{--             style="background-color:{{ $colorScheme['fill'] }}; color:{{ $colorScheme['font'] }}; border-color:{{ $colorScheme['border'] }}"> --}}
{{--             {{ getInitials($share->name) }} --}}
{{--           </div> --}}
{{--           <div class="__content"> --}}
{{--             <div class="__name" --}}
{{--               style="color:{{ $colorScheme['font'] }}"> --}}
{{--               <span>{{ $share->name }}</span> --}}
{{--               <div class="__badge" --}}
{{--                 style="background-color:{{ $colorScheme['fill'] }}; color:{{ $colorScheme['font'] }}"> --}}
{{--                 <svg width="13" height="13" --}}
{{--                   viewBox="0 0 13 13" fill="none" --}}
{{--                   xmlns="http://www.w3.org/2000/svg"> --}}
{{--                   <path --}}
{{--                     d="M5.31775 2.75V3.75H8.61275L2.81775 9.545L3.52275 10.25L9.31775 4.455V7.75H10.3177V2.75H5.31775Z" --}}
{{--                     fill="{{ $colorScheme['font'] }}" /> --}}
{{--                 </svg> --}}
{{--                 <span>Shared</span> --}}
{{--               </div> --}}
{{--             </div> --}}
{{--             <div class="__relation"> --}}
{{--               @if (!empty($share->family)) --}}
{{--                 {{ $share->family }} --}}
{{--               @elseif (!empty($share->friends)) --}}
{{--                 {{ $share->friends }} --}}
{{--               @elseif (!empty($share->professional)) --}}
{{--                 {{ $share->professional }} --}}
{{--               @endif --}}
{{--             </div> --}}
{{--           </div> --}}
{{--         </div> --}}
{{--       </a> --}}
{{--     @endforeach --}}
{{--   </div> --}}
{{-- </div> --}}

{{-- <div class="col-md-8 index-100 h-100"> --}}
{{--   <div class="card-box-style bg-navy-blue rounded"> --}}
{{--     <div class="row"> --}}
{{--       <div class="col-md-12 d-flex"> --}}
{{--         <h4 class="text-white">Share Info</h4> --}}
{{--         <span class="ms-auto"> --}}
{{--           <a href="#" data-bs-toggle="modal" --}}
{{--             data-bs-target="#infoModal"> --}}
{{--             <img src="{{ asset('img/info.png') }}"> --}}
{{--           </a> --}}
{{--         </span> --}}
{{--       </div> --}}
{{--     </div> --}}
{{--     <div class="row mx-1 mt-3"> --}}
{{--       <div class="col-md-12"> --}}
{{--         @if ($view == 'form' || $view == 'edit') --}}
{{--           <livewire:account.share.pages.form :id="$share_id" /> --}}
{{--         @elseif($view == 'view') --}}
{{--           <livewire:account.share.pages.shared-with-me --}}
{{--             :id="$contact_id" /> --}}
{{--         @endif --}}
{{--       </div> --}}
{{--     </div> --}}
{{--   </div> --}}
{{-- </div> --}}
