{{-- @php --}}
{{--   @dump(get_defined_vars()); --}}
{{-- @endphp --}}

<div class="myallMainAppInner">

  @push('styles')
    <style>
      .modal-backdrop {
        z-index: -1 !important;
      }

      .modal-content {
        margin: 2px auto;
        z-index: 5100 !important;
      }
    </style>
  @endpush

  {{-- <div class="row mb-5"> --}}

  <div class="col-md-12">
    <livewire:account.partials.my-search-bar />
  </div>

  {{-- <div class="something"> --}}
  {{-- NOTE: How to info box --}}
  {{-- @include('livewire.account.share.partials.info') --}}

  {{-- <h2>Shared</h2> --}}

  {{-- @dump($shared_with) --}}
  {{-- @foreach ($shared_with as $share) --}}

  {{--   <div> --}}
  {{--     <a href="{{ url('share-profile/edit/' . $share->id) }}"> --}}
  {{--       {{ $share->name }} </a> --}}
  {{--   </div> --}}

  {{-- @endforeach --}}

  {{-- <h2>Received</h2> --}}
  {{-- @dump($shared_with_me) --}}
  {{-- @foreach ($shared_with_me as $contact) --}}
  {{-- @dump($contact->user) --}}
  {{--   <div class="@if ($contact_id == $contact->user->id) active @endif"> --}}
  {{--     <a href="{{ url('share-profile/view/' . $contact->user->id) }}"> --}}
  {{--       {{ $contact->user->name . ' ' . $contact->user->middle_name . ' ' . $contact->user->surname }}</a> --}}
  {{--   </div> --}}
  {{-- @endforeach --}}

  {{-- <button href="{{ url('share-profile/form') }}" class=""> --}}
  {{--   Create New Share --}}
  {{-- </button> --}}


  {{-- NOTE: Below are views for editing and viewing the share. To be placed
      in a modal --}}
  {{-- <div class=""> --}}
  {{-- @if ($view == 'form' || $view == 'edit') --}}
  {{-- <livewire:account.share.pages.form :id="$share_id" /> --}}
  {{-- @endif --}}
  {{-- NOTE: This is the Shared with me view. --}}
  {{-- @if ($view == 'view') --}}
  {{--   <livewire:account.share.pages.shared-with-me :id="$contact_id" /> --}}
  {{-- @endif --}}
  {{-- </div> --}}
  {{-- ENDNOTE --}}
  {{-- </div> --}}

  {{-- OLD SECTION --}}

  {{-- @push('styles') --}}
  {{--   <style> --}}
  {{--     .tab-content { --}}
  {{--       display: none; --}}
  {{--       padding: 20px; --}}
  {{--       border: 1px solid #ccc; --}}
  {{--       border-radius: 20px; --}}
  {{--     } --}}
  {{-- --}}
  {{--     .tab-content.active { --}}
  {{--       display: block; --}}
  {{--     } --}}
  {{--   </style> --}}
  {{-- @endpush --}}

  <div class="myallContactsTabs">
    @if (Auth::user()->free_account == 0)
      <div class="__tab @if ($activeTab == 'tabContacts') active @endif"
        onclick="Livewire.emit('loadTabContent','tabContacts')">
        <svg width="17" height="16" viewBox="0 0 17 16" fill="none"
          xmlns="http://www.w3.org/2000/svg">
          <path d="M6.06779 9.16536C4.50779 9.16536 1.40112 9.94536 1.40112
          11.4987V12.6654H10.7345V11.4987C10.7345 9.94536 7.62779 9.16536
          6.06779 9.16536ZM2.96112 11.332C3.52112 10.9454 4.87446 10.4987
          6.06779 10.4987C7.26112 10.4987 8.61446 10.9454 9.17446
          11.332H2.96112ZM6.06779 7.9987C7.35446 7.9987 8.40112 6.95203 8.40112
          5.66536C8.40112 4.3787 7.35446 3.33203 6.06779 3.33203C4.78112 3.33203
          3.73446 4.3787 3.73446 5.66536C3.73446 6.95203 4.78112 7.9987 6.06779
          7.9987ZM6.06779 4.66536C6.62112 4.66536 7.06779 5.11203 7.06779
          5.66536C7.06779 6.2187 6.62112 6.66536 6.06779 6.66536C5.51446 6.66536
          5.06779 6.2187 5.06779 5.66536C5.06779 5.11203 5.51446 4.66536 6.06779
          4.66536ZM10.7611 9.20537C11.5345 9.76537 12.0678 10.512 12.0678
          11.4987V12.6654H14.7345V11.4987C14.7345 10.152 12.4011 9.38537 10.7611
          9.20537ZM10.0678 7.9987C11.3545 7.9987 12.4011 6.95203 12.4011
          5.66536C12.4011 4.3787 11.3545 3.33203 10.0678 3.33203C9.70779 3.33203
          9.37446 3.4187 9.06779 3.56536C9.48779 4.1587 9.73446 4.88536 9.73446
          5.66536C9.73446 6.44536 9.48779 7.17203 9.06779 7.76536C9.37446
          7.91203 9.70779 7.9987 10.0678 7.9987Z" fill="#040A5E" />
        </svg>
        <span>Contacts</span>
      </div>
      <div class="__tab @if ($activeTab == 'tabShared') active @endif"
        onclick="Livewire.emit('loadTabContent','tabShared')">
        <svg width="17" height="16" viewBox="0 0 17 16" fill="none"
          xmlns="http://www.w3.org/2000/svg">
          <path d="M6.40108 3V4.33333H10.7944L3.06775 12.06L4.00775 13L11.7344
          5.27333V9.66667H13.0677V3H6.40108Z" fill="#040A5E" />
        </svg>
        <span>Shared</span>
      </div>
    @endif
    <div class="__tab @if ($activeTab == 'tabReceived') active @endif"
      onclick="Livewire.emit('loadTabContent','tabReceived')">
      <svg width="17" height="16" viewBox="0 0 17 16" fill="none"
        xmlns="http://www.w3.org/2000/svg">
        <path d="M13.5677 3.94L12.6277 3L4.90108
          10.7267V6.33333H3.56775V13H10.2344V11.6667H5.84108L13.5677 3.94Z"
          fill="#040A5E" />
      </svg>
      <span>Received</span>
    </div>
  </div>


  <div class="myallContactsMainInner"
    @if ($activeTab == 'tabReceived' && Auth::user()->free_account == 1) style="border-radius:0px 20px 20px 20px"
    @elseif ($activeTab == 'tabReceived' || $activeTab == 'tabShared') style="border-radius:20px" @endif>

    @if (Auth::user()->free_account == 0)
      @if ($activeTab == 'tabContacts')
        <livewire:account.my-network.network :id="Auth::user()->id" />
      @endif
      @if ($activeTab == 'tabShared')
        @livewire('account.share.partials.shared-tab', ['shared_with' => $shared_with, 'view' => $view, 'share_id' => $share_id, 'contact_id' => $contact_id])
      @endif
    @endif

    @if ($activeTab == 'tabReceived')
      @livewire('account.share.partials.received-tab', ['shared_with_me' => $shared_with_me, 'view' => $view, 'share_id' => $share_id, 'contact_id' => $contact_id])
    @endif
  </div>


  {{--     </div> --}}
  {{--   </div> --}}
  {{-- </div> --}}
  {{-- END --}}
</div>

<div class="modal" id="infoModal" tabindex="-1"
  aria-labelledby="exampleModalLabel" aria-hidden="true">

  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-body">

        <div class="text-end">
          <button type="button" class="btn-close" data-bs-dismiss="modal"
            aria-label="Close">
          </button>
        </div>

        <div class="row">
          <div class="col-md-12">
            <div class="list-group">

              <a href="#" class="list-group-item list-group-item-action"
                wire:click.prevent='showSingleInfoImage("Navigating the Protection of Personal Information.jpg")'>
                Navigating the Protection of Personal Information
              </a>

              <a href="#" class="list-group-item list-group-item-action"
                wire:click.prevent='showSingleInfoImage("Sharing info with Executors upon users passing.jpg")'>
                Sharing info with Executors upon user's passing
              </a>

              <a href="#" class="list-group-item list-group-item-action"
                wire:click.prevent='showSingleInfoImage("Tips for creating strong passwords.jpg")'>
                Tips for creating strong passwords
              </a>

              <a href="#" class="list-group-item list-group-item-action"
                wire:click.prevent='showSingleInfoImage("WrapUp and Protection of Personal Information Act.jpg")'>
                MyAll and Protection of Personal Information Act
              </a>

              <a href="#" class="list-group-item list-group-item-action"
                wire:click.prevent='showSingleInfoImage("WrapUps Password Security.jpg")'>
                MyAll's Password Security
              </a>

              <a href="#" class="list-group-item list-group-item-action"
                wire:click.prevent='showSingleInfoImage("Your Childs Data and WrapUp Privacy and Protection.jpg")'>
                Your Child's Data and MyAll Privacy and Protection
              </a>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal" id="singleInfoImageModal" tabindex="-1" aria-labelledby=""
  aria-hidden="true">

  <div class="modal-dialog modal-dialog-centered modal-lg">
    <div class="modal-content">
      <div class="modal-body">

        <div class="text-end">
          <button type="button" class="btn-close" data-bs-dismiss="modal"
            aria-label="Close"></button>
        </div>

        <div class="row">
          <div class="col-md-12">
            <img src="{{ asset($single_info_image) }}" class="w-100">
          </div>
        </div>

      </div>
    </div>
  </div>

</div>

@push('scripts')
  <script>
    $(document).ready(function() {
      window.addEventListener('show-single-info-image', event => {
        $('.modal').modal('hide');
        $('#singleInfoImageModal').modal('show');
      });
    })


    document.addEventListener('DOMContentLoaded', () => {
      function initializeColorSelectors() {
        const colorSelectors = document.querySelectorAll('.__color-selector');
        const colorContainers = document.querySelectorAll(
          '.__contact-color-container');

        // Check if the elements exist
        if (colorSelectors.length === 0 || colorContainers.length === 0) {
          return;
        }

        colorSelectors.forEach((selector, index) => {
          const container = colorContainers[index];

          selector.addEventListener('mouseenter', () => {
            container.style.display = 'flex';
            adjustPosition(container);
          });

          selector.addEventListener('mouseleave', (event) => {
            if (!container.contains(event.relatedTarget)) {
              setTimeout(() => {
                container.style.display = 'none';
              }, 200);
            }
          });

          container.addEventListener('mouseleave', (event) => {
            if (!selector.contains(event.relatedTarget)) {
              setTimeout(() => {
                container.style.display = 'none';
              }, 200);
            }
          });
        });

        function adjustPosition(element) {
          const rect = element.getBoundingClientRect();
          const viewportWidth = window.innerWidth;
          const viewportHeight = window.innerHeight;

          if (rect.right > viewportWidth) {
            element.style.left =
              `calc(${viewportWidth - rect.width}px - 1rem)`;
          }
          if (rect.bottom > viewportHeight) {
            element.style.top = `${viewportHeight - rect.height}px`;
          }
          if (rect.left < 0) {
            element.style.left = '0px';
          }
          if (rect.top < 0) {
            element.style.top = '0px';
          }
        }
      }

      // Ensure the function is called initially if elements are already present
      if (document.querySelectorAll('.__color-selector').length > 0) {
        initializeColorSelectors();
      }

      // Re-initialize when Livewire loads the component
      document.addEventListener('livewire:load', () => {
        if (document.querySelectorAll('.__color-selector').length > 0) {
          initializeColorSelectors();
        }
      });

      // Re-initialize when Livewire updates the component
      document.addEventListener('livewire:update', () => {
        if (document.querySelectorAll('.__color-selector').length > 0) {
          initializeColorSelectors();
        }
      });
    });
  </script>
@endpush

</div>
