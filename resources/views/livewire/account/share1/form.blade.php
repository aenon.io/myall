<form wire:submit.prevent="shareProfile">
    <div class="row">
        <div class="col-md-4">
            <div class="mb-3">
                <label class="form-label">Name</label>
                <input type="text" class="form-control" name="name" wire:model.defer="name">
            </div>
        </div>
        <div class="col-md-4">
            <div class="mb-3">
                <label class="form-label">Email</label>
                <input type="email" class="form-control" name="email" wire:model.defer="email">
            </div>
        </div>
        <div class="col-md-4">
            <div class="mb-3">
                <label class="form-label">ID Number</label>
                <input type="text" class="form-control" name="id_number" wire:model.defer="id_number">
            </div>
        </div>
        <div class="col-md-12">
            <label class="form-label">Select what you would like to share</label>
            @foreach($sections AS $k=>$section)
                @if(is_array($section))
                <div class="mt-3">
                    <label class="form-label"><b>{{ ucwords(str_replace('_', ' ', $k)) }}</b></div>
                    <div class="row ms-2">
                        @foreach($section AS $kk=>$sec)
                            @if(!is_array($sec))
                            <div class="col-md-3">
                                <div class="form-check">
                                    <input class="form-check-input {{ $k }}" type="checkbox" value="{{ $sec }}" wire:model.defer="share_sections">
                                    <label class="form-check-label" for="flexCheckDefault">
                                        @if($sec == "safe_keeping")
                                            Safekeeping
                                        @elseif($sec == "debt_expenses")
                                            Debt & Expenses
                                        @else
                                            {{ ucwords(str_replace('_', ' ', $sec)) }}
                                        @endif
                                    </label>
                                </div>
                            </div>
                            @else
                            <div class="col-md-12 mt-3">
                                <div class="row">
                                    <div class="col-md-12">
                                        <hr />
                                    </div>
                                    <div class="col-md-12">
                                        <lable class="form-label">@if($kk == "connections") Emergency Contacts @elseif($kk == "properties_wealth") Personal Wealth @elseif($kk == "debt_expenses") Debt & Expenses @elseif($kk == "safe_keeping") Safekeeping @else {{ ucwords(str_replace('_', ' ', $kk)) }} @endif</label>
                                    </div>
                                    @foreach($sec AS $ss)
                                    <div class="col-md-3 ms-2">
                                        <div class="form-check">
                                            <input class="form-check-input {{ $kk }}" type="checkbox" value="{{ $ss }}" wire:model.defer="share_sections">
                                            <label class="form-check-label" for="flexCheckDefault">
                                                @if($ss == "accountant_bookkeeper")
                                                    Accountant / Bookkeeper
                                                @elseif($ss == "safe_keeping")
                                                    Safekeeping
                                                @elseif($ss == "children")
                                                    Children <a href="#" data-bs-toggle="modal" data-bs-target="#minorModal"><i class="fa fa-info-circle" style="color: #1c355e"></i></a>
                                                @else
                                                    {{ ucwords(str_replace('_', ' ', $ss)) }} 
                                                @endif
                                            </label>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                            @endif
                        @endforeach
                        
                    </div>
                </div>
                @else
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" value="{{ $section }}" wire:model.defer="share_sections">
                    <label class="form-check-label" for="flexCheckDefault">
                        <b>{{ ucwords(str_replace('_', ' ', $section)) }}</b>
                    </label>
                </div>
                @endif
            @endforeach
        </div>
        <div class="col-md-12 mt-3">
            <div class="mb-3">
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" value="yes" id="accept_share_terms">
                    <label class="form-check-label" for="accept_share_terms">
                        I have carefully reviewed the "WrapUp Protection of Personal Information Act Policy" and hereby authorise WrapUp to share and manage my data in accordance with the POPI Act, as well as following my instructions when sharing my selected personal and financial information with my selected connections. I am aware that if I choose not to accept the terms and conditions outlined in this policy and consent declaration, it is my responsibility to immediately notify WrapUp. Failure to do so will imply my acceptance of and agreement to the terms and conditions as stipulated in the policy.
                    </label>
                </div>
            </div>
        </div>
        <div class="col-md-12 mt-5">
            <div class="d-grid">
                <a href="#" type="button" wire:click.prevent="showModal" class="btn btn-red">SHARE</a>
            </div>
        </div>
        
    </div>
</form>