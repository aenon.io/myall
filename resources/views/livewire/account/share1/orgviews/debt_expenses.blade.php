<div class="row">
	<div class="col-md-12">
		<div class="accordion" id="accordionExps">
            <div class="accordion-item">
                <h2 class="accordion-header" id="heading-Expenses">
                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#expenses" aria-expanded="false" aria-controls="expenses">
                        Expenses
                    </button>
                </h2>
                <div id="expenses" class="accordion-collapse collapse" aria-labelledby="expenses-heading" data-bs-parent="#accordionExps">
                    <div class="accordion-body">
                        <div class="row">
                            <div class="col-md-12 overflow-auto">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Description</th>
                                            <th>Amount</th>
                                            <th>Payment Terms</th>
                                            <th>Payment Date</th>
                                            <th>File</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($cur_contact->expenses AS $exp)
                                        	@php
                                        	$k = $exp->type;
                                        	@endphp
                                            @if($k == 'Expenses' || $k == 'Communication' || $k == 'Donations' || $k == 'Education' || $k == 'Entertainment & Subscriptions' || $k == 'Household' || $k == 'Personal' || $k == 'Professional Services' || $k == 'Rent, Levies & Licenses' || $k == 'Other')
                                                <tr>
                                                    <td>{{ ucwords($exp->description) }}</td>
                                                    <td>{{ number_format($exp->amount, 2) }}</td>
                                                    <td>{{ $exp->term }}</td>
                                                    <td>{{ $exp->payment_date }}</td>
                                                    <td>
                                                        @if($exp->document)
                                                        <a href="{{ url('storage/'.$exp->document) }}" target="_blank"><i class="bx bxs-file"></i></a>
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endif
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="accordion-item">
                <h2 class="accordion-header" id="Loans-heading">
                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#loans" aria-expanded="false" aria-controls="loans">
                        Loans
                    </button>
                </h2>
                <div id="loans" class="accordion-collapse collapse" aria-labelledby="loans-heading" data-bs-parent="#accordionExps">
                    <div class="accordion-body">
                        <div class="row">
                            <div class="col-md-12 overflow-auto">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Description</th>
                                            <th>Amount</th>
                                            <th>Lender</th>
                                            <th>Payment Terms</th>
                                            <th>Payment Date</th>
                                            <th>File</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($cur_contact->expenses AS $exp)
                                        	@php
                                        	$k = $exp->type;
                                        	@endphp
                                            @if($k == 'loan')
                                                <tr>
                                                    <td>{{ ucwords($exp->description) }}</td>
                                                    <td>{{ number_format($exp->amount, 2) }}</td>
                                                    <td>{{ $exp->lender }}</td>
                                                    <td>{{ $exp->term }}</td>
                                                    <td>{{ $exp->payment_date }}</td>
                                                    <td>
                                                        @if($exp->document)
                                                            <a href="{{ url('storage/'.$exp->document) }}" target="_blank"><i class="bx bxs-file"></i></a>
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endif
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="accordion-item">
                <h2 class="accordion-header" id="insurance-heading">
                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#insurance" aria-expanded="false" aria-controls="insurance">
                        Insurance
                    </button>
                </h2>
                <div id="insurance" class="accordion-collapse collapse" aria-labelledby="insurance-heading" data-bs-parent="#accordionExps">
                    <div class="accordion-body">
                        <table class="table">
				            <thead>
				                <tr>
				                    <th>Type</th>
				                    <th>Insurer</th>
				                    <th>Amount Payable</th>
				                    <th>Payment Terms</th>
				                    <th>Payment Date</th>
				                    <th>File</th>
				                </tr>
				            </thead>
				            <tbody>
				                @foreach($cur_contact->insurances AS $ins)
				                <tr>
				                    <td>{{ $ins->insurance_type }}</td>
				                    <td>{{ $ins->insurer }}</td>
				                    <td>@if($ins->outstanding_debt) {{ number_format($ins->outstanding_debt,2) }} @else 0.00 @endif</td>
				                    <td>{{ $ins->payment_terms }}</td>
				                    <td>{{ $ins->payment_date }}</td>
				                    <!--<td>{{ $ins->outstanding_debt }}</td>-->
				                    <td>
				                        @if($ins->file)
				                        <a href="{{ url('storage/'.$ins->file) }}" target="_blank"><i class="bx bxs-file"></i></a>
				                        @endif
				                    </td>
				                </tr>
				                @endforeach
				            </tbody>
				        </table>
                    </div>
                </div>
            </div>
            <div class="accordion-item">
                <h2 class="accordion-header" id="heading-Ohter">
                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#other" aria-expanded="false" aria-controls="other">
                        Debt
                    </button>
                </h2>
                <div id="other" class="accordion-collapse collapse" aria-labelledby="other-heading" data-bs-parent="#accordionExps">
                    <div class="accordion-body">
                        <div class="row">
                            <div class="col-md-12 overflow-auto">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Description</th>
                                            <th class="text-end">Amount</th>
                                            <th>Payment Date</th>
                                            <th>Payment Term</th>
                                            <th>File</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($cur_contact->expenses AS $exp)
                                        	@php
                                        	$k = $exp->type;
                                        	@endphp
                                            @if($k == 'debt')
                                                <tr>
                                                    <td>{{ ucwords($exp->description) }}</td>
                                                    <td class="text-end">{{ number_format($exp->amount, 2) }}</td>
                                                    <td>{{ $exp->payment_date }}</td>
                                                    <td>{{ $exp->term }}</td>
                                                    <td>
                                                        @if($exp->document)
                                                            <a href="{{ url('storage/'.$exp->Document) }}" target="_blank"><i class="bx bxs-file"></i></a>
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endif
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</div>
</div>