<div class="row">
	<div class="col-md-12">
		<div class="accordion" id="accordionContacts">
			@foreach($cur_contact->groupedContacts() AS $key=>$cnt_tp)
			<div class="accordion-item">
				<h2 class="accordion-header" id="heading_{{ $key }}">
					<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse_{{ $key }}" aria-expanded="false" aria-controls="collapse_{{ $key }}">
						{{ ucwords($key) }}
					</button>
				</h2>
				<div id="collapse_{{ $key }}" class="accordion-collapse collapse" aria-labelledby="heading_{{ $key }}" data-bs-parent="#accordionContacts">
					<div class="accordion-body">
						<table class="table">
							<thead>
								<tr>
									<th>Name</th>
									<th>Relation</th>
									<th>Email</th>
									<th>Contact Number</th>
								</tr>
							</thead>
							<tbody>
								@foreach($cnt_tp AS $cnt)
								<tr>
									<td>{{ $cnt->name.' '.$cnt->surname }}</td>
									<td>{{ $cnt->relation }}</td>
									<td>{{ $cnt->email }}</td>
									<td>{{ $cnt->contact_number }}</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
			@endforeach
		</div>
	</div>
</div>