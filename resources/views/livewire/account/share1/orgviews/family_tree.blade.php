<div class="row">
	<div class="col-md-12">
		
	</div>
</div>
@push('scripts')
<script>
	$(document).ready(function(){
		//loadTree();
	});
	
	function loadTree(){
        FamilyTree.templates.hugo = Object.assign({}, FamilyTree.templates.base);
        FamilyTree.templates.hugo.defs = 
        `<clipPath id="hugo_img_0">
            <circle id="hugo_img_0_stroke" stroke-width="2" stroke="#fff" x="90" y="-5" rx="25" ry="50%" width="70" height="70"></circle>
        </clipPath>
        <linearGradient id="hugo_grad_female" x1="0%" y1="0%" x2="100%" y2="0%">
            <stop offset="0%" style="stop-color:#FF8024;stop-opacity:1" />
            <stop offset="100%" style="stop-color:#FF46A3;stop-opacity:1" />
        </linearGradient>
        <linearGradient id="hugo_grad_male" x1="0%" y1="0%" x2="100%" y2="0%">
            <stop offset="0%" style="stop-color:#00D3A5;stop-opacity:1" />
            <stop offset="100%" style="stop-color:#00A7D4;stop-opacity:1" />
        </linearGradient>
        <linearGradient id="hugo_grad" x1="0%" y1="0%" x2="100%" y2="0%">
            <stop offset="0%" style="stop-color:#ffd292;stop-opacity:1" />
            <stop offset="100%" style="stop-color:#ffd292;stop-opacity:1" />
        </linearGradient>
        <g id="hugo_up">
            <circle cx="12" cy="12" r="15" fill="transparent"></circle>
            ${FamilyTree.icon.ft(24,24,'#fff', 0, 0)}
        </g>
        <g id="hugo_node_menu" style="cursor:pointer;">
            <rect x="0" y="0" fill="transparent" width="22" height="22"></rect>
            <circle cx="11" cy="4" r="2" fill="#ffffff"></circle><circle cx="11" cy="11" r="2" fill="#ffffff"></circle>
            <circle cx="11" cy="18" r="2" fill="#ffffff"></circle>
        </g>
        <style>
            .{randId} .bft-edit-form-header{
                background: linear-gradient(90deg, #D0D0D0 0%, #909090 100%);
            }
            .{randId}.male .bft-edit-form-header{
                background: linear-gradient(90deg, #00D3A5 0%, #00A7D4 100%);
            }
            .{randId}.female .bft-edit-form-header{
                background: linear-gradient(90deg, #FF8024 0%, #FF46A3 100%);
            }  
            .{randId} .bft-img-button{
                background-color: #909090;
            }      
            .{randId} .bft-img-button:hover{
                background-color: #D0D0D0;
            }
            .{randId}.male .bft-img-button{
                background-color: #00A7D4;
            }      
            .{randId}.male .bft-img-button:hover{
                background-color: #00D3A5;
            }
            .{randId}.female .bft-img-button{
                background-color: #FF46A3;
            }      
            .{randId}.female .bft-img-button:hover{
                background-color: #FF8024;
            }
        </style>`;
        FamilyTree.templates.hugo.img_0 = '<clipPath id="ulaImg">'
        + '<circle cx="40" cy="60" r="30" fill="white"></circle>'
        + '</clipPath>'
        + '<image preserveAspectRatio="xMidYMid slice" clip-path="url(#ulaImg)" xlink:href="{val}" x="10" y="30" width="60" height="60">'
        + '</image>';
        FamilyTree.templates.hugo.field_0 = '<text ' + FamilyTree.attr.width + ' ="230" style="font-size: 18px;font-weight:bold;"  fill="#1c355e" x="80" y="43" text-anchor="left">{val}</text>';
        FamilyTree.templates.hugo.field_1 = '<text ' + FamilyTree.attr.width + ' ="230" style="font-size: 18px;font-weight:bold;"  fill="#1c355e" x="80" y="63" text-anchor="left">{val}</text>';
        FamilyTree.templates.hugo.field_2 = '<text ' + FamilyTree.attr.width + ' ="230" style="font-size: 16px;font-weight:bold;" fill="#1c355e" x="80" y="83" text-anchor="left">{val}</text>';
        FamilyTree.templates.hugo.nodeMenuButton = `<use x="225" y="10" ${FamilyTree.attr.control_node_menu_id}="{id}" xlink:href="#hugo_node_menu" />`; 
        
        FamilyTree.templates.hugo.node = '<rect x="0" y="0" height="{h}" width="{w}" stroke-width="3" fill="#b1cfff" stroke="#b1cfff" rx="40" ry="40"></rect>';
        FamilyTree.templates.hugo.defs = '<filter id="shadow" x="-20%" y="-20%" width="150%" height="150%"><feOffset result="offOut" in="SourceAlpha" dx="2" dy="2" /><feGaussianBlur result="blurOut" in="offOut" stdDeviation="2" /><feBlend in="SourceGraphic" in2="blurOut" mode="normal" /></filter>';

        FamilyTree.templates.hugo_blue = Object.assign({}, FamilyTree.templates.hugo);
        FamilyTree.templates.hugo_blue.node = '<rect x="0" y="0" height="{h}" width="{w}" stroke-width="3" fill="#add8e6" stroke="#add8e6" rx="40" ry="40" filter="url(#shadow)"></rect>';
        FamilyTree.templates.hugo_blue.defs = '<filter id="shadow" x="-20%" y="-20%" width="150%" height="150%"><feOffset result="offOut" in="SourceAlpha" dx="2" dy="2" /><feGaussianBlur result="blurOut" in="offOut" stdDeviation="2" /><feBlend in="SourceGraphic" in2="blurOut" mode="normal" /></filter>';

        FamilyTree.templates.hugo_pink = Object.assign({}, FamilyTree.templates.hugo);
        FamilyTree.templates.hugo_pink.node = '<rect x="0" y="0" height="{h}" width="{w}" stroke-width="3" fill="#ffb1c9" stroke="#ffb1c9" rx="40" ry="40" filter="url(#shadow)"></rect>';
        FamilyTree.templates.hugo_pink.defs = '<filter id="shadow" x="-20%" y="-20%" width="150%" height="150%"><feOffset result="offOut" in="SourceAlpha" dx="2" dy="2" /><feGaussianBlur result="blurOut" in="offOut" stdDeviation="2" /><feBlend in="SourceGraphic" in2="blurOut" mode="normal" /></filter>';

        FamilyTree.templates.hugo_orange = Object.assign({}, FamilyTree.templates.hugo);
        FamilyTree.templates.hugo_orange.node = '<rect x="0" y="0" height="{h}" width="{w}" stroke-width="3" fill="#ffd38e" stroke="#ffd38e" rx="40" ry="40" filter="url(#shadow)"></rect>';
        FamilyTree.templates.hugo_orange.defs = '<filter id="shadow" x="-20%" y="-20%" width="150%" height="150%"><feOffset result="offOut" in="SourceAlpha" dx="2" dy="2" /><feGaussianBlur result="blurOut" in="offOut" stdDeviation="2" /><feBlend in="SourceGraphic" in2="blurOut" mode="normal" /></filter>';

        FamilyTree.templates.hugo_green = Object.assign({}, FamilyTree.templates.hugo);
        FamilyTree.templates.hugo_green.node = '<rect x="0" y="0" height="{h}" width="{w}" stroke-width="3" fill="#c7e599" stroke="#c7e599" rx="40" ry="40" filter="url(#shadow)"></rect>';
        FamilyTree.templates.hugo_green.defs = '<filter id="shadow" x="-20%" y="-20%" width="150%" height="150%"><feOffset result="offOut" in="SourceAlpha" dx="2" dy="2" /><feGaussianBlur result="blurOut" in="offOut" stdDeviation="2" /><feBlend in="SourceGraphic" in2="blurOut" mode="normal" /></filter>';

        FamilyTree.templates.hugo_yellow = Object.assign({}, FamilyTree.templates.hugo);
        FamilyTree.templates.hugo_yellow.node = '<rect x="0" y="0" height="{h}" width="{w}" stroke-width="3" fill="#fce571" stroke="#fce571" rx="40" ry="40" filter="url(#shadow)"></rect>';
        FamilyTree.templates.hugo_yellow.defs = '<filter id="shadow" x="-20%" y="-20%" width="150%" height="150%"><feOffset result="offOut" in="SourceAlpha" dx="2" dy="2" /><feGaussianBlur result="blurOut" in="offOut" stdDeviation="2" /><feBlend in="SourceGraphic" in2="blurOut" mode="normal" /></filter>';

        var tree_json = $.parseJSON('{!! $tree !!}');
        //console.log(tree_json);

        var family = new FamilyTree(document.getElementById("tree"), {
            template: "hugo",
            scaleInitial: FamilyTree.match.boundary,
            orientation: FamilyTree.orientation.left,
            showXScroll: false,
            showYScroll: true,
            enableSearch: false,
            interactive: true,
            partnerNodeSeparation: 100,
            mouseScrool: FamilyTree.action.ctrlZoom,
            nodeBinding: {
                field_0: "name",
                field_1: "surname",
                field_2: "relation",
                img_0: "img",
            },
            editForm: {
                buttons: {
                    pdf: null,
                    share: null,
                    delete: {
                        icon: FamilyTree.icon.remove(24,24,'#fff'),
                        text: "Remove",
                        hideIfDetailsMode: false,
                    }
                },
                /*
                generateElementsFromFields: false,
                elements: [
                    { type: 'textbox', label: 'First Name', binding: 'Name'},
                    { type: 'textbox', label: 'Surname Name', binding: 'Name'},
                    { type: 'textbox', label: 'Photo Url', binding: 'ImgUrl', btn: 'Upload'}        
                ]
                */
            },    
            tags: {
                Orange: {
                    template: "hugo_orange"
                },
                Green: {
                    template: "hugo_green"
                },
                Yellow: {
                    template: "hugo_yellow"
                },
                Blue: {
                    template: "hugo_blue"
                },
                Pink: {
                    template: "hugo_pink"
                }
            },
            nodes: tree_json
            // Example of a node with color
            //nodes: [{ id: 4, pids: [1],  name: "Gogo Alfred", relation: "Grand Mother", gender: "female", img: "{{ asset('../img/family-treeFamily Tree ICON.png') }}", tags: ["hugo_pink"] }]
            
        });
        
        /*
        family.on('click', function(sender, args){
            var id = args.node.id;
            Livewire.emit('showForm', id);
            return false;
        });

        family.editUI.on('button-click', function (sender, args) {
            var data = family.get(args.nodeId);
            if (args.name == 'delete') {
                Livewire.emit('delete-node', data.id);
            }
            if(args.name == "edit"){
                $('.bft-edit-form').hide();
                Livewire.emit('show-edit-node', data.id);
                return false;
            }
        });
        */
    }
</script>
@endpush