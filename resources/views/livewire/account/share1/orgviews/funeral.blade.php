<div class="row">
	<div class="col-md-12">
		<div class="accordion" id="accordionFuneral">
			<div class="accordion-item">
				<h2 class="accordion-header" id="headingPolicy">
					<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapsePolicy" aria-expanded="false" aria-controls="collapsePolicy">
						Funeral Policies
					</button>
				</h2>
				<div id="collapsePolicy" class="accordion-collapse collapse" aria-labelledby="headingPolicy" data-bs-parent="#accordionFuneral">
					<div class="accordion-body">
						@if($cur_contact->funeralCovers->count() > 0)
							@foreach($cur_contact->funeralCovers AS $cover)
								<div class="mb-3">
									<ul class="list-group">
										<li class="list-group-item d-flex">
											<span>Company</span>
											<b class="ms-auto">{{ $cover->company }}</b>
										</li>
										<li class="list-group-item d-flex">
											<span>Policy Number</span>
											<b class="ms-auto">{{ $cover->policy_number }}</b>
										</li>
										<li class="list-group-item d-flex">
											<span>Policy Holder</span>
											<b class="ms-auto">{{ $cover->policy_holder }}</b>
										</li>
										<li class="list-group-item d-flex">
											<span>Cover Amount</span>
											<b class="ms-auto">{{ $cover->coverage_amount }}</b>
										</li>
										@if($cover->dependants->count() > 0)
										<li class="list-group-item">
											<span>Beneficiaries</span>
											@foreach($cover->dependants AS $dpnd)
											<div class="d-flex">
												<span>Name</span>
												<b class="ms-auto">{{ $dpnd->name.' '.$dpnd->surname }}</b>
											</div>
											<div class="d-flex">
												<span>ID Number</span>
												<b class="ms-auto">{{ $dpnd->id_number }}</b>
											</div>
											<div class="d-flex">
												<span>Percentage</span>
												<b class="ms-auto">{{ $dpnd->percentage }}%</b>
											</div>
											<div class="mb-3">
												<hr />
											</div>
											@endforeach
										</li>
										@endif
									</ul>
								</div>
							@endforeach
						@endif
					</div>
				</div>
			</div>
			<div class="accordion-item">
				<h2 class="accordion-header" id="headingFnrl">
					<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseFnrl" aria-expanded="false" aria-controls="collapseFnrl">
						Funeral Details
					</button>
				</h2>
				<div id="collapseFnrl" class="accordion-collapse collapse" aria-labelledby="headingFnrl" data-bs-parent="#accordionFuneral">
					<div class="accordion-body">
						@if($cur_contact->funeral)
						<ul class="list-group">
							<li class="list-group-item d-flex">
								<span>Memorial Service</span>
								<b class="ms-auto">{{ $cur_contact->funeral->memorials_service }}</b>
							</li>
							<li class="list-group-item d-flex">
								<span>Cremation Details</span>
								<b class="ms-auto">{{ $cur_contact->funeral->cremation_details }}</b>
							</li>
							<li class="list-group-item d-flex">
								<span>Parlor Service</span>
								<b class="ms-auto">{{ $cur_contact->funeral->parlor_details }}</b>
							</li>
							<li class="list-group-item d-flex">
								<span>Requests</span>
								<b class="ms-auto">{{ $cur_contact->funeral->requests }}</b>
							</li>
							<li class="list-group-item">
								<b>Obituary</b>
								<div>{{ $cur_contact->funeral->obituary }}</div>
							</li>
						</ul>
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>
</div>