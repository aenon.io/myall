<div class="row">
    <div class="col-md-12">
        <div class="accordion" id="accordion_personal">
            @if($cur_share->personal)
            <div class="accordion-item">
                <h2 class="accordion-header" id="headingMe">
                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseMe" aria-expanded="false" aria-controls="collapseMe">
                        About {{ $cur_contact->name }}
                    </button>
                </h2>
                <div id="collapseMe" class="accordion-collapse collapse" aria-labelledby="headingMe" data-bs-parent="#accordion_personal">
                    <div class="accordion-body">    
                        <ul class="list-group">
                            <li class="list-group-item d-flex">
                                <span>Name</span>
                                <b class="ms-auto">{{ $cur_contact->name.' '.$cur_contact->middle_name.' '.$cur_contact->surname }}</b>
                            </li>
                            <li class="list-group-item d-flex">
                                <span>Date of Birth</span>
                                <b class="ms-auto">{{ $cur_contact->date_of_birth }}</b>
                            </li>
                            <li class="list-group-item d-flex">
                                <span>ID Number</span>
                                <b class="ms-auto">{{ $cur_contact->id_number }}</b>
                            </li>
                            @if($cur_contact->contact_number)
                            <li class="list-group-item d-flex">
                                <span>Contact Number</span>
                                <b class="ms-auto">{{ $cur_contact->contact_number }}</b>
                            </li>
                            @endif
                            <li class="list-group-item d-flex">
                                <span>Email</span>
                                <b class="ms-auto">{{ $cur_contact->email }}</b>
                            </li>
                            <li class="list-group-item d-flex">
                                <span>Gender</span>
                                <b class="ms-auto">{{ $cur_contact->gender }}</b>
                            </li>
                            <li class="list-group-item d-flex">
                                <span>Ethnicity</span>
                                <b class="ms-auto">{{ $cur_contact->ethnicity }}</b>
                            </li>
                            <li class="list-group-item">
                                <span>Address</span><br />
                                <b class="ms-auto">{{ $cur_contact->street_address }}</b><br />
                                <b class="ms-auto">{{ $cur_contact->city }}</b><br />
                                @if($cur_contact->postal_code)
                                <b class="ms-auto">{{ $cur_contact->postal_code }}</b><br />
                                @endif
                                <b class="ms-auto">{{ $cur_contact->province }}</b>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            @endif
            <div class="accordion-item">
                <h2 class="accordion-header" id="headingEdu">
                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseEdu" aria-expanded="false" aria-controls="collapseEdu">
                        Education
                    </button>
                </h2>
                <div id="collapseEdu" class="accordion-collapse collapse" aria-labelledby="headingEdu" data-bs-parent="#accordion_personal">
                    <div class="accordion-body">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Qualification</th>
                                    <th>Year</th>
                                    <th class="text-end">Certificate</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($cur_contact->education AS $edu)
                                <tr>
                                    <td>{{ $edu->qualification }}</td>
                                    <td>{{ $edu->year }}</td>
                                    <td class="text-end">
                                        @if($edu->certificate)
                                            <a href="{{ url('storage/'.$edu->certificate) }}">View</a>
                                        @endif
                                    </td>
                                </tr>
                                @if($edu->comment)
                                <tr class="table-secondary">
                                    <td colspan="3">{{ $edu->comment }}</td>
                                </tr>
                                @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            @if($cur_share->spouse)
            <div class="accordion-item">
                <h2 class="accordion-header" id="headingSpouse">
                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseSpouse" aria-expanded="false" aria-controls="collapseSpouse">
                        Proof of Marriage / Spouse / Life Partner
                    </button>
                </h2>
                <div id="collapseSpouse" class="accordion-collapse collapse" aria-labelledby="headingSpouse" data-bs-parent="#accordion_personal">
                    <div class="accordion-body">
                        @foreach($cur_contact->spouses AS $spouse)
                        <div class="mb-3">
                            <ul class="list-group">
                                <li class="list-group-item d-flex">
                                    <span>Martial Status</span>
                                    <b class="ms-auto">{{ $spouse->marital_status }}</b>
                                </li>
                                <li class="list-group-item d-flex">
                                    <span>Name</span>
                                    <b class="ms-auto">{{ $spouse->spouse_name.' '.$spouse->spouse_middle_name.' '.$spouse->spouse_surname }}</b>
                                </li>
                                <li class="list-group-item d-flex">
                                    <span>ID Number</span>
                                    <b class="ms-auto">{{ $spouse->spouse_id_number }}</b>
                                </li>
                                @if($spouse->marriage_certificate)
                                <li class="list-group-item d-flex">
                                    <span>Marriage Certificate</span>
                                    <b class="ms-auto"><a href="{{ url('storage/'.$spouse->marriage_certificate) }}">View</a></b>
                                </li>
                                @endif
                                @if($spouse->antenuptial_contract)
                                <li class="list-group-item d-flex">
                                    <span>Antenuptial Contract</span>
                                    <b class="ms-auto"><a href="{{ url('storage/'.$spouse->antenuptial_contract) }}">View</a></b>
                                </li>
                                @endif
                                @if($spouse->date_of_death)
                                <li class="list-group-item d-flex">
                                    <span>Date Of Death</span>
                                    <b class="ms-auto">{{ $spouse->date_of_death }}</b>
                                </li>
                                @endif
                                @if($spouse->masters_office)
                                <li class="list-group-item d-flex">
                                    <span>Masters Office</span>
                                    <b class="ms-auto">{{ $spouse->masters_office }}</b>
                                </li>
                                @endif
                                @if($spouse->estate_number)
                                <li class="list-group-item d-flex">
                                    <span>Estate Number</span>
                                    <b class="ms-auto">{{ $spouse->estate_number }}</b>
                                </li>
                                @endif
                                @if($spouse->attorney_auditor_trust_contact_details)
                                <li class="list-group-item d-flex">
                                    <span>Attorney Auditor Trust Contact Details</span>
                                    <b class="ms-auto">{{ $spouse->attorney_auditor_trust_contact_details }}</b>
                                </li>
                                @endif
                                @if($spouse->estate_wrapping_up_company)
                                <li class="list-group-item d-flex">
                                    <span>Estate Wrapping Up Company</span>
                                    <b class="ms-auto">{{ $spouse->estate_wrapping_up_company }}</b>
                                </li>
                                @endif
                                @if($spouse->dweling_property)
                                <li class="list-group-item d-flex">
                                    <span>Dweling Property</span>
                                    <b class="ms-auto">{{ $spouse->dweling_property }}</b>
                                </li>
                                @endif
                                @if($spouse->usufruct)
                                <li class="list-group-item d-flex">
                                    <span>Usufruct</span>
                                    <b class="ms-auto">{{ $spouse->usufruct }}</b>
                                </li>
                                @endif
                                @if($spouse->death_certificate)
                                <li class="list-group-item d-flex">
                                    <span>Death Certificate</span>
                                    <b class="ms-auto"><a href="{{ url('storage/'.$spouse->death_certificate) }}">View</a></b>
                                </li>
                                @endif
                                @if($spouse->will)
                                <li class="list-group-item d-flex">
                                    <span>Will</span>
                                    <b class="ms-auto"><a href="{{ url('storage/'.$spouse->will) }}">View</a></b>
                                </li>
                                @endif
                                @if($spouse->date_of_divorce)
                                <li class="list-group-item d-flex">
                                    <span>Date Of Divorce</span>
                                    <b class="ms-auto">{{ $spouse->date_of_divorce }}</b>
                                </li>
                                @endif
                                @if($spouse->court_order_and_agreement)
                                <li class="list-group-item d-flex">
                                    <span>Court Order And Agreement</span>
                                    <b class="ms-auto"><a href="{{ url('storage/'.$spouse->court_order_and_agreement) }}">View</a></b>
                                </li>
                                @endif
                                @if($spouse->maintenance_order)
                                <li class="list-group-item d-flex">
                                    <span>Maintenance Order</span>
                                    <b class="ms-auto">{{ $spouse->maintenance_order }}</b>
                                </li>
                                @endif
                                @if($spouse->proof_of_maintenance_order)
                                <li class="list-group-item d-flex">
                                    <span>Proof Of Maintenance Order</span>
                                    <b class="ms-auto"><a href="{{ url('storage/'.$spouse->proof_of_maintenance_order) }}">View</a></b>
                                </li>
                                @endif
                                @if($spouse->specific_agreements_description)
                                <li class="list-group-item d-flex">
                                    <span>Specific Agreements Description</span>
                                    <b class="ms-auto">{{ $spouse->specific_agreements_description }}</b>
                                </li>
                                @endif
                                @if($spouse->specific_agreements_file)
                                <li class="list-group-item d-flex">
                                    <span>Specific Agreements</span>
                                    <b class="ms-auto"><a href="{{ url('storage/'.$spouse->specific_agreements_file) }}"></b>
                                </li>
                                @endif
                                @if($spouse->comunity_marriage)
                                <li class="list-group-item d-flex">
                                    <span>In Community Marriage</span>
                                    <b class="ms-auto">{{ $spouse->comunity_marriage }}</b>
                                </li>
                                @endif
                                @if($spouse->accrual)
                                <li class="list-group-item d-flex">
                                    <span>With Accrual</span>
                                    <b class="ms-auto">{{ $spouse->accrual }}</b>
                                </li>
                                @endif
                                @if($spouse->executor)
                                <li class="list-group-item d-flex">
                                    <span>Executor</span>
                                    <b class="ms-auto">{{ $spouse->executor }}</b>
                                </li>
                                @endif
                                @if($spouse->domestic_agreement)
                                <li class="list-group-item d-flex">
                                    <span>Domestic Agreement</span>
                                    <b class="ms-auto"><a href="{{ url('storage/'.$spouse->domestic_agreement) }}">View</a></b>
                                </li>
                                @endif
                                @if($spouse->divorce_agreement)
                                <li class="list-group-item d-flex">
                                    <span>Divorce Agreement</span>
                                    <b class="ms-auto"><a href="{{ url('storage/'.$spouse->divorce_agreement) }}">View</a></b>
                                </li>
                                @endif
                            </ul>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
            @endif
            @if($cur_share->work)
            <div class="accordion-item">
                <h2 class="accordion-header" id="headingWork">
                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseWork" aria-expanded="false" aria-controls="collapseWork">
                        Current Place of Work
                    </button>
                </h2>
                <div id="collapseWork" class="accordion-collapse collapse" aria-labelledby="headingWork" data-bs-parent="#accordion_personal">
                    <div class="accordion-body">
                        <div class="mb-3">
                            @if($cur_contact->work)
                            <ul class="list-group">
                                <li class="list-group-item d-flex">
                                    <span>Name Of Employer</span>
                                    <b class="ms-auto">{{ $cur_contact->work->employer_name }}</b>
                                </li>
                                <li class="list-group-item d-flex">
                                    <span>Salary Number</span>
                                    <b class="ms-auto">{{ $cur_contact->work->salary_number }}</b>
                                </li>
                                <li class="list-group-item d-flex">
                                    <span>Address</span>
                                    <b class="ms-auto">{{ $cur_contact->work->employer_address }}</b>
                                </li>
                            </ul>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            @endif
            @if($cur_share->children)
            <div class="accordion-item">
                <h2 class="accordion-header" id="headingChild">
                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseChild" aria-expanded="false" aria-controls="collapseChild">
                        Children Details
                    </button>
                </h2>
                <div id="collapseChild" class="accordion-collapse collapse" aria-labelledby="headingChild" data-bs-parent="#accordion_personal">
                    <div class="accordion-body">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Full Name</th>
                                    <th>Date of Birth</th>
                                    <th>ID Number</th>
                                    <th>Birth Certificate</th>
                                    <th>Adoption Certificate</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($cur_contact->children AS $child)
                                <tr>
                                    <td>{{ $child->child_name.' '.$child->middle_name.' '.$child->child_surname }}</td>
                                    <td>{{ $child->child_date_of_birth }}</td>
                                    <td>{{ $child->child_id_number }}</td>
                                    <td>
                                        @if($child->child_birth_certificate)
                                        <a href="{{ url('storage/'.$child->child_birth_certificate) }}">View</a>
                                        @endif
                                    </td>
                                    <td>
                                        @if($child->child_adoption_certificate)
                                        <a href="{{ url('storage/'.$child->child_adoption_certificate) }}">View</a>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            @endif
            @if($cur_share->medical_aid)
            <div class="accordion-item">
                <h2 class="accordion-header" id="headingMed">
                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseMed" aria-expanded="false" aria-controls="collapseMed">
                        Medical Aid
                    </button>
                </h2>
                <div id="collapseMed" class="accordion-collapse collapse" aria-labelledby="headingMed" data-bs-parent="#accordion_personal">
                    <div class="accordion-body">
                        <ul class="list-group">
                            @foreach($cur_contact->medicalAids AS $med)
                            <li class="list-group-item d-flex">
                                <span>Name</span>
                                <b class="ms-auto">{{ $med->name.' '.$med->surname }}</b>    
                            </li>
                            <li class="list-group-item d-flex">
                                <span>ID Number</span>
                                <b class="ms-auto">{{ $med->id_number }}</b>    
                            </li>
                            <li class="list-group-item d-flex">
                                <span>Company</span>
                                <b class="ms-auto">{{ $med->company }}</b>    
                            </li>
                            <li class="list-group-item d-flex">
                                <span>Reference Number</span>
                                <b class="ms-auto">{{ $med->reference_number }}</b>    
                            </li>
                            <li class="list-group-item d-flex">
                                <span>Medical Aid Certificate</span>
                                <b class="ms-auto"><a href="{{ url('storage/'.$med->medical_aid_certificate) }}">View</a></b>    
                            </li>
                            <li class="list-group-item d-flex">
                                <span>Conditions</span>
                                <b class="ms-auto">
                                    @foreach($med->sub_conditions AS $cond)
                                        {{ $cond->condition }}<br />
                                    @endforeach    
                                </b>    
                            </li>
                            <li class="list-group-item">
                                <span>Dependents</span>
                                <hr />
                                @foreach($med->dependants AS $dpnd)
                                <div class="mb-3">
                                    <div class="d-flex">
                                        <span>Name</span>
                                        <b class="ms-auto">{{ $dpnd->name.' '.$dpnd->surname }}</b>
                                    </div>
                                    <div class="d-flex">
                                        <span>ID Number</span>
                                        <b class="ms-auto">{{ $dpnd->id_number }}</b>
                                    </div>
                                    <div class="d-flex">
                                        <span>Condition</span>
                                        <b class="ms-auto">{{ $dpnd->condition }}</b>
                                    </div>
                                    @if($dpnd->percentage)
                                    <div class="d-flex">
                                        <span>Percentage</span>
                                        <b class="ms-auto">{{ $dpnd->percentage }}</b>
                                    </div>
                                    @endif
                                    <hr />
                                </div>
                                @endforeach
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
            @endif
        </div>
    </div>
</div>