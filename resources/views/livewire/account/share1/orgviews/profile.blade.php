<div class="row">
	<div class="col-md-12">
		<div class="row">
			<div class="col-md-6 text-center text-md-start">
                <pie class="ten">
                    @if($cur_contact->profile_pic)
                    <img class="rounded-50-lg" src="{{ asset('storage/'.$cur_contact->profile_pic) }}" alt="Images">
                    @else
                    <img class="rounded-50-lg" src="{{ asset('img/placeholder.png') }}" alt="Images">
                    @endif
                </pie>
            </div>
            <div class="col-md-6 text-center text-md-start">
                <h4 class="text-navy-blue">{{ $cur_contact->name.' '.$cur_contact->surname }}</h4>
                <div class="mb-3 mt-2">
                    <b class="text-red">{{ $cur_contact->occupation }}</b>
                </div>
                <p>{{ mb_substr($cur_contact->bio, 0, 300) }}</p>
            </div>
		</div>	
	</div>
</div>