<div class="row">
	<div class="col-md-12">
		<div class="accordion" id="accordionPrsnl">
			<div class="accordion-item">
				<h2 class="accordion-header" id="headingAsst">
					<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseAsst" aria-expanded="false" aria-controls="collapseAsst">
						Assets
					</button>
				</h2>
				<div id="collapseAsst" class="accordion-collapse collapse" aria-labelledby="headingAsst" data-bs-parent="#accordionPrsnl">
					<div class="accordion-body">
						<table class="table">
							<thead>
								<tr>
									<th>Type</th>
									<th>Value</th>
									<th>Outstanding</th>
									<th>Description</th>
								</tr>
							</thead>
							<tbody>
								@foreach($cur_contact->assets AS $asset)
								<tr>
									<td>{{ $asset->asset_type }}</td>
									<td>{{ $asset->value }}</td>
									<td>{{ $asset->outstanding }}</td>
									<td>{{ $asset->description }}</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="accordion-item">
				<h2 class="accordion-header" id="headingInv">
					<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseInv" aria-expanded="false" aria-controls="collapseInv">
						Investments and Policies
					</button>
				</h2>
				<div id="collapseInv" class="accordion-collapse collapse" aria-labelledby="headingInv" data-bs-parent="#accordionPrsnl">
					<div class="accordion-body">
						<table class="table">
							<thead>
								<tr>
									<th>Type</th>
									<th>Company / Organisation</th>
									<th>Policy Number</th>
									<th>Value</th>
									<th>File</th>
								</tr>
							</thead>
							<tbody>
								@foreach($cur_contact->investments AS $inv)
								<tr>
									<td>{{ $inv->investment_type }}</td>
									<td>{{ $inv->description }}</td>
									<td>{{ $inv->policy_number }}</td>
									<td>{{ $inv->value }}</td>
									<td>
										@if($inv->file)
										<a href="{{ url('storage/'.$inv->file) }}">View</a>
										@endif
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="accordion-item">
				<h2 class="accordion-header" id="headingBnk">
					<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseBnk" aria-expanded="false" aria-controls="collapseBnk">
						Bank Accounts
					</button>
				</h2>
				<div id="collapseBnk" class="accordion-collapse collapse" aria-labelledby="headingBnk" data-bs-parent="#accordionPrsnl">
					<div class="accordion-body">
						<table class="table">
							<thead>
								<tr>
									<th>Account Type</th>
									<th>Bank</th>
									<th>Account Number</th>
									<th>Value</th>
									<th>File</th>
								</tr>
							</thead>
							<tbody>
								@foreach($cur_contact->banks AS $bnk)
								<tr>
									<td>{{ $bnk->account_type }}</td>
									<td>{{ $bnk->bank }}</td>
									<td>{{ $bnk->account_number }}</td>
									<td>{{ $inv->value }}</td>
									<td>
										@if($bnk->file)
										<a href="{{ url('storage/'.$bnk->file) }}">View</a>
										@endif
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>