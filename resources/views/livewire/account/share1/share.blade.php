<div>
    <div class="container-fluid">
        <style>
            .info .accordion-item {
                background-color: #eff1f4;
            }
            .info .accordion-button {
                border: 1px solid #1c355e ; 
                background-color: #1c355e ; 
                color: #FFF;
            }
            .info .accordion-button::after {
                width: 10px;
                height: 10px;
                background: transparent;
                text-indent: -9999px;
                border-top: 2px solid #FFF;
                border-left: 2px solid #FFF;
                transition: all 250ms ease-in-out;
                text-decoration: none;
                color: transparent;
                transform: rotate(220deg);
            }
            .info .accordion-button::after {
                margin-left: auto;
                color: #FFF !important;
            }
            .info .accordion-button:not(.collapsed) {
                background-color: #FFF;
            }
            .info .accordion-item .accordion-body {
                background-color: #FFF;
            }
            .share-label{
                margin-left: 0.2rem;
            }
            .shar-nav .nav-item{
                font-weight: 700;
                margin-right: 20px;
            }
            .shar-nav .nav-item .nav-link{
                color: #1c355e;
            }
            .shar-nav .nav-item .nav-link.active{
                color: #ee0041;
            }
            .line-top{
                border-top: 1px solid #CCCCCC;
            }
        </style>
        <div class="row">
            <div class="col-md-12">
                <livewire:account.partials.header />
                <div class="row">
                    <div class="col-md-4">
                        <div class="accordion info" id="accordionExample">
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="headingOne">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                        WrapUp Organiser's Sharing Function
                                    </button>
                                </h2>
                                <div id="collapseOne" class="accordion-collapse collapse" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                                    <div class="accordion-body">
                                        @if(Auth::user()->free_account == 1)
                                        <div class="mb-3">
                                            <p>(This awesome feature is only available in our paid package. Want to unlock it? Upgrade your account <a href="{{ url('register') }}"><b class="text-blue">here</b></a>)</p>
                                        </div>
                                        @endif
                                        <p>Simplify access to important information and enhance communication with our new sharing function.</p>                                                   
                                        <p>With this feature, you can securely share information with anyone, including non-WrapUp members.</p>                                                    
                                        <p>Experience the convenience and flexibility of easily sharing specific documents, insurance policies, and personal details.</p>                                                
                                        <p>Here's how it works:</p>
                                        <ol>                                                  
                                            <li>Enter the recipient's name, email address, and ID number.</li>                                             
                                            <li>Select the specific information you want to share from your WrapUp Organiser. </li>                                                
                                            <li>The recipient will receive an email containing a secure link. To access the shared information, the recipient must verify their identity by using their ID number.</li>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="headingTwo">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        Benefits of Sharing your info
                                    </button>
                                </h2>
                                <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                                    <div class="accordion-body">
                                        @if(Auth::user()->free_account == 1)
                                        <div class="mb-3">
                                            <p>(This awesome feature is only available in our paid package. Want to unlock it? Upgrade your account <a href="{{ url('register') }}"><b class="text-blue">here</b></a>)</p>
                                        </div>
                                        @endif
                                        <p><b>24/7 Access:</b> Ensure your trusted contacts have continuous access to important documents, providing peace of mind during emergencies or time-sensitive matters.</p>
                                        <p><b>Convenience:</b> Access shared information from any device, eliminating the need for physical copies or proximity to saved documents.</p>
                                        <p><b>Emergency Preparedness:</b> Empower trusted contacts to act on your behalf during emergencies by quickly accessing critical details like medical records or emergency contacts.</p>
                                        <p><b>Remote Accessibility:</b> Extend your support network even when physically distant, allowing trusted contacts to access shared information from anywhere.</p>
                                        <p><b>Collaborative Planning:</b> Foster smoother collaboration among family members or trusted individuals for financial matters, estate planning, or organising important documents.</p>
                                        <p><b>Secure and Controlled Sharing:</b> Prioritise data security with robust encryption and industry-leading protocols. Maintain control by removing access or updating shared content at any time.</p>

                                        <p><i>Embrace the power of sharing and simplify your life with the WrapUp Smart Life Organiser. Enhance your support network and ensure your trusted contacts always have essential information, regardless of their location.</i></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @if(Auth::user()->free_account == 0)
                        <div class="card-box-style rounded p-3">
                            <div class="others-title organiser-description-medium d-flex">
                                <h3>Shared With</h3>
                                <span class="ms-auto">
                                    <a href="" class="btn btn-outline-one text-red" wire:click.prevent="clearShareForm">New Share</a>
                                </span>
                            </div>
                            <ul class="list-group">
                                @foreach($contacts AS $contact)
                                <a class="list-group-item d-flex" href="#" wire:click.prevent="showShare({{ $contact->id }})">
                                    <span>
                                        {{ $contact->name }}
                                        <div class=""><small class="text-muted">{{ $contact->email }}</small></div>
                                    </span>
                                    <span class="ms-auto">
                                        <button class="text-danger" wire:click.prevent="removeShare({{ $contact->id }})"><i class="bx bxs-trash"></i></button>
                                    </span>
                                </a>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                        <div class="card-box-style rounded p-3">
                            <div class="">
                                <h5 style="font-weight: 600; font-size: 20px;">Shared With Me</h5>
                                <span class="text-muted">Click on the connection to view their shared information with you. The details will appear on the right side of your screen.</span>
                            </div>
                            <ul class="list-group">
                                @foreach($shared_with_me AS $contact)
                                <a class="list-group-item d-flex" href="#" wire:click.prevent="showSharePage({{ $contact->user->id }})">
                                    <span>
                                        {{ $contact->user->name.' '.$contact->user->surname }}
                                        <div class=""><small class="text-muted">{{ $contact->user->email }}</small></div>
                                    </span>
                                </a>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="card-box-style rounded p-5">
                            <div class="row">
                                <div class="col-md-12 text-end">
                                    <a href="#" data-bs-toggle="modal" data-bs-target="#infoModal"><img src="{{ asset('img/info.png') }}"></a>
                                </div>
                            </div>
                            @if($errors->any())
                                <div class="alert alert-danger">
                                    <span>{{ $errors->first() }}</span>
                                </div>
                            @endif
                            @if (session()->has('message'))
                                <div class="alert alert-success">
                                    {{ session('message') }}
                                </div>
                            @endif

                            @if($showSharePage)
                                @if($cur_contact)
                                    <h4 class="text-navy-blue">{{ strtoupper($cur_contact->name.' '.$cur_contact->surname) }}</h4>
                                @endif
                                @include('livewire.account.share.viewShare')
                            @else
                                <h4 class="text-navy-blue">Share Info</h4>
                                @include('livewire.account.share.form')
                            @endif
                        </div>
                    </div>
                </div>
        </div>
    </div>
    <div class="row d-block d-lg-none">
        <div class="col-md-12">
            <div style="margin-bottom: 100px"></div>
        </div>
    </div>
        
    <div class="modal" tabindex="-1" id="share-modal" wire:ignore.self>
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Share Profile</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" wire:click.prevent="closeModal"></button>
                </div>
                <div class="modal-body">
                    <div class="">
                        <p>
                            You are about to share Personal and Financial information - to proceed, kindly indicate that you understand and you are certain that you want to do so. </p>
                        </p>
                        <input class="form-check-input" type="checkbox" wire:model="confirmModal">
                        <label class="form-check-label share-label" for="flexCheckDefault">
                            I understand
                        </label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-two" wire:click.prevent="closeModal" data-bs-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-red" wire:click="shareProfile" wire:loading.attr="disabled">Continue</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal" id="infoModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="text-end">
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <h5>Info-graphics coming soon</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal" id="minorModal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="text-navy-blue">Children</h4>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <p>According to the POPI Act, only a person who is legally competent to do so, which includes a person who is the legal guardian of their minor child, may consent to the sharing of general information about their minor children.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    
        
    @push('scripts')
        <script>
            window.addEventListener('show-share-modal', event => {
                $('#share-modal').modal('show');
            });
            
            window.addEventListener('close-share-modal', event => {
                $('.modal').modal('hide');
            });
        </script>
            
        <script>
            $(document).ready(function(){
                $('.has_sub').on('change', function(){
                    var id = $(this).attr('id');
                    if($(this).is(':checked')){
                        $('.'+id).prop('checked', true);
                    }
                    else{
                        $('.'+id).prop('checked', false);
                    }
                });
            });
        </script>
    @endpush
</div>
