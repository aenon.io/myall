@if($cur_share)
<div class="row">
	<div class="col-md-12">
		<span class="text-muted">Click on a category below to view the info</span>
	</div>
	<div class="col-md-12 px-0 mx-0 line-top">
		<nav class="navbar navbar-expand-lg navbar-light" style="background-color: rgba(255, 255, 255, 1);">
			<div class="container-fluid mx-0 px-0">
				<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="shar-nav collapse navbar-collapse" id="navbarSupportedContent">
					<ul class="navbar-nav me-auto mb-2 mb-lg-0">
						@if($cur_share->profile)
						<li class="nav-item">
							<a class="nav-link @if($cur_view == 'profile') active @endif" aria-current="page" href="#" wire:click.prevent="showPage('profile', 'profile')">Profile</a>
						</li>
						@endif
						@if($cur_share->organiser)
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle @if($cur_view == 'organiser') active @endif" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
								Organiser <i class="bx bx-chevron-down"></i>
							</a>
							<ul class="dropdown-menu" aria-labelledby="navbarDropdown">
								@if($cur_share->personal)
									<li><a class="dropdown-item" href="#" wire:click.prevent="showPage('organiser', 'personal')">Personal</a></li>
								@endif
								@if($cur_share->emergency)
									<li><a class="dropdown-item" href="#" wire:click.prevent="showPage('organiser', 'emergency_contacts')">Emergency Contacts</a></li>
								@endif
								@if($cur_share->business_wealth)
									<li><a class="dropdown-item" href="#" wire:click.prevent="showPage('organiser', 'business_wealth')">Business Wealth</a></li>
								@endif
								@if($cur_share->properties_wealth)
									<li><a class="dropdown-item" href="#" wire:click.prevent="showPage('organiser', 'properties_wealth')">Personal Wealth</a></li>
								@endif
								@if($cur_share->debt_expenses)
									<li><a class="dropdown-item" href="#" wire:click.prevent="showPage('organiser', 'debt_expenses')">Debt & Expenses</a></li>
								@endif
								@if($cur_share->social_media)
									<li><a class="dropdown-item" href="#" wire:click.prevent="showPage('organiser', 'social_media')">Social Media</a></li>
								@endif
								@if($cur_share->safe_keeping)
									<li><a class="dropdown-item" href="#" wire:click.prevent="showPage('organiser', 'safe_keeping')">Safekeeping</a></li>
								@endif
								@if($cur_share->memories)
									<li><a class="dropdown-item" href="#" wire:click.prevent="showPage('organiser', 'memories')">Memories</a></li>
								@endif
								@if($cur_share->funeral)
									<li><a class="dropdown-item" href="#" wire:click.prevent="showPage('organiser', 'funeral')">Funeral</a></li>
								@endif
							</ul>
						</li>
						@endif
						@if($cur_share->family_tree)
						<li class="nav-item">
							<a class="nav-link @if($cur_view == 'family_tree') active @endif" href="#" wire:click.prevent="showPage('family_tree', 'family_tree')">My Network</a>
						</li>
						@endif
					</ul>
				</div>
			</div>
		</nav>
	</div>
</div>
<div class="mt-5">
@if($view == "profile")
	@include("livewire.account.share.orgviews.profile")
@elseif($view == "personal")
	@include("livewire.account.share.orgviews.personal")
@elseif($view == "emergency_contacts")
	@include("livewire.account.share.orgviews.emergency_contacts")
@elseif($view == "business_wealth")
	@include("livewire.account.share.orgviews.business_wealth")
@elseif($view == "properties_wealth")
	@include("livewire.account.share.orgviews.properties_wealth")
@elseif($view == "debt_expenses")
	@include("livewire.account.share.orgviews.debt_expenses")
@elseif($view == "social_media")
	@include("livewire.account.share.orgviews.social_media")
@elseif($view == "safe_keeping")
	@include("livewire.account.share.orgviews.safe_keeping")
@elseif($view == "memories")
	@include("livewire.account.share.orgviews.memories")
@elseif($view == "funeral")
	@include("livewire.account.share.orgviews.funeral")
@elseif($view == "family_tree")
	@include("livewire.account.share.orgviews.family_tree")
@endif
</div>
@else
<div class="col-md-12">
	<div class="text-center mt-5">
		<h5>Share Info</h5>
		<p>View what others have shared with you here.</p>
		<p>Upgrade your account to get full access to everything WrapUp has to offer and share info with others.</p>
		<div class="mt-3">
			<a href="{{ url('register') }}" class="btn btn-red">Upgrade Account</a>
		</div>
	</div>
</div>
@endif