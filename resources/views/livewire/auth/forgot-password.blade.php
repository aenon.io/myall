<section class="wrapper section-2 __forgot-password">
  <div class="__island">
    <div class="__heading">
      <h2>Forgot Password</h2>
      <p class="">
        Enter your email address and we will send you a reset password
        link
      </p>
    </div>
    <div class="__body">
      <div class="__form">
        <form wire:submit.prevent="resetPass">
          <div class="">
            <label class="form-label">Email address</label>
            <input type="email" class="form-control" name="email"
              wire:model.defer="email">
          </div>
          <div class="">
            <input type="submit" class="" value="RESET">
          </div>
        </form>
      </div>
      @if (session()->has('message'))
        <div class="alert alert-success mt-3">
          {{ session('message') }}
        </div>
      @endif
      @if ($errors->any())
        <div class="alert alert-danger mt-3">
          <span>{{ $errors->first() }}</span>
        </div>
      @endif
    </div>
  </div>

  {{-- <div class="container"> --}}
  {{--   <div class="row mt-lg-15 mb-10 mt-10"> --}}
  {{--     <div class="col-md-6 offset-md-3"> --}}
  {{--       <div class="card"> --}}
  {{--         <div class="card-body"> --}}
  {{--           <div class="row mt-5"> --}}
  {{--             <div class="col-md-12"> --}}
  {{--               <h2 class="banner-title">Forgot Password</h2> --}}
  {{--               <p class="text-red"> --}}
  {{--                 Enter your email address and we will send you a reset password --}}
  {{--                 link --}}
  {{--               </p> --}}
  {{--             </div> --}}
  {{--             <div class="col-md-12"> --}}
  {{--               @if (session()->has('message')) --}}
  {{--                 <div class="alert alert-success"> --}}
  {{--                   {{ session('message') }} --}}
  {{--                 </div> --}}
  {{--               @endif --}}
  {{--               @if ($errors->any()) --}}
  {{--                 <div class="alert alert-danger"> --}}
  {{--                   <span>{{ $errors->first() }}</span> --}}
  {{--                 </div> --}}
  {{--               @endif --}}
  {{--             </div> --}}
  {{--             <div class="col-md-12"> --}}
  {{--               <form wire:submit.prevent="resetPass"> --}}
  {{--                 <div class="mb-3"> --}}
  {{--                   <label class="form-label">Email address</label> --}}
  {{--                   <input type="email" class="form-control" name="email" --}}
  {{--                     wire:model.defer="email"> --}}
  {{--                 </div> --}}
  {{--                 <div class="d-grid mb-3"> --}}
  {{--                   <input type="submit" --}}
  {{--                     class="btn btn-lg btn-gradient gradient-1" value="RESET"> --}}
  {{--                 </div> --}}
  {{--               </form> --}}
  {{--             </div> --}}
  {{--           </div> --}}
  {{--         </div> --}}
  {{--       </div> --}}
  {{--     </div> --}}
  {{--   </div> --}}
  {{-- </div> --}}
</section>
