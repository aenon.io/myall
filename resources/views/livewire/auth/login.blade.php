<div>
  <section class="wrapper section-2">
    <div class="container">
      <div class="row mt-lg-15 mb-10 mt-10">
        <div class="col-md-10 offset-md-1 col-lg-6 offset-lg-3">
          <div class="card">
            <div class="card-body">
              <div class="login-img-wrapper">
                <img class="l-img" src="{{ asset('img/e1d2f5423a.jpg') }}">
              </div>
              <div class="row mt-5">
                <div class="l-banner">
                  <h2 class="lbanner-title">Sign in</h2>
                </div>
              </div>
              <div class="login-hr">
                <hr class="lhr">
              </div>
              <div class="card-content">
                <form wire:submit.prevent="authUser">
                  <div class="row">
                    <div class="col-md-12">
                      @if ($errors->any())
                        <div class="alert alert-danger">
                          <span>{{ $errors->first() }}</span>
                        </div>
                      @endif
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="loginform1">
                        <label class="form-label">Email</label>
                        <input type="email" class="form-control"
                          placeholder="" name="email"
                          wire:model.defer="email">
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="loginform">
                        <label class="form-label">Password</label>
                        <div class="input-group mb-3">
                          <input
                            @if (!$show_password) type="password" @else type="text" @endif
                            class="form-control password_with_toggle"
                            placeholder="" name="password"
                            wire:model.defer="password">
                          <span class="input-group-text"
                            wire:click.prevent="showPassword"><i
                              class="fa @if (!$show_password) fa-eye @else fa-eye-slash @endif"
                              id="togglePassword"></i></span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="login-btn-wrapper">
                      <input type="submit" class="login-btn" value="Submit">
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                      <span><a class="card-sml"
                          href="{{ url('register') }}">Register</a></span>
                    </div>
                    <div class="col-md-6 text-md-end text-start">
                      <span class="ms-auto"><a class="card-sml"
                          href="{{ url('forgot-password') }}">Forgot
                          Password?</a></span>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  @push('scripts')
    <script>
      $(document).ready(function() {
        $('.password_toggle').on('click', function() {
          var input = $(this).parent().find('input');
          var type = $(input).attr('type');
          if (type == "password") {
            $(input).attr('type', "text");
            $(this).find('i').addClass('fa-eye-slash');
            $(this).find('i').removeClass('fa-eye');

          } else {
            $(input).attr('type', 'password');
            $(this).find('i').removeClass('fa-eye-slash');
            $(this).find('i').addClass('fa-eye');
          }
        })
      });
    </script>
  @endpush
</div>
