<div>
  <?php
  header('HTTP/1.0 200 OK');
  flush();
  ?>
  <section class="wrapper section-2 __register"
    style="min-height: 100vh; display: flex; flex-direction: column; justify-content: space-between; ">
    @if (!$view)
      <div class="container mb-10">
        <div class="row mt-5">
          <div class="col-md-12">
            <div class="banner-container">
              @if ($selectedPlan == 'free')
                <h2 class="banner-title">Register for Free Access</h2>
              @else
                <h2 class="banner-title">Register for Full Access</h2>
              @endif
              <div class="o-wrapper __register-plan">
                <button wire:click="$emit('setSessionPlan', 'free')"
                  class="v-title full-access @if ($selectedPlan == 'free') active @endif">
                  Free
                </button>
                <button wire:click="$emit('setSessionPlan', 'monthly')"
                  class="v-title full-access @if ($selectedPlan == 'monthly') active @endif">
                  Monthly
                </button>
                <button wire:click="$emit('setSessionPlan', 'annual')"
                  class="v-title full-access @if ($selectedPlan == 'annual') active @endif">
                  Annual
                </button>
              </div>
            </div>
          </div>
        </div>
        @if ($question_stage == 0)
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-body text-center">
                  <h2 class="page-title">Welcome</h2>
                  <div class="row mt-5">
                    <div class="col-md-8 offset-md-2">
                      <h3 class="blue-color">First we need to ask you these two
                        questions before we can get started.</h3>
                      <div class="mt-5">
                        <a href="#"
                          class="btn btn-lg btn-gradient gradient-1"
                          wire:click.prevent="setQuestionStage(1)">CONTINUE</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div><br>
              <p style="text-align: center;">You are not required to complete
                your online Will by answering these questions. We understand
                that you may already have a Will or may prefer to create one at
                a later time. However, your Profile will always provide you with
                the option to create a Will or upload an existing one whenever
                you are ready.</p>
            </div>
          </div>
        @elseif($question_stage == 1)
          <div class="row mb-5">
            <div class="col-sm-12 col-md-12 col-lg-6 mb-3">
              <div class="card">
                <div class="card-body">
                  <div class="d-flex">
                    <h1>Question 1</h1>
                    <span class="ms-auto">
                      <!--<a href="#"><img src="{{ asset('img/vid.png') }}"></a>-->
                      <a href="#"
                        wire:click.prevent="showInfoGraphics(1)"><img
                          src="{{ asset('img/info.png') }}"></a>
                    </span>
                  </div>
                  <h3>Do you own property outside of South Africa?</h3>
                  <div class="row">
                    <div class="col-sm-4 col-md-4 col-lg-4 mt-2">
                      <div class="d-flex justify-content-around">
                        <a href="#"
                          class="btn btn-red rounded-pill qstn-btn"
                          wire:click.prevent="updateQustionOne('yes')">Yes</a>
                      </div>
                    </div>
                    <div class="col-sm-4 col-md-4 col-lg-4 mt-2">
                      <div class="d-flex justify-content-around">
                        <a href="#"
                          class="btn btn-red rounded-pill qstn-btn"
                          wire:click.prevent="updateQustionOne('no')">No</a>
                      </div>
                    </div>
                    <div class="col-sm-12 col-md-4 col-lg-4 mt-2">
                      <div class="d-flex justify-content-around">
                        <a href="#" class="btn btn-outline-one qstn-btn"
                          wire:click.prevent="setQuestionStage(0)">BACK</a>
                      </div>
                    </div>
                  </div>
                  @if ($q_one_options)
                    <div class="mt-5">
                      <div class="mb-3">
                        <label class="form-label">Select applicable
                          option</label>
                        <select class="form-control" name="property_options"
                          wire:model="property_options">
                          <option value="">Select Option</option>
                          <option value="go">Yes, and have possessions in
                            South Africa</option>
                          <option value="no_go">Yes, and have no possessions in
                            South Africa</option>
                        </select>
                      </div>
                    </div>
                  @endif
                </div>
              </div>
              <div class="mt-3 text-end">
                <a href="#" class="btn btn-red rounded-pill btn-sm"
                  data-bs-toggle="modal"
                  data-bs-target="#disclaimer-modal">LEGAL DISCLAIMER</a>
              </div>
            </div>
            @if ($q_one_des)
              <div class="col-sm-12 col-md-12 col-lg-6">
                <div class="card">
                  <div class="card-body">
                    <p><b>Please Note</b></p>
                    <p>This online Will application only applies to South
                      African inheritance property to be administered in
                      accordance with South African law.</p>
                    <p>If you have foreign property, the law of that foreign
                      country may apply, depending on where the property is
                      situated, registered, held, or even managed. Additionally,
                      your nationality, last place of residence, or your main
                      place of residence may further determine which country’s
                      law governs the inheritance rules.</p>
                    <p>Any applicable foreign law may also govern who the
                      beneficiary and executor may be, as well as the
                      formalities of your Will.</p>
                    <p>This includes whether you can have one Will for all the
                      countries or whether you need a separate Will for each
                      country.</p>
                    <p>If you already have an offshore Will, a later Will may
                      revoke your existing offshore Will.</p>
                    <p>Please contact us for assistance.</p>
                    <div class="mt-5">
                      <div class="form-check">
                        <input class="form-check-input" type="checkbox"
                          value="1" id="q_one_pass"
                          wire:model="q_one_pass">
                        <label class="form-check-label" for="q_one_pass">
                          Yes, I have read and understood the requirements for
                          completing my online Will
                        </label>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            @endif
            {{-- <div class="mt-2">
                    <a href="#" class="btn btn-red rounded-pill" wire:click.prevent="setQuestionStage(0)">BACK</a>
                </div> --}}
          </div>
        @elseif($question_stage == 2)
          <div class="row mb-5">
            <div class="col-sm-12 col-md-12 col-lg-6 mb-3">
              <div class="card">
                <div class="card-body">
                  <div class="d-flex">
                    <h1>Question 2</h1>
                    <span class="ms-auto">
                      <!--<a href="#"><img src="{{ asset('img/vid.png') }}"></a>-->
                      <a href="#"
                        wire:click.prevent="showInfoGraphics(2)"><img
                          src="{{ asset('img/info.png') }}"></a>
                    </span>
                  </div>
                  <h3>Can you read and write your full name on your Will by
                    means of a signature?</h3>
                  <div class="row">
                    <div class="col-sm-12 col-md-4 col-lg-4 mt-2">
                      <div class="d-flex justify-content-around">
                        <a href="#"
                          class="btn btn-red rounded-pill qstn-btn"
                          wire:click.prevent="updateQustionTwo('yes')">Yes</a>
                      </div>
                    </div>
                    <div class="col-sm-12 col-md-4 col-lg-4 mt-2">
                      <div class="d-flex justify-content-around">
                        <a href="#"
                          class="btn btn-red rounded-pill qstn-btn"
                          wire:click.prevent="updateQustionTwo('no')">No</a>
                      </div>
                    </div>
                    <div class="col-sm-12 col-md-4 col-lg-4 mt-2">
                      <div class="d-flex justify-content-around">
                        <a href="#" class="btn btn-outline-one qstn-btn"
                          wire:click.prevent="setQuestionStage(1)">BACK</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="mt-3 text-end">
                <a href="#" class="btn btn-red rounded-pill btn-sm"
                  data-bs-toggle="modal"
                  data-bs-target="#disclaimer-modal">LEGAL DISCLAIMER</a>
              </div>
            </div>
            @if ($q_two_des)
              <div class="col-sm-12 col-md-12 col-lg-6">
                <div class="card">
                  <div class="card-body">
                    <p><b>Please Note</b></p>
                    <p>If you cannot sign your own name, you may ask someone to
                      sign the Will on your behalf or sign the Will by making a
                      mark, cross, or a thumbprint.</p>
                    <p>When the Will is signed by someone on your behalf or by
                      making a mark, a Commissioner of Oaths must certify your
                      identity.</p>
                    <p>The Commissioner of Oaths must also sign every page of
                      the Will.</p>
                    <p>Please confirm that you have understood this by selecting
                      the checkbox below to continue.</p>
                    <div class="mt-5">
                      <div class="form-check">
                        <input class="form-check-input" type="checkbox"
                          value="1" id="q_two_pass"
                          wire:model="q_two_pass">
                        <label class="form-check-label" for="q_two_pass">
                          Yes, I have read and understood the requirements for
                          completing my online Will
                        </label>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            @endif
          </div>
        @elseif($question_stage == 'fail')
          <div class="row mb-5">
            <div class="col-md-12">
              <div class="card">
                <div class="card-body">
                  <h1 class="page-title text-red text-uppercase">Unsuccessful -
                    But we can still help you</h1>
                  <div class="mt-3">
                    <h3>You may not qualify for our Online Will Service</h3>
                    <h3>Please contact us for assistance with a solution to suit
                      your needs.</h3>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-12 mt-5">
              <div class="card">
                <div class="card-body">
                  <livewire:landing.partials.contact />
                </div>
              </div>
            </div>
          </div>
          {{-- NOTE: Here's the form  --}}
        @elseif($question_stage == 'form')
          <form class="register-content-form __form"
            wire:submit.prevent="saveUser">
            <div class="details-wrapper">
              <p class="d-heading">Your details</p>
            </div>
            <div class="register-content-wrapper">
              <div class="row">
                <div class="col-md-12">
                  @if ($errors->any())
                    <div class="alert alert-danger">
                      <span>{{ $errors->first() }}</span>
                    </div>
                  @endif
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-4">
                  <div class="mb-3">
                    <label class="form-label">Name*</label>
                    <input type="text" class="form-control" placeholder=""
                      name="name" wire:model.defer="name">
                  </div>
                </div>
                <div class="col-sm-12 col-md-12 col-lg-4">
                  <div class="mb-3">
                    <label class="form-label">Middle Name*</label>
                    <input type="text" class="form-control" placeholder=""
                      name="middle_name" wire:model.defer="middle_name">
                  </div>
                </div>
                <div class="col-sm-12 col-md-12 col-lg-4">
                  <div class="mb-3">
                    <label class="form-label">Surname*</label>
                    <input type="text" class="form-control" placeholder=""
                      name="surname" wire:model.defer="surname">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-4">
                  <div class="mb-3">
                    <label class="form-label btm-row">Date of Birth</label>
                    <input type="date" class="form-control"
                      placeholder="DOB*" max="2999-12-31"
                      name="date_of_birth" wire:model.defer="date_of_birth">
                  </div>
                </div>
                <div class="col-sm-12 col-md-12 col-lg-4">
                  <div class="mb-3">
                    <label class="form-label btm-row">Gender <i
                        class="fa fa-info-circle"
                        wire:click.prevent="showTipModal('gender')"></i></label>
                    <select class="form-control" placeholder="Gender*"
                      name="gender" wire:model.defer="gender">
                      <option value="">Select Option</option>
                      <option value="Male">Male</option>
                      <option value="Female">Female</option>
                    </select>
                  </div>
                </div>
                <div class="col-sm-12 col-md-12 col-lg-4">
                  <div class="mb-3">
                    <label class="form-label btm-row">Ethnicity <i
                        class="fa fa-info-circle"
                        wire:click.prevent="showTipModal('ethnicity')"></i></label>
                    <select class="form-control" placeholder="Ethnicity*"
                      name="ethnicity" wire:model.defer="ethnicity">
                      <option value="">Select Option</option>
                      @foreach ($ethnicities as $eth)
                        <option value="{{ $eth }}">{{ $eth }}
                        </option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="id-content">
                  <p class="id-passport">Add ID and/or Passport number below
                  </p>
                </div>
                <div class="grid-register">
                  <div class="col-sm-12 col-md-12">
                    <div class="mb-1">
                      <label class="form-label">ID Number*</label>
                      <input type="text" class="form-control form-id"
                        placeholder="" name="id_number"
                        wire:model.defer="id_number">
                    </div>
                  </div>
                  <div class="col-sm-12 col-md-12">
                    <div class="mb-1">
                      <label class="form-label">Passport Number*</label>
                      <input type="text" class="form-control form-id"
                        placeholder="" name="passport_number"
                        wire:model.defer="passport_number">
                    </div>
                  </div>
                </div>
                <div class="grid-register">
                  <div class="col-md-12">
                    <div class="mb-3">
                      <label class="form-label email-br">Email Address*</label>
                      <input type="email" class="form-control"
                        placeholder="" name="email"
                        wire:model.defer="email">
                    </div>
                  </div>
                </div>
                <div>
                  <hr class="register-hr">
                </div>
                <div class="row" style="gap: 0.4rem">
                  <div class="grid-register">
                    <div class="mb-3">
                      <p class="password-heading">Create Password</p>
                      <p class="pass-requirements">Min. length of 15
                        characters. Includes:</p>
                      <p class="pass-requirements req-btm">
                        Uppercase letters (A-Z), lowercase letters (a-z),
                        numbers (0-9), and special characters (e.g.,
                        !@#$%^&*()_-+=)
                      </p>
                      <div class="input-group mb-3">
                        <input
                          @if (!$show_password) type="password" @else type="text" @endif
                          class="form-control password_with_toggle"
                          placeholder="" name="password"
                          wire:model="password">
                        <span class="input-group-text"
                          wire:click.prevent="showPassword"><i
                            class="fa @if (!$show_password) fa-eye @else fa-eye-slash @endif"></i></span>
                      </div>
                    </div>
                  </div>
                  <div class="grid-register __password-strength-content"
                    style="line-height: 1.2">
                    <div class="pass-strenght __progress-container">
                      <small class="text-muted">Weak</small>
                      <div class="progress password-strength-cont">
                        <div
                          class="progress-bar password-strength-progress {{ $pass_bg }} text-center"
                          role="progressbar"
                          style="width: {{ $password_score }}%;"
                          aria-valuenow="{{ $password_score }}"
                          aria-valuemin="0" aria-valuemax="100">
                        </div>
                      </div>
                      <small class="text-muted">Strong</small>
                    </div>
                    <small class="text-muted">
                      {{ $password_warning }}
                    </small>
                  </div>
                  <div class="grid-register">
                    <div class="mb-3">
                      <label class="form-label pass-lbl">Confirm
                        Password</label>
                      <div class="input-group mb-3">
                        <input
                          @if (!$show_password) type="password" @else type="text" @endif
                          class="form-control password_with_toggle"
                          placeholder="" name="password_confirmation"
                          wire:model.defer="password_confirmation">
                        <span class="input-group-text"
                          wire:click.prevent="showPassword"><i
                            class="fa @if (!$show_password) fa-eye @else fa-eye-slash @endif"
                            id="togglePassword"></i></span>
                      </div>
                    </div>
                  </div>
                  <div class="passpol-container">
                    <p class="pol-link">Password Policy & FAQs</p>
                  </div>
                  <div>
                    <hr class="register-hr">
                  </div>
                  <div class="btn-container">
                    <div class="back-btn">
                      <a href="#" class="btn btn-back"
                        role="button">Back</a>
                    </div>
                    <div class="submit-btn">
                      <input type="submit" class="btn-submit"
                        value="Next">
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="legal-disc-wrapper">
              <p class="legal-disc">Legal Disclaimer</p>
            </div>
          </form>
        @endif
      </div>
    @elseif($view == 'payment')
      <div class="__payments container mb-10">
        <div class="row mt-5">
          <div class="col-md-12" style="text-align: center">
            <h2 class="banner-title">Payment</h2>
          </div>
        </div>
        <div class="__island">
          <div class="__heading">
            You will be redirected to a secure payment platform form.
          </div>
          <div class="__body">
            <p>Your account will not be active until you complete payment. Click
              on the button below to be redirected to secure payment platform.
            </p>
            <p>
              You can cancel your subscription at any time. Please note that
              refunds are not available.
              However, we will securely store your entered data for 3 months
              after
              cancellation. This ensures a seamless renewal process,
              allowing you to effortlessly update any information that hasn't
              been
              modified in the past 3 months.
              The same applies if you decide not to renew the yearly
              subscription.
            </p>
            <form method="post" action="{{ $pf_url }}">
              @foreach ($form_data as $k => $v)
                <input type="hidden" name="{{ $k }}"
                  value="{{ $v }}">
              @endforeach
              <input type="submit" class="btn btn-lg"
                value="Process Payment">
            </form>
          </div>
        </div>
      </div>
    @elseif($view == 'payment-canceled')
      <div class="__payments container">
        <div class="row mt-5">
          <div class="col-md-12" style="text-align: center">
            <h2 class="banner-title">Payment Cancelled</h2>
          </div>
        </div>
        <div class="__island">
          <div class="__heading">Your payment was unsuccessful. Click on the
            button below to
            retry.</div>
          <div class="mt-3">
            <a href="{{ url('register/payment/' . $user_id) }}"
              class="btn btn-lg">RETRY PAYMENT</a>
          </div>
        </div>
      </div>
    @elseif($view == 'payment-complete')
      <div class="__payments container">
        <div class="row mt-5">
          <div class="col-md-12" style="text-align: center">
            <h2 class="banner-title">Payment successful</h2>
          </div>
        </div>
        <div class="__island">
          <div class="__heading">
            Your payment was successful. Click on the button below to login.
          </div>
          <div class="mt-3">
            <a href="{{ url('login') }}" class="btn btn-lg">LOGIN</a>
          </div>
        </div>
      </div>
    @elseif($view == 'upgrade')
      <div class="__payments __upgrade container mb-10">
        <div class="row mt-5">
          <div class="col-md-12" style="text-align: center">
            <h2 class="banner-title">Choose Plan</h2>
          </div>
        </div>
        <div class="__island">
          <div class="__heading">
            {{-- You will be redirected to a secure payment platform form. --}}
          </div>
          <div class="__body">
            <button wire:click="$emit('setSessionPlan', ['monthly', true])">
              <div>Monthly</div>
              <div class="__pricing">R29/month</div>
            </button>
            <button wire:click="$emit('setSessionPlan', ['annual', true])">
              <div>Annual</div>
              <div class="__pricing">R313/Year</div>
            </button>
          </div>
        </div>
      </div>
    @endif
    <footer class="footer-wrapper">
      <div class="socials-wrapper-f">
        <div class="social-svg">
          <svg width="32" height="32" viewBox="0 0 32 32"
            fill="none" xmlns="http://www.w3.org/2000/svg">
            <rect width="32" height="32" rx="16"
              fill="#F5ADBF" fill-opacity="0.1" />
            <path d="M19 14.2H16.9905V13C16.9905 12.3808 17.0384 11.9908 17.8815
            11.9908H18.9464V10.0828C18.4282 10.0264 17.9072 9.99884 17.3856
            10C15.839 10 14.7102 10.9942 14.7102 12.8194V14.2H13V16.6L14.7102
            16.5994V22H16.9905V16.5982L18.7383 16.5976L19 14.2Z"
              fill="#FFADBC" />
          </svg>
          <svg width="32" height="32" viewBox="0 0 32 32"
            fill="none" xmlns="http://www.w3.org/2000/svg">
            <rect width="32" height="32" rx="16"
              fill="#F5ADBF" fill-opacity="0.1" />
            <path d="M13.3333 10C11.4953 10 10 11.4953 10 13.3333V18.6667C10 20.5047
            11.4953 22 13.3333 22H18.6667C20.5047 22 22 20.5047 22
            18.6667V13.3333C22 11.4953 20.5047 10 18.6667 10H13.3333ZM13.3333
            11.3333H18.6667C19.7693 11.3333 20.6667 12.2307 20.6667
            13.3333V18.6667C20.6667 19.7693 19.7693 20.6667 18.6667
            20.6667H13.3333C12.2307 20.6667 11.3333 19.7693 11.3333
            18.6667V13.3333C11.3333 12.2307 12.2307 11.3333 13.3333
            11.3333ZM19.3333 12C19.1565 12 18.987 12.0702 18.8619
            12.1953C18.7369 12.3203 18.6667 12.4899 18.6667 12.6667C18.6667
            12.8435 18.7369 13.013 18.8619 13.1381C18.987 13.2631 19.1565
            13.3333 19.3333 13.3333C19.5101 13.3333 19.6797 13.2631 19.8047
            13.1381C19.9298 13.013 20 12.8435 20 12.6667C20 12.4899 19.9298
            12.3203 19.8047 12.1953C19.6797 12.0702 19.5101 12 19.3333 12ZM16
            12.6667C14.162 12.6667 12.6667 14.162 12.6667 16C12.6667 17.838
            14.162 19.3333 16 19.3333C17.838 19.3333 19.3333 17.838 19.3333
            16C19.3333 14.162 17.838 12.6667 16 12.6667ZM16 14C17.1027 14 18
            14.8973 18 16C18 17.1027 17.1027 18 16 18C14.8973 18 14 17.1027 14
            16C14 14.8973 14.8973 14 16 14Z" fill="#FFADBC" />
          </svg>
          <svg width="32" height="32" viewBox="0 0 32 32"
            fill="none" xmlns="http://www.w3.org/2000/svg">
            <rect width="32" height="32" rx="16"
              fill="#F5ADBF" fill-opacity="0.1" />
            <path d="M12.6861 22H10.1982V13.9882H12.6861V22ZM11.4408 12.8953C10.6453
            12.8953 10 12.2364 10 11.4408C10 11.0587 10.1518 10.6922 10.422
            10.422C10.6922 10.1518 11.0587 10 11.4408 10C11.8229 10 12.1894
            10.1518 12.4596 10.422C12.7298 10.6922 12.8816 11.0587 12.8816
            11.4408C12.8816 12.2364 12.2361 12.8953 11.4408 12.8953ZM21.9973
            22H19.5148V18.0999C19.5148 17.1704 19.4961 15.9784 18.2213
            15.9784C16.9279 15.9784 16.7296 16.9883 16.7296
            18.0329V22H14.2445V13.9882H16.6305V15.0811H16.6654C16.9975 14.4516
            17.8088 13.7873 19.0193 13.7873C21.5371 13.7873 22 15.4454 22
            17.599V22H21.9973Z" fill="#FFADBC" />
          </svg>
        </div>
      </div>
      <div class="f-link-wrapper">
        <div class="f-l-title">
          <a href="{{ url('docs/MyAll Privacy Policy.pdf') }}" class="link"
            target="_blank">Privacy Policy</a>
          <svg width="1" height="18" viewBox="0 0 1 18"
            fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M0.5 0.962891V17.9629" stroke="#929BCA"
              stroke-miterlimit="3.86874" stroke-linejoin="round" />
          </svg>
          <a href="{{ url('docs/MyAll Terms and Conditions.pdf') }}"
            class="link" target="_blank">Terms & Conditions</a>
          <svg width="1" height="18" viewBox="0 0 1 18"
            fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M0.5 0.962891V17.9629" stroke="#929BCA"
              stroke-miterlimit="3.86874" stroke-linejoin="round" />
          </svg>
          <a href="{{ url('docs/MyAll PAIA Manual.pdf') }}" class="link"
            target="_blank">PAIA Manual</a>
        </div>
      </div>
      <div style="margin-top: 1rem">
        <div
          style="text-align: center; font-size: 0.6rem; color:
        var(--theme-light-pink)">
          &copy; Copyright
          2024 MYALL SA (Pty)
          Ltd
          (2024/494838/07). All Rights
          Reserved</div>
      </div>
    </footer>
  </section>

  <div class="modal" tabindex="-1" id="info-modal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-body">
          <div class="text-end"><button type="button" class="btn-close"
              data-bs-dismiss="modal" aria-label="Close"></button></div>
          <div class="my-3">
            <b class="text-blue">Please select Infographics below for helpful
              information</b>
          </div>
          <div class="list-group">
            @foreach ($info_graphics as $k => $v)
              <a href="#" class="list-group-item list-group-item-action"
                wire:click.prevent="showSingleGraphic('{{ $v }}')">
                {{ $k }}
              </a>
            @endforeach
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="modal" tabindex="-1" id="sigle-info-modal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-body">
          <div class="text-end"><button type="button" class="btn-close"
              data-bs-dismiss="modal" aria-label="Close"></button></div>
          <div class="">
            <img src="" id="cur_img" class="w-100">
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal" tabindex="-1" id="tip-modal">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-body">
          <div class="d-flex">
            <h3>{{ $tip_title }}</h3>
            <span class="ms-auto">
              <button type="button" class="btn-close"
                data-bs-dismiss="modal" aria-label="Close"></button>
            </span>
          </div>
          <p class="tip-body">{{ $tip_body }}</p>
        </div>
      </div>
    </div>
  </div>

  <div class="modal" tabindex="-1" id="disclaimer-modal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-body">
          <div class="d-flex">
            <h5 class="modal-title">Legal Content Disclaimer</h5>
            <span class="ms-auto">
              <button type="button" class="btn-close"
                data-bs-dismiss="modal" aria-label="Close"></button>
            </span>
          </div>
          <div class="mt-3">
            <p>The information contained on this website is aimed at providing
              members of the public with guidance on the law in South Africa.
            </p>
            <p>This information has not been provided to meet the individual
              requirements of a specific person and MyAll insists that legal
              advice be obtained to address a person’s unique circumstances.</p>
            <p>It is important to remember that the law is constantly changing
              and although MyAll strives to keep the information up to date,
              MyAll cannot guarantee that the information will be updated
              and/or be without errors or omissions.</p>
            <p>As a result, MyAll, its employees, independent contractors,
              associates or third parties will under no circumstances accept
              liability or be held liable for any actions or omissions by MyAll
              which may result in any harm or liability flowing from the use of
              or the inability to use the information provided.</p>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-outline-two"
            data-bs-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

  @push('scripts')
    <script>
      $(document).ready(function() {
        window.addEventListener('show-tip-modal', event => {
          $('#tip-modal').modal('show');
        });
        window.addEventListener('showInfoModal', event => {
          $('#info-modal').modal("show");
        });
        window.addEventListener('showSingleInfoModal', event => {
          $('.modal').each(function() {
            $(this).modal('hide');
          });
          $('#cur_img').attr('src', event.detail.img);
          $('#sigle-info-modal').modal('show');
        });
        window.addEventListener('go-to-bottom', event => {
          $("html, body").animate({
            scrollTop: $(document).height()
          }, 1000);
        });
      });
    </script>
  @endpush

</div>
