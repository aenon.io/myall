  <section class="wrapper section-2 __forgot-password __reset">

    <div class="__island">
      <div class="__heading">
        <h2>Reset Password</h2>
      </div>
      <div class="__body">
        <div class="__form">
          <form wire:submit.prevent="updatePassword">
            @if ($errors->any())
              <div class="alert alert-danger">
                <span>{{ $errors->first() }}</span>
              </div>
            @endif
            <div class="">
              <div class="">
                <label class="form-label">Email address</label>
                <input type="email" class="form-control" name="email"
                  wire:model.defer="email">
              </div>
            </div>
            <div class="__password-row">
              <div class="">
                <label class="form-label">Password</label>
                <div class="input-group mb-3">
                  <input
                    @if ($show_pass) type="text" @else type="password" @endif
                    class="form-control password_with_toggle"
                    placeholder="Password*" name="password"
                    wire:model="password">
                  <span class="input-group-text password_toggle"
                    wire:click.prevent="showPass"><i
                      class="fa @if ($show_pass) fa-eye-slash @else fa-eye @endif"
                      id="togglePassword"></i></span>
                </div>
              </div>
              <div class="__password-strength-content">
                <div class="">
                  <div class="__progress-container">
                    <small class="text-muted">Weak</small>
                    <div class="progress password-strength-cont">
                      <div
                        class="progress-bar password-strength-progress {{ $pass_bg }} text-center"
                        role="progressbar"
                        style="width: @if ($password_score == 0) 0%; @else {{ $password_score }}%; @endif"
                        aria-valuenow="{{ $password_score }}" aria-valuemin="0"
                        aria-valuemax="100">
                      </div>
                    </div>
                    <small class="text-muted">Strong</small>
                  </div>
                </div>
                <small>
                  {{ $password_warning }}
                </small>
                <div class="">
                  <small>Click <a {{-- href="{{ url('docs/MyAll FAQs & Password Policy.pdf') }}" --}} href=""
                      target="_blank"><b class="text-blue">here</b></a>
                    to view the Password Policy and FAQs</small>
                </div>
              </div>
            </div>
            <div class="">
              <div class="">
                <label class="form-label">Password Confirmation</label>
                <div class="input-group mb-3">
                  <input
                    @if ($show_pass) type="text" @else type="password" @endif
                    class="form-control password_with_toggle"
                    placeholder="Confirm Password*" name="password_confirmation"
                    wire:model.defer="password_confirmation">
                  <span class="input-group-text password_toggle"
                    wire:click.prevent="showPass"><i
                      class="fa @if ($show_pass) fa-eye-slash @else fa-eye @endif"
                      id="togglePassword"></i></span>
                </div>
              </div>
            </div>
            <div class="">
              <input type="submit" class="btn btn-red rounded-pill"
                value="RESET PASSWORD">
            </div>
          </form>
        </div>
      </div>
    </div>

    {{-- <div class="container-card"> --}}
    {{--   <div class="mb-5 mt-5"> --}}
    {{--     <div class="px-0"> --}}
    {{--       <div class="container"> --}}
    {{--         <div class="card"> --}}
    {{--           <div class="card-body"> --}}
    {{--             <form wire:submit.prevent="updatePassword"> --}}
    {{--               <div class="row"> --}}
    {{--                 @if ($errors->any()) --}}
    {{--                   <div class="alert alert-danger"> --}}
    {{--                     <span>{{ $errors->first() }}</span> --}}
    {{--                   </div> --}}
    {{--                 @endif --}}
    {{--                 <div class="col-md-12"> --}}
    {{--                   <h2>Reset Password</h2> --}}
    {{--                 </div> --}}
    {{--                 <div class="col-md-12"> --}}
    {{--                   <div class="mb-3"> --}}
    {{--                     <label class="form-label">Email address</label> --}}
    {{--                     <input type="email" class="form-control" --}}
    {{--                       name="email" wire:model.defer="email"> --}}
    {{--                   </div> --}}
    {{--                 </div> --}}
    {{--                 <div class="col-md-12"> --}}
    {{--                   <div class="mb-3"> --}}
    {{--                     <label class="form-label">Password</label> --}}
    {{--                     <div class="input-group mb-3"> --}}
    {{--                       <input --}}
    {{--                         @if ($show_pass) type="text" @else type="password" @endif --}}
    {{--                         class="form-control password_with_toggle" --}}
    {{--                         placeholder="Password*" name="password" --}}
    {{--                         wire:model="password"> --}}
    {{--                       <span class="input-group-text password_toggle" --}}
    {{--                         wire:click.prevent="showPass"><i --}}
    {{--                           class="fa @if ($show_pass) fa-eye-slash @else fa-eye @endif" --}}
    {{--                           id="togglePassword"></i></span> --}}
    {{--                     </div> --}}
    {{--                   </div> --}}
    {{--                   <div class="row"> --}}
    {{--                     <div class="col-md-12"> --}}
    {{--                       <small class="text-muted">Password strength</small> --}}
    {{--                       <div class="progress password-strength-cont"> --}}
    {{--                         <div --}}
    {{--                           class="progress-bar password-strength-progress {{ $pass_bg }} text-center" --}}
    {{--                           role="progressbar" --}}
    {{--                           style="width: @if ($password_score == 0) 5%; @else {{ $password_score }}%; @endif" --}}
    {{--                           aria-valuenow="{{ $password_score }}" --}}
    {{--                           aria-valuemin="0" aria-valuemax="100"> --}}
    {{--                           {{ $password_score }}% --}}
    {{--                         </div> --}}
    {{--                       </div> --}}
    {{--                       <small class="text-muted"> --}}
    {{--                         {{ $password_warning }} --}}
    {{--                       </small> --}}
    {{--                     </div> --}}
    {{--                     <div class="col-md-12"> --}}
    {{--                       <p>Click <a --}}
    {{--                           href="{{ url('docs/MyAll FAQs & Password Policy.pdf') }}" --}}
    {{--                           target="_blank"><b class="text-blue">here</b></a> --}}
    {{--                         to view the Password Policy and FAQs</p> --}}
    {{--                     </div> --}}
    {{--                   </div> --}}
    {{--                 </div> --}}
    {{--                 <div class="col-md-12"> --}}
    {{--                   <div class="mb-3"> --}}
    {{--                     <label class="form-label">Password Confirmation</label> --}}
    {{--                     <div class="input-group mb-3"> --}}
    {{--                       <input --}}
    {{--                         @if ($show_pass) type="text" @else type="password" @endif --}}
    {{--                         class="form-control password_with_toggle" --}}
    {{--                         placeholder="Confirm Password*" --}}
    {{--                         name="password_confirmation" --}}
    {{--                         wire:model.defer="password_confirmation"> --}}
    {{--                       <span class="input-group-text password_toggle" --}}
    {{--                         wire:click.prevent="showPass"><i --}}
    {{--                           class="fa @if ($show_pass) fa-eye-slash @else fa-eye @endif" --}}
    {{--                           id="togglePassword"></i></span> --}}
    {{--                     </div> --}}
    {{--                   </div> --}}
    {{--                 </div> --}}
    {{--                 <div class="col-md-12"> --}}
    {{--                   <input type="submit" class="btn btn-red rounded-pill" --}}
    {{--                     value="RESET PASSWORD"> --}}
    {{--                 </div> --}}
    {{--               </div> --}}
    {{--             </form> --}}
    {{--           </div> --}}
    {{--         </div> --}}
    {{--       </div> --}}
    {{--     </div> --}}
    {{--   </div> --}}
    {{-- </div> --}}
  </section>
