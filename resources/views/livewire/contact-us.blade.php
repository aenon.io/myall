{{-- ISSUE: This component needs to be tested. Form fields should be populated
with the data from the current user, invisibly in the background. --}}

<div class="myallMainAppInner">
  <div class="">
    <div class="myallContactUs">
      <livewire:account.partials.my-search-bar />
      <div class="myallContactUsInner card-box-style">
        <div class="">
          <div class="__heading">
            <h3 class="page-title h3-title-style">Get in touch</h3>
          </div>

          <div class="__island">
            <div class="__info">
              <div>
                <img src="{{ asset('img/18a3eae234.jpg') }}"
                  alt="Get in touch with MyAll">
              </div>
              <div class="__info-inner">
                <div class="__info-item">
                  <p class="__title">For more info</p>
                  <p>
                    <a href=""><svg width="21" height="20"
                        viewBox="0 0 21 20" fill="none"
                        xmlns="http://www.w3.org/2000/svg">
                        <path d="M18.901 4.9987C18.901 4.08203 18.151 3.33203
                                            17.2344 3.33203H3.90104C2.98438
                                            3.33203 2.23438 4.08203 2.23438
                                            4.9987V14.9987C2.23438 15.9154
                                            2.98438 16.6654 3.90104
                                            16.6654H17.2344C18.151 16.6654
                                            18.901 15.9154 18.901
                                            14.9987V4.9987ZM17.2344
                                            4.9987L10.5677 9.16536L3.90104
                                            4.9987H17.2344ZM17.2344
                                            14.9987H3.90104V6.66536L10.5677
                                            10.832L17.2344 6.66536V14.9987Z" />
                      </svg>
                      hello@myall.co.za
                    </a>
                  </p>
                </div>
                <hr>
                <div class="__info-item">
                  <p class="__title">For security related issues</p>
                  <p>
                    <a href=""><svg width="21" height="20"
                        viewBox="0 0 21 20" fill="none"
                        xmlns="http://www.w3.org/2000/svg">
                        <path d="M18.901 4.9987C18.901 4.08203 18.151 3.33203
                                            17.2344 3.33203H3.90104C2.98438
                                            3.33203 2.23438 4.08203 2.23438
                                            4.9987V14.9987C2.23438 15.9154
                                            2.98438 16.6654 3.90104
                                            16.6654H17.2344C18.151 16.6654
                                            18.901 15.9154 18.901
                                            14.9987V4.9987ZM17.2344
                                            4.9987L10.5677 9.16536L3.90104
                                            4.9987H17.2344ZM17.2344
                                            14.9987H3.90104V6.66536L10.5677
                                            10.832L17.2344 6.66536V14.9987Z" />
                      </svg>
                      reporting@myall.co.za
                    </a>
                  </p>
                </div>
              </div>
            </div>

            <div class="__message">
              <h3>Message Us</h3>
              <hr>
              <div class="__textarea">

                {{-- <label class="form-label">How can we help you?</label> --}}
                <textarea class="form-control-contact text-area-form"
                  placeholder="Enter message" wire:model.defer="message"></textarea>

              </div>
              <div class="__accept-terms">
                <div class="">
                  <div class="form-check">
                    <input class="form-check-input" type="checkbox"
                      name="flexRadioDefault" id="flexRadioDefault1"
                      wire:model.defer="accept_terms">
                  </div>
                </div>
                <div class="">
                  <p style="" class="h3-width h3-m-bottom">I have read the
                    <a href="{{ url('/docs/MyAll Terms and Conditions.pdf') }}"
                      target="blank" class="text-pink link-font-bold">Terms &
                      Conditions</a> and <a
                      href="{{ url('/docs/MyAll Privacy Policy.pdf') }}"
                      target="blank" class="text-pink link-font-bold">Privacy
                      Policy</a>, and give consent to MyAll to share and
                    manage my data in accordance with the POPI Act.
                  </p>
                </div>
              </div>
              <hr>
              <div class="__submit">
                <a href="#" class="btn"
                  wire:click.prevent="submitContactForm">
                  <svg width="20" height="20" viewBox="0 0 20 20"
                    fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M2.925 5.025L9.18333 7.70833L2.91667 6.875L2.925
                                            5.025ZM9.175 12.2917L2.91667
                                            14.975V13.125L9.175 12.2917ZM1.25833
                                            2.5L1.25 8.33333L13.75 10L1.25
                                            11.6667L1.25833 17.5L18.75
                                            10L1.25833 2.5Z" fill="white" />
                  </svg>
                  Send
                </a>
              </div>


            </div>
          </div>

          {{--     <div class="col-lg-8"> --}}
          {{--       <div class="row"> --}}
          {{--         @if ($errors->any()) --}}
          {{--           <div class="alert alert-danger"> --}}
          {{--             <span>{{ $errors->first() }}</span> --}}
          {{--           </div> --}}
          {{--         @endif --}}
          {{--         @if (session()->has('message')) --}}
          {{--           <div class="alert alert-success"> --}}
          {{--             <span>{{ session('message') }}</span> --}}
          {{--           </div> --}}
          {{--         @endif --}}
          {{--         <div class="col-md-6"> --}}
          {{--           <div class="marg-top-lab mb-3"> --}}
          {{--             <label class="form-label">Name</label> --}}
          {{--             <input type="text" class="form-control-contact" --}}
          {{--               placeholder="Name*" wire:model.defer="name"> --}}
          {{--           </div> --}}
          {{--         </div> --}}
          {{--         <div class="col-md-6"> --}}
          {{--           <div class="marg-top-lab mb-3"> --}}
          {{--             <label class="form-label">Surname</label> --}}
          {{--             <input type="text" class="form-control-contact" --}}
          {{--               placeholder="Surname*" wire:model.defer="surname"> --}}
          {{--           </div> --}}
          {{--         </div> --}}
          {{--         <div class="col-md-6"> --}}
          {{--           <div class="mb-3"> --}}
          {{--             <label class="form-label">Contact Number</label> --}}
          {{--             <input type="text" class="form-control-contact" --}}
          {{--               placeholder="Contact Number*" --}}
          {{--               wire:model.defer="contact_number"> --}}
          {{--           </div> --}}
          {{--         </div> --}}
          {{--         <div class="col-md-6"> --}}
          {{--           <div class="mb-3"> --}}
          {{--             <label class="form-label">Email</label> --}}
          {{--             <input type="text" class="form-control-contact" --}}
          {{--               placeholder="Email*" wire:model.defer="email"> --}}
          {{--           </div> --}}
          {{--         </div> --}}
          {{--         <div class="col-md-12"> --}}
          {{--           <div class="mb-3"> --}}
          {{--             <label class="form-label">How can we help you?</label> --}}
          {{--             <textarea class="form-control-contact text-area-form" --}}
          {{--               placeholder="Have any questions or need some info?" --}}
          {{--               wire:model.defer="message"></textarea> --}}
          {{--           </div> --}}
          {{--         </div> --}}
          {{--       </div> --}}
          {{--     </div> --}}
          {{--     <div class="col-lg-4 padding-left-60"> --}}
          {{--       <div class="row"> --}}
          {{--         <div class="col-md-12 mt-5"> --}}
          {{--           <h3 class="page-title h3-style">Submit your request</h3> --}}
          {{--           <p class="text-pink h3-width h3-m-bottom"><b --}}
          {{--               class="text-pink">Have any questions or need more --}}
          {{--               information? Let’s get in touch.</b></p> --}}
          {{--         </div> --}}
          {{--         <div class="display-flex h3-mail-block"> --}}
          {{--           <div class="icon-div"> --}}
          {{--             <img class="icon-margin" --}}
          {{--               src="{{ asset('img/contact/email_icon.png') }}" --}}
          {{--               alt="Images"> --}}
          {{--           </div> --}}
          {{--           <div> --}}
          {{--             <a href="mailto:hello@wrapup.co.za" --}}
          {{--               class="link-txt-style">hello@wrapup.co.za</a> --}}
          {{--           </div> --}}
          {{--         </div> --}}
          {{--         <div class="col-md-12 mt-2"> --}}
          {{--           <p class="text-pink h3-width h3-m-bottom"><b --}}
          {{--               class="text-pink">For any security related matters, --}}
          {{--               contact --}}
          {{--               us on</b></p> --}}
          {{--         </div> --}}
          {{--         <div class="display-flex h3-mail-block"> --}}
          {{--           <div class="icon-div"> --}}
          {{--             <img class="icon-margin" --}}
          {{--               src="{{ asset('img/contact/email_icon.png') }}" --}}
          {{--               alt="Images"> --}}
          {{--           </div> --}}
          {{--           <div> --}}
          {{--             <a href="mailto:reporting@wrapup.co.za" --}}
          {{--               class="link-txt-style">reporting@wrapup.co.za</a> --}}
          {{--           </div> --}}
          {{--         </div> --}}
          {{--         <div class="col-md-1"> --}}
          {{--           <div class="form-check"> --}}
          {{--             <input class="form-check-input" type="checkbox" --}}
          {{--               name="flexRadioDefault" id="flexRadioDefault1" --}}
          {{--               wire:model.defer="accept_terms"> --}}
          {{--           </div> --}}
          {{--         </div> --}}
          {{--         <div class="col-md-9"> --}}
          {{--           <p style="font-size: 12px;letter-spacing: 1.2px;" --}}
          {{--             class="h3-width h3-m-bottom">I have read the <a --}}
          {{--               href="{{ url('/docs/MyAll Terms and Conditions.pdf') }}" --}}
          {{--               target="blank" class="text-pink link-font-bold">Terms & --}}
          {{--               Conditions</a> and <a --}}
          {{--               href="{{ url('/docs/MyAll Privacy Policy.pdf') }}" --}}
          {{--               target="blank" class="text-pink link-font-bold">Privacy --}}
          {{--               Policy</a>, and give consent to WrapUp to share and manage --}}
          {{--             my data in accordance with the POPI Act.</p> --}}
          {{--           <a href="#" class="btn btn-filled-pink" --}}
          {{--             wire:click.prevent="submitContactForm">SUBMIT --}}
          {{--             REQUEST</a> --}}
          {{--         </div> --}}
          {{--       </div> --}}
          {{--     </div> --}}
          {{--   </div> --}}
          {{-- </div> --}}




        </div>
        <div class="row d-block d-lg-none">
          <div class="col-md-12">
            <div style="margin-bottom: 100px"></div>
          </div>
        </div>
      </div>
      <div class="modal" tabindex="-1" id="contact_modal" wire:ignore.self>
        <div class="modal-dialog modal-dialog-centered">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Add Contact</h5>
              <button type="button" class="btn-close" data-bs-dismiss="modal"
                aria-label="Close"></button>
            </div>
            <div class="modal-body">
              <form wire:submit.prevent="saveContact">
                <div class="mb-3">
                  <label class="form-label">Name</label>
                  <input type="text" class="form-control" name="name"
                    wire:model.defer="contact_name">
                </div>
                <div class="mb-3">
                  <label class="form-label">Surname</label>
                  <input type="text" class="form-control" name="surname"
                    wire:model.defer="contact_surname">
                </div>
                <div class="mb-3">
                  <label class="form-label">Email</label>
                  <input type="email" class="form-control" name="email"
                    wire:model.defer="contact_email">
                </div>
                <div class="mb-3">
                  <label class="form-label">Contact Number</label>
                  <input type="text" class="form-control"
                    name="contact_number" wire:model.defer="contact_number">
                </div>
                <div class="mb-3">
                  <label class="form-label">Relation</label>
                  <input type="text" class="form-control" name="relation"
                    wire:model.defer="relation">
                </div>

                <div class="mb-3">
                  <label class="form-label">Organisation / Company</label>
                  <input type="text" class="form-control"
                    name="organisation_or_company"
                    wire:model.defer="organisation_or_company">
                </div>
                <div class="mb-3 text-center">
                  <input type="submit" class="btn btn-red" value="save">
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
