<div>
  <div style="background-color: var(--theme-light-blue)">
    <section class="wrapper hero-bg bg-overlay bg-overlay-400 bg-content"
      style="background-image: url({{ asset('img/4a0b6e58af.jpg') }}); background-size: cover; background-repeat: no-repeat; ">
      <div class="hero-content container" style="z-index: 5; position:relative">
        <div class="row justify-content-center">
          <div class="content order-md-1 order-2 text-center"
            data-cues="slideInDown" data-group="page-title" data-delay="600">
            <h1 class="display-2 mb-1">Stay Connected &amp; Organised with MyAll
              -
              Your Personal Life Management Hub!</h1>
            <h2 class="mb-6">MyAll is your all-in-one digital assistant,
              securely storing and
              managing your life’s essential information.</h2>
            <h3 class="mb-6">It guides you on what to
              keep and highlights what's important. Easily accessible,
              shareable,
              and manageable.</h3>
            <div class="d-flex justify-content-center" data-cues="slideInDown"
              data-group="page-title-buttons" data-delay="900">
              <span>
                <a href="#pricing"
                  class="btn btn-gradient btn-lg rounded-pill btn-wire btn-start">
                  Let's get started
                </a>
              </span>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <div class="section-wrapper">
    <section>
      <div class="welcome-section-wrapper">
        <div class="welcome-content-wrapper">
          <div class="lifebud-svg-container">
            <img id="" class="lifebud"
              src="{{ asset('img/lifebudimg.png') }}" alt="">
          </div>
          <div class="w-content-container">
            <div class="w-oval-wrapper">
              <div class="w-oval">
                <p class="lifebud">Meet 'Keeper', Your MyAll guide</p>
              </div>
            </div>
            <div class="w-heading-wrapper">
              <div class="w-heading">
                <p>Hey champs, welcome to the new way of doing things.</p>
              </div>
            </div>
            <div class="whr">
              <hr class="w-hr">
            </div>
            <div class="w-content-container2">
              <div class="w-content">
                <p class="lifebud">We're here to make your life a breeze, time
                  to
                  give your paperwork the
                  boot. MyAll guides you through what information really
                  matters,
                  preparing you for life's
                  emergencies and giving you easy access to information whenever
                  you need it. <b>We've got
                    your back.</b></p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section>
      <span id="what-is-myall" class="__anchor"></span>
      <div class="intro-wrapper">
        <div class="intro-grid">
          <div class="intro-content">
            <div class="myall-small">
              <p class="oval-content">What is MyAll</p>
            </div>
            <div class="heading-container">
              <h1 class="heading-body">Organise important documents with our
                <b>MyAll Life Organiser</b> and
                create your digital will with ease.
              </h1>
            </div>
            <div class="intro-content">
              <hr class="rule-body">
              <p class="content">Gone are the days of overflowing file cabinets
                and frantic searches for
                important documents. Introducing MyAll - your digital vault for
                life's most crucial
                information, neatly organised and accessible with a tap. This
                proudly South African platform
                is like Facebook, but for the things that truly matter - medical
                records, legal docs,
                insurance policies, and more. No more scrambling at the doctor's
                office - your entire health
                profile lives here.</p>
              <p class="content">MyAll goes beyond just robust document storage.
                It combines the practical
                with life's special memories, letting you store updates and
                share
                with your MyAll community
                in an interactive social space. Experience life's admin, made
                easy
                and enjoyable. Ditch the
                clutter and bring order to your world's pivotal events and
                information. With MyAll, you've
                got a space as lekker as you are for managing life's biggest
                moments.</p>
              <p><b>Don't admin, MyAll it!</b></p>
              <div class="listitembox">
                <svg class="svg" width="20" height="20"
                  viewBox="0 0 20 20" fill="none"
                  xmlns="http://www.w3.org/2000/svg">
                  <path d="M7.99167 10.8332C8.45 10.8332 8.825 11.2082 8.825
                    11.6665C8.825 12.1248 8.45 12.4998 7.99167 12.4998C7.53334
                    12.4998 7.15834 12.1248 7.15834 11.6665C7.15834 11.2082
                    7.53334 10.8332 7.99167 10.8332ZM7.99167 9.1665C6.60834
                    9.1665 5.49167 10.2832 5.49167 11.6665C5.49167 13.0498
                    6.60834 14.1665 7.99167 14.1665C9.375 14.1665 10.4917
                    13.0498 10.4917 11.6665C10.4917 10.2832 9.375 9.1665 7.99167
                    9.1665ZM15.075 7.49984L15.9833 5.4915L17.9917
                    4.58317L15.9833 3.67484L15.075 1.6665L14.1667
                    3.67484L12.1583 4.58317L14.1667 5.4915L15.075
                    7.49984ZM17.3917 10.5998L16.7417 9.1665L16.0917
                    10.5998L14.6583 11.2498L16.0917 11.8998L16.7417
                    13.3332L17.3917 11.8998L18.825 11.2498L17.3917 10.5998ZM13.2
                    11.6665C13.2 11.5665 13.2 11.4582 13.1917 11.3582L14.8083
                    10.1332L12.725 6.52484L10.8583 7.30817C10.6917 7.19984
                    10.5083 7.0915 10.325 6.99984L10.075 4.99984H5.90834L5.65834
                    7.00817C5.475 7.09984 5.3 7.20817 5.125 7.3165L3.25834
                    6.52484L1.175 10.1332L2.79167 11.3582C2.78334 11.4582
                    2.78334 11.5665 2.78334 11.6665C2.78334 11.7665 2.78334
                    11.8748 2.79167 11.9748L1.175 13.1998L3.25834 16.8082L5.125
                    16.0248C5.29167 16.1332 5.475 16.2415 5.65834
                    16.3332L5.90834 18.3332H10.075L10.325 16.3248C10.5083
                    16.2332 10.6833 16.1332 10.8583 16.0165L12.725
                    16.7998L14.8083 13.1915L13.1917 11.9665C13.2 11.8748 13.2
                    11.7665 13.2 11.6665ZM12.0167 14.6998L10.575 14.0915C10.1083
                    14.5915 9.49167 14.9582 8.8 15.1165L8.6
                    16.6665H7.38334L7.19167 15.1165C6.5 14.9582 5.88334 14.5915
                    5.41667 14.0915L3.975 14.6998L3.36667 13.6415L4.60834
                    12.6998C4.50834 12.3748 4.45834 12.0332 4.45834
                    11.6748C4.45834 11.3165 4.50834 10.9748 4.60834
                    10.6498L3.36667 9.70817L3.975 8.64984L5.41667
                    9.25817C5.88334 8.75817 6.5 8.3915 7.19167 8.23317L7.38334
                    6.6665H8.60834L8.8 8.2165C9.49167 8.37484 10.1083 8.7415
                    10.575 9.2415L12.0167 8.63317L12.625 9.6915L11.3833
                    10.6332C11.4833 10.9582 11.5333 11.2998 11.5333
                    11.6582C11.5333 12.0165 11.4833 12.3582 11.3833
                    12.6832L12.625 13.6248L12.0167 14.6998Z" fill="#040A5E" />
                </svg>
                <div>
                  <p class="listitemtitle"><b>All-in-one Life Organiser: Your
                      own
                      digital life story</b>
                  </p>
                  <p class="listitemcontent">Easily organise all documents and
                    personal info in one
                    user-friendly platform.
                  <p>
                </div>
              </div>
              <div class="listitembox">
                <svg class="svg" width="20" height="20"
                  viewBox="0 0 20 20" fill="none"
                  xmlns="http://www.w3.org/2000/svg">
                  <path d="M13.75
                    16.4335H3.74999V6.25016H9.58333V4.5835H3.74999C2.83333
                    4.5835 2.08333 5.3335 2.08333 6.25016V16.2502C2.08333
                    17.1668 2.83333 17.9168 3.74999 17.9168H13.75C14.6667
                    17.9168 15.4167 17.1668 15.4167
                    16.2502V10.4168H13.75V16.4335Z" fill="#040A5E" />
                  <path d="M15.4167 2.0835H13.75V4.5835H11.25C11.2583 4.59183 11.25
                    6.25016 11.25 6.25016H13.75V8.74183C13.7583 8.75016 15.4167
                    8.74183 15.4167
                    8.74183V6.25016H17.9167V4.5835H15.4167V2.0835Z"
                    fill="#040A5E" />
                  <path d="M12.0833 7.91683H5.41666V9.5835H12.0833V7.91683Z"
                    fill="#040A5E" />
                  <path d="M5.41666 10.4168V12.0835H12.0833V10.4168H5.41666Z"
                    fill="#040A5E" />
                  <path d="M12.0833 12.9168H5.41666V14.5835H12.0833V12.9168Z"
                    fill="#040A5E" />
                </svg>
                <div>
                  <p class="listitemtitle"><b>DIY Online Will</b></p>
                  <p class="listitemcontent">Create, download, and update your
                    Will online with
                    expert
                    guidance.</p>
                </div>
              </div>
              <div class="listitembox">
                <svg class="svg" width="20" height="20"
                  viewBox="0 0 20 20" fill="none"
                  xmlns="http://www.w3.org/2000/svg">
                  <path d="M15 13.4335C14.3667 13.4335 13.8 13.6835 13.3667
                    14.0752L7.425 10.6169C7.46667 10.4252 7.5 10.2335 7.5
                    10.0335C7.5 9.83353 7.46667 9.64186 7.425 9.45019L13.3
                    6.0252C13.75 6.44186 14.3417 6.70019 15 6.70019C16.3833
                    6.70019 17.5 5.58353 17.5 4.2002C17.5 2.81686 16.3833 1.7002
                    15 1.7002C13.6167 1.7002 12.5 2.81686 12.5 4.2002C12.5
                    4.4002 12.5333 4.59186 12.575 4.78353L6.7 8.20853C6.25
                    7.79186 5.65833 7.53353 5 7.53353C3.61667 7.53353 2.5
                    8.65019 2.5 10.0335C2.5 11.4169 3.61667 12.5335 5
                    12.5335C5.65833 12.5335 6.25 12.2752 6.7 11.8585L12.6333
                    15.3252C12.5917 15.5002 12.5667 15.6835 12.5667
                    15.8669C12.5667 17.2085 13.6583 18.3002 15 18.3002C16.3417
                    18.3002 17.4333 17.2085 17.4333 15.8669C17.4333 14.5252
                    16.3417 13.4335 15 13.4335ZM15 3.36686C15.4583 3.36686
                    15.8333 3.74186 15.8333 4.2002C15.8333 4.65853 15.4583
                    5.03353 15 5.03353C14.5417 5.03353 14.1667 4.65853 14.1667
                    4.2002C14.1667 3.74186 14.5417 3.36686 15 3.36686ZM5
                    10.8669C4.54167 10.8669 4.16667 10.4919 4.16667
                    10.0335C4.16667 9.57519 4.54167 9.20019 5 9.20019C5.45833
                    9.20019 5.83333 9.57519 5.83333 10.0335C5.83333 10.4919
                    5.45833 10.8669 5 10.8669ZM15 16.7169C14.5417 16.7169
                    14.1667 16.3419 14.1667 15.8835C14.1667 15.4252 14.5417
                    15.0502 15 15.0502C15.4583 15.0502 15.8333 15.4252 15.8333
                    15.8835C15.8333 16.3419 15.4583 16.7169 15 16.7169Z"
                    fill="#040A5E" />
                </svg>
                <div>
                  <p class="listitemtitle"><b>Secure Sharing</b></p>
                  <p class="listitemcontent">Share vital info with loved ones
                    and
                    advisors, enhancing
                    convenience during emergencies.</p>
                </div>
              </div>
              <div class="listitembox">
                <svg class="svg" width="20" height="20"
                  viewBox="0 0 20 20" fill="none"
                  xmlns="http://www.w3.org/2000/svg">
                  <path d="M15.8333 3.33317H12.35C12 2.3665 11.0833 1.6665 10
                    1.6665C8.91667 1.6665 8 2.3665 7.65 3.33317H4.16667C3.25
                    3.33317 2.5 4.08317 2.5 4.99984V16.6665C2.5 17.5832 3.25
                    18.3332 4.16667 18.3332H15.8333C16.75 18.3332 17.5 17.5832
                    17.5 16.6665V4.99984C17.5 4.08317 16.75 3.33317 15.8333
                    3.33317ZM10 3.12484C10.1833 3.12484 10.3417 3.20817 10.4583
                    3.33317C10.5583 3.4415 10.625 3.5915 10.625 3.74984C10.625
                    4.0915 10.3417 4.37484 10 4.37484C9.65833 4.37484 9.375
                    4.0915 9.375 3.74984C9.375 3.5915 9.44167 3.4415 9.54167
                    3.33317C9.65833 3.20817 9.81667 3.12484 10 3.12484ZM15.8333
                    16.6665H4.16667V4.99984H15.8333V16.6665ZM10 5.83317C8.625
                    5.83317 7.5 6.95817 7.5 8.33317C7.5 9.70817 8.625 10.8332 10
                    10.8332C11.375 10.8332 12.5 9.70817 12.5 8.33317C12.5
                    6.95817 11.375 5.83317 10 5.83317ZM10 9.1665C9.54167 9.1665
                    9.16667 8.7915 9.16667 8.33317C9.16667 7.87484 9.54167
                    7.49984 10 7.49984C10.4583 7.49984 10.8333 7.87484 10.8333
                    8.33317C10.8333 8.7915 10.4583 9.1665 10 9.1665ZM5
                    14.5582V15.8332H15V14.5582C15 12.4748 11.6917 11.5748 10
                    11.5748C8.30833 11.5748 5 12.4665 5 14.5582ZM6.925
                    14.1665C7.5 13.6998 8.90833 13.2332 10 13.2332C11.0917
                    13.2332 12.5083 13.6998 13.075 14.1665H6.925Z"
                    fill="#040A5E" />
                </svg>
                <div>
                  <p class="listitemtitle"><b>Your Trusted Representative</b>
                  </p>
                  <p class="listitemcontent">Have a backup person in place for
                    emergency situations in
                    case you pass away, for them to gain access to all your
                    information, making things
                    easier in a tough situation.</p>
                </div>
              </div>
              <hr class="rule-body">
              <a href="{{ url('register') }}" class="btn btn-default"
                role="button">
                Register Now
              </a>
            </div>
          </div>
          <div class="image">
            <img id="" class="diagram"
              src="{{ asset('img/diagram.png') }}" alt="">
          </div>
        </div>
      </div>
    </section>
    <section>
      <div class="get-notified-box">
        <div class="notif-content">
          <p class="signup heading">
            {{-- Stay updated about <b>MyAll</b> and the exciting features in development? --}}
            <strong>Life Hacks</strong>
          </p>
          <p class="signup subheading">
            Get in on the Good Stuff & Subscribe for free!
          </p>
          <p class="signup subheading">
            For all those things you didn't even know you needed to know—like
            handling family dramas, navigating legal tangles, surviving
            step-parent shenanigans.. and more. Who knew adulting could be this
            entertaining?
          </p>
          <p class="signup subheading small">
            "At MyAll, we exist to support and nurture the love and care people
            have for themselves and their families." – Cherene Cook, Founder and
            CEO of MyAll</p>
          <div class="notif-btn-container">
            <button href="#" class="btn btn-notif" role="button"
              wire:click.prevent="openMailingListModal">
              I'm in, Lets do this!
            </button>
          </div>
        </div>
        <div class="notif-img-wrapper">
          <img class="notifimage" src=" {{ asset('img/38ab9a0e40.jpg') }}">
        </div>
      </div>
    </section>
    <section>
      <span id="how-it-works" class="__anchor"></span>
      <div class="get-started-wrapper">
        <div class="h-oval-container">
          <div class="h-oval">
            <p class="how-works">How it works</p>
          </div>
        </div>
        <div class="g-title-container">
          <div class="g-title">
            <p class="get-started">Get started with MyAll</p>
          </div>
        </div>
        <div class="g-hr-container">
          <hr class="gs-hr">
        </div>
        <div class="g-body-container">
          <div class="g-body-content">
            <div class="wrapper1">
              <div class="gsvg-container">
                <svg width="70" height="70" viewBox="0 0 70 70"
                  fill="none" xmlns="http://www.w3.org/2000/svg">
                  <rect x="11" y="11" width="48" height="48"
                    rx="24" fill="white" />
                  <rect x="5.5" y="5.5" width="59" height="59"
                    rx="29.5" stroke="black" stroke-opacity="0.1"
                    stroke-width="11" />
                  <path d="M38.0085 26.0455V43.5H34.3182V29.5483H34.2159L30.2188
                    32.054V28.7812L34.5398 26.0455H38.0085Z" fill="#822384" />
                </svg>
              </div>
              <div class="gwrapper-content">
                <p class="g-stitle">Register and create your profile</p>
                <p class="g-content">The MyAll Life Organiser has a
                  user-friendly
                  interface and a super
                  secure platform.</p>
              </div>
            </div>
            <div class="wrapper2">
              <div class="gsvg-container">
                <svg width="70" height="70" viewBox="0 0 70 70"
                  fill="none" xmlns="http://www.w3.org/2000/svg">
                  <rect x="11" y="11" width="48" height="48"
                    rx="24" fill="white" />
                  <rect x="5.5" y="5.5" width="59" height="59"
                    rx="29.5" stroke="black" stroke-opacity="0.1"
                    stroke-width="11" />
                  <path d="M28.4148 43.5V40.8409L34.6278 35.0881C35.1563 34.5767
                    35.5994 34.1165 35.9574 33.7074C36.321 33.2983 36.5966
                    32.8977 36.7841 32.5057C36.9716 32.108 37.0653 31.679
                    37.0653 31.2188C37.0653 30.7074 36.9489 30.267 36.7159
                    29.8977C36.483 29.5227 36.1648 29.2358 35.7614
                    29.0369C35.358 28.8324 34.9006 28.7301 34.3892
                    28.7301C33.8551 28.7301 33.3892 28.8381 32.9915
                    29.054C32.5938 29.2699 32.2869 29.5795 32.071 29.983C31.8551
                    30.3864 31.7472 30.8665 31.7472 31.4233H28.2443C28.2443
                    30.2812 28.5028 29.2898 29.0199 28.4489C29.5369 27.608
                    30.2614 26.9574 31.1932 26.4972C32.125 26.0369 33.1989
                    25.8068 34.4148 25.8068C35.6648 25.8068 36.7528 26.0284
                    37.679 26.4716C38.6108 26.9091 39.3352 27.517 39.8523
                    28.2955C40.3693 29.0739 40.6278 29.9659 40.6278
                    30.9716C40.6278 31.6307 40.4972 32.2812 40.2358
                    32.9233C39.9801 33.5653 39.5227 34.2784 38.8636
                    35.0625C38.2045 35.8409 37.2756 36.7756 36.0767
                    37.8665L33.5284 40.3636V40.483H40.858V43.5H28.4148Z"
                    fill="#95297B" />
                </svg>
              </div>
              <div class="gwrapper-content">
                <p class="g-stitle">Start getting your files sorted</p>
                <p class="g-content">Access crucial information effortlessly
                  with
                  MyAll. Store personal and
                  medical details online for quick retrieval during doctor
                  visits,
                  work-related needs, or
                  emergencies.</p>
              </div>
            </div>
            <div class="wrapper3">
              <div class="gsvg-container">
                <svg width="70" height="70" viewBox="0 0 70 70"
                  fill="none" xmlns="http://www.w3.org/2000/svg">
                  <rect x="11" y="11" width="48" height="48"
                    rx="24" fill="white" />
                  <rect x="5.5" y="5.5" width="59" height="59"
                    rx="29.5" stroke="black" stroke-opacity="0.1"
                    stroke-width="11" />
                  <path d="M34.8665 43.7386C33.5938 43.7386 32.4602 43.5199 31.4659
                    43.0824C30.4773 42.6392 29.696 42.0312 29.1222
                    41.2585C28.554 40.4801 28.2614 39.5824 28.2443
                    38.5653H31.9602C31.983 38.9915 32.1222 39.3665 32.3778
                    39.6903C32.6392 40.0085 32.9858 40.2557 33.4176
                    40.4318C33.8494 40.608 34.3352 40.696 34.875 40.696C35.4375
                    40.696 35.9347 40.5966 36.3665 40.3977C36.7983 40.1989
                    37.1364 39.9233 37.3807 39.571C37.625 39.2187 37.7472
                    38.8125 37.7472 38.3523C37.7472 37.8864 37.6165 37.4744
                    37.3551 37.1165C37.0994 36.7528 36.7301 36.4687 36.2472
                    36.2642C35.7699 36.0597 35.2017 35.9574 34.5426
                    35.9574H32.9148V33.2472H34.5426C35.0994 33.2472 35.5909
                    33.1506 36.017 32.9574C36.4489 32.7642 36.7841 32.4972
                    37.0227 32.1562C37.2614 31.8097 37.3807 31.4062 37.3807
                    30.946C37.3807 30.5085 37.2756 30.125 37.0653
                    29.7955C36.8608 29.4602 36.571 29.1989 36.196
                    29.0114C35.8267 28.8239 35.3949 28.7301 34.9006
                    28.7301C34.4006 28.7301 33.9432 28.821 33.5284
                    29.0028C33.1136 29.179 32.7812 29.4318 32.5312
                    29.7614C32.2812 30.0909 32.1477 30.4773 32.1307
                    30.9205H28.5938C28.6108 29.9148 28.8977 29.0284 29.4545
                    28.2614C30.0114 27.4943 30.7614 26.8949 31.7045
                    26.4631C32.6534 26.0256 33.7244 25.8068 34.9176
                    25.8068C36.1222 25.8068 37.1761 26.0256 38.0795
                    26.4631C38.983 26.9006 39.6847 27.4915 40.1847
                    28.2358C40.6903 28.9744 40.9403 29.804 40.9347
                    30.7244C40.9403 31.7017 40.6364 32.517 40.0227
                    33.1705C39.4148 33.8239 38.6222 34.2386 37.6449
                    34.4148V34.5511C38.929 34.7159 39.9063 35.1619 40.5767
                    35.8892C41.2528 36.6108 41.5881 37.5142 41.5824
                    38.5994C41.5881 39.5938 41.3011 40.4773 40.7216
                    41.25C40.1477 42.0227 39.3551 42.6307 38.3438
                    43.0739C37.3324 43.517 36.1733 43.7386 34.8665 43.7386Z"
                    fill="#A62F73" />
                </svg>
              </div>
              <div class="gwrapper-content">
                <p class="g-stitle">Share vital information with your Contacts
                </p>
                <p class="g-content">Share vital information on MyAll with
                  others. When they accept your
                  sent request and register, they become your contacts, allowing
                  easy tracking of shared
                  details for better organisation and communication.</p>
              </div>
            </div>
            <div class="wrapper4">
              <div class="gsvg-container">
                <svg width="70" height="70" viewBox="0 0 70 70"
                  fill="none" xmlns="http://www.w3.org/2000/svg">
                  <rect x="11" y="11" width="48" height="48"
                    rx="24" fill="white" />
                  <rect x="5.5" y="5.5" width="59" height="59"
                    rx="29.5" stroke="black" stroke-opacity="0.1"
                    stroke-width="11" />
                  <path d="M27.6506 40.4318V37.5256L34.9375
                    26.0455H37.4432V30.0682H35.9602L31.3665
                    37.3381V37.4744H41.7216V40.4318H27.6506ZM36.0284
                    43.5V39.5455L36.0966 38.2585V26.0455H39.5568V43.5H36.0284Z"
                    fill="#BA366A" />
                </svg>
              </div>
              <div class="gwrapper-content">
                <p class="g-stitle">Memories</p>
                <p class="g-content">Explore Your Living Memories: Preserve and
                  Share Your Legacy. Gift
                  your memories, a timeless treasure that your family will
                  forever
                  cherish. With Living
                  Memories, leave a lasting impact for generations to come, to
                  be
                  embraced only once
                  you've bid farewell.</p>
              </div>
            </div>
            <div class="wrapper5">
              <div class="gsvg-container">
                <svg width="70" height="70" viewBox="0 0 70 70"
                  fill="none" xmlns="http://www.w3.org/2000/svg">
                  <rect x="11" y="11" width="48" height="48"
                    rx="24" fill="white" />
                  <rect x="5.5" y="5.5" width="59" height="59"
                    rx="29.5" stroke="black" stroke-opacity="0.1"
                    stroke-width="11" />
                  <path d="M34.7898 43.7386C33.5852 43.7386 32.5114 43.517 31.5682
                    43.0739C30.6307 42.6307 29.8864 42.0199 29.3352
                    41.2415C28.7841 40.4631 28.4972 39.571 28.4744
                    38.5653H32.054C32.0938 39.2415 32.3778 39.7898 32.9062
                    40.2102C33.4347 40.6307 34.0625 40.8409 34.7898
                    40.8409C35.3693 40.8409 35.8807 40.7131 36.3239
                    40.4574C36.7727 40.196 37.1222 39.8352 37.3722
                    39.375C37.6278 38.9091 37.7557 38.375 37.7557
                    37.7727C37.7557 37.1591 37.625 36.6193 37.3636
                    36.1534C37.108 35.6875 36.7528 35.3239 36.2983
                    35.0625C35.8438 34.8011 35.3239 34.6676 34.7386
                    34.6619C34.2273 34.6619 33.7301 34.767 33.2472
                    34.9773C32.7699 35.1875 32.3977 35.4744 32.1307
                    35.8381L28.8494 35.25L29.6761
                    26.0455H40.3466V29.0625H32.7188L32.267
                    33.4347H32.3693C32.6761 33.0028 33.1392 32.6449 33.7585
                    32.3608C34.3778 32.0767 35.071 31.9347 35.8381
                    31.9347C36.8892 31.9347 37.8267 32.1818 38.6506
                    32.6761C39.4744 33.1705 40.125 33.8494 40.6023
                    34.7131C41.0795 35.571 41.3153 36.5597 41.3097
                    37.679C41.3153 38.8551 41.0426 39.9006 40.4915
                    40.8153C39.946 41.7244 39.1818 42.4403 38.1989
                    42.9631C37.2216 43.4801 36.0852 43.7386 34.7898 43.7386Z"
                    fill="#CF3D60" />
                </svg>
              </div>
              <div class="gwrapper-content">
                <p class="g-stitle">Nominate person</p>
                <p class="g-content">You can nominate a person to access your
                  information in case you pass
                  away.</p>
              </div>
            </div>
          </div>
          <div class="g-button-wrapper">
            <div class="g-button">
              <a href="{{ url('register') }}" class="btn btn-g"
                role="button">
                Register now
              </a>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section>
      <span id="security" class="__anchor"></span>
      <div class="security-wrapper">
        <div class= "security-content-wrapper">
          <div class="security-img-wrapper">
            <div class="securitysvg">
              <svg class="s-svg" width="55" height="67"
                viewBox="0 0 55 67" fill="none"
                xmlns="http://www.w3.org/2000/svg">
                <g filter="url(#filter0_d_119_168)">
                  <path d="M27.26 0.0783247L2.30623 9.17735L2.13361 28.1087C1.99046
                    43.8071 12.4573 58.5452 26.6931 62.2502C40.9942 58.8054
                    51.728 44.2606 51.8711 28.5622L52.0437 9.63087L27.26
                    0.0783247ZM45.6539 28.5055C45.5406 40.9399 37.5088 52.3694
                    26.7521 55.7843C16.0595 52.1738 8.23713 40.6308 8.3508
                    28.1654L8.48629 13.3063L27.1979 6.88615L45.7894
                    13.6464L45.6539 28.5055ZM17.1312 26.691L12.677
                    31.0339L23.5811 42.1387L41.3362 24.7044L36.993
                    20.2814L23.6924 33.3416L17.1312 26.691Z" fill="#E7E8F2" />
                </g>
                <defs>
                  <filter id="filter0_d_119_168" x="0.132172" y="0.078125"
                    width="53.9116" height="66.1719"
                    filterUnits="userSpaceOnUse"
                    color-interpolation-filters="sRGB">
                    <feFlood flood-opacity="0" result="BackgroundImageFix" />
                    <feColorMatrix in="SourceAlpha" type="matrix"
                      values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                      result="hardAlpha" />
                    <feOffset dy="2" />
                    <feGaussianBlur stdDeviation="1" />
                    <feComposite in2="hardAlpha" operator="out" />
                    <feColorMatrix type="matrix"
                      values="0 0 0 0 0.0147825 0 0 0 0 0.0404013 0 0 0 0
                      0.367042 0 0 0 0.15 0" />
                    <feBlend mode="normal" in2="BackgroundImageFix"
                      result="effect1_dropShadow_119_168" />
                    <feBlend mode="normal" in="SourceGraphic"
                      in2="effect1_dropShadow_119_168" result="shape" />
                  </filter>
                </defs>
              </svg>
            </div>
            <img class="securityimg"
              src=" {{ asset('img/securityimg.png') }}">
          </div>
          <div class="security-body">
            <div class="oval-wrapper">
              <div class="security-oval">
                <p class="s-oval">Security</p>
              </div>
            </div>
            <div class="security-title">
              <p class="s-title">How <b>secure</b> is my data?</p>
            </div>
            <div class="security-subheading">
              {{-- <p>How will your data be stored and protected?</p> --}}
              <hr class="security-hr">
            </div>
            <div class="security-content">
              {{-- <p>Here at MyAll, we're all about keeping your data super secure. We --}}
              {{--   use robust encryption, --}}
              {{--   enforce strong password policies, and regularly amp up security --}}
              {{--   measures to protect your --}}
              {{--   info. Rest easy knowing your experience on our platform is safe --}}
              {{--   and reliable—not even the --}}
              {{--   people working at MyAll can see your information.</p> --}}
              {{-- <p>Curious for more details? Pop us an email at secure@myall.co.za --}}
              {{--   with your questions.</p> --}}

              <p>Here at MyAll, we're all about keeping your data super secure.
                Rest
                easy knowing your experience on our platform is safe and
                reliable—not
                even the people working at MyAll can see your information.</p>
              <br>
              <p>We provide comprehensive security measures, including a web
                application
                firewall, traffic analysis, and DDoS protection. This means we
                use
                advanced
                tools to safeguard our website, monitor traffic, and protect
                against cyber
                attacks, ensuring your data is secure with us.</p>
              <br>
              <strong>How will your data be stored and protected?</strong>
              <br>
              <p>Your data is stored securely using the latest encryption
                technologies. We continuously monitor our systems and regularly
                update our security protocols to stay ahead of potential
                threats.
              </p>
              <br>
              <p>Curious for more details? Pop us an email at hello@myall.co.za
                with your questions.</p>
            </div>
          </div>
        </div>
      </div>
      <div>
        <hr class="end-hr">
      </div>
    </section>
    <section>
      <span id="pricing" class="__anchor"></span>
      <div class="pricing-wrapper">
        <div class="p-oval-wrapper">
          <div class="p-oval">
            <p class="pricing">Pricing</p>
          </div>
        </div>
        <div class="p-title-container">
          <p class="p-title">Choose your option</p>
        </div>
        <div class="options-container">
          <div class="option">
            <div class="psvg-container">
              <svg width="112" height="112" viewBox="0 0 112 112"
                fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M55.9814 105.963C28.3775 105.963 6.00001 83.5852 6.00001
                  55.9813C6.00001 28.3774 28.3775 6.00001 55.9814 6C83.5854 6
                  105.963 28.3774 105.963 55.9813C105.963 83.5852 83.5854
                  105.963 55.9814 105.963Z" fill="#CF3D60" stroke="#FAECEF"
                  stroke-width="12" />
                <path d="M55.5005 46.4156C62.297 46.4156 68.3582 50.2352 71.317
                  56.2785C68.3582 62.3217 62.297 66.1414 55.5005 66.1414C48.7041
                  66.1414 42.6429 62.3217 39.684 56.2785C42.6429 50.2352 48.7041
                  46.4156 55.5005 46.4156ZM55.5005 42.8291C46.5342 42.8291
                  38.877 48.4061 35.7747 56.2785C38.877 64.1509 46.5342 69.7279
                  55.5005 69.7279C64.4668 69.7279 72.124 64.1509 75.2263
                  56.2785C72.124 48.4061 64.4668 42.8291 55.5005
                  42.8291ZM55.5005 51.7954C57.9752 51.7954 59.9837 53.8038
                  59.9837 56.2785C59.9837 58.7532 57.9752 60.7616 55.5005
                  60.7616C53.0258 60.7616 51.0174 58.7532 51.0174
                  56.2785C51.0174 53.8038 53.0258 51.7954 55.5005
                  51.7954ZM55.5005 48.2089C51.0532 48.2089 47.4309 51.8312
                  47.4309 56.2785C47.4309 60.7257 51.0532 64.3481 55.5005
                  64.3481C59.9478 64.3481 63.5702 60.7257 63.5702
                  56.2785C63.5702 51.8312 59.9478 48.2089 55.5005 48.2089Z"
                  fill="white" />
              </svg>
            </div>
            <div class="o-wrapper">
              <p class="v-title">View Only Access</p>
            </div>
            <div class="price-free">
              <p class="free">Free</p>
            </div>
            <ul class="content">
              <li class="c c-red">Register and create a free profile just to
                view info someone has shared with you.</li>
              <li class="c">Only one person registered on a full access
                package can share info with one free profile at a time</li>
              <li class="c">Accessible on any device with internet</li>
              <li class="c">Securely stored info</li>
              <li class="c">Option to upgrade</li>
            </ul>
            <div class="f-signupbtn">
              <button href="" class="btn btn-default4" role="button"
                onclick="setActivePlan('free'); event.preventDefault(); window.location.href = '{{ url('register') }}';">
                Free Access
              </button>
            </div>
          </div>
          <div class="option">
            <div class="psvg-container">
              <svg width="112" height="112" viewBox="0 0 112 112"
                fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M55.9814 105.963C28.3775 105.963 6.00001 83.5852 6.00001
                  55.9813C6.00001 28.3774 28.3775 6.00001 55.9814 6C83.5854 6
                  105.963 28.3774 105.963 55.9813C105.963 83.5852 83.5854
                  105.963 55.9814 105.963Z" fill="#040A5E" stroke="#E6E7EF"
                  stroke-width="12" />
                <path d="M75.2263 56.2604L70.8508 51.2573L71.4605 44.6402L64.9868
                  43.1697L61.5976 37.4492L55.5005 40.0674L49.4034
                  37.4492L46.0142 43.1697L39.5405 44.6222L40.1502
                  51.2572L35.7747 56.2604L40.1502 61.2636L39.5405
                  67.8986L46.0142 69.3691L49.4034 75.1075L55.5005
                  72.4714L61.5976 75.0896L64.9868 69.3691L71.4605
                  67.8986L70.8508 61.2815L75.2263 56.2604ZM68.143
                  58.9144L67.1387 60.08L67.2822 61.6043L67.605 65.1011L62.6915
                  66.213L61.9024 67.54L60.1271 70.5526L56.9351 69.1718L55.5005
                  68.5621L54.0838 69.1718L50.8918 70.5526L49.1165
                  67.5579L48.3275 66.2309L43.414 65.1191L43.7367 61.6043L43.8802
                  60.08L42.876 58.9144L40.5627 56.2784L42.876 53.6243L43.8802
                  52.4587L43.7188 50.9165L43.396 47.4376L48.3096 46.3258L49.0986
                  44.9988L50.8739 41.9861L54.0659 43.3669L55.5005
                  43.9767L56.9172 43.3669L60.1092 41.9861L61.8845
                  44.9988L62.6735 46.3258L67.5871 47.4376L67.2643
                  50.9345L67.1208 52.4587L68.125 53.6243L70.4383 56.2604L68.143
                  58.9144Z" fill="white" />
                <path d="M52.0754 59.4165L47.915 55.2383L45.261 57.9102L52.0754
                  64.7425L65.2379 51.5442L62.5839 48.8722L52.0754 59.4165Z"
                  fill="white" />
              </svg>
            </div>
            <div class="o-wrapper">
              <p class="v-title full-access">Full Access</p>
            </div>
            <livewire:landing.partials.pricing>
              {{-- <div class="o-wrapper"> --}}
              {{--   <p class="paid-title">Full Access</p> --}}
              {{-- </div> --}}
              {{-- <div class="p-subheading"> --}}
              {{--   <div class="m-option"> --}}
              {{--     <p class="monthly">Monthly</p> --}}
              {{--   </div> --}}
              {{--   <div class="a-option"> --}}
              {{--     <p class="annual">Annual (Save 16%)</p> --}}
              {{--   </div> --}}
              {{-- </div> --}}
              {{-- <div class="p-price"> --}}
              {{--   <p class="price">R29/Month</p> --}}
              {{-- </div> --}}
              <ul class="content">
                <li class="c">Accessible on any device with internet</li>
                <li class="c">Your info is securely stored</li>
                <li class="c">Share and receive info with your contacts
                </li>
                <li class="c">Upload personal and financial info</li>
                <li class="c">View info about yourself and others</li>
                <li class="c">Visual overview of all shared info</li>
                <li class="c">Vault for extra essential docs</li>
                <li class="c">Track debts, expenses, loans, policies, etc
                </li>
                <li class="c">Create and download your Will</li>
                <li class="c">Choose a trusted contact to access your info
                  in
                  emergencies</li>
                <li class="c">Data is kept for 3 months for your trusted
                  contact to access your info in
                  case of your passing</li>
              </ul>
              <div class="p-signupbtn">
                <a href="{{ url('register') }}" class="btn btn-default5"
                  role="button">Register</a>
              </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <section class="section-contact">
    <span id="contact" class="__anchor"></span>
    <div class="contact-wrapper">
      <div class="b-oval-wrapper">
        <div class="b-oval">
          <p class="b-oval-content">Contact</p>
        </div>
      </div>
      <div class="c-title-container">
        <p class="c-title">Get in touch</p>
      </div>
      <div class="contact-grid">
        <livewire:landing.partials.contact />
        <div class="cimg-container">
          <div class="c-img-wrapper">
            <div class="c-img">
              <img class="c-image" src=" {{ asset('img/contactimg.png') }}">
            </div>
            <div class="c-links">
              <p class="c-subheading">For more info</p>
              <svg class="email-svg" width="17" height="14"
                viewBox="0 0 17 14" fill="none"
                xmlns="http://www.w3.org/2000/svg">
                <path d="M16.8334 1.96257C16.8334 1.0459 16.0834 0.295898 15.1667
                  0.295898H1.83335C0.916687 0.295898 0.166687 1.0459 0.166687
                  1.96257V11.9626C0.166687 12.8792 0.916687 13.6292 1.83335
                  13.6292H15.1667C16.0834 13.6292 16.8334 12.8792 16.8334
                  11.9626V1.96257ZM15.1667 1.96257L8.50002 6.12923L1.83335
                  1.96257H15.1667ZM15.1667 11.9626H1.83335V3.62923L8.50002
                  7.7959L15.1667 3.62923V11.9626Z" fill="#040A5E" />
              </svg>
              <p class="c-email">hello@myall.co.za</p>
              <hr class="hrc">
              <p class="c-subheading">For security related issues</p>
              <svg class="email-svg" width="17" height="14"
                viewBox="0 0 17 14" fill="none"
                xmlns="http://www.w3.org/2000/svg">
                <path d="M16.8334 1.96257C16.8334 1.0459 16.0834 0.295898 15.1667
                  0.295898H1.83335C0.916687 0.295898 0.166687 1.0459 0.166687
                  1.96257V11.9626C0.166687 12.8792 0.916687 13.6292 1.83335
                  13.6292H15.1667C16.0834 13.6292 16.8334 12.8792 16.8334
                  11.9626V1.96257ZM15.1667 1.96257L8.50002 6.12923L1.83335
                  1.96257H15.1667ZM15.1667 11.9626H1.83335V3.62923L8.50002
                  7.7959L15.1667 3.62923V11.9626Z" fill="#040A5E" />
              </svg>
              <p class="c-email">reporting@myall.co.za</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section>
    <footer class="footer-wrapper">
      <div class="socials-wrapper-f">
        <div class="social-svg">
          <a href="https://www.facebook.com/profile.php?id=61565350083731"
            target="_blank">
            <svg width="32" height="32" viewBox="0 0 32 32"
              fill="none" xmlns="http://www.w3.org/2000/svg">
              <rect width="32" height="32" rx="16"
                fill="#F5ADBF" fill-opacity="0.1" />
              <path d="M19 14.2H16.9905V13C16.9905 12.3808 17.0384 11.9908 17.8815
              11.9908H18.9464V10.0828C18.4282 10.0264 17.9072 9.99884 17.3856
              10C15.839 10 14.7102 10.9942 14.7102 12.8194V14.2H13V16.6L14.7102
              16.5994V22H16.9905V16.5982L18.7383 16.5976L19 14.2Z"
                fill="#FFADBC" />
            </svg>
          </a>
          <a href="https://www.instagram.com/myall_sa/?utm_source=desktop_footer"
            target="_blank">
            <svg width="32" height="32" viewBox="0 0 32 32"
              fill="none" xmlns="http://www.w3.org/2000/svg">
              <rect width="32" height="32" rx="16"
                fill="#F5ADBF" fill-opacity="0.1" />
              <path d="M13.3333 10C11.4953 10 10 11.4953 10 13.3333V18.6667C10 20.5047
              11.4953 22 13.3333 22H18.6667C20.5047 22 22 20.5047 22
              18.6667V13.3333C22 11.4953 20.5047 10 18.6667 10H13.3333ZM13.3333
              11.3333H18.6667C19.7693 11.3333 20.6667 12.2307 20.6667
              13.3333V18.6667C20.6667 19.7693 19.7693 20.6667 18.6667
              20.6667H13.3333C12.2307 20.6667 11.3333 19.7693 11.3333
              18.6667V13.3333C11.3333 12.2307 12.2307 11.3333 13.3333
              11.3333ZM19.3333 12C19.1565 12 18.987 12.0702 18.8619
              12.1953C18.7369 12.3203 18.6667 12.4899 18.6667 12.6667C18.6667
              12.8435 18.7369 13.013 18.8619 13.1381C18.987 13.2631 19.1565
              13.3333 19.3333 13.3333C19.5101 13.3333 19.6797 13.2631 19.8047
              13.1381C19.9298 13.013 20 12.8435 20 12.6667C20 12.4899 19.9298
              12.3203 19.8047 12.1953C19.6797 12.0702 19.5101 12 19.3333 12ZM16
              12.6667C14.162 12.6667 12.6667 14.162 12.6667 16C12.6667 17.838
              14.162 19.3333 16 19.3333C17.838 19.3333 19.3333 17.838 19.3333
              16C19.3333 14.162 17.838 12.6667 16 12.6667ZM16 14C17.1027 14 18
              14.8973 18 16C18 17.1027 17.1027 18 16 18C14.8973 18 14 17.1027 14
              16C14 14.8973 14.8973 14 16 14Z" fill="#FFADBC" />
            </svg>
          </a>
          <a href="https://www.linkedin.com/company/myall-sa/posts/"
            target="_blank">
            <svg width="32" height="32" viewBox="0 0 32 32"
              fill="none" xmlns="http://www.w3.org/2000/svg">
              <rect width="32" height="32" rx="16"
                fill="#F5ADBF" fill-opacity="0.1" />
              <path d="M12.6861 22H10.1982V13.9882H12.6861V22ZM11.4408 12.8953C10.6453
              12.8953 10 12.2364 10 11.4408C10 11.0587 10.1518 10.6922 10.422
              10.422C10.6922 10.1518 11.0587 10 11.4408 10C11.8229 10 12.1894
              10.1518 12.4596 10.422C12.7298 10.6922 12.8816 11.0587 12.8816
              11.4408C12.8816 12.2364 12.2361 12.8953 11.4408 12.8953ZM21.9973
              22H19.5148V18.0999C19.5148 17.1704 19.4961 15.9784 18.2213
              15.9784C16.9279 15.9784 16.7296 16.9883 16.7296
              18.0329V22H14.2445V13.9882H16.6305V15.0811H16.6654C16.9975 14.4516
              17.8088 13.7873 19.0193 13.7873C21.5371 13.7873 22 15.4454 22
              17.599V22H21.9973Z" fill="#FFADBC" />
            </svg>
          </a>
        </div>
      </div>
      <div class="f-link-wrapper">
        <div class="f-l-title">
          <a href="{{ url('docs/MyAll Privacy Policy.pdf') }}" class="link"
            target="_blank">Privacy Policy</a>

          <svg width="1" height="18" viewBox="0 0 1 18"
            fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M0.5 0.962891V17.9629" stroke="#929BCA"
              stroke-miterlimit="3.86874" stroke-linejoin="round" />
          </svg>

          <a href="{{ url('docs/MyAll Terms and Conditions.pdf') }}"
            class="link" target="_blank">Terms & Conditions</a>

          <svg width="1" height="18" viewBox="0 0 1 18"
            fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M0.5 0.962891V17.9629" stroke="#929BCA"
              stroke-miterlimit="3.86874" stroke-linejoin="round" />
          </svg>

          <a href="{{ url('docs/MyAll PAIA Manual.pdf') }}" class="link"
            target="_blank">PAIA Manual</a>

          <svg width="1" height="18" viewBox="0 0 1 18"
            fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M0.5 0.962891V17.9629" stroke="#929BCA"
              stroke-miterlimit="3.86874" stroke-linejoin="round" />
          </svg>

          <a href="{{ url('docs/MyAll Legal Content Disclaimer.pdf') }}"
            class="link" target="_blank">Legal Content Disclaimer</a>

          <svg width="1" height="18" viewBox="0 0 1 18"
            fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M0.5 0.962891V17.9629" stroke="#929BCA"
              stroke-miterlimit="3.86874" stroke-linejoin="round" />
          </svg>

          <a href="{{ url('docs/MyAll Data protection policy.pdf') }}"
            class="link" target="_blank">Data Protection Policy</a>

          <svg width="1" height="18" viewBox="0 0 1 18"
            fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M0.5 0.962891V17.9629" stroke="#929BCA"
              stroke-miterlimit="3.86874" stroke-linejoin="round" />
          </svg>

          <a href="{{ url('docs/MyAll FAQs & Password Policy.pdf') }}"
            class="link" target="_blank">FAQs &amp; Password Policy</a>
        </div>
      </div>
      <div style="margin-top: 1rem">
        <div
          style="text-align: center; font-size: 0.6rem; color:
        var(--theme-light-pink)">
          &copy; Copyright
          2024 MYALL SA (Pty)
          Ltd
          (2024/494838/07). All Rights
          Reserved</div>
      </div>
    </footer>
  </section>
