<div class="contact-body">
  <div class="c-subheading">
    <p class="message">Message us</p>
  </div>
  <div class="c-hr">
    <hr class="hrc">
  </div>
  <div class="form-wrapper">
    <form wire:submit.prevent="submitContactForm">
      <div class="form-grid">
        <label for="name">Name</label><br class="l-hide-d">
        <label class="l-hide-m" for="surname">Surname</label>
        <input type="text" id="name" name="name" value=""
          wire:model.defer="name">
        <br class="l-hide-d">
        <label class="l-hide-d" for="surname">Surname</label>
        <br class="l-hide-d">
        <input type="text" id="surname" name="surname" value=""
          wire:model.defer="surname">
        <br class="l-hide-d">
        <label for="contact-number">Contact Number</label>
        <br class="l-hide-d">
        <label class="l-hide-m" for="email">Email</label>
        <input type="text" id="contact-number" name="contact_number"
          value="" wire:model.defer="contact_number">
        <br class="l-hide-d">
        <label class="l-hide-d" for="email">Email</label>
        <br class="l-hide-d">
        <input type="email" id="email" name="email" value=""
          wire:model.defer="email">
      </div>
      <label for="message">Message</label><br>
      <textarea id="message" name="message" rows="1" cols="50"
        wire:model.defer="message"></textarea>
      <br>
      <div class="disc-grid">
        <input type="checkbox" id="disclaimer" name="terms_check"
          value="disclaimer" wire:model.defer="terms_check">
        <label class="disclaimer" for="disclaimer">
          I have read the <a
            href="{{ url('docs/MyAll Terms and Conditions.pdf') }}"
            target="_blank" class="disc-bold" style="font-weight: 600">Terms &
            Conditions</a> and <a
            href="{{ url('docs/MyAll Privacy Policy.pdf') }}" target="_blank"
            class="disc-bold" style="font-weight: 600">Privacy Policy</a> , and
          give
          consent to MyAll to
          share and manage my data in accordance with the POPI Act.
        </label>
      </div>

      <div class="c-hr">
        <hr class="hrc">
      </div>
      <div class="">
        @if ($errors->any())
          <div class="alert alert-danger">
            <span>{{ $errors->first() }}</span>
          </div>
        @endif
        @if (session()->has('message'))
          <div class="alert alert-success">
            <span>{{ session('message') }}</span>
          </div>
        @endif
      </div>
      <div class="submit-btn">
        <button type="submit" class="btn btn-default6" role="button">
          <svg width="20" height="20" viewBox="0 0 20 20" fill="none"
            xmlns="http://www.w3.org/2000/svg" style="margin-right: 0.2rem">
            <path d="M2.925 5.025L9.18333 7.70833L2.91667 6.875L2.925
            5.025ZM9.175 12.2917L2.91667 14.975V13.125L9.175 12.2917ZM1.25833
            2.5L1.25 8.33333L13.75 10L1.25 11.6667L1.25833 17.5L18.75 10L1.25833
            2.5Z" fill="white" />
          </svg>
          Send
        </button>
      </div>
    </form>
  </div>
</div>
{{-- <form wire:submit.prevent="submitContactForm"> --}}
{{-- <div class="row"> --}}
{{--   <div class="col-md-12"> --}}
{{--     <h1>LET'S GET IN TOUCH</h1> --}}
{{--     <p class="text-pink text-red"><b>Fill in the form below and we’ll contact --}}
{{--         you.</b></p> --}}
{{--     @if (request()->segment(1) == 'pricing') --}}
{{--       <p> --}}
{{--         You can cancel your paid subscription at any time using the form --}}
{{--         below. Please note that refunds are not available. If you are a paid --}}
{{--         user, we will securely store your entered data for 3 months after --}}
{{--         cancellation. This ensures a seamless renewal process, allowing you to --}}
{{--         effortlessly update any information that hasn't been modified in the --}}
{{--         past 3 months. The same applies if you decide not to renew the yearly --}}
{{--         subscription. --}}
{{--       </p> --}}
{{--     @endif --}}
{{--   </div> --}}
{{--   <div class="col-md-12"> --}}
{{--     @if ($errors->any()) --}}
{{--       <div class="alert alert-danger"> --}}
{{--         <span>{{ $errors->first() }}</span> --}}
{{--       </div> --}}
{{--     @endif --}}
{{--     @if (session()->has('message')) --}}
{{--       <div class="alert alert-success"> --}}
{{--         <span>{{ session('message') }}</span> --}}
{{--       </div> --}}
{{--     @endif --}}
{{--   </div> --}}
{{--   <div class="col-md-8"> --}}
{{--     <div class="row"> --}}
{{--       <div class="col-md-6"> --}}
{{--         <div class="mb-3"> --}}
{{--           <label class="form-label">Name</label> --}}
{{--           <input type="text" class="form-control" placeholder="Name*" --}}
{{--             name="name" wire:model.defer="name"> --}}
{{--         </div> --}}
{{--       </div> --}}
{{--       <div class="col-md-6"> --}}
{{--         <div class="mb-3"> --}}
{{--           <label class="form-label">Surname</label> --}}
{{--           <input type="text" class="form-control" placeholder="Surname*" --}}
{{--             name="surname" wire:model.defer="surname"> --}}
{{--         </div> --}}
{{--       </div> --}}
{{--       <div class="col-md-6"> --}}
{{--         <div class="mb-3"> --}}
{{--           <label class="form-label">Contact Number</label> --}}
{{--           <input type="text" class="form-control" --}}
{{--             placeholder="Contact Number*" name="contact_number" --}}
{{--             wire:model.defer="contact_number"> --}}
{{--         </div> --}}
{{--       </div> --}}
{{--       <div class="col-md-6"> --}}
{{--         <div class="mb-3"> --}}
{{--           <label class="form-label">Email</label> --}}
{{--           <input type="text" class="form-control" placeholder="Email*" --}}
{{--             name="email" wire:model.defer="email"> --}}
{{--         </div> --}}
{{--       </div> --}}
{{--       <div class="col-md-12"> --}}
{{--         <div class="mb-3"> --}}
{{--           <label class="form-label">How can we help you?</label> --}}
{{--           <textarea class="form-control" --}}
{{--             placeholder="Have any questions or need some info?" name="message" --}}
{{--             wire:model.defer="message"></textarea> --}}
{{--         </div> --}}
{{--       </div> --}}
{{--     </div> --}}
{{--   </div> --}}
{{--   <div class="col-md-4"> --}}
{{--     <div class="row"> --}}
{{--       <div class="col-md-12 mt-5"> --}}
{{--         <h3 class="page-title">Submit your request</h3> --}}
{{--         <p class="text-pink text-red"><b>Have any questions or need more --}}
{{--             information? Let’s get in touch.</b></p> --}}
{{--       </div> --}}
{{--       <div class="col-md-12"> --}}
{{--         <div class="row mb-3"> --}}
{{--           <div class="col-md-12"> --}}
{{--             <span class="pe-3"><i --}}
{{--                 class="fa fa-envelope blue-color"></i></span> --}}
{{--             <b class="blue-color">hello@myall.co.za</b> --}}
{{--           </div> --}}
{{--         </div> --}}
{{--       </div> --}}
{{--       <div class="col-md-12 mt-2"> --}}
{{--         <p class="text-pink text-red"><b>For any security related matters, --}}
{{--             <br />contact us on</b></p> --}}
{{--       </div> --}}
{{--       <div class="col-md-12"> --}}
{{--         <div class="row mb-3"> --}}
{{--           <div class="col-md-12"> --}}
{{--             <span class="pe-3"><i --}}
{{--                 class="fa fa-envelope blue-color"></i></span> --}}
{{--             <b class="blue-color">reporting@myall.co.za</b> --}}
{{--           </div> --}}
{{--         </div> --}}
{{--       </div> --}}
{{--       <div class="col-md-12"> --}}
{{--         <div class="form-check"> --}}
{{--           <input class="form-check-input" type="checkbox" name="terms_check" --}}
{{--             id="terms_check" wire:model.defer="terms_check"> --}}
{{--           <label class="form-check-labe" for="terms_check"> --}}
{{--             <p style="font-size: 12px;">I have read the <a --}}
{{--                 href="{{ url('docs/MyAll Terms and Conditions.pdf) }}" --}}
{{--                 target="_blank" class="link-font-bold">Terms & Conditions</a> --}}
{{--               and <a --}}
{{--                 href="{{ url('docs/MyAll Privacy Policy.pdf') }}" --}}
{{--                 target="blank" class="link-font-bold">Privacy Policy</a>, and --}}
{{--               give consent to WrapUp to share and manage my data in accordance --}}
{{--               with the POPI Act.</p> --}}
{{--           </label> --}}
{{--         </div> --}}
{{--       </div> --}}
{{--       <div class="col-md-12 ms-3"> --}}
{{--         <input type="submit" class="btn btn-red rounded-pill" --}}
{{--           value="SUBMIT REQUEST"> --}}
{{--       </div> --}}
{{--     </div> --}}
{{--   </div> --}}
{{-- </div> --}}
{{-- </form> --}}
