<header class="__navigation @if (Request::path() !== '/') __bg-full @endif"
  data-header="@if (Request::path() !== '/') bg-full @endif">
  <div class="__wrapper __desktop">
    <div>
      <a href="{{ url('/') }}">
        <img class="__logo" src="{{ asset('img/logo.svg') }}"
          srcset="{{ asset('img/logo.svg') }}" alt="MyAll" />
      </a>
    </div>
    <nav>
      <ul class="">
        <li class="__nav-item">
          <a class="__nav-link" href="{{ url('/') }}">
            Home
          </a>
        </li>
        <li class="__nav-item" id="offer-link">
          <a class="__nav-link" href="">
            Our offer
            <svg width="12" height="12" viewBox="0 0 12 12"
              fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M3.705 4.14844L6 6.43844L8.295 4.14844L9 4.85344L6 7.85344L3
                4.85344L3.705 4.14844Z" fill="#E26C88" />
            </svg>
          </a>
          <div class="__menu" id="offer-menu">
            <ul>
              <div class="__link">
                <a href="#what-is-myall">
                  <li>What is MyAll</li>
                </a>
              </div>
              <div class="__link">
                <a href="#how-it-works">
                  <li>How it works</li>
                </a>
              </div>
              <div class="__link">
                <a href="#security">
                  <li>Security</li>
                </a>
              </div>
            </ul>
          </div>
        </li>
        <li class="__nav-item">
          <a class="__nav-link" href="#pricing">
            Pricing
          </a>
        </li>
        <li class="__nav-item">
          <a class="__nav-link" href="#contact">
            Contact
          </a>
        </li>
        @if (Auth::guest())
          <li class="__nav-item">
            <a class="__nav-link __bordered" href="#"
              wire:click.prevent="$emit('show-login-modal')">
              Sign in
            </a>
          </li>
          <li class="__nav-item">
            <a class="__nav-link __bordered __solid"
              href="{{ url('register') }}">
              Register
            </a>
          </li>
        @else
          <li class="__nav-item">
            <a class="__nav-link __bordered" href="{{ url('profile') }}">
              Dashboard
            </a>
          </li>
        @endif

      </ul>
    </nav>
    <div class="__sub-nav">
      {{-- TODO: Add links to the social icons --}}
      <div class="__social">
        <a href="https://www.facebook.com/profile.php?id=61565350083731"
          target="_blank" class="__icon facebook">
          <i class="fa fa-facebook"></i>
        </a>
        <a href="https://www.instagram.com/myall_sa/?utm_source=desktop_landing"
          target="_blank" class="__icon instagram">
          <i class="fa fa-instagram"></i>
        </a>
        <a href="https://www.linkedin.com/company/myall-sa/posts/"
          target="_blank" class="__icon linkedin">
          <i class="fa fa-linkedin"></i>
        </a>
      </div>

      <div class="__separator"><span></span></div>

      <div id="myallLandingLanguage">
        <div class="__content">
          <svg width="16" height="17" viewBox="0 0 16 17" fill="none"
            xmlns="http://www.w3.org/2000/svg">
            <path d="M7.99992 1.83398C4.31992 1.83398 1.33325 4.82065 1.33325
            8.50065C1.33325 12.1807 4.31992 15.1673 7.99992 15.1673C11.6799
            15.1673 14.6666 12.1807 14.6666 8.50065C14.6666 4.82065 11.6799
            1.83398 7.99992 1.83398ZM2.66659 8.50065C2.66659 8.09398 2.71992
            7.69398 2.80659 7.31398L5.99325 10.5007V11.1673C5.99325 11.9007
            6.59325 12.5007 7.32659 12.5007V13.7873C4.70659 13.454 2.66659
            11.214 2.66659 8.50065ZM11.9266 12.1007C11.7533 11.5607 11.2599
            11.1673 10.6599 11.1673H9.99325V9.16732C9.99325 8.80065 9.69325
            8.50065 9.32659 8.50065H5.32659V7.16732H6.65992C7.02659 7.16732
            7.32659 6.86732 7.32659 6.50065V5.16732H8.65992C9.39325 5.16732
            9.99325 4.56732 9.99325 3.83398V3.56065C11.9466 4.34732 13.3333
            6.26732 13.3333 8.50065C13.3333 9.88732 12.7933 11.154 11.9266
            12.1007Z" fill="#ffadbc" />
          </svg>
          Language
        </div>
        <div class="__lang-menu">
          <div id="google_translate_element"></div>
        </div>
      </div>
    </div>
  </div>
  <div class="__wrapper __mobile">
    <div class="__outer">
      <a href="{{ url('/') }}">
        <img class="__logo" src="{{ asset('img/logo.svg') }}"
          srcset="{{ asset('img/logo.svg') }}" alt="MyAll" />
      </a>
      <button id="menu-toggle">
        <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
          xmlns="http://www.w3.org/2000/svg">
          <path d="M3 18H21V16H3V18ZM3 13H21V11H3V13ZM3 6V8H21V6H3Z"
            fill="#FFADBC" />
        </svg>
      </button>
    </div>
    <div class="__panel">
      <div class="__header">
        <button>
          <svg width="20" height="21" viewBox="0 0 20 21" fill="none"
            xmlns="http://www.w3.org/2000/svg">
            <path d="M15.8334 5.84175L14.6584 4.66675L10.0001 9.32508L5.34175
            4.66675L4.16675 5.84175L8.82508 10.5001L4.16675 15.1584L5.34175
            16.3334L10.0001 11.6751L14.6584 16.3334L15.8334 15.1584L11.1751
            10.5001L15.8334 5.84175Z" fill="#FFADBC" />
          </svg>
        </button>
      </div>
      <nav>
        <ul>
          <li>
            <a class="__nav-link" href="{{ url('/') }}">
              Home
            </a>
          </li>
          <li>
            <a class="__nav-link" href="#what-is-myall">
              What is MyAll
            </a>
          </li>
          <li>
            <a class="__nav-link" href="#how-it-works">
              How it works
            </a>
          </li>
          <li>
            <a class="__nav-link" href="#security">
              Security
            </a>
          </li>
          <li>
            <a class="__nav-link" href="#pricing">
              Pricing
            </a>
          </li>
          <li>
            <a class="__nav-link" href="#contact">
              Contact
            </a>
          </li>
          @if (Auth::guest())
            <li class="__nav-item">
              <a class="__nav-link __bordered" href="#"
                wire:click.prevent="$emit('show-login-modal')">
                Sign in
              </a>
            </li>
            <li class="__nav-item">
              <a class="__nav-link __bordered __solid"
                href="{{ url('register') }}">
                Register
              </a>
            </li>
          @else
            <li class="__nav-item">
              <a class="__nav-link __bordered" href="{{ url('profile') }}">
                Dashboard
              </a>
            </li>
          @endif
        </ul>
        <div class="__social">
          <a href="https://www.facebook.com/profile.php?id=61565350083731"
            target="_blank" class="__icon facebook">
            <i class="fa fa-facebook"></i>
          </a>
          <a href="https://www.instagram.com/myall_sa/?utm_source=mobile_landing"
            target="_blank" class="__icon instagram">
            <i class="fa fa-instagram"></i>
          </a>
          <a href="https://www.linkedin.com/company/myall-sa/posts/"
            target="_blank" class="__icon linkedin">
            <i class="fa fa-linkedin"></i>
          </a>
        </div>
      </nav>
    </div>
  </div>
  {{-- NOTE: Previous header --}}
  {{-- <nav class="navbar navbar-expand-lg center-nav navbar-bg-dark"> --}}
  {{--   <div class="flex-lg-row align-items-center container flex-nowrap"> --}}
  {{--     <div class="navbar-brand w-100"> --}}
  {{--       <a href="{{ url('/') }}"> --}}
  {{--         <img src="{{ asset('img/logo.svg') }}" alt="MyAll" /> --}}
  {{--       </a> --}}
  {{--     </div> --}}
  {{--     <div class="navbar-collapse offcanvas offcanvas-nav offcanvas-start"> --}}
  {{--       <div class="offcanvas-header d-lg-none"> --}}
  {{--         <button type="button" class="btn-close btn-close-pink" --}}
  {{--           data-bs-dismiss="offcanvas" aria-label="Close"></button> --}}
  {{--       </div> --}}
  {{--       <div class="offcanvas-body ms-lg-auto h-100 nav-grid"> --}}
  {{--         <ul class="navbar-nav mt-3"> --}}
  {{--           <li class="nav-item"><a class="nav-linkp" --}}
  {{--               href="{{ url('/') }}">Home</a></li> --}}
  {{--           <li class="nav-item"><a class="nav-linkp" href="">Our --}}
  {{--               offer</a></li> --}}
  {{--           <li class="nav-item"><a class="nav-linkp" --}}
  {{--               href="">Testimonials</a></li> --}}
  {{--           @if (Auth::guest()) --}}
  {{--             <li class="nav-item d-lg-none"><a class="nav-linkp" --}}
  {{--                 href="{{ url('register') }}">Register</a> --}}
  {{--             </li> --}}
  {{--           @else --}}
  {{--             <li class="nav-item"><a class="nav-link" --}}
  {{--                 href="{{ url('profile') }}">Profile Dashboard</a> --}}
  {{--             </li> --}}
  {{--           @endif --}}
  {{--           <li class="nav-item"><a class="nav-linkp" --}}
  {{--               href="#pricing-anchor">Pricing</a></li> --}}
  {{--           <li class="nav-item"><a class="nav-linkp" href="">Our --}}
  {{--               story</a></li> --}}
  {{--           <li class="nav-item"><a class="nav-linkp" --}}
  {{--               href="#contact-anchor">Contact</a> --}}
  {{--           </li> --}}
  {{-- --}}
  {{--         </ul> --}}
  {{--         <div class="d-lg-none order-4 mt-auto pb-6 pt-6 text-center"> --}}
  {{--           <div class="d-grid mb-3 gap-2"> --}}
  {{--             @if (Auth::guest()) --}}
  {{--               <a href="{{ url('login') }}" --}}
  {{--                 class="btn btn-sm btn-red rounded-pill">Sign in</a> --}}
  {{--               <a href="{{ url('login') }}" --}}
  {{--                 class="btn btn-sm btn-red rounded-pill">Register</a> --}}
  {{--             @endif --}}
  {{--           </div> --}}
  {{--           <nav class="nav social d-flex justify-content-around mt-4"> --}}
  {{--             <a href="https://www.facebook.com/wrapuponline" target="_blank" --}}
  {{--               class="drawer-social-icons facebook"><i --}}
  {{--                 class="fa fa-facebook"></i></a> --}}
  {{--             <a href="https://www.instagram.com/wrapuponline/" target="_blank" --}}
  {{--               class="drawer-social-icons instagram"><i --}}
  {{--                 class="fa fa-instagram"></i></a> --}}
  {{--             <a href="https://www.linkedin.com/company/wrapupza/" --}}
  {{--               target="_blank" class="drawer-social-icons linkedin"><i --}}
  {{--                 class="fa fa-linkedin"></i></a> --}}
  {{--           </nav> --}}
  {{--         </div> --}}
  {{--       </div> --}}
  {{--     </div> --}}
  {{--     <div class="navbar-other w-100 d-flex"> --}}
  {{--       @if (Auth::guest()) --}}
  {{--         <div class="d-none d-md-block ms-5 mt-3"> --}}
  {{--           <a href="#" class="btn btn-sm btn-red rounded-pill" --}}
  {{--             wire:click.prevent="$emit('show-login-modal')">Sign in</a> --}}
  {{--         </div> --}}
  {{--       @endif --}}
  {{--       <ul class="navbar-nav align-items-center ms-auto mt-3 flex-row"> --}}
  {{--         <li class="nav-item d-none d-lg-block nav-social-block"> --}}
  {{--           <a href="#" class="social-icons facebook" target="_blank"><i --}}
  {{--               class="fa fa-facebook"></i></a> --}}
  {{--           <a href="#" class="social-icons instagram" target="_blank"><i --}}
  {{--               class="fa fa-instagram"></i></a> --}}
  {{--           <a href="#" class="social-icons linkedin" target="_blank"><i --}}
  {{--               class="fa fa-linkedin"></i></a> --}}
  {{--         </li> --}}
  {{--         <li class="nav-item d-lg-none"> --}}
  {{--           <button class="hamburger offcanvas-nav-btn"><span></span></button> --}}
  {{--         </li> --}}
  {{--       </ul> --}}
  {{--     </div> --}}
  {{--   </div> --}}
  {{-- </nav> --}}

  {{-- </header> --}}

  {{-- MODALS --}}
  <div class="modal" tabindex="-1" id="login-modal" wire:ignore.self>
    <div class="modal-dialog modal-lg modal-dialog-centered">
      <div class="modal-content __modal-login">
        <div class="modal-body p-0">
          <div class="row">
            <div class="col-md-6 fill __login __img-container"
              style="overflow: hidden">
              <img src="{{ asset('img/e1d2f5423a.jpg') }}"
                style="height: 100%;">
            </div>
            <div class="col-md-6">
              <div class="close-lm">
                <button type="button" class="btn-close"
                  data-bs-dismiss="modal" aria-label="Close"></button>
              </div>
              <div class="d-flex lm-p-title">
                <h2 class="lm-page-title">Sign In</h2>
              </div>
              <div class="login-m-hr">
                <hr class="hrc">
              </div>
              <div class="row sign-in-content">
                <div class="col-md-12">
                  @if ($errors->any())
                    <div class="alert alert-danger">
                      <span>{{ $errors->first() }}</span>
                    </div>
                  @endif
                  <form class="login-m-form" wire:submit.prevent="loginUser">
                    <div class="mb-3">
                      <label class="form-label">Email address</label>
                      <input type="email" class="form-control"
                        name="email" wire:model.defer="email">
                    </div>
                    <div class="mb-3">
                      <label class="form-label">Password</label>
                      <div class="input-group mb-3">
                        <input
                          @if ($show_password) type="text" @else type="password" @endif
                          class="form-control password_with_toggle"
                          name="password" wire:model.defer="password">
                        <span class="input-group-text password_toggle"
                          wire:click="togglePassword"><i class="fa fa-eye"
                            id="togglePassword"></i></span>
                      </div>
                    </div>
                    <div class="btn-login-holder">
                      <div class="d-grid mb-3">
                        <input type="submit" class="btn btn-lg login-m-btn"
                          value="Submit">
                      </div>
                    </div>
                  </form>
                  <div class="d-flex login-text">
                    <span><a href="{{ url('pricing') }}">Register</a></span>
                    <span class="ms-auto"><a
                        href="{{ url('forgot-password') }}">Forgot
                        Password?</a></span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="modal" tabindex="-1" id="mailing-list-modal"
    wire:ignore.self>
    <div class="modal-dialog modal-lg modal-dialog-centered"
      style="max-width: 23rem;">
      <div class="modal-content">
        <div class="modal-body p-0">
          <form wire:submit.prevent="savemailingList">
            <div class="" style="padding: 1rem">
              <div class="" style="color: var(--theme-dark-pink)">
                <h3 class=""
                  style="color: var(--theme-dark-pink); font-size: 1.8rem; line-height: 1.2; font-weight: 400;">
                  {{-- Sign --}}
                  {{-- up for exclusive MyAll --}}
                  {{-- <strong>updates</strong> and --}}
                  {{-- <strong>developments</strong>. --}}
                  Sign up for free <strong>life advice</strong> and
                  <strong>tips</strong>!
                </h3>
                <p style="line-height: 1.2" class="">
                  {{-- Just add your details below and grant us permission to send --}}
                  {{-- you occasional communication via email and/or SMS. We promise --}}
                  {{-- we won’t spam you. --}}
                  Just add your details below and let us send you occasional
                  emails or SMS with valuable insights on family matters, legal
                  issues, emergencies, and more. Don’t worry, we won’t spam
                  you.
                </p>
              </div>
              <div class="">
                @if (session()->has('message'))
                  <div class="alert alert-success">
                    {{ session('message') }}
                  </div>
                @endif
                @if ($errors->any())
                  <div class="alert alert-danger">
                    <span>{{ $errors->first() }}</span>
                  </div>
                @endif
              </div>
              <div style="display: flex; flex-direction: column; gap: 1rem">
                <div class="">
                  <div class="">
                    <input type="text"
                      class="form-control-white form-control"
                      placeholder="Name and Surname*" name="name"
                      wire:model.defer="name">
                  </div>
                </div>
                <div class="">
                  <div class="">
                    <input type="text"
                      class="form-control-white form-control"
                      placeholder="Mobile Number*" name="contact_number"
                      wire:model.defer="contact_number">
                  </div>
                </div>
                <div class="">
                  <div class="">
                    <input type="text"
                      class="form-control-white form-control"
                      placeholder="Email*" name="email"
                      wire:model.defer="mailing_email">
                  </div>
                </div>
                <div class=""
                  style="display: flex; flex-direction: column; gap: 1rem">
                  <div class="">
                    <div class="row align-center-flex">
                      <div class="col-md-12">
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox"
                            name="mailing_confirm" id="mailing_confirm"
                            wire:model.defer="mailing_confirm">
                          <label class="form-check-label"
                            for="mailing_confirm" style="font-size: 12px;">
                            I have read the <a href="" target="blank"
                              class="link-font-bold">Terms &
                              Conditions</a> and <a href=""
                              target="blank" class="link-font-bold">Privacy
                              Policy</a>, and give consent to MyAll to share and
                            manage my data in accordance with the POPI Act.
                          </label>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="">
                    <div class="">
                      <input type="submit" id="less-padding"
                        class="btn btn-red rounded-pill" value="SUBMIT"
                        style="width: 100%; border: 0;
                        background-color:var(--theme-dark-pink);">
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  @push('scripts')
    <script>
      $(document).ready(function() {
        /*
        $('.password_toggle').on('click', function(){
            var input = $(this).parent().find('input');
            var type  = $(input).attr('type');
            if(type == "password"){
                $(input).attr('type', "text");
                $(this).find('i').addClass('fa-eye-slash');
                $(this).find('i').removeClass('fa-eye');

            }
            else{
                $(input).attr('type', 'password');
                $(this).find('i').removeClass('fa-eye-slash');
                $(this).find('i').addClass('fa-eye');
            }
        })
        */
      });
      $(document).ready(function() {
        window.addEventListener('show-modal', event => {
          $('.modal').modal('hide');
          $('#login-modal').modal('show');
        });
        window.addEventListener('show-mailing-list-modal', event => {
          $('#mailing-list-modal').modal('show');
        });
      });

      document.addEventListener('DOMContentLoaded', () => {
        const header = document.querySelector('header.__navigation');
        const watcher = document.createElement('div');
        watcher.setAttribute('data-scroll-watcher', '');
        document.body.before(watcher);


        const myallLandingLanguage = document.querySelector(
          '#myallLandingLanguage .__content');
        const languageMenu = document.querySelector(
          '#myallLandingLanguage .__lang-menu');

        const headerDataSet = header.dataset.header.trim();
        if (!headerDataSet) {
          const observer = new IntersectionObserver((entries) => {
            header.classList.toggle('__bg-full', !entries[0].isIntersecting)
          }, {
            rootMargin: '200px 0px 0px 0px'
          });

          observer.observe(watcher);
        }

        const offerLink = document.getElementById('offer-link');
        const offerLinkMenu = document.getElementById(
          'offer-menu');

        offerLink.addEventListener('mouseenter', () => {
          offerLinkMenu.style.display = 'block';
        });

        offerLinkMenu.addEventListener('mouseleave', () => {
          offerLinkMenu.style.display = 'none';
        });

        offerLinkMenu.querySelectorAll('a').forEach(link => {
          link.addEventListener('click', () => {
            offerLinkMenu.style.display = 'none';
          });
        });

        myallLandingLanguage.addEventListener('click', (event) => {
          languageMenu.style.display = languageMenu.style.display ===
            'block' ? 'none' : 'block';
          event.stopPropagation();
        });

        document.addEventListener('click', (event) => {
          if (!languageMenu.contains(event.target) && event.target !==
            myallLandingLanguage) {
            languageMenu.style.display = 'none';
          }
        });

      })

      const panel = document.querySelector('.__panel');
      const menuToggleBtn = document.getElementById('menu-toggle');
      const closeButton = document.querySelector('.__header button');
      const navLinks = document.querySelectorAll('.__nav-link');

      const togglePanel = () => {
        panel.classList.toggle('open');
        document.body.style.overflow = 'hidden';
      };

      const closePanel = () => {
        panel.classList.remove(
          'open');
        document.body.style.overflow = 'scroll';
      };

      navLinks.forEach(link => {
        link.addEventListener('click', () => {
          closePanel();
        });
      });

      if (menuToggleBtn) {
        menuToggleBtn.addEventListener('click', togglePanel);
      }

      if (closeButton) {
        closeButton.addEventListener('click', closePanel);
      }
    </script>
  @endpush
</header>
