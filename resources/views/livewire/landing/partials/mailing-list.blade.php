<div class="col-md-8 padding-left-60">
    <form wire:submit.prevent="savemailingList">
        <div class="row">
            <div class="col-md-12 mt-5">
                <h3 class="page-title sub-title page-title-mobile m-m-b">Want to know more or get exclusive updates about MyAll and the exciting features in development?</h3>
                <p style="font-size: 14px;" class="width-75 marg-bottom-25 font-40 center-txt m-m-b">Just add your details below and grant us permission to send you occasional communication via email and/or SMS. <span style="font-weight: 700;">We promise we won’t spam you.</span></p>
            </div>
            <div class="col-md-12">
                @if (session()->has('message'))
                    <div class="alert alert-success">
                        {{ session('message') }}
                    </div>
                @endif
                @if($errors->any())
                <div class="alert alert-danger">
                    <span>{{ $errors->first() }}</span>
                </div>
                @endif
            </div>
            <div class="col-md-12">
                <div class="mb-3">
                    <input type="text" class="form-control-white form-control" placeholder="Name and Surname*" name="name" wire:model.defer="name">
                </div>
            </div>
            <div class="col-md-12">
                <div class="mb-3">
                    <input type="text" class="form-control-white form-control" placeholder="Mobile Number*" name="contact_number" wire:model.defer="contact_number">
                </div>
            </div>
            <div class="col-md-12">
                <div class="mb-3 marg-bottom-25">
                    <input type="text" class="form-control-white form-control" placeholder="Email*" name="email" wire:model.defer="email">
                </div>
            </div>
            <div class="row">
                <div class="col-md-9">
                    <div class="row align-center-flex">
                        <div class="col-md-12">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="mailing_confirm" id="mailing_confirm" wire:model.defer="mailing_confirm">
                                <label class="form-check-label" for="mailing_confirm" style="font-size: 12px;">
                                    I have read the <a href="http://52.73.227.231/docs/MyAll Terms and Conditions.pdf" target="blank" class="link-font-bold">Terms & Conditions</a> and <a href="http://52.73.227.231/docs/MyAll Privacy Policy.pdf" target="blank" class="link-font-bold">Privacy Policy</a>, and give consent to WrapUp to share and manage my data in accordance with the POPI Act.
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="">
                        <input type="submit" id="less-padding" class="btn btn-red rounded-pill" value="SUBMIT">
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
