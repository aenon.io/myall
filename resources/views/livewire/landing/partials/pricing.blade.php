<div class="pricing-options-container">
  <div class="pricing-tabs p-subheading">
    <button class="pricing-tab @if (
        $selectedPlan == 'monthly' ||
            $selectedPlan == 'free' ||
            $selectedPlan == null) active @endif"
      id="monthly-tab" onclick="setActivePlan('monthly')">
      Monthly
    </button>
    <button class="pricing-tab @if ($selectedPlan == 'annual') active @endif"
      id="annual-tab" onclick="setActivePlan('annual')">
      Annual (Save 16%)
    </button>
  </div>
  @if (
      $selectedPlan == 'monthly' ||
          $selectedPlan == 'free' ||
          $selectedPlan == null)
    <div id="monthly-pricing" class="pricing-content">
      <div class="p-price" style="border-radius: 0px 8px 8px 8px">
        <p class="price">R29/Month</p>
      </div>
    </div>
  @elseif ($selectedPlan == 'annual')
    <div id="annual-pricing" class="pricing-content">
      <div class="p-price" style="border-radius: 8px 0px 8px 8px">
        <p class="price">R313/Year</p>
      </div>
    </div>
  @endif
</div>

<script>
  let activePlan = localStorage.getItem('activePlan') || 'monthly';

  function setActivePlan(plan) {
    activePlan = plan;
    localStorage.setItem('activePlan', plan);

    Livewire.emit('setSessionPlan', plan);
  }

  setActivePlan(activePlan);
</script>
