<div>
    <div class="pricing-main">
        <section class="wrapper container-colour">
            <div class="image-wrapper pricing-bg-image bg-image bg-full pb-15">
                <div class="card-body pt-10 pb-8 px-0">
                    <div class="container pt-5">
                        <div class="row gx-lg-8 gx-xl-12 gy-10 gy-lg-0 row-space-top mobile-row">
                            <div class="col-md-12 text-center">
                                <h2 class="page-title-white pricing-title text-white">Ready to streamline your important documents?</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="wrapper hero-bg pricing-hero-bg bg-overlay bg-overlay-400 bg-content">
            <div class="container pricing-container pt-10 pb-10" style="z-index: 5; position:relative">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card-box-style pricing-card rounded p-5 mb-5">
                            <div class="row d-flex justify-content-center">
                                <div class="col-md-12 text-center">
                                    <h3 class="text-pink pricing-card-title mt-5 mb-5"><b class="text-pink">PRICING</b></h3>
                                    <hr class="pricing-bottom-line">
                                </div>
                                <div class="col-md-12 d-block d-md-none mt-3 text-center">
                                    <b style="font-size: 12px"><i class="fa fa-chevron-left"></i> SLIDE TO VIEW OPTIONS <i class="fa fa-chevron-right"></i></b>
                                </div>
                                <div class="col-md-12 pt-5 overflow-auto">
                                    <h5 class="pricing-subtitle mt-5 mb-3"></h5>
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th scope="col">
                                                    <h5 class="pricing-subtitle mt-5 mb-3">Your access includes:</h5>
                                                </th>
                                                <th scope="col">
                                                    <p class="text-pink"><b class="text-pink">FREE <span class="text-blue">REGISTRATION</span></b></p>
                                                </th>
                                                <th scope="col">
                                                    <p class="text-pink">
                                                        <b class="text-blue">SIGN UP FOR OUR WRAPUP ORGANISER FOR ONLY</b> R399 PER YEAR! R33.33 A MONTH <b class="text-blue">SUBSCRIPTION FEE BILLED ANNUALLY.</b>
                                                    </p>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <th scope="row" class="text-blue" style="font-weight:100">Others can share their information with you from their MyAll profile</th>
                                                <td scope="row" class="text-center"><img src="{{ asset('img/pricing/ELEMENT TICK.png') }}" class="img-fluid" alt=""></td>
                                                <td scope="row" class="text-center"><img src="{{ asset('img/pricing/ELEMENT TICK.png') }}" class="img-fluid" alt=""></td>
                                            </tr>
                                            <tr>
                                                <th scope="row" class="text-blue" style="font-weight:100">Accessible on laptops, phones, or tablets with internet connectivity</th>
                                                <td scope="row" class="text-center"><img src="{{ asset('img/pricing/ELEMENT TICK.png') }}" class="img-fluid" alt=""></td>
                                                <td scope="row" class="text-center"><img src="{{ asset('img/pricing/ELEMENT TICK.png') }}" class="img-fluid" alt=""></td>
                                            </tr>
                                            <tr>
                                                <th scope="row" class="text-blue" style="font-weight:100">Your entered information is securely stored</th>
                                                <td scope="row" class="text-center"><img src="{{ asset('img/pricing/ELEMENT TICK.png') }}" class="img-fluid" alt=""></td>
                                                <td scope="row" class="text-center"><img src="{{ asset('img/pricing/ELEMENT TICK.png') }}" class="img-fluid" alt=""></td>
                                            </tr>
                                            <tr>
                                                <th scope="row" class="text-blue" style="font-weight:100">Option to upgrade anytime to full access</th>
                                                <td scope="row" class="text-center"><img src="{{ asset('img/pricing/ELEMENT TICK.png') }}" class="img-fluid" alt=""></td>
                                                <td scope="row" class="text-center"><img src="{{ asset('img/pricing/ELEMENT TICK.png') }}" class="img-fluid" alt=""></td>
                                            </tr>
                                            <tr>
                                                <th scope="row" class="text-blue" style="font-weight:100">Document Organiser: Easily upload and store personal and financial information</th>
                                                <td scope="row" class="text-center"><img src="{{ asset('img/list-x.png') }}" class="img-fluid" alt=""></td>
                                                <td scope="row" class="text-center"><img src="{{ asset('img/pricing/ELEMENT TICK.png') }}" class="img-fluid" alt=""></td>
                                            </tr>
                                            <tr>
                                                <th scope="row" class="text-blue" style="font-weight:100">Keep track of vital information about yourself and your loved ones</th>
                                                <td scope="row" class="text-center"><img src="{{ asset('img/list-x.png') }}" class="img-fluid" alt=""></td>
                                                <td scope="row" class="text-center"><img src="{{ asset('img/pricing/ELEMENT TICK.png') }}" class="img-fluid" alt=""></td>
                                            </tr>
                                            <tr>
                                                <th scope="row" class="text-blue" style="font-weight:100">Share important information with others in case of emergencies</th>
                                                <td scope="row" class="text-center"><img src="{{ asset('img/list-x.png') }}" class="img-fluid" alt=""></td>
                                                <td scope="row" class="text-center"><img src="{{ asset('img/pricing/ELEMENT TICK.png') }}" class="img-fluid" alt=""></td>
                                            </tr>
                                            <tr>
                                                <th scope="row" class="text-blue" style="font-weight:100">Your Network Builder provides a visual overview of your information exchanges</th>
                                                <td scope="row" class="text-center"><img src="{{ asset('img/list-x.png') }}" class="img-fluid" alt=""></td>
                                                <td scope="row" class="text-center"><img src="{{ asset('img/pricing/ELEMENT TICK.png') }}" class="img-fluid" alt=""></td>
                                            </tr>
                                            <tr>
                                                <th scope="row" class="text-blue" style="font-weight:100">Online storage vault for any extra essential documents</th>
                                                <td scope="row" class="text-center"><img src="{{ asset('img/list-x.png') }}" class="img-fluid" alt=""></td>
                                                <td scope="row" class="text-center"><img src="{{ asset('img/pricing/ELEMENT TICK.png') }}" class="img-fluid" alt=""></td>
                                            </tr>
                                            <tr>
                                                <th scope="row" class="text-blue" style="font-weight:100">Keep track of debts, expenses, loans, policies, and more</th>
                                                <td scope="row" class="text-center"><img src="{{ asset('img/list-x.png') }}" class="img-fluid" alt=""></td>
                                                <td scope="row" class="text-center"><img src="{{ asset('img/pricing/ELEMENT TICK.png') }}" class="img-fluid" alt=""></td>
                                            </tr>
                                            <tr>
                                                <th scope="row" class="text-blue" style="font-weight:100">Create your online Will or upload an existing Will</th>
                                                <td scope="row" class="text-center"><img src="{{ asset('img/list-x.png') }}" class="img-fluid" alt=""></td>
                                                <td scope="row" class="text-center"><img src="{{ asset('img/pricing/ELEMENT TICK.png') }}" class="img-fluid" alt=""></td>
                                            </tr>
                                            <tr>
                                                <th scope="row" class="text-blue" style="font-weight:100">Effortless and speedy Will drafting process, expertly guided by informative videos and infographics compiled by our attorney who specialises in deceased estates and Wills.</th>
                                                <td scope="row" class="text-center"><img src="{{ asset('img/list-x.png') }}" class="img-fluid" alt=""></td>
                                                <td scope="row" class="text-center"><img src="{{ asset('img/pricing/ELEMENT TICK.png') }}" class="img-fluid" alt=""></td>
                                            </tr>
                                            <tr>
                                                <th scope="row" class="text-blue" style="font-weight:100">Intelligent system to save time, reduce stress, and improve productivity</th>
                                                <td scope="row" class="text-center"><img src="{{ asset('img/list-x.png') }}" class="img-fluid" alt=""></td>
                                                <td scope="row" class="text-center"><img src="{{ asset('img/pricing/ELEMENT TICK.png') }}" class="img-fluid" alt=""></td>
                                            </tr>
                                            <tr>
                                                <th scope="row" class="text-blue" style="font-weight:100">Storage of your entered data for 3 months after cancellation or when you do not renew your annual subscription.</th>
                                                <td scope="row" class="text-center"><img src="{{ asset('img/list-x.png') }}" class="img-fluid" alt=""></td>
                                                <td scope="row" class="text-center"><img src="{{ asset('img/pricing/ELEMENT TICK.png') }}" class="img-fluid" alt=""></td>
                                            </tr>
                                            <tr>
                                                <th scope="row" class="text-blue" style="font-weight:100">And much more!</th>
                                                <td scope="row" class="text-center"><img src="{{ asset('img/list-x.png') }}" class="img-fluid" alt=""></td>
                                                <td scope="row" class="text-center"><img src="{{ asset('img/pricing/ELEMENT TICK.png') }}" class="img-fluid" alt=""></td>
                                            </tr>
                                            <tr>
                                                <th scope="row"></th>
                                                <td scope="row" class="text-center"><span><a href="#" class="btn btn-gradient gradient-1 btn-lg rounded-pill pricing-btn" wire:click.prevent="showRegister('free')"=>FREE ACCESS</a></span></td>
                                                <td scope="row" class="text-center"><span><a href="#" class="btn btn-gradient gradient-1 btn-lg rounded-pill pricing-btn" wire:click.prevent="showRegister('paid')">FULL ACCESS</a></span></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!--<div class="" style="margin-top: 3.5rem !important;">-->
                        <!--    <p class="pricing-cancellation text-center">-->
                        <!--    You can cancel your subscription at any time using the form below. Please note that refunds are not available. -->
                        <!--    However, we will securely store your entered data for 3 months after cancellation. This ensures a seamless renewal process, -->
                        <!--    allowing you to effortlessly update any information that hasn't been modified in the past 3 months. -->
                        <!--    The same applies if you decide not to renew the yearly subscription.-->
                        <!--    </p>-->
                        <!--</div>-->
                    </div>
                </div>
            </div>
        </section>
        <section class="wrapper section-2" id="contact-us">
            <div class="card image-wrapper bg-full bg-overlay bg-overlay-light-500">
                <div class="grey-fill"></div>
                <div class="card-body px-0 mt-lg-n14 mobile-left-right">
                    <div class="container">
                        <livewire:landing.partials.contact />
                    </div>
                </div>
            </div>
        </section>
        <section class="wrapper" style="background-color: #eff1f4;">
            <div class="container py-10">
                <div class="row align-center-flex">
                    <div class="col-md-4">
                        <div class="row">
                            <img class="" src="{{ asset('img/SUB_IMAGE.png') }}" alt="Images">
                        </div>
                    </div>
                    <livewire:landing.partials.mailing-list>
                        <div class="col-md-4">
                        </div>
                        <div class="col-md-8 padding-left-60">
                            <hr class="pricing-bottom-line-2">
                            <p style="font-size: 14px;" class="width-75 marg-bottom-25 font-40 center-txt m-m-b">Your data will be safely secured and stored in accordance with the POPI Act and we take all necessary precautions to treat it with utmost sensitivity and respect.</p>
                            <p style="font-size: 14px;" class="width-75 marg-bottom-25 font-40 center-txt m-m-b">All information is encrypted with a BCrypt hashing algorithm meeting industry-standard encryption, strong password policies, and regular security measures to safeguard your information, ensuring a safe and trustworthy experience on our platform. Want to know more? Send us an email to <a href="mailto:hello@myall.co.za">hello@myall.co.za</a> with your questions.</p>
                        </div>
                </div>
            </div>
        </section>
    </div>
</div>
