<div>
    <section class="wrapper section-2">
        <div class="container-card">
            <div class="mb-5 mt-5">
                <div class="px-0">
                    <div class="container">
                        @if($is_logged_in)
                        <div class="card">
                            <div class="card-body">
                                @if($user)
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row d-flex justify-content-center">
                                                <div class="col-sm-12 col-md-12 col-lg-6 center-img mb-2">
                                                    @php
                                                    $pic = asset('img/placeholder.png');
                                                    if($user->profile_pic){
                                                        $pic = asset("storage/".$user->profile_pic);
                                                    }
                                                    @endphp
                                                    <div class="profile_img_cont" style="width: 150px; height: 150px; background-color: red; background-size: cover; border-radius:50% 50% 50% 50%;" data-image="{{ $pic }}"></div>
                                                </div>
                                                <div class="col-sm-12 col-md-12 col-lg-6">
                                                    <h6><a href="#" class="link-dark">{{ $user->name.' '.$user->middle_name.' '.$user->surname }}</a></h6>
                                                    <span class="post-meta fs-15">{{ $user->occupation }}</span>
                                                    <p>{{ $user->bio }}</p>
                                                </div>
                                            </div>
                                    </div>
                                    <div class="col-md-12 mt-3">
                                        <span class="text-muted">Click on a tab below to see the categories shared with you</span>
                                    </div>
                                    <div class="col-md-12">
                                        <ul class="nav nav-tabs nav-tabs-basic share_nav" role="tablist">
                                            @if($share->profile)
                                            <li class="nav-item" role="presentation"><a class="nav-link sh-nav-link active" data-bs-toggle="tab" href="#profile" aria-selected="true" role="tab">Profile</a></li>
                                            @endif
                                            @if($share->family_tree)
                                            <li class="nav-item" role="presentation"><a class="nav-link sh-nav-link" data-bs-toggle="tab" href="#family-tree" aria-selected="false" role="tab" tabindex="-1">My Network</a></li>
                                            @endif
                                            @if($share->organiser)
                                            <li class="nav-item" role="presentation"><a class="nav-link sh-nav-link" data-bs-toggle="tab" href="#organiser" aria-selected="false" role="tab" tabindex="-1">Organiser</a></li>
                                            @endif
                                        </ul>
                                        <div class="tab-content mt-0 mt-md-5">
                                            <div class="tab-pane fade active show" id="profile" role="tabpanel">
                                                <div class="col-md-12 overflow-auto">
                                                    <table class="table">
                                                        <tbody>
                                                            <tr>
                                                                <td><b>Name</b></td>
                                                                <td>{{ $user->name.' '.$user->middle_name.' '.$user->surname }}</td>
                                                            </tr>
                                                            <tr>
                                                                <td><b>DOB</b></td>
                                                                <td>{{ $user->date_of_birth }}</td>
                                                            </tr>
                                                            <tr>
                                                                <td><b>ID Number</b></td>
                                                                <td>{{ $user->id_number }}</td>
                                                            </tr>
                                                            <tr>
                                                                <td><b>Contact Number</b></td>
                                                                <td>{{ $user->contact_number }}</td>
                                                            </tr>
                                                            <tr>
                                                                <td><b>Email</b></td>
                                                                <td>{{ $user->email }}</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="family-tree" role="tabpanel">
                                                <div class="col-md-12">
                                                    <div class="card-box-style rounded p-5">
                                                        <div style="width:100%; height:700px;" id="tree" wire:ignore></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="organiser" role="tabpanel">
                                                <div class="row">
                                                    <div class="col-md-12 my3">
                                                        <span class="text-muted">Click on one of the Categories below and scroll down to reveal more information</span>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <ul class="nav nav-tabs nav-tabs-basic flex-column share_nav share_nav_vertical">
                                                            @if($share->personal)
                                                                <li class="nav-item" role="presentation">
                                                                    <a class="nav-link sh-nav-link active" data-bs-toggle="tab" aria-selected="true" role="tab" href="#personal">Personal</a>
                                                                </li>
                                                            @endif
                                                            @if($share->connections)
                                                                @if($user->connections->count() > 0)
                                                                <li class="nav-item" role="presentation">
                                                                    <a class="nav-link sh-nav-link" data-bs-toggle="tab" aria-selected="false" role="tab" tabindex="-1" href="#connections">Emergency Contacts</a>
                                                                </li>
                                                                @endif
                                                            @endif
                                                            @if($share->business_wealth)
                                                                @if($user->businessWealths->count() > 0)
                                                                <li class="nav-item" role="presentation">
                                                                    <a class="nav-link sh-nav-link" data-bs-toggle="tab" aria-selected="false" role="tab" tabindex="-1" href="#BusinessWealth">Business Wealth</a>
                                                                </li>
                                                                @endif
                                                            @endif
                                                            @if($share->properties_wealth)
                                                                <li class="nav-item" role="presentation">
                                                                    <a class="nav-link sh-nav-link" data-bs-toggle="tab" aria-selected="false" role="tab" tabindex="-1" href="#properties_wealth">Personal Wealth</a>
                                                                </li>
                                                            @endif
                                                            @if($share->debt_expenses)
                                                                 <li class="nav-item" role="presentation">
                                                                    <a class="nav-link sh-nav-link" data-bs-toggle="tab" aria-selected="false" role="tab" tabindex="-1" href="#debt_expenses">Debt & Expenses</a>
                                                                </li>
                                                            @endif
                                                            {{--
                                                            <li class="nav-item" role="presentation">
                                                                <a class="nav-link sh-nav-link" data-bs-toggle="tab" aria-selected="false" role="tab" tabindex="-1" href="#">Debt & Monthly Expenses</a>
                                                            </li>
                                                            --}}
                                                            @if($share->social_media)
                                                                <li class="nav-item" role="presentation">
                                                                    <a class="nav-link sh-nav-link" data-bs-toggle="tab" aria-selected="false" role="tab" tabindex="-1" href="#social_media">Social Media</a>
                                                                </li>
                                                            @endif
                                                            @if($share->safe_keeping)
                                                                <li class="nav-item" role="presentation">
                                                                    <a class="nav-link sh-nav-link" data-bs-toggle="tab" aria-selected="false" role="tab" tabindex="-1" href="#safe_keeping">Safekeeping</a>
                                                                </li>
                                                            @endif
                                                            {{--
                                                            <li class="nav-item" role="presentation">
                                                                <a class="nav-link sh-nav-link" data-bs-toggle="tab" aria-selected="false" role="tab" tabindex="-1" href="#">Memories</a>
                                                            </li>
                                                            --}}
                                                            @if($share->memories)
                                                                <li class="nav-item" role="presentation">
                                                                    <a class="nav-link sh-nav-link" data-bs-toggle="tab" aria-selected="false" role="tab" tabindex="-1" href="#memories">Memories</a>
                                                                </li>
                                                            @endif
                                                            @if($share->funeral)
                                                                <li class="nav-item" role="presentation">
                                                                    <a class="nav-link sh-nav-link" data-bs-toggle="tab" aria-selected="false" role="tab" tabindex="-1" href="#funeral">Funeral</a>
                                                                </li>
                                                            @endif
                                                        </ul>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <div class="tab-content mt-0 mt-md-5">
                                                            @if($share->personal)
                                                                <div class="tab-pane fade active show" id="personal" role="tabpanel">
                                                                    <h2>Personal</h2>
                                                                    <div class="accordion accordion-wrapper" id="accordionSimpleExample">
                                                                        @if($share->profile)
                                                                            <div class="card plain accordion-item">
                                                                                <div class="card-header" id="headingSimpleOne">
                                                                                    <button class="collapsed" data-bs-toggle="collapse" data-bs-target="#collapseSimpleOne" aria-expanded="false" aria-controls="collapseSimpleOne"> Account </button>
                                                                                </div>
                                                                                <div id="collapseSimpleOne" class="accordion-collapse collapse" aria-labelledby="headingSimpleOne" data-bs-parent="#accordionSimpleExample">
                                                                                    <div class="col-md-12 overflow-auto">
                                                                                        <table class="table">
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <td><b>Name</b></td>
                                                                                                    <td>{{ $user->name.' '.$user->middle_name.' '.$user->surname }}</td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td><b>DOB</b></td>
                                                                                                    <td>{{ $user->date_of_birth }}</td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td><b>ID Number</b></td>
                                                                                                    <td>{{ $user->id_number }}</td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>Contact Number</td>
                                                                                                    <td>{{ $user->contact_number }}</td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>Email</b></td>
                                                                                                    <td>{{ $user->email }}</td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        @endif
                                                                        @if($share->education)
                                                                            <div class="card plain accordion-item">
                                                                                <div class="card-header" id="headingEducation">
                                                                                    <button class="collapsed" data-bs-toggle="collapse" data-bs-target="#collapseEducation" aria-expanded="false" aria-controls="collapseEducation"> Education </button>
                                                                                </div>
                                                                                <div id="collapseEducation" class="accordion-collapse collapse" aria-labelledby="headingEducation" data-bs-parent="#accordionSimpleExample">
                                                                                    <div class="card-body">
                                                                                        <div class="col-md-12 overflow-auto">
                                                                                            <table class="table">
                                                                                            <thead>
                                                                                                <tr>
                                                                                                    <th>Qualification</th>
                                                                                                    <th>Year Of Completion</th>
                                                                                                    <th>Comment</th>
                                                                                                </tr>
                                                                                            </thead>
                                                                                            <tbody>
                                                                                                @foreach($user->education AS $education)
                                                                                                <tr>
                                                                                                    <td>{{ $education->qualification }}</td>
                                                                                                    <td>{{ $education->year }}</td>
                                                                                                    <td>{{ $education->comment }}</td>
                                                                                                </tr>
                                                                                                @endforeach
                                                                                            </tbody>
                                                                                        </table>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div> 
                                                                        @endif
                                                                        @if($share->spouse)
                                                                            <div class="card plain accordion-item">
                                                                                <div class="card-header" id="headingSimpleTwo">
                                                                                    <button class="collapsed" data-bs-toggle="collapse" data-bs-target="#collapseSimpleTwo" aria-expanded="false" aria-controls="collapseSimpleTwo"> Marriage / Spouse / Life Partner  </button>
                                                                                </div>
                                                                                <div id="collapseSimpleTwo" class="accordion-collapse collapse" aria-labelledby="headingSimpleTwo" data-bs-parent="#accordionSimpleExample">
                                                                                    <div class="card-body">
                                                                                        <div class="col-md-12 overflow-auto">
                                                                                            <table class="table">
                                                                                            <thead>
                                                                                                <tr>
                                                                                                    <th>Status</th>
                                                                                                    <th>Name</th>
                                                                                                </tr>
                                                                                            </thead>
                                                                                            <tbody>
                                                                                                @foreach($user->spouses AS $spouse)
                                                                                                <tr>
                                                                                                    <td>{{ $spouse->marital_status }}</td>
                                                                                                    <td>{{ $spouse->spouse_name.' '.$spouse->spouse_surname }}</td>
                                                                                                </tr>
                                                                                                @endforeach
                                                                                            </tbody>
                                                                                        </table>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        @endif
                                                                        @if($share->work)
                                                                            @if($user->work)
                                                                            <div class="card plain accordion-item">
                                                                                <div class="card-header" id="headingSimpleThree">
                                                                                    <button class="collapsed" data-bs-toggle="collapse" data-bs-target="#collapseSimpleThree" aria-expanded="false" aria-controls="collapseSimpleThree"> Place Of Work </button>
                                                                                </div>
                                                                                <div id="collapseSimpleThree" class="accordion-collapse collapse" aria-labelledby="headingSimpleThree" data-bs-parent="#accordionSimpleExample">
                                                                                    <div class="card-body">
                                                                                        <p>
                                                                                            <b>Company Name</b><br />
                                                                                            {{ $user->work->employer_name }}
                                                                                        </p>
                                                                                        <p>
                                                                                            <b>Salary Number</b><br />
                                                                                            {{ $user->work->salary_number }}
                                                                                        </p>
                                                                                        <p>
                                                                                            <b>Address</b><br />
                                                                                            {{ $user->work->employer_address }}
                                                                                        </p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            @endif
                                                                        @endif
                                                                        @if($share->children)
                                                                            @if($user->children->count() > 0)
                                                                            <div class="card plain accordion-item">
                                                                                <div class="card-header" id="headingSimpleFive">
                                                                                    <button class="collapsed" data-bs-toggle="collapse" data-bs-target="#collapseSimpleFive" aria-expanded="false" aria-controls="collapseSimpleFive"> Children </button>
                                                                                </div>
                                                                                <div id="collapseSimpleFive" class="accordion-collapse collapse" aria-labelledby="headingSimpleFive" data-bs-parent="#accordionSimpleExample">
                                                                                    <div class="card-body">
                                                                                        <div class="col-md-12 overflow-auto">
                                                                                            <table class="table">
                                                                                            <thead>
                                                                                                <tr>
                                                                                                    <th>Name</th>
                                                                                                    <th>DOB</th>
                                                                                                </tr>
                                                                                            </thead>
                                                                                            <tbody>
                                                                                                @foreach($user->children AS $child)
                                                                                                <tr>
                                                                                                    <td>{{ $child->child_name.' '.$child->middle_name.' '.$child->child_surname }}</td>
                                                                                                    <td>{{ $child->child_date_of_birth }}</td>
                                                                                                </tr>
                                                                                                @endforeach
                                                                                            </tbody>
                                                                                        </table>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            @endif
                                                                        @endif
                                                                        @if($share->medical_aid)
                                                                            @if($user->medicalAids->count() > 0)
                                                                            <div class="card plain accordion-item">
                                                                                <div class="card-header" id="headingSimpleSeven">
                                                                                    <button class="collapsed" data-bs-toggle="collapse" data-bs-target="#collapseSimpleSeven" aria-expanded="false" aria-controls="collapseSimpleSeven"> Medical Aid</button>
                                                                                </div>
                                                                                <div id="collapseSimpleSeven" class="accordion-collapse collapse" aria-labelledby="headingSimpleSeven" data-bs-parent="#accordionSimpleExample">
                                                                                    <div class="card-body">
                                                                                        <div class="col-md-12 overflow-auto">
                                                                                            <table class="table">
                                                                                            <thead>
                                                                                                <tr>
                                                                                                    <th>Company</th>
                                                                                                    <th>Reference #</th>
                                                                                                    <th>Certificate</th>
                                                                                                </tr>
                                                                                            </thead>
                                                                                            <tbody>
                                                                                                @foreach($user->medicalAids AS $aid)
                                                                                                <tr>
                                                                                                    <td>{{ $aid->company }}</td>
                                                                                                    <td>{{ $aid->reference_number }}</td>
                                                                                                    <td>
                                                                                                        @if($aid->medical_aid_certificate)
                                                                                                        <a href="{{ url('storage/'.$aid->medical_aid_certificate) }}" target="_blank">View Document</a>
                                                                                                        @endif
                                                                                                    </td>
                                                                                                </tr>
                                                                                                @endforeach
                                                                                            </tbody>
                                                                                        </table>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            @endif
                                                                        @endif
                                                                        {{-- @if($share->what_to_find)
                                                                            @if($user->itemToFind->count() > 0)
                                                                            <div class="card plain accordion-item">
                                                                                <div class="card-header" id="headingSimpleSix">
                                                                                    <button class="collapsed" data-bs-toggle="collapse" data-bs-target="#collapseSimpleSix" aria-expanded="false" aria-controls="collapseSimpleSix"> Where To Find</button>
                                                                                </div>
                                                                                <div id="collapseSimpleSix" class="accordion-collapse collapse" aria-labelledby="headingSimpleSix" data-bs-parent="#accordionSimpleExample">
                                                                                    <div class="card-body">
                                                                                        <table class="table">
                                                                                            <thead>
                                                                                                <tr>
                                                                                                    <th>Name</th>
                                                                                                    <th>Where</th>
                                                                                                </tr>
                                                                                            </thead>
                                                                                            <tbody>
                                                                                                @foreach($user->ItemToFind AS $item)
                                                                                                <tr>
                                                                                                    <td>{{ $item->item_name }}</td>
                                                                                                    <td>{{ $item->location }}</td>
                                                                                                </tr>
                                                                                                @endforeach
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            @endif
                                                                        @endif --}}
                                                                    </div>
                                                                </div>
                                                            @endif
                                                            @if($share->connections)
                                                                @if($user->connections->count() > 0)
                                                                <div class="tab-pane fade" id="connections" role="tabpanel">
                                                                    <h2>Connections</h2>
                                                                    <div class="accordion accordion-wrapper" id="accordionConnections">
                                                                        @php
                                                                        $conn_types = [
                                                                            'family',
                                                                            'personal',
                                                                            'business',
                                                                            'doctors',
                                                                            'emergency',
                                                                            'accountant_bookkeeper',
                                                                            'private_banker',
                                                                            'lawyer',
                                                                        ];
                                                                        @endphp
                                                                        @foreach($conn_types AS $type)
                                                                            @if($share->$type)
                                                                                @if($user->connectionsByType($type)->count() > 0)
                                                                                    <div class="card plain accordion-item">
                                                                                        <div class="card-header" id="heading{{ $type }}">
                                                                                            <button class="collapsed" data-bs-toggle="collapse" data-bs-target="#collapse{{ $type }}" aria-expanded="false" aria-controls="collapse{{ $type }}"> @if($type == 'personal') Friends @else {{ ucwords( str_replace('_', ' ', $type)) }} @endif</button>
                                                                                        </div>
                                                                                        <div id="collapse{{ $type }}" class="accordion-collapse collapse" aria-labelledby="heading{{ $type }}" data-bs-parent="#accordionConnections">
                                                                                            <div class="card-body">
                                                                                                <div class="col-md-12 overflow-auto">
                                                                                                    <table class="table">
                                                                                                    <thead>
                                                                                                        <tr>
                                                                                                            <th>Name</th>
                                                                                                            <th>Relation / Company</th>
                                                                                                            <th>Email</th>
                                                                                                            <th>Contact</th>
                                                                                                        </tr>
                                                                                                    </thead>
                                                                                                    <tbody>
                                                                                                        
                                                                                                        @foreach($user->connectionsByType($type) AS $cnt)
                                                                                                        <tr>
                                                                                                            <td>{{ $cnt->name.' '.$cnt->surname }}</td>
                                                                                                            <td>{{ $cnt->relation }}</td>
                                                                                                            <td>{{ $cnt->email }}</td>
                                                                                                            <td>{{ $cnt->contact_number }}</td>
                                                                                                        </tr>
                                                                                                        @endforeach
                                                                                                    </tbody>
                                                                                                </table>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                @endif
                                                                            @endif
                                                                        @endforeach
                                                                    </div>
                                                                </div>
                                                                @endif
                                                            @endif
                                                            @if($share->business_wealth)
                                                                @if($user->businessWealths->count() > 0)
                                                                <div class="tab-pane fade" id="BusinessWealth" role="tabpanel">
                                                                    <div class="col-md-12 overflow-auto">
                                                                        <table class="table">
                                                                        <thead>
                                                                            <tr>
                                                                                <th>Business Name</th>
                                                                                <th>Business Type</th>
                                                                                <th>Registration Number</th>
                                                                                <th>VAT Number</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            @foreach($user->businessWealths AS $cmp)
                                                                            <tr>
                                                                                <td>{{ $cmp->business_name }}</td>
                                                                                <td>{{ $cmp->business_type }}</td>
                                                                                <td>{{ $cmp->registration_number }}</td>
                                                                                <td>{{ $cmp->vat_number }}</td>
                                                                            </tr>
                                                                            @endforeach
                                                                        </tbody>
                                                                    </table>
                                                                    </div>
                                                                </div>
                                                                @endif
                                                            @endif
                                                            @if($share->properties_wealth)
                                                                <div class="tab-pane fade" id="properties_wealth" role="tabpanel">
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <div class="accordion accordion-wrapper" id="accordion-property-wealth">
                                                                                <div class="card plain accordion-item">
                                                                                    <div class="card-header" id="assets-heading">
                                                                                        <button class="collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#assets" aria-expanded="true" aria-controls="assets">
                                                                                            Assets
                                                                                        </button>
                                                                                    </div>
                                                                                    <div id="assets" class="accordion-collapse collapse" aria-labelledby="assets-heading" data-bs-parent="#accordion-property-wealth">
                                                                                        <div class="accordion-body">
                                                                                            <div class="col-md-12 overflow-auto">
                                                                                                <table class="table">
                                                                                                <thead>
                                                                                                    <tr>
                                                                                                        <th>Type</th>
                                                                                                        <th class="text-end">Value</th>
                                                                                                        <th class="text-end">Outstanding</th>
                                                                                                    </tr>
                                                                                                </thead>
                                                                                                <tbody>
                                                                                                    @foreach($user->assets AS $asset)
                                                                                                    <tr>
                                                                                                        <td>{{ $asset->asset_type }}</td>
                                                                                                        <td class="text-end">{{ number_format($asset->value, 2) }}</td>
                                                                                                        <td class="text-end">{{ number_format($asset->outstanding, 2) }}</td>
                                                                                                    </tr>
                                                                                                    @endforeach
                                                                                                </tbody>
                                                                                            </table>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="card plain accordion-item">
                                                                                    <div class="card-header" id="investment-heading">
                                                                                        <button class="collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#investment" aria-expanded="false" aria-controls="investment">
                                                                                            Investments and Policies
                                                                                        </button>
                                                                                    </div>
                                                                                    <div id="investment" class="accordion-collapse collapse" aria-labelledby="investment-heading" data-bs-parent="#accordion-property-wealth">
                                                                                        <div class="accordion-body">
                                                                                            <div class="col-md-12 overflow-auto">
                                                                                                <table class="table">
                                                                                                <thead>
                                                                                                    <tr>
                                                                                                        <th>Type</th>
                                                                                                        <th class="text-end">Value</th>
                                                                                                        <th>File</th>
                                                                                                    </tr>
                                                                                                </thead>
                                                                                                <tbody>
                                                                                                    @foreach($user->investments AS $inv)
                                                                                                    <tr>
                                                                                                        <td>{{ $inv->investment_type }}</td>
                                                                                                        <td class="text-end">{{ number_format($inv->value,2) }}</td>
                                                                                                        <td>
                                                                                                            @if($inv->file)
                                                                                                            <a href="{{ url('storage/'.$inv->file) }}" target="_blank">View</a>
                                                                                                            @endif
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    @endforeach
                                                                                                </tbody>
                                                                                            </table>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="card plain accordion-item">
                                                                                    <div class="card-header" id="banks-heading">
                                                                                        <button class="collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#banks" aria-expanded="false" aria-controls="banks">
                                                                                            Bank Accounts
                                                                                        </button>
                                                                                    </div>
                                                                                    <div id="banks" class="accordion-collapse collapse" aria-labelledby="banks-heading" data-bs-parent="#accordion-property-wealth">
                                                                                        <div class="accordion-body">
                                                                                            <div class="col-md-12 overflow-auto">
                                                                                                <table class="table">
                                                                                                <thead>
                                                                                                    <tr>
                                                                                                        <th>Account Type</th>
                                                                                                        <th>Bank</th>
                                                                                                        <th>Account Number</th>
                                                                                                        <th>Value</th>
                                                                                                        <th>File</th>
                                                                                                    </tr>
                                                                                                </thead>
                                                                                                <tbody>
                                                                                                    @foreach($user->banks AS $bnk)
                                                                                                    <tr>
                                                                                                        <td>{{ $bnk->account_type }}</td>
                                                                                                        <td>{{ $bnk->bank }}</td>
                                                                                                        <td>{{ $bnk-> account_number }}</td>
                                                                                                        <td>{{ $bnk->value }}</td>
                                                                                                        <td>
                                                                                                            @if($bnk->file)
                                                                                                            <a href="{{ url('storage/'.$bnk->file) }}">View</a>
                                                                                                            @endif
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    @endforeach
                                                                                                </tbody>
                                                                                            </table>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            @endif
                                                            @if($share->debt_expenses)
                                                                <div class="tab-pane fade" id="debt_expenses" role="tabpanel">
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <div class="accordion accordion-wrapper" id="accordion-property-wealth">
                                                                                @if($share->expenses)
                                                                                    <div class="card plain accordion-item">
                                                                                        <div class="card-header" id="expenses-heading">
                                                                                            <button class="collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#expenses" aria-expanded="true" aria-controls="expenses">
                                                                                                Expenses
                                                                                            </button>
                                                                                        </div>
                                                                                        <div id="expenses" class="accordion-collapse collapse" aria-labelledby="expenses-heading" data-bs-parent="#accordion-property-wealth">
                                                                                            <div class="accordion-body">
                                                                                                <div class="col-md-12 overflow-auto">
                                                                                                    <table class="table">
                                                                                                    <thead>
                                                                                                        <tr>
                                                                                                            <th>Description</th>
                                                                                                            <th>Amount</th>
                                                                                                            <th>Rate</th>
                                                                                                        </tr>
                                                                                                    </thead>
                                                                                                    <tbody>
                                                                                                        @foreach($user->expenses AS $expense)
                                                                                                        <tr>
                                                                                                            @if($expense->type == 'Expenses')
                                                                                                                <td>{{ $expense->description }}</td>
                                                                                                                <td>{{ number_format($expense->amount, 2) }}</td>
                                                                                                                <td>{{ $expense->rate }}</td>
                                                                                                            @endif
                                                                                                        </tr>
                                                                                                        @endforeach
                                                                                                    </tbody>
                                                                                                </table>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                @endif
                                                                                @if($share->loans)
                                                                                    <div class="card plain accordion-item">
                                                                                        <div class="card-header" id="loans-heading">
                                                                                            <button class="collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#loans" aria-expanded="true" aria-controls="loans">
                                                                                                Loans
                                                                                            </button>
                                                                                        </div>
                                                                                        <div id="loans" class="accordion-collapse collapse" aria-labelledby="loans-heading" data-bs-parent="#accordion-property-wealth">
                                                                                            <div class="accordion-body">
                                                                                                <div class="col-md-12 overflow-auto">
                                                                                                    <table class="table">
                                                                                                    <thead>
                                                                                                        <tr>
                                                                                                            <th>Lender</th>
                                                                                                            <th>Description</th>
                                                                                                            <th>Amount</th>
                                                                                                            <th>Rate</th>
                                                                                                        </tr>
                                                                                                    </thead>
                                                                                                    <tbody>
                                                                                                        @foreach($user->expenses AS $expense)
                                                                                                        <tr>
                                                                                                            @if($expense->type == 'Loans')
                                                                                                                <td>{{ $expense->lender }}</td>
                                                                                                                <td>{{ $expense->description }}</td>
                                                                                                                <td>{{ number_format($expense->amount, 2) }}</td>
                                                                                                                <td>{{ $expense->rate }}</td>
                                                                                                            @endif
                                                                                                        </tr>
                                                                                                        @endforeach
                                                                                                    </tbody>
                                                                                                </table>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                @endif
                                                                                @if($share->insurance)
                                                                                    <div class="card plain accordion-item">
                                                                                        <div class="card-header" id="insurance-heading">
                                                                                            <button class="collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#insurance" aria-expanded="false" aria-controls="insurance">
                                                                                                Insurance
                                                                                            </button>
                                                                                        </div>
                                                                                        <div id="insurance" class="accordion-collapse collapse" aria-labelledby="insurance-heading" data-bs-parent="#accordion-property-wealth">
                                                                                            <div class="accordion-body">
                                                                                                <div class="col-md-12 overflow-auto">
                                                                                                    <table class="table">
                                                                                                    <thead>
                                                                                                        <tr>
                                                                                                            <th>Type</th>
                                                                                                            <th>Insurer</th>
                                                                                                            <th>Policy Number</th>
                                                                                                            <th>Contact</th>
                                                                                                            <th>Financial Advisor</th>
                                                                                                            <th class="text-end">Outstanding Debt</th>
                                                                                                        </tr>
                                                                                                    </thead>
                                                                                                    <tbody>
                                                                                                        @foreach($user->insurances AS $ins)
                                                                                                        <tr>
                                                                                                            <td>{{ $ins->insurance_type }}</td>
                                                                                                            <td>{{ $ins->insurer }}</td>
                                                                                                            <td>{{ $ins->policy_number }}</td>
                                                                                                            <td>{{ $ins->contact }}</td>
                                                                                                            <td>{{ $ins->financial_advisor }}</td>
                                                                                                            <td class="text-end">{{ number_format($ins->oustanding_debt ,2) }}</td>
                                                                                                        </tr>
                                                                                                        @endforeach
                                                                                                    </tbody>
                                                                                                </table>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                @endif
                                                                                @if($share->other_expenses)
                                                                                    <div class="card plain accordion-item">
                                                                                        <div class="card-header" id="other-heading">
                                                                                            <button class="collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#other" aria-expanded="true" aria-controls="other">
                                                                                                Other Expenses
                                                                                            </button>
                                                                                        </div>
                                                                                        <div id="other" class="accordion-collapse collapse" aria-labelledby="other-heading" data-bs-parent="#accordion-property-wealth">
                                                                                            <div class="accordion-body">
                                                                                                <div class="col-md-12 overflow-auto">
                                                                                                    <table class="table">
                                                                                                    <thead>
                                                                                                        <tr>
                                                                                                            <th>Description</th>
                                                                                                            <th>Amount</th>
                                                                                                            <th>Rate</th>
                                                                                                        </tr>
                                                                                                    </thead>
                                                                                                    <tbody>
                                                                                                        @foreach($user->expenses AS $expense)
                                                                                                        <tr>
                                                                                                            @if($expense->type == 'Other')
                                                                                                                <td>{{ $expense->description }}</td>
                                                                                                                <td>{{ number_format($expense->amount, 2) }}</td>
                                                                                                                <td>{{ $expense->rate }}</td>
                                                                                                            @endif
                                                                                                        </tr>
                                                                                                        @endforeach
                                                                                                    </tbody>
                                                                                                </table>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                @endif
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            @endif
                                                            @if($share->social_media)
                                                                <div class="tab-pane fade" id="social_media" role="tabpanel">
                                                                    <div class="accordion accordion-wrapper" id="accordionSocialMedia">
                                                                        @php
                                                                        $count = 0;
                                                                        @endphp
                                                                        @foreach($user->groupedSocialMedia() AS $key=>$media)
                                                                        <div class="card plain accordion-item">
                                                                            <div class="card-header" id="heading_{{ strtolower(str_replace(' ','_',$key)) }}">
                                                                                <button class="accordion-button @if($count > 0) collapsed @endif" type="button" data-bs-toggle="collapse" data-bs-target="#collapse_{{ strtolower(str_replace(' ','_',$key)) }}" aria-expanded="@if($count == 0) true @else false @endif" aria-controls="collapse_{{ strtolower(str_replace(' ','_',$key)) }}">
                                                                                    {{ ucwords($key) }}
                                                                                </button>
                                                                            </div>
                                                                            <div id="collapse_{{ strtolower(str_replace(' ','_',$key)) }}" class="accordion-collapse collapse @if($count == 0) show @endif" aria-labelledby="heading_{{ strtolower(str_replace(' ','_',$key)) }}" data-bs-parent="#accordionSocialMedia">
                                                                                <div class="accordion-body">
                                                                                    @foreach($media AS $md)
                                                                                        <div class="mb-3">
                                                                                            <h5>
                                                                                                @if($md->network != "Other")
                                                                                                {{ $md->network }}
                                                                                                @else
                                                                                                {{ $md->specific_network }}
                                                                                                @endif
                                                                                            </h5>
                                                                                            <ul class="list-group custom-list">
                                                                                                <li class="list-group-item d-flex">
                                                                                                    Username
                                                                                                    <span class="ms-auto">
                                                                                                        {{ $md->user_name }}
                                                                                                    </span>
                                                                                                </li>
                                                                                                @if($md->legacy_contact)
                                                                                                <li class="list-group-item d-flex">
                                                                                                    Legacy Contact
                                                                                                    <span class="ms-auto">
                                                                                                        {{ $md->legacy_contact }}
                                                                                                    </span>
                                                                                                </li>
                                                                                                @endif
                                                                                                @if($md->link)
                                                                                                <li class="list-group-item">
                                                                                                    Link<br />
                                                                                                    <b>
                                                                                                        <a href="{{ url($md->link) }}" target="_blank">{{ $md->link }}</a>
                                                                                                    </b>
                                                                                                </li>
                                                                                                @endif
                                                                                                <li class="list-group-item">
                                                                                                    Instructions<br />
                                                                                                    <b>
                                                                                                        {{ $md->instructions }}
                                                                                                    </b>
                                                                                                </li>
                                                                                            </ul>
                                                                                        </div>
                                                                                    @endforeach
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        @php
                                                                        $count++;
                                                                        @endphp
                                                                        @endforeach
                                                                    </div>
                                                                </div>
                                                            @endif
                                                            @if($share->safe_keeping || $share->where_to_find || $share->income_tax)
                                                                <div class="tab-pane fade" id="safe_keeping" role="tabpanel">
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <div class="accordion accordion-wrapper" id="accordion-property-wealth">
                                                                                <div class="card plain accordion-item">
                                                                                    <div class="card-header" id="safe-heading">
                                                                                        <button class="collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#safe" aria-expanded="true" aria-controls="safe">
                                                                                            Safekeeping
                                                                                        </button>
                                                                                    </div>
                                                                                    <div id="safe" class="accordion-collapse collapse" aria-labelledby="safe-heading" data-bs-parent="#accordion-property-wealth">
                                                                                        <div class="accordion-body">
                                                                                            <div class="col-md-12 overflow-auto">
                                                                                                <table class="table">
                                                                                                <thead>
                                                                                                    <tr>
                                                                                                        <th>Title</th>
                                                                                                        <th>Description</th>
                                                                                                        <th>File</th>
                                                                                                    </tr>
                                                                                                </thead>
                                                                                                <tbody>
                                                                                                    @foreach($user->safe_keep AS $keep)
                                                                                                    <tr>
                                                                                                        <td>{{ $keep->title }}</td>
                                                                                                        <td>{{ $keep->description }}</td>
                                                                                                        <td>
                                                                                                            @if($keep->file)
                                                                                                            <a href="{{ url('storage/'.$keep->file) }}">View File</a>
                                                                                                            @endif
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    @endforeach
                                                                                                </tbody>
                                                                                            </table>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                @if($share->where_to_find)
                                                                                    <div class="card plain accordion-item">
                                                                                        <div class="card-header" id="find-heading">
                                                                                            <button class="collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#find" aria-expanded="false" aria-controls="find">
                                                                                                Where To Find
                                                                                            </button>
                                                                                        </div>
                                                                                        <div id="find" class="accordion-collapse collapse" aria-labelledby="find-heading" data-bs-parent="#accordion-property-wealth">
                                                                                            <div class="accordion-body">
                                                                                                <div class="col-md-12 overflow-auto">
                                                                                                    <table class="table">
                                                                                                    <thead>
                                                                                                        <tr>
                                                                                                            <th>Item Name</th>
                                                                                                            <th>Location</th>  
                                                                                                        </tr>
                                                                                                    </thead>
                                                                                                    <tbody>
                                                                                                        @foreach($user->itemToFind AS $item)
                                                                                                        <tr>
                                                                                                            <td>{{ $item->item_name }}</td>
                                                                                                            <td>{{ $item->location}}</td>
                                                                                                        </tr>
                                                                                                        @endforeach
                                                                                                    </tbody>
                                                                                                </table>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                @endif
                                                                                @if($share->income_tax)
                                                                                    @if($user->tax)
                                                                                        <div class="card plain accordion-item">
                                                                                            <div class="card-header" id="headingSimpleFour">
                                                                                                <button class="collapsed" data-bs-toggle="collapse" data-bs-target="#collapseSimpleFour" aria-expanded="false" aria-controls="collapseSimpleFour"> Income Tax </button>
                                                                                            </div>
                                                                                            <div id="collapseSimpleFour" class="accordion-collapse collapse" aria-labelledby="headingSimpleFour" data-bs-parent="#accordionSimpleExample">
                                                                                                <div class="card-body">
                                                                                                    <p>
                                                                                                        <b>Tax Number</b><br />
                                                                                                        {{ $user->tax->tax_number }}
                                                                                                    </p>
                                                                                                    @if($user->tax->tax_assessment)
                                                                                                    <p>
                                                                                                        <b>Latest Assessment</b><br />
                                                                                                        <a href="{{ url('storage/'.$user->tax->tax_assessment	) }}" target="_blank">View Document</a>
                                                                                                    </p>
                                                                                                    @endif
                                                                                                    @if($user->tax->tax_certificate)
                                                                                                    <p>
                                                                                                        <b>Tax Certificate</b><br />
                                                                                                        <a href="{{ url('storage/'.$user->tax->tax_certificate) }}" target="_blank">View Document</a>
                                                                                                    </p>
                                                                                                    @endif
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    @endif
                                                                                @endif
                                                                            </div>
                                                                        </div>    
                                                                    </div>
                                                                </div>
                                                            @endif
                                                            @if($share->funeral)
                                                                <div class="tab-pane fade" id="funeral" role="tabpanel">
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                        @if($user->funeralCovers)
                                                                            <h2>Funeral Cover</h2>
                                                                            <div class="col-md-12 overflow-auto">
                                                                                <table class="table">
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th>Company</th>
                                                                                        <th>Policy Number</th>
                                                                                        <th>Policy Holder</th>
                                                                                        <th>Coverage</th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                    @foreach($user->funeralCovers as $cover)
                                                                                    <tr>
                                                                                        <td>{{ $cover->company }}</td>
                                                                                        <td>{{ $cover->policy_number }}</td>
                                                                                        <td>{{ $cover->policy_holder }}</td>
                                                                                        <td>{{ $cover->coverage_amount }}</td>
                                                                                    </tr>
                                                                                    @endforeach
                                                                                </tbody>
                                                                            </table>
                                                                            </div>
                                                                        @endif
                                                                        @if($user->funeral)
                                                                            <ul class="list-group custom-list">
                                                                                <li class="list-group-item">
                                                                                    <b>Funeral Policy Details</b><br />
                                                                                    <span>{{ $user->funeral->policy_details }}</span>
                                                                                </li>
                                                                                <li class="list-group-item">
                                                                                    <b>Memorial Service Details</b><br />
                                                                                    <span>{{ $user->funeral->memorials_service }}</span>
                                                                                </li>
                                                                                <li class="list-group-item">
                                                                                    <b>Cremation Details</b><br />
                                                                                    <span>{{ $user->funeral->cremation_details }}</span>
                                                                                </li>
                                                                                <li class="list-group-item">
                                                                                    <b>Funeral Parlor Details</b><br />
                                                                                    <span>{{ $user->funeral->parlor_details }}</span>
                                                                                </li>
                                                                                <li class="list-group-item">
                                                                                    <b>Specofic Requests</b><br />
                                                                                    <span>{{ $user->funeral->requests }}</span>
                                                                                </li>
                                                                                <li class="list-group-item">
                                                                                    <b>Own Obituary</b><br />
                                                                                    <span>{{ $user->funeral->obituary }}</span>
                                                                                </li>
                                                                            </ul>
                                                                        @endif
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            @endif
                                                            @if($share->memories)
                                                                <div class="tab-pane fade" id="memories" role="tabpanel">
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            @if($user->memories)
                                                                                <h2>Memories</h2>
                                                                                <div class="col-md-12 overflow-auto">
                                                                                    <table class="table">
                                                                                    <thead>
                                                                                        <tr>
                                                                                            <th>Recipient</th>
                                                                                            <th>Notes</th>
                                                                                            <th>Media</th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    <tbody>
                                                                                        @foreach($user->memories as $memory)
                                                                                        <tr>
                                                                                            <td>{{ $memory->recipient }}</td>
                                                                                            <td>{{ $memory->notes }}</td>
                                                                                            @if($memory->media_type == 'Link')
                                                                                                <td><a href="{{ url($memory->link) }}" target="_blank">{{ $memory->link }}</a></td>
                                                                                            @else 
                                                                                                <td>
                                                                                                    @if($memory->media)
                                                                                                        <a href="{{ url('storage/'.$memory->media) }}" target="_blank">View</a>
                                                                                                    @endif
                                                                                                </td>
                                                                                            @endif
                                                                                        </tr>
                                                                                        @endforeach
                                                                                    </tbody>
                                                                                </table>
                                                                                </div>
                                                                            @endif
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            @if($user->questionniare)
                                                                                <ul class="list-group custom-list">
                                                                                    <li class="list-group-item">
                                                                                        <b>Favorite Song</b><br />
                                                                                        <span>{{ $user->questionniare->favorite_song }}</span>
                                                                                    </li>
                                                                                    <li class="list-group-item">
                                                                                        <b>Favorite Movie</b><br />
                                                                                        <span>{{ $user->questionniare->favorite_movie }}</span>
                                                                                    </li>
                                                                                    <li class="list-group-item">
                                                                                        <b>Favorite Food</b><br />
                                                                                        <span>{{ $user->questionniare->favorite_food }}</span>
                                                                                    </li>
                                                                                    <li class="list-group-item">
                                                                                        <b>Favorite Place visited and why</b><br />
                                                                                        <span>{{ $user->questionniare->favorite_place }}</span>
                                                                                    </li>
                                                                                    <li class="list-group-item">
                                                                                        <b>Special memory that comes to mind</b><br />
                                                                                        <span>{{ $user->questionniare->special_memory }}</span>
                                                                                    </li>
                                                                                    <li class="list-group-item">
                                                                                        <b>Your biggest lesson in life</b><br />
                                                                                        <span>{{ $user->questionniare->biggest_lesson }}</span>
                                                                                    </li>
                                                                                    <li class="list-group-item">
                                                                                        <b>A regret</b><br />
                                                                                        <span>{{ $user->questionniare->regret }}</span>
                                                                                    </li>
                                                                                    <li class="list-group-item">
                                                                                        <b>You will miss the most</b><br />
                                                                                        <span>{{ $user->questionniare->miss_most }}</span>
                                                                                    </li>
                                                                                    <li class="list-group-item">
                                                                                        <b>Best part of life</b><br />
                                                                                        <span>{{ $user->questionniare->life_best_part }}</span>
                                                                                    </li>
                                                                                    <li class="list-group-item">
                                                                                        <b>Worst part of life</b><br />
                                                                                        <span>{{ $user->questionniare->life_worst_part }}</span>
                                                                                    </li>
                                                                                </ul>
                                                                            @endif
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            @if($user->questionniare)
                                                                                <ul class="list-group custom-list">
                                                                                    <li class="list-group-item">
                                                                                        <b>Religion / Beliefs</b><br />
                                                                                        <span>{{ $user->questionniare->religion_beliefs }}</span>
                                                                                    </li>
                                                                                    <li class="list-group-item">
                                                                                        <b>Funeral wishes - anything specific</b><br />
                                                                                        <span>{{ $user->questionniare->funeral_wish}}</span>
                                                                                    </li>
                                                                                    <li class="list-group-item">
                                                                                        <b>What you din't know</b><br />
                                                                                        <span>{{ $user->questionniare->dint_know }}</span>
                                                                                    </li>
                                                                                    <li class="list-group-item">
                                                                                        <b>What you hope for someone</b><br />
                                                                                        <span>{{ $user->questionniare->hopes_for_someone }}</span>
                                                                                    </li>
                                                                                    <li class="list-group-item">
                                                                                        <b>This made you very happy</b><br />
                                                                                        <span>{{ $user->questionniare->happy_thing }}</span>
                                                                                    </li>
                                                                                    <li class="list-group-item">
                                                                                        <b>This made you very sad</b><br />
                                                                                        <span>{{ $user->questionniare->sad_thing }}</span>
                                                                                    </li>
                                                                                    <li class="list-group-item">
                                                                                        <b>You best quality</b><br />
                                                                                        <span>{{ $user->questionniare->best_quality }}</span>
                                                                                    </li>
                                                                                    <li class="list-group-item">
                                                                                        <b>What must people remember about you?</b><br />
                                                                                        <span>{{ $user->questionniare->remember_me_by }}</span>
                                                                                    </li>
                                                                                    <li class="list-group-item">
                                                                                        <b>A funny memory</b><br />
                                                                                        <span>{{ $user->questionniare->funny_memory }}</span>
                                                                                    </li>
                                                                                    <li class="list-group-item">
                                                                                        <b>Bucket list</b><br />
                                                                                        <span>{{ $user->questionniare->bucket_list }}</span>
                                                                                    </li>
                                                                                </ul>
                                                                            @endif
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @else
                                <div class="row">
                                    <div class="col-md-12 text-center mt-5 mb-5">
                                        <h1>YOU DO NOT HAVE PERMISSION TO VIEW THIS PROFILE.</h1>
                                    </div>
                                </div>    
                                @endif
                            </div>
                        </div>
                        @else
                        <div class="row">
                            <div class="col-md-6 offset-md-3">
                                <div class="card">
                                    <div class="card-body">
                                        <h5>Verify</h5>
                                        <p>Enter your ID number below to view this profile</p>
                                        @if($errors->any())
                                            <div class="alert alert-danger">
                                                <span>{{ $errors->first() }}</span>
                                            </div>
                                        @endif
                                        <div class="mb-3">
                                            <label class="form-label">ID Number</label>
                                            <input type="text" class="form-control" name="id_number" wire:model.defer="id_number">
                                        </div>
                                        <div class="d-grid">
                                            <a href="#" class="btn btn-primary" style="background-color:#eb0045; border:3px solid #eb0045;" wire:click.prevent="verifyUser">Verify</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>    
        </div>
    </section>
    @push('scripts')
    <script>
        $(document).ready(function(){
            var pic = $('.profile_img_cont').data('image');
            $('.profile_img_cont').css('background-image', 'url(' + pic + ')');
        });

        var active = '{{ $is_logged_in }}';
        $(document).ready(function(){
            if(active == 1){
                loadTree();
            }
        });
        document.addEventListener("DOMContentLoaded", () => {
            Livewire.hook('element.updated', (el, component) => {
                if(active == 1){
                    loadTree();
                }
            });
        });
        
        function loadTree(){
            FamilyTree.templates.hugo = Object.assign({}, FamilyTree.templates.base);
            FamilyTree.templates.hugo.defs = 
            `<clipPath id="hugo_img_0">
                <circle id="hugo_img_0_stroke" stroke-width="2" stroke="#fff" x="90" y="-5" rx="25" ry="50%" width="70" height="70"></circle>
            </clipPath>
            <linearGradient id="hugo_grad_female" x1="0%" y1="0%" x2="100%" y2="0%">
                <stop offset="0%" style="stop-color:#FF8024;stop-opacity:1" />
                <stop offset="100%" style="stop-color:#FF46A3;stop-opacity:1" />
            </linearGradient>
            <linearGradient id="hugo_grad_male" x1="0%" y1="0%" x2="100%" y2="0%">
                <stop offset="0%" style="stop-color:#00D3A5;stop-opacity:1" />
                <stop offset="100%" style="stop-color:#00A7D4;stop-opacity:1" />
            </linearGradient>
            <linearGradient id="hugo_grad" x1="0%" y1="0%" x2="100%" y2="0%">
                <stop offset="0%" style="stop-color:#ffd292;stop-opacity:1" />
                <stop offset="100%" style="stop-color:#ffd292;stop-opacity:1" />
            </linearGradient>
            <g id="hugo_up">
                <circle cx="12" cy="12" r="15" fill="transparent"></circle>
                ${FamilyTree.icon.ft(24,24,'#fff', 0, 0)}
            </g>
            <g id="hugo_node_menu" style="cursor:pointer;">
                <rect x="0" y="0" fill="transparent" width="22" height="22"></rect>
                <circle cx="11" cy="4" r="2" fill="#ffffff"></circle><circle cx="11" cy="11" r="2" fill="#ffffff"></circle>
                <circle cx="11" cy="18" r="2" fill="#ffffff"></circle>
            </g>
            <style>
                .{randId} .bft-edit-form-header{
                    background: linear-gradient(90deg, #D0D0D0 0%, #909090 100%);
                }
                .{randId}.male .bft-edit-form-header{
                    background: linear-gradient(90deg, #00D3A5 0%, #00A7D4 100%);
                }
                .{randId}.female .bft-edit-form-header{
                    background: linear-gradient(90deg, #FF8024 0%, #FF46A3 100%);
                }  
                .{randId} .bft-img-button{
                    background-color: #909090;
                }      
                .{randId} .bft-img-button:hover{
                    background-color: #D0D0D0;
                }
                .{randId}.male .bft-img-button{
                    background-color: #00A7D4;
                }      
                .{randId}.male .bft-img-button:hover{
                    background-color: #00D3A5;
                }
                .{randId}.female .bft-img-button{
                    background-color: #FF46A3;
                }      
                .{randId}.female .bft-img-button:hover{
                    background-color: #FF8024;
                }
            </style>`;
            FamilyTree.templates.hugo.img_0 = '<clipPath id="ulaImg">'
            + '<circle cx="40" cy="60" r="30" fill="white"></circle>'
            + '</clipPath>'
            + '<image preserveAspectRatio="xMidYMid slice" clip-path="url(#ulaImg)" xlink:href="{val}" x="10" y="30" width="60" height="60">'
            + '</image>';
            FamilyTree.templates.hugo.field_0 = '<text ' + FamilyTree.attr.width + ' ="230" style="font-size: 18px;font-weight:bold;"  fill="#1c355e" x="80" y="43" text-anchor="left">{val}</text>';
            FamilyTree.templates.hugo.field_1 = '<text ' + FamilyTree.attr.width + ' ="230" style="font-size: 18px;font-weight:bold;"  fill="#1c355e" x="80" y="63" text-anchor="left">{val}</text>';
            FamilyTree.templates.hugo.field_2 = '<text ' + FamilyTree.attr.width + ' ="230" style="font-size: 16px;font-weight:bold;" fill="#1c355e" x="80" y="83" text-anchor="left">{val}</text>';
            FamilyTree.templates.hugo.nodeMenuButton = `<use x="225" y="10" ${FamilyTree.attr.control_node_menu_id}="{id}" xlink:href="#hugo_node_menu" />`; 
            
            FamilyTree.templates.hugo.node = '<rect x="0" y="0" height="{h}" width="{w}" stroke-width="3" fill="#b1cfff" stroke="#b1cfff" rx="40" ry="40"></rect>';
            FamilyTree.templates.hugo.defs = '<filter id="shadow" x="-20%" y="-20%" width="150%" height="150%"><feOffset result="offOut" in="SourceAlpha" dx="2" dy="2" /><feGaussianBlur result="blurOut" in="offOut" stdDeviation="2" /><feBlend in="SourceGraphic" in2="blurOut" mode="normal" /></filter>';

            FamilyTree.templates.hugo_blue = Object.assign({}, FamilyTree.templates.hugo);
            FamilyTree.templates.hugo_blue.node = '<rect x="0" y="0" height="{h}" width="{w}" stroke-width="3" fill="#add8e6" stroke="#add8e6" rx="40" ry="40" filter="url(#shadow)"></rect>';
            FamilyTree.templates.hugo_blue.defs = '<filter id="shadow" x="-20%" y="-20%" width="150%" height="150%"><feOffset result="offOut" in="SourceAlpha" dx="2" dy="2" /><feGaussianBlur result="blurOut" in="offOut" stdDeviation="2" /><feBlend in="SourceGraphic" in2="blurOut" mode="normal" /></filter>';

            FamilyTree.templates.hugo_pink = Object.assign({}, FamilyTree.templates.hugo);
            FamilyTree.templates.hugo_pink.node = '<rect x="0" y="0" height="{h}" width="{w}" stroke-width="3" fill="#ffb1c9" stroke="#ffb1c9" rx="40" ry="40" filter="url(#shadow)"></rect>';
            FamilyTree.templates.hugo_pink.defs = '<filter id="shadow" x="-20%" y="-20%" width="150%" height="150%"><feOffset result="offOut" in="SourceAlpha" dx="2" dy="2" /><feGaussianBlur result="blurOut" in="offOut" stdDeviation="2" /><feBlend in="SourceGraphic" in2="blurOut" mode="normal" /></filter>';

            FamilyTree.templates.hugo_orange = Object.assign({}, FamilyTree.templates.hugo);
            FamilyTree.templates.hugo_orange.node = '<rect x="0" y="0" height="{h}" width="{w}" stroke-width="3" fill="#ffd38e" stroke="#ffd38e" rx="40" ry="40" filter="url(#shadow)"></rect>';
            FamilyTree.templates.hugo_orange.defs = '<filter id="shadow" x="-20%" y="-20%" width="150%" height="150%"><feOffset result="offOut" in="SourceAlpha" dx="2" dy="2" /><feGaussianBlur result="blurOut" in="offOut" stdDeviation="2" /><feBlend in="SourceGraphic" in2="blurOut" mode="normal" /></filter>';

            FamilyTree.templates.hugo_green = Object.assign({}, FamilyTree.templates.hugo);
            FamilyTree.templates.hugo_green.node = '<rect x="0" y="0" height="{h}" width="{w}" stroke-width="3" fill="#c7e599" stroke="#c7e599" rx="40" ry="40" filter="url(#shadow)"></rect>';
            FamilyTree.templates.hugo_green.defs = '<filter id="shadow" x="-20%" y="-20%" width="150%" height="150%"><feOffset result="offOut" in="SourceAlpha" dx="2" dy="2" /><feGaussianBlur result="blurOut" in="offOut" stdDeviation="2" /><feBlend in="SourceGraphic" in2="blurOut" mode="normal" /></filter>';

            FamilyTree.templates.hugo_yellow = Object.assign({}, FamilyTree.templates.hugo);
            FamilyTree.templates.hugo_yellow.node = '<rect x="0" y="0" height="{h}" width="{w}" stroke-width="3" fill="#fce571" stroke="#fce571" rx="40" ry="40" filter="url(#shadow)"></rect>';
            FamilyTree.templates.hugo_yellow.defs = '<filter id="shadow" x="-20%" y="-20%" width="150%" height="150%"><feOffset result="offOut" in="SourceAlpha" dx="2" dy="2" /><feGaussianBlur result="blurOut" in="offOut" stdDeviation="2" /><feBlend in="SourceGraphic" in2="blurOut" mode="normal" /></filter>';

            var tree_json = $.parseJSON('{!! $tree !!}');
            //console.log(tree_json);

            var family = new FamilyTree(document.getElementById("tree"), {
                template: "hugo",
                scaleInitial: FamilyTree.match.boundary,
                orientation: FamilyTree.orientation.left,
                showXScroll: false,
                showYScroll: true,
                enableSearch: false,
                interactive: true,
                partnerNodeSeparation: 100,
                mouseScrool: FamilyTree.action.ctrlZoom,
                nodeBinding: {
                    field_0: "name",
                    field_1: "surname",
                    field_2: "relation",
                    img_0: "img",
                },
                editForm: {
                    buttons: {
                        pdf: null,
                        share: null,
                        delete: {
                            icon: FamilyTree.icon.remove(24,24,'#fff'),
                            text: "Remove",
                            hideIfDetailsMode: false,
                        }
                    },
                    /*
                    generateElementsFromFields: false,
                    elements: [
                        { type: 'textbox', label: 'First Name', binding: 'Name'},
                        { type: 'textbox', label: 'Surname Name', binding: 'Name'},
                        { type: 'textbox', label: 'Photo Url', binding: 'ImgUrl', btn: 'Upload'}        
                    ]
                    */
                },    
                tags: {
                    Orange: {
                        template: "hugo_orange"
                    },
                    Green: {
                        template: "hugo_green"
                    },
                    Yellow: {
                        template: "hugo_yellow"
                    },
                    Blue: {
                        template: "hugo_blue"
                    },
                    Pink: {
                        template: "hugo_pink"
                    }
                },
                nodes: tree_json
                // Example of a node with color
                //nodes: [{ id: 4, pids: [1],  name: "Gogo Alfred", relation: "Grand Mother", gender: "female", img: "{{ asset('../img/family-tree/My Network ICON.png') }}", tags: ["hugo_pink"] }]
                
            });
            
            
            family.on('click', function(sender, args){
                var id = args.node.id;
                Livewire.emit('showForm', id);
                return false;
            });

            family.editUI.on('button-click', function (sender, args) {
                var data = family.get(args.nodeId);
                if (args.name == 'delete') {
                    Livewire.emit('delete-node', data.id);
                }
                if(args.name == "edit"){
                    $('.bft-edit-form').hide();
                    Livewire.emit('show-edit-node', data.id);
                    return false;
                }
            });
        }
    </script> 
    @endpush
</div>