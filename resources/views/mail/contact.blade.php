<!doctype html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml"
  xmlns:v="urn:schemas-microsoft-com:vml"
  xmlns:o="urn:schemas-microsoft-com:office:office">
  <head>
    <title>New Contact on MyAll</title><!--[if !mso]><!-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge"><!--<![endif]-->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <style type="text/css">
      #outlook a {
        padding: 0;
      }

      body {
        margin: 0;
        padding: 0;
        -webkit-text-size-adjust: 100%;
        -ms-text-size-adjust: 100%;
      }

      table,
      td {
        border-collapse: collapse;
        mso-table-lspace: 0pt;
        mso-table-rspace: 0pt;
      }

      img {
        border: 0;
        height: auto;
        line-height: 100%;
        outline: none;
        text-decoration: none;
        -ms-interpolation-mode: bicubic;
      }

      p {
        display: block;
        margin: 13px 0;
      }
    </style><!--[if mso]>
        <noscript>
        <xml>
        <o:OfficeDocumentSettings>
          <o:AllowPNG/>
          <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
        </xml>
        </noscript>
        <![endif]--><!--[if lte mso 11]>
        <style type="text/css">
          .mj-outlook-group-fix { width:100% !important; }
        </style>
        <![endif]--><!--[if !mso]><!-->
    <link
      href="https://fonts.googleapis.com/css2?family=Inter:wght@100..900&display=swap"
      rel="stylesheet" type="text/css">
    <style type="text/css">
      @import url(https://fonts.googleapis.com/css2?family=Inter:wght@100..900&display=swap);
    </style><!--<![endif]-->
    <style type="text/css">
      @media only screen and (min-width:480px) {
        .mj-column-per-100 {
          width: 100% !important;
          max-width: 100%;
        }
      }
    </style>
    <style media="screen and (min-width:480px)">
      .moz-text-html .mj-column-per-100 {
        width: 100% !important;
        max-width: 100%;
      }
    </style>
    <style type="text/css">
      @media only screen and (max-width:480px) {
        table.mj-full-width-mobile {
          width: 100% !important;
        }

        td.mj-full-width-mobile {
          width: auto !important;
        }
      }
    </style>
  </head>
  <body style="word-spacing:normal;background-color:#FFFFFF;">
    <div style="background-color:#FFFFFF;">
      <!--[if mso | IE]><table align="center" border="0" cellpadding="0" cellspacing="0" class="" role="presentation" style="width:530px;" width="530" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
      <div style="margin:0px auto;max-width:530px;">
        <table align="center" border="0" cellpadding="0" cellspacing="0"
          role="presentation" style="width:100%;">
          <tbody>
            <tr>
              <td
                style="direction:ltr;font-size:0px;padding:80px 0px 0px 0px;text-align:center;">
                <!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:530px;" ><![endif]-->
                <div class="mj-column-per-100 mj-outlook-group-fix"
                  style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                  <table border="0" cellpadding="0" cellspacing="0"
                    role="presentation" style="vertical-align:top;"
                    width="100%">
                    <tbody>
                      <tr>
                        <td align="center"
                          style="font-size:0px;padding:0px;word-break:break-word;">
                          <table border="0" cellpadding="0" cellspacing="0"
                            role="presentation"
                            style="border-collapse:collapse;border-spacing:0px;">
                            <tbody>
                              <tr>
                                <td style="width:150px;"><img alt="MyAll"
                                    height="auto"
                                    src="{{ asset('img/logotype-alt.png') }}"
                                    style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:13px;"
                                    width="150"></td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div><!--[if mso | IE]></td></tr></table><![endif]-->
              </td>
            </tr>
          </tbody>
        </table>
      </div>
      <!--[if mso | IE]></td></tr></table><table align="center" border="0" cellpadding="0" cellspacing="0" class="" role="presentation" style="width:530px;" width="530" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
      <div style="margin:0px auto;max-width:530px;">
        <table align="center" border="0" cellpadding="0" cellspacing="0"
          role="presentation" style="width:100%;">
          <tbody>
            <tr>
              <td
                style="direction:ltr;font-size:0px;padding:20px 0;text-align:center;">
                <!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:530px;" ><![endif]-->
                <div class="mj-column-per-100 mj-outlook-group-fix"
                  style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                  <table border="0" cellpadding="0" cellspacing="0"
                    role="presentation" style="vertical-align:top;"
                    width="100%">
                    <tbody>
                      <tr>
                        <td align="left"
                          style="font-size:0px;padding:10px 25px;word-break:break-word;">
                          <div
                            style="font-family:Inter,Helvetica,Arial,sans-serif;font-size:13px;line-height:1.6;text-align:left;color:#000000;">
                            <p>Hi there,</p>
                            <p>You have a new contact on MyAll website.</p>
                            <p><b>Contact Details:</b>
                            <table>
                              <tr>
                                <td><b>Name:</b></td>
                                <td>{{ $name }}</td>
                              </tr>
                              <tr>
                                <td><b>Contact Number:</b></td>
                                <td>{{ $contact_number }}</td>
                              </tr>
                              <tr>
                                <td><b>Email:</b></td>
                                <td>{{ $email }}</td>
                              </tr>
                              <tr>
                                <td colspan="2">
                                  <b>Message:</b><br>{{ $message_content }}
                                </td>
                              </tr>
                            </table>
                            </p>
                            <p>Regards,</p>
                            <p>MyAll Team</p>
                          </div>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div><!--[if mso | IE]></td></tr></table><![endif]-->
              </td>
            </tr>
          </tbody>
        </table>
      </div>
      <!--[if mso | IE]></td></tr></table><table align="center" border="0" cellpadding="0" cellspacing="0" class="" role="presentation" style="width:530px;" width="530" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
      <div style="margin:0px auto;max-width:530px;">
        <table align="center" border="0" cellpadding="0" cellspacing="0"
          role="presentation" style="width:100%;">
          <tbody>
            <tr>
              <td
                style="direction:ltr;font-size:0px;padding:0px 0px 0px 0px;text-align:center;">
                <!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:530px;" ><![endif]-->
                <div class="mj-column-per-100 mj-outlook-group-fix"
                  style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                  <table border="0" cellpadding="0" cellspacing="0"
                    role="presentation" style="vertical-align:top;"
                    width="100%">
                    <tbody>
                      <tr>
                        <td align="left"
                          style="font-size:0px;padding:10px 25px;word-break:break-word;">
                          <div
                            style="font-family:Inter,Helvetica,Arial,sans-serif;font-size:10px;line-height:1;text-align:left;color:#000000;">
                            © 2024 MYALL SA (Pty) Ltd (2024/494838/07). All
                            Rights Reserved</div>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div><!--[if mso | IE]></td></tr></table><![endif]-->
              </td>
            </tr>
          </tbody>
        </table>
      </div><!--[if mso | IE]></td></tr></table><![endif]-->
    </div>
  </body>
</html>
