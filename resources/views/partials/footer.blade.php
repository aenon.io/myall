<div class="footer-area">
    <div class="container-fluid">
        <div class="footer-content">
            <p>&copy; Designed & Developed by <a href="https://thinktank.co.za/" target="_blank">ThinkTank Creatives</a></p>
        </div>
    </div>
</div>