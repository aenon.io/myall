<!doctype html>
<html>
    <head>
        @php
        //$data = file_get_contents(url('/img/dgt.png'));
        //$base64 = 'data:image/' . 'png' . ';base64,' . base64_encode($data);
        $base64 = null;
        @endphp
        <style>
            body {
                background-color: transparent;
                font-family: sans-serif;
                -webkit-font-smoothing: antialiased;
                font-size: 18px;
                line-height: 1.8;
                margin: 0;
                padding: 0;
                -ms-text-size-adjust: 100%;
                -webkit-text-size-adjust: 100%;
            }
            
            @page {
                margin: 50px 50px 310px 50px;
            }
            @page:first{
                margin: 50px 50px 50px 50px;
            }
            footer, .footer{
                position: fixed; 
                bottom: -300px; 
                left: 0px; 
                right: 0px;
                height: 320px;
                overflow: hidden;
                background-color: #FFFFFF;
            }
            /*footer:last-of-type{*/
            /*    display: none;*/
            /*}*/
            .signature {
                border: 0;
                border-bottom: 1px solid #000;
                width: 80%;
            }
            ol{
                /*padding: 20px;*/
            }
            .sig_list li, .testator{
                margin-top: 50px;
            }
            ul.sig_list{
                list-style-type: none;
            }
            ul.sig_list li{
                margin-bottom: 0px;
            }
            .container{
                width: 100%;
            }
            .row{
                display: flex;
                flex-wrap: wrap;
            }
            .col-md-12{
                width: 100%;
            }
            .col-md-6{
                flex: 0 0 auto;
                width: 50%;
            }
            .w-100{
                width: 100%;
            }
            .d-block{
                display: block;
            }
            .text-center{
                text-align: center;
            }
            .text-justify{
                text-align: justify;
                text-justify: none;
            }
            .wrapper-page {
                page-break-after: always;
            }
            .wrapper-page:last-child {
                page-break-after: avoid;
            }
            .content{
                background-color: #FFFFFF;
            }
            .verticalLine {
                background-image:url(<?php echo $base64 ?>);
                background-repeat:repeat;
                background-position: 0px 5px;
                /*
                background-image: linear-gradient(#000, #000);
                background-size: 2px 500%;
                background-repeat: no-repeat;
                background-position: center center;
                */
            }
        </style>
        
    </head>
    <body class="verticalLine">
        <div class="footer">
            <div class="container">
                <table class="w-100">
                    <tr>
                        <td>
                            <div class="">
                                <u>AS WITNESS:</u>
                            </div>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>
                            <ol class="sig_list">
                                <li><input type="text" class="signature" /></li>
                                <li><input type="text" class="signature" /></li>
                            </ol>
                        </td>
                        <td>
                            <ul class="sig_list">
                                <li>
                                    <input type="text" class="signature" /><br />
                                    <b>TESTATOR</b>
                                </li>
                            </ul>
                        </td>
                    </td>
                </table>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <img src="{{ asset('img/will_logo.png') }}" class="w-100">
                </div>
            </div>
        </div>
        
        <div class="content">
            <div class="text-center">
                <h2>LAST WILL AND TESTAMENT</h2>
            </div>
            <div class="text-center">
                I, the undersigned <b>{{ $user->name.' '.$user->middle_name.' '.$user->surname }}</b><br />
                Identity Number <b>{{ $user->id_number }}</b><br />
                presently living at <b>{{ $user->street_address.', '.$user->city.', '.$user->postal_code.' '.$user->province }}</b><br />
                @foreach($user->will_spouses AS $sp)
                    and married
                    @if($sp->marital_in_community_of_property == "yes") 
                        in community of property 
                    @else
                        out of community of property
                    @endif with <b>{{ $sp->first_name.' '.$sp->middle_name.' '.$sp->surname }}</b><br />
                @endforeach
                <p>hereby revoke, cancel, and annul all and any previous testamentary acts and/or dispositions made by me and desire that the same shall be null, void and of no force and effects whatsoever regarding all of my assets and property; and</p>
                <p>I hereby make and execute this as my last will and testament regarding all of my assets and property as follows:</p>
            </div>
            @if($user->executor())
            <div class="text-justify">
                <div class="text-center">
                    <h4>NOMINATION OF EXECUTOR</h4>
                </div>
                <p style="font-size: 15px"><i>The executor is the person who will administer the estate</i></p>
                <p>I hereby nominate, @if($user->executor()->relation) my <b>@if(Auth::user()->executor()->relation == "Other") {{ strtolower(Auth::user()->executor()->specify_relation) }} @else {{ strtolower(Auth::user()->executor()->relation) }}@endif</b> @endif <b>{{ $user->executor()->first_name.' '.$user->executor()->middel_name.' '.$user->executor()->surname }}</b> identity number <b>{{ $user->executor()->id_number }}</b> as executor of my estate and give him/her all the powers and authority they need or are allowed in law. I direct that my said executor shall not be required to give security to the Master of the High Court for the due fulfilment of his or her duties.</p>
                <p>Should the person nominated as executor die before me or at the same time as me, then I nominate my <b>@if(Auth::user()->sub_executor()->relation == "Other") {{ strtolower(Auth::user()->sub_executor()->specify_relation) }} @else {{ strtolower(Auth::user()->sub_executor()->relation) }} @endif {{ $user->sub_executor()->first_name.' '.$user->sub_executor()->middle_name.' '.$user->sub_executor()->surname }}</b> identity number <b>{{ $user->sub_executor()->id_number }}</b> as executor of my estate and give him/her all the powers and authority they need or are allowed in law.  I direct that my said executor shall not be required to give security to the Master of the High Court for the due fulfilment of any of his or her duties.</p>
                <p>The person nominated as executor may resign or relinquish this position if they so wish. The nominated executor is required to name a person of their choice to replace them as the executor of this will. I direct that my said executor shall not be required to give security to the Master of the High Court for the due fulfilment of any of his or her duties.</p>
                <p>In the event of a joint appointment, the remaining executor will be entitled to assume another person of their choice to succeed the person who cannot act. I direct that my said executor shall not be required to give security to the Master of the High Court for the due fulfilment of any of his/her duties.</p>
            </div>
            @endif
            @if($user->guardians->count() > 0)
            <div class="text-justify">
                <div class="text-center">
                    <h4>NOMINATION OF GUARDIAN</h4>
                </div>
                <p style="font-size: 15px"><i>The guardian is the person who will look after any minor child (a child under 18 years)</i><br />&nbsp;</p>
                @foreach($user->children AS $child)
                    @if($child->guardian)
                    <p>To the extent that I may be legally entitled to do so, and after the death of my child's co-guardian, I nominate <b>{{ $child->guardian->guardian->first_name.' '.$child->guardian->guardian->middle_name.' '.$child->guardian->guardian->surname }}</b>, identity number <b>{{ $child->guardian->guardian->id_number }}</b> to be the guardian of my minor child <b>{{ $child->child_name.' '.$child->middle_name.' '.$child->child_surname }}</b>@if($child->child_date_of_birth), born <b>{{ $child->child_date_of_birth }}</b>@endif. The nominated guardian shall not have to furnish security for the due and proper exercise of his or her duties.</p>
                    @endif
                @endforeach
            </div>
            @endif
            
            @if($user->specific_inheritance()->count() > 0)
            <div class="text-justify">
                <div class="text-center">
                    <h4>INHERITANCE: SPECIAL BEQUESTS AND REMAINDER OF ESTATE</h4>
                </div>
                <p style="font-size: 15px"><i>A special bequest is a particular item or sum of money left to a specific person or organisation</i></p>
                @foreach($user->specific_inheritance() AS $sp)
                    @foreach($sp->individual_beneficiary AS $ind)
                         <p>
                            I leave to 
                            <b>{{ $ind->first_name.' '.$ind->middle_name.' '.$ind->surname }}</b>
                            @if($ind->id_number)
                                @if(strlen($ind->id_number) == 13)
                                ID number <b>{{ $ind->id_number }}</b>
                                @else
                                born <b>{{ $ind->id_number }}</b>
                                @endif
                            @endif 
                            the following:
                        </p>
                         @if($ind->value)
                         <p><b>R {{ number_format($ind->value, 2) }}</b></p>
                         @endif
                         @if($ind->description)
                         <p>{{ $ind->description }}</p>
                         @endif
                    @endforeach
                    @foreach($sp->organisation_beneficiery AS $cmp)
                        <p>I leave to the <b>{{ $cmp->organisation_type.' '.$cmp->organisation_name.' '.$cmp->registration_number }}</b> the following:</p>
                        @if($cmp->value)
                         <p><b>R {{ number_format($cmp->value, 2) }}</b></p>
                         @endif
                         @if($cmp->description)
                         <p>{{ $cmp->description }}</p>
                         @endif
                    @endforeach
                @endforeach
                
                @foreach($user->remainder_inheritance() AS $rem)
                    @if($rem->division_type == "equal_part")
                        <p>I leave the remainder of my estate in equal shares to the following heirs: </p>
                    @elseif($rem->division_type == "percentage_shares")
                        <p>I leave the remainder of my estate as follows:</p>
                    @endif
                    
                    @php
                    $count = 0;
                    $tot_count = $rem->individual_beneficiary->count() + $rem->organisation_beneficiery->count();
                    @endphp
                    
                    @foreach($rem->individual_beneficiary AS $ind)
                        @php
                        $count++;
                        @endphp
                        
                        @if($rem->division_type == "one_beneficiery")
                            <p>I leave the remainder of my estate to 
                                <b>{{ $ind->first_name.' '.$ind->middle_name.' '.$ind->surname }}</b>
                                @if($ind->id_number)
                                    @if(strlen($ind->id_number) == 13)
                                    ID number <b>{{ $ind->id_number }}.</b>
                                    @else
                                    born <b>{{ $ind->id_number }}.</b>
                                    @endif
                                @endif
                            </p>
                        @elseif($rem->division_type == "equal_part")
                            <p>
                                <b>{{ $ind->first_name.' '.$ind->middle_name.' '.$ind->surname }}</b>
                                @if($ind->id_number)
                                    @if(strlen($ind->id_number) == 13)
                                    ID number <b>{{ $ind->id_number }}</b>
                                    @else
                                    born <b>{{ $ind->id_number }}</b>
                                    @endif
                                @endif
                                
                                @if($count < $tot_count)
                                and
                                @endif
                            </p>
                        @elseif($rem->division_type == "percentage_shares")
                            <p>
                                <b>{{ $ind->percentage.'%' }}</b> to 
                                <b>{{ $ind->first_name.' '.$ind->middle_name.' '.$ind->surname }}</b>
                                @if($ind->id_number)
                                    @if(strlen($ind->id_number) == 13)
                                    ID number <b>{{ $ind->id_number }}</b>
                                    @else
                                    born <b>{{ $ind->id_number }}</b>
                                    @endif
                                @endif 
                                
                                @if($count < $tot_count)
                                and
                                @endif
                            </p>
                        @endif
                        @if($ind->description)
                            <p>{{ $ind->description }}</p>
                        @endif
                    @endforeach
                    @foreach($rem->organisation_beneficiery AS $cmp)
                        @php
                        $count++;
                        @endphp
                    
                        @if($rem->division_type == "one_beneficiery")
                            <p>I leave the remainder of my estate to the
                            @if($cmp->organisation_type == "other")
                            <b>{{ strtolower($cmp->organisation_other_type) }}</b>
                            @else
                            <b>{{ strtolower($cmp->organisation_type) }}</b>
                            @endif
                            <b>{{ $cmp->organisation_name.' '.$cmp->registration_number }}</b></p>
                        @elseif($rem->division_type == "equal_part")
                            <p>the 
                            @if($cmp->organisation_type == "other")
                            <b>{{ strtolower($cmp->organisation_other_type) }}</b>
                            @else
                            <b>{{ strtolower($cmp->organisation_type) }}</b>
                            @endif
                            <b>$cmp->organisation_name.' '.$cmp->registration_number }}</b>
                            
                            @if($count < $tot_count)
                                and
                                @endif
                            
                            </p>
                        @elseif($rem->division_type == "percentage_shares")
                            <p><b>{{ $cmp->percentage.'%' }}</b> to the
                            @if($cmp->organisation_type == "other")
                            <b>{{ strtolower($cmp->organisation_other_type) }}</b>
                            @else
                            <b>{{ strtolower($cmp->organisation_type) }}</b>
                            @endif
                            <b>{{ $cmp->organisation_name.' '.$cmp->registration_number }}</b>
                            
                            @if($count < $tot_count)
                                and
                                @endif
                            
                            </p>
                        @endif
                        @if($cmp->description)
                            <p>{{ $cmp->description }}</p>
                        @endif
                    @endforeach
                @endforeach
            </div>
            @endif
            @if($user->regular_inheritance()->count() > 0)
            <div class="tex-justify">
                <div class="text-center">
                    <h4>INHERITANCE: BEQUEST OF ENTIRE ESTATE</h4>
                </div>
                @foreach($user->regular_inheritance() AS $inh)
                    @if($inh->division_type == "equal_part")
                        <p>I leave my entire estate in equal shares to the following residue heirs:</p>
                    @elseif($inh->division_type == "percentage_shares")
                        <p>I leave the remainder of my estate as follows:</p>
                    @endif
                    
                    @php
                    $count = 0;
                    $tot_count = $inh->individual_beneficiary->count() + $inh->organisation_beneficiery->count();
                    @endphp
                    
                    @foreach($inh->individual_beneficiary AS $ind)
                        @php
                        $count++;
                        @endphp
                        @if($inh->division_type == "one_beneficiery")
                            <p>I leave my entire estate to 
                                <b>{{ $ind->first_name.' '.$ind->middle_name.' '.$ind->surname }}</b>
                                @if($ind->id_number)
                                    @if(strlen($ind->id_number) == 13)
                                    ID number <b>{{ $ind->id_number }}.</b>
                                    @else
                                    born <b>{{ $ind->id_number }}.</b>
                                    @endif
                                @endif
                            </p>
                        @elseif($inh->division_type == "equal_part")
                            <p>
                                <b>{{ $ind->first_name.' '.$ind->middle_name.' '.$ind->surname }}</b>
                                @if($ind->id_number)
                                    @if(strlen($ind->id_number) == 13)
                                    ID number <b>{{ $ind->id_number }}</b>
                                    @else
                                    born <b>{{ $ind->id_number }}</b>
                                    @endif
                                @endif
                                
                                @if($count < $tot_count)
                                and
                                @endif
                                
                            </p>
                        @elseif($inh->division_type == "percentage_shares")
                            <p>
                                <b>{{ $ind->percentage.'%' }}</b> to <b>{{ $ind->first_name.' '.$ind->middle_name.' '.$ind->surname }}</b> 
                                @if($ind->id_number)
                                    @if(strlen($ind->id_number) == 13)
                                    ID number <b>{{ $ind->id_number }}</b>
                                    @else
                                    born <b>{{ $ind->id_number }}</b>
                                    @endif
                                @endif
                                
                                @if($count < $tot_count)
                                and
                                @endif
                            </p>
                        @endif
                        @if($ind->description)
                            <p>{{ $ind->description }}</p>
                        @endif
                    @endforeach
                    @foreach($inh->organisation_beneficiery AS $cmp)
                        @php
                        $count++;
                        @endphp
                        @if($inh->division_type == "one_beneficiery")
                            <p>I leave my entire estate to the
                            @if($cmp->organisation_type == "other")
                            <b>{{ strtolower($cmp->organisation_other_type) }}</b>
                            @else
                            <b>{{ strtolower($cmp->organisation_type) }}</b>
                            @endif
                            <b>{{ $cmp->organisation_name.' '.$cmp->registration_number }}</b></p>
                        @elseif($inh->division_type == "equal_part")
                            <p>the
                            @if($cmp->organisation_type == "other")
                            <b>{{ strtolower($cmp->organisation_other_type) }}</b>
                            @else
                            <b>{{ strtolower($cmp->organisation_type) }}</b>
                            @endif
                            <b>{{ $cmp->organisation_name.' '.$cmp->registration_number }}</b>
                            
                            @if($count < $tot_count)
                                and
                            @endif
                            
                            </p>
                        @elseif($inh->division_type == "percentage_shares")
                            <p><b>{{ $cmp->percentage.'%' }}</b> to the
                            @if($cmp->organisation_type == "other")
                            <b>{{ strtolower($cmp->organisation_other_type) }}</b>
                            @else
                            <b>{{ strtolower($cmp->organisation_type) }}</b>
                            @endif
                            <b>{{ $cmp->organisation_name.' '.$cmp->registration_number }}</b>
                            
                            @if($count < $tot_count)
                            and
                            @endif
                            
                            </p>
                        @endif
                        @if($cmp->description)
                            <p>{{ $cmp->description }}</p>
                        @endif
                    @endforeach
                @endforeach
                @foreach($user->remainder_inheritance() AS $rem)
                    @if($rem->division_type == "equal_part")
                        <p>I leave the remainder of my estate in equal shares to the following heirs: </p>
                    @elseif($rem->division_type == "percentage_shares")
                        <p>I leave the remainder of my estate as follows:</p>
                    @endif
                    @foreach($rem->individual_beneficiary AS $ind)
                        @if($rem->division_type == "one_beneficiery")
                            <p>I leave the remainder of my estate to 
                                <b>{{ $ind->first_name.' '.$ind->middle_name.' '.$ind->surname }}</b>
                                @if($ind->id_number)
                                    @if(strlen($ind->id_number) == 13)
                                    ID number <b>{{ $ind->id_number }}.</b>
                                    @else
                                    born <b>{{ $ind->id_number }}.</b>
                                    @endif
                                @endif
                            </p>
                        @elseif($rem->division_type == "equal_part")
                            <p>
                                <b>{{ $ind->first_name.' '.$ind->middle_name.' '.$ind->surname }}</b>
                                @if($ind->id_number)
                                    @if(strlen($ind->id_number) == 13)
                                    ID number <b>{{ $ind->id_number }}</b>
                                    @else
                                    born <b>{{ $ind->id_number }}</b>
                                    @endif
                                @endif
                            </p>
                        @elseif($rem->division_type == "percentage_shares")
                            <p><b>{{ $ind->percentage.'%' }}</b> to 
                                <b>{{ $ind->first_name.' '.$ind->middle_name.' '.$ind->surname }}</b> 
                                @if($ind->id_number)
                                    @if(strlen($ind->id_number) == 13)
                                    ID number <b>{{ $ind->id_number }}</b>
                                    @else
                                    born <b>{{ $ind->id_number }}</b>
                                    @endif
                                @endif
                            </p>
                        @endif
                        @if($ind->description)
                            <p>{{ $ind->description }}</p>
                        @endif
                    @endforeach
                    @foreach($rem->organisation_beneficiery AS $cmp)
                        @if($rem->division_type == "one_beneficiery")
                            <p>I leave the remainder of my estate to the
                            @if($cmp->organisation_type == "other")
                            <b>{{ strtolower($cmp->organisation_other_type) }}</b>
                            @else
                            <b>{{ strtolower($cmp->organisation_type) }}</b>
                            @endif
                            <b>{{ $cmp->organisation_name.' '.$cmp->registration_number }}</b></p>
                        @elseif($rem->division_type == "equal_part")
                            <p>the
                            @if($cmp->organisation_type == "other")
                            <b>{{ strtolower($cmp->organisation_other_type) }}</b>
                            @else
                            <b>{{ strtolower($cmp->organisation_type) }}</b>
                            @endif
                            <b>{{ $cmp->organisation_name.' '.$cmp->registration_number }}</b></p>
                        @elseif($rem->division_type == "percentage_shares")
                            <p><b>{{ $cmp->percentage.'%' }}</b> to the
                            @if($cmp->organisation_type == "other")
                            <b>{{ strtolower($cmp->organisation_other_type) }}</b>
                            @else
                            <b>{{ strtolower($cmp->organisation_type) }}</b>
                            @endif
                            <b>{{ $cmp->organisation_name.' '.$cmp->registration_number }}</b></p>
                        @endif
                        @if($cmp->description)
                            <p>{{ $cmp->description }}</p>
                        @endif
                    @endforeach
                @endforeach
            </div>
            @endif
            @if($user->simulteneous_death_inheritance()->count() > 0)
            <div class="text-justify">
                <div class="text-center">
                    <h4>SIMULTANEOUS DEATH</h4>
                </div>
                <p>The following provisions under “simultaneous death” are not intended to place any restrictions on the testate freedom of my nominated beneficiary or beneficiaries but only to make sure my estate does not devolve in terms of the provisions of the South African Intestate Succession Act 81 of 1987.</p>  
                <p>Should any of my nominated beneficiaries and I die simultaneously or in the same incident and it is not clear who died first, then as a substitute of this beneficiary’s inheritance, I direct the following:</p>
                
                @foreach($user->simulteneous_death_inheritance() AS $simul)
                    
                    
                    @if($simul->sub_type == "specific_inheritance")
                        @foreach($simul->individual_beneficiary AS $ind)
                             <p>I leave to 
                                <b>{{ $ind->first_name.' '.$ind->middle_name.' '.$ind->surname }}</b>
                                @if($ind->id_number)
                                    @if(strlen($ind->id_number) == 13)
                                    ID number <b>{{ $ind->id_number }}</b>
                                    @else
                                    born <b>{{ $ind->id_number }}</b>
                                    @endif
                                @endif 
                            the following:</p>
                             @if($ind->value)
                             <p><b>R {{ number_format($ind->value, 2) }}</b></p>
                             @endif
                             @if($ind->description)
                             <p>{{ $ind->description }}</p>
                             @endif
                        @endforeach
                        @foreach($simul->organisation_beneficiery AS $cmp)
                            <p>I leave to the
                            @if($cmp->organisation_type == "other")
                            <b>{{ strtolower($cmp->organisation_other_type) }}</b>
                            @else
                            <b>{{ strtolower($cmp->organisation_type) }}</b>
                            @endif
                            <b>{{ $cmp->organisation_name.' '.$cmp->registration_number }}</b> the following:</p>
                            @if($cmp->value)
                             <p><b>R {{ number_format($cmp->value, 2) }}</b></p>
                             @endif
                             @if($cmp->description)
                             <p>{{ $cmp->description }}</p>
                             @endif
                        @endforeach
                    @endif
                    
                    @php
                    $count = 0;
                    $tot_count = $simul->individual_beneficiary->count() + $simul->organisation_beneficiery->count();
                    @endphp
                    
                    @if($simul->sub_type == "inheritance")
                        @if($simul->division_type == "equal_part")
                            <p>I leave my entire estate in equal shares to the following residue heirs:</p>
                        @elseif($simul->division_type == "percentage_shares")
                            <p>I leave the remainder of my estate as follows:</p>
                        @endif
                        @foreach($simul->individual_beneficiary AS $ind)
                            @php
                            $count++;
                            @endphp
                            @if($simul->division_type == "one_beneficiery")
                                <p>I leave my entire estate to 
                                    <b>{{ $ind->first_name.' '.$ind->middle_name.' '.$ind->surname }}</b>
                                    @if($ind->id_number)
                                        @if(strlen($ind->id_number) == 13)
                                        ID number <b>{{ $ind->id_number }}.</b>
                                        @else
                                        born <b>{{ $ind->id_number }}.</b>
                                        @endif
                                    @endif
                                </p>
                            @elseif($simul->division_type == "equal_part")
                                <p>
                                    <b>{{ $ind->first_name.' '.$ind->middle_name.' '.$ind->surname }}</b>
                                    @if($ind->id_number)
                                        @if(strlen($ind->id_number) == 13)
                                        ID number <b>{{ $ind->id_number }}</b>
                                        @else
                                        born <b>{{ $ind->id_number }}</b>
                                        @endif
                                    @endif
                                    
                                    @if($count < $tot_count)
                                        and
                                    @endif
                                </p>
                            @elseif($simul->division_type == "percentage_shares")
                                <p><b>{{ $ind->percentage.'%' }}</b> to 
                                    <b>{{ $ind->first_name.' '.$ind->middle_name.' '.$ind->surname }}</b> 
                                    @if($ind->id_number)
                                        @if(strlen($ind->id_number) == 13)
                                        ID number <b>{{ $ind->id_number }}</b>
                                        @else
                                        born <b>{{ $ind->id_number }}</b>
                                        @endif
                                    @endif
                                    
                                    @if($count < $tot_count)
                                        and
                                    @endif
                                </p>
                            @endif
                            @if($ind->description)
                                <p>{{ $ind->description }}</p>
                            @endif
                        @endforeach
                        @foreach($simul->organisation_beneficiery AS $cmp)
                            @php
                            $count++;
                            @endphp
                            
                            @if($simul->division_type == "one_beneficiery")
                                <p>I leave my entire estate to the
                                @if($cmp->organisation_type == "other")
                            <b>{{ strtolower($cmp->organisation_other_type) }}</b>
                            @else
                            <b>{{ strtolower($cmp->organisation_type) }}</b>
                            @endif
                                <b>{{ $cmp->organisation_name.' '.$cmp->registration_number }}</b></p>
                            @elseif($simul->division_type == "equal_part")
                                <p>the
                                    @if($cmp->organisation_type == "other")
                                        <b>{{ strtolower($cmp->organisation_other_type) }}</b>
                                    @else
                                        <b>{{ strtolower($cmp->organisation_type) }}</b>
                                    @endif
                                    <b>{{ $cmp->organisation_name.' '.$cmp->registration_number }}</b>
                                    
                                    @if($count < $tot_count)
                                        and
                                    @endif
                                </p>
                                @elseif($simul->division_type == "percentage_shares")
                                    <p><b>{{ $cmp->percentage.'%' }}</b> to the
                                    @if($cmp->organisation_type == "other")
                                        <b>{{ strtolower($cmp->organisation_other_type) }}</b>
                                    @else
                                        <b>{{ strtolower($cmp->organisation_type) }}</b>
                                    @endif
                                    <b>{{ $cmp->organisation_name.' '.$cmp->registration_number }}</b>
                                    @if($count < $tot_count)
                                        and
                                    @endif
                                    </p>
                            @endif
                            @if($cmp->description)
                                <p>{{ $cmp->description }}</p>
                            @endif
                        @endforeach
                    @endif
                    @if($simul->sub_type == "remainder-inheritance")
                        @if($simul->division_type == "equal_part")
                            <p>I leave the remainder of my estate in equal shares to the following heirs: </p>
                        @elseif($simul->division_type == "percentage_shares")
                            <p>I leave the remainder of my estate as follows:</p>
                        @endif
                        @foreach($simul->individual_beneficiary AS $ind)
                            @php
                            $count++;
                            @endphp
                            
                            @if($simul->division_type == "one_beneficiery")
                                <p>I leave the remainder of my estate to 
                                    <b>{{ $ind->first_name.' '.$ind->middle_name.' '.$ind->surname }}</b>
                                    @if($ind->id_number)
                                        @if(strlen($ind->id_number) == 13)
                                        ID number <b>{{ $ind->id_number }}.</b>
                                        @else
                                        born <b>{{ $ind->id_number }}.</b>
                                        @endif
                                    @endif
                                </p>
                            @elseif($simul->division_type == "equal_part")
                                <p>
                                    <b>{{ $ind->first_name.' '.$ind->middle_name.' '.$ind->surname }}</b>
                                    @if($ind->id_number)
                                        @if(strlen($ind->id_number) == 13)
                                        ID number <b>{{ $ind->id_number }}</b>
                                        @else
                                        born <b>{{ $ind->id_number }}</b>
                                        @endif
                                    @endif
                                    
                                    @if($count < $tot_count)
                                        and
                                    @endif
                                </p>
                            @elseif($simul->division_type == "percentage_shares")
                                <p><b>{{ $ind->percentage.'%' }}</b> to 
                                    <b>{{ $ind->first_name.' '.$ind->middle_name.' '.$ind->surname }}</b> 
                                    @if($ind->id_number)
                                        @if(strlen($ind->id_number) == 13)
                                        ID number <b>{{ $ind->id_number }}</b>
                                        @else
                                        born <b>{{ $ind->id_number }}</b>
                                        @endif
                                    @endif
                                    
                                    @if($count < $tot_count)
                                        and
                                    @endif
                                </p>
                            @endif
                            @if($ind->description)
                                <p>{{ $ind->description }}</p>
                            @endif
                        @endforeach
                        @foreach($simul->organisation_beneficiery AS $cmp)
                            @php
                            $count++;
                            @endphp
                            
                            @if($simul->division_type == "one_beneficiery")
                                <p>I leave the remainder of my estate to the
                                @if($cmp->organisation_type == "other")
                            <b>{{ strtolower($cmp->organisation_other_type) }}</b>
                            @else
                            <b>{{ strtolower($cmp->organisation_type) }}</b>
                            @endif
                                <b>{{ $cmp->organisation_name.' '.$cmp->registration_number }}</b></p>
                            @elseif($simul->division_type == "equal_part")
                                <p>the
                                    @if($cmp->organisation_type == "other")
                                        <b>{{ strtolower($cmp->organisation_other_type) }}</b>
                                    @else
                                        <b>{{ strtolower($cmp->organisation_type) }}</b>
                                    @endif
                                    <b>{{ $cmp->organisation_name.' '.$cmp->registration_number }}</b>
                                
                                    @if($count < $tot_count)
                                        and
                                    @endif
                                
                                </p>
                            @elseif($simul->division_type == "percentage_shares")
                                <p>
                                    <b>{{ $cmp->percentage.'%' }}</b> to the
                                    @if($cmp->organisation_type == "other")
                                        <b>{{ strtolower($cmp->organisation_other_type) }}</b>
                                    @else
                                        <b>{{ strtolower($cmp->organisation_type) }}</b>
                                    @endif
                                    <b>{{ $cmp->organisation_name.' '.$cmp->registration_number }}</b>
                                    
                                    @if($count < $tot_count)
                                        and
                                    @endif
                                        
                                </p>
                            @endif
                            @if($cmp->description)
                                <p>{{ $cmp->description }}</p>
                            @endif
                        @endforeach
                    @endif
                @endforeach
            </div>
            @endif
            @if($user->trusts->count() > 0)
            <div class="text-justify">
                @foreach($user->trusts AS $trust)
                    <div class="text-center">
                        @if($trust->trust_type == "minor-children")
                            <h4>TESTAMENTARY TRUST FOR A MINOR BENEFICIARY </h4>
                        @elseif($trust->trust_type == "incapacity")
                            <h4>TESTAMENTARY TRUST (SPECIAL TRUST TYPE A)</h4>
                        @endif
                    </div>
                    <h4>Interpretation</h4>
                    <p>In this Trust Deed, unless the context indicates otherwise, words denoting the singular shall include the plural and vice versa, and words denoting one gender shall include the other.</p>
                    
                    <p>Headings to clauses are for reference purposes only and are not to be used in the interpretation thereof.</p>        
                    <h4>Beneficiaries to the Trust</h4>
                    @if($trust->trust_type == "minor-children")
                        <p>If a beneficiary of my estate is a minor under the age of 18 (eighteen years) at the time of the development of my estate, then all the assets awarded and accrued to this beneficiary shall be administered by the trustees of this trust. Such a beneficiary shall be deemed to be an income and capital beneficiary to this trust. The trust shall continue until my trustees in their discretion terminate the trust, and/or my trustees have the option to terminate the trust when all or some of the minor beneficiaries reaches the age of eighteen years old.</p>
                    @elseif($trust->trust_type == "incapacity")
                        <p>The asset/s awarded and accrued to 
                        @php
                        $count = 1;
                        @endphp
                        @foreach($user->trust_beneficiaries AS $ben)
                            <b>{{ $ben->name.' '.$ben->middle_name.' '.$ben->surname }}</b>
                            @php
                            $count++;
                            @endphp
                            
                            @if($user->trust_beneficiaries->count() > 1)
                                @if($count > 1 && $count < $user->trust_beneficiaries->count())
                                ,
                                @else
                                and
                                @endif
                            @endif
                            
                        @endforeach
                        in this last Will and Testament shall be administered by the trustees of this trust. Such a beneficiary shall be deemed to be an income and capital beneficiary to this trust.</p>
                    @endif
                    
                    <h4>Nomination of Trustees and Waiver of Security</h4>
                    <p>I hereby nominate as the trustees of my trust,@php $count = 0; @endphp@foreach($trust->trustees AS $trustee) @php $count++; $tot = $trust->trustees->count(); @endphp <b>{{$trustee->first_name.' '.$trustee->middle_name.' '.$trustee->surname}}</b>@if($count > 1 && $count == ($tot - 1)) and @elseif($count < $tot),@endif @endforeach granting them all the powers and authority they need or are allowed in law.</p>
                    <p>No person appointed as trustee is required to furnish security to the Master of the High Court or to anybody else to fulfil his or her duties as trustee. The Master of the High Court is hereby requested to dispense with such security.</p>
                    
                    <h4>Loss of Office</h4>
                    <p>Any trustee shall immediately resign if: *a trustee’s estate is sequestrated; * a trustee files an application for the surrender of his/her estate or an application for an administration order; *a trustee commits an act of insolvency as defined in the Insolvency Act for the time being in force; *a trustee makes any arrangements or composition with his/her creditors generally; *a trustee is found to be of unsound mind; *a trustee is declared incapable of managing his/her own affairs or of acting as trustee; *a trustee is convicted of any crime involving dishonesty; *a trustee is disqualified in terms of the Companies Act as amended from time to time to act as a director of a company.</p>
                    <h4>Powers to Invest and Maintain the Monies or Assets</h4>
                    <p>My trustees shall invest and maintain the monies or assets of the trust during the continuance of the trust with the care, diligence and skill which can reasonably be expected of a person who manages the affairs of another. My trustees shall always exercise impartial and independent discretion in any type of investment and in the maintenance of the trust's monies and assets. My trustees shall have the right to vary or transpose such investments from time to time, to realise these investments and to re-invest such investments in good faith to the advantage of the trust and its beneficiaries. Any income in the trust not utilised by my trustees shall be re-invested by them in terms of this clause. Any type of investment or reinvestment of the trust’s monies shall be done with the assistance of an FPSB registered and approved financial advisor who holds a CFP certification.</p>  
                    <h4>Powers of Encumbrance</h4>
                    <p>Encumbrance is the use of property as surety to get a loan for needed funds (for example, a house can be encumbered with a bond if money is needed for expensive medical treatment for a beneficiary)</p>
                    <p>My trustees are entitled to encumber any of the assets of the trust as they consider necessary for the overall well-being and interest of the trust and its beneficiaries.</p> 
                    <p>My trustees will be allowed to borrow money for any purpose of the trust and to secure the repayment of such a loan in a manner that supports the overall well-being and interest of the trust and its beneficiaries. </p>
                    <p>My trustees will be entitled to mortgage or pledge or lease any of the assets of the trust for the overall well-being and interest of the trust and its trust beneficiaries.</p>
                    <h4>Benefit Awards from the Income of this Trust</h4>
                    <p>My trustees shall use the income of this trust, or as much as they decide is necessary, for the physical wellbeing, health, education and maintenance needs of the beneficiaries. Should the income not be sufficient for this, my trustees may use as much of the capital as necessary. Overall, it is my trustees who have the power to decide on what to allow as reasonable maintenance and reasonable expenses.</p>
                    <h4>General Powers of Trustees</h4>
                    <p>Without prejudice to the generality of or in any way limiting the powers of my trustees, my trustees shall be entitled to:</p>
                    <ul>
                        <li>Exercise the voting power attached to any shares forming part of the trust assets in a way they consider to be in the best interest of the trust.</li>
                        <li>Acquire shares in any private or public company incorporated in or outside the Republic of South Africa.</li>
                        <li>Acquire in the name of the trust, or as trustees of the trust, any movable or immovable property and to register this property in the name of the trust. My trustees may from time-to-time exchange, sell, lease or otherwise deal with this property or any portion of it and grant the rights to prospect or to acquire the same servitude or other rights as my trustees may consider necessary.</li>
                        <li>Dispose of movable or immovable property comprising a portion of the trust assets in such a manner as the trustees may deem fit.
                        <li>Institute or defend legal proceedings in the name of the trust or to proceed to arbitration for any matter and to sign all deeds, powers of attorney and any other documents that may be necessary in these circumstances.</li>
                        <li>Pay to the service provider (like a school, medical practitioner, bookshop) and/or to the beneficiary’s guardian for the reasonable maintenance and/or expenses of the beneficiary.</li>
                        <li>Exercise the powers vested in them as trustees to take whatever actions necessary to achieve the objects and purposes of the trust.</li>
                        <li>To make use of the services of professional persons such as attorneys, accountants, medical doctors, financial advisors and any other consultants that my trustees may consider necessary.</li> 
                    </ul>    
                    <h4>Obligations of Trustees</h4>
                    <p>My trustees shall always:</p>
                    <ul>
                        <li>In the performance of his/her duties act with the care, diligence and skill which can reasonably be expected of a person who manages the affairs of another.</li>
                        <li>Always act jointly in transactions with third parties and exercise their contractual powers together.</li>
                        <li>Open and operate a bank account in the name of the trust.</li>
                        <li>Keep trust property separate from their own estate and any other estate.</li>
                        <li>Cause proper records and books of account to be kept reflecting truly and correctly his/her administration of the trust.</li>
                        <li>Ensure that a balance sheet of the assets and liabilities of the Trust is prepared annually in respect of each year by a registered public accountant.</li>
                    </ul>
                    <h4>Trustees’ Remuneration</h4>
                    <p>My trustees shall be entitled to charge only 0.5% commission on income accruing per year. No fees are allowed on capital distributed and no acceptance fee is allowed on capital introduced. </p> 
                    <h4>Vesting of the Beneficiary's Inheritance and Termination of Trust</h4>
                    <p>Any right to income or any assets of my beneficiary in this trust shall not be subject to the right of attachment nor to the right to sell in execution. The income rights or assets due to a trust beneficiary shall under no circumstances form part of a portion of the estate of such beneficiary before such rights and assets have been transferred or paid over to the beneficiary.</p>
                    <p>In the event of a trust beneficiary’s estate being declared insolvent or if the trust beneficiary makes an arrangement with creditors, my trustees shall then retain the inheritance in trust, and in their absolute discretion shall use the capital and the income of the inheritance for the maintenance of the beneficiary and his/her dependants. The maintenance includes what is needed for the physical wellbeing, health, and education of the beneficiary and his/her dependants.</p>
                    <p>The trust shall continue until my trustees in their discretion terminate the trust. My trustees may in their discretion decide when to transfer a trust beneficiary's income and capital share. The beneficiary's share of the trust shall then be capitalised and the capital together with the accumulated income, less advances made, shall devolve upon him or her entirely to deal with as he or she deems fit. Therefore, when calculating the share to be awarded to each beneficiary, my trustees shall take into consideration the benefits received by each beneficiary during the duration of the trust that were reasonably needed for the physical wellbeing, health, and education of such beneficiary. My trustees must in their sole discretion take all relative factors into consideration to make just awards.</p>
                @endforeach
                
                
            </div>
            @endif
            <div class="text-justify">
                <div class="text-center">
                    <h4>MARITAL EXCLUSION</h4>
                </div>
                <p>I direct that any inheritance or income accruing in terms of this will and trust shall not form part of any existing or future community estate. The right of accrual as referred to in the Matrimonial Property Act 88 of 1984 is hereby specifically excluded from any inheritance received in terms of this will and trust. Any inheritance or income accruing in terms of this will and trust shall not be subject to the right of attachment or execution by any creditor of any spouse or partner.</p>
            </div>
            @if($user->funeral_wishes)
            @if($user->funeral_wishes->cremated == "yes" || $user->funeral_wishes->organ_donor == "yes")
            <div class="text-justify">
                <div class="text-center">
                    <h4>FUNERAL WISHES</h4>
                </div>
                @if($user->funeral_wishes->organ_donor == "yes")
                <p>Disposal of organs: I direct that any useable tissues and organs of myself may be donated to any hospital or institution selected by my executors and/or immediate family members to be used in a way my executors and/or immediate family members deem suitable. I authorise my executors and/or immediate family members to sign all the prescribed forms required to carry out my instructions.</p>
                @endif
                @if($user->funeral_wishes->cremated == "yes")
                <p>Provision for cremation: I direct that my executors and/or immediate family members shall arrange for my body to be cremated and the ashes to be given to my family to do with as they please. If I die in a country outside South Africa, my wish is for my body to be cremated in the country of my death, if possible, and only my ashes to be returned to South Africa. I further direct that all cremation expenses shall be paid out of my Estate. </p>
                @endif
                @if($user->funeral_wishes->special_wishes)
                <p>Special instructions</p>
                <p>{{ $user->funeral_wishes->special_wishes }}</p>
                @endif
            </div>
            @endif
            @endif
            <div class="text-justify">
                <p>SIGNED AT ................................................................................... SOUTH AFRICA <br />
                ON THE ..........................................................................................20...................... <br />
                in the presence of the subscribing witnesses all being present and signing at the same time and in the presence of one another.</p>
                <div class="container">
                    <table class="w-100">
                        <tr>
                            <td>
                                <div class="">
                                    <u>AS WITNESS:</u>
                                </div>
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>
                                <ol class="sig_list">
                                    <li><input type="text" class="signature" /></li>
                                    <li><input type="text" class="signature" /></li>
                                </ol>
                            </td>
                            <td>
                                <ul class="sig_list">
                                    <li>
                                        <input type="text" class="signature" /><br />
                                        <b>TESTATOR</b>
                                    </li>
                                </ul>
                            </td>
                        </td>
                    </table>
                </div>
            </div>
            <div class="text-justify">
                <div class="text-center">
                    <h4>DECLARATION BY WITNESSES OF THIS WILL</h4>
                </div>
                <p>
                    @if($user->witnesses->count() > 1)
                    We, 
                    @else
                    I,
                    @endif
                    @php
                    $count = 0;
                    @endphp
                    @foreach($user->witnesses AS $witness)
                    @php
                    $count++;
                    @endphp
                    @if($count > 1 && $count == $user->witnesses->count())
                    &nbsp;and&nbsp;
                    @elseif($count > 1)
                        ,&nbsp;
                    @endif
                    <b>{{ $witness->first_name.' '.$witness->middle_name.' '.$witness->surname }}</b>
                    @endforeach
                declare as follows:</p>
                
                <p>This instrument was signed on the above written date by the testator <b>{{ $user->name.' '.$user->middle_name.' '.$user->surname }}</b>, and in our presence, <b>{{ $user->name.' '.$user->middle_name.' '.$user->surname }}</b>, declared this instrument to be the testator's last will and testament.  At the request of <b>{{ $user->name.' '.$user->middle_name.' '.$user->surname }}</b> and in the presence of <b>{{ $user->name.' '.$user->middle_name.' '.$user->surname }}</b>, we subscribe our names as witnesses hereto. Each of us observed the signing of the will and testament by <b>{{ $user->name.' '.$user->middle_name.' '.$user->surname }}</b> and by each other subscribing we witnessed and affirmed that each signature is the true signature of the person whose name was signed. Each of us is now 14 years or older and a competent witness. To the best of our knowledge <b>{{ $user->name.' '.$user->middle_name.' '.$user->surname }}</b> is legally empowered to make a will and testament, is mentally competent and under no constraint or undue influence.</p>
                
                @php
                $count = 0;
                @endphp
                @foreach($user->witnesses AS $witness)
                @php
                $count++;
                @endphp
                <div class="" style="width: 100%">
                    <table style="width: 100%">
                        <thead>
                            <tr>
                                <th style="text-align: left">Witness {{ $count }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Name: </td>
                                <td><b>{{ $witness->first_name.' '.$witness->middle_name.' '.$witness->surname }}</b></td>
                            </tr>
                            <tr>
                                <td>{{ $witness->idType() }}: </td>
                                <td><b>{{ $witness->id_number }}</b></td>
                            </tr>
                            <tr>
                                <td>Contact details: </td>
                                <td><b>{{ $witness->phone_number }}</b></td>
                            </tr>
                            <tr>
                                <td>Address:</td>
                                <td><b>{{ $witness->address.', '.$witness->city.', '.$witness->postal_code.', '.$witness->province }}</b></td>
                            </tr>
                            <tr>
                                <td>Signature: </td>
                                <td>_______________________________________</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                @endforeach
            </div>
        </div>
        
        <script type="text/php">
            if (isset($pdf)) {
                $x = 250;
                $y = 10;
                $text = "Page {PAGE_NUM} of {PAGE_COUNT}";
                $font = $fontMetrics->get_font("helvetica", "normal");
                $size = 12;
                $color = array(0,0,0);
                $word_space = 2;  //  default
                $char_space = 2;  //  default
                $angle = 0.0;   //  default
                $pdf->page_text($x, $y, $text, $font, $size, $color);
            }
        </script>
    </body>
</html>