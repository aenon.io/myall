@props(['url'])
<tr>
  <td class="header" style="width: 100%">
    <a href="{{ $url }}" style="display: inline-block; width: 570px">
      <div style="background-color: #040A5E; padding: 2%; width: 100%">
        <img src="{{ asset('img/logotype.png') }}" width="150">
      </div>
    </a>
  </td>
</tr>
