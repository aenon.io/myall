<?php

use Illuminate\Support\Facades\Route;

use App\Http\Livewire\Account\Profile\Profile;
use App\Http\Livewire\Account\FamilyTree\FamilyTree;
use App\Http\Livewire\Account\MyNetwork\MyNetwork;
use App\Http\Livewire\Account\Organiser\Organiser;
use App\Http\Livewire\ShareProfile;
use App\Http\Livewire\ContactUs;
use App\Http\Livewire\Account\Share\Share;

use App\Http\Controllers\AuthController;
use App\Http\Livewire\Landing\ShareProfile as ViewShareProfile;
use App\Http\Livewire\Auth\ResetPassword;
use App\Http\Livewire\Auth\ForgotPassword;

use App\Http\Livewire\Auth\Register;
use App\Http\Livewire\Auth\Login;

use App\Http\Livewire\Landing\Home;
use App\Http\Livewire\Landing\Pricing;
use App\Http\Livewire\Account\Search\Search;

use App\Models\User;

use App\Lib\willPdfGen;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('mail', function () {
    $data = [
        'name' => 'Admin',
        'surname' => 'Root',
    ];
    return view('mail.welcome', $data);
});

Route::get('will', [willPdfGen::class, 'getPdf']);

Route::get('register', Register::class)->name('register');
Route::get('register/{view}/{user_id}', Register::class);
Route::post('register/{view}/{user_id}', Register::class);
Route::get('logout', [AuthController::class, 'logOut']);
Route::get('login', Login::class)->name('login');
Route::get('forgot-password', ForgotPassword::class);

Route::get('/', Home::class);
// Route::get('pricing', Home::class);
Route::get('pricing', function () {
    return redirect('/#pricing');
});
Route::get('landing/contact-us', function () {
    return view('landing.contact_us');
});

Route::middleware(['auth'])->group(function () {
    Route::get('profile', Profile::class);
    Route::get('profile/{view}', Profile::class);

    Route::get('my-network', MyNetwork::class);

    Route::get('organiser', Organiser::class);
    Route::get('organiser/{view}', Organiser::class);
    Route::get('organiser/{view}/{stage}', Organiser::class);
    Route::get('organiser/{view}/{stage}/{review_page}', Organiser::class);

    Route::get('share-profile', Share::class);
    Route::get('share-profile/{view}', Share::class);
    Route::get('share-profile/{view}/{id}', Share::class);

    Route::get('contact-us', ContactUs::class);

    Route::get('search', Search::class);
});

Route::get('profile/{id}/{hash}', function () {
    Session::put('invited', true);
    return redirect('pricing');
});

Route::get('password', ResetPassword::class)->name('password.reset');

// Route::get('home', Home::class);
// Route::get('family-tree', FamilyTree::class);
